<?php

namespace WSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AdminBundle\Controller\CouponController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use AdminBundle\Entity\Userinfo;
use AdminBundle\Entity\Rolemaster;
use AdminBundle\Entity\Gcmuser;
use AdminBundle\Entity\Apnsuser;
use AdminBundle\Entity\users;

class WSAutosugestController extends WSBaseController {

    /**
     * @Route("/ws/autosuggestlist/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function autosuggestlistAction(Request $request) {

        $this->title = "Auto suggest";
        $param = $this->requestAction($request, 0);

        $this->validateRule = array(
            array(
                'rule' => 'NOTNULL',
                'field' => array(),
            ),
        );

        if ($this->validateData($param)) {
            $flag = '' ;            
            $CourseunitRepository = $this->getDoctrine()->getManager()->getRepository('AdminBundle:course_unit');
          
               
                $CourseunitList = $CourseunitRepository->createQueryBuilder()
                                ->select('unit_name','is_deleted')
                               ->field('is_deleted')->equals("0")
                                ->getQuery()->execute()->toArray();
                $unit_name = [] ;
                if($CourseunitList){
                    foreach($CourseunitList as $ckey=>$cval){
                        $unit_name[] = $cval->getUnitName();
                    }
                }
                  // var_dump($unit_name);exit;                
                $response = $unit_name ; 
               
           
        } else {
            $this->error = "PIM";
        }

        if (empty($response)) {
            $response = false;
            $this->error = "NRF";
        }

        $this->data = $response;
        return $this->responseAction();
    }

}

?>
