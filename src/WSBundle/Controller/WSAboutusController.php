<?php
namespace WSBundle\Controller;

use Symfony\Component\HttpFoundation\Request;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class WSAboutusController extends WSBaseController{
    /**
     * @Route("/ws/getAboutUs/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function aboutusAction($param, Request $request){
		try{
			$response = false;
			$faq_arr = [];
			$this->title = "About us";
			//$param = $this->requestAction($this->getRequest());
			$param = $this->requestAction($request,0);
			$this->validateRule = array(
				array(
					'rule'=>'NOTNULL',
					'field'=>array('domain_id'),
				),
			);
            		
			if($this->validateData($param)){

				$domain_id = $param->domain_id;
				$language_id = isset($param->language_id) ? $param->language_id : 1;
				
				$em = $this->getDoctrine()->getManager();
				$conn = $em->getConnection();
				$about_us = $em->getRepository('AdminBundle:Aboutus')
					->findOneBy(array(
						'language_id'=>$language_id,
						'domain_id' => $domain_id,
						'is_deleted'=>0
					)
				);

				if(!empty($about_us)){
					
					$response = $about_us->getDescription();					
					$this->error = "SFD";
				}
				else{
					$this->error = "NRF";
				}
			}else{
				$this->error = "PIM";
			}
			if(empty($response)){
				$response = null;
			}
			$this->data = $response;
			return $this->responseAction();
		}
		catch(\Exception $e){
			$this->error = "SFND".$e;
			$this->data = false;
			return $this->responseAction();
		}
	}
}
?>