<?php

namespace WSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AdminBundle\Controller\CouponController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use AdminBundle\Entity\Userinfo;
use AdminBundle\Entity\Rolemaster;
use AdminBundle\Entity\Gcmuser;
use AdminBundle\Entity\Apnsuser;
use AdminBundle\Entity\users;
use AdminBundle\Entity\coursemaster;
use AdminBundle\Entity\Tournament;

class WSFilterClassController extends WSBaseController {

    /**
     * @Route("/ws/filterclass/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function filterclassAction(Request $request) {

        $this->title = "filterclass";
        $param = $this->requestAction($request, 0);

        $em = $this->getDoctrine()->getManager();
        $this->validateRule = array(
            array(
                'rule' => 'NOTNULL',
                'field' => array(),
            ),
        );
        if ($this->validateData($param)) {
            $skip = 0 ; $limit = 100 ;
             if(isset($param->limit)){
                 $limit = $param->limit ; //20 ;
             }
             if(isset($param->skip)){
                 $skip = $param->skip ; //20 ;
             }

            $repository = $this->getDoctrine()->getManager()->getRepository('AdminBundle:course_unit');
            $coursemaster = $repository->createQueryBuilder()
                            ->sort('id', 'desc')
                            ->field('youtubelink')->notEqual("")
                            ->field('is_deleted')->equals("0") 
                            ->limit($limit)
                            ->skip($skip)
                            ->getQuery()->execute()->toArray();
            if (isset($param->course_type)) {
                $course_type = $param->course_type;
                $coursemaster = $repository->createQueryBuilder()
                                ->field('course_type')
                                ->equals($course_type)
                                ->field('youtubelink')
                                ->notEqual("")
                                ->field('is_deleted')
                                ->equals('0')
                                ->sort('id', 'desc')
                                 ->limit($limit)
                            ->skip($skip)
                                ->getQuery()->execute()->toArray();
            }
            if (isset($param->course_id)) {
                $course_id = $param->course_id;
                $course_id_arr =  explode(",",$course_id);
                  $course_unit_relation_list_arr =  $this->getDoctrine()->getManager()->getRepository('AdminBundle:course_unit_relation')->createQueryBuilder()
                             ->field('is_deleted')->equals("0")
                             ->field('relation_with')->equals("course")
                             ->field('relation_object_id')->in($course_id_arr)
                            ->getQuery()->execute()->toArray();
                  
               
                $course_unit_IDS = [] ;
                if($course_unit_relation_list_arr){
                    foreach($course_unit_relation_list_arr as $cukey=>$cuval){
                        $course_unit_IDS[] = $cuval->getCourse_unit_id();
                    }
                }
                $coursemaster = $repository->createQueryBuilder()                                
                                ->field('youtubelink')->notEqual("")
                                ->field('id')->in($course_unit_IDS)
                                ->sort('id', 'desc')
                                ->getQuery()->execute()->toArray();
               // var_dump($coursemaster);exit;
            }            
            if (isset($param->category_id)) {
                $category_id = $param->category_id;
                $category_id_arr =  explode(",",$category_id);
                $course_unit_relation_list_arr =  $this->getDoctrine()->getManager()->getRepository('AdminBundle:course_unit_relation')->createQueryBuilder()
                             ->field('is_deleted')->equals("0")
                             ->field('relation_with')->equals("category")
                             ->field('relation_object_id')->in($category_id_arr)
                            ->getQuery()->execute()->toArray();                  
               
                $course_unit_IDS = [] ;
                if($course_unit_relation_list_arr){
                    foreach($course_unit_relation_list_arr as $cukey=>$cuval){
                        $course_unit_IDS[] = $cuval->getCourse_unit_id();
                    }
                }
                $coursemaster = $repository->createQueryBuilder()                                
                                ->field('youtubelink')->notEqual("")
                                ->field('id')->in($course_unit_IDS)
                                ->sort('id', 'desc')
                                ->getQuery()->execute()->toArray();
               
            }
            if (isset($param->course_id) && isset($param->course_type)) {
                $course_id = $param->course_id;
                $course_type = $param->course_type;               
                $course_id_arr =  explode(",",$course_id);
                $course_unit_relation_list_arr =  $this->getDoctrine()->getManager()->getRepository('AdminBundle:course_unit_relation')->createQueryBuilder()
                             ->field('is_deleted')->equals("0")
                             ->field('relation_with')->equals("course")
                             ->field('relation_object_id')->in($course_id_arr)
                            ->getQuery()->execute()->toArray(); 
                $course_unit_IDS = [] ;
                if($course_unit_relation_list_arr){
                    foreach($course_unit_relation_list_arr as $cukey=>$cuval){
                        $course_unit_IDS[] = $cuval->getCourse_unit_id();
                    }
                }
                $coursemaster = $repository->createQueryBuilder()                                
                                ->field('youtubelink')->notEqual("")
                                ->field('id')->in($course_unit_IDS)
                                ->field('course_type')->equals($course_type)
                                ->sort('id', 'desc')
                                ->getQuery()->execute()->toArray();
            }
            if (isset($param->course_id) && isset($param->category_id)) {
                $course_id = $param->course_id;
                $category_id = $param->category_id;
                $course_id_arr =  explode(",",$course_id);
                $category_id_arr =  explode(",",$category_id);
                $course_category_arr = array_merge($course_id_arr,$category_id_arr);
                $course_unit_relation_list_arr =  $this->getDoctrine()->getManager()->getRepository('AdminBundle:course_unit_relation')->createQueryBuilder()
                             ->field('is_deleted')->equals("0")
                             ->field('relation_object_id')->in($course_category_arr)
                            ->getQuery()->execute()->toArray(); 
                $course_unit_IDS = [] ;
                if($course_unit_relation_list_arr){
                    foreach($course_unit_relation_list_arr as $cukey=>$cuval){
                        $course_unit_IDS[] = $cuval->getCourse_unit_id();
                    }
                }
                $coursemaster = $repository->createQueryBuilder()                                
                                ->field('youtubelink')->notEqual("")
                                ->field('id')->in($course_unit_IDS)                               
                                ->sort('id', 'desc')
                                ->getQuery()->execute()->toArray();               
            }
            if (isset($param->course_type) && isset($param->category_id)) {
                $course_type = $param->course_type;
                $category_id = $param->category_id;
                $coursemaster = $repository->createQueryBuilder()
                                ->field('course_type')
                                ->equals($course_type)
                                ->field('youtubelink')
                                ->notEqual("")
                                ->field('course_media_id')
                                ->equals($category_id)
                                ->sort('id', 'desc')
                                ->getQuery()->execute()->toArray();
            }
            if (isset($param->course_type) && isset($param->category_id) && isset($param->course_id)) {
                $course_type = $param->course_type;
                $category_id = $param->category_id;
                $course_id = $param->course_id;
                $coursemaster = $repository->createQueryBuilder()
                                ->field('coursemaster_id')
                                ->equals($course_id)
                                ->field('course_type')
                                ->equals($course_type)
                                ->field('youtubelink')
                                ->notEqual("")
                                ->field('course_media_id')
                                ->equals($category_id)
                                ->sort('id', 'desc')
                                ->getQuery()->execute()->toArray();
            }

//            $Outer_categorymaster = $repository1->createQueryBuilder()
//                           ->field('is_deleted')->equals("0")
//                           ->field('category_type')->equals('category')
//                           ->sort('sort_order', 'asc')
//                           ->getQuery()->execute()->toArray();
//            if($Outer_categorymaster){
//                foreach($Outer_categorymaster as $okey=>$oval){
//                    $category_class_list = $category_courseunitIDS = NULL;
//                    // fetch category course 
//                     $course_unit_relation_list = $em
//                                    ->getRepository('AdminBundle:course_unit_relation')
//                                    ->findBy(array('is_deleted' => "0", 'relation_object_id' => $oval->getId(), "relation_with" => "category"));
//                     if($course_unit_relation_list){
//                         foreach($course_unit_relation_list as $cukey=>$cuval){
//                             $category_courseunitIDS = $cuval->getCourse_unit_id();                         
//                             
//                         }
//                     }
//                    $outer_category_arr = array(
//                        "id"=>$oval->getId(),
//                        "name"=>$oval->getName(),
//                        "name"=>$oval->getDescription(),
//                        "classlist"=>$category_class_list
//                    );
//                }
//            }
            if (!empty($coursemaster)) {
                foreach ($coursemaster as $value) {
                    $url = $value->getYoutubelink();
                    $img_url = '';
                    $query_v = '';
                    $views = 0;
                    $time = $flag = $publishedAt = "";
                   // $category_id = 17;
                    $course_type = $value->getCourseType();
                  

                    $course_id_arr = [];
                    $course_name = '';
                    $course_unit_relation_list = $em
                            ->getRepository('AdminBundle:course_unit_relation')
                            ->findBy(array('is_deleted' => "0", 'course_unit_id' => $value->getId(),'relation_with'=>'course'));
                    if($course_unit_relation_list){
                        foreach($course_unit_relation_list as $delkey=>$delval){
                            $course_id_arr[] = $delval->getRelation_object_id();
                        }
                    }
                
                    if($course_id_arr){
                        foreach($course_id_arr as $ckey=>$cval){
                            $courseC = $em->getRepository('AdminBundle:coursemaster')->find($cval);            
                            if (!empty($courseC)) {
                                if($course_name == ""){
                                    $course_name = $courseC->getCourseName();
                                }
                                else{
                                    $course_name = $course_name . " , " .$courseC->getCourseName();
                                }
                            }
                        }
                    }

                    $course_type = $value->getCourseType();
                    $course_Tags = $value->getTag_id();
                    $course_tag_list = NULL;
                    if(!empty($course_Tags) && $course_Tags != '' && $course_Tags != NULL){
                        $course_Tags = explode(",",$course_Tags);
                       
                        foreach($course_Tags as $tagval){
                             $course_tag_info = $this->getDoctrine()->getManager()->getRepository('AdminBundle:course_unit_tag')->find($tagval);                                 
                             if($course_tag_info){
                                 $course_tag_list[] = array("tag_id"=>$tagval,"tag_name"=>$course_tag_info->getTag_name());
                             }
                        } 
                    }

                    $newdata[] = array(
                        "main_id" => $value->getId(),
                        "unit_code" => $value->getUnitCode(),
                        "title" => $value->getUnitName(),
                        "cover_image" => $this->getmedia1Action($value->getClass_cover_image(), $this->container->getParameter('live_path')),                        
                        "course_type"=>$course_type,
                      //  "category_id"=>$category_id,
                        "youtubelink" => $url,
                        "live_type" => $value->getPrerequisites(),
                        "course_type" => $value->getCourseType(),
                        "course_name" => $course_name,
                        "tags"=>$course_tag_list
                    );
                }
              
                $this->error = "SFD";
                $this->error_msg = "";
                $response = $newdata;
            } else {
                $this->error = "NRF";
            }
        } else {
            $this->error = "PIM";
        }

        if (empty($response)) {
            $response = false;
        }
        $this->data = $response;
        return $this->responseAction();
    }

}
