<?php
namespace WSBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AdminBundle\Controller\CouponController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use AdminBundle\Entity\Userinfo ;
use AdminBundle\Entity\Rolemaster ;
use AdminBundle\Entity\Gcmuser ;
use AdminBundle\Entity\Apnsuser ;
use AdminBundle\Entity\usermaster;
class WSForgotpasswordController extends WSBaseController
{
  /**
   * @Route("/ws/forgotpassword/{param}",defaults ={"param"=""},requirements={"param"=".+"})
   *
   */
    public function forgotpasswordAction($param, Request $request) {
    
    $entity = $this->getDoctrine()->getManager();
    // begin transaction and suspend auto commit
    
    $con = $entity->getConnection();
    
    //try{
      $this->title = "Manage Otp";
      $param = $this->requestAction($request, 0);
      
      $this->validateRule = array(
        array(
          'rule' => 'NOTNULL',
          'field' => array('mobile_no','operation')
        ),
      );
      
      if ($this->validateData($param)) {        
        $operation = !empty($param->operation) ? $param->operation : 'send' ;
        $mobile_no = !empty($param->mobile_no) ? $param->mobile_no : 0 ;
        $user_id = !empty($param->user_id) ? $param->user_id : 0 ;
        $otp = !empty($param->otp) ? $param->otp : 0 ;
  
        $em = $this->getDoctrine()->getManager();
        $user_details = $em->getRepository("AdminBundle:usermaster")->findOneBy(array("user_mobile"=>$mobile_no ));
          //var_dump($user_details);exit;
          if($user_details){
            $mobile_no = $user_details->getUser_mobile();  
          }else{
            $this->error = "UNF";
            
            return $this->responseAction();
          }
        $user_details =null;
        if($operation == 'send_again' || $operation == 'verify_again'){
/*          
          $sql = "UPDATE user_master SET email_verified = '1' where user_master_id = '$user_id'";
*/
          $user_details = $em->getRepository("AdminBundle:usermaster")->findOneBy(array("user_mobile"=>$mobile_no ));
          //var_dump($user_details);exit;
          if($user_details){
            $mobile_no = $user_details->getUser_mobile();  
          }else{
            $this->error = "UNF";
            
            return $this->responseAction();
          }
        }
        
        $auth_key = '249885AXRjvpYf3m5c023864';
        $message = urlencode("Welcome to Chessbase. Your OTP verification code is ##OTP##.") ;
        $sender = 'Gchess';
        if(strpos($mobile_no, '+') == false){
              $mobile_no = "+91".$mobile_no;
            }
        
        if(!empty($mobile_no)){
          if($operation == 'send' || $operation == 'send_again' ){
            $curl = curl_init();
            curl_setopt_array($curl, array(
              CURLOPT_URL => "http://control.msg91.com/api/sendotp.php?authkey=$auth_key&message=$message&sender=$sender&mobile=$mobile_no",
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "POST",
              CURLOPT_POSTFIELDS => "",
              CURLOPT_SSL_VERIFYHOST => 0,
              CURLOPT_SSL_VERIFYPEER => 0,
            ));
            $response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);
            
            $response_data = json_decode($response);
            
            
            if ($err) {
              $this->error = "SFND";
            } else {
              if(isset($response_data->type) && $response_data->type == 'success' ){
                $response = true;
                $this->error = "SFD";               
              }else{
                $this->error = "SFND";  
              }
            }
          }
          
          if($operation == 'verify' || $operation == 'verify_again'){
          
            $curl = curl_init();
            curl_setopt_array($curl, array(
              CURLOPT_URL => "https://control.msg91.com/api/verifyRequestOTP.php?authkey=$auth_key&mobile=$mobile_no&otp=$otp",
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "POST",
              CURLOPT_POSTFIELDS => "",
              CURLOPT_SSL_VERIFYHOST => 0,
              CURLOPT_SSL_VERIFYPEER => 0,
              CURLOPT_HTTPHEADER => array(
              "content-type: application/x-www-form-urlencoded"
              ),
            ));
            $response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);
            
            $response_data = json_decode($response);
            
            if($response_data->message == "otp_not_verified" ){
               $this->error = "INO";  
               $this->data = $response;
              return $this->responseAction();
            }
            if ($err) {
              $this->error = "SFND";  
            } else {
              if(isset($response_data->type) == 'success' ){
                
                if($operation == 'verify_again'){
                  
                  if(!empty($user_details)){
                    $user_details->setEmail_verified(1);
                    $em->flush();
                  } 
                  
                }
        
                $response = true;
                $this->error = "SFD";               
              }else{
                  
                $response = $response_data->message;
                $this->error = "SFND";  
              }
            }
          
          }
          
          if($operation == 'resend' ){
          
            $curl = curl_init();
            curl_setopt_array($curl, array(
              CURLOPT_URL => "http://control.msg91.com/api/retryotp.php?authkey=$auth_key&mobile=$mobile_no",
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "POST",
              CURLOPT_POSTFIELDS => "",
              CURLOPT_SSL_VERIFYHOST => 0,
              CURLOPT_SSL_VERIFYPEER => 0,
              CURLOPT_HTTPHEADER => array(
              "content-type: application/x-www-form-urlencoded"
              ),
            ));
            $response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);
            $response_data = json_decode($response);
            
            if ($err) {
              $this->error = "SFND";  
            } else {
              if(isset($response_data->type) == 'success' ){
                $response = true;
                $this->error = "SFD";               
              }else{
                $response = $response_data->message;
                $this->error = "SFND";  
              }
            }
          
          }
          
          
        }else{
          $this->error = "NRF";
        }       
      }else{
        $this->error = "PIM";
        return $this->responseAction();       
      }
      
      if(empty($response)) {
        $response = false;
        $this->error = "NRF";
        $this->error_msg = "No Record Found";
      }
      $this->data = $response;
      return $this->responseAction();
      
    /*} catch(\Exception $e) {
      
      echo $e->getMessage();exit;
      
      $this->error = "SFND" ;
      $this->data = false ;
      return $this->responseAction() ;
        }*/
    }
 
  
    /**
     * @Route("/ws/changeforgetpassword/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function changeforgetpasswordAction(Request $request) {
        //try{
        $this->title = "change forget password";
        $param = $this->requestAction($request, 0);
        $em = $this->getDoctrine()->getManager();
        $this->validateRule = array(
            array(
                'rule' => 'NOTNULL',
                'field' => array('mobile_no',"password"),
            ),
        );
            if ($this->validateData($param)) {
               
                    $mobile_no = $param->mobile_no;
                    $password = $param->password;
                
                    
                        $usermaster = $em->getRepository("AdminBundle:usermaster")
                                ->findOneBy(array(
                                        "user_mobile"=>$mobile_no,
                                        
                                    )
                                );
                               
                               
                        if(!empty($usermaster)){
                            $usermaster->setUser_password(md5($password));
                            $em->flush();
                            $this->error = "SFD";
                            $response = true;
                        }else{
                            $this->error = "UNF";
                        }
                  
                   
            }else{
                 $this->error = "PIM";
            }         
                    
                    if (empty($response)) {
                        $response = false;
                    }
                    $this->data = $response;
                    return $this->responseAction();
    }
  /*
   * form for rest the password
   * Dev Name : Naman Joshi
   */
  /**
	* @Route("/Password-Reset/{hash}/{email}",defaults={"hash"="","email"=""})
	* @Template()
	*/
	public function passwordresetviewAction($hash,$email)
	{
    $em = $this->getDoctrine()->getManager();
		if(isset($_REQUEST['status']) && !empty($_REQUEST['status']))
		{
			return array("done"=>$_REQUEST['status']);
		}
		if(isset($hash) && !empty($hash) && isset($email) && !empty($email))
		{
			$hash = str_split($hash,32);
			$time = $hash[1];
			$hashemail = $hash[0];
			if((md5($email) == $hashemail) && (intval($time+(30*60)) > time()))
			{
         
       $user = $em->getRepository("AdminBundle:usermaster")
                            ->findOneBy(array(
                                    "user_email_password"=>$email,
                                )
                            );
/*
        $user = $this->getDoctrine()
              ->getManager()
              ->getRepository("MainAdminBundle:Usermaster")
              ->findOneBy(array("is_deleted"=>'0',"email"=>$email,"status"=>'active'));
*/
				if(!empty($user))
				{
					$msg = "1"; // user found
				}
				else
				{
					$msg = "2"; // user not found
				}
			}
			else
			{
				$msg = "3"; // link expired
			}
      $user_type = '';
			return array("email"=>$email,"msg"=>$msg,'user_type'=>$user_type);
		}
		return array();
	}
  /*
   * form submit mathod for rest the password
   * Dev Name : Naman Joshi
   */
  /**
	* @Route("/Password-Reset-Form")
	*/
	public function passwordresetform(){
     $em = $this->getDoctrine()->getManager();
		if(isset($_POST['change_password']))
		{
      $email = $_POST['email'];
      
      $user = $em->getRepository("AdminBundle:usermaster")
                            ->findOneBy(array(
                                    "user_email_password"=>$email,
                                )
                            );
      /*
      $user = $this->getDoctrine()
            ->getManager()
            ->getRepository("MainAdminBundle:Usermaster")
            ->findOneBy(array("is_deleted"=>'0',"email"=>$email,"status"=>'active'));
*/
			$msg = "2";
			if(!empty($user))
			{
        $user2 = $em->getRepository("AdminBundle:usermaster")
                            ->findOneBy(array(
                                    "user_email_password"=>$email,
                                )
                            );
        if(!empty($user2))
        {
          $user2->setUser_password(md5($_POST['new_password']));
          $em->flush();
          $msg = "1";
        }
        else{
          exit;
        }
			}
			return $this->redirect($this
						->generateUrl("ws_wsforgotpassword_passwordresetview")."?status=".$msg);
		}
		exit;
	}
}
?>