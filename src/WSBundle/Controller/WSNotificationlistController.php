<?php
namespace WSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AdminBundle\Controller\CouponController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use AdminBundle\Entity\Userinfo ;
use AdminBundle\Entity\Rolemaster ;
use AdminBundle\Entity\Gcmuser ;
use AdminBundle\Entity\Apnsuser ;
use AdminBundle\Entity\generalnotification;

class WSNotificationlistController extends WSBaseController
{
/**
     * @Route("/ws/notificationlist/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function notificationlistAction(Request $request){
        //try{
            $this->title = "notification list" ;
            $param = $this->requestAction($request, 0);
            // use to validate required param validation
            $em = $this->getDoctrine()->getManager();
            $this->validateRule = array(
                array(
                    'rule'=>'NOTNULL',
                    'field'=>array('user_id'),
                ),
            );
            if($this->validateData($param)){
            	$user_id = $param->user_id;
            	$datarray = null;
            	$generalnotification = $em->getRepository("AdminBundle:generalnotification")
                                ->findBy(array(
                                        "is_deleted"=>"0"
                                    )
                                );
                       
                 if(!empty($generalnotification)){    

                 foreach ($generalnotification as $key => $value) {
                    $user_master_id = explode(",", $value->getUsermasterid());
                     if(in_array($user_id,$user_master_id )){
                      $live_path  = $this->container->getParameter('live_path');
                $em         = $this->getDoctrine()->getManager();
                $media_info = $em->getRepository("AdminBundle:mediamaster")->findOneBy(array(
                    "id" => $value->getImageId()
                ));
                
                if (!empty($media_info)) {
                    $image_url = $live_path . $media_info->getMediaLocation() . "/" . $media_info->getMediaName();
                } else {
                    $image_url = null;
                }         
                if(empty($image_url)){
                  $notification_type = "text";
                }else{
                  $notification_type = "image";
                }
                          $datarray[] =array(
                            "notification_id"=>$value->getId(),
                            "notification_type"=>$notification_type,
                            "title"=>$value->getTitle(),
                            "message"=>$value->getMessage(),
                            "image_url"=>$image_url,
                            "send_to"=>$value->getSendTo(),
                            "create_date"=>strtotime($value->getCreatedate()),


                           );   
                       
                     }     
                  } 
                   $response =    $datarray;
                    $this->error = "SFD";
                    if($datarray == null){
                      $this->error = "NRF";
                    }
                }else{
                   $this->error = "NRF";
                }             
            } else {
                $this->error = "PIM";
            }
            if (empty($response)) {
                $response = false;
            }
            $this->data = $response;
            return $this->responseAction();
        }

    /**
     * @Route("/ws/pushnotification/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function pushnotificationAction(Request $request){
         $this->title = "notification list" ;
            $param = $this->requestAction($request, 0);
            // use to validate required param validation
            $em = $this->getDoctrine()->getManager();
            $this->validateRule = array(
                array(
                    'rule'=>'NOTNULL',
                    'field'=>array('note_title','note_message'),
                ),
            );
            if($this->validateData($param)){
             
               $note_title = $param->note_title;
               $note_message = $param->note_message;
               $send_to = 'customer';
                 $media_id = $media_id1 = 0;
                  $upload_dir = "";
                  
                  if(isset($_FILES['image']) && !empty($_FILES['image'])){
                      $category_logo=$_FILES['image'];
                    $logo = $category_logo['name'];
                    $media_type_id = 1;
                        $tmpname =$category_logo['tmp_name'];
                        
                        $file_path = $this->container->getParameter('file_path');
                
                        $logo_path = $file_path.'/notification';
    
                        $logo_upload_dir = $this->container->getParameter('upload_dir').'/notification/';
                    
                        $media_id = $this->mediauploadAction($logo,$tmpname,$logo_path,$logo_upload_dir,$media_type_id); 
                     // var_dump($media_id);exit;
                  }
                  if(isset($_FILES['body_report_image']) && !empty($_FILES['body_report_image'])){
                       $category_logo=$_FILES['body_report_image'];
                   ;
                  
                    $path = "user_report";
                    $logo = $category_logo['name'];
                    $media_type_id = 1;
                        $tmpname =$category_logo['tmp_name'];
                        
                        $file_path = $this->container->getParameter('file_path');
                
                        $logo_path = $file_path.'/notification';
    
                        $logo_upload_dir = $this->container->getParameter('upload_dir').'/notification/';
                    $media_id = $this->mediauploadAction($logo,$tmpname,$logo_path,$logo_upload_dir,$media_type_id); 
                  
                  }
               $general_notification = new generalnotification();
                $general_notification->setNotificationType("");
                $general_notification->setTitle( $note_title );
                $general_notification->setMessage($note_message);
                if (!empty($_FILES['image'])) {
                    if ($media_id != "FALSE") {
                        $general_notification->setImageId($media_id);
                    }
                }
                if (isset(  $param->user_id ) && !empty(  $param->user_id )) {
                    $user_id = $param->user_id;
                   
                }
                else{
                     $user_id =0;
                }

                $general_notification->setUserMasterId($user_id);
                $general_notification->setSendTo($send_to);

                $general_notification->setCreateDate(date("Y-m-d H:i:s"));
                $general_notification->setIs_deleted(0);
                $em = $this->getDoctrine()->getManager();

                $em->persist($general_notification);
                $em->flush();
                $response =    true;
                    $this->error = "SFD";

            
            } else {
                $this->error = "PIM";
            }
            if (empty($response)) {
                $response = false;
            }
            $this->data = $response;
            return $this->responseAction();

    }

}