<?php

namespace WSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AdminBundle\Controller\CouponController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use AdminBundle\Entity\Userinfo;
use AdminBundle\Entity\Role_master;
use AdminBundle\Entity\Gcmuser;
use AdminBundle\Entity\Apnsuser;
use AdminBundle\Entity\users;
use AdminBundle\Entity\fideuser;

class WSUserCouponController extends WSBaseController {

    /**
     * @Route("/ws/usercoupon/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function usercouponAction(Request $request) {
        //try{
        $this->title = "User Coupons";
        $param = $this->requestAction($request, 0);
        // use to validate required param validation
        $em = $this->getDoctrine()->getManager();
        $this->validateRule = array(
            array(
                'rule' => 'NOTNULL',
                'field' => array(
                    'user_id'
                )
            )
        );

        if ($this->validateData($param)) {
            $coupon_array = [] ;
            $user_id = $param->user_id;
            $userMaster = $em->getRepository("AdminBundle:usermaster")->findOneBy(array(
                "id" => $user_id
            ));
            $user_coupon_assigned_list = $em->getRepository('AdminBundle:coupon_assigned_to_user')->findBy(array(
                                                        "user_id"=>$user_id,
                                                        "is_deleted"=>"0"
                                                    ));

            if($user_coupon_assigned_list){
                foreach($user_coupon_assigned_list as $uskey=>$usval){
                    // get Coupon Details 
                    $coupon_info = $em->getRepository('AdminBundle:Couponmaster')->findOneBy(array("id"=>$usval->getCoupon_id()));
                   

                    //$usedcoupan = sizeof( $user_coupon_assigned_list);
                    if($coupon_info){
                        $coupon_array[] = array(
                            "coupon_id"=>$coupon_info->getId(),
                            "coupon_code"=>$coupon_info->getCoupon_code(),
                            "coupon_name"=>$coupon_info->getCoupon_name(),
                            "start_date"=>$coupon_info->getStart_date(),
                            "end_date"=>$coupon_info->getEnd_date(),
                            "discount_value"=>$coupon_info->getDiscount_value(),
                            "discount_type"=>$coupon_info->getDiscount_type(),
                            "return_type"=>$coupon_info->getReturn_type(),
                            "no_of_user_use"=>$coupon_info->getNo_of_user_use(),
                            "no_of_times_use"=>$coupon_info->getNo_of_times_use(),
                            "coupon_usage_interval"=>$coupon_info->getCoupon_usage_interval(),
                            //"number_time_used"=>$usedcoupan
                                );
                    }
                }
                if(!empty($coupon_array)){
                    $this->error = "SFD";
                    $response = $coupon_array ;
                }
                else{
                    $this->error = "NRF";
                    $response = false ;
                }
            }
            else{
                $this->error = "NRF";
            }
          
        } else {
            $this->error = "PIM";
        }
        if (empty($response)) {
            $response = false;
        }
        $this->data = $response;
        return $this->responseAction();
    }

}
