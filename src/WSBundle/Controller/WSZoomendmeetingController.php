<?php

namespace WSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AdminBundle\Controller\CouponController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use AdminBundle\Entity\Userinfo;
use AdminBundle\Entity\Rolemaster;
use AdminBundle\Entity\Gcmuser;
use AdminBundle\Entity\Apnsuser;
use AdminBundle\Entity\users;

class WSZoomendmeetingController extends WSBaseController {

    /**
     * @Route("/ws/endzoommeeting/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function endzoommeetingAction(Request $request) {

        $this->title = "End Zoom Meeting";
        $param = $this->requestAction($request, 0);

        $this->validateRule = array(
            array(
                'rule' => 'NOTNULL',
                'field' => array('meeting_id', 'course_unit_id'),
            ),
        );

        if ($this->validateData($param)) {
            $course_unit_id = $param->course_unit_id;
            $meeting_id = $param->meeting_id;
            $flag = '';
            $em = $this->getDoctrine()->getManager();
            $emREpo = $this->getDoctrine()->getManager()->getRepository('AdminBundle:course_unit');
            $courseunitInfo = $emREpo->findOneBy(array('is_deleted' => "0", "id" => $course_unit_id, 'unit_code' => $meeting_id, 'prerequisites' => 'zoom'));
            if ($courseunitInfo) {

                // update Meeting Status 
                // check meeting have Recordings or not 
                $calledFunction = 'meetings/' . $meeting_id . '/recordings';
                $request_url = $this->api_url . $calledFunction . '?access_token=' . $this->access_token;
                $headers = array();
                // $headers[] = 'Accept: application/xml';
                $headers[] = 'authorization: Bearer ' . $this->access_token;

                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_URL => $request_url,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "GET",
                    CURLOPT_HTTPHEADER => $headers
                ));

                $responseFetchedData = curl_exec($curl);
                $err = curl_error($curl);

                curl_close($curl);

                if ($err) {
                    $this->error_msg = $err;
                    $this->error = "SFND";
                } else {
                    if ($responseFetchedData) {
                        //var_dump($response);exit;
                        if (isset($responseFetchedData->code) && ($responseFetchedData->code == 3301 || $responseFetchedData->code == 404)) {
                            $this->error_msg = $response->message;
                            //$this->error = "NRF";
                            
                            $courseunitInfo->setYoutubelink('');
                            $courseunitInfo->setCourseType("0"); // Recorded 
                            $tags = $courseunitInfo->getTag_id();
                            if($tags == ""){
                                $tags = "5ca9fb219bc8b76ede5b7933"; // Live Tag id
                            }
                            else{
                                $tags = $tags . "," ."5ca9fb219bc8b76ede5b7933"; // Live Tag id
                            }
                            $courseunitInfo->setTag_id($tags);
                            $em->persist($courseunitInfo);
                            $em->flush();
                            $response = true ;
                            
                            $this->error = "MRNF";
                            $this->error_msg = "Meeting Record not Found , but Link updated ";
                        } else {
                            $meeting_Password = '123456';
                            $headersmeetingCloud = array();
                            $headersmeetingCloud[] = 'content-type: application/json';
                            $headersmeetingCloud[] = 'authorization: Bearer ' . $this->access_token;

                            //----------------retrive meeting Password : -----------------
                            $curl = curl_init();
                            $REtriveMeeting = 'meetings/' . $meeting_id ;
                            $REtriveMeetingURL = $this->api_url . $REtriveMeeting;
                            
                            curl_setopt_array($curl, array(
                              CURLOPT_URL => $REtriveMeetingURL,
                              CURLOPT_RETURNTRANSFER => true,
                              CURLOPT_ENCODING => "",
                              CURLOPT_MAXREDIRS => 10,
                              CURLOPT_TIMEOUT => 30,
                              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                              CURLOPT_CUSTOMREQUEST => "GET",
                              CURLOPT_HTTPHEADER => $headersmeetingCloud
                            ));

                            $RetriveMeetingResponse = curl_exec($curl);
                            $RetriveMettingErr = curl_error($curl);

                            curl_close($curl);

                            if ($err) {
                            //  echo "cURL Error #:" . $RetriveMettingErr;
                            } else {
                              if($RetriveMeetingResponse){
                                  if(isset($RetriveMeetingResponse->password)){
                                      $meeting_Password = $RetriveMeetingResponse->password ;
                                  }
                              }
                            }
                            //----------------------------------
                            $curl = curl_init();
                            $calledFunction_meeting_cloud = 'meetings/' . $meeting_id . '/recordings/settings';
                            $request_url_meetingCloud = $this->api_url . $calledFunction_meeting_cloud . '?access_token=' . $this->access_token;
                            
                            $dataMeetingcloud = array(
                                'approval_type' => 0,
                                'on_demand' => false, // scheduled meetings
                                'password' => $meeting_Password,
                                'send_email_to_host' => true,
                                "share_recording" => "Asia/Kolkata",
                                "show_social_share_buttons" => 'publicly',
                                "viewer_download" => false
                            );
                            curl_setopt_array($curl, array(
                                CURLOPT_URL => $request_url_meetingCloud,
                                CURLOPT_RETURNTRANSFER => true,
                                CURLOPT_ENCODING => "",
                                CURLOPT_MAXREDIRS => 10,
                                CURLOPT_TIMEOUT => 30,
                                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                CURLOPT_CUSTOMREQUEST => "PATCH",
                                CURLOPT_POSTFIELDS => json_encode($dataMeetingcloud),
                                CURLOPT_HTTPHEADER => $headersmeetingCloud
                            ));
                            $responseMeetingCloud = curl_exec($curl);
                            $errMeetingCloud = curl_error($curl);
                            curl_close($curl);

                            if ($errMeetingCloud) {
                                $this->error_msg = $errMeetingCloud;
                                $this->error = "SFND";
                            } else {
                                //  echo $response;
                                //get metting Cloud Data 
                                $meetingURL = '';
                                $responseFetchedData = json_decode($responseFetchedData);
                                if (isset($responseFetchedData->recording_files[0]->play_url)) {
                                    $meetingURL = $responseFetchedData->recording_files[0]->play_url;
                                } elseif (isset($responseFetchedData->share_url)) {
                                    $meetingURL = $responseFetchedData->share_url;
                                }

                                // now update Course unit Status 
                                $courseunitInfo->setYoutubelink($meetingURL);
                                $courseunitInfo->setCourseType("0"); // Recorded 
                                $tags = $courseunitInfo->getTag_id();
                                if($tags == ""){
                                    $tags = "5ca9fb219bc8b76ede5b7933"; // Live Tag id
                                }
                                else{
                                    $tags = $tags . "," ."5ca9fb219bc8b76ede5b7933"; // Live Tag id
                                }
                                $courseunitInfo->setTag_id($tags);
                                $em->persist($courseunitInfo);
                                $em->flush();
                                $response = true ;
                                $this->error = "SFD";
                                $this->error_msg = "Link updated in YoutubeLink Field";
                            }
                        }
                    }
                }
            } else {
                $this->error = "NRF";
            }
        } else {
            $this->error = "PIM";
        }

        if (empty($response)) {
            $response = false;
            $this->error = "NRF";
        }

        $this->data = $response;
        return $this->responseAction();
    }

}

?>
