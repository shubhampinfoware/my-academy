<?php
namespace WSBundle\Controller;

use Symfony\Component\HttpFoundation\Request;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use AdminBundle\Entity\Reviewsmaster;

class WSReviewController extends WSBaseController{
    /**
     * @Route("/ws/addReview/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function addReviewAction($param, Request $request){
		try{
			$response = false;
			$faq_arr = [];
			$this->title = "Add Review";
			//$param = $this->requestAction($this->getRequest());
			$param = $this->requestAction($request,0);
			$this->validateRule = array(
				array(
					'rule'=>'NOTNULL',
					'field'=>array(
						'user_id',
						'review_type',
						'relation_id',
						'review_text',
						'domain_id'
					),
				),
			);
            		
			if($this->validateData($param)){

				$domain_id = $param->domain_id;
				$user_id = $param->user_id;
				$review_type = $param->review_type;
				$relation_id = $param->relation_id;
				$review_text = $param->review_text;
				$ratings = isset($param->ratings) ? $param->ratings : 3;
				
				$entity = $this->getDoctrine()->getManager();

				$review = $entity->getRepository("AdminBundle:Reviewsmaster")->findOneBy([
					'domain_id' => $domain_id,
					'created_by' => $user_id,
					'review_type' => $review_type,
					'relation_id' => $relation_id,
				]);

				if(!empty($review)){
					$this->error = "ARE";
					$this->error_msg = "Already Exists";
					$this->data = [];
					return $this->responseAction();
				}

				// add review
				$reviews_master = new Reviewsmaster();
				$reviews_master->setReview_text($review_text);
				$reviews_master->setReview_type($review_type);
				$reviews_master->setRelation_id($relation_id);
				$reviews_master->setIs_testimonial('inactive');
				$reviews_master->setRatings($ratings);
				$reviews_master->setCreated_by($user_id);
				$reviews_master->setCreated_datetime(date('Y-m-d H:i:s'));
				$reviews_master->setDomain_id($domain_id);
				$reviews_master->setIs_deleted(0);

				$entity->persist($reviews_master);
				$entity->flush();

				$this->error = "SFD";
				$this->error_msg = "Review Added";
				$response = array(
					'reviews_master_id' => $reviews_master->getReviews_master_id()
				);
			}else{
				$this->error = "PIM";
			}
			if(empty($response)){
				$response = null;
			}
			$this->data = $response;
			return $this->responseAction();
		}
		catch(\Exception $e){

			// echo $e->getMessage();exit;

			$this->error = "SFND";
			$this->data = false;
			return $this->responseAction();
		}
	}

	/**
     * @Route("/ws/getReviewList/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function getReviewListAction($param, Request $request){
		try{
			$response = false;
			$this->title = "Review List";
			$param = $this->requestAction($request,0);
			$this->validateRule = array(
				array(
					'rule'=>'NOTNULL',
					'field'=>array(
						'domain_id'
					),
				),
			);
            		
			if($this->validateData($param)){

				$domain_id = $param->domain_id;
				$review_type = isset($param->review_type) ? $param->review_type : ''; // course, trainer

				$testimonial_clause = '';
				$is_testimonial = isset($param->is_testimonial) ? $param->is_testimonial : '';
				if($is_testimonial != '' && $is_testimonial == "true"){
					$testimonial_clause = ' and lower(is_testimonial) = "active" ';
				}

				$course_join = '';
				$course_clause = '';

				$review_clause = '';
				if($review_type != ''){
					$review_clause = " and r.review_type = '{$review_type}' ";
				}

				if($review_type == 'course'){
					$course_join = ', coursemaster c ';
					$course_clause = ' and c.is_deleted = 0 and lower(c.course_status) = "active" and c.main_coursemaster_id = r.relation_id ';

					if(isset($param->course_id)){
						$course_id = $param->course_id;
						$course_clause .= " and r.relation_id = {$course_id}";
					}
				}
				
				$_sql = "SELECT * from reviews_master r, usermaster u {$course_join} where r.created_by = u.usermaster_id {$course_clause} {$review_clause} and r.domain_id = {$domain_id} and lower(u.user_status) = 'active' and r.is_deleted = 0 and u.is_deleted = 0 {$course_clause} {$testimonial_clause}";
				$review_list = $this->firequery($_sql);

				if(!empty($review_list)){
					foreach($review_list as $_review){

						$course_info = [];
						$trainer_info = [];

						if($_review['review_type'] == 'course'){
							$sql = "SELECT * from coursemaster where is_deleted = 0 and lower(course_status) = 'active' and main_coursemaster_id = {$_review['relation_id']} ";
							$course = $this->firequery($sql);

							if(!empty($course)){
								$course = $course[0];
								$course_info = array(
									'main_course_id' => $course['main_coursemaster_id'],
									'course_name' => $course['course_name'],
									'course_description' => $course['course_description'],
									'course_price' => $course['course_price'],
									'course_image_url' => $this->getimage($course['course_image'])
								);
							}
						} else if($_review['review_type'] == 'trainer'){
							$sql = "SELECT * from usermaster where is_deleted = 0 and domain_id = {$domain_id} and lower(user_status) = 'active' and usermaster_id = {$_review['relation_id']}";
							$trainer = $this->firequery($sql);

							if(!empty($trainer)){
								$trainer = $trainer[0];
								$trainer_info = array(
									'id' => $trainer['usermaster_id'],
									'firstname' => $trainer['user_first_name'],
									'lastname' => $trainer['user_last_name'],
									'email' => $trainer['user_email_password'],
									'user_mobile' => $trainer['user_mobile'],
									'age' => $trainer['age'],
									'user_bio' => $trainer['user_bio'],
									'achievements' => $trainer['achievements'],
									'image_url' => $this->getimage($trainer['media_id'])
								);
							}
						}

						$created_by = array(
							'user_id' => $_review['usermaster_id'],
							'firstname' => $_review['user_first_name'],
							'lastname' => $_review['user_last_name'],
							'email' => $_review['user_email_password'],
							'user_mobile' => $_review['user_mobile'],
							'image_url' => $this->getimage($_review['media_id'])
						);

						$response[] = array(
							'reviews_master_id' => $_review['reviews_master_id'],
							'review_text' => $_review['review_text'],
							'review_type' => $_review['review_type'],
							'ratings' => $_review['ratings'],
							'datetime' => $_review['created_datetime'],
							'created_by' => $created_by,
							'course_info' => $course_info,
							'trainer_info' => $trainer_info
						);
						$this->error = "SFD";
					}
				}

			}else{
				$this->error = "PIM";
			}
			if(empty($response)){
				$response = null;
				$this->error = "NRF";
			}
			$this->data = $response;
			return $this->responseAction();
		}
		catch(\Exception $e){
			$this->error = "SFND".$e;
			$this->data = false;
			return $this->responseAction();
		}
	}
}
?>