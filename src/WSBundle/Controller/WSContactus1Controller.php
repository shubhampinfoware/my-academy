<?php

namespace WSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AdminBundle\Controller\CouponController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use AdminBundle\Entity\Contactus;

class WSContactus1Controller extends WSBaseController {

    /**
     * @Route("/ws/addContactus/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function addContactusAction(Request $request) {

        $this->title = "Contact Us";
        $param = $this->requestAction($request, 0);
        $this->validateRule = array(
            array(
                'rule' => 'NOTNULL',
                'field' => array('email', 'message', 'contact_no', 'domain_id'),
            ),
        );

        if ($this->validateData($param)) {
            $user_id = isset($param->user_id) ? $param->user_id : 0;
            $title = isset($param->title) ? $param->title : '';
            $email = $param->email;
            $contact_no = $param->contact_no;
            $message = $param->message;
            $domain_id = $param->domain_id;

            $chkExists = $this->getDoctrine()->getRepository("AdminBundle:Contactus")->findOneBy([
                'email' => $email,
                'domain_id' => $domain_id
            ]);

            if(!empty($chkExists)){
                $this->error = "ARE";
                $this->error_msg = "Email Already Exists";
                $this->data = null;
                return $this->responseAction();
            }

            $contactus = new Contactus();
            $contactus->setUserid($user_id);
            $contactus->setTitle($title);
            $contactus->setEmail($email);
            $contactus->setContact_no($contact_no);
            $contactus->setMessage($message);
            $contactus->setDomain_id($domain_id);
            $contactus->setIs_deleted(0);

            $em = $this->getDoctrine()->getManager();
            $em->persist($contactus);
            $em->flush();

            $response = array(
                "contact_us_id" => $contactus->getContactus_id(),
            );

            $this->error = "SFD";
        } else {
            $this->error = "PIM";
        }
        if (empty($response)) {
            $response = False;
        }
        $this->data = $response;
        return $this->responseAction();
    }
}