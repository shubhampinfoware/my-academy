<?php

namespace WSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AdminBundle\Controller\CouponController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use AdminBundle\Entity\Userinfo;
use AdminBundle\Entity\Role_master;
use AdminBundle\Entity\Gcmuser;
use AdminBundle\Entity\Apnsuser;
use AdminBundle\Entity\users;
use AdminBundle\Entity\fideuser;

class WSUserEditController extends WSBaseController {

    /**
     * @Route("/ws/useredit/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function usereditAction(Request $request) {
        //try{
        $this->title = "User registration";
        $param = $this->requestAction($request, 0);
        // use to validate required param validation
        $em = $this->getDoctrine()->getManager();
        $this->validateRule = array(
            array(
                'rule' => 'NOTNULL',
                'field' => array(
                    'user_id'
                )
            )
        );

        if ($this->validateData($param)) {
            $user_id = $param->user_id;
            $userMaster = $em->getRepository("AdminBundle:Usermaster")->findOneBy(array(
                "usermaster_id" => $user_id
            ));

            $fideuser = $em->getRepository("AdminBundle:Fideuser")->findOneBy(
                array(
                    "user_id" => $user_id,
                )
            );
            if (isset($_FILES['user_image']) && !empty($_FILES['user_image'])) {
                $category_logo = $_FILES['user_image'];
                $logo = $category_logo['name'];
                $media_type_id = 1;
                $tmpname = $category_logo['tmp_name'];

                $file_path = $this->container->getParameter('file_path');

                $logo_path = $file_path . '/user';

                $logo_upload_dir = $this->container->getParameter('upload_dir') . '/user/';

                $media_id = $this->mediauploadAction($logo, $tmpname, $logo_path, $logo_upload_dir, $media_type_id);
            }

            if (!empty($media_id) && $media_id != 0) {
                $userMaster->setMedia_id($media_id);
            }

            if (isset($param->email_id)) {
                $userMaster->setUser_email_password($param->email_id);
            }
            if (isset($param->password)) {
                $password = $param->password;
                if ($password != "") {
                    $userMaster->setUser_password(md5($password));
                }
            }
            if (isset($param->firstname)) {
                $userMaster->setUser_first_name($param->firstname);
            }

            if (isset($param->lastname)) {
                $userMaster->setUser_last_name($param->lastname);
            }
            if (isset($param->country_id)) {
                $country = $param->country_id . "," . $param->country;

                $userMaster->setDomain_id($country);
                $em->flush();
            }
            if (isset($param->date_of_birth)) {
                $timestamp = $param->date_of_birth;
                $datetimeFormat = 'Y-m-d H:i:s';
                $date = new \DateTime();
                $date = new \DateTime('now', new \DateTimeZone('Asia/Kolkata'));
                $date->setTimestamp($timestamp);
                $date_of_birth = $date->format($datetimeFormat);
                $userMaster->setBirthdate($date_of_birth);
            }
            if (isset($param->mobile_no)) {
                $userMaster->setUser_mobile($param->mobile_no);
            }
            if (isset($param->fide)) {
                $userMaster->setExt_field1($param->fide);
            }
            if (isset($param->fide_id)) {
                if (!empty($fideuser)) {
                    $fideuser->setFide_id($param->fide_id);
                }
            }
            if (isset($param->role_id)) {
                $userMaster->setUser_role_id($param->role_id);
            }
            $em->flush();

            $live_path = $this->container->getParameter('live_path');
            $em = $this->getDoctrine()->getManager();

            $media_info = $em->getRepository("AdminBundle:Mediamaster")->findOneBy(array(
                "mediamaster_id" => $userMaster->getMedia_id()
            ));

            if (!empty($media_info)) {
                $image_url = $live_path . $media_info->getMedia_location() . "/" . $media_info->getMedia_name();
            } else {
                $image_url = null;
            }

            $fide = $userMaster->getExt_field1();
            if (!empty($fideuser)) {
                $fide_id = $fideuser->getFide_id();
            } else {
                $fide_id = 0;
            }
            if (empty($fide) or $fide == "") {
                $fide = "0000";
            }
            $wallet_details = NULL ;
            $wallet_master = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Walletmaster')->findOneBy(array("user_master_id"=>$userMaster->getUsermaster_id()));
            
            if($wallet_master){
                 $wallet_details = array(
                    "virtual_money"=>$wallet_master->getVirtual_money_balance(),
                    "actual_money"=>$wallet_master->getActual_money_balance(),
                    "wallet_points"=>$wallet_master->getWallet_points(),
                    "cashback"=>$wallet_master->getCashback_balance()
                );
            }

            $response = array(
                "user_id" => $userMaster->getUsermaster_id(),
                "email" => $userMaster->getUser_email_password(),
                "firstname" => $userMaster->getUser_first_name(),
                "lastname" => $userMaster->getUser_last_name(),
                "mobile_no" => $userMaster->getUser_mobile(),
                "img" => $this->getimage($userMaster->getMedia_id()),
                "user_image" => $image_url,
                "user_wallet"=>$wallet_details
            );
            $this->error = "SFD";
        } else {
            $this->error = "PIM";
        }
        if (empty($response)) {
            $response = false;
        }
        $this->data = $response;
        return $this->responseAction();
    }

}
