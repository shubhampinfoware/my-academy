<?php

namespace WSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AdminBundle\Entity\Usermaster;

class WSOtpManageController extends WSBaseController {

    /**
     * @Route("/ws/otpManage/{param}",defaults ={"param"=""},requirements={"param"=".+"})
     *
     */
    public function otpManageAction($param, Request $request) {

        $entity = $this->getDoctrine()->getManager();
        // begin transaction and suspend auto commit

        $con = $entity->getConnection();

        try {
            $this->title = "Manage Otp";
            $param = $this->requestAction($request, 0);

            $this->validateRule = array(
                array(
                    'rule' => 'NOTNULL',
                    'field' => array('mobile_no', 'operation')
                ),
            );

            if ($this->validateData($param)) {

                $operation = !empty($param->operation) ? $param->operation : 'send';
                $mobile_no = !empty($param->mobile_no) ? $param->mobile_no : 0;
                $user_id = !empty($param->user_id) ? $param->user_id : 0;
                $otp = !empty($param->otp) ? $param->otp : 0;

                $em = $this->getDoctrine()->getManager();
                $user_details = null;
                $user_details = $em->getRepository("AdminBundle:usermaster")->find($user_id);
                if ($operation == 'send_again' || $operation == 'verify_again') {
                    /* 					
                      $sql = "UPDATE user_master SET email_verified = '1' where user_master_id = '$user_id'";
                     */
                    
                    if ($user_details) {
                        $mobile_no = $this->keyDecryptionAction($user_details->getUser_mobile());
                       // $mobile_no = $user_details->getUser_mobile();
                    } else {
                        $this->error = "UNE";

                        return $this->responseAction();
                    }
                }

                $auth_key = '249885AXRjvpYf3m5c023864';
                $message = urlencode("Welcome to Chesscoaching. Your OTP verification code is ##OTP##.");
                $sender = 'Gchess';


                if (!empty($mobile_no)) {
                    if ($operation == 'send' || $operation == 'send_again') {
                        $curl = curl_init();
                        curl_setopt_array($curl, array(
                            CURLOPT_URL => "http://control.msg91.com/api/sendotp.php?authkey=$auth_key&message=$message&sender=$sender&mobile=$mobile_no",
                            CURLOPT_RETURNTRANSFER => true,
                            CURLOPT_ENCODING => "",
                            CURLOPT_MAXREDIRS => 10,
                            CURLOPT_TIMEOUT => 30,
                            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                            CURLOPT_CUSTOMREQUEST => "POST",
                            CURLOPT_POSTFIELDS => "",
                            CURLOPT_SSL_VERIFYHOST => 0,
                            CURLOPT_SSL_VERIFYPEER => 0,
                        ));
                        $response = curl_exec($curl);
                        $err = curl_error($curl);
                        curl_close($curl);

                        $response_data = json_decode($response);


                        if ($err) {
                            $this->error = "SFND";
                        } else {
                            if (isset($response_data->type) && $response_data->type == 'success') {
                                $response = true;
                                $this->error = "SFD";
                            } else {
                                $this->error = "SFND";
                            }
                        }
                    }

                    if ($operation == 'verify' || $operation == 'verify_again') {

                        $curl = curl_init();
                        curl_setopt_array($curl, array(
                            CURLOPT_URL => "https://control.msg91.com/api/verifyRequestOTP.php?authkey=$auth_key&mobile=$mobile_no&otp=$otp",
                            CURLOPT_RETURNTRANSFER => true,
                            CURLOPT_ENCODING => "",
                            CURLOPT_MAXREDIRS => 10,
                            CURLOPT_TIMEOUT => 30,
                            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                            CURLOPT_CUSTOMREQUEST => "POST",
                            CURLOPT_POSTFIELDS => "",
                            CURLOPT_SSL_VERIFYHOST => 0,
                            CURLOPT_SSL_VERIFYPEER => 0,
                            CURLOPT_HTTPHEADER => array(
                                "content-type: application/x-www-form-urlencoded"
                            ),
                        ));
                        $response = curl_exec($curl);
                        $err = curl_error($curl);
                        curl_close($curl);

                        $response_data = json_decode($response);

                        if ($response_data->message == "otp_not_verified") {
                            $this->error = "INO";
                            $this->data = $response;
                            return $this->responseAction();
                        }
                        if ($err) {
                            $this->error = "SFND";
                        } else {
                            //var_dump($response_data->type);exit;
                            if (isset($response_data->type) == 'success') {

                                if ($operation == 'verify' || $operation == 'verify_again') {

                                    if (!empty($user_details)) {

                                        $user_details->setExt_field("true");
                                       // $user_details->setEmail_verified(1);
                                        $em->flush();
                                    }
                                }

                                $response = true;
                                $this->error = "SFD";
                            } else {

                                $response = $response_data->message;
                                $this->error = "SFND";
                            }
                        }
                    }

                    if ($operation == 'resend') {

                        $curl = curl_init();
                        curl_setopt_array($curl, array(
                            CURLOPT_URL => "http://control.msg91.com/api/retryotp.php?authkey=$auth_key&mobile=$mobile_no",
                            CURLOPT_RETURNTRANSFER => true,
                            CURLOPT_ENCODING => "",
                            CURLOPT_MAXREDIRS => 10,
                            CURLOPT_TIMEOUT => 30,
                            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                            CURLOPT_CUSTOMREQUEST => "POST",
                            CURLOPT_POSTFIELDS => "",
                            CURLOPT_SSL_VERIFYHOST => 0,
                            CURLOPT_SSL_VERIFYPEER => 0,
                            CURLOPT_HTTPHEADER => array(
                                "content-type: application/x-www-form-urlencoded"
                            ),
                        ));
                        $response = curl_exec($curl);
                        $err = curl_error($curl);
                        curl_close($curl);
                        $response_data = json_decode($response);

                        if ($err) {
                            $this->error = "SFND";
                        } else {
                            if (isset($response_data->type) == 'success') {
                                $response = true;
                                $this->error = "SFD";
                            } else {
                                $response = $response_data->message;
                                $this->error = "SFND";
                            }
                        }
                    }
                } else {
                    $this->error = "NRF";
                }
            } else {
                $this->error = "PIM";
                return $this->responseAction();
            }

            if (empty($response)) {
                $response = false;
                $this->error = "NRF";
                $this->error_msg = "No Record Found";
            }
            $this->data = $response;
            return $this->responseAction();
        } catch (\Exception $e) {

            echo $e->getMessage();
            exit;

            $this->error = "SFND";
            $this->data = false;
            return $this->responseAction();
        }
    }

}

?>