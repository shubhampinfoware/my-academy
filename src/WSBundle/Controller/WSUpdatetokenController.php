<?php

namespace WSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use AdminBundle\Entity\Gcmuser;
use AdminBundle\Entity\Apnsuser;

class WSUpdatetokenController extends WSBaseController {

    /**
     * @Route("/ws/updateToken/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function updatetokenAction($param, Request $request) {

        $entity = $em = $this->getDoctrine()->getManager();

        try {
            $this->title = "Update Token";
            $param = $this->requestAction($request, 0);
            $this->validateRule = array(
                array(
                    'rule' => 'NOTNULL',
                    'field' => array(
                        'user_id', 'device_id', 'device_token', 'device_name'
                    ),
                ),
            );

            if ($this->validateData($param)) {

                $device_token = $param->device_token;
                $device_id = $param->device_id;
                $user_id = $param->user_id;
                $device_name = strtoupper($param->device_name);

                $user_name = '';
                $user_type = '';

				if($user_id != 0){
					$userMaster = $em->getRepository("AdminBundle:users")
												->findOneBy(array(
														"id"=>$user_id,
													)
												);
												
					if($userMaster){
						$user_name = $userMaster->getUsername();
					}								
				}	
                if ($device_name == 'ANDROID') {

                    $device_old = $entity->getRepository("AdminBundle:Gcmuser")
                            ->findBy(array('device_id' => $device_id, 'is_deleted' => "0"));

                    if ($device_old) {
                        foreach ($device_old as $device) {
                            $device->setIs_deleted("1");
                            $entity->flush();
                        }
                    }

					  $gcm_user_del = new Gcmuser();
					  $gcm_user_del->setGcmregid($device_token);
					  $gcm_user_del->setUser_id($user_id);
					  $gcm_user_del->setDeviceid($device_id);
					  $gcm_user_del->setAppid('CUST');
					  $gcm_user_del->setName($user_name);
					  
					  $gcm_user_del->setCreateddate(date('Y-m-d H:i:s'));
				  
					  $gcm_user_del->setIs_deleted("0");
					  $em->persist($gcm_user_del);
					  $em->flush();
					$response = true;
					$this->error = "SFD ";
                }
                if ($device_name == 'IOS' or $device_name == 'IPHONE') {

                    $device_old =   $entity->getRepository('AdminBundle:Apnsuser')
                            ->findBy(array('device_id' => $device_id, 'is_deleted' => 0));

                    if ($device_old) {
                        foreach ($device_old as $device) {
                            $device->setIs_deleted(1);
                            $em->flush();
                        }
                    }

                    $new_user = new Apnsuser();
                    $new_user->setApnsRegid($device_token);
                    $new_user->setUser_id($user_id);
                    $new_user->setUserType($user_type);
                    $new_user->setDeviceId($device_id);
                    $new_user->setAppId('CUST');
                
                    $new_user->setName($user_name);
                    $new_user->setBadge(0);
                    $new_user->setCreatedDate(date('y-m-d H:i:s'));
                    $new_user->setIs_deleted(0);

                    $em->persist($new_user);
                    $em->flush();
                        $response = true;
                    $this->error = "SFD ";
                }

            } else {
                $this->error = "PIM";
            }
            if (empty($response)) {
                $response = false;
            }

            $this->data = $response;
            return $this->responseAction();
        } catch (\Exception $e) {

            $this->error = "SFND" . $e->getMessage();
            $this->data = false;
            return $this->responseAction();
        }
    }

}

?>
