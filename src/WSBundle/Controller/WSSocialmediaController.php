<?php

namespace WSBundle\Controller;





use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use AdminBundle\Controller\CouponController;

use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\HttpFoundation\Request;

use AdminBundle\Entity\Userinfo ;

use AdminBundle\Entity\Rolemaster ;

use AdminBundle\Entity\Gcmuser ;

use AdminBundle\Entity\Apnsuser ;

use AdminBundle\Entity\users;

use AdminBundle\Entity\Contactus;

use \DOMDocument;

use \DOMXPath;



class WSSocialmediaController extends WSBaseController

{

    /**

     * @Route("/ws/socialmedia/{param}",defaults = {"param"=""},requirements={"param"=".+"})

     * @Template()

     */

    public function socialmediaAction(Request $request){



        $this->title = "Social Media";

        $param = $this->requestAction($request, 0);

         $em = $this->getDoctrine()->getManager();

        $this->validateRule = array(

            array(

                'rule' => 'NOTNULL',

                'field' => array(),

            ),

        );	

	

		if ($this->validateData($param)) {



			$twitter = '<a class="twitter-timeline" href="https://twitter.com/ChessbaseIndia?ref_src=twsrc%5Etfw">Tweets by ChessbaseIndia</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>';



			

			$response [] = array('social_type'=>'Twitter','data_type'=>'HTML','data'=>$twitter,'icon'=>'https://cdn1.iconfinder.com/data/icons/logotypes/32/twitter-512.png');

			

			/*

			$fb = '<div class="fb-page" data-href="https://www.facebook.com/chessbaseindia"data-width="700" data-tabs = "timeline" data-hide-cover="false" data-show-facepile="false"></div>';

					  

			$fb .= "<script>(function(d, s, id){var js, fjs = d.getElementsByTagName(s)[0];if (d.getElementById(id))return;js = d.createElement(s); js.id = id;js.src = 'https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v3.1&appId=349425628851564&autoLogAppEvents=1';fjs.parentNode.insertBefore(js, fjs);}(document, 'script', 'facebook-jssdk'));</script>";		*/  

			

//			return new Response($fb);



			$response [] = array('social_type'=>'Facebook','data_type'=>'url','data'=>'http://skillgamesacademy.com/chessbase/ws/fbPageLink','icon'=>'https://cdn2.iconfinder.com/data/icons/social-18/512/Facebook-512.png');

			

			$instagram = 'https://www.instagram.com/chessbaseindia/';	

			

			$response [] = array('social_type'=>'Instagram','data_type'=>'url','data'=>$instagram,'icon'=>'https://cdn2.iconfinder.com/data/icons/instagram-new/512/instagram-square-flat-3-512.png');

			

			

			

			

/*			$response = $twitter;

			$response = array('twitter'=>$twitter,'fb'=>$fb,'google_plus'=>$google_plus,'instagram'=>$instagram);	

			*/	

			$this->error = "SFD";

		

		}else{

			$this->error = "PIM";

		}

		

		if (empty($response)) {

            $response = false;

        }



        $this->data = $response;

        return $this->responseAction();

		



    }

	

	/**

     * @Route("/ws/onlineShoppingPage/{param}",defaults = {"param"=""},requirements={"param"=".+"})

     * @Template()

     */

    public function onlineShoppingPageAction(Request $request){



		$html = file_get_contents('https://chessbase.in/online-shop/'); //get the html returned from the following url



		$dom = new DomDocument();

		libxml_use_internal_errors(TRUE); 

		$dom->loadHTML($html);

		$classname = 'full';

		$finder = new DomXPath($dom);

		$nodes = $finder->query("//*[contains(concat(' ', normalize-space(@class), ' '), ' $classname ')]");

		$innerHTML = '';

		$tmp_dom = new DOMDocument(); 

		foreach ($nodes as $node) 

		{

			$tmp_dom->appendChild($tmp_dom->importNode($node,true));

		}

		$innerHTML.=trim($tmp_dom->saveHTML()); 

		echo $innerHTML;

		exit;

		

	

        $this->title = "Online Shopping Page";

        $param = $this->requestAction($request, 0);

        $this->validateRule = array(

            array(

                'rule' => 'NOTNULL',

                'field' => array(),

            ),

        );	

	

		if ($this->validateData($param)) {

			

			

			$html = '<iframe src="https://chessbase.in/online-shop/" height="800" width="500"></iframe>';

			

			$html .= '<script>var iframe = document.getElementById("myFrame");

				  var elmnt = iframe.contentWindow.document.getElementsByClassName("full")[0];

				  document.write(elmnt.innerHTML)

				  </script>';

		

			$response = $html;	

			$this->error = "SFD";

			

		}else{

			$this->error = "PIM";

		}

		

		if (empty($response)) {

            $response = false;

        }



        $this->data = $response;

        return $this->responseAction();

		



    }

    /**

     * @Route("/ws/onlineShoppingPage1/{param}",defaults = {"param"=""},requirements={"param"=".+"})

     * @Template()

     */

    public function onlineShoppingPage1Action(Request $request){



		$html = file_get_contents('https://shop.chessbase.com/de/cat_root'); //get the html returned from the following url



		$dom = new DomDocument();

		libxml_use_internal_errors(TRUE); 

		$dom->loadHTML($html);

		$classname = 'cb-table1';

		$finder = new DomXPath($dom);

		$nodes = $finder->query("//*[contains(concat(' ', normalize-space(@class), ' '), ' $classname ')]");

		$innerHTML = '';

		$tmp_dom = new DOMDocument(); 

		foreach ($nodes as $node) 

		{

			$tmp_dom->appendChild($tmp_dom->importNode($node,true));

		}

		$innerHTML.=trim($tmp_dom->saveHTML()); 

		echo $innerHTML;

		exit;

		

	

        $this->title = "Online Shopping Page";

        $param = $this->requestAction($request, 0);

        $this->validateRule = array(

            array(

                'rule' => 'NOTNULL',

                'field' => array(),

            ),

        );	

	

		if ($this->validateData($param)) {

			

			

			$html = '<iframe src="https://chessbase.in/online-shop/" height="800" width="500"></iframe>';

			

			$html .= '<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script><script>var iframe = document.getElementById("myFrame");

				  var elmnt = iframe.contentWindow.document.getElementsByClassName("full")[0];

				  document.write(elmnt.innerHTML)

				  </script>';

		

			$response = $html;	

			$this->error = "SFD";

			

		}else{

			$this->error = "PIM";

		}

		

		if (empty($response)) {

            $response = false;

        }



        $this->data = $response;

        return $this->responseAction();

		



    }





    /**

     * @Route("/ws/aboutus/{param}",defaults = {"param"=""},requirements={"param"=".+"})

     * @Template()

     */

    public function aboutusAction(Request $request){



		/*$html = file_get_contents('https://chessbase.in/about/'); //get the html returned from the following url



		$dom = new DomDocument();

		libxml_use_internal_errors(TRUE); 

		$dom->loadHTML($html);

		$classname = 'content';

		$finder = new DomXPath($dom);

		$nodes = $finder->query("//*[contains(concat(' ', normalize-space(@id), ' '), ' $classname ')]");

		$innerHTML = '';

		$tmp_dom = new DOMDocument(); 

		foreach ($nodes as $node) 

		{

			$tmp_dom->appendChild($tmp_dom->importNode($node,true));

		}

		$innerHTML.=trim($tmp_dom->saveHTML()); 

		echo $innerHTML;

		exit;*/



		



		

	

        $this->title = "Aboutus";

        $param = $this->requestAction($request, 0);

        $this->validateRule = array(

            array(

                'rule' => 'NOTNULL',

                'field' => array(),

            ),

        );	

	

		if ($this->validateData($param)) {

			

			
			

			

			$response = 'https://chessbase.in/about/';	

			$this->error = "SFD";

			

		}else{

			$this->error = "PIM";

		}

		

		if (empty($response)) {

            $response = false;

        }



        $this->data = $response;

        return $this->responseAction();

		



    }

	

	/**

     * @Route("/ws/fbPageLink/{param}",defaults = {"param"=""},requirements={"param"=".+"})

     * @Template()

     */

    public function fbPageLinkAction(Request $request){

		$fb = '<div class="fb-page" data-href="https://www.facebook.com/chessbaseindia"data-width="700" data-tabs = "timeline" data-hide-cover="false" data-show-facepile="false"></div>';

					  

		$fb .= "<script>(function(d, s, id){var js, fjs = d.getElementsByTagName(s)[0];if (d.getElementById(id))return;js = d.createElement(s); js.id = id;js.src = 'https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v3.1&appId=349425628851564&autoLogAppEvents=1';fjs.parentNode.insertBefore(js, fjs);}(document, 'script', 'facebook-jssdk'));</script>";	

		

		return new Response($fb);

    }

	

	

	/**

     * @Route("/ws/onlineShoppingPageLink/{param}",defaults = {"param"=""},requirements={"param"=".+"})

     * @Template()

     */

    public function onlineShoppingPageLinkAction(Request $request){

		

        $this->title = "Online Shopping Page Link";

        $param = $this->requestAction($request, 0);
		$em = $this->getDoctrine()->getManager();
        $this->validateRule = array(

            array(

                'rule' => 'NOTNULL',

                'field' => array(),

            ),

        );	

	

		if ($this->validateData($param)) {

			$data =null;
			$user_id ="";
			$country = null;
			if(isset($param->user_id)){
				$user_id = $param->user_id;
				$usermaster = $em->getRepository("AdminBundle:usermaster")->find($user_id);
				if(!empty($usermaster)){
					$country = $usermaster->getDomainId();
				}
			}
			$hostname = $_SERVER["SERVER_NAME"];
			

			$arraycountry = ["India","Bangladesh","Pakistan","Nepal","Thailand","Malaysia","Sri Lanka","Philippines","Indonesia"];
			
			if($country != "1" and $country != null){

				$countrydata = explode(",", $country );
				$country_id = $countrydata[0];
				$country_name = $countrydata[1];
			}else{
				$country_id ="INT";
				$country_name = "International";
			}
			if(isset($param->country)){
				$country_name =$param->country;
			}
			if(!isset($param->country) && !isset($param->user_id)){
				$country_name ="India";
			}
			if(in_array($country_name,$arraycountry)){
				$data = array(

						"shop_id"=>$country_id,

						"country"=>$country_name,

						"shop_url"=>'http://'.$hostname.''.$this->generateUrl("ws_wssocialmedia_onlineshoppingpage"),

						"country_logo"=>'https://upload.wikimedia.org/wikipedia/commons/6/63/International_flag_globe.png'



					);
			}else{
				

					$data = array(

						"shop_id"=>$country_id,

						"country"=>$country_name,

						"shop_url"=>'https://shop.chessbase.com/de/cat_root',

						"country_logo"=>'https://upload.wikimedia.org/wikipedia/commons/6/63/International_flag_globe.png'



					);
			}

				


			

			


					$response = $data;

			$this->error = "SFD";

			

		}else{

			$this->error = "PIM";

		}

		

		if (empty($response)) {

            $response = false;

        }



        $this->data = $response;

        return $this->responseAction();

		

		

	}





	





	/**

     * @Route("/ws/termscondition/{param}",defaults = {"param"=""},requirements={"param"=".+"})

     * @Template()

     */

    public function termsconditionAction(Request $request){

		



			$this->title = "termscondition" ;

			$param = $this->requestAction($request,0) ;

      $this->validateRule = array(

				array(

					'rule'=>'NOTNULL',

					'field'=>array(),

				),

			) ;

      /*

      ini_set('xdebug.var_display_max_depth', -1);

      ini_set('xdebug.var_display_max_children', -1);

      ini_set('xdebug.var_display_max_data', -1);

      var_dump($param->first_name);exit;

      */

			if($this->validateData($param)){

			       



				 $em = $this->getDoctrine()->getManager();

				 $Termscondition = $em->getRepository("AdminBundle:Termscondition")

                            ->findAll();



                 if(!empty($Termscondition)){           

					$response = array(

						"title"=>$Termscondition[0]->getName(),

						

						"description"=>$Termscondition[0]->getDescription(),

						

					

          

					);

					$this->error = "SFD" ;

				}

				else{

					$this->error = "NRF" ;

				}

			}else{

				$this->error = "PIM" ;

			}

			if(empty($response))

			{

				$response=False;

			}

			$this->data = $response ;

			return $this->responseAction() ;

		

		

	}





	/**

     * @Route("/ws/getfidedetails/{param}",defaults = {"param"=""},requirements={"param"=".+"})

     * @Template()

     */

    public function registerwithfideAction(Request $request){



    	$this->title = "Fide  data" ;

			$param = $this->requestAction($request,0) ;

      	$this->validateRule = array(

				array(

					'rule'=>'NOTNULL',

					'field'=>array("fide"),

				),

			);

      	if($this->validateData($param)){

      		$fide = $param->fide;





	    	$html = file_get_contents('https://ratings.fide.com/card.phtml?event='.$fide); 



			$dom = new DomDocument();

			libxml_use_internal_errors(TRUE); 

			$dom->loadHTML($html);

			$classname = 'contentpaneopen';

			$finder = new DomXPath($dom);



			$nodes = $finder->query("//*[contains(concat(' ', normalize-space(@width), ' '), ' 230 ')]");

			$innerHTML = '';

			$tmp_dom = new DOMDocument(); 

			foreach ($nodes as $node) 

			{

				$tmp_dom->appendChild($tmp_dom->importNode($node,true));

				

			}



			$innerHTML.=trim($tmp_dom->saveHTML()); 

			if(empty($innerHTML)){

				$this->error = "NRF" ;

				$this->data = False ;

				return $this->responseAction() ;

			}

			$name = $innerHTML;





			

	    	$html = file_get_contents('https://ratings.fide.com/card.phtml?event='.$fide); 



			$dom = new DomDocument();

			libxml_use_internal_errors(TRUE); 

			$dom->loadHTML($html);

			$classname = 'contentpaneopen';

			$finder = new DomXPath($dom);



			$nodes = $finder->query("//*[contains(concat(' ', normalize-space(@align), ' '), ' center ')]");

			$innerHTML = '';

			$tmp_dom = new DOMDocument(); 

			

				$tmp_dom->appendChild($tmp_dom->importNode($nodes[1],true));

				

			



			$innerHTML.=trim($tmp_dom->saveHTML()); 

			

			

			$rating = preg_match("/\d\d\d\d/", $innerHTML, $match);

      		$cnt = sizeof($match);



      		if($cnt!=0){

      			$rating = $match[0];

      		}else{

      			$rating = 0000;

      		}



			if (strpos($name, ",") !== false) {

				$name = explode(",", $name);

				$lastname = explode("&nbsp;", $name[0]);

				$first_name = str_replace("</td>","",$name[1]);

				$data = [

					"first_name"=>trim($first_name),

					"last_name"=>$lastname[1],

					"rating"=>$rating,

					"fide_id"=>$fide

				];

			}else{

				$name = explode(" ", $name);

				$firstname = explode("&nbsp;", $name[3]);

				$lastname = str_replace("</td>","",$name[4]);

				$data = [

					

					"first_name"=>$firstname[1],

					"last_name"=>trim($lastname),

					"rating"=>$rating,

					"fide_id"=>$fide

				];

			}

			$response = $data;

			$this->error = "SFD" ;

		}else{

			$this->error = "PIM" ;

		}

		if(empty($response))

		{

			$response=False;

		}

		$this->data = $response ;

		return $this->responseAction() ;

		

    }





    /**

     * @Route("/ws/newslink/{param}",defaults = {"param"=""},requirements={"param"=".+"})

     * @Template()

     */

    public function newlinkAction(Request $request){



		$html = file_get_contents('https://chessbase.in/news/Tata-Steel-Chess-India-Day-3'); //get the html returned from the following url



		$dom = new DomDocument();

		libxml_use_internal_errors(TRUE); 

		$dom->loadHTML($html);

		$classname = 'full';

		$finder = new DomXPath($dom);

		$nodes = $finder->query("//*[contains(concat(' ', normalize-space(@class), ' '), ' $classname ')]");

		$innerHTML = '';

		$tmp_dom = new DOMDocument(); 

		foreach ($nodes as $node) 

		{

			$tmp_dom->appendChild($tmp_dom->importNode($node,true));

		}

		$innerHTML.=trim($tmp_dom->saveHTML()); 

		//echo $innerHTML;

		//exit;

		

	

        $this->title = "Online Shopping Page";

        $param = $this->requestAction($request, 0);

        $this->validateRule = array(

            array(

                'rule' => 'NOTNULL',

                'field' => array(),

            ),

        );	

	

		if ($this->validateData($param)) {

			

			

			$html = '<iframe src="https://chessbase.in/news/Tata-Steel-Chess-India-Day-3" height="800" width="500"></iframe>';

			

			$html .= '<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script><script>var iframe = document.getElementById("myFrame");

				  var elmnt = iframe.contentWindow.document.getElementsByClassName("introduction")[0];

				  document.write(elmnt.innerHTML)

				  </script>';

			//echo "<html><body>".$html."</body></html>";exit;

			$response = $html;	

			$this->error = "SFD";

			

		}else{

			$this->error = "PIM";

		}

		

		if (empty($response)) {

            $response = false;

        }



        $this->data = $response;

        return $this->responseAction();

		



    }

		





}