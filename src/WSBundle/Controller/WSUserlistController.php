<?php

namespace WSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AdminBundle\Controller\CouponController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use AdminBundle\Entity\Userinfo;
use AdminBundle\Entity\Rolemaster;
use AdminBundle\Entity\Gcmuser;
use AdminBundle\Entity\Apnsuser;
use AdminBundle\Entity\users;

class WSUserlistController extends WSBaseController {

    /**
     * @Route("/ws/getUserList/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function getUserListAction(Request $request) {

        $this->title = "User List";
        $param = $this->requestAction($request, 0);

        $this->validateRule = array(
            array(
                'rule' => 'NOTNULL',
                'field' => array(
                    'role_id',
                    'domain_id',
                ),
            ),
        );

        if ($this->validateData($param)) {

            $role_id = $param->role_id;
            $domain_id = $param->domain_id;

            $user_list = $this->getDoctrine()->getRepository('AdminBundle:Usermaster')->findBy(
                array(
                    'user_role_id' => $role_id,
                    'domain_id' => $domain_id,
                    'user_status' => 'Active',
                    'is_deleted' => "0"
                )
            );

            if (!empty($user_list)) {
                foreach ($user_list as $_user) {

                    $courses = 0;
                    $user_id = $_user->getUsermaster_id();
                    if($role_id == $this->TRAINER_ROLE_ID){
                        $_sql = "SELECT * from coursemaster c, coursetrainermaster ct where c.main_coursemaster_id = ct.coursemaster_id and c.course_status = 'Active' and ct.usermaster_id = {$user_id} and c.is_deleted = 0 and ct.is_deleted = 0 group by main_coursemaster_id";
                        $course_list = $this->firequery($_sql);

                        $courses = count($course_list);
                    }

                    $response[] = array(
                        "id" => $user_id,
                        "firstname" => $_user->getUser_first_name(),
                        "lastname" => $_user->getUser_last_name(),
                        "email" => $_user->getUser_email_password(),
                        "user_mobile" => $_user->getUser_mobile(),
                        "courses" => $courses,
                        "age" => $_user->getAge(),
                        "user_bio" => $_user->getUser_bio(),
                        "achievements" => $_user->getAchievements(),
                        "image_url" => $this->getimage($_user->getMedia_id())
                    );
                }
                $this->error = "SFD";
                $this->data = $response;
            } else {
                $response = false;
                $this->error = "NRF";
            }
        } else {
            $this->error = "PIM";
        }

        if (empty($response)) {
            $response = false;
            $this->error = "NRF";
        }

        $this->data = $response;
        return $this->responseAction();
    }

}

?>
