<?php
namespace WSBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AdminBundle\Controller\CouponController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use AdminBundle\Entity\Userinfo ;
use AdminBundle\Entity\Rolemaster ;
use AdminBundle\Entity\Gcmuser;
use AdminBundle\Entity\Apnsuser;
use AdminBundle\Entity\users;

class WSUserRoleController extends WSBaseController {

     /**
     * @Route("/ws/getUserRoles/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function getUserRolesAction(Request $request) {

        $this->title = "Get User Roles";
        $param = $this->requestAction($request, 0);

        $this->validateRule = array(
            array(
                'rule' => 'NOTNULL',
                'field' => array(),
            ),
        );

		if ($this->validateData($param)) {
				
			$em = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Rolemaster');
			$role = $em->findAll();
			
			$userroles_new = null;
			
			$guest = $this->GUEST_ROLE_ID;

			$admin = '5b9cb0e4e81f71094840f902';
			$organization = '5c261503d618fc0d0c174c1f';

			
			if($role){
				foreach($role as $role_){
					if(strtolower($role_->getId()) != $guest && strtolower($role_->getId()) != $admin && strtolower($role_->getId()) != $organization){
						$response [] = array('role_id'=>$role_->getId(),
										     'role_name' => $role_->getRolename()
											);
					}	
				}
			}

			$this->error = "SFD";
			$this->data = $response;		
		}else{
			$this->error = "PIM";
		}
		
		if(empty($response)){
			$response = false;
			$this->error = "NRF";
		}
		
		$this->data = $response;
		return $this->responseAction();		
    }

}

?>
