<?php

namespace WSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AdminBundle\Controller\CouponController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use AdminBundle\Entity\Userinfo;
use AdminBundle\Entity\Rolemaster;
use AdminBundle\Entity\Gcmuser;
use AdminBundle\Entity\Apnsuser;
use AdminBundle\Entity\users;
use AdminBundle\Entity\coursemaster;
use AdminBundle\Entity\Tournament;
use AdminBundle\Entity\user_meeting_url;
use AdminBundle\Entity\user_package_relation;

class WSSubscriptionController extends WSBaseController {

    /**
     * @Route("/ws/subscription/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function subscriptionAction(Request $request) {
        $this->title = "subscription";
        $param = $this->requestAction($request, 0);

        $em = $this->getDoctrine()->getManager();
        $this->validateRule = array(
            array(
                'rule' => 'NOTNULL',
                'field' => array(),
            ),
        );

        if ($this->validateData($param)) {
            $user_id = $userdata = null;
            $larnardata = $trainerdata = $userdata = null;
            $coursetrainerid = [];
            $userMaster = NULL;
            if (isset($param->user_id)) {
                $user_id = $param->user_id;
                $userMaster = $em->getRepository("AdminBundle:usermaster")
                        ->find($user_id);
            }

            if (!empty($userMaster)) {
                $usertrainerrelation = $em->getRepository("AdminBundle:usertrainerrelation")
                        ->findOneBy(array(
                    "user_id" => $userMaster->getId(),
                        )
                );
                $userMaster->setLogindate(date('Y-m-d H:i:s'));
                $em->flush();
                if (!empty($usertrainerrelation)) {
                    if ($usertrainerrelation->getIs_approved() == "1") {
                        $role_id = $this->TRAINER_ROLE_ID;
                        $role = "trainer";
                        $coursetrainerone = $em
                                ->getRepository('AdminBundle:Coursetrainermaster')
                                ->findBy(array('is_deleted' => "0", 'usermaster_id' => $user_id));

                        foreach ($coursetrainerone as $key => $value) {
                            $coursetrainerid[] = $value->getCoursemasterId();
                        }
                    } else {
                        $role_id = $this->LEARNER_ROLE_ID;
                        $role = "learner";
                    }
                } else {
                    $role_id = $this->LEARNER_ROLE_ID;
                    $role = "learner";
                }
            } else {
                $role_id = $this->LEARNER_ROLE_ID;
                $role = "learner";
            }

            if (!isset($param->subscription_id)) {
                $subscription_master = $this->getDoctrine()->getManager()->getRepository('AdminBundle:subscriptionmaster')->findBy(array(
                    "is_deleted" => "0"
                ));

                if (isset($param->premium_flag) && ($param->premium_flag == "false")) {
                    $repository1 = $em->getRepository('AdminBundle:subscriptionmaster');
                    $subscription_master = $repository1->createQueryBuilder()
                                    ->field('is_deleted')->equals("0")
                                    ->field('price')->equals("0")
                                    ->getQuery()->execute()->toArray();
                }
                if (isset($param->premium_flag) && ($param->premium_flag == "true")) {
                    $repository1 = $em->getRepository('AdminBundle:subscriptionmaster');
                    $subscription_master = $repository1->createQueryBuilder()
                                    ->field('is_deleted')->equals("0")
                                    ->field('price')->notEqual("0")
                                    ->getQuery()->execute()->toArray();
                }
            } else {
                $subscription_master = $this->getDoctrine()->getManager()->getRepository('AdminBundle:subscription_master')->findBy(array(
                    "is_deleted" => "0", 'id' => $param->subscription_id
                ));
                if (isset($param->premium_flag) && ($param->premium_flag == "false")) {
                    $repository1 = $em->getRepository('AdminBundle:subscription_master');
                    $subscription_master = $repository1->createQueryBuilder()
                                    ->field('is_deleted')->equals("0")
                                    ->field('price')->equals("0")
                                    ->field('id')->equals($param->subscription_id)
                                    ->getQuery()->execute()->toArray();
                }
                if (isset($param->premium_flag) && ($param->premium_flag == "true")) {
                    $repository1 = $em->getRepository('AdminBundle:subscriptionmaster');
                    $subscription_master = $repository1->createQueryBuilder()
                                    ->field('is_deleted')->equals("0")
                                    ->field('price')->notEqual("0")
                                    ->field('id')->equals($param->subscription_id)
                                    ->getQuery()->execute()->toArray();
                }
                //var_dump( $subscription_master);exit;
            }
            foreach ($subscription_master as $key => $value) {
                if ($user_id != NULL) {
                    $user_package_relation = $this->getDoctrine()->getManager()->getRepository('AdminBundle:user_package_relation')->findBy(array(
                        "user_id" => $user_id,
                        "package_id" => $value->getId(),
                        "is_deleted" => "0"
                    ));
                }
                $flag = false;
                if (!empty($user_package_relation)) {
                    $flag = true; //is subcribe
                }
                $coursedataid = $value->getCourseId();
                $coursearray = null;
                foreach ($coursedataid as $key => $value1) {
                    # code...

                    $coursemaster = $this->getDoctrine()->getManager()->getRepository('AdminBundle:coursemaster')->findOneBy(array(
                        "is_deleted" => "0",
                        "id" => $value1
                    ));
                    if($coursemaster){
                        $coursearray[] = array(
                            "course_id" => $coursemaster->getId(),
                            "name" => $coursemaster->getCourseName(),
                        );
                    }
                }
                if($coursearray != NULL){
                foreach ($coursearray as $key => $courseval) {
                    $coursetrainerone = $em->getRepository('AdminBundle:Coursetrainermaster')
                            ->findBy(array('is_deleted' => "0", 'coursemaster_id' => $courseval['course_id']));
                    foreach ($coursetrainerone as $key => $ctvalue) {
                        $usermaster_id = $ctvalue->getUsermasterId();
                        $usermaster = $this->getDoctrine()->getManager()->getRepository('AdminBundle:usermaster')->findOneBy(array(
                            "is_deleted" => "0",
                            "id" => $usermaster_id
                        ));
                        if (!empty($usermaster)) {
                            $userdata[] = array(
                                "user_id" => $usermaster->getId(),
                                "email" => $usermaster->getUser_email_password(),
                                "name" => $usermaster->getUser_first_name(),
                                "img" => $this->getimage($usermaster->getMedia_id()),
                                "mobile_no" => $usermaster->getUser_mobile(),
                            );
                        }
                    }
                }
                }
                if (!empty($userdata)) {
                    $userdata = array_map("unserialize", array_unique(array_map("serialize", $userdata)));
                }

                $date1 = $value->getStartDate();
                $date2 = $value->getEndDate();
                $date1 = new \DateTime($date1);
                $date2 = new \DateTime($date2);
                $date3 = new \DateTime(date("Y-m-d"));
                $interval = $date1->diff($date2);

                $totaldays = $interval->days;
                $interval = $date3->diff($date2);
                $pendingdays = $interval->days;
                $usedper = 0 ;
                if($totaldays > 0 )
                    $usedper = (( $totaldays - $pendingdays ) * 100) / $totaldays;
                $user_package_relation = $this->getDoctrine()->getManager()->getRepository('AdminBundle:user_package_relation')->findBy(array(
                    "package_id" => $value->getId(),
                    "is_deleted" => "0"
                ));
                $totaluser = sizeof($user_package_relation);
                $premium_flag = true;
                if ($value->getPrice() <= 0 || $value->getPrice() == "" || $value->getPrice() == NULL) {
                    $premium_flag = false;
                }
                if ($role == "learner") {
                    $larnardata[] = array(
                        "subscription_id" => $value->getId(),
                        "name" => $value->getExtra(),
                        "desc" => $value->getSubscription_desc(),
                        'price' => $value->getPrice(),
                        'premium_flag' => $premium_flag,
                        'is_subcribed' => $flag, //is subcribe
                        "image_url" => $this->getimage($value->getExtra1()),
                        "startdate" => date("d-M-Y", strtotime($value->getStartDate())),
                        "enddate" => date("d-M-Y", strtotime($value->getEndDate())),
                        "course" => $coursearray,
                        "totaldays" => $totaldays,
                        "pendingdays" => $pendingdays,
                        "totaluser" => $totaluser,
                        "usedper" => round($usedper),
                        "trainerlist" => $userdata
                    );
                } else {
                    if (count(array_intersect($coursedataid, $coursetrainerid)) > 0) {

                        $trainerdata[] = array(
                            "subscription_id" => $value->getId(),
                            "name" => $value->getExtra(),
                            "desc" => $value->getSubscription_desc(),
                            'price' => $value->getPrice(),
                            'premium_flag' => $premium_flag,
                            "image_url" => $this->getimage($value->getExtra1()),
                            'is_subcribed' => $flag, //is subcribe
                            "startdate" => date("d-M-Y", strtotime($value->getStartDate())),
                            "enddate" => date("d-M-Y", strtotime($value->getEndDate())),
                            "course" => $coursearray,
                            "totaldays" => $totaldays,
                            "pendingdays" => $pendingdays,
                            "totaluser" => $totaluser,
                            "usedper" => round($usedper),
                            "trainerlist" => $userdata
                        );
                    }
                }
                $userdata = null;
            }
            $this->error = "SFD";
            if ($role == "learner") {
                $response = $larnardata;
            } else {
                $response = $trainerdata;
            }
        } else {
            $this->error = "PIM";
        }
        if (empty($response)) {
            $this->error = "NRF";
            $response = false;
        }
        $this->data = $response;
        return $this->responseAction();
    }

    /**
     * @Route("/ws/buysubscription/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function buysubscriptionAction(Request $request) {
        $this->title = "subscription";
        $param = $this->requestAction($request, 0);

        $em = $this->getDoctrine()->getManager();
        $this->validateRule = array(
            array(
                'rule' => 'NOTNULL',
                'field' => array("user_id", "subscription_id"),
            ),
        );
        if ($this->validateData($param)) {
            $coupon_id = NULL;
            $user_id = $param->user_id;
            $subscription_id = $param->subscription_id;
            $subscription_info = $this->getDoctrine()->getManager()->getRepository("AdminBundle:subscription_master")->find($subscription_id);
            if (empty($subscription_info)) {
                $this->error = "PIW";
                $this->data = false;
                return $this->responseAction();
            }
            $wallet_info = $this->getDoctrine()->getManager()->getRepository('AdminBundle:wallet_master')->findOneBy(array("user_id" => $user_id));
            if ($wallet_info) {
                $wallet_id = $wallet_info->get_id();
            }
            $cashback_amt = 0;
            $final_payment_amt = $subscription_amt = $subscription_info->getPrice();

            if (isset($param->coupon_id) && !empty($param->coupon_id)) {
                $coupon_id = $param->coupon_id;
                $coupon_info = $em->getRepository('AdminBundle:Couponmaster')
                        ->findOneBy(array('id' => $coupon_id, 'is_deleted' => "0"));

                if ($coupon_info) {
                    /*$user_coupon_assigned_list = $em->getRepository('AdminBundle:coupon_assigned_to_user')->findBy(array(
                                                        "coupon_id"=>$coupon_id,
                                                        "user_id"=>$user_id,
                                                        "is_deleted"=>"0"
                                                    ));
                    $coupon_usedBy = sizeof($user_coupon_assigned_list); // 3 
                    $use_can_coupon = $coupon_info->getNo_of_times_use() ; // 7 
                     */       
					// var_dump($coupon_info);
					
                    if ((date("Y-m-d H:i:s") < ($coupon_info->getEnd_date()))){//$use_can_coupon > $coupon_usedBy){
                        $discount_type = $coupon_info->getDiscount_type();
                        $return_type = $coupon_info->getReturn_type();
                        $disc_cal_number = $coupon_info->getDiscount_value();

                        if ($discount_type == 'percentage') {
                            $discount_value = ($subscription_amt * $disc_cal_number ) / 100;
                        } else {
                            $discount_value = $disc_cal_number;
                        }
                        if ($return_type = 'less_amount') {
                            $final_payment_amt = $final_payment_amt - $discount_value;
                        } elseif ($return_type == 'cashback') {
                            $cashback_amt = $discount_value;
                        }
                    }
                    else{
                        $response = false;
                        $this->error = "CCE";
                        $this->error_msg = "Coupon Code Expire";
						 return $this->responseAction();
                    }
                    
                }
            }


            $subscription_master_rel = $this->getDoctrine()->getManager()->getRepository('AdminBundle:user_package_relation')->findBy(array(
                "user_id" => $user_id,
                "subscription_id" => $subscription_id,
                "is_deleted" => "0"
            ));
            if (empty($subscription_master_rel)) {
                $user_package_relation = new user_package_relation();
                $user_package_relation->setUser_id($user_id);
                $user_package_relation->setPackageId($subscription_id);
                $user_package_relation->setCreatedDatetime(date("Y-m-d H:i:s"));
                if($final_payment_amt > 0 ){
                    $user_package_relation->setIsArchieved("1");
                    $user_package_relation->setIs_deleted("1");
                }
                else{
                     $this->error = "SFD";
                    $user_package_relation->setIsArchieved("0");
                    $user_package_relation->setIs_deleted("0");
                }
                $em->persist($user_package_relation);
                $em->flush();
                $user_package_relation_id = $user_package_relation->getId();
                $response = true;
                 if($final_payment_amt > 0 ){
                // ---------------------------------
                    $email = 'aaska@infoware.ws';
                    $surl = $this->base_url . $this->generateUrl("admin_payu_paymentsuccessful");
                    $furl = $this->base_url . $this->generateUrl("admin_payu_paymentfailure");

                    //$email = "testphp@gmail.com";
                    // productinfo - will get in response from payu if success

                    $param_arr = array(
                        'key' => $this->MERCHANT_KEY,
                        'txnid' => substr(hash('sha256', mt_rand() . microtime()), 0, 20),
                        'amount' => $final_payment_amt,
                        'service_provider'=>'payu_paisa',
                        'firstname' => 'firstname',
                        'email' => $email,
                        'phone' => '7405093206',
                        'productinfo' => $user_package_relation_id,
                        'surl' => $surl,
                        'furl' => $furl,
                        'lastname' => 'sample',
                        'address1' => "address1",
                        'city' => 'ahmedabad',
                        'state' => 'gujarat',
                        'zipcode' => '380015',
                        'country' => 'india',
                        'udf1' => $user_id,
                        'udf2' => $coupon_id,
                        'udf4' => 'india',
                        'udf5' => 'india'
                    );

                    $result = $this->pay_page($param_arr, $this->shared_secret);
                    
                    if (!empty($result)) {
                        if ($result['status'] == 1) {
                            $response = array(
                                'payu_server_url' => $result['data']
                            );
                            $this->error = "PSUCCESS";
                        } else {
                            $response = false;
                            $this->error = "PFAILED";
                            $this->error_msg = "something went wrong";
                        }
                    } else {
                        $response = false;
                        $this->error = "PFAILED";
                        $this->error_msg = "something went wrong";
                    }
                }
               
                if($final_payment_amt == 0 ){
                     $user_info = $this->getDoctrine()->getManager()->getRepository('AdminBundle:usermaster')->find($user_id);
                    $subscription_amt = $subscription_info->getPrice();
                    $subscription_id = $subscription_info->getId();
                    $course_ids = $subscription_info->getCourseId();
                    
                    $course_unit_arr = [] ; 
                    if($course_ids != NULL && $course_ids != ""){
                        foreach($course_ids as $ckey=>$cval){
                            $course_unit_relation_list = $em
                                    ->getRepository('AdminBundle:course_unit_relation')
                                    ->findBy(array('is_deleted' => "0", 'relation_object_id' => $cval,'relation_with'=>'course'));

                            if($course_unit_relation_list){
                                foreach($course_unit_relation_list as $delkey=>$delval){
                                    $course_unit_arr[] = $delval->getCourse_unit_id();
                                }
                            }
                        }
                    }
                    
                    if($course_unit_arr != NULL && !empty($course_unit_arr)){
                        foreach($course_unit_arr as $cukey=>$cuval){
                            $course_unit_info = $this->getDoctrine()->getManager()->getRepository('AdminBundle:course_unit')->find(array("id"=>$cuval,"is_deleted"=>"0","course_type"=>"1"));
                            if($course_unit_info != NULL){
                                $regIDD = '';
                                $need_approveUsers = [];
                                $zoom_meeting_id = $course_unit_info->getUnitCode();
                               // echo "<br>Meeting is : " . $zoom_meeting_id ;
                                $Meeting_reg_Function = 'meetings/' . $zoom_meeting_id . '/registrants';
                                $meeting_reg_request_url = $this->api_url . $Meeting_reg_Function;
                                $curl = curl_init();
                                $headers = array();
                                $headers[] = 'content-type: application/json';
                                $headers[] = 'authorization: Bearer ' . $this->access_token;
                                $meeting_reg_data = array(
                                    "email" => $user_info->getUser_email_password(),
                                    "first_name"=> $user_info->getUser_first_name(),
                                    "last_name"=> $user_info->getUser_last_name(),
                                    "phone"=>$user_info->getUser_mobile()                              

                                );
                                curl_setopt_array($curl, array(
                                    CURLOPT_URL => $meeting_reg_request_url,
                                    CURLOPT_RETURNTRANSFER => true,
                                    CURLOPT_ENCODING => "",
                                    CURLOPT_MAXREDIRS => 10,
                                    CURLOPT_TIMEOUT => 30,
                                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                    CURLOPT_CUSTOMREQUEST => "POST",
                                    CURLOPT_POSTFIELDS =>json_encode($meeting_reg_data),
                                    CURLOPT_HTTPHEADER => $headers
                                ));

                                $Meeting_registrant_response = curl_exec($curl);
                                $Meeting_registrant_err = curl_error($curl);

                                curl_close($curl);

                                if ($Meeting_registrant_err) {
                                   // echo "cURL Error #:" . $Meeting_registrant_err; 
                                } else {   

                                     $Meeting_registrant_response = json_decode($Meeting_registrant_response);

                                     if(isset($Meeting_registrant_response->registrant_id)){

                                         $need_approveUsers[] = array(                                              
                                                    "id"=> $Meeting_registrant_response->registrant_id,
                                                    "email"=> $user_info->getUser_email_password()
                                                   );

                                     }
                                }
                                //var_dump($need_approveUsers);
                                if($need_approveUsers != NULL){
                                    $curl = curl_init();
                                    $Meeting_reg_StatusFunction = 'meetings/' . $zoom_meeting_id . '/registrants/status';
                                    $meeting_reg_Statusrequest_url = $this->api_url . $Meeting_reg_StatusFunction;
                                    $meeting_registrant_updateData = array(                                        
                                            "action"=> "approve",
                                            "registrants"=>$need_approveUsers
                                    );

                                    curl_setopt_array($curl, array(
                                      CURLOPT_URL => $meeting_reg_Statusrequest_url,
                                      CURLOPT_RETURNTRANSFER => true,
                                      CURLOPT_ENCODING => "",
                                      CURLOPT_MAXREDIRS => 10,
                                      CURLOPT_TIMEOUT => 30,
                                      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                      CURLOPT_CUSTOMREQUEST => "PUT",
                                      CURLOPT_POSTFIELDS =>json_encode($meeting_registrant_updateData),
                                      CURLOPT_HTTPHEADER => $headers
                                    ));

                                    $Meeting_registrant_status_response = curl_exec($curl);
                                    $Meeting_registrant_status_err = curl_error($curl);

                                    curl_close($curl);

                                    if ($Meeting_registrant_status_err) {
                                      //echo " --> cURL Error #:" . $Meeting_registrant_status_err;exit;
                                    } else {
                                      //echo $Meeting_registrant_status_response;
                                      //var_dump($Meeting_registrant_status_response);exit;
                                      // add in meeting user table
                                        $joinURL = '';
                                        $regIDD = $Meeting_registrant_response->registrant_id ;
                                        if(isset($Meeting_registrant_response->join_url)){
                                            $joinURL = $Meeting_registrant_response->join_url;
                                        }
                                        $user_meeting_url = new user_meeting_url();
                                        $user_meeting_url->setUser_id($user_id);
                                        $user_meeting_url->setCourse_unit_id($course_unit_info->getId());
                                        $user_meeting_url->setMeeting_id($zoom_meeting_id);
                                        $user_meeting_url->setMeeting_url($joinURL);
                                        $user_meeting_url->setRegistrant_id($Meeting_registrant_response->registrant_id);
                                        $user_meeting_url->setIs_deleted("0");
                                        $em->persist($user_meeting_url);
                                        $em->flush();

                                    }
                               }
                               //----------------------------------
                                $curl = curl_init();
                                $Meeting_getRegistrant_Function = 'meetings/' . $zoom_meeting_id . '/registrants?status=approved';
                                $meeting_getRegistrant_url = $this->api_url . $Meeting_getRegistrant_Function;
                                curl_setopt_array($curl, array(
                                  CURLOPT_URL => $meeting_getRegistrant_url,
                                  CURLOPT_RETURNTRANSFER => true,
                                  CURLOPT_ENCODING => "",
                                  CURLOPT_MAXREDIRS => 10,
                                  CURLOPT_TIMEOUT => 30,
                                  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                  CURLOPT_CUSTOMREQUEST => "GET",
                                  CURLOPT_HTTPHEADER => $headers
                                ));

                                $Meeting_getRegistrant_response = curl_exec($curl);
                                $err = curl_error($curl);

                                curl_close($curl);

                                if ($err) {
                                  echo "cURL Error #:" . $err;
                                } else {
                                 // echo $Meeting_getRegistrant_response;
                                }
                                $Meeting_getRegistrant_response = (json_decode($Meeting_getRegistrant_response));
                                $Meeting_getRegistrant_response = $Meeting_getRegistrant_response->registrants;

                                foreach($Meeting_getRegistrant_response as $nval){

                                    $need_reg_id = $nval->id;
                                    $need_join_url = $nval->join_url;
                                    if($regIDD == $need_reg_id){
                                        //-------------------------------------------------------------------------------------
                                        $user_meeting_url = $this->getDoctrine()->getManager()->getRepository('AdminBundle:user_meeting_url')->findOneBy(array("registrant_id" => $need_reg_id));
                                        if($user_meeting_url){
                                            $user_meeting_url->setMeeting_url($need_join_url);
                                            $em->flush();
                                        }
                                    }
                                }
                            }
                        }
                    }
                
                }
               
                $this->data = $response;
                // ---------------------------------
                // $this->error = "SFD";
            } else {
                $this->error = "ARE";
            }
        } else {
            $this->error = "PIM";
        }

        if (empty($response)) {
            $response = false;
        }
        //  $this->data = $response;
        return $this->responseAction();
    }

    /**
     * @Route("/ws/coursesubscription/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function coursesubscriptionAction(Request $request) {
        $this->title = "Course subscription ";
        $param = $this->requestAction($request, 0);

        $em = $this->getDoctrine()->getManager();
        $this->validateRule = array(
            array(
                'rule' => 'NOTNULL',
                'field' => array("course_id"),
            ),
        );

        if ($this->validateData($param)) {
            $userdata = null;
            $larnardata = $trainerdata = $userdata = null;
            $course_id = $param->course_id;
            $subscriptionList = [];
            $coursemasterDetails = $this->getDoctrine()->getManager()->getRepository('AdminBundle:coursemaster')->findOneBy(array("id" => $course_id, "is_deleted" => "0"));
            if (!empty($coursemasterDetails)) {
                $subscription_list = $this->getDoctrine()->getManager()->getRepository('AdminBundle:subscription_master')->findBy(array("is_deleted" => '0'));
                if (!empty($subscription_list)) {
                    foreach ($subscription_list as $subkey => $subval) {
                        $courseIDS = $subval->getCourseId();
                        if (in_array($course_id, $courseIDS)) {
                            $subscriptionList[] = array(
                                "subscription_id" => $subval->getId(),
                                "name" => $subval->getExtra()
                            );
                        }
                    }
                }
                if (!empty($subscriptionList)) {
                    $this->error = "SFD";
                    $response = $subscriptionList;
                }
            } else {
                $this->error = "PIW";
            }
        } else {
            $this->error = "PIM";
            $this->data = false;
            return $this->responseAction();
        }
        if (empty($response)) {
            $this->error = "NRF";
            $response = false;
        }
        $this->data = $response;
        return $this->responseAction();
    }

}
