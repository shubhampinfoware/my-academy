<?php

namespace WSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AdminBundle\Controller\CouponController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use AdminBundle\Entity\Userinfo;
use AdminBundle\Entity\Rolemaster;
use AdminBundle\Entity\Gcmuser;
use AdminBundle\Entity\Apnsuser;
use AdminBundle\Entity\users;
use AdminBundle\Entity\coursemaster;
use AdminBundle\Entity\course_unit;
use AdminBundle\Entity\course_unit_relation;

class WSAddclassController extends WSBaseController {

    /**
     * @Route("/ws/addclass/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function courseAction(Request $request) {

        $this->title = "Add Class";
        $param = $this->requestAction($request, 0);

        $em = $this->getDoctrine()->getManager();
        $this->validateRule = array(
            array(
                'rule' => 'NOTNULL',
                'field' => array("user_id", "class_name", "youtube_link", "course_id", "category_id"),
            ),
        );
        if ($this->validateData($param)) {
            $user_id = $param->user_id;
            $class_name = $param->class_name;
            $youtube_link = $param->youtube_link;
            $course_id = $param->course_id;
            $category_id = $param->category_id;
            $class_level_id = $param->level_id;
            $description = "";
            if (isset($param->description)) {
                $description = $param->description;
            }
            $course_id = explode(",",$course_id);
            $category_id = explode(",",$category_id);
            $class_type = 0;
            $usermaster = $userMaster = $em->getRepository("AdminBundle:usermaster")
                    ->findOneBy(array(
                "id" => $user_id,
                "user_role_id" => $this->TRAINER_ROLE_ID,
                "is_deleted" => "0",
                    )
            );
            if (empty($usermaster)) {
                $this->error = "UNF";
                $this->data = false;
                return $this->responseAction();
            }

            $date_time = date("Y-m-d H:i:s");
            $query_str = parse_url($_REQUEST['youtube_link'], PHP_URL_QUERY);
            parse_str($query_str, $query_params);
            $json = file_get_contents('https://www.googleapis.com/youtube/v3/videos?id=' . $query_params['v'] . '&part=contentDetails&key=AIzaSyAS2T7d5SqW81JNqAmICfTtEVRNiOxtAYY');
            $obj = json_decode($json);
            if ($obj->items) {
                $media_id = 0;
                if (isset($_FILES['class_image']) && !empty($_FILES['class_image'])) {
                    $category_logo = $_FILES['class_image'];
                    $logo = $category_logo['name'];
                    $media_type_id = 1;
                    $tmpname = $category_logo['tmp_name'];
                    $file_path = $this->container->getParameter('file_path');
                    $logo_path = $file_path . '/user';
                    $logo_upload_dir = $this->container->getParameter('upload_dir') . '/user/';
                    $media_id = $this->mediauploadAction($logo, $tmpname, $logo_path, $logo_upload_dir, $media_type_id);
                    // var_dump($media_id);exit;
                }
                if (isset($param->class_id)) {
                    $class_id = $param->class_id;
                    $class = $userMaster = $em->getRepository("AdminBundle:course_unit")
                            ->findOneBy(array(
                        "id" => $class_id,
                        "is_deleted" => "0"
                            )
                    );
                    if (empty($class)) {
                        $this->error = "NRF";
                        $response = false;
                        return $this->responseAction();
                    }
                    else{
                        $course_unit_relation_list = $em
                            ->getRepository('AdminBundle:course_unit_relation')
                            ->findBy(array('is_deleted' => "0", 'course_unit_id' => $class_id));
                        if($course_unit_relation_list){
                            foreach($course_unit_relation_list as $delkey=>$delval){
                                $delval->setIs_deleted(1);
                                $em->flush();
                            }
                        }
                    }
                } else {
                    $class = new course_unit();
                    $class->setClass_cover_image($media_id);
                }
                if (isset($param->class_id) && ($media_id != 0 )) {
                    $class->setClass_cover_image($media_id);
                }
                $class->setUnitName($class_name);
                $class->setCourseType($class_type);
                $class->setYoutubeLink($youtube_link);
                $class->setCoursemasterId(0);
                $class->setCourseMediaId(0);   
                $class->setCourseUnitMediaId(0);
                $class->setSortOrder(0);

                $class->setStatus("Active");
                $class->setPrerequisites("youtube");
                $class->setInstruction($description);
                $class->setCreatedBy($user_id);
                $class->setLockFeature($user_id);
                $class->setClass_level_id($class_level_id);

                $class->setLanguageId("1");
                $class->setDomain_id("1");
                $class->setIs_deleted("0");

                $class->setCreate_date($date_time);
                $class->setUpdatedDate($date_time);
                $class->setAllowShuffle("Yes");
                $class->setMainCourseUnitId(0);
                $em->persist($class);
                $em->flush();
                $main_cat = $class->getId();
                $class->setMainCourseUnitId($main_cat);
                $class->setCreate_date($date_time);
                $em->persist($class);
                $em->flush();
                $this->error = "SFD";
                $main_id = $class->getId();
                $response = array(
                    'main_id' => $main_id
                );
                if($category_id){
                    foreach($category_id as $ckey=>$cval){
                        $class_catgeory_realtion = new course_unit_relation();
                        $class_catgeory_realtion->setCourse_unit_id($main_cat);
                        $class_catgeory_realtion->setRelation_with('category');
                        $class_catgeory_realtion->setRelation_object_id($cval);
                        $class_catgeory_realtion->setIs_deleted(0);
                        $class_catgeory_realtion->setField_1('');
                        $class_catgeory_realtion->setField_2('');
                        $em->persist($class_catgeory_realtion);
                        $em->flush();
                    }
                }
                if($course_id){
                    foreach($course_id as $ckey=>$cval){
                        $class_catgeory_realtion = new course_unit_relation();
                        $class_catgeory_realtion->setCourse_unit_id($main_cat);
                        $class_catgeory_realtion->setRelation_with('course');
                        $class_catgeory_realtion->setRelation_object_id($cval);
                        $class_catgeory_realtion->setIs_deleted(0);
                        $class_catgeory_realtion->setField_1('');
                        $class_catgeory_realtion->setField_2('');
                        $em->persist($class_catgeory_realtion);
                        $em->flush();
                    }
                }
            } else {
                $this->error = "IVU";
                $response = false;
                return $this->responseAction();
            }
        } else {
            $this->error = "PIM";
        }

        if (empty($response)) {
            $response = false;
        }
        $this->data = $response;
        return $this->responseAction();
    }

}
