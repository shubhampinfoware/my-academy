<?php

namespace WSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AdminBundle\Controller\CouponController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use AdminBundle\Entity\Userinfo;
use AdminBundle\Entity\Rolemaster;
use AdminBundle\Entity\Gcmuser;
use AdminBundle\Entity\Apnsuser;
use AdminBundle\Entity\users;
use AdminBundle\Entity\fideuser;

class WSUserLoginController extends WSBaseController
{

    /**
     * @Route("/ws/userlogin/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function userLoginAction(Request $request)
    {
        //try{
        $this->title = "User login";
        $param = $this->requestAction($request, 0);
        $em = $this->getDoctrine()->getManager();
        $this->validateRule = array(
            array(
                'rule' => 'NOTNULL',
                'field' => array(
                    'email',
                    'password',
                    'domain_id'
                ),
            ),
        );

        if ($this->validateData($param)) {
            $email = $param->email;
            $password = $param->password;
            $domain_id = $param->domain_id;

            if (isset($param->password)) {
                $password = md5($param->password);
                $userMaster = $em->getRepository("AdminBundle:Usermaster")
                    ->findOneBy(
                        array(
                            "user_email_password" => $email,
                            "user_password" => $password,
                            'domain_id' => $domain_id
                        )
                    );
            }
            if (isset($param->verify)) {
                if ($param->verify == true) {
                    $userMaster = $em->getRepository("AdminBundle:Usermaster")
                        ->findOneBy(
                            array(
                                "user_email_password" => $email,
                                'domain_id' => $domain_id
                            )
                        );
                } else {
                    $this->error = "NRF";
                    $this->data = null;
                    return $this->responseAction();
                }
            }
            if (!empty($userMaster)) {

                $usertrainerrelation = $em->getRepository("AdminBundle:Usertrainerrelation")
                    ->findOneBy(
                        array(
                            "user_id" => $userMaster->getUsermaster_id(),
                        )
                    );
                $userMaster->setLogindate(date('Y-m-d H:i:s'));
                $em->flush();
                $role_id = $userMaster->getUser_role_id();
                $em = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Rolemaster');

                $userrole = $em->find($role_id);
                if ($userrole) {
                    $roled = array(
                        "role_id" => $userMaster->getUser_role_id(),
                        "role_name" => $userrole->getRolename()
                    );
                }

                $response = array(
                    "user_id" => $userMaster->getUsermaster_id(),
                    "email" => $userMaster->getUser_email_password(),
                    "name" => $userMaster->getUser_first_name(),
                    "verify" => $userMaster->getExt_field(),
                    "img" => $this->getimage($userMaster->getMedia_id()),
                    "mobile_no" => $userMaster->getUser_mobile(),
                    "roles" => $roled,
                    "birthdate" => $userMaster->getBirthdate()
                );

                $this->error = "SFD";
            } else {
                $this->error = "NRF";
            }
        } else {
            $this->error = "PIM";
        }
        if (empty($response)) {
            $response = false;
        }

        $this->data = $response;
        return $this->responseAction();
        /* }catch(\Exception $e){
          $this->error = "SFND" ;
          $this->data = "SFND" ;
          return $this->responseAction() ;
          } */
    }

    /**
     * @Route("/ws/userloginPortal/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function userLoginPortalAction(Request $request)
    {
        //try{
        $this->title = "User login Portal";
        $param = $this->requestAction($request, 0);
        $em = $this->getDoctrine()->getManager();
        $this->validateRule = array(
            array(
                'rule' => 'NOTNULL',
                'field' => array('email', 'user_password'),
            ),
        );

        if ($this->validateData($param)) {
            $email = $param->email;
            $normalpassword = $param->user_password;
            $password = md5($param->user_password);
            $userMasterFromChesscoahing = $em->getRepository("AdminBundle:usermaster")
                ->findOneBy(array(
                    "user_email_password" => $email,
                    "user_password" => $password,
                    "register_from" => "actualuserchesscoachingsite"
                ));

            if ($userMasterFromChesscoahing) {
                $userMaster = $userMasterFromChesscoahing;
                $usertrainerrelation = $em->getRepository("AdminBundle:usertrainerrelation")
                    ->findOneBy(
                        array(
                            "user_id" => $userMaster->getId(),
                        )
                    );
                $userMaster->setLogindate(date('Y-m-d H:i:s'));
                $em->flush();
                if ($usertrainerrelation->getIs_approved() == "1") {
                    $role_id = $this->TRAINER_ROLE_ID;
                } else {
                    $role_id = $this->LEARNER_ROLE_ID;
                }
                $em = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Rolemaster');

                $userrole = $em->find($role_id);
                if ($userrole) {
                    $roled = array(
                        "role_id" => $userMaster->getUser_role_id(),
                        "role_name" => $userrole->getRolename()
                    );
                }


                $response = array(
                    "user_id" => $userMaster->getId(),
                    "email" => $userMaster->getUser_email_password(),
                    "name" => $userMaster->getUser_first_name(),
                    "verify" => $userMaster->getExtField(),
                    "img" => $this->getimage($userMaster->getMedia_id()),
                    "mobile_no" => $userMaster->getUser_mobile(),
                    "roles" => $roled,
                    "birthdate" => $userMaster->getBirthdate()
                );

                $this->error = "SFD";
            } else {
                // Find From Chessbase 
                $device_token = '123456';
                $device_id = '123456';
                $external_id = $chesscoachingUser_id = '';
                $register_from = 'chessbaseuserchesscoachingsite';
                $passed_role = $this->LEARNER_ROLE_ID;
                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_URL => "https://app.chessbase.in/chessbase/app.php/ws/userlogin",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "POST",
                    CURLOPT_POSTFIELDS => "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"email\"\r\n\r\n" . $email . "\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"password\"\r\n\r\n" . $normalpassword . "\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"device_name\"\r\n\r\nandroid\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"device_token\"\r\n\r\n123456\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"device_id\"\r\n\r\n123456\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--",
                    CURLOPT_HTTPHEADER => array(
                        "cache-control: no-cache",
                        "content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
                        "postman-token: 1d772f93-b43b-2bd8-96a5-1f9d5a24bfa6"
                    ),
                ));

                $response = curl_exec($curl);
                $err = curl_error($curl);

                curl_close($curl);

                if ($err) {
                    $response = false;
                    $this->error = "CBDBERR";
                } else {
                    //  echo $response;
                    $response = json_decode($response);
                    if (isset($response->data) && ($response->data != false)) {
                        $response_data = $response->data;

                        $external_id = $chesscoachingUser_id = $response_data->user_id;
                        $first_name = $response_data->first_name;
                        $last_name = $response_data->last_name;
                        $mobile_no = $response_data->mobile_no;
                        $verify = false;

                        // check in DB
                        $userMaster = $em->getRepository("AdminBundle:usermaster")->findOneBy(array("external_id" => $external_id));
                        if ($userMaster) {
                            $usertrainerrelation = $em->getRepository("AdminBundle:usertrainerrelation")
                                ->findOneBy(
                                    array(
                                        "user_id" => $userMaster->getId(),
                                    )
                                );
                            $userMaster->setUser_first_name($first_name);
                            $userMaster->setUser_last_name($last_name);
                            $userMaster->setUser_password(md5($normalpassword));
                            $userMaster->setUser_mobile($mobile_no);
                            $userMaster->setLogindate(date('Y-m-d H:i:s'));
                            $em->flush();
                            if ($usertrainerrelation->getIs_approved() == "1") {
                                $role_id = $this->TRAINER_ROLE_ID;
                            } else {
                                $role_id = $this->LEARNER_ROLE_ID;
                            }
                            $em = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Rolemaster');

                            $userrole = $em->find($role_id);
                            if ($userrole) {
                                $roled = array(
                                    "role_id" => $userMaster->getUser_role_id(),
                                    "role_name" => $userrole->getRolename()
                                );
                            }


                            $response = array(
                                "user_id" => $userMaster->getId(),
                                "email" => $userMaster->getUser_email_password(),
                                "name" => $userMaster->getUser_first_name(),
                                "verify" => $userMaster->getExtField(),
                                "img" => $this->getimage($userMaster->getMedia_id()),
                                "mobile_no" => $userMaster->getUser_mobile(),
                                "roles" => $roled,
                                "birthdate" => $userMaster->getBirthdate()
                            );
                            $this->error = "SFD";
                        } else {
                            // call user register ws of chesscoaching
                            $user_register_curl = curl_init();
                            // $chesscoachinregisterUrl = "http://admin-pc/chesscoaching/ws/userregistration";
                            $chesscoachinregisterUrl = "https://skillgamesacademy.com/chesscoaching/ws/userregistration";
                            curl_setopt_array($user_register_curl, array(
                                CURLOPT_URL => $chesscoachinregisterUrl,
                                CURLOPT_RETURNTRANSFER => true,
                                CURLOPT_ENCODING => "",
                                CURLOPT_MAXREDIRS => 10,
                                CURLOPT_TIMEOUT => 30,
                                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                CURLOPT_CUSTOMREQUEST => "POST",
                                CURLOPT_POSTFIELDS => "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"firstname\"\r\n\r\n" . $first_name . "\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"lastname\"\r\n\r\n" . $last_name . "\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"email\"\r\n\r\n" . $email . "\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"mobile_no\"\r\n\r\n" . $mobile_no . "\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"role\"\r\n\r\n" . $passed_role . "\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"verify\"\r\n\r\nfalse\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"password\"\r\n\r\n" . $normalpassword . "\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"register_from\"\r\n\r\n" . $register_from . "\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"external_id\"\r\n\r\n" . $external_id . "\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--",
                                CURLOPT_HTTPHEADER => array(
                                    "cache-control: no-cache",
                                    "content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
                                    "postman-token: 403b7eaf-33df-02ae-1c86-2de43517d2db"
                                ),
                            ));

                            $user_register_response = curl_exec($user_register_curl);
                            $user_register_err = curl_error($user_register_curl);
                            curl_close($user_register_curl);
                            if ($user_register_err) {
                                $this->error = "UNR";
                                $response = false;
                            } else {
                                //echo $user_register_response;
                                $user_register_response = json_decode($user_register_response);
                                $user_register_response = $user_register_response->data;
                                $user_master_id = $user_register_response->user_id;

                                $userMaster = $em->getRepository("AdminBundle:usermaster")->findOneBy(array("id" => $user_master_id));
                                if ($userMaster) {
                                    $usertrainerrelation = $em->getRepository("AdminBundle:usertrainerrelation")
                                        ->findOneBy(
                                            array(
                                                "user_id" => $userMaster->getId(),
                                            )
                                        );
                                    $userMaster->setLogindate(date('Y-m-d H:i:s'));
                                    $em->flush();
                                    if ($usertrainerrelation->getIs_approved() == "1") {
                                        $role_id = $this->TRAINER_ROLE_ID;
                                    } else {
                                        $role_id = $this->LEARNER_ROLE_ID;
                                    }
                                    $em = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Rolemaster');

                                    $userrole = $em->find($role_id);
                                    if ($userrole) {
                                        $roled = array(
                                            "role_id" => $userMaster->getUser_role_id(),
                                            "role_name" => $userrole->getRolename()
                                        );
                                    }


                                    $response = array(
                                        "user_id" => $userMaster->getId(),
                                        "email" => $userMaster->getUser_email_password(),
                                        "name" => $userMaster->getUser_first_name(),
                                        "verify" => $userMaster->getExtField(),
                                        "img" => $this->getimage($userMaster->getMedia_id()),
                                        "mobile_no" => $userMaster->getUser_mobile(),
                                        "roles" => $roled,
                                        "birthdate" => $userMaster->getBirthdate()
                                    );
                                    $this->error = "SFD";
                                } else {
                                    $this->error = "URSBTIDWRNG";
                                    $response = false;
                                }
                            }
                        }
                    } else {
                        $this->error = "NRF";
                        $response = false;
                    }
                }
            }
        } else {
            $this->error = "PIM";
        }
        if (empty($response)) {
            $response = false;
        }

        $this->data = $response;
        return $this->responseAction();
        /* }catch(\Exception $e){
          $this->error = "SFND" ;
          $this->data = "SFND" ;
          return $this->responseAction() ;
          } */
    }

    /**
     * @Route("/ws/update_token/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function update_tokenAction(Request $request)
    {
        //try{
        $this->title = "Update token";
        $param = $this->requestAction($request, 0);
        $em = $this->getDoctrine()->getManager();
        $this->validateRule = array(
            array(
                'rule' => 'NOTNULL',
                'field' => array('user_id', 'device_type', 'device_id', 'reg_id'),
            ),
        );

        if ($this->validateData($param)) {
            $app_id = 'CUST';
            $devicename = strtolower(urlencode($param->device_type));

            $device_id = urlencode($param->device_id);

            $regid = $param->reg_id;

            $user_id = $param->user_id;

            $app_id = $param->app_id;



            $data = false;


            $userMaster = $em->getRepository("AdminBundle:users")
                ->find($user_id);



            if (!empty($userMaster) && !empty($devicename) && $devicename == 'android') {

                $gcm_user_del = $em->getRepository("AdminBundle:Gcmuser")->findBy(
                    array(
                        "device_id" => $device_id,
                        "app_id" => $app_id
                    )
                );
                foreach ($gcm_user_del as $value) {
                    $value->setIs_deleted(1);
                    $em->persist($value);
                    $em->flush();
                }
                $gcm_user_info = new Gcmuser();
                $gcm_user_info->setGcmregid($regid);
                $gcm_user_info->setUser_id($user_id);

                $gcm_user_info->setAppid($app_id);
                $gcm_user_info->setName('');
                $gcm_user_info->setCreateddate(date('Y-m-d H:i:s'));
                $gcm_user_info->setDeviceid($device_id);

                $gcm_user_info->setIs_deleted(0);

                $em->persist($gcm_user_info);
                $em->flush();
                $data = true;
                $this->error = "SFD";
            } else {
                $this->error = "NRF";
            }
            $response = $data;
        } else {
            $this->error = "PIM";
        }
        if (empty($response)) {
            $response = false;
        }

        $this->data = $response;
        return $this->responseAction();
        /* }catch(\Exception $e){
          $this->error = "SFND" ;
          $this->data = "SFND" ;
          return $this->responseAction() ;
          } */
    }

    /**
     * @Route("/ws/userlogout/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function userlogoutAction(Request $request)
    {
        //try{
        $this->title = "User Logout";
        $param = $this->requestAction($request, 0);
        $em = $this->getDoctrine()->getManager();
        $this->validateRule = array(
            array(
                'rule' => 'NOTNULL',
                'field' => array('user_id', 'device_id'),
            ),
        );

        if ($this->validateData($param)) {
            $app_id = 'CUST';
            $user_id = $param->user_id;
            $device_id = $param->device_id;

            $apns_user_delete_list = $em->getRepository("AdminBundle:Gcmuser")
                ->findOneBy(array(
                    "user_id" => $user_id,
                    "device_id" => $device_id,
                    "app_id" => $app_id,
                    "is_deleted" => '0'
                ));

            if (!empty($apns_user_delete_list)) {
                $apns_user_delete_list->setIs_deleted(1);

                $em->persist($apns_user_delete_list);
                $em->flush();
                $response = array('user_id' => $user_id, 'device_id' => $device_id, 'app_id' => $app_id);
                $this->error = "SFD";
            } else {
                $this->error = "NRF";
            }
            if (empty($response)) {
                $response = false;
            }

            $this->data = $response;
            return $this->responseAction();
        }
    }

    /**
     * @Route("/ws/changepassword/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function changepasswordAction(Request $request)
    {
        //try{
        $this->title = "change password";
        $param = $this->requestAction($request, 0);
        $em = $this->getDoctrine()->getManager();
        $this->validateRule = array(
            array(
                'rule' => 'NOTNULL',
                'field' => array('user_id', 'oldpassword', "newpassword"),
            ),
        );

        if ($this->validateData($param)) {

            $user_id = $param->user_id;
            $newpassword = $param->newpassword;
            $oldpassword = $param->oldpassword;

            $userMaster = $em->getRepository("AdminBundle:users")
                ->findOneBy(
                    array(
                        "id" => $user_id,
                    )
                );

            if ($userMaster->getBcrypt() == md5($oldpassword)) {
                if (!empty($userMaster)) {
                    $userMaster->setBcrypt(md5($newpassword));
                    $em->flush();
                    $this->error = "SFD";
                    $response = true;
                } else {
                    $this->error = "UNF";
                }
            } else {
                $this->error = "WOP";
            }
        } else {
            $this->error = "PIM";
        }

        if (empty($response)) {
            $response = false;
        }

        $this->data = $response;
        return $this->responseAction();
    }
}
