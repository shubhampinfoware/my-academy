<?php

namespace WSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AdminBundle\Entity\Usermaster;
use AdminBundle\Entity\Usersetting;
use AdminBundle\Entity\Apppushnotificationmaster;
use AdminBundle\Entity\Categorymaster;
use AdminBundle\Entity\mediamaster;
use AdminBundle\Entity\Apptypemaster;
use AdminBundle\Entity\Appdetails;
use AdminBundle\Entity\Bwidwssecurity;

header('Access-Control-Allow-Origin: *');

class WSBaseController extends Controller {

    public $title = "";
    public $error = "SFND";
    public $error_msg = "SFND";
    public $data = false;
    public $validateRule = array();
    // public $api_key = 'cfz5c7FMTKGQzgWtrGROfA'; //Please Input Your Own API Key Here Old';
    public $api_key = 'waRgqjneRTyIhCdnRhVl8A'; //Please Input Your Own API Key Here(chessbase Account)';
    public $api_secret = 'D1ArW1vL1yyFG5bwd87pgGMGwrOcK2n1'; //Please Input Your Own API Secret Here';
    public $shared_secret = "pa1234";
	
    public $api_url = 'https://api.zoom.us/v2/';
    // public $access_token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJpVFdrWGcxRFRjV2hVMG05cGU4V0R3IiwiZXhwIjoxNDk2MDkxOTY0MDAwfQ.Fijyp8y6Usz5mQ19PVh_SAcjtPANmjIXn3McMyklS4k'; // Old access token
    public $access_token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJ3YVJncWpuZVJUeUloQ2RuUmhWbDhBIiwiZXhwIjo0MDc5MDQ4NzgxMDAwfQ.fnQie6TWV4W1VOfDIpCrqVFNrcLvsrz11GV5x3sDP38'; //(Chess base account access token)
       
     
    public $MERCHANT_KEY = "gtKFFx";
    public $salt = "eCwWELxi";
    public $SUCCESS = 1;
    public $FAILURE = 0;
   // public $payu_server_url = "https://sandboxsecure.payu.in/";//https://secure.payu.in/";
    public $payu_server_url = "https://secure.payu.in/";
    public $base_url = "https://skillgamesacademy.com";

    public $ADMIN_ROLE_ID = 1;
    public $LEARNER_ROLE_ID = 2;
    public $GUEST_ROLE_ID = 3;
    public $TRAINER_ROLE_ID = 4;
    public $ORGANIZATION_ROLE_ID = 5;

    public function __construct() {
        header("Access-Control-Allow-Origin: *");
        date_default_timezone_set("Asia/Calcutta");
    }

    /**
     * @Route("/ws/")
     * @Template()
     */
    public function indexAction() {
        return array();
    }

    public function requestAction($req, $allowed_method) {
        // allowed type 0 = allowed Both request 
        // allowed type 1 = allowed only GET  request
        // allowed type 2 = allowed only POST request
        //var_dump($req->getContent());
        //var_dump($req->get('param'));
        //var_dump(json_encode($req->request->all()));
        //$allowed_method =1 ;
        $request_param = '';
        switch ($req->getMethod()) {
            case 'GET':
                if ($allowed_method != 2) {
                    $request_param = $req->get('param');
                }
                break;
            case 'POST':
                if ($allowed_method != 1) {
                    $request_param = json_encode($req->request->all());
                    //$request_param = $req->getContent() ;
                }
                break;
            default :
                $request_param = array();
                break;
        }
        //var_dump(json_decode($request_param));
        //exit;
        if (!empty($request_param)) {
            //$param= json_decode(stripcslashes($request_param));		
            $param = json_decode($request_param);

            if (isset($param->token) && !empty($param->token)) {
                $bwid_token = $param->token;
                $obj_security = $this->getDoctrine()
                        ->getManager()
                        ->getRepository('AdminBundle:Bwidwssecurity')
                        ->findBy(array('token' => $bwid_token));

                if (empty($obj_security)) {

                    $this->token = 'token';
                    return $this->responseAction();
                } else {
                    $token_expire_time = '10 ';

                    $token_created = $obj_security[0]->getToken_created();
                    $expire_token_date = strtotime(date("Y-m-d h:i:s", strtotime($token_expire_time . "	minutes", $token_created)));
                    $current_time = strtotime(date("Y-m-d h:i:s"));

                    if ($current_time >= $expire_token_date) {
                        $this->token = 'token';
                        return $this->responseAction();
                    }
                }
            }

            if (JSON_ERROR_NONE == json_last_error_msg()) {
                // valid JSON
                return $param;
            }
        }
        return array();
    }

    public function responseAction() {

        if (isset($this->token) && !empty($this->token)) {
            $this->error = $this->token;
            $this->data = "";
        }

        $response = array(
            "title" => $this->title,
            "error" => $this->error,
            "error_msg" => $this->error_msg,
            "data" => $this->data,
        );


        $jsonResponse = new JsonResponse();
        $jsonResponse->setData($response);

        return $jsonResponse;
    }

    public function clean_data($data, $type) {
        if (!empty($data)) {
            switch ($type) {
                case 1:
                    // array
                    if (is_array($data)) {
                        return $data;
                    }
                    break;
                case 2:
                    // array list
                    if (is_array($data) && !empty($data)) {
                        return $data;
                    }
                    break;
            }
        }
        return null;
    }

    public function rand_string($length) {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

        $size = strlen($chars);
        $str = "";
        for ($i = 0; $i < $length; $i++) {
            $str .= $chars[rand(0, $size - 1)];
        }

        return $str;
    }

    /**
     * use to validate request param
     * @param array $param
     * 
     * @return boolean
     */
    public function validateData($param) {

        if (count($this->validateRule)) {

            foreach ($this->validateRule as $value) {
                switch ($value['rule']) {
                    case 'NOTNULL' :
                        foreach ($value['field'] as $field) {

                            if (!isset($param->$field) && empty($param->$field)) {
                                // For error tracking
                                $this->error_msg = "Missing field : " . $field;

                                return false;
                            }
                        }

                        break;
                }
            }
        } else {
            $this->error = "Not Initialize";
            return false;
        }
        return true;
    }

    public function getdata($table, $condition) {
        $doc = $this->getDoctrine()->getManager()
                ->getRepository("MainAdminBundle:" . $table)
                ->findBy($condition);
        return $doc;
    }

    public function getonedata($table, $condition) {
        $doc = $this->getDoctrine()->getManager()
                ->getRepository("MainAdminBundle:" . $table)
                ->findOneBy($condition);
        return $doc;
    }

    public function firequery($query) {
        $em = $this->getDoctrine()->getManager()->getConnection()->prepare($query);
        $em->execute();
        $data = $em->fetchAll();
        return $data;
    }

    public function send_notification($registration_ids, $title, $message, $provider, $app_id, $domain_id, $tablename, $tabledataid) {

        /*
          $app_id= CUST // Customer App
          $app_id = DEL // Delivery App
          $domain_id = domain_code
         */

        switch ($provider) {
            case 1:
                $result = "FALSE";
                $development = false;
                $apns_url = NULL; // Set Later
                $pathCk = NULL; // Set Later
                $apns_port = 2195;


                $app_type_master = $this->getDoctrine()
                        ->getManager()
                        ->getRepository('AdminBundle:Apptypemaster')
                        ->findOneBy(array('is_deleted' => 0, 'app_type_code' => $app_id));

                if (!empty($app_type_master)) {
                    $app_details = $this->getDoctrine()
                            ->getManager()
                            ->getRepository('AdminBundle:Appdetails')
                            ->findOneBy(array('is_deleted' => 0, 'app_type_id' => $app_type_master->getApp_type_id(), "domain_id" => $domain_id, "status" => 'active'));

                    if (!empty($app_details)) {
                        if ($development) {

                            if (!empty($app_details->getApp_apns_certificate_development()) && $app_details->getApp_apns_certificate_development() != "" && !empty($app_details->getApp_apns_certificate_development_password()) && $app_details->getApp_apns_certificate_development_password() != "") {

                                $apns_url = 'gateway.sandbox.push.apple.com';

                                $pathCk = $this->container->get('kernel')->locateResource('@WSBundle/Controller/' . $app_details->getApp_apns_certificate_development());
                                $passphrase = $app_details->getApp_apns_certificate_development_password();
                            }
                        } else {
                            if (!empty($app_details->getApp_apns_certificate_production()) && $app_details->getApp_apns_certificate_production() != "" && !empty($app_details->getApp_apns_certificate_production_password()) && $app_details->getApp_apns_certificate_production_password() != "") {
                                $apns_url = 'gateway.push.apple.com';

                                $pathCk = $this->container->get('kernel')->locateResource('@WSBundle/Controller/' . $app_details->getApp_apns_certificate_production());
                                $passphrase = $app_details->getApp_apns_certificate_production_password();
                            }
                        }
                    }
                }

                if (!empty($apns_url) && $apns_url != "") {
                    $ctx = stream_context_create();
                    stream_context_set_option($ctx, 'ssl', 'local_cert', $pathCk);
                    stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

                    $apns = stream_socket_client(
                            'ssl://' . $apns_url . ':' . $apns_port, $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);

                    $data = json_decode($message);

                    $payload['aps'] = array(
                        'alert' => $data->detail,
                        'badge' => 1,
                        'sound' => 'default',
                        'response' => $data->response
                    );

                    $payload['job_id'] = $data->code;

                    // START LOOP
                    if ($registration_ids != "ALL") {
                        unset($where);
                        if (is_array($registration_ids)) {
                            $registration_ids = implode("','", $registration_ids);
                        }

                        $em = $this->getDoctrine()->getManager();

                        $connection = $em->getConnection();
                        $apns_user = $connection->prepare("SELECT * FROM apns_user WHERE apns_regid in ('" . $registration_ids . "') and is_deleted=0");

                        $apns_user->execute();
                        $apns_user_list = $apns_user->fetchAll();

                        // specific user 

                        $em = $this->getDoctrine()->getManager();
                        $connection = $em->getConnection();
                        $statement = $connection->prepare("UPDATE apns_user SET badge=(badge+1) WHERE apns_regid in ('" . $registration_ids . "') and is_deleted=0");
                        $statement->execute();
                    } else {
                        // All user
                        $em = $this->getDoctrine()->getManager();
                        $connection = $em->getConnection();
                        $statement = $connection->prepare("UPDATE apns_user SET badge = (badge+1) WHERE  is_deleted=0");
                        $statement->execute();
                    }

                    foreach (array_slice($apns_user_list, 0) as $key => $val) {
                        $device = $val['apns_regid'];

                        $final_payload = json_encode($payload);

                        $apnsMessage = chr(0) . pack('n', 32) . pack('H*', str_replace(' ', '', $device)) . pack('n', strlen($final_payload)) . $final_payload;

                        $result = fwrite($apns, $apnsMessage);
                        //var_dump($result);exit;
                        unset($apnsMessage);
                    }

                    // END LOOP

                    if (!$apns) {
                        $result = "Failed to connect : $error $errorString " . PHP_EOL;
                    }

                    fclose($apns);
                }

                break;

            case 2:
                $result = FALSE;
                $title_name = $title;
                $data = array("title" => $title_name, "message" => $message);
                $URL = 'https://android.googleapis.com/gcm/send';

                $fields = array(
                    'registration_ids' => $registration_ids,
                    'data' => $data,
                );

                $app_type_master = $this->getDoctrine()
                        ->getManager()
                        ->getRepository('AdminBundle:Apptypemaster')
                        ->findOneBy(array('is_deleted' => 0, 'app_type_code' => $app_id));
                if (!empty($app_type_master)) {
                    $app_details = $this->getDoctrine()
                            ->getManager()
                            ->getRepository('AdminBundle:Appdetails')
                            ->findOneBy(array('is_deleted' => 0, 'app_type_id' => $app_type_master->getApp_type_id(), "domain_id" => $domain_id, "status" => 'active'));
                    if (!empty($app_details)) {
                        /* if($app_id == '1')
                          {
                          // Customer App
                          $headers = array(
                          'Authorization: key=#',
                          'Content-Type: application/json'
                          );
                          }
                          elseif($app_id == '2')
                          {

                          // Delivery App
                          $headers = array(
                          'Authorization: key=#',
                          'Content-Type: application/json'
                          );
                          } */
                        if (!empty($app_details->getApp_gcm_key()) && $app_details->getApp_gcm_key() != "") {
                            $headers = array(
                                'Authorization: key=' . $app_details->getApp_gcm_key(),
                                'Content-Type: application/json'
                            );

                            $data1 = json_decode($message);
                            $response = "";
                            if (isset($data1->response) && !empty($data1->response)) {
                                $response = print_r($data1->response, 1);
                            } else {
                                $response = "";
                            }

                            $code = "";
                            if (isset($data1->code) && !empty($data1->code)) {
                                $code = $data1->code;
                            } else {
                                $code = "";
                            }

                            $registration_ids_array = $registration_ids;
                            if (is_array($registration_ids)) {
                                $registration_ids = implode("','", $registration_ids);
                            } else {
                                $registration_ids = "";
                            }

                            $detail = "";
                            if (isset($data1->detail) && !empty($data1->detail)) {
                                $detail = $data1->detail;
                            } else {
                                $detail = "";
                            }
                            $em = $this->getDoctrine()->getManager();
                            if (!empty($registration_ids_array)) {
                                foreach ($registration_ids_array as $val) {

                                    $apppushnotificationmaster = new Apppushnotificationmaster();
                                    $apppushnotificationmaster->setDevice_name('android');
                                    $apppushnotificationmaster->setApp_id($app_id);
                                    $apppushnotificationmaster->setDomain_id($domain_id);
                                    $apppushnotificationmaster->setDevice_token($val);

                                    $connection = $em->getConnection();
                                    $gcm = $connection->prepare("SELECT * FROM gcm_user WHERE gcm_regid = '" . $val . "' and is_deleted=0 ORDER BY gcm_user_id DESC");
                                    $gcm->execute();
                                    $gcm_user = $gcm->fetchAll();
                                    if (!empty($gcm_user)) {
                                        $user_id = $gcm_user[0]['user_id'];
                                        $device_id = $gcm_user[0]['device_id'];

                                        $em = $this->getDoctrine()->getManager();
                                        $user_setting = $em->getRepository('AdminBundle:Usersetting')->findOneBy(array("user_id" => $user_id, "is_deleted" => 0));

                                        $value = json_decode($user_setting->getSetting_value(), true);

                                        $lang_id = $value['language'];

                                        $apppushnotificationmaster->setUser_id($user_id);
                                        $apppushnotificationmaster->setLanguage_id($lang_id);
                                        $apppushnotificationmaster->setDevice_id($device_id);
                                    }

                                    $apppushnotificationmaster->setData($detail);
                                    $apppushnotificationmaster->setCode($code);
                                    $apppushnotificationmaster->setTable_name($tablename);
                                    $apppushnotificationmaster->setTable_id($tabledataid);
                                    $apppushnotificationmaster->setResponse($response);
                                    $apppushnotificationmaster->setDatetime(date("Y-m-d H:i:s"));
                                    $apppushnotificationmaster->setIs_deleted(0);
                                    $em = $this->getDoctrine()->getManager();
                                    $em->persist($apppushnotificationmaster);
                                    $em->flush();
                                }
                            }
                            // Open connection
                            $ch = curl_init();

                            // Set the url, number of POST vars, POST data
                            curl_setopt($ch, CURLOPT_URL, $URL);
                            curl_setopt($ch, CURLOPT_POST, true);
                            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                            // Disabling SSL Certificate support temporarly
                            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

                            // Execute post
                            $result = curl_exec($ch);

                            if ($result === FALSE) {
                                die('Curl failed: ' . curl_error($ch));
                            }
                            // Close connection
                            curl_close($ch);
                        }
                    }
                }

                break;
        }

        return $result;
    }

    /*
      get GCM user device / @param int $user_id / @return mixed
     */

    public function find_gcm_regid($user_id) {

        if (is_array($user_id)) {
            $user_id = implode("','", $user_id);
        }

        $em = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();
        $gcm_user = $connection->prepare("SELECT * FROM gcm_user WHERE user_id in ('" . $user_id . "') and gcm_regid NOT LIKE '' and user_type = 'user' and is_deleted = 0");
        $gcm_user->execute();
        $gcm_user_list = $gcm_user->fetchAll();

        if (count($gcm_user_list) > 0) {
            $reg_ids = array_map(function($sub) {
                return $sub['gcm_regid'];
            }, $gcm_user_list);
            return $reg_ids;
        }
        return false;
    }

    /*
      get APNS user device / @param int $user_id / @return mixed
     */

    public function find_apns_regid($user_id) {

        if (is_array($user_id)) {
            $user_id = implode("','", $user_id);
        }

        $em = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();
        $apns_user = $connection->prepare("SELECT * FROM apns_user WHERE user_id in ('" . $user_id . "') and apns_regid NOT LIKE '(null)' and apns_regid NOT LIKE '' and user_type = 'user' and is_deleted=0");
        $apns_user->execute();
        $apns_user_list = $apns_user->fetchAll();


        if (count($apns_user_list) > 0) {
            $reg_ids = array_map(function($sub) {
                return $sub['apns_regid'];
            }, $apns_user_list);
            return $reg_ids;
        }
        return false;
    }

    // Category List

    function get_hirerachy($lang_id, $parent_hieraerchy_id, $current_category_id, $domain_id) {
        $child_data = "";
        /* $domain_id = $this->get('session')->get('domain_id'); */

        $single_category = $this->getDoctrine()
                ->getManager()
                ->getRepository('AdminBundle:Categorymaster')
                ->findOneBy(array('is_deleted' => 0, 'main_category_id' => $current_category_id, 'domain_id' => $domain_id, "language_id" => $lang_id));

        /* 		var_dump($single_category);	   */
        $all_sub_category = $this->getDoctrine()
                ->getManager()
                ->getRepository('AdminBundle:Categorymaster')
                ->findBy(array('is_deleted' => 0, 'parent_category_id' => $single_category->getMain_category_id(), 'domain_id' => $domain_id, "language_id" => $lang_id));

        if (count($all_sub_category) == 0) {
            if (!empty($single_category)) {
                $parent_category_name = $single_category->getCategory_name();
                $image = "";
                $category_image_id = 0;
                if (!empty($single_category->getCategory_image_id()) && $single_category->getCategory_image_id() != 0) {

                    $media_library_master = $this->getDoctrine()
                            ->getManager()
                            ->getRepository('AdminBundle:Mediamaster')
                            ->findOneBy(array('is_deleted' => 0, 'media_master_id' => $single_category->getCategory_image_id()));

                    if (!empty($media_library_master)) {
                        $category_image_id = $single_category->getCategory_image_id();
                        $image = $this->container->getParameter('live_path') . $media_library_master->getMedia_location() . '/' . $media_library_master->getMedia_name();
                    }
                }
            }

            $data = array(
                "category_master_id" => $single_category->getCategory_master_id(),
                "category_name" => $single_category->getCategory_name(),
                "parent_category_id" => $single_category->getParent_category_id(),
                "parent_category_name" => $parent_category_name,
                "category_description" => strip_tags($single_category->getCategory_description()),
                "main_category_id" => $single_category->getMain_category_id(),
                "category_image_id" => $category_image_id,
                "category_image" => $image,
                "language_id" => $single_category->getLanguage_id(),
                "child_data" => null,
            );
        } else {
            if (!empty($single_category)) {
                $parent_category_name = $single_category->getCategory_name();
                $image = "";
                $category_image_id = 0;
                if (!empty($single_category->getCategory_image_id()) && $single_category->getCategory_image_id() != 0) {

                    $media_library_master = $this->getDoctrine()
                            ->getManager()
                            ->getRepository('AdminBundle:Mediamaster')
                            ->findOneBy(array('is_deleted' => 0, 'media_master_id' => $single_category->getCategory_image_id()));

                    if (!empty($media_library_master)) {
                        $category_image_id = $single_category->getCategory_image_id();
                        $image = $this->container->getParameter('live_path') . $media_library_master->getMedia_location() . '/' . $media_library_master->getMedia_name();
                    }
                }
            }
            $data_temp[] = array(
                "category_master_id" => $single_category->getCategory_master_id(),
                "category_name" => $single_category->getCategory_name(),
                "parent_category_id" => $single_category->getParent_category_id(),
                "parent_category_name" => $parent_category_name,
                "category_description" => strip_tags($single_category->getCategory_description()),
                "main_category_id" => $single_category->getMain_category_id(),
                "category_image_id" => $category_image_id,
                "category_image" => $image,
                "language_id" => $single_category->getLanguage_id(),
                "classname" => "cat " . $single_category->getMain_category_id()
            );

            $data_child = '';
            if (count($all_sub_category) > 0) {
                foreach (array_slice($all_sub_category, 0) as $lkey => $lval) {
                    $parent_category_name = 'No Parent ';

                    if ($lval->getParent_category_id() == 0) {
                        
                    } else {

                        $data_child[] = $this->get_hirerachy($lang_id, $lval->getParent_category_id(), $lval->getMain_category_id(), $domain_id);
                    }
                }
            }

            $data = array(
                "category_master_id" => $data_temp[0]['category_master_id'],
                "category_name" => $data_temp[0]['category_name'],
                "parent_category_id" => $data_temp[0]['parent_category_id'],
                "parent_category_name" => $data_temp[0]['parent_category_name'],
                "category_description" => $data_temp[0]['category_description'],
                "main_category_id" => $data_temp[0]['main_category_id'],
                "category_image_id" => $data_temp[0]['category_image_id'],
                "category_image" => $data_temp[0]['category_image'],
                "language_id" => $data_temp[0]['language_id'],
                "child_data" => $data_child,
                "classname" => $data_temp[0]['classname']
            );
        }
        return $data;
    }

    // Category List

    function get_hirerachy_id($lang_id, $parent_hieraerchy_id, $current_category_id, $domain_id) {
        $child_data = "";
        static $cate_arr_sub = '';
        /* $domain_id = $this->get('session')->get('domain_id'); */

        $single_category = $this->getDoctrine()
                ->getManager()
                ->getRepository('AdminBundle:Categorymaster')
                ->findOneBy(array('is_deleted' => 0, 'main_category_id' => $current_category_id, 'domain_id' => $domain_id, "language_id" => $lang_id));

        $all_sub_category = $this->getDoctrine()
                ->getManager()
                ->getRepository('AdminBundle:Categorymaster')
                ->findBy(array('is_deleted' => 0, 'parent_category_id' => $single_category->getMain_category_id(), 'domain_id' => $domain_id, "language_id" => $lang_id));

        if (count($all_sub_category) == 0) {
            if (!empty($single_category)) {
                $parent_category_name = $single_category->getCategory_name();
                $cate_arr_sub .= "," . $single_category->getMain_category_id();
            }

            $data = array(
                "child_data" => null,
                "cate_arr_sub" => $cate_arr_sub
            );
        } else {
            if (!empty($single_category)) {
                $parent_category_name = $single_category->getCategory_name();
            }
            $cate_arr_sub .= "," . $single_category->getMain_category_id();
            $data_temp[] = array(
                "cate_arr_sub" => $cate_arr_sub
            );

            $data_child = '';
            if (count($all_sub_category) > 0) {
                foreach (array_slice($all_sub_category, 0) as $lkey => $lval) {
                    $parent_category_name = 'No Parent ';

                    if ($lval->getParent_category_id() == 0) {
                        
                    } else {

                        $data_child[] = $this->get_hirerachy_id($lang_id, $lval->getParent_category_id(), $lval->getMain_category_id(), $domain_id);
                    }
                }
            }

            $data = array(
                "child_data" => $data_child,
                "cate_arr_sub" => $data_temp[0]['cate_arr_sub']
            );
        }
        return $cate_arr_sub;
    }

    public function getaddressAction($address_id, $lang_id) {
        $em = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();
        $statement = $connection->prepare("SELECT address_master.*,city_master.city_name,area_master.area_name FROM address_master JOIN city_master ON address_master.city_id = city_master.main_city_id JOIN area_master ON address_master.area_id = area_master.main_area_id WHERE address_master.main_address_id = " . $address_id . " AND address_master.language_id = " . $lang_id . " AND city_master.language_id = " . $lang_id . " AND area_master.language_id = " . $lang_id . " AND address_master.is_deleted = 0");
        $statement->execute();
        $address_info = $statement->fetchAll();

        if (!empty($address_info) && $address_id != 0) {
            $lat = $lng = NULL;

            if (!empty($address_info[0]['lat'])) {
                $lat = $address_info[0]['lat'];
            }

            if (!empty($address_info[0]['lng'])) {
                $lng = $address_info[0]['lng'];
            }

            $address = array(
                "address" => $address_info[0]['address_name'],
                "base_address_type" => $address_info[0]['base_address_type'],
                "address_type" => $address_info[0]['address_type'],
                "city_name" => $address_info[0]['city_name'],
                "city_id" => $address_info[0]['city_id'],
                "area_name" => $address_info[0]['area_name'],
                "street" => $address_info[0]['street'],
                "flate_house_number" => $address_info[0]['flate_house_number'],
                "society_building_name" => $address_info[0]['society_building_name'],
                "landmark" => $address_info[0]['landmark'],
                "pincode" => $address_info[0]['pincode'],
                "gmap_link" => $address_info[0]['gmap_link'],
                "lat" => $lat,
                "lng" => $lng,
            );
        } else {
            /* $address = array(
              "address"=>NULL,
              "base_address_type"=>NULL,
              "address_type"=>NULL,
              "city_name"=>NULL,
              "area_name"=>NULL,
              "street"=>NULL,
              "flate_house_number"=>NULL,
              "society_building_name"=>NULL,
              "landmark"=>NULL,
              "pincode"=>NULL,
              "gmap_link"=>NULL,
              "lat"=>NULL,
              "lng"=>NULL,
              ); */
            $address = array();
        }
        return $address;
    }

    function array_push_assoc($array, $key, $value) {
        $array[$key] = $value;
        return $array;
    }

    public function getimage($media_master_id) {
        $live_path = $this->container->getParameter('live_path');
        $em = $this->getDoctrine()->getManager();
        $media_info = $em->getRepository("AdminBundle:Mediamaster")
                ->findOneBy(array("mediamaster_id" => $media_master_id));

        if (!empty($media_info)) {
            $image_url = $live_path . $media_info->getMedia_location() . "/" . $media_info->getMedia_name();
        } else {
            $image_url = "";
        }
        return $image_url;
    }

    public function getimageData($media_master_id) {
        $live_path = $this->container->getParameter('live_path');

        $media_info = $this->getDoctrine()
                ->getManager()
                ->getRepository("AdminBundle:Mediamaster")
                ->findOneBy(array("media_master_id" => $media_master_id, "is_deleted" => 0));

        if (!empty($media_info)) {
            $image_url = $live_path . $media_info->getMedia_location() . "/" . $media_info->getMedia_name();
        } else {
            $image_url = "";
        }
        return array($image_url, $media_info->getMedia_type_id());
    }

    function mediauploadAction($file, $tmpname, $path, $upload_dir, $mediatype_id) {

        $clean_image = preg_replace('/\s+/', '', $file);
        $logo_name = date('Y_m_d_H_i_s') . '_' . $clean_image;

        if (!file_exists($upload_dir)) {
            mkdir($upload_dir, 0777);
        }
        //logo upload check
        if (move_uploaded_file($tmpname, $upload_dir . $logo_name)) {
            $Mediamaster = new mediamaster();

            //$Mediamaster->setMedia_type_id($mediatype_id);
            $Mediamaster->setMedia_title($logo_name);
            $Mediamaster->setMedia_location($path);
            $Mediamaster->setMedia_name($logo_name);
            $Mediamaster->setCreated_datetime(date('Y-m-d H:i:s'));
            $Mediamaster->setIs_deleted(0);

            $dm = $this->getDoctrine()->getManager();
            $dm->persist($Mediamaster);
            $dm->flush();
            $media_library_master_id = $Mediamaster->getMediamaster_id();
            return $media_library_master_id;
        } else {
            return FALSE;
        }
    }

    public function keyEncryptionAction($string) {
        if ($string != "" && $string != NULL && !empty($string) && ctype_space($string) == false) {
            $key = $this->container->getParameter('key');
            $res = '';
            for ($i = 0; $i < strlen($string); $i++) {
                $c = ord(substr($string, $i));

                $c += ord(substr($key, (($i + 1) % strlen($key))));
                $res .= chr($c & 0xFF);
            }
            return base64_encode($res);
        }
        return "";
    }

    public function keyDecryptionAction($string) {
        if ($string != "" && $string != NULL && !empty($string) && ctype_space($string) == false) {
            $key = $this->container->getParameter('key');

            $res = '';
            $string = base64_decode($string);
            for ($i = 0; $i < strlen($string); $i++) {
                $c = ord(substr($string, $i));

                $c -= ord(substr($key, (($i + 1) % strlen($key))));
                $res .= chr(abs($c) & 0xFF);
            }
            return $res;
        }
        return "";
    }

    public function generate_token($device_id) {
        $token = $device_id . 'bwizard' . time();
        $new_token = md5($token);
        return $new_token;
    }

    public function getprofilename($session_code) {
        $query = "SELECT option_profile_id FROM `quiz_answer_user` WHERE session = " . $session_code;
        $em = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();
        $statement = $connection->prepare($query);
        $statement->execute();
        $quizanswertotal = $statement->fetchAll();
        $length = sizeof($quizanswertotal);
        $query = "SELECT COUNT(quiz_answer_user_id) as cnt ,option_profile_id FROM `quiz_answer_user` WHERE session = " . $session_code . " GROUP by option_profile_id ORDER by cnt DESC";
        $em = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();
        $statement = $connection->prepare($query);
        $statement->execute();
        $quizanswer = $statement->fetchAll();
        $profilelist = null;
        foreach ($quizanswer as $value) {
            $per = ($value['cnt'] * 100) / $length;
            $query = "SELECT * FROM `profilemaster` where is_deleted = 0 and profilemaster_id=" . $value["option_profile_id"];
            $statement = $connection->prepare($query);
            $statement->execute();
            $profile = $statement->fetchAll();
            $profilelist[] = array(
                "profile_id" => $profile[0]["profilemaster_id"],
                "profile_name" => $profile[0]["profile_name"],
                "media_image" => $this->getimage($profile[0]["profile_img"]),
                "description" => $profile[0]["profile_description"],
                "per" => $per
            );
        }



        return $profilelist;
    }

    /**
     * @Route("/getmedia/{media_library_master_id}/{live_path}")
     */
    public function getmedia1Action($media_library_master_id, $live_path) {

        $media_library = $this->getDoctrine()->getManager()
                ->getRepository('AdminBundle:mediamaster')
                ->findOneBy(array('id' => $media_library_master_id));

        if ($media_library) {
            return ($live_path . $media_library->getMedia_location() . "/" . $media_library->getMedia_name());
        } else {
            return false;
        }
    }

    protected $payu_params = [];

    private function check_params() {
        if (empty($this->payu_params['key']))
            return $this->error('key');
        if (empty($this->payu_params['txnid']))
            return $this->error('txnid');
        if (empty($this->payu_params['amount']))
            return $this->error('amount');
        if (empty($this->payu_params['firstname']))
            return $this->error('firstname');
        if (empty($this->payu_params['email']))
            return $this->error('email');
        if (empty($this->payu_params['phone']))
            return $this->error('phone');
        if (empty($this->payu_params['productinfo']))
            return $this->error('productinfo');
        if (empty($this->payu_params['surl']))
            return $this->error('surl');
        if (empty($this->payu_params['furl']))
            return $this->error('furl');

        return true;
    }

    public static function get_hash($params, $salt) {
        $posted = array();

        if (!empty($params))
            foreach ($params as $key => $value)
                $posted[$key] = htmlentities($value, ENT_QUOTES);

        $hash_sequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";

        $hash_vars_seq = explode('|', $hash_sequence);
        $hash_string = null;

        foreach ($hash_vars_seq as $hash_var) {
            $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
            $hash_string .= '|';
        }


        $hash_string .= $salt;
        return strtolower(hash('sha512', $hash_string));
    }

    public static function curl_call($url, $data) {

        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_POST => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_USERAGENT => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36',
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0));

        $o = curl_exec($ch);

        echo '<pre>';
        print_r($url);
        exit;

        if (curl_errno($ch)) {
            $c_error = curl_error($ch);

            if (empty($c_error))
                $c_error = 'Server Error';

            return array('curl_status' => 0, 'error' => $c_error);
        }

        $o = trim($o);
        return array('curl_status' => 1, 'result' => $o);
    }

    public function pay_page($params, $salt) {

        if (is_array($params)) {
            foreach ($params as $key => $value) {
                $this->payu_params[$key] = $value;
            }
        }

        $result = $this->pay($params, $salt);

        return $result;
    }

    public function pay($params = null) {

        $this->payu_params['hash'] = $this->get_hash($this->payu_params, $this->salt);
        $result = $this->curl_call($this->payu_server_url . '_payment?type=merchant_txn', http_build_query($this->payu_params));

        $transaction_id = ($result['curl_status'] === $this->SUCCESS) ? $result['result'] : null;
       
        if (empty($transaction_id)  ) {
            return array(
                'status' => $this->FAILURE,
                'data' => $result['error']
            );
        }

        return array(
            'status' => $this->SUCCESS,
            'data' => $this->payu_server_url . '_payment_options?mihpayid=' . $transaction_id
        );
    }

}
