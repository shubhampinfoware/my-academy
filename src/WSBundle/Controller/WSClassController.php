<?php

namespace WSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AdminBundle\Controller\CouponController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use AdminBundle\Entity\Userinfo;
use AdminBundle\Entity\Rolemaster;
use AdminBundle\Entity\Gcmuser;
use AdminBundle\Entity\Apnsuser;
use AdminBundle\Entity\users;
use AdminBundle\Entity\user_meeting_url;
use AdminBundle\Entity\coursemaster;
use AdminBundle\Entity\Tournament;


class WSClassController extends WSBaseController {

    /**
     * @Route("/ws/class/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function classAction(Request $request) {

        $this->title = "course";
        $param = $this->requestAction($request, 0);

        $em = $this->getDoctrine()->getManager();
        $this->validateRule = array(
            array(
                'rule' => 'NOTNULL',
                'field' => array('user_id'),
            ),
        );
        if ($this->validateData($param)) {
            $newdata = $admincreated = null;
            $live_tag = '';
            $live_tag_id = '5ca9fb219bc8b76ede5b7933';
            if (isset($param->live_tag)) {
                $live_tag = $param->live_tag;
            }
            if (isset($param->user_id)) {
                $user_id = $param->user_id;
                $usermaster = $userMaster = $em->getRepository("AdminBundle:usermaster")
                        ->findOneBy(array(
                    "id" => $user_id,
                    "is_deleted" => "0",
                        )
                );
                if (empty($usermaster)) {
                    $this->error = "UNF";
                    $this->data = false;
                    return $this->responseAction();
                }
                $usertrainerrelation = $em->getRepository("AdminBundle:usertrainerrelation")
                        ->findOneBy(array(
                    "user_id" => $user_id,
                        )
                );
                if ($usertrainerrelation->getIs_approved() == "1") {

                    $role_id = $this->TRAINER_ROLE_ID;
                    $role = "traniner";
                    $coursetrainerone = $em->getRepository('AdminBundle:Coursetrainermaster')
                            ->findBy(array(
                        'is_deleted' => "0",
                        'usermaster_id' => $user_id
                    ));

                    $courseidarray = [];

                    if (!empty($coursetrainerone)) {
                        foreach ($coursetrainerone as $key => $value3) {

                            $courseInfo = $this->getDoctrine()->getManager()->getRepository('AdminBundle:coursemaster')->find($value3->getCoursemasterId());
                            if (!empty($courseInfo)) {
                                $courseidarray[] = $value3->getCoursemasterId();
                            }
                        }
                    }

                    $all_assigned_class_master = $em->getRepository('AdminBundle:course_unit')->findBy(array('is_deleted' => "0", 'lock_feature' => $user_id));
                    if ($all_assigned_class_master) {
                        foreach ($all_assigned_class_master as $akey => $aval) {
                            if (!in_array($aval->getCoursemasterId(), $courseidarray)) {
                                $courseidarray[] = $aval->getCoursemasterId();
                            }
                        }
                    }
                } else {

                    $role_id = $this->LEARNER_ROLE_ID;
                    $role = "learner";
                    $user_package_relation = $this->getDoctrine()->getManager()->getRepository('AdminBundle:user_package_relation')->findBy(array(
                        "user_id" => $user_id,
                        "is_deleted" => "0"
                    ));
                    $courseidarray = [];

                    foreach ($user_package_relation as $key => $value1) {
                        $subscription_master = $this->getDoctrine()->getManager()->getRepository('AdminBundle:subscription_master')->find($value1->getPackageId());
                        if (!empty($subscription_master)) {
                            $courseids = $subscription_master->getCourseId();
                            foreach ($courseids as $key => $ids) {
                                if (!in_array($ids, $courseidarray)) {
                                    $courseidarray[] = $ids;
                                }
                            }
                        }
                    }
                }
            }

            $course_type = '';
            $newIDS = [];
            $repository = $this->getDoctrine()->getManager()->getRepository('AdminBundle:course_unit');
            if (!empty($courseidarray)) {
                foreach ($courseidarray as $key => $coursemaster_id) {
                    if (isset($param->course_type)) {
                        $course_type = "" . $param->course_type;
                        $coursemaster = $repository->createQueryBuilder()
                                        //                                       ->field('coursemaster_id')
                                        //                                      ->equals((string) $coursemaster_id)
                                        ->field('youtubelink')
                                        ->notEqual("")
                                        ->field('course_type')
                                        ->equals($course_type)
                                        // ->sort('id', 'desc')
                                        ->getQuery()->execute()->toArray();
                    } else {
                        $coursemaster = $repository->createQueryBuilder()
//                                        ->field('coursemaster_id')
//                                        ->equals($coursemaster_id)
//                                        ->field('youtubelink')
//                                        ->notEqual("")
                                        ->sort('id', 'desc')
                                        ->getQuery()->execute()->toArray();
                    }

                    $flag = false;
                    $publishedAt = '';

                    if (!empty($coursemaster)) {
                        foreach ($coursemaster as $value) {
                        //    var_dump($value);exit;
                            $okFlag = true;
                            if ($live_tag != '') {
                                $tags = explode(",", $value->getTag_id());
                                if (!empty($tags)) {
                                    if (in_array($live_tag_id, $tags)) {
                                        
                                    } else {
                                        $okFlag = false;
                                    }
                                } else {
                                    $okFlag = false;
                                }
                            }
                            if ($okFlag) {
                                $url = $value->getYoutubelink();
                                $img_url = '';
                                $query_v = '';
                                $views = 0;
                                $time = "";
                                $course_id_arr = [];
                                $course_name = '';
                                $course_unit_relation_list = $em
                                        ->getRepository('AdminBundle:course_unit_relation')
                                        ->findBy(array('is_deleted' => "0", 'course_unit_id' => $value->getId(), 'relation_with' => 'course'));
                                if ($course_unit_relation_list) {
                                    foreach ($course_unit_relation_list as $delkey => $delval) {
                                        if (!in_array($delval->getRelation_object_id(), $course_id_arr)) {
                                            $course_id_arr[] = $delval->getRelation_object_id();
                                        }
                                    }
                                }

                                if ($course_id_arr) {
                                    foreach ($course_id_arr as $ckey => $cval) {
                                        $courseC = $em->getRepository('AdminBundle:coursemaster')->find($cval);
                                        if (!empty($courseC)) {
                                            if ($course_name == "") {
                                                $course_name = $courseC->getCourseName();
                                            } else {
                                                $course_name = $course_name . " , " . $courseC->getCourseName();
                                            }
                                        }
                                    }
                                }

                                $course_type = $value->getCourseType();
                                if (!in_array($value->getId(), $newIDS)) {
                                    $newIDS[] = $value->getId();
                                    $course_Tags = $value->getTag_id();
                                    $course_tag_list = NULL;
                                    if (!empty($course_Tags) && $course_Tags != '' && $course_Tags != NULL) {
                                        $course_Tags = explode(",", $course_Tags);

                                        foreach ($course_Tags as $tagval) {
                                            $course_tag_info = $this->getDoctrine()->getManager()->getRepository('AdminBundle:course_unit_tag')->find($tagval);
                                            if ($course_tag_info) {
                                                $course_tag_list[] = array("tag_id" => $tagval, "tag_name" => $course_tag_info->getTag_name());
                                            }
                                        }
                                    }
                                    // get user wise url 
                                    $MeetingJoinUrl = '';
                                    $user_meeting_url = $this->getDoctrine()->getManager()->getRepository('AdminBundle:user_meeting_url')->findOneBy(array("is_deleted" => "0", "user_id" => $user_id, "course_unit_id" => $value->getId(), "meeting_id" => $value->getUnitCode()));
                                    if ($user_meeting_url) {
                                        $MeetingJoinUrl = $user_meeting_url->getMeeting_url();
                                    }
                                    $newdata[] = array(
                                        "main_id" => $value->getId(),
                                        "unit_code" => $value->getUnitCode(),
                                        "title" => $value->getUnitName(),
                                        "cover_image" => $this->getmedia1Action($value->getClass_cover_image(), $this->container->getParameter('live_path')),
                                        "youtubelink" => $MeetingJoinUrl,
                                        "another_link" => $url,
                                        "live_type" => $value->getPrerequisites(),
                                        //"img_url" => $img_url,
                                        "course_type" => $value->getCourseType(),
                                        "created_by" => $value->getCreatedBy(),
                                        "video_id" => $query_v, //
                                        // "views" => $views,
                                        // "category_name" => $category_name,
                                        //'flag' => $flag, //image_flag true
                                        "course_name" => $course_name,
                                        "course_id" => $coursemaster_id,
                                        "course_tags" => $course_tag_list,
                                            //'publishedat' => $publishedAt, // zoom
                                            // "time" => $time, // zoom
                                    );
                                }
                            }
                        }
                        $this->error = "SFD";
                    }


                    if (isset($param->user_id)) {

                        $user_id = $param->user_id;

                        $courseunit = $repository->createQueryBuilder()
                                        ->field('coursemaster_id')
                                        ->equals($coursemaster_id)
                                        ->field('youtubelink')
                                        ->equals("")
                                        ->field('course_type')
                                        ->equals($course_type)
                                        ->field('lock_feature') // as assignd user
                                        ->equals($user_id)
                                        ->sort('id', 'desc')
                                        ->getQuery()->execute()->toArray();
                        if (!empty($courseunit)) {
                            foreach ($courseunit as $key => $value) {
                                $category_id = $value->getCourseMediaId();
                                $category = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Categorymaster')->find($category_id);
                                $category_name = '';
                                if (!empty($category)) {
                                    $category_name = $category->getCategoryName();
                                }
                                /* $coursemaster = $this->getDoctrine()->getManager()->getRepository('AdminBundle:coursemaster')->find($coursemaster_id);

                                  $course_name ='';
                                  if(!empty($coursemaster)){
                                  $course_name = $coursemaster->getCourseName();
                                  } */
                                $course_id_arr = [];
                                $course_name = '';
                                $course_unit_relation_list = $em
                                        ->getRepository('AdminBundle:course_unit_relation')
                                        ->findBy(array('is_deleted' => "0", 'course_unit_id' => $value->getId(), 'relation_with' => 'course'));
                                if ($course_unit_relation_list) {
                                    foreach ($course_unit_relation_list as $delkey => $delval) {
                                        $course_id_arr[] = $delval->getRelation_object_id();
                                    }
                                }

                                if ($course_id_arr) {
                                    foreach ($course_id_arr as $ckey => $cval) {
                                        $courseC = $em->getRepository('AdminBundle:coursemaster')->find($cval);
                                        if (!empty($courseC)) {
                                            if ($course_name == "") {
                                                $course_name = $courseC->getCourseName();
                                            } else {
                                                $course_name = $course_name . " , " . $courseC->getCourseName();
                                            }
                                        }
                                    }
                                }

                                $admincreated[] = array(
                                    "main_id" => $value->getId(),
                                    "title" => $value->getUnitName(),
                                    "unit_code" => $value->getUnitCode(),
                                    "cover_image" => $this->getmedia1Action($value->getClass_cover_image(), $this->container->getParameter('live_path')),
                                    "course_type" => $value->getCourseType(),
                                    "course_name" => $course_name,
                                    "course_id" => $coursemaster_id
                                );
                            }
                        }
                    }
                }
            }

            $this->error = "SFD";
            $response["live_class"] = $newdata;
            $response["admin_created"] = $admincreated;
        } else {
            $this->error = "PIM";
        }

        if (empty($response)) {
            $response = false;
        }
        $this->data = $response;
        return $this->responseAction();
    }

    /**
     * @Route("/ws/classdetails/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function classdetailsAction(Request $request) {
        $this->title = "classdetails";
        $param = $this->requestAction($request, 0);

        $em = $this->getDoctrine()->getManager();
        $this->validateRule = array(
            array(
                'rule' => 'NOTNULL',
                'field' => array('main_id'),
            ),
        );
        if ($this->validateData($param)) {
            $main_id = $param->main_id;
            $user_id = 0;
            if (isset($param->user_id)) {
                $user_id = $param->user_id;
            }
            $course_unit = $this->getDoctrine()->getManager()->getRepository('AdminBundle:course_unit')->find($main_id);
            if (!empty($course_unit)) {
                $query_v = $img_url = $flag = $publishedAt = $url1 = '';
                $url = $course_unit->getYoutubelink();
                if (!empty($url)) {
                    if ($course_unit->getPrerequisites() == 'youtube') {
                        $parts = parse_url($url);
                        $id = 0 ;  $time = "";
                        if(isset($parts['query'])){
                            parse_str($parts['query'], $query);
                            $json = file_get_contents("https://www.googleapis.com/youtube/v3/videos?part=snippet%2CcontentDetails%2Cstatistics&id=" . $query['v'] . "&key=AIzaSyAS2T7d5SqW81JNqAmICfTtEVRNiOxtAYY");
                            $obj = json_decode($json);
                            $id = sizeof($obj->items);
                        }
                        $category_id = 17;

                        $publishedAt = date("d-M-Y");

                        if ($id > 0) {
                            $views = $obj->items[0]->statistics->viewCount;
                            $duration = $obj->items[0]->contentDetails->duration;
                            $data2 = substr($duration, 2);
                            $data3 = substr($data2, 0, strlen($data2) - 1);
                            $time = "";
                            if (strpos($data3, 'M') !== false) {
                                $array = explode("M", $data3);
                                if (strpos($array[0], 'H') !== false) {
                                    $hours = explode("H", $array[0]);
                                    $time = $hours[0] . ":" . $hours[1] . ":" . $array[1];
                                } else {
                                    $time = $array[0] . ":" . $array[1];
                                }
                            } else {
                                $time = $data3;
                            }
                            $category_id = $obj->items[0]->snippet->categoryId;
                            $publishedAt = date("d-M-Y", strtotime($obj->items[0]->snippet->publishedAt));
                            $flag = false;
                        } else {
                            $flag = true;
                        }
                        $course_type = $course_unit->getCourseType();
                        if (($time != "0" or $time != 0) and $course_type == "1") {
                            $class_master = $em->getRepository('AdminBundle:course_unit')
                                    ->findOneBy(array('is_deleted' => "0", 'id' => $main_id));
                            $class_master->setCourseType("0");
                            $em = $this->getDoctrine()->getManager();
                            $em->flush();
                            $course_type = "0";
                        }

                        $json = file_get_contents("https://www.googleapis.com/youtube/v3/videoCategories?part=snippet&id=" . $category_id . "&key=AIzaSyAS2T7d5SqW81JNqAmICfTtEVRNiOxtAYY");

                        $obj = json_decode($json);
                        $id = sizeof($obj->items);
                        if ($id > 0) {
                            $category_name = $obj->items[0]->snippet->title;
                        }
                        $img_url = $query_v = '';
                        if(isset($query['v'])){
                            $img_url = 'https://img.youtube.com/vi/' . $query['v'] . '/0.jpg';
                            $query_v = $query['v'];
                        }
                    }
                    elseif ($course_unit->getPrerequisites() == 'zoom') {
                        $flag = true;
                        $img_url = '';
                        $publishedAt = '';
                        $meeting_id = $course_unit->getUnitCode();
                        $calledFunction = 'meetings/' . $meeting_id;
                        $request_url = $this->api_url . $calledFunction . '?access_token=' . $this->access_token;
                        $handle = curl_init($request_url);
                        $headers = array();
                        $headers[] = 'Content-Type: application/json';
                        curl_setopt($handle, CURLOPT_HTTPHEADER, $headers);
                        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
                        $response_curl = curl_exec($handle);
                        $httpcode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
                        $resp = json_decode($response_curl);
                        curl_close($handle);
                        if ($httpcode == 200) {
                            $publishedAt = $resp->created_at;
                        }
                        $query_v = '';
                    }

                    $coursemaster_id = $course_unit->getCoursemasterId();
                    /* $coursemaster = $this->getDoctrine()->getManager()->getRepository('AdminBundle:coursemaster')->find($coursemaster_id);
                      $course_name = '';
                      if ($coursemaster) {
                      $course_name = $coursemaster->getCourseName();
                      } */

                    $course_id_arr = [];
                    $course_name = '';
                    $course_unit_relation_list = $em
                            ->getRepository('AdminBundle:course_unit_relation')
                            ->findBy(array('is_deleted' => "0", 'course_unit_id' => $main_id, 'relation_with' => 'course'));
                    if ($course_unit_relation_list) {
                        foreach ($course_unit_relation_list as $delkey => $delval) {
                            $course_id_arr[] = $delval->getRelation_object_id();
                        }
                    }

                    if ($course_id_arr) {
                        foreach ($course_id_arr as $ckey => $cval) {
                            $courseC = $em->getRepository('AdminBundle:coursemaster')->find($cval);
                            if (!empty($courseC)) {
                                if ($course_name == "") {
                                    $course_name = $courseC->getCourseName();
                                } else {
                                    $course_name = $course_name . " , " . $courseC->getCourseName();
                                }
                            }
                        }
                    }

                    $repository = $this->getDoctrine()->getManager()->getRepository('AdminBundle:course_unit');
                    $releatedunit = $repository->createQueryBuilder()
                                    ->field('coursemaster_id')
                                    ->equals($coursemaster_id)
                                    ->field('youtubelink')
                                    ->notEqual("")
                                    ->field('id')
                                    ->notEqual($course_unit->getId())
                                    ->sort('id', 'desc')
                                    ->limit(4)
                                    ->getQuery()->execute()->toArray();
                    $relateddata = null;
                    if (!empty($releatedunit)) {
                        foreach ($releatedunit as $value) {
                            $relatedurl = $value->getYoutubelink();
                            $relatedimg_url = '';
                            $relatedquery_v = '';
                            $relatedviews = 0;
                            $relatedpublishedAt = '';
                            $relatedtime = $category_name = $views = $course_type = $time = "";
                            if ($value->getPrerequisites() == 'youtube') {
                                $parts = parse_url($relatedurl);
                                $id = $category_id = 0 ;
                                if(isset($parts['query'])){
                                    parse_str($parts['query'], $relatedquery);
                                    $json = file_get_contents("https://www.googleapis.com/youtube/v3/videos?part=snippet%2CcontentDetails%2Cstatistics&id=" . $relatedquery['v'] . "&key=AIzaSyAS2T7d5SqW81JNqAmICfTtEVRNiOxtAYY");
                                    $obj = json_decode($json);
                                    $id = sizeof($obj->items);
                                    $category_id = 17;
                                }
                                $publishedAt = date("d-M-Y");

                                if ($id > 0) {
                                    $relatedviews = $obj->items[0]->statistics->viewCount;
                                    $duration = $obj->items[0]->contentDetails->duration;
                                    $data2 = substr($duration, 2);
                                    $data3 = substr($data2, 0, strlen($data2) - 1);
                                    $relatedtime = "";
                                    if (strpos($data3, 'M') !== false) {
                                        $array = explode("M", $data3);
                                        if (strpos($array[0], 'H') !== false) {
                                            $hours = explode("H", $array[0]);
                                            $relatedtime = $hours[0] . ":" . $hours[1] . ":" . $array[1];
                                        } else {
                                            $relatedtime = $array[0] . ":" . $array[1];
                                        }
                                    } else {
                                        $relatedtime = $data3;
                                    }
                                    $category_id = $obj->items[0]->snippet->categoryId;
                                    $relatedpublishedAt = date("d-M-Y", strtotime($obj->items[0]->snippet->publishedAt));
                                    $relatedflag = false;
                                } else {
                                    $relatedflag = true;
                                }
                                $course_type = $value->getCourseType();
                                if (($relatedtime != "0" or $relatedtime != 0) and $course_type == "1") {
                                    $class_master = $em->getRepository('AdminBundle:course_unit')
                                            ->findOneBy(array('is_deleted' => "0", 'id' => $value->getId()));
                                    $class_master->setCourseType("0");
                                    $em = $this->getDoctrine()->getManager();
                                    $em->flush();
                                    $course_type = "0";
                                }

                                $json = file_get_contents("https://www.googleapis.com/youtube/v3/videoCategories?part=snippet&id=" . $category_id . "&key=AIzaSyAS2T7d5SqW81JNqAmICfTtEVRNiOxtAYY");

                                $obj = json_decode($json);
                                $id = sizeof($obj->items);
                                if ($id > 0) {
                                    $category_name = $obj->items[0]->snippet->title;
                                }
                                $relatedimg_url = $relatedquery_v = '';
                                if(isset($relatedquery['v'])){
                                    $relatedimg_url = 'https://img.youtube.com/vi/' . $relatedquery['v'] . '/0.jpg';
                                    $relatedquery_v = $relatedquery['v'];
                                }
                            } 
                            elseif ($value->getPrerequisites() == 'zoom') {
                                $relatedflag = true;
                                $relatedimg_url = '';
                                $relatedpublishedAt = '';
                                $meeting_id = $value->getUnitCode();
                                $calledFunction = 'meetings/' . $meeting_id;
                                $request_url = $this->api_url . $calledFunction . '?access_token=' . $this->access_token;
                                $handle = curl_init($request_url);
                                $headers = array();
                                $headers[] = 'Content-Type: application/json';
                                curl_setopt($handle, CURLOPT_HTTPHEADER, $headers);
                                curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
                                $response_curl = curl_exec($handle);
                                $httpcode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
                                $resp = json_decode($response_curl);
                                curl_close($handle);
                                if ($httpcode == 200) {
                                    $relatedpublishedAt = $resp->created_at;
                                }
                            }

                            /* $coursemaster = $this->getDoctrine()->getManager()->getRepository('AdminBundle:coursemaster')->find($coursemaster_id);
                              $course_name = '';
                              if ($coursemaster) {
                              $course_name = $coursemaster->getCourseName();
                              } */

                            $course_id_arr = [];
                            $course_name = '';
                            $course_unit_relation_list = $em
                                    ->getRepository('AdminBundle:course_unit_relation')
                                    ->findBy(array('is_deleted' => "0", 'course_unit_id' => $value->getId(), 'relation_with' => 'course'));
                            if ($course_unit_relation_list) {
                                foreach ($course_unit_relation_list as $delkey => $delval) {
                                    $course_id_arr[] = $delval->getRelation_object_id();
                                }
                            }

                            if ($course_id_arr) {
                                foreach ($course_id_arr as $ckey => $cval) {
                                    $courseC = $em->getRepository('AdminBundle:coursemaster')->find($cval);
                                    if (!empty($courseC)) {
                                        if ($course_name == "") {
                                            $course_name = $courseC->getCourseName();
                                        } else {
                                            $course_name = $course_name . " , " . $courseC->getCourseName();
                                        }
                                    }
                                }
                            }

                            if ($value->getCourseType() == "1" && $value->getPrerequisites() == 'youtube') {
                                $relatedviews = file_get_contents("https://www.youtube.com/live_stats?v=" . $relatedquery_v);
                            }
                            $relatedcategory_id = $value->getCourseMediaId();
                            $relatedcategory_name = "";
                            $category = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Categorymaster')->find($relatedcategory_id);
                            if (!empty($category)) {
                                $relatedcategory_name = $category->getCategoryName();
                            }

                            $relateddata[] = array(
                                "main_id" => $value->getId(),
                                "title" => $value->getUnitName(),
                                "cover_image" => $this->getmedia1Action($value->getClass_cover_image(), $this->container->getParameter('live_path')),
                                "private_flag" => $value->getPrivateflag(),
                                "youtubelink" => $relatedurl,
                                "img_url" => $relatedimg_url,
                                "course_type" => $value->getCourseType(),
                                "live_type" => $value->getPrerequisites(),
                                "video_id" => $query_v,
                                "views" => $relatedviews,
                                "category_name" => $category_name,
                                'flag' => $relatedflag,
                                "course_name" => $course_name,
                                "course_id" => $coursemaster_id,
                                'publishedat' => $relatedpublishedAt,
                                "time" => $relatedtime,
                            );
                        }
                    }
                    $category_id = $course_unit->getCourseMediaId();
                    $category_name = "";
                    /* $category = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Categorymaster')->find($category_id);
                      if (!empty($category)) {
                      $category_name = $category->getCategoryName();
                      } */

                    $category_id_arr = [];
                    $category_name = "";
                    $course_unit_relation_list = $em
                            ->getRepository('AdminBundle:course_unit_relation')
                            ->findBy(array('is_deleted' => "0", 'course_unit_id' => $main_id, 'relation_with' => 'category'));
                    if ($course_unit_relation_list) {
                        foreach ($course_unit_relation_list as $delkey => $delval) {
                            $category_id_arr[] = $delval->getRelation_object_id();
                        }
                    }
//var_dump($course_unit_relation_list);exit;
                    $level_id_arr = [];

                    $course_unit_level_list = $em
                            ->getRepository('AdminBundle:course_unit_level')
                            ->findBy(array('is_deleted' => "0", 'course_unit_id' => $main_id));
                    if ($course_unit_level_list) {
                        foreach ($course_unit_level_list as $delkey => $delval) {
                            $level_id_arr[] = $delval->getClass_level_id();
                        }
                    }
                    $course_id_arr = [];
                    $course_name = '';
                    $course_unit_relation_list = $em
                            ->getRepository('AdminBundle:course_unit_relation')
                            ->findBy(array('is_deleted' => "0", 'course_unit_id' => $main_id, 'relation_with' => 'course'));
                    if ($course_unit_relation_list) {
                        foreach ($course_unit_relation_list as $delkey => $delval) {
                            $course_id_arr[] = $delval->getRelation_object_id();
                        }
                    }

                    if ($course_id_arr) {
                        foreach ($course_id_arr as $ckey => $cval) {
                            $courseC = $em->getRepository('AdminBundle:coursemaster')->find($cval);
                            if (!empty($courseC)) {
                                if ($course_name == "") {
                                    $course_name = $courseC->getCourseName();
                                } else {
                                    $course_name = $course_name . " , " . $courseC->getCourseName();
                                }
                            }
                        }
                    }

                    if ($category_id_arr) {
                        //  var_dump($category_id_arr);exit;
                        foreach ($category_id_arr as $ckey => $cval) {
                            $category = $em->getRepository('AdminBundle:Categorymaster')->find($cval);
                            if (!empty($category)) {
                                if ($category_name == "") {
                                    $category_name = $category->getCategoryName();
                                    $category_id = $cval;
                                } else {
                                    $category_name = $category_name . " , " . $category->getCategoryName();
                                    $category_id = $category_id . ' , ' . $cval;
                                }
                            }
                        }
                    }

                    if ($course_type == "1" && $course_unit->getPrerequisites() == 'youtube') {
                        $views = file_get_contents("https://www.youtube.com/live_stats?v=" . $query['v']);
                    }
                    $purchase_flag = 'no';
                    $package_id = 0;
                    $purchase_arr = NULL ;
                    // check Course Purchase or not
                    if ($user_id != 0) {
                        $subscription_master = $this->getDoctrine()->getManager()->getRepository('AdminBundle:subscription_master')->findBy(array("is_deleted" => "0"));
                        if (!empty($subscription_master)) {
                            foreach ($subscription_master as $subkey => $subval) {
                                $courseids = $subval->getCourseId();
                                if (array_intersect($courseids, $course_id_arr)) {
                                    $package_id = $subval->getId();
                                    // check that package is purhcased or not 
                                    $user_package_relation = $this->getDoctrine()->getManager()->getRepository('AdminBundle:user_package_relation')->findBy(array("is_deleted" => "0", "user_id" => $user_id, "package_id" => $package_id));
                                    if ($user_package_relation) {
                                        $purchase_flag = 'yes';
                                     
                                    }
                                    $purchase_arr[] = array("package_id"=>$package_id,"purchase_flag"=>$purchase_flag);
                                }
                            }
                        }
                        //$user_meeting_url = $this->getDoctrine('doctrine_mongodb')->getManager()->getRepository("AdminBundle:user_meeting_url")->findOneBy(array("user_id"=>$user_id,"course_unit_id"=>$main_id,"is_deleted"=>"0"));
                        $user_meeting_url = $this->getDoctrine()->getManager()->getRepository('AdminBundle:user_meeting_url')->findOneBy(array("is_deleted" => "0", "user_id" => $user_id, "course_unit_id" => $course_unit->getId()));
                        
                        if($user_meeting_url){
                            $url = $user_meeting_url->getMeeting_url();
                        }
                    }
                    if($course_unit->getPrivateflag() == "1"){
                        
                        $privateURL_ID = $course_unit->getYoutubelink() ;//$url ;
                        
//                        $video_cipher_authArray = array(
//                           "accept: application/json",
//                           "authorization: Apisecret 43edcebba4784f639d5bfe544f2677f9ea49abe8ae6348d29c5281c1473bed04",
//                           "cache-control: no-cache",
//                           "content-type: application/json",
//                         );
                        $video_cipher_authArray = array(
                           "accept: application/json",
                           "authorization: Apisecret CcQEnBzTEbGH3RwnxFKgdhb5a0XZ6ScL3GGQb2HexKHu8cYDw9fVF1jS3axuWDiE",
                           "cache-control: no-cache",
                           "content-type: application/json",
                         );
                        
                        //Get OTP of video URL

                        $curl = curl_init();
                        $videoCurl = "https://dev.vdocipher.com/api/videos/".$privateURL_ID."/otp" ;
                        curl_setopt_array($curl, array(
                          CURLOPT_URL =>$videoCurl ,
                          CURLOPT_RETURNTRANSFER => true,
                          CURLOPT_ENCODING => "",
                          CURLOPT_MAXREDIRS => 10,
                          CURLOPT_TIMEOUT => 30,
                          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                          CURLOPT_CUSTOMREQUEST => "POST",
                          CURLOPT_HTTPHEADER => $video_cipher_authArray
                        ));

                        $VideoCipherOtpResponse = curl_exec($curl);
                        $VideoCipherOtpError = curl_error($curl);

                        curl_close($curl);

                        if ($VideoCipherOtpError) {
                          echo "cURL Error #:" . $VideoCipherOtpError;
                        } else {
                          //  var_dump($VideoCipherOtpResponse);exit;
                          //echo $VideoCipherOtpResponse;
                            $url = $VideoCipherOtpResponse = json_decode($VideoCipherOtpResponse);
                           
                            $url->videoCipherId = $privateURL_ID ;
                            if(isset($VideoCipherOtpResponse->otp) && ($VideoCipherOtpResponse->otp != NULL)){
                                $videoCipherOTP = $VideoCipherOtpResponse->otp;
                                $videoCipherPlaybackInfo = $VideoCipherOtpResponse->playbackInfo;
                            }
                          //  var_dump($VideoCipherOtpResponse);exit;
                        }
                    }
                    $unit_files = [];
                    // get Course unit media Files 
                    $course_unit_media =  $em
                            ->getRepository('AdminBundle:course_unit_media')
                            ->findBy(array("course_unit_id"=> $course_unit->getId()));
                    if($course_unit_media){
                        foreach($course_unit_media as $uval){
                            $live_path = $this->container->getParameter('live_path');
                            $imgClass = $this->getmedia1Action($uval->getMedia_id(), $live_path);
                            $unit_files[] = array(
                                "media_id"=>$uval->getMedia_id(),
                                "media_type"=>$uval->getMediaType(),
                                "file_name"=>$uval->getUploadFileTitle(),
                                "media_url"=>$imgClass
                            );
                        }
                    }
                    $newdata = array(
                        "main_id" => $course_unit->getId(),
                        "title" => $course_unit->getUnitName(),
                        "cover_image" => $this->getmedia1Action($course_unit->getClass_cover_image(), $this->container->getParameter('live_path')),
                        "desc" => $course_unit->getInstruction(),
                        "private_flag" => $course_unit->getPrivateflag(),
                        "youtubelink" => $url,
                       // "youtubelink1" => $url1,
                        "img_url" => $img_url,
                        "live_type" => $course_unit->getPrerequisites(),
                        "course_type" => $course_unit->getCourseType(),
                        "video_id" => $query_v,
                        "views" => (int) $views,
                        "category_name" => $category_name,
                        'flag' => $flag,
                        "course_name" => $course_name,
                        "course_id" => implode(",", $course_id_arr),
                        "category_id" => implode(",", $category_id_arr),
                        "level_id" => implode(",", $level_id_arr),
                        'publishedat' => $publishedAt,
                        "relateddata" => $relateddata,
                        "time" => $time,
                        "purchase_flag" => $purchase_flag,
                        "purchase_package_id" => $package_id,
                        "purchase_arr"=>$purchase_arr,
                         "unit_files"=>$unit_files
                    );
                    $this->error = "SFD";
                    $response = $newdata;
                } 
                else {

                    $category_id = $course_unit->getCourseMediaId();
                    $category_name = "";
                    $category = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Categorymaster')->find($category_id);
                    if (!empty($category)) {
                        $category_name = $category->getCategoryName();
                    }
                    $coursemaster_id = $course_unit->getCoursemasterId();
                    $coursemaster = $this->getDoctrine()->getManager()->getRepository('AdminBundle:coursemaster')->find($coursemaster_id);
                    $course_name = '';
                    if ($coursemaster) {
                        $course_name = $coursemaster->getCourseName();
                    }
                    $category_id_arr = [];
                    $category_name = "";
                    $course_unit_relation_list = $em
                            ->getRepository('AdminBundle:course_unit_relation')
                            ->findBy(array('is_deleted' => "0", 'course_unit_id' => $course_unit->getId(), 'relation_with' => 'category'));
                    if ($course_unit_relation_list) {
                        foreach ($course_unit_relation_list as $delkey => $delval) {
                            $category_id_arr[] = $delval->getRelation_object_id();
                        }
                    }

                    $course_id_arr = [];
                    $course_name = '';
                    $course_unit_relation_list = $em
                            ->getRepository('AdminBundle:course_unit_relation')
                            ->findBy(array('is_deleted' => "0", 'course_unit_id' => $course_unit->getId(), 'relation_with' => 'course'));
                    if ($course_unit_relation_list) {
                        foreach ($course_unit_relation_list as $delkey => $delval) {
                            $course_id_arr[] = $delval->getRelation_object_id();
                        }
                    }

                    if ($course_id_arr) {
                        foreach ($course_id_arr as $ckey => $cval) {
                            $courseC = $em->getRepository('AdminBundle:coursemaster')->find($cval);
                            if (!empty($courseC)) {
                                if ($course_name == "") {
                                    $course_name = $courseC->getCourseName();
                                } else {
                                    $course_name = $course_name . " , " . $courseC->getCourseName();
                                }
                            }
                        }
                    }

                    if ($category_id_arr) {
                        foreach ($category_id_arr as $ckey => $cval) {
                            $category = $em->getRepository('AdminBundle:Categorymaster')->find($cval);
                            if (!empty($category)) {
                                if ($category_name == "") {
                                    $category_name = $category->getCategoryName();
                                    $category_id = $cval;
                                } else {
                                    $category_name = $category_name . " , " . $category->getCategoryName();
                                    $category_id = $category_id . ' , ' . $cval;
                                }
                            }
                        }
                    }
                    $level_id_arr = [];

                    $course_unit_level_list = $em
                            ->getRepository('AdminBundle:course_unit_level')
                            ->findBy(array('is_deleted' => "0", 'course_unit_id' => $course_unit->getId()));
                    if ($course_unit_level_list) {
                        foreach ($course_unit_level_list as $delkey => $delval) {
                            $level_id_arr[] = $delval->getClass_level_id();
                        }
                    }
                    $purchase_flag = 'no';
                    $package_id = 0;
                      $purchase_arr = NULL ;
                    // check Course Purchase or not
                    if ($user_id != 0) {
                        $subscription_master = $this->getDoctrine()->getManager()->getRepository('AdminBundle:subscription_master')->findBy(array("is_deleted" => "0"));
                        if (!empty($subscription_master)) {
                            foreach ($subscription_master as $subkey => $subval) {
                                $courseids = $subval->getCourseId();
                                if (array_intersect($courseids, $course_id_arr)) {
                                    $package_id = $subval->getId();
                                    // check that package is purhcased or not 
                                    $user_package_relation = $this->getDoctrine()->getManager()->getRepository('AdminBundle:user_package_relation')->findBy(array("is_deleted" => "0", "user_id" => $user_id, "package_id" => $package_id));
                                    if ($user_package_relation) {
                                        $purchase_flag = 'yes';
                                    }
                                    $purchase_arr[] = array("package_id"=>$package_id,"purchase_flag"=>$purchase_flag);
                                }
                            }
                        }
                        $user_meeting_url = $this->getDoctrine()->getManager()->getRepository('AdminBundle:user_meeting_url')->findOneBy(array("is_deleted" => "0", "user_id" => $user_id, "course_unit_id" => $course_unit->getId()));
                        
                        if($user_meeting_url){
                            $url = $user_meeting_url->getMeeting_url();
                        }
                    }
                    
                    // get Course unit media Files 
                    $course_unit_media =  $em
                            ->getRepository('AdminBundle:course_unit_media')
                            ->findBy(array("course_unit_id"=> $course_unit->getId()));
                    $unit_files = [];
                    if($course_unit_media){
                        foreach($course_unit_media as $uval){
                            $live_path = $this->container->getParameter('live_path');
                            $imgClass = $this->getmedia1Action($uval->getMedia_id(), $live_path);
                            $unit_files[] = array(
                                "media_id"=>$uval->getMedia_id(),
                                "media_type"=>$uval->getMediaType(),
                                "file_name"=>$uval->getUploadFileTitle(),
                                "media_url"=>$imgClass
                            );
                        }
                    }
                    $newdata = array(
                        "main_id" => $course_unit->getId(),
                        "title" => $course_unit->getUnitName(),
                        "cover_image" => $this->getmedia1Action($course_unit->getClass_cover_image(), $this->container->getParameter('live_path')),
                        "desc" => $course_unit->getInstruction(),
                        "youtubelink" => "",
                       // "youtubelink1" => $url1 ,
                        "img_url" => '',
                        "course_type" => $course_unit->getCourseType(),
                        "video_id" => '',
                        "views" => 0,
                        "category_name" => $category_name,
                        'flag' => true,
                        "course_name" => $course_name,
                        "course_id" => implode(",", $course_id_arr),
                        "category_id" => implode(",", $category_id_arr),
                        "level_id" => implode(",", $level_id_arr),
                        'publishedat' => '',
                        "relateddata" => null,
                        "time" => '',
                       "purchase_flag" => $purchase_flag,
                        "purchase_package_id" => $package_id,
                        "purchase_arr"=>$purchase_arr,
                        "unit_files"=>$unit_files
                    );

                    $this->error = "SFD";
                    $response = $newdata;
                }
            } else {
                $this->error = "NRF";
            }
        } else {
            $this->error = "PIM";
        }
        if (empty($response)) {
            $response = false;
        }
        $this->data = $response;
        return $this->responseAction();
    }

    /**
     * @Route("/ws/classshortdetails/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function classshortdetailsAction(Request $request) {
        $this->title = "classshortdetails";
        $param = $this->requestAction($request, 0);

        $em = $this->getDoctrine()->getManager();
        $this->validateRule = array(
            array(
                'rule' => 'NOTNULL',
                'field' => array(),
            ),
        );
        if ($this->validateData($param)) {
            $main_id = $param->main_id;
            $course_unit = $this->getDoctrine()->getManager()->getRepository('AdminBundle:course_unit')->find($main_id);
            if (!empty($course_unit)) {
                $url = $course_unit->getYoutubelink();
                $trainerEmail = $trainerName = '';
                if ($course_unit->getLockFeature() != "" && $course_unit->getLockFeature() != "0") {
                    $Trainerinfo = $this->getDoctrine()->getManager()->getRepository('AdminBundle:usermaster')->find($course_unit->getLockFeature());
                    if ($Trainerinfo) {
                        $trainerEmail = $Trainerinfo->getUser_email_password();
                        $trainerName = $Trainerinfo->getUser_first_name() . " " . $Trainerinfo->getUser_last_name();
                    }
                }
                $img_url = $query_v = $category_name = $flag = $publishedAt = $time = '';
                $views = $category_id = 0;
                if (!empty($url)) {
                    $course_type = $course_unit->getCourseType();

                    if ($course_unit->getPrerequisites() == 'youtube') {
                        $parts = parse_url($url);
                        parse_str($parts['query'], $query);
                        $json = file_get_contents("https://www.googleapis.com/youtube/v3/videos?part=snippet%2CcontentDetails%2Cstatistics&id=" . $query['v'] . "&key=AIzaSyAS2T7d5SqW81JNqAmICfTtEVRNiOxtAYY");
                        $obj = json_decode($json);
                        $id = sizeof($obj->items);
                        $category_id = 17;

                        $publishedAt = date("d-M-Y");

                        if ($id > 0) {
                            $views = $obj->items[0]->statistics->viewCount;
                            $duration = $obj->items[0]->contentDetails->duration;
                            $data2 = substr($duration, 2);
                            $data3 = substr($data2, 0, strlen($data2) - 1);
                            $time = "";
                            if (strpos($data3, 'M') !== false) {
                                $array = explode("M", $data3);
                                if (strpos($array[0], 'H') !== false) {
                                    $hours = explode("H", $array[0]);
                                    $time = $hours[0] . ":" . $hours[1] . ":" . $array[1];
                                } else {
                                    $time = $array[0] . ":" . $array[1];
                                }
                            } else {
                                $time = $data3;
                            }
                            $category_id = $obj->items[0]->snippet->categoryId;
                            $publishedAt = date("d-M-Y", strtotime($obj->items[0]->snippet->publishedAt));
                            $flag = false;
                        } else {
                            $flag = true;
                        }

                        if (($time != "0" or $time != 0) and $course_type == "1") {
                            $class_master = $em->getRepository('AdminBundle:course_unit')
                                    ->findOneBy(array('is_deleted' => "0", 'id' => $value->getId()));
                            $class_master->setCourseType("0");
                            $em = $this->getDoctrine()->getManager();
                            $em->flush();
                            $course_type = "0";
                        }

                        $json = file_get_contents("https://www.googleapis.com/youtube/v3/videoCategories?part=snippet&id=" . $category_id . "&key=AIzaSyAS2T7d5SqW81JNqAmICfTtEVRNiOxtAYY");

                        $obj = json_decode($json);
                        $id = sizeof($obj->items);
                        if ($id > 0) {
                            $category_name = $obj->items[0]->snippet->title;
                        }
                        $img_url = 'https://img.youtube.com/vi/' . $query['v'] . '/0.jpg';
                        $query_v = $query['v'];
                    } elseif ($course_unit->getPrerequisites() == 'zoom') {
                        $flag = true;
                        $img_url = '';
                        $publishedAt = '';
                        $meeting_id = $course_unit->getUnitCode();
                        $calledFunction = 'meetings/' . $meeting_id;
                        $request_url = $this->api_url . $calledFunction . '?access_token=' . $this->access_token;
                        $handle = curl_init($request_url);
                        $headers = array();
                        $headers[] = 'Content-Type: application/json';
                        curl_setopt($handle, CURLOPT_HTTPHEADER, $headers);
                        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
                        $response_curl = curl_exec($handle);
                        $httpcode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
                        $resp = json_decode($response_curl);
                        curl_close($handle);
                        if ($httpcode == 200) {
                            $publishedAt = $resp->created_at;
                        }
                        $query_v = $views = $category_name = $category_id = $time = '';
                    }


                    if ($course_type == "1" && $course_unit->getPrerequisites() == 'youtube') {
                        $views = file_get_contents("https://www.youtube.com/live_stats?v=" . $query['v']);
                    }

                    $newdata = array(
                        "main_id" => $course_unit->getId(),
                        "title" => $course_unit->getUnitName(),
                        "cover_image" => $this->getmedia1Action($course_unit->getClass_cover_image(), $this->container->getParameter('live_path')),
                        "desc" => $course_unit->getInstruction(),
                        "youtubelink" => $url,
                        "img_url" => $img_url,
                        "live_type" => $course_unit->getPrerequisites(),
                        "course_type" => $course_unit->getCourseType(),
                        "video_id" => $query_v,
                        "views" => (int) $views,
                        "category_name" => $category_name,
                        'flag' => $flag,
                        "category_id" => $category_id,
                        'publishedat' => $publishedAt,
                        'trainer_id' => $course_unit->getLockFeature(),
                        'trainer_name' => $trainerName,
                        'trainer_email' => $trainerEmail,
                        "time" => $time,
                    );
                    $this->error = "SFD";
                    $response = $newdata;
                } else {
                    $category_id = $course_unit->getCourseMediaId();
                    $category_name = "";
                    $category = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Categorymaster')->find($category_id);
                    if (!empty($category)) {
                        $category_name = $category->getCategoryName();
                    }
                    $coursemaster_id = $course_unit->getCoursemasterId();
                    $coursemaster = $this->getDoctrine()->getManager()->getRepository('AdminBundle:coursemaster')->find($coursemaster_id);
                    $course_name = '';
                    if ($coursemaster) {
                        $course_name = $coursemaster->getCourseName();
                    }
                    $newdata = array(
                        "main_id" => $course_unit->getId(),
                        "title" => $course_unit->getUnitName(),
                        "cover_image" => $this->getmedia1Action($course_unit->getClass_cover_image(), $this->container->getParameter('live_path')),
                        "desc" => $course_unit->getInstruction(),
                        "youtubelink" => "",
                        "img_url" => '',
                        "course_type" => $course_unit->getCourseType(),
                        "video_id" => '',
                        "views" => 0,
                        "category_name" => $category_name,
                        'flag' => true,
                        "category_id" => $category_id,
                        'publishedat' => '', "relateddata" => null,
                        'trainer_id' => $course_unit->getLockFeature(),
                        'trainer_name' => $trainerName,
                        'trainer_email' => $trainerEmail,
                        "time" => '',
                    );

                    $this->error = "SFD";
                    $response = $newdata;
                }
            } else {
                $this->error = "NRF";
            }
        } else {
            $this->error = "PIM";
        }
        if (empty($response)) {
            $response = false;
        }
        $this->data = $response;
        return $this->responseAction();
    }

}
