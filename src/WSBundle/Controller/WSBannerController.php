<?php

namespace WSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AdminBundle\Controller\CouponController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use AdminBundle\Entity\Userinfo;
use AdminBundle\Entity\Rolemaster;
use AdminBundle\Entity\Gcmuser;
use AdminBundle\Entity\Apnsuser;
use AdminBundle\Entity\users;

class WSBannerController extends WSBaseController {

    /**
     * @Route("/ws/getbanner/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function getbannerAction(Request $request) {

        $this->title = "get Banner";
        $param = $this->requestAction($request, 0);

        $this->validateRule = array(
            array(
                'rule' => 'NOTNULL',
                'field' => array(),
            ),
        );

        if ($this->validateData($param)) {

            $em = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Bannermaster');
            $bannermaster = $em->findBy(array('is_deleted' => "0"));




            if (!empty($bannermaster)) {
                foreach ($bannermaster as $bannermaster) {
                    $response[] = array(
                        "id" => $bannermaster->getId(),
                        "name" => $bannermaster->getName(),
                        "description" => $bannermaster->getDescription(),
                        "link1" => $bannermaster->getLink1(),
                        "link2" => $bannermaster->getLink2(),
                        "status" => $bannermaster->getStatus(),
                        "banner_image" => $this->getimage($bannermaster->getMedia_id())
                    );
                }
                $this->error = "SFD";
                $this->data = $response;
            } else {
                $response = false;
                $this->error = "NRF";
            }
        } else {
            $this->error = "PIM";
        }

        if (empty($response)) {
            $response = false;
            $this->error = "NRF";
        }

        $this->data = $response;
        return $this->responseAction();
    }

}

?>
