<?php

namespace WSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AdminBundle\Controller\CouponController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use AdminBundle\Entity\Userinfo;
use AdminBundle\Entity\Rolemaster;
use AdminBundle\Entity\Gcmuser;
use AdminBundle\Entity\Apnsuser;
use AdminBundle\Entity\users;
use AdminBundle\Entity\coursemaster;
use AdminBundle\Entity\Tournament;

class WSCatwiseclassController extends WSBaseController {

    /**
     * @Route("/ws/catwiseclass/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function classAction(Request $request) {

        $this->title = "catwiseclass";
        $param = $this->requestAction($request, 0);

        $em = $this->getDoctrine()->getManager();
        $this->validateRule = array(
            array(
                'rule' => 'NOTNULL',
                'field' => array(),
            ),
        );
        if ($this->validateData($param)) {
            $display_type = 'section';
            
            $catadata = null;
            $repository1 = $em->getRepository('AdminBundle:Categorymaster');
            $categorymaster = $repository1->createQueryBuilder()
                            ->field('is_deleted')->equals("0")
                            ->field('category_type')->equals($display_type)
                            
                            ->sort('sort_order', 'asc')
                            ->getQuery()->execute()->toArray();
            if(isset($param->display_type)){
                $display_type = $param->display_type;
                $categorymaster = $repository1->createQueryBuilder()
                            ->field('is_deleted')->equals("0")
                            ->field('category_type')->equals($display_type)
                            ->field('display_on_home')->equals("yes")
                            ->sort('sort_order', 'asc')
                            ->getQuery()->execute()->toArray();
            }
           
            // var_dump($categorymaster);exit;

            if (!empty($categorymaster)) {


                foreach ($categorymaster as $key => $catval) {
                    $sectionID = $catval->getId();
                    $newdata = [];
                 //   echo "SEction id : " . $sectionID . "<br><br>";
                    
                    $getCategoryList = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Categorymaster')->findBy(array("is_deleted" => "0", "parent_category_id" => $sectionID));
                    if(isset($param->display_type)){
                       $getCategoryList = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Categorymaster')->findBy(array("is_deleted" => "0", "main_category_id" => $sectionID));
                     
                    }
                    if ($getCategoryList) {
                        foreach ($getCategoryList as $gckey => $gcval) {
                            $newdata = [];
                            $repository = $this->getDoctrine()->getManager()->getRepository('AdminBundle:course_unit');

                            // get unit rleation with category
                            $course_unit_relation_list = $em
                                    ->getRepository('AdminBundle:course_unit_relation')
                                    ->findBy(array('is_deleted' => "0", 'relation_object_id' => $gcval->getId(), "relation_with" => "category"));
//                             echo "Section sub catgory : <br><br>";
//                             var_dump($getCategoryList);
//                            echo "<br><br>Sub category unit relation <br><br>";
//                            var_dump($course_unit_relation_list);
//                            echo "<br><br>";

                            $course_unit_arr = [];
                            if ($course_unit_relation_list) {
                                foreach ($course_unit_relation_list as $cukey => $cuval) {
                                    $course_unit_arr[] = $cuval->getCourse_unit_id();
                                    $value = $repository->find($cuval->getCourse_unit_id());
                                    
                                    if ($value) {
                                      //  echo "<br><br> Single unit <br><br>";
                              
                                        $url = $value->getYoutubelink();
                                        $desc = $value->getInstruction();
                                        if (!empty($url)) {
                                                // var_dump($value);
                                            if ($value->getPrerequisites() == 'youtube') {
                                                $parts = parse_url($url);
                                                @parse_str($parts['query'], $query);
                                                $id = 0 ;
                                                if($query){
                                                $json = file_get_contents("https://www.googleapis.com/youtube/v3/videos?part=snippet%2CcontentDetails%2Cstatistics&id=" . $query['v'] . "&key=AIzaSyAS2T7d5SqW81JNqAmICfTtEVRNiOxtAYY");
                                                $obj = json_decode($json);
                                                $id = sizeof($obj->items);
                                                }
                                                // $json = file_get_contents("https://www.googleapis.com/youtube/v3/videos?part=snippet%2CcontentDetails%2Cstatistics&id=" . $query['v'] . "&key=AIzaSyAS2T7d5SqW81JNqAmICfTtEVRNiOxtAYY");
                                                // $obj = json_decode($json);
                                               // $id = 0; //sizeof($obj->items);
                                                $category_id = 17;

                                                $publishedAt = date("d-M-Y");

                                                $time = "";
                                                if ($id > 0) {
                                                    $views = $obj->items[0]->statistics->viewCount;
                                                    $duration = $obj->items[0]->contentDetails->duration;
                                                    $data2 = substr($duration, 2);
                                                    $data3 = substr($data2, 0, strlen($data2) - 1);
                                                    if (strpos($data3, 'M') !== false) {
                                                        $array = explode("M", $data3);
                                                        if (strpos($array[0], 'H') !== false) {
                                                            $hours = explode("H", $array[0]);
                                                            $time = $hours[0] . ":" . $hours[1] . ":" . $array[1];
                                                        } else {
                                                            $time = $array[0] . ":" . $array[1];
                                                        }
                                                    } else {
                                                        $time = $data3;
                                                    }
                                                    $category_id = $obj->items[0]->snippet->categoryId;
                                                    $publishedAt = date("d-M-Y", strtotime($obj->items[0]->snippet->publishedAt));
                                                    $flag = false;
                                                } else {
                                                    $flag = true;
                                                }
                                                $course_type = $value->getCourseType();
                                                if (($time != "0" or $time != 0) and $course_type == "1") {
                                                    $class_master = $em->getRepository('AdminBundle:course_unit')
                                                            ->findOneBy(array('is_deleted' => "0", 'id' => $value->getId()));
                                                    $class_master->setCourseType("0");
                                                    $em = $this->getDoctrine()->getManager();
                                                    $em->flush();
                                                    $course_type = "0";
                                                }
                                                $img_url = '';
                                                if(isset($query['v'])){
                                                    $img_url = 'https://img.youtube.com/vi/' . $query['v'] . '/0.jpg';
                                                    $query_v = $query['v'];
                                                }
                                                $flag = false;
                                            } elseif ($value->getPrerequisites() == 'zoom') {
                                                $query_v = $time = '';
                                                $views = 0;
                                                $flag = true;
                                                $img_url = '';
                                                $publishedAt = '';
                                                $meeting_id = $value->getUnitCode();
                                                $calledFunction = 'meetings/' . $meeting_id;
                                                $request_url = $this->api_url . $calledFunction . '?access_token=' . $this->access_token;
                                                $handle = curl_init($request_url);
                                                $headers = array();
//                                    $headers[] = 'Content-Type: application/json';
//                                    curl_setopt($handle, CURLOPT_HTTPHEADER, $headers);
//                                    curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
//                                    $response_curl = curl_exec($handle);
//                                    $httpcode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
//                                    $resp = json_decode($response_curl);
//                                    curl_close($handle);
//                                    if ($httpcode == 200) {
//                                        $publishedAt = $resp->created_at;
//                                    }
                                            }
                                            $course_id_arr = [];
                                            $course_name = '';
                                            $course_unit_relation_list = $em
                                                    ->getRepository('AdminBundle:course_unit_relation')
                                                    ->findBy(array('is_deleted' => "0", 'course_unit_id' =>  $value->getId(), 'relation_with' => 'course'));
                                            if ($course_unit_relation_list) {
                                                foreach ($course_unit_relation_list as $delkey => $delval) {
                                                    if(!in_array($delval->getRelation_object_id(),$course_id_arr)){
                                                    $course_id_arr[] = $delval->getRelation_object_id();
                                                    }
                                                }
                                            }

                                            if ($course_id_arr) {
                                                foreach ($course_id_arr as $ckey => $cval) {
                                                    $courseC = $em->getRepository('AdminBundle:coursemaster')->find($cval);
                                                    if (!empty($courseC)) {
                                                        if ($course_name == "") {
                                                            $course_name = $courseC->getCourseName();
                                                        } else {
                                                            $course_name = $course_name . " , " . $courseC->getCourseName();
                                                        }
                                                    }
                                                }
                                            }
                                            $course_type = $value->getCourseType();

                                            $views = 0;
                                            if ($course_type == "1" && $value->getPrerequisites() == 'youtube') {
                                                $views = @file_get_contents("https://www.youtube.com/live_stats?v=" . $query['v']);
                                            }
                                            $category_id = $value->getCourseMediaId();
                                            $category_name = "";
                                            $category = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Categorymaster')->find($category_id);
                                            if (!empty($category)) {
                                                $category_name = $category->getCategoryName();
                                            }

                                            $newdata[] = array(
                                                "main_id" => $value->getId(),
                                                "title" => $value->getUnitName(),
                                                "youtubelink" => $url,
                                                "cover_image_id" => $value->getClass_cover_image(),
                                                "cover_image" => $this->getmedia1Action($value->getClass_cover_image(), $this->container->getParameter('live_path')),
                                                "live_type" => $value->getPrerequisites(),
                                                "img_url" => $img_url,
                                                "course_type" => $value->getCourseType(),
                                                "desc" => $desc,
                                                "video_id" => $query_v,
                                                "views" => $views,
                                                // "category_name" => $category_name,
                                                'flag' => $flag, //image_flag
                                                //"course_name" => $course_name,
                                                "course_name" => $course_name,
                                               // "course_id" => $coursemaster_id,
                                                'publishedat' => $publishedAt,
                                                "time" => $time,
                                                'type' => $value->getPrerequisites()
                                            );
//                                            echo " New data : -----------------------<br>";
//                                            var_dump($newdata);
                                        }
                                    }
                                }
                            }
                            
                           
                            
                        }
                    }
//echo "<br>===================================================================<br>";
                    $catadata[] = array(
                        "id" => $catval->getId(),
                        "name" => $catval->getCategoryName(),
                        "description" => $catval->getCategoryDescription(),
                        "category_image" => $this->getimage($catval->getCategoryImageId()),
                        "classlist" => $newdata
                    );

                    if ($catadata != NULL) {
                        $this->error = "SFD";
                        $response = $catadata;
                    } else {
                        $response = false;
                        $this->error = "NRF";
                    }
                }
            } else {
                $this->error = "NRF";
            }
        } else {
            $this->error = "PIM";
        }

        if (empty($response)) {
            $response = false;
        }
        $this->data = $response;
        return $this->responseAction();
    }

}
