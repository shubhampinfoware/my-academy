<?php

namespace WSBundle\Controller;

//header("Access-Control-Allow-Origin: *");
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use AdminBundle\Entity\Usermaster;
use AdminBundle\Entity\Quizprogress;
use AdminBundle\Entity\Quizansweruser;

class WSProfileController extends WSBaseController {

    private $least_accuracy;

    public function __construct() {
        parent::__construct();

        $this->least_accuracy = 50;
    }

   /**
     * @Route("ws/profile/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function profileAction($param,Request $request) {
        //try{
        $this->title = "profile list";
        $param = $this->requestAction($request, 0);
        $this->validateRule = array(
            array(
                'rule' => 'NOTNULL',
                'field' => array('language_id',"profile_id"),
            ),
        );
        $flag = "true";


        if ($this->validateData($param)) {
            
            $sql = "";
            $questions = null; 
            $options = null;
            if(isset($param->profile_id)) {
                $profile_id = $param->profile_id;
                $sql = "AND profilemaster.profilemaster_id = ". $profile_id;
            }
            $language_id = $param->language_id;

            $query = "SELECT profilemaster.*,
                      media_master.media_location,media_master.media_name
                      FROM profilemaster
                      JOIN media_master
                        ON profilemaster.profile_img = media_master.media_master_id
                        WHERE profilemaster.is_deleted = 0 and  profilemaster.language_id = ".$language_id." ".$sql;
                    
                $em = $this->getDoctrine()->getManager();
                $connection = $em->getConnection();
                $statement = $connection->prepare($query);
                $statement->execute();
                $selected_quiz = $statement->fetchAll();
                
            if(!empty($selected_quiz)){

               
                    
                     $response =  array(
                        "profile_id"=>$profile_id,
                        "profile_name"=>$selected_quiz[0]["profile_name"],
                        "media_image"=>$this->getimage($selected_quiz[0]["profile_img"]),
                        "description"=>$selected_quiz[0]["profile_description"]
                    );
                    
                

               
                 $this->error = "SFD";
            }
            else{
                $this->error = "NPF";
                $this->error_msg = "No profile Found";
            }    
        }
        else {
            $this->error = "PIM";
        }
         $this->data = $response;
        return $this->responseAction();
    }

}    