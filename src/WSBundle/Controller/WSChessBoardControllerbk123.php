<?php
namespace WSBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AdminBundle\Controller\CouponController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use AdminBundle\Entity\userinfo ;
use AdminBundle\Entity\Rolemaster ;
use AdminBundle\Entity\Gcmuser;
use AdminBundle\Entity\Apnsuser;
use AdminBundle\Entity\users;

class WSChessBoardController extends WSBaseController {

     /**
     * @Route("/ws/getChessBoard/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function getChessBoardAction(Request $request) {

        $this->title = "Get Chess Board";
        $param = $this->requestAction($request, 0);
        $em = $this->getDoctrine()->getManager();
        $this->validateRule = array(
            array(
                'rule' => 'NOTNULL',
                'field' => array(),
            ),
        );

		if ($this->validateData($param)) {
			
			$user_id = !empty($param->user_id) ? $param->user_id : 0; 
			
			$html = '<link rel="stylesheet" type="text/css" href="https://pgn.chessbase.com/CBReplay.css"/>
			<script src="https://pgn.chessbase.com/jquery-3.0.0.min.js"></script>
			<script src="https://pgn.chessbase.com/cbreplay.js" type="text/javascript"></script>
			<script>
				window.load = function (){
					document.getElementById( "westSouth0" ).style.display = "none";
				}
				$(document).ready(function(){
					$("#westSouth0").hide();
					$(".jqx-splitter-splitbar-vertical").hide();
					$("#east0").hide();
					$("#west0").css("min-width","100%");
					$("#westNest0").css("height","154%");
					
					$("#aCanvParentBG1").css("height","362px");
					$("#aCanvParentBG1").css("width","498px");
					
					$("#aCanvParentCanvas1").css("width","498px");
					$("#aCanvParentCanvas1").css("height","362px");	
					$("#b-btnFullScreen-0").hide();
					$(".jqx-splitter-splitbar-horizontal").hide();
					var html_board = $("#westNest0").html();
					
				});
			</script>
			';
			
			#get Chessboard of today
			$today = date('Y-m-d H:i:s');
			
			$iso_date = date(DATE_ISO8601, strtotime($today));
			
//			echo $iso_date;exit;
			
			$repository = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Chessgame');
			if($user_id != 0){
				
			$chess_game =  $repository->createQueryBuilder()
						->field('end_on')->gte($today)
						->field('start_on')->lte($today)
						->field('is_deleted')->equals("0")
						->field('player_one')->equals($user_id)
						->getQuery()->execute()->toArray();
						
				
			}else{

				$chess_game =  $repository->createQueryBuilder()
						->field('end_on')->gte($today)
						->field('start_on')->lte($today)
						->field('is_deleted')->equals("0")
						->getQuery()->execute()->toArray();
						
			
			}
			 
		//	echo"<pre>";print_r($chess_game);exit;	
			$pgn_file = '';
			if(!empty($chess_game)){
				foreach($chess_game as $game){
					$pgn_id = $game->getPgnId();
					$pgn_file = $this->getmedia1Action($pgn_id,$this->container->getParameter('live_path'));
				}
				
				if($pgn_file != ''){
				$file_data = file_get_contents("$pgn_file");
			//	echo $file_data;exit;
				$html .= '<div style="width:500px;height:500px"><div class="cbreplay">
						'.$file_data.'
						</div></div>
						';
				}
				
				$response = array('chess_board'=>$html);
				$this->error = "SFD";
			
			}else{
				$this->error = "NRF";
				$respone = false;
			}
//			echo $pgn_file;exit;
			
			
			
					
/*			$html .= '<div style="width:500px;height:500px"><div class="cbreplay">

					[Event "World Championship 28th"]
					[White "Spassky, Boris V"]
					[Black "Fischer, Robert James"]
					[Site "Reykjavik"]
					[Result "1–0"]
					[Date "1972.08.06"]
					[WhiteElo "2660"]
					[BlackElo "2785"]

					1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 a6 6. Bg5 e6 7. f4 Qb6 8. Qd2 Qxb2 9. Nb3 Qa3 10. Bxf6 gxf6 11. Be2 h5 12. 0-0 Nc6 13. Kh1 Bd7 14. Nb1 Qb4 15. Qe3 d5 16. exd5 Ne7 17. c4 Nf5 18. Qd3 h4 19. Bg4 Nd6 20. N1d2 f5 21. a3 Qb6 22. c5 Qb5 23. Qc3 fxg4 24. a4 h3 25. axb5 hxg2+ 26. Kxg2 Rh3 27. Qf6 Nf5 28. c6 Bc8 29. dxe6 fxe6 30. Rfe1 Be7 31. Rxe6
					</div></div>';
					*/
			if(empty($response)){
				$this->error = "NRF";
			}else{
				$this->data = $response;					
			}

//			return new Response($html);		
		}else{
			$this->error = "PIM";
		}
		
		if(empty($response)){
			$response = false;
			$this->error = "NRF";
		}
		
		$this->data = $response;
		return $this->responseAction();		
    }

}

?>
