<?php

namespace WSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/ws")
     */
    public function indexAction()
    {
		exit('ws-bundle');
        return $this->render('WSBundle:Default:index.html.twig');
    }
}
