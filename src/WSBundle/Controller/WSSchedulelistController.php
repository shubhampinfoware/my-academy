<?php
namespace WSBundle\Controller;

use Symfony\Component\HttpFoundation\Request;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class WSSchedulelistController extends WSBaseController{

    /**
     * @Route("/ws/getScheduleList/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function getScheduleListAction($param, Request $request){
		try{
			$response = false;
			$faq_arr = [];
			$this->title = "Schedule List";
			$param = $this->requestAction($request,0);
			$this->validateRule = array(
				array(
					'rule'=>'NOTNULL',
					'field'=>array(
						'domain_id'
					),
				),
			);
            		
			if($this->validateData($param)){

				$user_id = isset($param->user_id) ? $param->user_id : '';
				$domain_id = $param->domain_id;
				$language_id = isset($param->language_id) ? $param->language_id : 1;
				
				$today = date('Y-m-d');
				// if(isset($param->current_date)){
				// 	$today = $param->current_date;
				// }
				
				$current_monday = date('Y-m-d', strtotime('monday this week'));
				$current_sunday = date('Y-m-d', strtotime('sunday this week'));

				$next_monday = date('Y-m-d', strtotime('monday next week'));
				$next_sunday = date('Y-m-d', strtotime('sunday next week'));

				if($user_id != ''){
					// user courses
					$_sql = "SELECT main_coursemaster_id from user_package_relation u, subscription_master s, subscription_course_relation rel, coursemaster c where u.package_id = s.subscription_master_id and s.subscription_master_id = rel.subscription_id and c.main_coursemaster_id = rel.course_id and u.user_id = {$user_id} and c.language_id = {$language_id} and c.domain_id = {$domain_id} and u.is_deleted = 0 and s.is_deleted = 0 and rel.is_deleted = 0 and c.is_deleted = 0 group by c.main_coursemaster_id";
				} else {
					$_sql = "SELECT main_coursemaster_id from coursemaster c where c.language_id = {$language_id} and c.domain_id = {$domain_id} and c.is_deleted = 0 group by c.main_coursemaster_id";
				}
				$course_list = $this->firequery($_sql);

				$lession_this_week = [];
				$lession_next_week = [];
				$todays_lession = [];
				if(!empty($course_list)){
					foreach($course_list as $_course){
						$main_course_id = $_course['main_coursemaster_id'];

						$_query = "SELECT * from schedule_assigned_list where course_id = {$main_course_id} and schedule_date >= '{$current_monday}' and schedule_date <= '$current_sunday' and is_deleted = 0";
						$this_week_assiged_list = $this->firequery($_query);

						if(!empty($this_week_assiged_list)){
							foreach($this_week_assiged_list as $_list){
								$schedule_day = date('D', strtotime($_list['schedule_date']));

								$lession_this_week[] = array(
									'schedule_assigned_list_id' => $_list['schedule_assigned_list_id'],
									'schedule_title' => $schedule_day.' : '.$_list['schedule_title'],
									'schedule_id' => $_list['schedule_id'],
									'schedule_date' => $_list['schedule_date'],
									'main_course_id' => $_list['course_id'],
									'main_course_unit_id' => $_list['unit_id'],
									'trainer_id' => $_list['trainer_id']
								);
							}
						}

						$_query = "SELECT * from schedule_assigned_list where course_id = {$_course['main_coursemaster_id']} and schedule_date >= '{$next_monday}' and schedule_date <= '$next_sunday' and is_deleted = 0 order by schedule_date asc";
						$next_week_assiged_list = $this->firequery($_query);

						if(!empty($next_week_assiged_list)){
							foreach($next_week_assiged_list as $_list){
								$schedule_day = date('D', strtotime($_list['schedule_date']));

								$lession_next_week[] = array(
									'schedule_assigned_list_id' => $_list['schedule_assigned_list_id'],
									'schedule_title' => $schedule_day.' : '.$_list['schedule_title'],
									'schedule_id' => $_list['schedule_id'],
									'schedule_date' => $_list['schedule_date'],
									'main_course_id' => $_list['course_id'],
									'main_course_unit_id' => $_list['unit_id'],
									'trainer_id' => $_list['trainer_id']
								);
							}
						}

						$_query = "SELECT * from schedule_assigned_list where course_id = {$_course['main_coursemaster_id']} and schedule_date = '{$today}' and is_deleted = 0 order by schedule_date asc";
						$today_assiged_list = $this->firequery($_query);

						if(!empty($today_assiged_list)){
							foreach($today_assiged_list as $_list){
								$todays_lession[] = array(
									'schedule_assigned_list_id' => $_list['schedule_assigned_list_id'],
									'schedule_title' => $_list['schedule_title'],
									'schedule_id' => $_list['schedule_id'],
									'schedule_date' => $_list['schedule_date'],
									'main_course_id' => $_list['course_id'],
									'main_course_unit_id' => $_list['unit_id'],
									'trainer_id' => $_list['trainer_id']
								);
							}
						}
					}
				}

				$response['lession_this_week'] = $lession_this_week;
				$response['lession_next_week'] = $lession_next_week;
				$response['todays_lession'] = $todays_lession;
				
				$this->error = "SFD";
				$this->data = $response;

				if(empty($response)){
					$this->error = "NRF";
					$this->data = null;
				}

				return $this->responseAction();
				
			}else{
				$this->error = "PIM";
			}
			if(empty($response)){
				$response = null;
			}
			$this->data = $response;
			return $this->responseAction();
		}
		catch(\Exception $e){
			$this->error = "SFND".$e;
			$this->data = false;
			return $this->responseAction();
		}
	}
}
?>