<?php

namespace WSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AdminBundle\Controller\CouponController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use AdminBundle\Entity\Userinfo;
use AdminBundle\Entity\Rolemaster;
use AdminBundle\Entity\Gcmuser;
use AdminBundle\Entity\Apnsuser;
use AdminBundle\Entity\users;
use AdminBundle\Entity\coursemaster;
use AdminBundle\Entity\Tournament;
use AdminBundle\Entity\Usermeetingurl;
use AdminBundle\Entity\Transactionmaster;
use AdminBundle\Entity\Userpackagerelation;

class WSSubscriptionv1Controller extends WSBaseController {

    /**
     * @Route("/ws/getSubscriptionList/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function getSubscriptionListAction(Request $request) {
        $this->title = "Subscription List";
        $param = $this->requestAction($request, 0);

        $em = $this->getDoctrine()->getManager();
        $this->validateRule = array(
            array(
                'rule' => 'NOTNULL',
                'field' => array(
                    'domain_id'
                ),
            ),
        );

        if ($this->validateData($param)) {
            
            $domain_id = $param->domain_id;
            $user_id = isset($param->user_id) ? $param->user_id : '';

            $today = date('Y-m-d');

            $_sql = "SELECT * from subscription_master where lower(status) = 'active' and start_date <= '{$today}' and end_date >= '{$today}' and is_deleted = 0";
            $subscriptionList = $this->firequery($_sql);

            $response = [];
            if(!empty($subscriptionList)){
                foreach($subscriptionList as $_subscribe){

                    $image_url = '';
                    if($_subscribe['extra1'] != ''){
                        $image_url = $this->getimage($_subscribe['extra1']);
                    }

                    $_sql = "SELECT * from subscription_course_relation rel, coursemaster c where c.main_coursemaster_id = rel.course_id and subscription_id = {$_subscribe['subscription_master_id']} and c.is_deleted = 0 and rel.is_deleted = 0 group by main_coursemaster_id";
                    $courseList = $this->firequery($_sql);

                    $course_list = [];
                    if(!empty($courseList)){
                        foreach($courseList as $_course){
                            
                            $course_list[] = array(
                                'main_course_id' => $_course['main_coursemaster_id'],
                                'course_name' => $_course['course_name'],
                                'course_description' => $_course['course_description'],
                                'course_price' => $_course['course_price'],
                                'course_image_url' => $this->getimage($_course['course_image'])
                            );
                        }
                    }

                    $is_subscribed = false;
                    if ($user_id != '') {
                        $user_package_relation = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Userpackagerelation')->findBy(array(
                            "package_id" => $_subscribe['subscription_master_id'],
                            "user_id" => $user_id,
                            "is_deleted" => "0"
                        ));

                        if (!empty($user_package_relation)) {
                            $is_subscribed = true; //is subcribe
                        }
                    }

                    $response[] = array(
                        'subscription_id' => $_subscribe['subscription_master_id'],
                        'subscription_name' => $_subscribe['extra'],
                        'description' => $_subscribe['subscription_desc'],
                        'price' => $_subscribe['price'],
                        'is_subscribed' => $is_subscribed,
                        'image_url' => $image_url,
                        'course_list' => $course_list
                    );

                    $this->error = "SFD";
                }
            }

        } else {
            $this->error = "PIM";
        }
        if (empty($response)) {
            $this->error = "NRF";
            $response = false;
        }
        $this->data = $response;
        return $this->responseAction();
    }

    /**
     * @Route("/ws/buySubscription/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function buySubscriptionAction(Request $request) {
        $this->title = "Buy Subscription";
        $param = $this->requestAction($request, 0);

        $em = $this->getDoctrine()->getManager();
        $this->validateRule = array(
            array(
                'rule' => 'NOTNULL',
                'field' => array("user_id", "subscription_id"),
            ),
        );

        if ($this->validateData($param)) {
            $coupon_id = NULL;
            $user_id = $param->user_id;
            $subscription_id = $param->subscription_id;

            $_user = $this->getDoctrine()->getRepository("AdminBundle:Usermaster")->findOneBy([
                'usermaster_id' => $user_id,
                'is_deleted' => 0
            ]);

            if(empty($_user)){
                $this->error = "NRF";
                $this->data = [];
                return $this->responseAction();
            }

            $subscription_info = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Subscriptionmaster")->find($subscription_id);
            if (empty($subscription_info)) {
                $this->error = "PIW";
                $this->data = false;
                return $this->responseAction();
            }

            $wallet_info = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Walletmaster')->findOneBy(array("user_master_id" => $user_id));
            if ($wallet_info) {
                //$wallet_id = $wallet_info->get_id();
            }
            $cashback_amt = 0;
            $final_payment_amt = $subscription_amt = $subscription_info->getPrice();

            if (isset($param->coupon_id) && !empty($param->coupon_id)) {
                $coupon_id = $param->coupon_id;
                $coupon_info = $em->getRepository('AdminBundle:Couponmaster')
                        ->findOneBy(array('couponmaster_id' => $coupon_id, 'is_deleted' => "0"));

                if ($coupon_info) {
                    
                    if ((date("Y-m-d H:i:s") < ($coupon_info->getEnd_date()))){//$use_can_coupon > $coupon_usedBy){
                        $discount_type = $coupon_info->getDiscount_type();
                        $return_type = $coupon_info->getReturn_type();
                        $disc_cal_number = $coupon_info->getDiscount_value();

                        if ($discount_type == 'percentage') {
                            $discount_value = ($subscription_amt * $disc_cal_number ) / 100;
                        } else {
                            $discount_value = $disc_cal_number;
                        }
                        if ($return_type = 'less_amount') {
                            $final_payment_amt = $final_payment_amt - $discount_value;
                        } elseif ($return_type == 'cashback') {
                            $cashback_amt = $discount_value;
                        }
                    }
                    else{
                        $response = false;
                        $this->error = "CCE";
                        $this->error_msg = "Coupon Code Expire";
						 return $this->responseAction();
                    }
                    
                }
            }

            $subscription_master_rel = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Userpackagerelation')->findBy(array(
                "user_id" => $user_id,
                "package_id" => $subscription_id,
                "is_deleted" => "0"
            ));

            if (empty($subscription_master_rel)) {
                $user_package_relation = new Userpackagerelation();
                $user_package_relation->setUser_id($user_id);
                $user_package_relation->setPackage_id($subscription_id);
                $user_package_relation->setCreated_datetime(date("Y-m-d H:i:s"));
                $user_package_relation->setIs_archieved(0);
                $user_package_relation->setIs_deleted(0);

                $em->persist($user_package_relation);
                $em->flush();
                
                $user_package_relation_id = $user_package_relation->getUser_package_relation_id();

                $transaction = new Transactionmaster();
                $transaction->setRef_number('cash');
                $transaction->setPayment_id('123456');
                $transaction->setUser_id($user_id);
                $transaction->setStatus('success');
                $transaction->setUnmappedstatus('CAPTURED');
                $transaction->setTxnid('123456');
                $transaction->setAmount($final_payment_amt);
                $transaction->setNameoncard($_user->getUser_first_name());
                $transaction->setTxn_field_1('');
                $transaction->setTxn_field_2('');
                $transaction->setTxn_field_3('');
                $transaction->setCreated_datetime(date("Y-m-d H:i:s"));
                $transaction->setIs_deleted('0');
                $transaction->setProduct_id($subscription_id);

                $em->persist($transaction);
                $em->flush();

                $this->error = "SFD";

                $response = array(
                    'user_package_relation_id' => $user_package_relation_id
                );
               
                $this->data = $response;
            } else {
                $this->error = "ARE";
            }
        } else {
            $this->error = "PIM";
        }

        if (empty($response)) {
            $response = false;
        }
        //  $this->data = $response;
        return $this->responseAction();
    }

    /**
     * @Route("/ws/coursesubscription/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function coursesubscriptionAction(Request $request) {
        $this->title = "Course subscription ";
        $param = $this->requestAction($request, 0);

        $em = $this->getDoctrine()->getManager();
        $this->validateRule = array(
            array(
                'rule' => 'NOTNULL',
                'field' => array("course_id"),
            ),
        );

        if ($this->validateData($param)) {
            $userdata = null;
            $larnardata = $trainerdata = $userdata = null;
            $course_id = $param->course_id;
            $subscriptionList = [];
            $coursemasterDetails = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Coursemaster')->findOneBy(array("id" => $course_id, "is_deleted" => "0"));
            if (!empty($coursemasterDetails)) {
                $subscription_list = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Subscriptionmaster')->findBy(array("is_deleted" => '0'));
                if (!empty($subscription_list)) {
                    foreach ($subscription_list as $subkey => $subval) {
                        $courseIDS = $subval->getCourseId();
                        if (in_array($course_id, $courseIDS)) {
                            $subscriptionList[] = array(
                                "subscription_id" => $subval->getId(),
                                "name" => $subval->getExtra()
                            );
                        }
                    }
                }
                if (!empty($subscriptionList)) {
                    $this->error = "SFD";
                    $response = $subscriptionList;
                }
            } else {
                $this->error = "PIW";
            }
        } else {
            $this->error = "PIM";
            $this->data = false;
            return $this->responseAction();
        }
        if (empty($response)) {
            $this->error = "NRF";
            $response = false;
        }
        $this->data = $response;
        return $this->responseAction();
    }

}
