<?php

namespace WSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AdminBundle\Controller\CouponController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use AdminBundle\Entity\Userinfo;
use AdminBundle\Entity\Rolemaster;
use AdminBundle\Entity\Gcmuser;
use AdminBundle\Entity\Apnsuser;
use AdminBundle\Entity\users;
use AdminBundle\Entity\course_unit_relation;
use AdminBundle\Entity\course_unit;
use Google_Client;
use Google_Service_YouTube;
use Google_Service_YouTube_LiveBroadcastSnippet;
use DateTime;
use Google_Service_Exception;
use Google_Exception;
use DateTimeZone;
use AdminBundle\Entity\usermaster;
use AdminBundle\Entity\coursemaster;
use AdminBundle\Entity\user_meeting_url;

class WSAddlivestreamController extends WSBaseController {

    /**
     * @Route("/ws/addliveclass/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function addliveclassAction(Request $request) {

        $this->title = "Add Live Class";
        $param = $this->requestAction($request, 0);

        $em = $this->getDoctrine()->getManager();
        $this->validateRule = array(
            array(
                'rule' => 'NOTNULL',
                'field' => array("user_id", "name", "course_id", "status", "category_id"),
            ),
        );
        if ($this->validateData($param)) {
            $user_id = trim($param->user_id);
            $name = $param->name;
            $course_id = $param->course_id;
            $status1 = $param->status;
            $category_id = $param->category_id;
            $class_level_id = $param->level_id;
            $live_type = 'youtube';
            $date_time = date("Y-m-d H:i:s");
            $token = array(
                "access_token" => $param->access_token,
                "expires_in" => $param->expires_in,
                "token_type" => $param->token_type,
                "id_token" => $param->id_token,
            );
            $course_id = explode(",", $course_id);
            $category_id = explode(",", $category_id);
            // var_dump($token);exit;
            $class_type = "1";
            $description = $youtubelink =  "";
            if (isset($param->description)) {
                $description = $param->description;
            }
            if (isset($param->youtubelink)) {
                $youtubelink = $param->youtubelink;
            }

            $usermaster = $userMaster = $em->getRepository("AdminBundle:usermaster")
                    ->find(array(
                "id" => $user_id,
                "user_role_id" => $this->TRAINER_ROLE_ID,
                "is_deleted" => "0",
                    )
            );


            if (empty($usermaster)) {
                $this->error = "UNF";
                $this->error_msg = "User not found";
                $this->data = false;
                return $this->responseAction();
            }
            $startdate = $param->startdate;
            $starttime = $param->starttime;
            $value1 = strtolower($starttime);
            $arr = explode(":", $value1);
            $arr0 = (int) $arr[0];
            $arr1 = $arr[1];
            $str = substr($arr1, 1);
            $starttimemod = (string) $arr0 . ':' . $str;

            $a = new DateTime($starttimemod);
            $b = new DateTime('5:30');
            $interval = $a->diff($b);
            $starttime = $interval->format('%H:%i');
            $dt = new DateTime($startdate . ' ' . $starttime);
            /// $dt->setTimezone(new \DateTimeZone('UTC'));
            $startdattime = $dt->format('Y-m-d\TH:i:s.u\Z');


            $enddate = $param->enddate;
            $endtime = $param->endtime;
            $value1 = strtolower($endtime);
            $arr = explode(":", $value1);
            $arr0 = (int) $arr[0];
            $arr1 = $arr[1];
            $str = substr($arr1, 1);
            $endtimemod = (string) $arr0 . ':' . $str;
            $a = new DateTime($endtimemod);
            $b = new DateTime('5:30');
            $interval = $a->diff($b);
            $endtime = $interval->format('%H:%i');
            $dt = new DateTime($enddate . ' ' . $endtime);
            // $dt->setTimezone(new \DateTimeZone('UTC'));
            $enddattime = $dt->format('Y-m-d\TH:i:s.u\Z');
            $OAUTH2_CLIENT_ID = '255955648270-vrq2e3lu7shh8jfe7rnqlgjomeuh8da3.apps.googleusercontent.com';
            $OAUTH2_CLIENT_SECRET = '8mJFfsB2rX-RtVpXsoM9i2HC';
            $client = new Google_Client();
            $client->setClientId($OAUTH2_CLIENT_ID);
            $client->setClientSecret($OAUTH2_CLIENT_SECRET);
            $client->setScopes('https://www.googleapis.com/auth/youtube');
            $redirect = filter_var('http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'], FILTER_SANITIZE_URL);
            $client->setRedirectUri($redirect);
            // Define an object that will be used to make all API requests.
            $youtube = new Google_Service_YouTube($client);
            // Check if an auth token exists for the required scopes
            $tokenSessionKey = 'token';
            if (isset($_GET['code'])) {
                if (strval($_SESSION['state']) !== strval($_GET['state'])) {
                    die('The session state did not match.');
                }
                $client->authenticate($_GET['code']);
                //$_SESSION[$tokenSessionKey] = $client->getAccessToken();
                $token = $client->getAccessToken();
                header('Location: ' . $redirect);
            }
            if ($token !== null) {
                $client->setAccessToken($token);
            }
            //var_dump($this->get('session')->get('token'));exit;
// Check to ensure that the access token was successfully acquired.
            if ($client->getAccessToken()) {
                try {
                    // Create an object for the liveBroadcast resource's snippet. Specify values
                    // for the snippet's title, scheduled start time, and scheduled end time.
                    $broadcastSnippet = new \Google_Service_YouTube_LiveBroadcastSnippet();
                    $broadcastSnippet->setTitle($name);
                    $broadcastSnippet->setScheduledStartTime($startdattime);
                    $broadcastSnippet->setScheduledEndTime($enddattime);
                    // Create an object for the liveBroadcast resource's status, and set the
                    // broadcast's status to "private".
                    $status = new \Google_Service_YouTube_LiveBroadcastStatus();
                    $status->setPrivacyStatus($status1);
                    // Create the API request that inserts the liveBroadcast resource.
                    $broadcastInsert = new \Google_Service_YouTube_LiveBroadcast();
                    $broadcastInsert->setSnippet($broadcastSnippet);
                    $broadcastInsert->setStatus($status);
                    $broadcastInsert->setKind('youtube#liveBroadcast');
                    // Execute the request and return an object that contains information
                    // about the new broadcast.

                    $broadcastsResponse = $youtube->liveBroadcasts->insert('snippet,status', $broadcastInsert, array());



                    // Create an object for the liveStream resource's snippet. Specify a value
                    // for the snippet's title.
                    $streamSnippet = new \Google_Service_YouTube_LiveStreamSnippet();
                    $streamSnippet->setTitle('New Stream');
                    // Create an object for content distribution network details for the live
                    // stream and specify the stream's format and ingestion type.
                    $cdn = new \Google_Service_YouTube_CdnSettings();
                    $cdn->setFormat("1080p");
                    $cdn->setIngestionType('rtmp');
                    // Create the API request that inserts the liveStream resource.
                    $streamInsert = new \Google_Service_YouTube_LiveStream();
                    $streamInsert->setSnippet($streamSnippet);
                    $streamInsert->setCdn($cdn);
                    $streamInsert->setKind('youtube#liveStream');
                    // Execute the request and return an object that contains information
                    // about the new stream.

                    $streamsResponse = $youtube->liveStreams->insert('snippet,cdn', $streamInsert, array());
                    // Bind the broadcast to the live stream.
                    $htmlBody = '';
                    $bindBroadcastResponse = $youtube->liveBroadcasts->bind(
                            $broadcastsResponse['id'], 'id,contentDetails', array(
                        'streamId' => $streamsResponse['id'],
                    ));
                    $htmlBody .= "<h3>Added Broadcast</h3><ul>";
                    $htmlBody .= sprintf('<li>%s published at %s (%s)</li>', $broadcastsResponse['snippet']['title'], $broadcastsResponse['snippet']['publishedAt'], $broadcastsResponse['id']);
                    $htmlBody .= '</ul>';
                    $htmlBody .= "<h3>Added Stream</h3><ul>";
                    $htmlBody .= sprintf('<li>%s (%s)</li>', $streamsResponse['snippet']['title'], $streamsResponse['id']);
                    $htmlBody .= '</ul>';
                    $htmlBody .= "<h3>Bound Broadcast</h3><ul>";
                    $htmlBody .= sprintf('<li>Broadcast (%s) was bound to stream (%s).</li>', $bindBroadcastResponse['id'], $bindBroadcastResponse['contentDetails']['boundStreamId']);
                    $htmlBody .= '</ul>';
                    //var_dump($htmlBody);exit;
                } catch (Google_Service_Exception $e) {
                    $htmlBody = sprintf('<p>A service error occurred: <code>%s</code></p>', htmlspecialchars($e->getMessage()));
                    $data = json_decode($e->getMessage());
                    //  var_dump($data->error);exit;
                    if ($data->error->code == '400') {
                        $this->error = "ATP";
                        $this->error_msg = $data->error->message;
                        $this->data = false;
                        return $this->responseAction();
                    } else {
                        $this->error = "UNS";
                        // $this->error = "ATP";
                        $this->error_msg = $data->error->message;
                        $this->data = false;
                        return $this->responseAction();
                    }
                } catch (Google_Exception $e) {
                    $htmlBody = sprintf('<p>An client error occurred: <code>%s</code></p>', htmlspecialchars($e->getMessage()));
                    //var_dump($htmlBody);exit;
                    $this->error = "GER";

                    $this->data = false;
                    return $this->responseAction();
                }

                $_SESSION[$tokenSessionKey] = $client->getAccessToken();
            } elseif ($OAUTH2_CLIENT_ID == 'REPLACE_ME') {
                $htmlBody = <<<END
        <h3>Client Credentials Required</h3>
        <p>
          You need to set <code>\$OAUTH2_CLIENT_ID</code> and
          <code>\$OAUTH2_CLIENT_ID</code> before proceeding.
        <p>
END;
                //var_dump($htmlBody);exit;
            } else {
                // If the user hasn't authorized the app, initiate the OAuth flow
                $state = mt_rand();
                $client->setState($state);
                $_SESSION['state'] = $state;
                $authUrl = $client->createAuthUrl();
                $htmlBody = <<<END
  <h3>Authorization Required</h3>
  <p>You need to <a href="$authUrl">authorize access</a> before proceeding.<p>
END;
                // var_dump($htmlBody);exit;
            }


            if ($client->getAccessToken()) {
                $media_id = 0;
                if (isset($_FILES['class_image']) && !empty($_FILES['class_image'])) {
                    $category_logo = $_FILES['class_image'];
                    $logo = $category_logo['name'];
                    $media_type_id = 1;
                    $tmpname = $category_logo['tmp_name'];
                    $file_path = $this->container->getParameter('file_path');
                    $logo_path = $file_path . '/user';
                    $logo_upload_dir = $this->container->getParameter('upload_dir') . '/user/';
                    $media_id = $this->mediauploadAction($logo, $tmpname, $logo_path, $logo_upload_dir, $media_type_id);
                    // var_dump($media_id);exit;
                }
                if (isset($param->class_id)) {
                    $class_id = $param->class_id;
                    $class = $userMaster = $em->getRepository("AdminBundle:course_unit")
                            ->findOneBy(array(
                        "id" => $class_id,
                        "is_deleted" => "0"
                            )
                    );
                    if (empty($class)) {
                        $this->error = "NRF";
                        $response = false;
                        return $this->responseAction();
                    } else {
                        $course_unit_relation_list = $em
                                ->getRepository('AdminBundle:course_unit_relation')
                                ->findBy(array('is_deleted' => "0", 'course_unit_id' => $class_id));
                        if ($course_unit_relation_list) {
                            foreach ($course_unit_relation_list as $delkey => $delval) {
                                $delval->setIs_deleted(1);
                                $em->flush();
                            }
                        }
                    }
                } else {
                    $class = new course_unit();
                    $class->setClass_cover_image($media_id);
                }

                if (isset($param->class_id) && ($media_id != 0 )) {
                    $class->setClass_cover_image($media_id);
                }
                $class->setUnitName($name);
                $class->setCourseType(1);
                if($youtubelink != ''){
                    $class->setYoutubeLink($youtubelink);
                }else{
                    $class->setYoutubeLink('https://www.youtube.com/watch?v=' . $broadcastsResponse['id']);
                }
                $class->setCoursemasterId(0);
                $class->setCourseMediaId(0);
                $class->setCourseUnitMediaId(0);
                $class->setSortOrder(0);

                $class->setStatus("Active");
                $class->setPrerequisites($live_type); // / as vimeo/youtube/zoom
                $class->setInstruction($description);
                $class->setCreatedBy($user_id);
                $class->setLockFeature($user_id);
                $class->setClass_level_id($class_level_id);

                $class->setLanguageId("1");
                $class->setDomain_id("1");
                $class->setIs_deleted("0");

                $class->setCreate_date($date_time);
                $class->setUpdatedDate($startdattime);
                $class->setAllowShuffle("Yes");
                $class->setMainCourseUnitId(0);
                $em->persist($class);
                $em->flush();
                $main_cat = $class->getId();
                $class->setMainCourseUnitId($main_cat);
                $class->setCreate_date($date_time);
                $em->persist($class);
                $em->flush();
                $this->error = "SFD";
                $response = true;
                if ($category_id) {
                    foreach ($category_id as $ckey => $cval) {
                        $class_catgeory_realtion = new course_unit_relation();
                        $class_catgeory_realtion->setCourse_unit_id($main_cat);
                        $class_catgeory_realtion->setRelation_with('category');
                        $class_catgeory_realtion->setRelation_object_id($cval);
                        $class_catgeory_realtion->setIs_deleted(0);
                        $class_catgeory_realtion->setField_1('');
                        $class_catgeory_realtion->setField_2('');
                        $em->persist($class_catgeory_realtion);
                        $em->flush();
                    }
                }
                if ($course_id) {
                    foreach ($course_id as $ckey => $cval) {
                        $class_catgeory_realtion = new course_unit_relation();
                        $class_catgeory_realtion->setCourse_unit_id($main_cat);
                        $class_catgeory_realtion->setRelation_with('course');
                        $class_catgeory_realtion->setRelation_object_id($cval);
                        $class_catgeory_realtion->setIs_deleted(0);
                        $class_catgeory_realtion->setField_1('');
                        $class_catgeory_realtion->setField_2('');
                        $em->persist($class_catgeory_realtion);
                        $em->flush();
                    }
                }
            }
        } else {
            $this->error = "PIM";
        }

        if (empty($response)) {
            $response = false;
        }
        $this->data = $response;
        return $this->responseAction();
    }

    /**
     * @Route("/ws/addzoomlive/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function addzoomliveAction(Request $request) {
        $this->title = "Add Zoom Live Class";
        $param = $this->requestAction($request, 0);
        $em = $this->getDoctrine()->getManager();
        $this->validateRule = array(
            array(
                'rule' => 'NOTNULL',
                'field' => array("user_id", "name", "course_id", "status", "category_id", 'password'),
            ),
        );
        if ($this->validateData($param)) {
            $user_id = trim($param->user_id);
            $name = $param->name;
            $password = $param->password;
            $course_id = $param->course_id;
            $status1 = $param->status;
            $category_id = $param->category_id;
            $class_level_id = $param->level_id;
            $course_id = explode(",", $course_id);
            $category_id = explode(",", $category_id);
            $live_type = 'zoom';
            $date_time = date("Y-m-d H:i:s");

            // var_dump($token);exit;
            $class_type = "1";
            $zoom_trainerID = "CjbvEPzXQAORznGLeziolA"; // Chessbase zoom account ID
            $description = "";
            $zoom_meeting_id = 0;
            $zoom_meeting_url = '';
            $httpcode = '';
            if (isset($param->description)) {
                $description = $param->description;
            }
            $usermaster = $userMaster = $em->getRepository("AdminBundle:usermaster")
                    ->findOneBy(array(
                "id" => $user_id,
                "user_role_id" => $this->TRAINER_ROLE_ID,
                "is_deleted" => "0",
                    )
            );

            if (empty($usermaster)) {
                $this->error = "UNF";
                $this->data = false;
                return $this->responseAction();
            }
            $user_role = $userMaster->getUserroleid();
            $zoom_usermaster = $em->getRepository("AdminBundle:Userzoomrelation")
                    ->findOneBy(array(
                "user_id" => $user_id,
                "is_deleted" => "0",
                    )
            );
            if (empty($zoom_usermaster)) {
                $this->error = "ZUNF";
                $this->data = false;
                return $this->responseAction();
            }
            $zoom_user_id = $zoom_usermaster->get_zoom_user_id();
            $startdate = $param->startdate;



            $zoom_meeting_created = false;
            $zoom_error_msg = NULL;
            $media_id = 0;
            if (isset($_FILES['class_image']) && !empty($_FILES['class_image'])) {
                $category_logo = $_FILES['class_image'];
                $logo = $category_logo['name'];
                $media_type_id = 1;
                $tmpname = $category_logo['tmp_name'];
                $file_path = $this->container->getParameter('file_path');
                $logo_path = $file_path . '/user';
                $logo_upload_dir = $this->container->getParameter('upload_dir') . '/user/';
                $media_id = $this->mediauploadAction($logo, $tmpname, $logo_path, $logo_upload_dir, $media_type_id);
                // var_dump($media_id);exit;
            }
            if (isset($param->class_id)) {
                $class_id = $param->class_id;
                $class = $em->getRepository("AdminBundle:course_unit")
                        ->findOneBy(array(
                    "id" => $class_id,
                    "is_deleted" => "0"
                        )
                );
                if (empty($class)) {
                    $this->error = "NRF";
                    $response = false;
                    return $this->responseAction();
                } else {
                    $course_unit_relation_list = $em
                            ->getRepository('AdminBundle:course_unit_relation')
                            ->findBy(array('is_deleted' => "0", 'course_unit_id' => $class_id));
                    if ($course_unit_relation_list) {
                        foreach ($course_unit_relation_list as $delkey => $delval) {
                            $delval->setIs_deleted("1");
                            $em->flush();
                        }
                    }
                    if ($class->getYoutubeLink() != '' && $class->getYoutubeLink() != NULL && !empty($class->getYoutubeLink())) {
                        $zoom_meeting_created = true;
                        $zoom_meeting_id = $class->getUnitCode();
                    } else {
                        $zoom_meeting_created = false;
                    }
                }
            } else {
                $class = new course_unit();
                $class->setClass_cover_image($media_id);
            }
            if (isset($param->class_id) && ($media_id != 0 )) {
                $class->setClass_cover_image($media_id);
            }
            if ($zoom_meeting_created == false && $user_role == $this->TRAINER_ROLE_ID) {
                // sendRequest($calledFunction, $data){
                $calledFunction = 'users/' . $zoom_trainerID . '/meetings';
                $request_url = $this->api_url . $calledFunction . '?access_token=' . $this->access_token;
                $data = array(
                    //'schedule_for' => 'chessbaseindia@gmail.com',
                    'topic' => $name,
                    'type' => 2, // scheduled meetings
                    'start_time' => $startdate,
                    'duration' => 30,
                    "timezone" => "Asia/Kolkata",
                    "password" => $password,
                    "agenda" => $name,
                    "settings" => array(
                        "host_video" => true,
                        "participant_video" => false,
                        "registration_type"=>3,
                        "approval_type"=>1,
                        //"cn_meeting"=>true,
                        //"in_meeting"=> false,
                        "join_before_host" => false,
                        "mute_upon_entry" => true,
                        // "watermark"=> true,
                        "use_pmi" => false,
                        "audio" => "both",
                        "auto_recording" => "none",
                        "enforce_login" => true,
                        "enforce_login_domains" => ""
                    )
                );


                $headers = array();
                // $headers[] = 'Accept: application/xml';
                $headers[] = 'Content-Type: application/json';

                $handle = curl_init($request_url);
                curl_setopt($handle, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($handle, CURLOPT_POST, true);
                curl_setopt($handle, CURLOPT_POSTFIELDS, json_encode($data));

                //curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
                // curl_setopt($handle, CURLOPT_ENCODING, "");
                // curl_setopt($handle, CURLOPT_MAXREDIRS, 10);
                // curl_setopt($handle, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
                // curl_setopt($handle, CURLOPT_TIMEOUT, 30);

                $responseCURL = curl_exec($handle);
                $httpcode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
                $ZoomResponseData = json_decode($responseCURL);

                curl_close($handle);
                if ($httpcode == '201') {
                    // user created ..

                    if ($ZoomResponseData) {

                        $zoom_meeting_id = $ZoomResponseData->id;
                        $zoom_meeting_url = $ZoomResponseData->join_url;
                        $current_user_id = $userMaster->getId();
                    }
                } else {
                    //  var_dump(json_encode($ZoomResponseData));exit;
                    $zoom_error_msg = $ZoomResponseData;
                }
            } elseif ($zoom_meeting_created == true && $user_role == $this->TRAINER_ROLE_ID) {
                $calledFunction = 'meetings/' . $zoom_user_id . '/meetings/' . $zoom_meeting_id;
                $request_url = $this->api_url . $calledFunction . '?access_token=' . $this->access_token;
                $data = array(
                    'topic' => $name,
                    'type' => 2, // scheduled meetings
                    'start_time' => $startdate,
                    'duration' => 30,
                    "timezone" => "Asia/Kolkata",
                    "password" => $password,
                    "agenda" => $name,
                    "settings" => array(
                        "host_video" => true,
                        "participant_video" => false,
                        //"cn_meeting"=>true,
                        //"in_meeting"=> false,
                        "join_before_host" => false,
                        "mute_upon_entry" => true,
                        // "watermark"=> true,
                        "use_pmi" => false,
                        "audio" => "both",
                        "auto_recording" => "none",
                        "enforce_login" => false,
                        "enforce_login_domains" => ""
                    )
                );


                $headers = array();
                // $headers[] = 'Accept: application/xml';
                $headers[] = 'Content-Type: application/json';

                $handle = curl_init($request_url);
                curl_setopt($handle, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($handle, CURLOPT_POST, true);
                curl_setopt($handle, CURLOPT_POSTFIELDS, json_encode($data));
                $responseCURL = curl_exec($handle);
                $httpcode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
                $ZoomResponseData = json_decode($responseCURL);
                curl_close($handle);
                if ($httpcode == '201') {
                    // user created ..

                    if ($ZoomResponseData) {

                        $zoom_meeting_id = $ZoomResponseData->id;
                        $zoom_meeting_url = $ZoomResponseData->join_url;
                        $current_user_id = $userMaster->getId();
                    }
                } else {
                    $zoom_meeting_id = 0;
                    //  var_dump(json_encode($ZoomResponseData));exit;
                    $zoom_error_msg = $ZoomResponseData;
                }
            }
            if ($zoom_meeting_id != 0) {
                $class->setUnitName($name);
                $class->setCourseType(1);

                $class->setYoutubeLink($zoom_meeting_url); // Zoom Link
                $class->setUnitCode($zoom_meeting_id); // Zoom Meeting ID
                $class->setCoursemasterId(0);
                $class->setCourseMediaId(0);  //as category_id
                $class->setCourseUnitMediaId(0);
                $class->setSortOrder(0);

                $class->setStatus("Active");
                $class->setPrerequisites($live_type); // / as vimeo/youtube/zoom
                $class->setInstruction($description);
                $class->setCreatedBy($user_id);
                $class->setLockFeature($user_id); // trainer id 
                $class->setClass_level_id($class_level_id);

                $class->setLanguageId("1");
                $class->setDomain_id("1");
                $class->setIs_deleted("0");

                $class->setCreate_date($date_time);
                $class->setUpdatedDate(date("Y-m-d H:i:s"));
                $class->setAllowShuffle("Yes");
                $class->setMainCourseUnitId(0);
                $em->persist($class);
                $em->flush();
                $main_cat = $class->getId();
                $class->setMainCourseUnitId($main_cat);
                $class->setCreate_date($date_time);
                $em->persist($class);
                $em->flush();

                if ($category_id) {
                    foreach ($category_id as $ckey => $cval) {
                        $class_catgeory_realtion = new course_unit_relation();
                        $class_catgeory_realtion->setCourse_unit_id($main_cat);
                        $class_catgeory_realtion->setRelation_with('category');
                        $class_catgeory_realtion->setRelation_object_id($cval);
                        $class_catgeory_realtion->setIs_deleted(0);
                        $class_catgeory_realtion->setField_1('');
                        $class_catgeory_realtion->setField_2('');
                        $em->persist($class_catgeory_realtion);
                        $em->flush();
                    }
                }
                if ($course_id) {
                    foreach ($course_id as $ckey => $cval) {
                        $class_catgeory_realtion = new course_unit_relation();
                        $class_catgeory_realtion->setCourse_unit_id($main_cat);
                        $class_catgeory_realtion->setRelation_with('course');
                        $class_catgeory_realtion->setRelation_object_id($cval);
                        $class_catgeory_realtion->setIs_deleted(0);
                        $class_catgeory_realtion->setField_1('');
                        $class_catgeory_realtion->setField_2('');
                        $em->persist($class_catgeory_realtion);
                        $em->flush();
                    }
                }
                // after Meeting Created , add meetin registrant
                $ConsiderSubscriptionIDs = [];
                $subscription_master = $this->getDoctrine()->getManager()->getRepository('AdminBundle:subscription_master')->findBy(array("is_deleted" => "0"));
                if ($subscription_master) {
                    foreach ($subscription_master as $subkey => $subval) {
                      
                        $courseidsofSub = $subval->getCourseId();
                        $courseidarray = [];
                        foreach ($courseidsofSub as $key => $ids) {
                            $courseidarray[] = $ids;
                        }
                         
                        if (!empty($courseidarray) && !empty($course_id)) {
                            $resultArr = array_intersect($courseidarray, $course_id);
                            if ($resultArr != NULL) {
                                $ConsiderSubscriptionIDs[] = $subval->getId();
                            }
                        }
                        
                    }
                }
                $registrantUsers[] = $user_id;
                if (!empty($ConsiderSubscriptionIDs)) {
                    foreach ($ConsiderSubscriptionIDs as $conkey => $conval) {
                       
                        $user_package_realtion_list = $this->getDoctrine()->getManager()->getRepository('AdminBundle:user_package_relation')->findBy(array("is_deleted" => "0"));
                        if ($user_package_realtion_list) {
                            foreach ($user_package_realtion_list as $uskey => $usval) {
                                if(!in_array($usval->getUserId(),$registrantUsers))
                                    $registrantUsers[] = $usval->getUserId();
                            }
                        }
                    }
                }
               $need_approveUsers = [] ;
                $regIDD = '';
                if ($registrantUsers != NULL) {
                    foreach ($registrantUsers as $rkey => $rval) {
                        $RegisterUserInfo = $this->getDoctrine()->getManager()->getRepository('AdminBundle:usermaster')->find($rval);
                        if ($RegisterUserInfo) {
                           
                            $Meeting_reg_Function = 'meetings/' . $zoom_meeting_id . '/registrants';
                            $meeting_reg_request_url = $this->api_url . $Meeting_reg_Function;
                            $curl = curl_init();
                            $headers = array();
                            $headers[] = 'content-type: application/json';
                            $headers[] = 'authorization: Bearer ' . $this->access_token;
                            $meeting_reg_data = array(
                                "email" => $RegisterUserInfo->getUser_email_password(),
                                "first_name"=> $RegisterUserInfo->getUser_first_name(),
                                "last_name"=> $RegisterUserInfo->getUser_last_name(),
                                "phone"=>$RegisterUserInfo->getUser_mobile()                              

                            );
                            curl_setopt_array($curl, array(
                                CURLOPT_URL => $meeting_reg_request_url,
                                CURLOPT_RETURNTRANSFER => true,
                                CURLOPT_ENCODING => "",
                                CURLOPT_MAXREDIRS => 10,
                                CURLOPT_TIMEOUT => 30,
                                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                CURLOPT_CUSTOMREQUEST => "POST",
                                CURLOPT_POSTFIELDS =>json_encode($meeting_reg_data),
                                CURLOPT_HTTPHEADER => $headers
                            ));

                            $Meeting_registrant_response = curl_exec($curl);
                            $Meeting_registrant_err = curl_error($curl);

                            curl_close($curl);
                           
                            if ($Meeting_registrant_err) {
                                //echo "cURL Error #:" . $Meeting_registrant_err;
                            } else {   
                                
                                 $Meeting_registrant_response = json_decode($Meeting_registrant_response);
                                
                                 if(isset($Meeting_registrant_response->registrant_id)){
                                    $need_approveUsers[] = array(                                              
                                                "id"=> $Meeting_registrant_response->registrant_id,
                                                "email"=> $RegisterUserInfo->getUser_email_password()
                                               );
                                     $joinURL = '';
                                     $regIDD = $Meeting_registrant_response->registrant_id ;
                                    if(isset($Meeting_registrant_response->join_url)){
                                        $joinURL = $Meeting_registrant_response->join_url;
                                    }
                                     $user_meeting_url = new user_meeting_url();
                                     $user_meeting_url->setUser_id($rval);
                                     $user_meeting_url->setCourse_unit_id($main_cat);
                                     $user_meeting_url->setMeeting_id($zoom_meeting_id);
                                     $user_meeting_url->setMeeting_url($joinURL);
                                     $user_meeting_url->setRegistrant_id($Meeting_registrant_response->registrant_id);
                                     $user_meeting_url->setIs_deleted("0");
                                     $em->persist($user_meeting_url);
                                     $em->flush();
                                 }
                            }
                        }
                    }
                }
                if($need_approveUsers != NULL){
                     $curl = curl_init();
                    $Meeting_reg_StatusFunction = 'meetings/' . $zoom_meeting_id . '/registrants/status';
                    $meeting_reg_Statusrequest_url = $this->api_url . $Meeting_reg_StatusFunction;
                    $meeting_registrant_updateData = array(                                        
                            "action"=> "approve",
                            "registrants"=>$need_approveUsers
                    );

                    curl_setopt_array($curl, array(
                      CURLOPT_URL => $meeting_reg_Statusrequest_url,
                      CURLOPT_RETURNTRANSFER => true,
                      CURLOPT_ENCODING => "",
                      CURLOPT_MAXREDIRS => 10,
                      CURLOPT_TIMEOUT => 30,
                      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                      CURLOPT_CUSTOMREQUEST => "PUT",
                      CURLOPT_POSTFIELDS =>json_encode($meeting_registrant_updateData),
                      CURLOPT_HTTPHEADER => $headers
                    ));

                    $Meeting_registrant_status_response = curl_exec($curl);
                    $Meeting_registrant_status_err = curl_error($curl);

                    curl_close($curl);

                    if ($Meeting_registrant_status_err) {
                      echo "cURL Error #:" . $Meeting_registrant_status_err;exit;
                    } else {
                      //echo $Meeting_registrant_status_response;
                    }
                }
                $curl = curl_init();
                $Meeting_getRegistrant_Function = 'meetings/' . $zoom_meeting_id . '/registrants?status=approved';
                $meeting_getRegistrant_url = $this->api_url . $Meeting_getRegistrant_Function;
                curl_setopt_array($curl, array(
                  CURLOPT_URL => $meeting_getRegistrant_url,
                  CURLOPT_RETURNTRANSFER => true,
                  CURLOPT_ENCODING => "",
                  CURLOPT_MAXREDIRS => 10,
                  CURLOPT_TIMEOUT => 30,
                  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                  CURLOPT_CUSTOMREQUEST => "GET",
                  CURLOPT_HTTPHEADER => $headers
                ));

                $Meeting_getRegistrant_response = curl_exec($curl);
                $err = curl_error($curl);

                curl_close($curl);

                if ($err) {
                  echo "cURL Error #:" . $err;
                } else {
                 // echo $Meeting_getRegistrant_response;
                }
                $Meeting_getRegistrant_response = (json_decode($Meeting_getRegistrant_response));
                $Meeting_getRegistrant_response = $Meeting_getRegistrant_response->registrants;
                foreach($Meeting_getRegistrant_response as $nval){
                               
                    $need_reg_id = $nval->id;
                    $need_join_url = $nval->join_url;
                    
                    //-------------------------------------------------------------------------------------
                    $user_meeting_url = $this->getDoctrine()->getManager()->getRepository('AdminBundle:user_meeting_url')->findOneBy(array("registrant_id" => $need_reg_id));
                    if($user_meeting_url){
                        $user_meeting_url->setMeeting_url($need_join_url);
                        $em->flush();
                    }
                    
                }
                $this->error = "SFD";
                $response = array(
                    "zoom_meeting_id" => $zoom_meeting_id,
                    "httpcode" => $httpcode
                );
            } else {
                $this->error = "MNC";
                $this->error_msg = json_encode($zoom_error_msg);
            }
        } else {
            $this->error = "PIM";
        }

        if (empty($response)) {
            $response = false;
        }
        $this->data = $response;
        return $this->responseAction();
    }

}
