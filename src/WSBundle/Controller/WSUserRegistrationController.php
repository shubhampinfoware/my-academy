<?php

namespace WSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AdminBundle\Controller\CouponController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use AdminBundle\Entity\usermaster;
use AdminBundle\Entity\Rolemaster;
use AdminBundle\Entity\Gcmuser;
use AdminBundle\Entity\Apnsuser;
use AdminBundle\Entity\users;
use AdminBundle\Entity\fideuser;
use AdminBundle\Entity\Walletmaster;
use AdminBundle\Entity\usertrainerrelation;
use AdminBundle\Entity\Userzoomrelation;

class WSUserRegistrationController extends WSBaseController {

    /**
     * @Route("/ws/userregistration/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function userRegistrationAction(Request $request) {
        //try{
        $this->title = "User registration";
        $param = $this->requestAction($request, 0);
        // use to validate required param validation
        $em = $this->getDoctrine()->getManager();
        $this->validateRule = array(
            array(
                'rule' => 'NOTNULL',
                'field' => array('firstname', 'lastname', 'email', 'mobile_no', 'role', 'verify', 'domain_id'),
            ),
        );
        if ($this->validateData($param)) {
            $firstname = $param->firstname;
            $lastname = $param->lastname;
            $email_id = $param->email;
            $mobile_no = $param->mobile_no;
            $user_roleID = $role = $param->role;
            $domain_id = $param->domain_id;
            $verify = $param->verify;
            $fide_id = 0;
            
            if (isset($param->fide)) {
                $fide_id = $param->fide;
            }
            $age = '';
            if (isset($param->age)) {
                $age = $param->age;
            }
            $parent_name = '';
            if (isset($param->parent_name)) {
                $parent_name = $param->parent_name;
            }
            $parent_mobile = '';
            if (isset($param->parent_mobile)) {
                $parent_mobile = $param->parent_mobile;
            }
            $fide_elo = '';
            if (isset($param->fide_elo)) {
                $fide_elo = $param->fide_elo;
            }
            $fide_title = '';
            if (isset($param->fide_title)) {
                $fide_title = $param->fide_title;
            }
            $register_from = 'actualuserchesscoachingsite';
            if (isset($param->register_from)) {
                $register_from = $param->register_from;
            }
            $external_id = '0';
            if (isset($param->external_id)) {
                $external_id = $param->external_id;
            }
            $fide_rating = 0;
            if (isset($param->fide_rating)) {
                $fide_rating = $param->fide_rating;
            }
            $password = "";
            if (isset($param->password)) {
                $password = $param->password;
            }
           

            if ($verify == false) {
                $this->error = "NVF";
                $this->data = false;
                return $this->responseAction();
            }

            $userDetail = $em->getRepository("AdminBundle:Usermaster")
                    ->findOneBy(
                        array(
                            "user_mobile" => $mobile_no,
                            'domain_id' => $domain_id
                        )
                    );

            if (isset($userDetail) && !empty($userDetail)) {
                $this->error = "ARE";
                $this->data = false;
                return $this->responseAction();
            }

            $userDetail = $em->getRepository("AdminBundle:Usermaster")
                    ->findOneBy(array(
                        "user_email_password" => $email_id,
                        "domain_id" => $domain_id
                    )
            );
            if (isset($userDetail) && !empty($userDetail)) {
                $this->error = "ARE";
                $this->data = false;
                return $this->responseAction();
            }
            $zoom_user_id = NULL;
            $domain_code = 1;
            $httpcode = 0;
            $ZoomResponseData = NULL;
            
            //---if Trainer is here then 
            /* if ($user_roleID == $this->TRAINER_ROLE_ID) {
                // sendRequest($calledFunction, $data){
                $calledFunction = 'users';
                $request_url = $this->api_url . $calledFunction . '?access_token=' . $this->access_token;
                $data = array('action' => 'create', 'user_info' => array(
                        "email" => $email_id,
                        "type" => 1,
                        "first_name" => $firstname,
                        "last_name" => $lastname,
                        "password" => $password
                    ));
                $headers = array();
                // $headers[] = 'Accept: application/xml';
                $headers[] = 'Content-Type: application/json';
                $handle = curl_init($request_url);
                curl_setopt($handle, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($handle, CURLOPT_POST, true);
                curl_setopt($handle, CURLOPT_POSTFIELDS, json_encode($data));
                $responseCURL = curl_exec($handle);
                $httpcode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
                $ZoomResponseData = json_decode($responseCURL);
                curl_close($handle);
                
                if ($ZoomResponseData) {
                    // $httpcode = $ZoomResponseData->code ;               
                    if ($httpcode == '201') {
                        $zoom_user_id = $ZoomResponseData->id;
                        
                    } else if ($httpcode == '409' || $httpcode == "1005" || $httpcode == "1009" || $httpcode == "1135") {
                        $this->error = "ZEAE";
                        $this->error_msg = "ZEAE";
                        $this->data = $ZoomResponseData;
                        return $this->responseAction();
                    } else if ($httpcode == '400') {
                        $this->error = "BDZ";
                        $this->error_msg = "Bad Request for Zoom";
                        $this->data = $ZoomResponseData;
                        return $this->responseAction();
                    }
                }
                else{
                    $this->error = "ZUNC";
                    $this->error_msg = "Zoom user not created : " . $httpcode . " With Zoom user id : " . $zoom_user_id;
                    $this->data = false;
                    return $this->responseAction();
                }
            }
            if ($user_roleID == $this->TRAINER_ROLE_ID) {
                if ($zoom_user_id == NULL) {
                    $this->error = "ZUNC";
                    $this->error_msg = "Zoom user not created : " . $httpcode . " With Zoom user id : " . $zoom_user_id;
                    $this->data = false;
                    return $this->responseAction();
                }
            } */

            //user image
            $media_id = $media_id1 = 0;
            $upload_dir = "";
            if (isset($_FILES['user_image']) && !empty($_FILES['user_image'])) {
                $category_logo = $_FILES['user_image'];
                $logo = $category_logo['name'];
                $media_type_id = 1;
                $tmpname = $category_logo['tmp_name'];
                $file_path = $this->container->getParameter('file_path');
                $logo_path = $file_path . '/user';
                $logo_upload_dir = $this->container->getParameter('upload_dir') . '/user/';
                $media_id = $this->mediauploadAction($logo, $tmpname, $logo_path, $logo_upload_dir, $media_type_id);
                // var_dump($media_id);exit;
            }
            if (isset($_FILES['body_report_image']) && !empty($_FILES['body_report_image'])) {
                $category_logo = $_FILES['body_report_image'];
                ;
                $path = "user_report";
                $logo = $category_logo['name'];
                $media_type_id = 1;
                $tmpname = $category_logo['tmp_name'];
                $file_path = $this->container->getParameter('file_path');
                $logo_path = $file_path . '/user';
                $logo_upload_dir = $this->container->getParameter('upload_dir') . '/user/';
                $media_id = $this->mediauploadAction($logo, $tmpname, $logo_path, $logo_upload_dir, $media_type_id);
            }

            $verification_code = rand(100000, 999999);

            $usermaster = new Usermaster();
            $usermaster->setUser_first_name($firstname);
            $usermaster->setUser_last_name($lastname);
            $usermaster->setUser_email_password($email_id);
            $usermaster->setUser_password(md5($password));
            $usermaster->setUser_mobile($mobile_no);
            $usermaster->setUser_status("active");
            $usermaster->setMedia_id($media_id);
            $usermaster->setUser_role_id($role);
            $usermaster->setDomain_id($domain_id);
            $usermaster->setCreate_date(date('Y-m-d H:i:s'));
            $usermaster->setLogindate(date('Y-m-d H:i:s'));
            $usermaster->setBirthdate(date('Y-m-d H:i:s'));
            $usermaster->setExt_field($verify);
            $usermaster->setExt_field1($fide_rating);
            $usermaster->setRegister_from($register_from);
            $usermaster->setExternal_id($external_id);
            $usermaster->setAge($age);
            $usermaster->setParent_name($parent_name);
            $usermaster->setParent_mobile($parent_mobile);
            $usermaster->setIs_deleted(0);
            $em->persist($usermaster);
            $em->flush();

            $fideuser = new Fideuser();
            $fideuser->setUser_id($usermaster->getUsermaster_id());
            $fideuser->setFide_id($fide_id);
            $fideuser->setFide_elo($fide_elo);
            $fideuser->setFide_title($fide_title);
            $fideuser->setIs_deleted(0);
            $em->persist($fideuser);
            $em->flush();

            $usertrainerrelation = new Usertrainerrelation();
            $usertrainerrelation->setUser_id($usermaster->getUsermaster_id());
            $usertrainerrelation->setIs_chessbase("0");
            $usertrainerrelation->setIs_approved("0");
            $usertrainerrelation->setIs_deleted("0");
            $em->persist($usertrainerrelation);
            $em->flush();

            // if ($usertrainerrelation->getIs_approved() == "1") {
            //     $role = $this->TRAINER_ROLE_ID;  // Trainer                      
            // } else {
            //     $role = $this->LEARNER_ROLE_ID; // Learner
            // }
            
            //---if Trainer is here then 
            if ($user_roleID == $this->TRAINER_ROLE_ID) {
                if ($httpcode == '201') {
                    if ($ZoomResponseData) {
                        $zoom_user_id = $ZoomResponseData->id;
                        $current_user_id = $usermaster->getUsermaster_id();
                        $userzoomrelation = new Userzoomrelation();
                        $userzoomrelation->setUser_id($current_user_id);
                        $userzoomrelation->set_zoom_user_id($zoom_user_id);
                        $userzoomrelation->set_zoom_insert_flag("created");
                        $userzoomrelation->set_zoom_insert_status("");
                        $userzoomrelation->setIs_deleted("0");
                        $em->persist($userzoomrelation);
                        $em->flush();
                    }
                } else {
                    
                }
            }
            //---------User Wallet Creation--------------
            $wallet_code = $usermaster->getUsermaster_id() . "_" . substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 5)), 0, 5);
            $wallet_master = new Walletmaster();
            $wallet_master->setWallet_code($wallet_code);
            $wallet_master->setUser_master_id($usermaster->getUsermaster_id());
            $wallet_master->setVirtual_money_balance(0);
            $wallet_master->setActual_money_balance(0);
            $wallet_master->setCashback_balance(0);
            $wallet_master->setWallet_points(0);
            $wallet_master->setLast_updated_wallet_datetime(date("Y-m-d H:i:s"));
            $wallet_master->setWallet_member_tag_id(1);
            $wallet_master->setIs_deleted(0);
            $em->persist($wallet_master);
            $em->flush();
            $em = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Rolemaster');
            $userrole = $em->find($role);
            if ($userrole) {
                $roled = array(
                    "role_id" => $role,
                    "role_name" => $userrole->getRolename()
                );
            }

            $response = array(
                "user_id" => $usermaster->getUsermaster_Id(),
                "zoom_user_id" => $zoom_user_id,
                "email" => $usermaster->getUser_email_password(),
                "firstname" => $usermaster->getUser_first_name(),
                "lastname" => $usermaster->getUser_last_name(),
                "mobile_no" => $usermaster->getUser_mobile(),
                "img" => $this->getimage($usermaster->getMedia_id()),
                "roles" => $roled,
                "fide_id" => $fide_id,
                "fide_rating" => $fide_rating,
                "httpcode" => $httpcode,
                "age" => $age,
                "parent_name" => $parent_name,
                "parent_mobile" => $parent_mobile,
                "fide_title" => $fide_title,
                "fide_elo" => $fide_elo,
                "httpcode" => $httpcode,
                "wallet_id" => $wallet_master->getWallet_master_id()
            );

            $this->error = "SFD";
        } else {
            $this->error = "PIM";
        }
        if (empty($response)) {
            $response = false;
        }
        $this->data = $response;
        return $this->responseAction();
        //}catch(\Exception $e){
        //  $this->error = "SFND ".$e ;
        //  $this->data = false ;
        //  return $this->responseAction() ;
        //}
    }

    /**
     * @Route("/ws/guestUsers/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function userguestAction(Request $request) {
        // try {
        $this->title = "Guest user registration";
        $param = $this->requestAction($request, 0);
        // use to validate required param validation
        $this->validateRule = array(
            array(
                'rule' => 'NOTNULL',
                'field' => array('device_token', 'device_id', 'device_name'),
            ),
        );
        $msg_sent_error = "false";
        $location = NULL;
        $em = $this->getDoctrine()->getManager();

        if ($this->validateData($param)) {
            $role_id = $this->GUEST_ROLE_ID;
            $device_id = $param->device_id;
            $device_name = strtoupper($param->device_name);
            $device_token = $param->device_token;
//                $domain_code = $param->domain_code;
            $user_gender = 'male';
//                $app_id = $param->app_id;
            $app_id = 'CUST';
            $date = date("Y-m-d H:i:s");
            $timestamp = date('Y-m-d H:i:s', strtotime($date));
            // make entry in user master
            $user = new users();
            $user->setUsername('');
            $user->setName('');
            $user->setAdmin(false);
            $user->setEditor(false);
            $user->setBcrypt('');
            $user->setEditortype("");
            //  var_dump($user);exit;
            $em->persist($user);
            $em->flush();
            $userid = $user->getUsermaster_id();
            $usermaster = new Userinfo();
            $usermaster->setUser_id($userid);
            $usermaster->setFirstname('');
            $usermaster->setLastname('');
            $usermaster->setBirthdate('');
            $usermaster->setMobile('');
            $usermaster->setEmail('');
            $usermaster->setFide('');
            $usermaster->setRoleid($role_id);
            $usermaster->setMedia_id(0);
            $usermaster->setIs_deleted(0);
            $em->persist($usermaster);
            $em->flush();
            if ($device_name == 'ANDROID' || strtolower($device_name) == 'android') {
                $gcm_user_del = $em->getRepository("AdminBundle:Gcmuser")
                        ->findBy(array("device_id" => $device_id, "app_id" => $app_id));
                foreach ($gcm_user_del as $value) {
                    $value->setIs_deleted(1);
                    $em->persist($value);
                    $em->flush();
                }
                $gcm_user_del = $em->getRepository("AdminBundle:Gcmuser")
                        ->findOneBy(array("user_id" => $userid, "app_id" => $app_id));

                if (!empty($gcm_user_del)) {
                    $gcm_user_del->setGcmRegid($device_token);
                    $gcm_user_del->setUser_id($userid);
                    $gcm_user_del->setDeviceId($device_id);
                    $gcm_user_del->setAppId($app_id);
                    $gcm_user_del->setName('');
                    $gcm_user_del->setCreatedDate(date('Y-m-d H:i:s'));
                    $gcm_user_del->setIs_deleted("0");
                    $em->persist($gcm_user_del);
                    $em->flush();
                } else {
                    $gcm_user_del = new Gcmuser();
                    $gcm_user_del->setGcmregid($device_token);
                    $gcm_user_del->setUser_id($userid);
                    $gcm_user_del->setDeviceid($device_id);
                    $gcm_user_del->setAppid($app_id);
                    $gcm_user_del->setName('');
                    $gcm_user_del->setCreateddate(date('Y-m-d H:i:s'));
                    $gcm_user_del->setIs_deleted("0");
                    $em->persist($gcm_user_del);
                    $em->flush();
                }
            }
            /*
              if (isset($device_token) && !empty($device_token) && $device_name == 'IPHONE') {
              $Apnsuser = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Apnsuser")->findOneBy(
              array(
              "device_id" => $device_id, "user_id" => $user_id, "domain_id" => $domain_code,
              "app_id" => $app_id, "is_deleted" => '0'
              ));
              //Apns existance check
              if (count($Apnsuser) > 0) {
              $apns_user_info = $this->getDoctrine()
              ->getManager()
              ->getRepository("AdminBundle:Apnsuser")
              ->findOneBy(
              array(
              "device_id" => $device_id,
              "domain_id" => $domain_code,
              "app_id" => $app_id,
              "user_id" => $user_id,
              "is_deleted" => '0'
              )
              );
              $apns_user_info->setApns_regid($device_token);
              $apns_user_info->setUser_id($user_id);
              $apns_user_info->setUser_type('guest');
              $apns_user_info->setDevice_id($device_id);
              $apns_user_info->setDomain_id($domain_code);
              $apns_user_info->setApp_id($app_id);
              $apns_user_info->setName('');
              $apns_user_info->setBadge("0");
              $apns_user_info->setCreated_date(date('Y-m-d H:i:s'));
              $apns_user_info->setIs_deleted(0);
              $em = $this->getDoctrine()->getManager();
              $em->flush();
              $data = true;
              } else {
              $apns_user_delete_list = $this->getDoctrine()
              ->getManager()
              ->getRepository("AdminBundle:Apnsuser")
              ->findBy(
              array(
              "device_id" => $device_id,
              "domain_id" => $domain_code,
              "app_id" => $app_id,
              "is_deleted" => '0'
              )
              );
              if (!empty($apns_user_delete_list)) {
              foreach (array_slice($apns_user_delete_list, 0) as $adkey => $adval) {
              $apns_user_single = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Apnsuser")->findOneBy(
              array(
              "apns_user_id" => $adval->getApns_user_id(), "domain_id" => $domain_code,
              "app_id" => $app_id, "is_deleted" => '0')
              );
              $apns_user_single->setIs_deleted(1);
              $em->persist($apns_user_single);
              $em->flush();
              }
              }
              $apns_user_info = new Apnsuser();
              $apns_user_info->setApns_regid($device_token);
              $apns_user_info->setUser_id($user_id);
              $apns_user_info->setUser_type('guest');
              $apns_user_info->setName('');
              $apns_user_info->setDomain_id($domain_code);
              $apns_user_info->setApp_id($app_id);
              $apns_user_info->setBadge("0");
              $apns_user_info->setDevice_id($device_id);
              $apns_user_info->setIs_deleted(0);
              $apns_user_info->setCreated_date(date('Y-m-d H:i:s'));
              $em = $this->getDoctrine()->getManager();
              $em->persist($apns_user_info);
              $em->flush();
              $data = true;
              }
              }
             */
            $dob = "";
            $response = array(
                "user_id" => $userid,
                "conatct_no" => "",
                "first_name" => "",
                "last_name" => "",
                "profile_image" => "",
                "email_id" => "",
                "birth_date" => $dob,
            );
            $this->error = "SFD";
        } else {
            $this->error = "PIM";
        }
        if (empty($response)) {
            $response = false;
        }
        $this->data = $response;
        return $this->responseAction();
        //} catch (\Exception $e) {
        //   $this->error = "SFND ".$e->getMessage();
        //    $this->data = false;
        //   return $this->responseAction();
        //}
    }
}
