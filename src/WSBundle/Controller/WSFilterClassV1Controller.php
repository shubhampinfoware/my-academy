<?php

namespace WSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AdminBundle\Controller\CouponController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use AdminBundle\Entity\Userinfo;
use AdminBundle\Entity\Rolemaster;
use AdminBundle\Entity\Gcmuser;
use AdminBundle\Entity\Apnsuser;
use AdminBundle\Entity\users;
use AdminBundle\Entity\coursemaster;
use AdminBundle\Entity\Tournament;

class WSFilterClassV1Controller extends WSBaseController {

    /**
     * @Route("/ws/filterclassV1/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function filterclassV1Action(Request $request) {

        $this->title = "filterclass V1";
        $param = $this->requestAction($request, 0);

        $em = $this->getDoctrine()->getManager();
        $this->validateRule = array(
            array(
                'rule' => 'NOTNULL',
                'field' => array(),
            ),
        );
        if ($this->validateData($param)) {
            $skip = 0;
            $limit = 100;
            if (isset($param->limit)) {
                $limit = (int) $param->limit; //20 ;
            }
            if (isset($param->skip)) {
                $skip = $param->skip; //20 ;
            }

            $repository = $this->getDoctrine()->getManager()->getRepository('AdminBundle:course_unit');
            $repository1 = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Categorymaster');
            
            if (isset($param->category_id)){
            $Outer_categorymaster = $repository1->createQueryBuilder()
                            ->field('is_deleted')->equals("0")
                           // ->field('category_type')->equals('section')
                            ->field('main_category_id')->equals($param->category_id)
                            ->sort('sort_order', 'asc')
                            ->getQuery()->execute()->toArray();
            }
            else{
                $Outer_categorymaster = $repository1->createQueryBuilder()
                            ->field('is_deleted')->equals("0")
                            ->field('category_type')->equals('section')
                            ->sort('sort_order', 'asc')
                            ->getQuery()->execute()->toArray();

            }
            //var_dump($Outer_categorymaster);exit;
            if ($Outer_categorymaster) {
                foreach ($Outer_categorymaster as $okey => $oval) {
                    // get Sections of this category 
                    $category_class_list = NULL;
                    $category_courseunitArray = [];
                     if (isset($param->category_id)){
                         $sub_category_list = $em->getRepository('AdminBundle:Categorymaster')->findBy(array("is_deleted" => "0", "main_category_id" => $oval->getId()));
                        if(isset($param->is_section)){
                             $sub_category_list = $em->getRepository('AdminBundle:Categorymaster')->findBy(array("is_deleted" => "0", "parent_category_id" => $oval->getId()));
                        }
                        
                     }
                     else{
                        $sub_category_list = $em->getRepository('AdminBundle:Categorymaster')->findBy(array("is_deleted" => "0", "parent_category_id" => $oval->getId())); 
                     }
                    if (!empty($sub_category_list)) {

                        $category_courseunitIDS = [];
                        foreach ($sub_category_list as $subcatekey => $subcateval) {


                            // fetch category course 
                            $course_unit_relation_list = $em
                                    ->getRepository('AdminBundle:course_unit_relation')
                                    ->findBy(array('is_deleted' => "0", 'relation_object_id' => $subcateval->getId(), "relation_with" => "category"));
                            if ($course_unit_relation_list) {
                                foreach ($course_unit_relation_list as $cukey => $cuval) {
                                    $category_courseunitIDS[] = $cuval->getCourse_unit_id();
                                }
                            }
                            $coursemaster = $repository->createQueryBuilder()
                                            ->sort('id', 'desc')
                                            ->field('youtubelink')->notEqual("")
                                            ->field('is_deleted')->equals("0")
                                            ->field('id')->in($category_courseunitIDS)
                                            ->limit($limit)
                                            ->skip($skip)
                                            ->getQuery()->execute()->toArray();
//                            echo "<br>==============> category name : " . $oval->getCategoryName()."<br>";
//                            var_dump($coursemaster);
//                            echo "<br><br>";
                            if (isset($param->course_type)) {
                                $course_type = $param->course_type;
                                $coursemaster = $repository->createQueryBuilder()
                                                ->field('course_type')
                                                ->equals($course_type)
                                                ->field('youtubelink')
                                                ->notEqual("")
                                                ->field('is_deleted')
                                                ->equals('0')
                                                ->field('id')->in($category_courseunitIDS)
                                                ->sort('id', 'desc')
                                                ->limit($limit)
                                                ->skip($skip)
                                                ->getQuery()->execute()->toArray();
                            }
                            if (isset($param->category_id) && false) {
                                $category_id = $param->category_id;
                                $category_id_arr = explode(",", $category_id);
                                $course_unit_relation_list_arr = $this->getDoctrine()->getManager()->getRepository('AdminBundle:course_unit_relation')->createQueryBuilder()
                                                ->field('is_deleted')->equals("0")
                                                ->field('relation_with')->equals("category")
                                                ->field('relation_object_id')->in($category_id_arr)
                                                ->getQuery()->execute()->toArray();

                                $course_unit_IDS = [];
                                if ($course_unit_relation_list_arr) {
                                    foreach ($course_unit_relation_list_arr as $cukey => $cuval) {
                                        $course_unit_IDS[] = $cuval->getCourse_unit_id();
                                    }
                                }
                                $coursemaster = $repository->createQueryBuilder()
                                                ->field('youtubelink')->notEqual("")
                                                ->field('id')->in($course_unit_IDS)
                                                ->sort('id', 'desc')
                                                ->getQuery()->execute()->toArray();
                            }
                            if (isset($param->course_type) && isset($param->category_id)) {
                                $course_type = $param->course_type;
                                $category_id = $param->category_id;
                                $coursemaster = $repository->createQueryBuilder()
                                                ->field('course_type')
                                                ->equals($course_type)
                                                ->field('youtubelink')
                                                ->notEqual("")
                                                ->field('id')->in($category_courseunitIDS)
                                                ->sort('id', 'desc')
                                                ->getQuery()->execute()->toArray();
                            }
                            if (!empty($coursemaster)) {
                                foreach ($coursemaster as $value) {
                                   // echo "<br> Count ==>" . count($category_courseunitArray) . " ==> course cnt ---> ". count($coursemaster) ;
                                   // echo "<br>" . (!in_array($value->getId(), $category_courseunitArray) && (count($category_courseunitArray) <= (int) $limit))  ;
                                    if (!in_array($value->getId(), $category_courseunitArray) && (count($category_courseunitArray) < (int) $limit)) {
                                        $url = $value->getYoutubelink();
                                        $img_url = '';
                                        $query_v = '';
                                        $views = 0;
                                        $time = $flag = $publishedAt = "";
                                        // $category_id = 17;
                                        $course_type = $value->getCourseType();


                                        $course_id_arr = [];
                                        $course_name = '';
                                        $course_unit_relation_list = $em
                                                ->getRepository('AdminBundle:course_unit_relation')
                                                ->findBy(array('is_deleted' => "0", 'course_unit_id' => $value->getId(), 'relation_with' => 'course'));
                                        if ($course_unit_relation_list) {
                                            foreach ($course_unit_relation_list as $delkey => $delval) {
                                                $course_id_arr[] = $delval->getRelation_object_id();
                                            }
                                        }

                                        if ($course_id_arr) {
                                            foreach ($course_id_arr as $ckey => $cval) {
                                                $courseC = $em->getRepository('AdminBundle:coursemaster')->find($cval);
                                                if (!empty($courseC)) {
                                                    if ($course_name == "") {
                                                        $course_name = $courseC->getCourseName();
                                                    } else {
                                                        $course_name = $course_name . " , " . $courseC->getCourseName();
                                                    }
                                                }
                                            }
                                        }

                                        $course_type = $value->getCourseType();
                                        $course_Tags = $value->getTag_id();
                                        $course_tag_list = NULL;
                                        if (!empty($course_Tags) && $course_Tags != '' && $course_Tags != NULL) {
                                            $course_Tags = explode(",", $course_Tags);

                                            foreach ($course_Tags as $tagval) {
                                                $course_tag_info = $this->getDoctrine()->getManager()->getRepository('AdminBundle:course_unit_tag')->find($tagval);
                                                if ($course_tag_info) {
                                                    $course_tag_list[] = array("tag_id" => $tagval, "tag_name" => $course_tag_info->getTag_name());
                                                }
                                            }
                                        }
                                        $category_courseunitArray[] = $value->getId();
                                        $category_class_list[] = array(
                                            "main_id" => $value->getId(),
                                            "unit_code" => $value->getUnitCode(),
                                            "title" => $value->getUnitName(),
                                            "cover_image" => $this->getmedia1Action($value->getClass_cover_image(), $this->container->getParameter('live_path')),
                                            "course_type" => $course_type,
                                            //  "category_id"=>$category_id,
                                            "youtubelink" => $url,
                                            "privateflag" => $value->getPrivateflag(),
                                            "live_type" => $value->getPrerequisites(),
                                            "course_type" => $value->getCourseType(),
                                            "course_name" => $course_name,
                                            "tags" => $course_tag_list
                                        );
                                    }
                                }
                            }
                        }
                    }

                    $outer_category_arr[] = array(
                        "id" => $oval->getId(),
                        "category_name" => $oval->getCategoryName(),
                        "category_desc" => $oval->getCategoryDescription(),
                        "classlist" => $category_class_list,
                       
                    );
                }
                // exit;
                $response = $outer_category_arr;
                $this->error = "SFD";
            } else {
                $response = false;
                $this->error = "NRF";
            }
        } else {
            $this->error = "PIM";
        }

        if (empty($response)) {
            $response = false;
        }
        $this->data = $response;
        return $this->responseAction();
    }

}
