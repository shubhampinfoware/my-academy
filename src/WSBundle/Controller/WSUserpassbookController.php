<?php

namespace WSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AdminBundle\Controller\CouponController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use AdminBundle\Entity\Userinfo;
use AdminBundle\Entity\Rolemaster;
use AdminBundle\Entity\Gcmuser;
use AdminBundle\Entity\Apnsuser;
use AdminBundle\Entity\Contactus;

class WSUserpassbookController extends WSBaseController {

    /**
     * @Route("/ws/userpassbook/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function userpassbookAction(Request $request) {
        $this->title = "User Passbook";
        $param = $this->requestAction($request, 0);
        $this->validateRule = array(
            array(
                'rule' => 'NOTNULL',
                'field' => array('user_id'),
            ),
        );
        if ($this->validateData($param)) {
            $user_id = $param->user_id;
            $repository = $this->getDoctrine()->getManager()->getRepository('AdminBundle:wallet_master');
            $wallet_master = $repository->findOneBy(array("user_master_id" => $user_id));

            $wallet_transactionarray = null;
            if ($wallet_master) {
                $totalvirtualmoney = $wallet_master->getVirtual_money_balance();
                $totalactualmoney = $wallet_master->getActual_money_balance();
                $cashback_balance = $wallet_master->getCashback_balance();

                $totalmoney = $totalvirtualmoney + $totalactualmoney + $cashback_balance;

                $repository = $this->getDoctrine()->getManager()->getRepository('AdminBundle:wallet_transaction_history');
                if (isset($param->balance_type)) {
                    $wallet_transaction = $repository->findBy(array(
                        "user_id" => $user_id,
                        "balance_type" => $param->balance_type
                    ));
                } else {
                    $wallet_transaction = $repository->findBy(array("user_id" => $user_id));
                }

                foreach ($wallet_transaction as $key => $value) {

                    $wallet_transactionarray[] = array(
                        "wallet_trans_id" => $value->getId(),
                        'wallet_transaction_type' => $value->getTransacation_operation(),
                        'wallet_transaction_amount' => $value->getWallet_trasaction_amount(),
                        'date_time' => $value->getTransaction_created_datetime(),
                        'balance_type' => $value->getBalance_type()
                    );
                }
                $response = array(
                    "totalmoney" => $totalmoney,
                    "totalvirtualmoney" => $totalvirtualmoney,
                    "totalactualmoney" => $totalactualmoney,
                    "cashback_balance" => $cashback_balance,
                    "wallet_transaction_list" => $wallet_transactionarray
                );
            }
            else{
                $this->error = "WNF";
                $response = false ;
            }
        } else {
            $this->error = "PIM";
        }
        if (empty($response)) {
            $response = False;
        }
        $this->data = $response;
        return $this->responseAction();
    }

}
