<?php
namespace WSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AdminBundle\Controller\CouponController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use AdminBundle\Entity\Userinfo ;
use AdminBundle\Entity\Rolemaster ;
use AdminBundle\Entity\Gcmuser ;
use AdminBundle\Entity\Apnsuser ;
use AdminBundle\Entity\generalnotification;

class WSCronupdatefideratingController extends WSBaseController
{
/**
     * @Route("/ws/updatefiderating/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function updatefideratingAction(Request $request){

    	 $this->title = "update fide rating" ;
            $param = $this->requestAction($request, 0);
            // use to validate required param validation
            $em = $this->getDoctrine()->getManager();
            $this->validateRule = array(
                array(
                    'rule'=>'NOTNULL',
                    'field'=>array(''),
                ),
            );
            if($this->validateData($param)){

            	
            }
             else {
                $this->error = "PIM";
            }
            if (empty($response)) {
                $response = false;
            }
            $this->data = $response;
            return $this->responseAction();

    }
}