<?php

namespace WSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AdminBundle\Controller\CouponController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use AdminBundle\Entity\Userinfo;
use AdminBundle\Entity\Rolemaster;
use AdminBundle\Entity\Gcmuser;
use AdminBundle\Entity\Apnsuser;
use AdminBundle\Entity\users;

class WSCategoryController extends WSBaseController {

    /**
     * @Route("/ws/getcategory/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function getcategoryAction(Request $request) {

        $this->title = "get category";
        $param = $this->requestAction($request, 0);

        $this->validateRule = array(
            array(
                'rule' => 'NOTNULL',
                'field' => array(),
            ),
        );

        if ($this->validateData($param)) {
            $flag = '' ;
             $em = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Categorymaster');
            if(isset($param->flag)){
                $flag = $param->flag ;
                $categorymaster = $em->findBy(array('is_deleted' => "0","category_type"=>$flag));
            }
            else{
                $categorymaster = $em->findBy(array('is_deleted' => "0"));
            }
           
            




            if (!empty($categorymaster)) {
                foreach ($categorymaster as $categorymaster) {
                   
                    
                            $response[] = array(
                                "id" => $categorymaster->getId(),
                                "name" => $categorymaster->getCategoryName(),
                                "description" => $categorymaster->getCategoryDescription(),
                                "sort_order" => $categorymaster->getSort_order(),
                                 "tag"=>$categorymaster->getCategory_type(),
                                "category_image" => $this->getimage($categorymaster->getCategoryImageId())
                            );
                       
                    
                }
                $this->error = "SFD";
                $this->data = $response;
            } else {
                $response = false;
                $this->error = "NRF";
            }
        } else {
            $this->error = "PIM";
        }

        if (empty($response)) {
            $response = false;
            $this->error = "NRF";
        }

        $this->data = $response;
        return $this->responseAction();
    }

}

?>
