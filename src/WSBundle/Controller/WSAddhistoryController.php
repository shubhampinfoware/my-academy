<?php

namespace WSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AdminBundle\Controller\CouponController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use AdminBundle\Entity\Userinfo;
use AdminBundle\Entity\Rolemaster;
use AdminBundle\Entity\Gcmuser;
use AdminBundle\Entity\Apnsuser;
use AdminBundle\Entity\users;
use AdminBundle\Entity\coursemaster;
use AdminBundle\Entity\user_history_video;

class WSAddhistoryController extends WSBaseController {

    /**
     * @Route("/ws/addhistroy/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function addhistroyAction(Request $request) {

        $this->title = "add histroy";
        $param = $this->requestAction($request, 0);

        $em = $this->getDoctrine()->getManager();
        $this->validateRule = array(
            array(
                'rule' => 'NOTNULL',
                'field' => array("user_id", "main_id", "course_id"),
            ),
        );
        if ($this->validateData($param)) {
            $user_id = $param->user_id;
            $main_id = $param->main_id;
            $course_id = $param->course_id;
            $Param_course_id_arr = explode(",", $course_id);
            $user_package_relation = $this->getDoctrine()->getManager()->getRepository('AdminBundle:user_package_relation')->findBy(array(
                "user_id" => $user_id,
                "is_deleted" => "0"
            ));
            $courseidarray = [];
            foreach ($user_package_relation as $key => $value1) {
                $subscription_master = $this->getDoctrine()->getManager()->getRepository('AdminBundle:subscription_master')->find($value1->getPackageId());
                if (!empty($subscription_master)) {
                    $courseids = $subscription_master->getCourseId();
                    foreach ($courseids as $key => $ids) {
                        $courseidarray[] = $ids;
                    }
                }
            }
            foreach ($Param_course_id_arr as $pckey => $pcval) {
                if (in_array($pcval, $courseidarray)) {
                    $user_history_video = $userMaster = $em->getRepository("AdminBundle:user_history_video")
                            ->findOneBy(array(
                        "main_id" => $main_id,
                        "course_id" => $course_id,
                        "user_id" => $user_id,
                        "is_deleted" => "0"
                            )
                    );
                    if (empty($user_history_video)) {
                        $user_history_video = new user_history_video();
                        $user_history_video->setUser_id($user_id);
                        $user_history_video->setMainId($main_id);
                        $user_history_video->setCourseId($pcval);
                        $user_history_video->setExtraId(""); //for user_id
                        $user_history_video->setExtra1Id("");
                        $user_history_video->setIs_deleted("0");
                        $em->persist($user_history_video);
                        $em->flush();
                        $this->error = "SFD";
                        $response = true;
                    }
                }
            }

            $this->error = "SFD";
            $response = true;
        } else {
            $this->error = "PIM";
        }

        if (empty($response)) {
            $response = false;
        }
        $this->data = $response;
        return $this->responseAction();
    }

    /**
     * @Route("/ws/historylist/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function histroyAction(Request $request) {

        $this->title = "history list";
        $param = $this->requestAction($request, 0);

        $em = $this->getDoctrine()->getManager();
        $this->validateRule = array(
            array(
                'rule' => 'NOTNULL',
                'field' => array("user_id"),
            ),
        );
        if ($this->validateData($param)) {
            $user_id = $param->user_id;

            $user_history_video = $userMaster = $em->getRepository("AdminBundle:user_history_video")
                    ->findBy(array(
                "user_id" => $user_id,
                "is_deleted" => "0"
                    )
            );

            $newdata = null;
            if (!empty($user_history_video)) {
                foreach ($user_history_video as $key => $value) {
                    $main_id = $value->getMainId();
                    $course_unit = $this->getDoctrine()->getManager()->getRepository('AdminBundle:course_unit')->find($main_id);

                    $url1 = $course_unit->getYoutubelink();
                    $parts = parse_url($url1);
                    parse_str($parts['query'], $query1);
                    $json = file_get_contents("https://www.googleapis.com/youtube/v3/videos?part=snippet%2CcontentDetails%2Cstatistics&id=" . $query1['v'] . "&key=AIzaSyAS2T7d5SqW81JNqAmICfTtEVRNiOxtAYY");
                    $obj = json_decode($json);
                    $id = sizeof($obj->items);
                    $category_id = 17;
                    $views = 0;
                    $publishedAt = date("d-M-Y");
                    $time1 = "";
                    if ($id > 0) {
                        $views1 = $obj->items[0]->statistics->viewCount;

                        $duration = $obj->items[0]->contentDetails->duration;

                        $data2 = substr($duration, 2);

                        $data3 = substr($data2, 0, strlen($data2) - 1);

                        $time1 = "";
                        if (strpos($data3, 'M') !== false) {
                            $array = explode("M", $data3);
                            if (strpos($array[0], 'H') !== false) {
                                $hours = explode("H", $array[0]);
                                $time1 = $hours[0] . ":" . $hours[1] . ":" . $array[1];
                            } else {
                                $time1 = $array[0] . ":" . $array[1];
                            }
                        } else {
                            $time1 = $data3;
                        }



                        $category_id1 = $obj->items[0]->snippet->categoryId;
                        $publishedAt1 = date("d-M-Y", strtotime($obj->items[0]->snippet->publishedAt));
                        $flag1 = false;
                    } else {
                        $flag1 = true;
                    }
                    $json = file_get_contents("https://www.googleapis.com/youtube/v3/videoCategories?part=snippet&id=" . $category_id . "&key=AIzaSyAS2T7d5SqW81JNqAmICfTtEVRNiOxtAYY");

                    $obj = json_decode($json);
                    $id = sizeof($obj->items);
                    if ($id > 0) {
                        $category_name1 = $obj->items[0]->snippet->title;
                    }
                    $coursemaster_id = $course_unit->getCoursemasterId();
                    $coursemaster = $this->getDoctrine()->getManager()->getRepository('AdminBundle:coursemaster')->find($coursemaster_id);

                    $course_name = $coursemaster->getCourseName();
                    $newdata[] = array(
                        "main_id" => $course_unit->getId(),
                        "title" => $course_unit->getUnitName(),
                        "desc" => $course_unit->getInstruction(),
                        "youtubelink" => $url1,
                        "img_url" => 'https://img.youtube.com/vi/' . $query1['v'] . '/0.jpg',
                        "course_type" => $course_unit->getCourseType(),
                        "video_id" => $query1['v'],
                        "views" => $views1,
                        "category_name" => $category_name1,
                        'flag' => $flag1,
                        "course_name" => $course_name,
                        "course_id" => $coursemaster_id,
                        'publishedat' => $publishedAt1,
                        "time" => $time1,
                    );
                }
                $this->error = "SFD";
                $response = $newdata;
            } else {
                $this->error = "NRF";
                $response = null;
            }
        } else {
            $this->error = "PIM";
        }

        if (empty($response)) {
            $response = false;
        }
        $this->data = $response;
        return $this->responseAction();
    }

}
