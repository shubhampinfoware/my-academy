<?php

namespace WSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AdminBundle\Controller\CouponController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use AdminBundle\Entity\Userinfo;
use AdminBundle\Entity\Rolemaster;
use AdminBundle\Entity\Gcmuser;
use AdminBundle\Entity\Apnsuser;
use AdminBundle\Entity\users;

class WSClasslevelController extends WSBaseController {

    /**
     * @Route("/ws/classlevel_list/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function classlevel_listAction(Request $request) {

        $this->title = "get Session List";
        $param = $this->requestAction($request, 0);

        $this->validateRule = array(
            array(
                'rule' => 'NOTNULL',
                'field' => array(),
            ),
        );

        if ($this->validateData($param)) {

            $em = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Class_level');
            $class_level_list = $em->findBy(array('is_deleted' => "0","level_status"=>'active'));




            if (!empty($class_level_list)) {
                foreach ($class_level_list as $class_level_list) {
                    // level wise Course 
                    $courseUnitArr = [] ;
                    $Class_level_Arr = $this->getDoctrine()->getManager()->getRepository('AdminBundle:course_unit_level')
                            ->findBy(array('is_deleted' => "0", 'class_level_id' => $class_level_list->getId()));
                    if($Class_level_Arr){
                        foreach($Class_level_Arr as $key=>$val){
                            $courseUnitArr[] = $val->getCourse_unit_id();
                        }
                    }
              
                 $repository1 = $this->getDoctrine()->getManager()->getRepository('AdminBundle:course_unit');
                $level_wise_course = $repository1->createQueryBuilder()
                                ->field('is_deleted')->equals("0")
                                ->field('id')->in($courseUnitArr)                              
                                ->getQuery()->execute()->toArray();
                    
                       
                    $level_wise_course_arr = NULL ; 
                    if($level_wise_course){
                        foreach($level_wise_course as $lkey=>$lval){
                            $level_wise_course_arr[] = array(
                                "course_unit_id"=>$lval->getId(),
                                "course_unit_name"=>$lval->getUnitName(),
                                "cover_image" => $this->getmedia1Action($lval->getClass_cover_image(), $this->container->getParameter('live_path')),
                                "youtubelink" => $lval->getYoutubelink()
                            );
                        }
                    }
                    $response[] = array(
                        "id" => $class_level_list->getId(),
                        "name" => $class_level_list->getLevel_name(),
                        "description" => $class_level_list->getLevel_desc(),
                        "status" => $class_level_list->getLevel_status(),
                        "unit_list"=>$level_wise_course_arr
                        
                    );
                }
                $this->error = "SFD";
                $this->error_msg = "";
                $this->data = $response;
            } else {
                $response = false;
                $this->error = "NRF";
            }
        } else {
            $this->error = "PIM";
        }

        if (empty($response)) {
            $response = false;
            $this->error = "NRF";
        }

        $this->data = $response;
        return $this->responseAction();
    }

}

?>
