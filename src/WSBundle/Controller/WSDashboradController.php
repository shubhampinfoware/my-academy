<?php

namespace WSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AdminBundle\Controller\CouponController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use AdminBundle\Entity\Userinfo;
use AdminBundle\Entity\Rolemaster;
use AdminBundle\Entity\Gcmuser;
use AdminBundle\Entity\Apnsuser;
use AdminBundle\Entity\users;
use AdminBundle\Entity\coursemaster;
use AdminBundle\Entity\Tournament;

class WSDashboradController extends WSBaseController {

    /**
     * @Route("/ws/dashboard/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function dashboardAction(Request $request) {

        $this->title = "dashboard";
        $param = $this->requestAction($request, 0);

        $em = $this->getDoctrine()->getManager();
        $this->validateRule = array(
            array(
                'rule' => 'NOTNULL',
                'field' => array("user_id"),
            ),
        );
        if ($this->validateData($param)) {
            $coursetrainerid = [];
            $totaluser = 0;

            $no_session = $no_course = $totalcourse = $totalsub = 0;
            $newdata = $previousdata = null;
            $user_id = $param->user_id;
            $userMaster = $em->getRepository("AdminBundle:usermaster")
                    ->find($user_id);

            if (!empty($userMaster)) {
                $usertrainerrelation = $em->getRepository("AdminBundle:usertrainerrelation")
                        ->findOneBy(array(
                    "user_id" => $userMaster->getId(),
                        )
                );
                $userMaster->setLogindate(date('Y-m-d H:i:s'));
                $em->flush();
                if ($usertrainerrelation->getIs_approved() == "1") {
                    $role_id = $this->TRAINER_ROLE_ID;
                    $role = "trainer";
                    $coursetrainerone = $em
                            ->getRepository('AdminBundle:Coursetrainermaster')
                            ->findBy(array('is_deleted' => "0", 'usermaster_id' => $user_id));
                    $no_course = sizeof($coursetrainerone);
                    if (!empty($coursetrainerone)) {
                        foreach ($coursetrainerone as $key => $value) {
                            $coursetrainerid[] = $value->getCoursemasterId();
                        }
                    }
                } else {
                    $role_id = $this->LEARNER_ROLE_ID;
                    $role = "learner";
                }
            }

            $subscription_master = $this->getDoctrine()->getManager()->getRepository('AdminBundle:subscription_master')->findBy(array(
                "is_deleted" => "0"
            ));
            $courseusersub_ids = [];
            foreach ($subscription_master as $key => $value) {

                $coursedataid = $value->getCourseId();

                if (count(array_intersect($coursedataid, $coursetrainerid)) > 0) {
                    $user_package_relation = $this->getDoctrine()->getManager()->getRepository('AdminBundle:user_package_relation')->findBy(array(
                        "package_id" => $value->getId(),
                        "is_deleted" => "0"
                    ));
                    $totaluser = $totaluser + sizeof($user_package_relation);
                }
                $user_package_relationdata = $this->getDoctrine()->getManager()->getRepository('AdminBundle:user_package_relation')->findBy(array(
                    "user_id" => $user_id,
                    "package_id" => $value->getId(),
                    "is_deleted" => "0"
                ));
                if (!empty($user_package_relationdata)) {
                    $totalsub = $totalsub + 1;
                    $courseusersub_ids = array_merge($courseusersub_ids, $coursedataid);
                    $totalcourse = $totalcourse + sizeof($coursedataid);
                }
            }

            $repository = $this->getDoctrine()->getManager()->getRepository('AdminBundle:course_unit');
            $course_unit = $repository->createQueryBuilder()
                            ->field('created_by')
                            ->equals($user_id)
                            ->sort('id', 'desc')
                            ->getQuery()->execute()->toArray();
            $no_session = sizeof($course_unit);
            $response["user_enrolled"] = $totaluser;
            $response["no_session"] = $no_session;
            $response["no_course_assign"] = $no_course;
            $response["last_login"] = date('d-M-y,h:i A', strtotime($userMaster->getLogindate()));
            if ($role == "trainer") {
                $course_unitdata = $repository->createQueryBuilder()
                                ->field('created_by')
                                ->equals($user_id)
                                ->field('youtubelink')
                                ->notEqual("")
                                ->field('course_type')
                                ->equals("1")
                                ->sort('id', 'desc')
                                ->limit(5)
                                ->getQuery()->execute()->toArray();
            } else {
                $course_unitdata = $repository->createQueryBuilder()
                                ->field('coursemaster_id')
                                ->in($courseusersub_ids)
                                ->field('youtubelink')
                                ->notEqual("")
                                ->field('course_type')
                                ->equals("1")
                                ->sort('id', 'desc')
                                ->limit(5)
                                ->getQuery()->execute()->toArray();
            }
            if (!empty($course_unitdata)) {
                foreach ($course_unitdata as $key => $value) {
                    if ($value->getprerequisites() == 'youtube' || 1) {
                        $img_url = $category_name = $course_name = $coursemaster_id = $publishedAt = $time = '' ;
                        $views = 0;
                        $url = $value->getYoutubelink();
                        $flag = true;
                        if ($value->getprerequisites() == 'youtube') {
                            $parts = parse_url($url);

                            $json = file_get_contents("https://www.googleapis.com/youtube/v3/videos?part=snippet%2CcontentDetails%2Cstatistics&id=" . $query['v'] . "&key=AIzaSyAS2T7d5SqW81JNqAmICfTtEVRNiOxtAYY");
                            $obj = json_decode($json);
                            $id = sizeof($obj->items);
                           
                            $publishedAt = date("d-M-Y");
                            $time = "";
                            if ($id > 0) {
                                $views = $obj->items[0]->statistics->viewCount;
                                $duration = $obj->items[0]->contentDetails->duration;
                                $data2 = substr($duration, 2);
                                $data3 = substr($data2, 0, strlen($data2) - 1);
                                $time = "";
                                if (strpos($data3, 'M') !== false) {
                                    $array = explode("M", $data3);
                                    if (strpos($array[0], 'H') !== false) {
                                        $hours = explode("H", $array[0]);
                                        $time = $hours[0] . ":" . $hours[1] . ":" . $array[1];
                                    } else {
                                        $time = $array[0] . ":" . $array[1];
                                    }
                                } else {
                                    $time = $data3;
                                }
                                $category_id = $obj->items[0]->snippet->categoryId;
                                $publishedAt = date("d-M-Y", strtotime($obj->items[0]->snippet->publishedAt));
                                $flag = false;
                            } else {
                                $flag = true;
                            }
                            $img_url = 'https://img.youtube.com/vi/' . $query['v'] . '/0.jpg';

                            //var_dump( $youtubedata );exit;


                            $json = file_get_contents("https://www.googleapis.com/youtube/v3/videoCategories?part=snippet&id=" . $category_id . "&key=AIzaSyAS2T7d5SqW81JNqAmICfTtEVRNiOxtAYY");

                            $obj = json_decode($json);
                            $id = sizeof($obj->items);
                            if ($id > 0) {
                                $category_name = $obj->items[0]->snippet->title;
                            }
                        }
                        $course_type = $value->getCourseType();
                        //$coursemaster_id = $value->getCoursemasterId();
                        //$coursemaster = $this->getDoctrine()->getManager()->getRepository('AdminBundle:coursemaster')->find($coursemaster_id);

                        //$course_name = $coursemaster->getCourseName();

                        $course_id_arr = [];
                        $course_name = '';
                        $course_unit_relation_list = $em
                               ->getRepository('AdminBundle:course_unit_relation')
                               ->findBy(array('is_deleted' => "0", 'course_unit_id' =>  $value->getId(), 'relation_with' => 'course'));
                        if ($course_unit_relation_list) {
                            foreach ($course_unit_relation_list as $delkey => $delval) {
                                if(!in_array($delval->getRelation_object_id(),$course_id_arr)){
                                $course_id_arr[] = $delval->getRelation_object_id();
                                }
                            }
                        }

                        if ($course_id_arr) {
                            foreach ($course_id_arr as $ckey => $cval) {
                                $courseC = $em->getRepository('AdminBundle:coursemaster')->find($cval);
                                if (!empty($courseC)) {
                                    if ($course_name == "") {
                                        $course_name = $courseC->getCourseName();
                                    } else {
                                        $course_name = $course_name . " , " . $courseC->getCourseName();
                                    }
                                }
                            }
                        }
                        $newdata[] = array(
                            "main_id" => $value->getId(),
                            "title" => $value->getUnitName(),
                            "youtubelink" => $url,
                            "img_url" => $img_url,
                            "course_type" => $value->getCourseType(),
                           // "video_id" => $query['v'],
                            "views" => $views,
                            "category_name" => $category_name,
                            'flag' => $flag,
                            "course_name" => $course_name,
                            "course_id" => $coursemaster_id,
                            'publishedat' => $publishedAt,
                            "time" => $time,
                        );
                    }
                }
            }
            $response["upcomingdata"] = $newdata;
            if ($role == "trainer") {
                $course_unitdata1 = $repository->createQueryBuilder()
                                ->field('created_by')
                                ->equals($user_id)
                                ->field('course_type')
                                ->equals("0")
                                ->field('youtubelink')
                                ->notEqual("")
                                ->sort('id', 'desc')
                                ->limit(5)
                                ->getQuery()->execute()->toArray();
            } else {
                $course_unitdata1 = $repository->createQueryBuilder()
                                ->field('coursemaster_id')
                                ->in($courseusersub_ids)
                                ->field('course_type')
                                ->equals("0")
                                ->field('youtubelink')
                                ->notEqual("")
                                ->sort('id', 'desc')
                                ->limit(5)
                                ->getQuery()->execute()->toArray();
            }
            if (!empty($course_unitdata1)) {
                foreach ($course_unitdata1 as $key => $value) {
                    $url = $value->getYoutubelink();
                    $img_url = '';
                    $query_v = '';
                    $views = 0;
                    $time = "";
                    if ($value->getPrerequisites() == 'youtube') {
                        $parts = parse_url($url);
                        parse_str($parts['query'], $query);
                        $json = file_get_contents("https://www.googleapis.com/youtube/v3/videos?part=snippet%2CcontentDetails%2Cstatistics&id=" . $query['v'] . "&key=AIzaSyAS2T7d5SqW81JNqAmICfTtEVRNiOxtAYY");
                        $obj = json_decode($json);
                        $id = sizeof($obj->items);
                        $category_id = 17;

                        $publishedAt = date("d-M-Y");

                        if ($id > 0) {
                            $views = $obj->items[0]->statistics->viewCount;
                            $duration = $obj->items[0]->contentDetails->duration;
                            $data2 = substr($duration, 2);
                            $data3 = substr($data2, 0, strlen($data2) - 1);
                            $time = "";
                            if (strpos($data3, 'M') !== false) {
                                $array = explode("M", $data3);
                                if (strpos($array[0], 'H') !== false) {
                                    $hours = explode("H", $array[0]);
                                    $time = $hours[0] . ":" . $hours[1] . ":" . $array[1];
                                } else {
                                    $time = $array[0] . ":" . $array[1];
                                }
                            } else {
                                $time = $data3;
                            }
                            $category_id = $obj->items[0]->snippet->categoryId;
                            $publishedAt = date("d-M-Y", strtotime($obj->items[0]->snippet->publishedAt));
                            $flag = false;
                        } else {
                            $flag = true;
                        }
                        $course_type = $value->getCourseType();
                        if (($time != "0" or $time != 0) and $course_type == "1") {
                            $class_master = $em->getRepository('AdminBundle:course_unit')
                                    ->findOneBy(array('is_deleted' => "0", 'id' => $value->getId()));
                            $class_master->setCourseType("0");
                            $em = $this->getDoctrine()->getManager();
                            $em->flush();
                            $course_type = "0";
                        }

                        $json = file_get_contents("https://www.googleapis.com/youtube/v3/videoCategories?part=snippet&id=" . $category_id . "&key=AIzaSyAS2T7d5SqW81JNqAmICfTtEVRNiOxtAYY");

                        $obj = json_decode($json);
                        $id = sizeof($obj->items);
                        if ($id > 0) {
                            $category_name = $obj->items[0]->snippet->title;
                        }
                        $img_url = 'https://img.youtube.com/vi/' . $query['v'] . '/0.jpg';
                        $query_v = $query['v'];
                    } elseif ($value->getPrerequisites() == 'zoom') {
                        $flag = true;
                        $img_url = '';
                        $publishedAt = '';
                        $meeting_id = $value->getUnitCode();
                        $calledFunction = 'meetings/' . $meeting_id;
                        $request_url = $this->api_url . $calledFunction . '?access_token=' . $this->access_token;
                        $handle = curl_init($request_url);
                        $headers = array();
                        $headers[] = 'Content-Type: application/json';
                        curl_setopt($handle, CURLOPT_HTTPHEADER, $headers);
                        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
                        $response_curl = curl_exec($handle);
                        $httpcode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
                        $resp = json_decode($response_curl);
                        curl_close($handle);
                        if ($httpcode == 200) {
                            $publishedAt = $resp->created_at;
                        }
                    }
                    //var_dump( $youtubedata );exit;
                    $course_type = $value->getCourseType();
                    if (($time != "0" or $time != 0) and $course_type == "1") {
                        $class_master = $em->getRepository('AdminBundle:course_unit')
                                ->findOneBy(array('is_deleted' => "0", 'id' => $value->getId()));
                        $class_master->setCourseType("0");
                        $em = $this->getDoctrine()->getManager();
                        $em->flush();
                        $course_type = "0";
                    }


//                    $coursemaster_id = $value->getCoursemasterId();
//                    $coursemaster = $this->getDoctrine()->getManager()->getRepository('AdminBundle:coursemaster')->find($coursemaster_id);
//
//                    $course_name = $coursemaster->getCourseName();


                    $previousdata[] = array(
                        "main_id" => $value->getId(),
                        "title" => $value->getUnitName(),
                        "youtubelink" => $url,
                        "img_url" => $img_url,
                        "course_type" => $value->getCourseType(),
                        "video_id" => $query_v,
                        "views" => $views,
                        'flag' => $flag,
                        // "course_name" => $course_name,
                        // "course_id" => $coursemaster_id,
                        'publishedat' => $publishedAt,
                        "time" => $time,
                    );
                }
            }
            $user_history_video = $userMaster = $em->getRepository("AdminBundle:user_history_video")
                    ->findBy(array(
                "user_id" => $user_id,
                "is_deleted" => "0"
                    )
            );
            $classviewed = sizeof($user_history_video);
            $response["previousdata"] = $previousdata;
            $response["totalsub"] = $totalsub;
            $response["classviewed"] = $classviewed;
            $response["totalcourse"] = $totalcourse;
            $this->error = "SFD";
        } else {
            $this->error = "PIM";
        }

        if (empty($response)) {
            $this->error = "NRF";
            $response = false;
        }
        $this->data = $response;
        return $this->responseAction();
    }

}
