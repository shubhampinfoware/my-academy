<?php

namespace WSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AdminBundle\Controller\CouponController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use AdminBundle\Entity\Userinfo;
use AdminBundle\Entity\Rolemaster;
use AdminBundle\Entity\Gcmuser;
use AdminBundle\Entity\Apnsuser;
use AdminBundle\Entity\users;
use AdminBundle\Entity\coursemaster;
use AdminBundle\Entity\Tournament;

class WSCourselistController extends WSBaseController {

    /**
     * @Route("/ws/course/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function courseAction(Request $request) {

        $this->title = "course";
        $param = $this->requestAction($request, 0);

        $em = $this->getDoctrine()->getManager();
        $this->validateRule = array(
            array(
                'rule' => 'NOTNULL',
                'field' => array(
                    'domain_id'
                ),
            ),
        );
        if ($this->validateData($param)) {
            
            $domain_id = $param->domain_id;
            $user_id = isset($param->user_id) ? $param->user_id : '';
            $language_id = isset($param->language_id) ? $param->language_id : 1;

            $_sql = "SELECT * from coursemaster c, coursetrainermaster ct, usermaster u where c.main_coursemaster_id = ct.coursemaster_id and u.usermaster_id = ct.usermaster_id and c.course_status = 'Active' and c.domain_id = {$domain_id} and c.language_id = {$language_id} and c.is_deleted = 0 and ct.is_deleted = 0 and u.is_deleted = 0 group by main_coursemaster_id";
            $course_list = $this->firequery($_sql);
            if(empty($course_list)){
                $this->error = "NRF";
                $response = false;
                $this->data = $response;
                return $this->responseAction();
            }

            $subscribed_course_id_list = [];
            if($user_id != ''){
                $_sql = "SELECT * from user_package_relation u, subscription_master sub where u.package_id = sub.subscription_master_id and u.user_id = {$user_id} and u.is_deleted = 0";
                $subscribed_course_list = $this->firequery($_sql);

                if(!empty($subscribed_course_list)){
                    foreach($subscribed_course_list as $_subcription){
                        $_sql = "SELECT * from subscription_course_relation where is_deleted = 0 and subscription_id = {$_subcription['subscription_master_id']}";
                        $subscribed_courses = $this->firequery($_sql);

                        if(!empty($subscribed_courses)){
                            foreach($subscribed_courses as $_course){
                                $subscribed_course_id_list[] = $_course['course_id'];
                            }
                        }
                    }
                }
            }

            $response = [];
            foreach($course_list as $_course){

                $course_unit_list = [];

                $_query = "SELECT unit.* from course_unit_relation cur, course_unit unit where relation_object_id = {$_course['main_coursemaster_id']} and relation_with = 'course' and unit.domain_id = {$domain_id} and unit.language_id = {$language_id} and cur.is_deleted = 0 and unit.is_deleted = 0 group by main_course_unit_id";
                $selected_class = $this->firequery($_query);

                // session list
                $total_course_units = 0;
                $total_live_units = 0;
                $course_unit_list = [];
                if(!empty($selected_class)){
                    foreach($selected_class as $_class){

                        if(trim($_class['course_type']) == 1){
                            $total_live_units++;
                        }

                        $course_unit_list[] = array(
                            'main_course_unit_id' => $_class['main_course_unit_id'],
                            'unit_name' => $_class['unit_name']
                        );
                    }
                }

                $course_includes = array(
                    'total_course_units' => count($selected_class),
                    'total_live_units' => $total_live_units,
                );

                // associated trainers
                $_query = "SELECT * from usermaster u, coursetrainermaster ctm where u.usermaster_id = ctm.usermaster_id and ctm.coursemaster_id = {$_course['main_coursemaster_id']} and u.is_deleted = 0 and ctm.is_deleted = 0";
                $associated_trainer = $this->firequery($_query);

                $trainer_list = [];
                if(!empty($associated_trainer)){
                    foreach($associated_trainer as $_trainer){
                        $trainer_list[] = array(
                            'id' => $_trainer['usermaster_id'],
                            'firstname' => $_trainer['user_first_name'],
                            'lastname' => $_trainer['user_last_name'],
                            'email' => $_trainer['user_email_password'],
                            'user_mobile' => $_trainer['user_mobile'],
                            "image_url" => $this->getimage($_trainer['media_id'])
                        );
                    }
                }

                // review list
                $_query = "SELECT * from reviews_master r, usermaster u where r.created_by = u.usermaster_id and relation_id = {$_course['main_coursemaster_id']} and review_type = 'course' and lower(u.user_status) = 'active' and u.is_deleted = 0 and r.is_deleted = 0";
                $reviews = $this->firequery($_query);

                $review_list = [];
                if(!empty($reviews)){
                    foreach($reviews as $_review){
                        $created_by = array(
							'user_id' => $_review['usermaster_id'],
							'firstname' => $_review['user_first_name'],
							'lastname' => $_review['user_last_name'],
							'email' => $_review['user_email_password'],
							'user_mobile' => $_review['user_mobile'],
							'image_url' => $this->getimage($_review['media_id'])
						);

                        $review_list[] = array(
							'reviews_master_id' => $_review['reviews_master_id'],
							'review_text' => $_review['review_text'],
							'review_type' => $_review['review_type'],
							'ratings' => $_review['ratings'],
                            'datetime' => $_review['created_datetime'],
                            'created_by' => $created_by
						);
                    }
                }

                $is_subscribed = false;
                if(!empty($subscribed_course_id_list)){
                    if(in_array($_course['main_coursemaster_id'], $subscribed_course_id_list)){
                        $is_subscribed = true;
                    }
                }

                // redirect user to subscription detail page if not course not subscribed
                $subscription_id = 0;
                if(!$is_subscribed){
                    $_query = "SELECT * from subscription_course_relation where course_id = {$_course['main_coursemaster_id']} and is_deleted = 0";
                    $subscription_course_relation = $this->firequery($_query);

                    if(!empty($subscription_course_relation)){
                        $subscription_id = $subscription_course_relation[0]['subscription_id'];
                    }
                }

                $response[] = array(
                    'main_course_id' => $_course['main_coursemaster_id'],
                    'course_name' => $_course['course_name'],
                    'course_description' => $_course['course_description'],
                    'course_price' => $_course['course_price'],
                    'is_subscribed' => $is_subscribed,
                    'subscription_id' => $subscription_id,
                    'course_image_url' => $this->getimage($_course['course_image']),
                    'created_by' => trim($_course['user_first_name'].' '.$_course['user_last_name']),
                    'course_unit_list' => $course_unit_list,
                    'trainer_list' => $trainer_list,
                    'course_includes' => $course_includes,
                    'review_list' => $review_list
                );
                $this->error = "SFD";
            }

        } else {
            $this->error = "PIM";
        }

        if (empty($response)) {
            $this->error = "NRF";
            $response = false;
        }
        $this->data = $response;
        return $this->responseAction();
    }

}
