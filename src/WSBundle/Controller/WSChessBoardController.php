<?php
namespace WSBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AdminBundle\Controller\CouponController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use AdminBundle\Entity\Userinfo ;
use AdminBundle\Entity\Rolemaster ;
use AdminBundle\Entity\Gcmuser;
use AdminBundle\Entity\Apnsuser;
use AdminBundle\Entity\users;

class WSChessBoardController extends WSBaseController {

     /**
     * @Route("/ws/getChessBoard/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function getChessBoardAction(Request $request) {

        $this->title = "Get Chess Board";
        $param = $this->requestAction($request, 0);
        $em = $this->getDoctrine()->getManager();
        $this->validateRule = array(
            array(
                'rule' => 'NOTNULL',
                'field' => array(),
            ),
        );

		if ($this->validateData($param)) {
			
			$user_id = !empty($param->user_id) ? $param->user_id : 0; 
			
			$html = '<link rel="stylesheet" type="text/css" href="https://pgn.chessbase.com/CBReplay.css"/>
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<style>
			html, body {margin: 0; height: 100%; overflow: hidden}
			</style>
			<script src="https://pgn.chessbase.com/jquery-3.0.0.min.js"></script>
			<script src="https://pgn.chessbase.com/cbreplay.js" type="text/javascript"></script>
			<script>
				
			</script>
			';
			
			#get Chessboard of today
			$today = date('Y-m-d H:i:s');
			
			$iso_date = date(DATE_ISO8601, strtotime($today));
			
//			echo $iso_date;exit;
			
			$repository = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Chessgame');
			if($user_id != 0){
				
			$chess_game =  $repository->createQueryBuilder()
						->field('end_on')->gte($today)
						->field('start_on')->lte($today)
						->field('is_deleted')->equals("0")
						->field('player_one')->equals($user_id)
						->getQuery()->execute()->toArray();
						
				
			}else{

				$chess_game =  $repository->createQueryBuilder()
						->field('end_on')->gte($today)
						->field('start_on')->lte($today)
						->field('is_deleted')->equals("0")
						->getQuery()->execute()->toArray();
						
			
			}
			
		

			//echo"<pre>";print_r($chess_game);exit;	
			$pgn_file = '';
			if(!empty($chess_game)){
				foreach($chess_game as $game){
					$pgn_id = $game->getPgnId();
					$pgn_file = $this->getmedia1Action($pgn_id,$this->container->getParameter('live_path'));
				}
				
				if($pgn_file != ''){
				$file_data = file_get_contents("$pgn_file");
				//var_dump($file_data);exit;
				//echo $file_data;exit;
				$html .= '<div id="data"  style="width:100%;height:100%;margin-top: -27px;"><div class="cbreplay">
						'.$file_data.'
						</div></div>
						';
				}
				echo trim($html);exit;
				$response = array('chess_board'=>$html);
				$this->error = "SFD";
			
			}else{
				$chess_game =  $repository->createQueryBuilder()
						
						->field('is_deleted')->equals("0")
						->sort('createdAt', 'desc')
						->getQuery()->getSingleResult();
						//var_dump($chess_game);exit;
				if(!empty($chess_game)){		
					
					$pgn_id = $chess_game->getPgnId();

					$pgn_file = $this->getmedia1Action($pgn_id,$this->container->getParameter('live_path'));
					if($pgn_file != ''){
				$file_data = file_get_contents("$pgn_file");
				//var_dump($file_data);exit;
			//	echo $file_data;exit;
				$html .= '<div id="data" style="width:100%;height:100%;margin-top: -27px;"><div class="cbreplay">
						'.$file_data.'
						</div></div>
						';
				}
				echo trim($html);exit;
				$response = array('chess_board'=>$html);
				$this->error = "SFD";
				}

			}
			//echo trim($html);exit;
//			echo $pgn_file;exit;
			
			
			
					
/*			$html .= '<div style="width:500px;height:500px"><div class="cbreplay">

					[Event "World Championship 28th"]
					[White "Spassky, Boris V"]
					[Black "Fischer, Robert James"]
					[Site "Reykjavik"]
					[Result "1–0"]
					[Date "1972.08.06"]
					[WhiteElo "2660"]
					[BlackElo "2785"]

					1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 a6 6. Bg5 e6 7. f4 Qb6 8. Qd2 Qxb2 9. Nb3 Qa3 10. Bxf6 gxf6 11. Be2 h5 12. 0-0 Nc6 13. Kh1 Bd7 14. Nb1 Qb4 15. Qe3 d5 16. exd5 Ne7 17. c4 Nf5 18. Qd3 h4 19. Bg4 Nd6 20. N1d2 f5 21. a3 Qb6 22. c5 Qb5 23. Qc3 fxg4 24. a4 h3 25. axb5 hxg2+ 26. Kxg2 Rh3 27. Qf6 Nf5 28. c6 Bc8 29. dxe6 fxe6 30. Rfe1 Be7 31. Rxe6
					</div></div>';
					*/
			if(empty($response)){
				$this->error = "NRF";
			}else{
				$this->data = $response;					
			}

//			return new Response($html);		
		}else{
			$this->error = "PIM";
		}
		
		if(empty($response)){
			$response = false;
			$this->error = "NRF";
		}
		
		$this->data = $response;
		return $this->responseAction();		
    }
	
	/**
     * @Route("/ws/ChessBoardPageLink/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function ChessBoardPageLinkAction(Request $request){
		
        $this->title = "Chess Board Page Link";
        $param = $this->requestAction($request, 0);
        $this->validateRule = array(
            array(
                'rule' => 'NOTNULL',
                'field' => array(),
            ),
        );	
	
		if ($this->validateData($param)) {
			
			$hostname = $_SERVER["SERVER_NAME"];
			

			$url_dynamic = 'http://'.$hostname.''.$this->generateUrl('ws_wschessboard_getchessboard');
			$new_url =  'http://'.$hostname.''.$this->generateUrl('ws_wschessboard_getchessboard2');
					
			$response = array('url'=>$url_dynamic,'new_url'=>$new_url);
			$this->error = "SFD";
			
		}else{
			$this->error = "PIM";
		}
		
		if (empty($response)) {
            $response = false;
        }

        $this->data = $response;
        return $this->responseAction();
		
		
	}

	/**
     * @Route("/ws/getChessBoard2/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function getChessBoard2Action(Request $request) {

        $this->title = "Get Chess Board2";
        $param = $this->requestAction($request, 0);
        $em = $this->getDoctrine()->getManager();
        $this->validateRule = array(
            array(
                'rule' => 'NOTNULL',
                'field' => array(),
            ),
        );

		if ($this->validateData($param)) {
			
			$user_id = !empty($param->user_id) ? $param->user_id : 0; 
			
			$html = '<html><head>
              <base href="https://chessbase.in" target="_blank">
    <meta name="google-site-verification" content="He9eqmgNlDAWtjGgtSOBx4et7rUVdBQYpCxIMHF886Y" />
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate"/>
        <meta http-equiv="Pragma" content="no-cache"/>
        <meta http-equiv="Expires" content="0"/>
        <link rel="shortcut icon" href="/img/favicon.ico" type="image/x-icon">

            <meta property="og:title" content="Live Games of Hari and Adhiban from Tata Steel - ChessBase India" />
            <meta property="og:image" content="https://www.chessbase.in/images/Hari_30th_Bday1_jpg?size=800" />
            <meta name="description" content="It&#39;s a great moment for Indian chess. Two of our players are playing in the Masters section of the Tata Steel 2017 in Wijk Aan Zee...." />
            <meta property="og:description" content="It&#39;s a great moment for Indian chess. Two of our players are..." />
        
        <link href="https://fonts.googleapis.com/css?family=Oswald|Open+sans" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" type="text/css" href="https://chessbase.in/css/CBI.css?8491702" />
        <link rel="stylesheet" type="text/css" href="https://chessbase.in/css/Textstyles.css" />
        <link rel="stylesheet" type="text/css" href="https://pgn.chessbase.com/CBReplay.css">
        <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
        <meta property="og:url" content="https://www.chessbase.in/news/live-games-tata-steel" />
        <meta name="google-site-verification" content="Iu4IlZtWHZ0ViSJCfGeayJ0nZUm7Pj_gZOJxe9d3hgc" />
        <script>
          (function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,"script","//www.google-analytics.com/analytics.js","ga");

          ga("create", "UA-70257419-1", "auto");
          ga("send", "pageview");
        </script>

        
        <script>
        !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
        n.push=n;n.loaded=!0;n.version="2.0";n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
        document,"script","https://connect.facebook.net/en_US/fbevents.js");
        fbq("init", "382980155389533"); 
        fbq("track", "PageView");
        </script>
        <noscript><img height="1" width="1" style="display:none"
        src="https://www.facebook.com/tr?id=382980155389533&ev=PageView&noscript=1"
        /></noscript>
        
        

        
        <script type="text/javascript">
        (function(d,s,id){var z=d.createElement(s);z.type="text/javascript";z.id=id;z.async=true;z.src="//static.zotabox.com/9/2/92110be250490bb48b3102befbc59551/widgets.js";var sz=d.getElementsByTagName(s)[0];sz.parentNode.insertBefore(z,sz)}(document,"script","zb-embed-code"));
        </script>
        

        
        <script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a1bb6891d108f0012ed9dfe&product=sticky-share-buttons"></script>
        

        
        <script type="text/javascript">
            (function(d,s,id){var z=d.createElement(s);z.type="text/javascript";z.id=id;z.async=true;z.src="//static.zotabox.com/b/5/b5932cda75ec9614b9f50776a4638cdb/widgets.js";var sz=d.getElementsByTagName(s)[0];sz.parentNode.insertBefore(z,sz)}(document,"script","zb-embed-code"));
        </script>
         <script src="https://pgn.chessbase.com/jquery-3.0.0.min.js"></script>

        
    </head><body>';
			
			#get Chessboard of today
			$today = date('Y-m-d H:i:s');
			
			$iso_date = date(DATE_ISO8601, strtotime($today));
			
//			echo $iso_date;exit;
			
			$repository = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Chessgame');
			if($user_id != 0){
				
			$chess_game =  $repository->createQueryBuilder()
						->field('end_on')->gte($today)
						->field('start_on')->lte($today)
						->field('is_deleted')->equals("0")
						->field('player_one')->equals($user_id)
						->getQuery()->execute()->toArray();
						
				
			}else{

				$chess_game =  $repository->createQueryBuilder()
						->field('end_on')->gte($today)
						->field('start_on')->lte($today)
						->field('is_deleted')->equals("0")
						->getQuery()->execute()->toArray();
						
			
			}
			
		

			//echo"<pre>";print_r($chess_game);exit;	
			$pgn_file = '';
			if(!empty($chess_game)){
				foreach($chess_game as $game){
					$pgn_id = $game->getPgnId();
					$pgn_file = $this->getmedia1Action($pgn_id,$this->container->getParameter('live_path'));
				}
				
				if($pgn_file != ''){
				$file_data = file_get_contents("$pgn_file");
				//var_dump($file_data);exit;
				//echo $file_data;exit;
				$html .= '<div class="chess-game-pgn-editor" data-pgn=
						"'.str_replace('"','&quot;',$file_data).'"
						</div></body>
						';
				}
				//echo trim($html);exit;
				$response = array('chess_board'=>$html);
				$this->error = "SFD";
			
			}else{
				$chess_game =  $repository->createQueryBuilder()
						
						->field('is_deleted')->equals("0")
						->sort('createdAt', 'desc')
						->getQuery()->getSingleResult();
						//var_dump($chess_game);exit;
				if(!empty($chess_game)){		
					
					$pgn_id = $chess_game->getPgnId();

					$pgn_file = $this->getmedia1Action($pgn_id,$this->container->getParameter('live_path'));
					if($pgn_file != ''){
				$file_data = file_get_contents("$pgn_file");
				//var_dump($file_data);exit;
			//	echo $file_data;exit;
				$html .= '<div class="chess-game-pgn-editor" data-pgn=
						"'.str_replace('"','&quot;',$file_data).'"
						  </div></body>
						';
				}
				//echo trim($html);exit;
				$response = array('chess_board'=>$html);
				$this->error = "SFD";
				}

			}
			$html.='<script src="https://pgn.chessbase.com/cbreplay.js" type="text/javascript"></script>
        <script type="text/javascript" src="/scripts/pgnplayer.js" ></script>
        <script type="text/javascript" src="/scripts/util.js" ></script>
        <script type="text/javascript" src="/scripts/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="/scripts/jquery.countdown.min.js"></script><script>
        $( document ).ready(function() {
    $("#ztb-widget-tab").hide();
});
        </script>';
			echo trim($html);exit;
//			echo $pgn_file;exit;
			
			
			
					
/*			$html .= '<div style="width:500px;height:500px"><div class="cbreplay">

					[Event "World Championship 28th"]
					[White "Spassky, Boris V"]
					[Black "Fischer, Robert James"]
					[Site "Reykjavik"]
					[Result "1–0"]
					[Date "1972.08.06"]
					[WhiteElo "2660"]
					[BlackElo "2785"]

					1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 a6 6. Bg5 e6 7. f4 Qb6 8. Qd2 Qxb2 9. Nb3 Qa3 10. Bxf6 gxf6 11. Be2 h5 12. 0-0 Nc6 13. Kh1 Bd7 14. Nb1 Qb4 15. Qe3 d5 16. exd5 Ne7 17. c4 Nf5 18. Qd3 h4 19. Bg4 Nd6 20. N1d2 f5 21. a3 Qb6 22. c5 Qb5 23. Qc3 fxg4 24. a4 h3 25. axb5 hxg2+ 26. Kxg2 Rh3 27. Qf6 Nf5 28. c6 Bc8 29. dxe6 fxe6 30. Rfe1 Be7 31. Rxe6
					</div></div>';
					*/
			if(empty($response)){
				$this->error = "NRF";
			}else{
				$this->data = $response;					
			}

//			return new Response($html);		
		}else{
			$this->error = "PIM";
		}
		
		if(empty($response)){
			$response = false;
			$this->error = "NRF";
		}
		
		$this->data = $response;
		return $this->responseAction();		
    }

}

?>
