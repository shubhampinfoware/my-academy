<?php
namespace WSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AdminBundle\Controller\CouponController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use AdminBundle\Entity\Userinfo ;
use AdminBundle\Entity\Rolemaster ;
use AdminBundle\Entity\Gcmuser ;
use AdminBundle\Entity\Apnsuser ;
use AdminBundle\Entity\bookmark;

class WSAddbookmarkController extends WSBaseController
{
/**
     * @Route("/ws/addbookmark/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function addbookmarkAction(Request $request){
        //try{
            $this->title = "Add Bookmark" ;
            $param = $this->requestAction($request, 0);
            // use to validate required param validation
            $em = $this->getDoctrine()->getManager();
            $this->validateRule = array(
                array(
                    'rule'=>'NOTNULL',
                    'field'=>array('user_id','item_id','type'),
                ),
            );
            if($this->validateData($param)){
            	$user_id = $param->user_id;
            	$video_id = $param->item_id;
              $type = $param->type;
              //type(news/tournament/video)
            	$Bookmark = $em->getRepository("AdminBundle:bookmark")
                                ->findOneBy(array(
                                        "userid"=>$user_id,
                                        "type"=>$type,
                                        "videoid"=>$video_id,
                                        "isdeleted"=>"0"
                                       
                                    )
                                ) ;
               	if(empty($Bookmark)){
               		$Bookmark = new bookmark();
               		$Bookmark->setUser_id($user_id);
                  $Bookmark->setType($type);
               		$Bookmark->setVideoid($video_id);
               		$Bookmark->setIs_deleted(0);
               		$em->persist($Bookmark);
                    $em->flush();
               	}else{
               		$Bookmark->setIs_deleted(1);
               		$em->flush();
               	}                 
               	 $this->error = "SFD";
                 $response = true;
            } else {
                $this->error = "PIM";
            }
            if (empty($response)) {
                $response = false;
            }
            $this->data = $response;
            return $this->responseAction();
        }


        /**
     * @Route("/ws/mybookmark/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function mybookmarkAction(Request $request){
        //try{
            $this->title = " Bookmark" ;
            $param = $this->requestAction($request, 0);
            // use to validate required param validation
            $em = $this->getDoctrine()->getManager();
            $this->validateRule = array(
                array(
                    'rule'=>'NOTNULL',
                    'field'=>array('user_id'),
                ),
            );
            if($this->validateData($param)){
              $user_id = $param->user_id;
            
             $video = null;
              $tournaments = null;
              $Bookmark = $em->getRepository("AdminBundle:bookmark")
                                ->findBy(array(
                                        "userid"=>$user_id,
                                        "isdeleted"=>"0"
                                       
                                    )
                                ) ;
                                $datarray = null;
                  if(!empty($Bookmark)){              
                 foreach ($Bookmark as $key => $value) {
                    $type = $value->getType();
                    if($type == 'video'){
                      $repository2 = $this->getDoctrine()->getManager()->getRepository('AdminBundle:videomaster');
                      $videomaster =   $repository2->findOneBy(
                                array(
                                    'is_deleted' => "0",
                                    'id'=>$value->getVideoid()
                                )
                            );
                      if(!empty( $videomaster)){
                      $video[] = array(
                            "main_id" => $videomaster->getId(),
                            "video_name"=>$videomaster->getName(),
                            "video_id"=>$videomaster->getVideoId(),
                            "view_count"=>$videomaster->getViewcount(),
                            "description"=>$videomaster->getDescription(),
                            "is_like"=>true,
                            "image_url" =>'https://img.youtube.com/vi/'.$videomaster->getVideoId().'/0.jpg'

                        );
                      }
                      }elseif($type == 'news'){
                         $repository2 = $this->getDoctrine()->getManager()->getRepository('AdminBundle:news');
                      $news =   $repository2->findOneBy(
                                array(
                                    'is_deleted' => "0",
                                    'id'=>$value->getVideoid()
                                )
                            );
                      if(!empty( $news)){
                        $now = $news->getDate();
                   


                      $date = $now->format('Y-m-d H:i:s');
                     $newdata[] = array(
                        "main_id" => $news->getId(),
                        "title"=>$news->getEditing()["title"],
                        "description"=>$news->getEditing()["intro"],
                        "author"=>$news->getEditing()["author"],
                        
                        "date"=>$now->getTimestamp(),
                        "is_like"=>true ,
                        "thumburl" =>'https://chessbase.in/images/'.$news->getLive()["thumburl"].'?size=256',
                        "imageurl"=>'https://chessbase.in/images/'.$news->getLive()["thumburl"],
                        "link"=>'https://chessbase.in/news/'.$news->getUrlname(),
                      

                    );
                      }

                      }
                    else{
                       $repository = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Tournament');
                        $tournament =   $repository->findOneBy(
                                array(
                                    'is_deleted' => "0",
                                    'id'=>$value->getVideoid()
                                )
                            );
                         $tournaments[] =  array(

                                'id' =>$tournament->getId() , 
                                 "is_like"=>true,
                                'title'=>$tournament->getTitle(),
                                'start'=>$tournament->getStart(),
                                "startstr" => date('d-M-Y', strtotime($tournament->getStart())),
                                'end'=>$tournament->getEnd(),
                                'endstr'=>$tournament->getEndstr(),
                                'city'=>$tournament->getCity(),
                                'state'=>$tournament->getState(),
                                'country'=>$tournament->getCountry(),
                                'pdfname'=>$tournament->getPdfname(),
                                'type'=>$tournament->getType(),
                                'desc'=>$tournament->getDesc(),
                                'venue'=>$tournament->getVenue(),
                                'prizecurrency'=>$tournament->getPrizecurrency(),
                                'totalprizefund'=>$tournament->getTotalprizefund(),
                                'prizefirst'=>$tournament->getPrizefirst(),
                                'prizesecond'=>$tournament->getPrizesecond(),
                                'prizethird'=>$tournament->getPrizethird(),
                                'organizername'=>$tournament->getOrganizername(),
                                'organizerphone'=>$tournament->getOrganizerphone(),
                                'organizeremail'=>$tournament->getOrganizeremail(),
                                'code'=>$tournament->getCode(),
                            );


                    }

                            
                       

                  } 
                   $response["video"] =    $video;
                   $response["tournament"] =    $tournaments;
                    $this->error = "SFD";
                }else{
                   $this->error = "NRF";
                }             
                
                
            } else {
                $this->error = "PIM";
            }
            if (empty($response)) {
                $response = false;
            }
            $this->data = $response;
            return $this->responseAction();
        }


}