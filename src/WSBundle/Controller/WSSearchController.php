<?php

namespace WSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AdminBundle\Controller\CouponController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use AdminBundle\Entity\Userinfo;
use AdminBundle\Entity\Rolemaster;
use AdminBundle\Entity\Gcmuser;
use AdminBundle\Entity\Apnsuser;
use AdminBundle\Entity\users;

class WSSearchController extends WSBaseController {

    /**
     * @Route("/ws/searchfilter/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function searchfilterAction(Request $request) {

        $this->title = "get category";
        $param = $this->requestAction($request, 0);

        $this->validateRule = array(
            array(
                'rule' => 'NOTNULL',
                'field' => array('filter_keyword'),
            ),
        );

        if ($this->validateData($param)) {
            $filter_keyword = $param->filter_keyword ;
            $flag = '' ;
            if(isset($param->flag)){
                $flag = $param->flag ;
            }
            $matchedCourseunitIds = $matchedCourseunitList = [] ; 
            $course_unit_repository = $this->getDoctrine()->getManager()->getRepository('AdminBundle:course_unit');
            $em = $this->getDoctrine()->getManager();
            $courseUnitList = $course_unit_repository->findBy(array('is_deleted'=>'0'));
            if($courseUnitList){
                foreach($courseUnitList as $cukey=>$cuval){
                    preg_match('/.*' . $filter_keyword . '(.+\n)*/i', $cuval->getUnitName(), $matches, PREG_OFFSET_CAPTURE);
                    preg_match('/.*' . ucfirst($filter_keyword) . '(.+\n)*/i', $cuval->getUnitName(), $matches1, PREG_OFFSET_CAPTURE);
                    if (!empty($matches) or ! empty($matches1) ) {
                        if(!in_array($cuval->getId() , $matchedCourseunitIds)) {
                            $matchedCourseunitIds[] = $cuval->getId();
                            $matchedCourseunitList[] = array(
                                "id"=>$cuval->getId(),
                                "unit_name"=>$cuval->getUnitName(),
                                "unit_img_id"=>$cuval->getCourseMediaId(),
                                "unit_img"=>$this->getimage($cuval->getClass_cover_image())
                            );
                        }
                    }
                
                }
            }
            //------category ---------------
            $matchedCategoryIds = $matchedCategoryList = [] ; 
            $Category_repository = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Categorymaster');
            $em = $this->getDoctrine()->getManager();
            $CategoryList = $Category_repository->findBy(array('is_deleted'=>'0'));
            if($CategoryList){
                foreach($CategoryList as $cukey=>$cuval){
                    preg_match('/.*' . $filter_keyword . '(.+\n)*/i', $cuval->getCategoryName(), $matches, PREG_OFFSET_CAPTURE);
                    preg_match('/.*' . ucfirst($filter_keyword) . '(.+\n)*/i', $cuval->getCategoryName(), $matches1, PREG_OFFSET_CAPTURE);
                    if (!empty($matches) or ! empty($matches1) ) {
                        if(!in_array($cuval->getId() , $matchedCourseunitIds)) {
                            $matchedCategoryIds[] = $cuval->getId();
                            $matchedCategoryList[] = array(
                                "id"=>$cuval->getId(),
                                "category_name"=>$cuval->getCategoryName(),
                                "category_img_id"=>$cuval->getCategoryImageId(),
                                "category_img"=>$this->getimage($cuval->getCategoryImageId())
                            );
                        }
                    }
                
                }
            }
            $search_arr = array(
                "filter_class"=>$matchedCourseunitList,
                "filter_category"=>$matchedCategoryList
            );   
            $response = $search_arr ;
            $this->error = "SFD";
           
        } else {
            $this->error = "PIM";
        }

        if (empty($response)) {
            $response = false;
            $this->error = "NRF";
        }

        $this->data = $response;
        return $this->responseAction();
    }

}

?>
