<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="userinfo")
*/
class Userinfo
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $_id;

	/**
	* @ORM\Column(type="string")
	*/
	protected $birthdate="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $createddate="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $email="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $fide="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $firstname="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $isdeleted=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $lastname="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $logindate="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $mediaid="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $mobile="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $roleid="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $userid="";

	public function get_id()
	{
		return $this->_id;
	}

	public function getBirthdate()
	{
		return $this->birthdate;
	}
	public function setBirthdate($birthdate)
	{
		$this->birthdate = $birthdate;
	}

	public function getCreateddate()
	{
		return $this->createddate;
	}
	public function setCreateddate($createddate)
	{
		$this->createddate = $createddate;
	}

	public function getEmail()
	{
		return $this->email;
	}
	public function setEmail($email)
	{
		$this->email = $email;
	}

	public function getFide()
	{
		return $this->fide;
	}
	public function setFide($fide)
	{
		$this->fide = $fide;
	}

	public function getFirstname()
	{
		return $this->firstname;
	}
	public function setFirstname($firstname)
	{
		$this->firstname = $firstname;
	}

	public function getIsdeleted()
	{
		return $this->isdeleted;
	}
	public function setIsdeleted($isdeleted)
	{
		$this->isdeleted = $isdeleted;
	}

	public function getLastname()
	{
		return $this->lastname;
	}
	public function setLastname($lastname)
	{
		$this->lastname = $lastname;
	}

	public function getLogindate()
	{
		return $this->logindate;
	}
	public function setLogindate($logindate)
	{
		$this->logindate = $logindate;
	}

	public function getMediaid()
	{
		return $this->mediaid;
	}
	public function setMediaid($mediaid)
	{
		$this->mediaid = $mediaid;
	}

	public function getMobile()
	{
		return $this->mobile;
	}
	public function setMobile($mobile)
	{
		$this->mobile = $mobile;
	}

	public function getRoleid()
	{
		return $this->roleid;
	}
	public function setRoleid($roleid)
	{
		$this->roleid = $roleid;
	}

	public function getUserid()
	{
		return $this->userid;
	}
	public function setUserid($userid)
	{
		$this->userid = $userid;
	}
}