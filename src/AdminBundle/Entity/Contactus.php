<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="contactus")
*/
class Contactus
{
	/**
	* @ORM\Column(type="integer")
	*/
	protected $Userid=0;

	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $contactus_id;

	/**
	* @ORM\Column(type="string")
	*/
	protected $email="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $contact_no="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $is_deleted="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $message="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $title="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $domain_id=0;

	public function getUserid()
	{
		return $this->Userid;
	}
	public function setUserid($Userid)
	{
		$this->Userid = $Userid;
	}

	public function getContactus_id()
	{
		return $this->contactus_id;
	}

	public function getEmail()
	{
		return $this->email;
	}
	public function setEmail($email)
	{
		$this->email = $email;
	}

	public function getContact_no()
	{
		return $this->contact_no;
	}
	public function setContact_no($contact_no)
	{
		$this->contact_no = $contact_no;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}

	public function getMessage()
	{
		return $this->message;
	}
	public function setMessage($message)
	{
		$this->message = $message;
	}

	public function getTitle()
	{
		return $this->title;
	}
	public function setTitle($title)
	{
		$this->title = $title;
	}

	public function getDomain_id()
	{
		return $this->domain_id;
	}
	public function setDomain_id($domain_id)
	{
		$this->domain_id = $domain_id;
	}
}