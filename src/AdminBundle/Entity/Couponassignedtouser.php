<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="coupon_assigned_to_user")
*/
class Couponassignedtouser
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $coupon_assigned_to_user_id;

	/**
	* @ORM\Column(type="string")
	*/
	protected $assign_by="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $assign_datetime="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $coupon_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $is_deleted="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $use_before_days="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $user_id=0;

	public function getCoupon_assigned_to_user_id()
	{
		return $this->coupon_assigned_to_user_id;
	}

	public function getAssign_by()
	{
		return $this->assign_by;
	}
	public function setAssign_by($assign_by)
	{
		$this->assign_by = $assign_by;
	}

	public function getAssign_datetime()
	{
		return $this->assign_datetime;
	}
	public function setAssign_datetime($assign_datetime)
	{
		$this->assign_datetime = $assign_datetime;
	}

	public function getCoupon_id()
	{
		return $this->coupon_id;
	}
	public function setCoupon_id($coupon_id)
	{
		$this->coupon_id = $coupon_id;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}

	public function getUse_before_days()
	{
		return $this->use_before_days;
	}
	public function setUse_before_days($use_before_days)
	{
		$this->use_before_days = $use_before_days;
	}

	public function getUser_id()
	{
		return $this->user_id;
	}
	public function setUser_id($user_id)
	{
		$this->user_id = $user_id;
	}
}