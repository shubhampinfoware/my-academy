<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="playlistmaster")
*/
class Playlistmaster
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $playlistmaster_id;

	/**
	* @ORM\Column(type="string")
	*/
	protected $channelid="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $description="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $is_deleted="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $name="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $playlist_id=0;

	public function getPlaylistmaster_id()
	{
		return $this->playlistmaster_id;
	}

	public function getChannelid()
	{
		return $this->channelid;
	}
	public function setChannelid($channelid)
	{
		$this->channelid = $channelid;
	}

	public function getDescription()
	{
		return $this->description;
	}
	public function setDescription($description)
	{
		$this->description = $description;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}

	public function getName()
	{
		return $this->name;
	}
	public function setName($name)
	{
		$this->name = $name;
	}

	public function getPlaylist_id()
	{
		return $this->playlist_id;
	}
	public function setPlaylist_id($playlist_id)
	{
		$this->playlist_id = $playlist_id;
	}
}