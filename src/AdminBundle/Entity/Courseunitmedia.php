<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="course_unit_media")
*/
class Courseunitmedia
{
	/**
	* @ORM\Column(type="string")
	*/
	protected $UploadFileTitle="";

	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $course_unit_media_id;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $course_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $course_unit_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $media_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $media_type="";

	public function getUploadFileTitle()
	{
		return $this->UploadFileTitle;
	}
	public function setUploadFileTitle($UploadFileTitle)
	{
		$this->UploadFileTitle = $UploadFileTitle;
	}

	public function getCourse_unit_media_id()
	{
		return $this->course_unit_media_id;
	}

	public function getCourse_id()
	{
		return $this->course_id;
	}
	public function setCourse_id($course_id)
	{
		$this->course_id = $course_id;
	}

	public function getCourse_unit_id()
	{
		return $this->course_unit_id;
	}
	public function setCourse_unit_id($course_unit_id)
	{
		$this->course_unit_id = $course_unit_id;
	}

	public function getMedia_id()
	{
		return $this->media_id;
	}
	public function setMedia_id($media_id)
	{
		$this->media_id = $media_id;
	}

	public function getMedia_type()
	{
		return $this->media_type;
	}
	public function setMedia_type($media_type)
	{
		$this->media_type = $media_type;
	}
}