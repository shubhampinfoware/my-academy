<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="coursetrainermaster")
*/
class Coursetrainermaster
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $coursetrainermaster_id;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $coursemaster_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $domain_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $is_accepted="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $is_deleted="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $usermaster_id=0;

	public function getCoursetrainermaster_id()
	{
		return $this->coursetrainermaster_id;
	}

	public function getCoursemaster_id()
	{
		return $this->coursemaster_id;
	}
	public function setCoursemaster_id($coursemaster_id)
	{
		$this->coursemaster_id = $coursemaster_id;
	}

	public function getDomain_id()
	{
		return $this->domain_id;
	}
	public function setDomain_id($domain_id)
	{
		$this->domain_id = $domain_id;
	}

	public function getIs_accepted()
	{
		return $this->is_accepted;
	}
	public function setIs_accepted($is_accepted)
	{
		$this->is_accepted = $is_accepted;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}

	public function getUsermaster_id()
	{
		return $this->usermaster_id;
	}
	public function setUsermaster_id($usermaster_id)
	{
		$this->usermaster_id = $usermaster_id;
	}
}