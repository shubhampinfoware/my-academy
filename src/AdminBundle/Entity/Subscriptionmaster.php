<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="subscription_master")
*/
class Subscriptionmaster
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $subscription_master_id;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $course_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $created_by="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $created_date="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $end_date="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $extra="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $extra1="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $is_deleted="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $price="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $start_date="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $status="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $subscription_desc="";

	public function getSubscription_master_id()
	{
		return $this->subscription_master_id;
	}

	public function getCourse_id()
	{
		return $this->course_id;
	}
	public function setCourse_id($course_id)
	{
		$this->course_id = $course_id;
	}

	public function getCreated_by()
	{
		return $this->created_by;
	}
	public function setCreated_by($created_by)
	{
		$this->created_by = $created_by;
	}

	public function getCreated_date()
	{
		return $this->created_date;
	}
	public function setCreated_date($created_date)
	{
		$this->created_date = $created_date;
	}

	public function getEnd_date()
	{
		return $this->end_date;
	}
	public function setEnd_date($end_date)
	{
		$this->end_date = $end_date;
	}

	public function getExtra()
	{
		return $this->extra;
	}
	public function setExtra($extra)
	{
		$this->extra = $extra;
	}

	public function getExtra1()
	{
		return $this->extra1;
	}
	public function setExtra1($extra1)
	{
		$this->extra1 = $extra1;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}

	public function getPrice()
	{
		return $this->price;
	}
	public function setPrice($price)
	{
		$this->price = $price;
	}

	public function getStart_date()
	{
		return $this->start_date;
	}
	public function setStart_date($start_date)
	{
		$this->start_date = $start_date;
	}

	public function getStatus()
	{
		return $this->status;
	}
	public function setStatus($status)
	{
		$this->status = $status;
	}

	public function getSubscription_desc()
	{
		return $this->subscription_desc;
	}
	public function setSubscription_desc($subscription_desc)
	{
		$this->subscription_desc = $subscription_desc;
	}
}