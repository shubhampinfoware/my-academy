<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="userzoomrelation")
*/
class Userzoomrelation
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $userzoomrelation_id;

	/**
	* @ORM\Column(type="string")
	*/
	protected $is_deleted="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $user_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $zoom_insert_flag="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $zoom_insert_status="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $zoom_user_id=0;

	public function getUserzoomrelation_id()
	{
		return $this->userzoomrelation_id;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}

	public function getUser_id()
	{
		return $this->user_id;
	}
	public function setUser_id($user_id)
	{
		$this->user_id = $user_id;
	}

	public function getZoom_insert_flag()
	{
		return $this->zoom_insert_flag;
	}
	public function setZoom_insert_flag($zoom_insert_flag)
	{
		$this->zoom_insert_flag = $zoom_insert_flag;
	}

	public function getZoom_insert_status()
	{
		return $this->zoom_insert_status;
	}
	public function setZoom_insert_status($zoom_insert_status)
	{
		$this->zoom_insert_status = $zoom_insert_status;
	}

	public function getZoom_user_id()
	{
		return $this->zoom_user_id;
	}
	public function setZoom_user_id($zoom_user_id)
	{
		$this->zoom_user_id = $zoom_user_id;
	}
}