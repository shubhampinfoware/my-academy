<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="wallet_transaction_history")
*/
class Wallettransactionhistory
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $wallet_transaction_history_id;

	/**
	* @ORM\Column(type="string")
	*/
	protected $balance_type="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $is_deleted="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $transacation_operation="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $transaction_created_by="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $transaction_created_datetime="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $updated_balance="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $user_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $wallet_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $wallet_transaction_history_field_1="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $wallet_transaction_history_field_2="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $wallet_transaction_history_field_3="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $wallet_trasaction_amount="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $wallet_trasaction_id=0;

	public function getWallet_transaction_history_id()
	{
		return $this->wallet_transaction_history_id;
	}

	public function getBalance_type()
	{
		return $this->balance_type;
	}
	public function setBalance_type($balance_type)
	{
		$this->balance_type = $balance_type;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}

	public function getTransacation_operation()
	{
		return $this->transacation_operation;
	}
	public function setTransacation_operation($transacation_operation)
	{
		$this->transacation_operation = $transacation_operation;
	}

	public function getTransaction_created_by()
	{
		return $this->transaction_created_by;
	}
	public function setTransaction_created_by($transaction_created_by)
	{
		$this->transaction_created_by = $transaction_created_by;
	}

	public function getTransaction_created_datetime()
	{
		return $this->transaction_created_datetime;
	}
	public function setTransaction_created_datetime($transaction_created_datetime)
	{
		$this->transaction_created_datetime = $transaction_created_datetime;
	}

	public function getUpdated_balance()
	{
		return $this->updated_balance;
	}
	public function setUpdated_balance($updated_balance)
	{
		$this->updated_balance = $updated_balance;
	}

	public function getUser_id()
	{
		return $this->user_id;
	}
	public function setUser_id($user_id)
	{
		$this->user_id = $user_id;
	}

	public function getWallet_id()
	{
		return $this->wallet_id;
	}
	public function setWallet_id($wallet_id)
	{
		$this->wallet_id = $wallet_id;
	}

	public function getWallet_transaction_history_field_1()
	{
		return $this->wallet_transaction_history_field_1;
	}
	public function setWallet_transaction_history_field_1($wallet_transaction_history_field_1)
	{
		$this->wallet_transaction_history_field_1 = $wallet_transaction_history_field_1;
	}

	public function getWallet_transaction_history_field_2()
	{
		return $this->wallet_transaction_history_field_2;
	}
	public function setWallet_transaction_history_field_2($wallet_transaction_history_field_2)
	{
		$this->wallet_transaction_history_field_2 = $wallet_transaction_history_field_2;
	}

	public function getWallet_transaction_history_field_3()
	{
		return $this->wallet_transaction_history_field_3;
	}
	public function setWallet_transaction_history_field_3($wallet_transaction_history_field_3)
	{
		$this->wallet_transaction_history_field_3 = $wallet_transaction_history_field_3;
	}

	public function getWallet_trasaction_amount()
	{
		return $this->wallet_trasaction_amount;
	}
	public function setWallet_trasaction_amount($wallet_trasaction_amount)
	{
		$this->wallet_trasaction_amount = $wallet_trasaction_amount;
	}

	public function getWallet_trasaction_id()
	{
		return $this->wallet_trasaction_id;
	}
	public function setWallet_trasaction_id($wallet_trasaction_id)
	{
		$this->wallet_trasaction_id = $wallet_trasaction_id;
	}
}