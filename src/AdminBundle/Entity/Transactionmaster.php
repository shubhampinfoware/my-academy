<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="transaction_master")
*/
class Transactionmaster
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $transaction_master_id;

	/**
	* @ORM\Column(type="string")
	*/
	protected $amount="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $created_datetime="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $is_deleted="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $nameoncard="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $payment_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $product_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $ref_number="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $status="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $txn_field_1="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $txn_field_2="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $txn_field_3="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $txnid="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $unmappedstatus="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $user_id=0;

	public function getTransaction_master_id()
	{
		return $this->transaction_master_id;
	}

	public function getAmount()
	{
		return $this->amount;
	}
	public function setAmount($amount)
	{
		$this->amount = $amount;
	}

	public function getCreated_datetime()
	{
		return $this->created_datetime;
	}
	public function setCreated_datetime($created_datetime)
	{
		$this->created_datetime = $created_datetime;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}

	public function getNameoncard()
	{
		return $this->nameoncard;
	}
	public function setNameoncard($nameoncard)
	{
		$this->nameoncard = $nameoncard;
	}

	public function getPayment_id()
	{
		return $this->payment_id;
	}
	public function setPayment_id($payment_id)
	{
		$this->payment_id = $payment_id;
	}

	public function getProduct_id()
	{
		return $this->product_id;
	}
	public function setProduct_id($product_id)
	{
		$this->product_id = $product_id;
	}

	public function getRef_number()
	{
		return $this->ref_number;
	}
	public function setRef_number($ref_number)
	{
		$this->ref_number = $ref_number;
	}

	public function getStatus()
	{
		return $this->status;
	}
	public function setStatus($status)
	{
		$this->status = $status;
	}

	public function getTxn_field_1()
	{
		return $this->txn_field_1;
	}
	public function setTxn_field_1($txn_field_1)
	{
		$this->txn_field_1 = $txn_field_1;
	}

	public function getTxn_field_2()
	{
		return $this->txn_field_2;
	}
	public function setTxn_field_2($txn_field_2)
	{
		$this->txn_field_2 = $txn_field_2;
	}

	public function getTxn_field_3()
	{
		return $this->txn_field_3;
	}
	public function setTxn_field_3($txn_field_3)
	{
		$this->txn_field_3 = $txn_field_3;
	}

	public function getTxnid()
	{
		return $this->txnid;
	}
	public function setTxnid($txnid)
	{
		$this->txnid = $txnid;
	}

	public function getUnmappedstatus()
	{
		return $this->unmappedstatus;
	}
	public function setUnmappedstatus($unmappedstatus)
	{
		$this->unmappedstatus = $unmappedstatus;
	}

	public function getUser_id()
	{
		return $this->user_id;
	}
	public function setUser_id($user_id)
	{
		$this->user_id = $user_id;
	}
}