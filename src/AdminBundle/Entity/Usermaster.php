<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="usermaster")
*/
class Usermaster
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $usermaster_id;

	/**
	* @ORM\Column(type="string")
	*/
	protected $birthdate="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $create_date="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $domain_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $ext_field="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $ext_field1="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $external_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $is_deleted=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $logindate="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $media_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $register_from="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $user_email_password="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $user_first_name="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $user_last_name="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $user_mobile="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $user_password="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $user_role_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $user_status="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $age="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $parent_mobile="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $parent_name="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $user_bio="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $achievements="";

	public function getUsermaster_id()
	{
		return $this->usermaster_id;
	}

	public function getBirthdate()
	{
		return $this->birthdate;
	}
	public function setBirthdate($birthdate)
	{
		$this->birthdate = $birthdate;
	}

	public function getCreate_date()
	{
		return $this->create_date;
	}
	public function setCreate_date($create_date)
	{
		$this->create_date = $create_date;
	}

	public function getDomain_id()
	{
		return $this->domain_id;
	}
	public function setDomain_id($domain_id)
	{
		$this->domain_id = $domain_id;
	}

	public function getExt_field()
	{
		return $this->ext_field;
	}
	public function setExt_field($ext_field)
	{
		$this->ext_field = $ext_field;
	}

	public function getExt_field1()
	{
		return $this->ext_field1;
	}
	public function setExt_field1($ext_field1)
	{
		$this->ext_field1 = $ext_field1;
	}

	public function getExternal_id()
	{
		return $this->external_id;
	}
	public function setExternal_id($external_id)
	{
		$this->external_id = $external_id;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}

	public function getLogindate()
	{
		return $this->logindate;
	}
	public function setLogindate($logindate)
	{
		$this->logindate = $logindate;
	}

	public function getMedia_id()
	{
		return $this->media_id;
	}
	public function setMedia_id($media_id)
	{
		$this->media_id = $media_id;
	}

	public function getRegister_from()
	{
		return $this->register_from;
	}
	public function setRegister_from($register_from)
	{
		$this->register_from = $register_from;
	}

	public function getUser_email_password()
	{
		return $this->user_email_password;
	}
	public function setUser_email_password($user_email_password)
	{
		$this->user_email_password = $user_email_password;
	}

	public function getUser_first_name()
	{
		return $this->user_first_name;
	}
	public function setUser_first_name($user_first_name)
	{
		$this->user_first_name = $user_first_name;
	}

	public function getUser_last_name()
	{
		return $this->user_last_name;
	}
	public function setUser_last_name($user_last_name)
	{
		$this->user_last_name = $user_last_name;
	}

	public function getUser_mobile()
	{
		return $this->user_mobile;
	}
	public function setUser_mobile($user_mobile)
	{
		$this->user_mobile = $user_mobile;
	}

	public function getUser_password()
	{
		return $this->user_password;
	}
	public function setUser_password($user_password)
	{
		$this->user_password = $user_password;
	}

	public function getUser_role_id()
	{
		return $this->user_role_id;
	}
	public function setUser_role_id($user_role_id)
	{
		$this->user_role_id = $user_role_id;
	}

	public function getUser_status()
	{
		return $this->user_status;
	}
	public function setUser_status($user_status)
	{
		$this->user_status = $user_status;
	}

	public function getAge()
	{
		return $this->age;
	}
	public function setAge($age)
	{
		$this->age = $age;
	}

	public function getParent_mobile()
	{
		return $this->parent_mobile;
	}
	public function setParent_mobile($parent_mobile)
	{
		$this->parent_mobile = $parent_mobile;
	}

	public function getParent_name()
	{
		return $this->parent_name;
	}
	public function setParent_name($parent_name)
	{
		$this->parent_name = $parent_name;
	}

	public function getUser_bio()
	{
		return $this->user_bio;
	}
	public function setUser_bio($user_bio)
	{
		$this->user_bio = $user_bio;
	}

	public function getAchievements()
	{
		return $this->achievements;
	}
	public function setAchievements($achievements)
	{
		$this->achievements = $achievements;
	}
}