<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="chess_game")
*/
class Chessgame
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $chess_game_id;

	/**
	* @ORM\Column(type="string")
	*/
	protected $book_details.book_name="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $book_details.bookimage_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $book_details.shop_link="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $description="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $end_on="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $game_session_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $game_type="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $image_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $is_archieved="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $is_deleted="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $pgn_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $player_two="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $start_on="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $title="";

	public function getChess_game_id()
	{
		return $this->chess_game_id;
	}

	public function getBook_details.book_name()
	{
		return $this->book_details.book_name;
	}
	public function setBook_details.book_name($book_details.book_name)
	{
		$this->book_details.book_name = $book_details.book_name;
	}

	public function getBook_details.bookimage_id()
	{
		return $this->book_details.bookimage_id;
	}
	public function setBook_details.bookimage_id($book_details.bookimage_id)
	{
		$this->book_details.bookimage_id = $book_details.bookimage_id;
	}

	public function getBook_details.shop_link()
	{
		return $this->book_details.shop_link;
	}
	public function setBook_details.shop_link($book_details.shop_link)
	{
		$this->book_details.shop_link = $book_details.shop_link;
	}

	public function getDescription()
	{
		return $this->description;
	}
	public function setDescription($description)
	{
		$this->description = $description;
	}

	public function getEnd_on()
	{
		return $this->end_on;
	}
	public function setEnd_on($end_on)
	{
		$this->end_on = $end_on;
	}

	public function getGame_session_id()
	{
		return $this->game_session_id;
	}
	public function setGame_session_id($game_session_id)
	{
		$this->game_session_id = $game_session_id;
	}

	public function getGame_type()
	{
		return $this->game_type;
	}
	public function setGame_type($game_type)
	{
		$this->game_type = $game_type;
	}

	public function getImage_id()
	{
		return $this->image_id;
	}
	public function setImage_id($image_id)
	{
		$this->image_id = $image_id;
	}

	public function getIs_archieved()
	{
		return $this->is_archieved;
	}
	public function setIs_archieved($is_archieved)
	{
		$this->is_archieved = $is_archieved;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}

	public function getPgn_id()
	{
		return $this->pgn_id;
	}
	public function setPgn_id($pgn_id)
	{
		$this->pgn_id = $pgn_id;
	}

	public function getPlayer_two()
	{
		return $this->player_two;
	}
	public function setPlayer_two($player_two)
	{
		$this->player_two = $player_two;
	}

	public function getStart_on()
	{
		return $this->start_on;
	}
	public function setStart_on($start_on)
	{
		$this->start_on = $start_on;
	}

	public function getTitle()
	{
		return $this->title;
	}
	public function setTitle($title)
	{
		$this->title = $title;
	}
}