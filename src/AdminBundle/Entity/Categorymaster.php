<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="categorymaster")
*/
class Categorymaster
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $categorymaster_id;

	/**
	* @ORM\Column(type="string")
	*/
	protected $category_description="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $category_image_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $category_name="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $category_type="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $created_datetime="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $display_on_home="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $domain_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $is_deleted="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $language_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $main_category_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $parent_category_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $sort_order="";

	public function getCategorymaster_id()
	{
		return $this->categorymaster_id;
	}

	public function getCategory_description()
	{
		return $this->category_description;
	}
	public function setCategory_description($category_description)
	{
		$this->category_description = $category_description;
	}

	public function getCategory_image_id()
	{
		return $this->category_image_id;
	}
	public function setCategory_image_id($category_image_id)
	{
		$this->category_image_id = $category_image_id;
	}

	public function getCategory_name()
	{
		return $this->category_name;
	}
	public function setCategory_name($category_name)
	{
		$this->category_name = $category_name;
	}

	public function getCategory_type()
	{
		return $this->category_type;
	}
	public function setCategory_type($category_type)
	{
		$this->category_type = $category_type;
	}

	public function getCreated_datetime()
	{
		return $this->created_datetime;
	}
	public function setCreated_datetime($created_datetime)
	{
		$this->created_datetime = $created_datetime;
	}

	public function getDisplay_on_home()
	{
		return $this->display_on_home;
	}
	public function setDisplay_on_home($display_on_home)
	{
		$this->display_on_home = $display_on_home;
	}

	public function getDomain_id()
	{
		return $this->domain_id;
	}
	public function setDomain_id($domain_id)
	{
		$this->domain_id = $domain_id;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}

	public function getLanguage_id()
	{
		return $this->language_id;
	}
	public function setLanguage_id($language_id)
	{
		$this->language_id = $language_id;
	}

	public function getMain_category_id()
	{
		return $this->main_category_id;
	}
	public function setMain_category_id($main_category_id)
	{
		$this->main_category_id = $main_category_id;
	}

	public function getParent_category_id()
	{
		return $this->parent_category_id;
	}
	public function setParent_category_id($parent_category_id)
	{
		$this->parent_category_id = $parent_category_id;
	}

	public function getSort_order()
	{
		return $this->sort_order;
	}
	public function setSort_order($sort_order)
	{
		$this->sort_order = $sort_order;
	}
}