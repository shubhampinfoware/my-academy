<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="usertrainerrelation")
*/
class Usertrainerrelation
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $usertrainerrelation_id;

	/**
	* @ORM\Column(type="string")
	*/
	protected $is_approved="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $is_chessbase="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $is_deleted="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $user_id=0;

	public function getUsertrainerrelation_id()
	{
		return $this->usertrainerrelation_id;
	}

	public function getIs_approved()
	{
		return $this->is_approved;
	}
	public function setIs_approved($is_approved)
	{
		$this->is_approved = $is_approved;
	}

	public function getIs_chessbase()
	{
		return $this->is_chessbase;
	}
	public function setIs_chessbase($is_chessbase)
	{
		$this->is_chessbase = $is_chessbase;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}

	public function getUser_id()
	{
		return $this->user_id;
	}
	public function setUser_id($user_id)
	{
		$this->user_id = $user_id;
	}
}