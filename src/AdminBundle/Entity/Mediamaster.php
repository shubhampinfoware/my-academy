<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="mediamaster")
*/
class Mediamaster
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $mediamaster_id;

	/**
	* @ORM\Column(type="string")
	*/
	protected $created_datetime="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $is_deleted="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $media_location="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $media_name="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $media_title="";

	public function getMediamaster_id()
	{
		return $this->mediamaster_id;
	}

	public function getCreated_datetime()
	{
		return $this->created_datetime;
	}
	public function setCreated_datetime($created_datetime)
	{
		$this->created_datetime = $created_datetime;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}

	public function getMedia_location()
	{
		return $this->media_location;
	}
	public function setMedia_location($media_location)
	{
		$this->media_location = $media_location;
	}

	public function getMedia_name()
	{
		return $this->media_name;
	}
	public function setMedia_name($media_name)
	{
		$this->media_name = $media_name;
	}

	public function getMedia_title()
	{
		return $this->media_title;
	}
	public function setMedia_title($media_title)
	{
		$this->media_title = $media_title;
	}
}