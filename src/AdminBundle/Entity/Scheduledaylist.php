<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="schedule_day_list")
*/
class Scheduledaylist
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $schedule_day_list_id;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $schedule_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $schedule_date="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $created_datetime="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $is_deleted=0;

	public function getSchedule_day_list_id()
	{
		return $this->schedule_day_list_id;
	}

	public function getSchedule_id()
	{
		return $this->schedule_id;
	}
	public function setSchedule_id($schedule_id)
	{
		$this->schedule_id = $schedule_id;
	}

	public function getSchedule_date()
	{
		return $this->schedule_date;
	}
	public function setSchedule_date($schedule_date)
	{
		$this->schedule_date = $schedule_date;
	}

	public function getCreated_datetime()
	{
		return $this->created_datetime;
	}
	public function setCreated_datetime($created_datetime)
	{
		$this->created_datetime = $created_datetime;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}
}