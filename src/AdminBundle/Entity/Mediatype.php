<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="media_type")
*/
class Mediatype
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $media_type_id;

	/**
	* @ORM\Column(type="string")
	*/
	protected $is_deleted="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $language_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $main_media_type_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $media_status="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $media_type_allowed="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $media_type_description="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $media_type_name="";

	public function getMedia_type_id()
	{
		return $this->media_type_id;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}

	public function getLanguage_id()
	{
		return $this->language_id;
	}
	public function setLanguage_id($language_id)
	{
		$this->language_id = $language_id;
	}

	public function getMain_media_type_id()
	{
		return $this->main_media_type_id;
	}
	public function setMain_media_type_id($main_media_type_id)
	{
		$this->main_media_type_id = $main_media_type_id;
	}

	public function getMedia_status()
	{
		return $this->media_status;
	}
	public function setMedia_status($media_status)
	{
		$this->media_status = $media_status;
	}

	public function getMedia_type_allowed()
	{
		return $this->media_type_allowed;
	}
	public function setMedia_type_allowed($media_type_allowed)
	{
		$this->media_type_allowed = $media_type_allowed;
	}

	public function getMedia_type_description()
	{
		return $this->media_type_description;
	}
	public function setMedia_type_description($media_type_description)
	{
		$this->media_type_description = $media_type_description;
	}

	public function getMedia_type_name()
	{
		return $this->media_type_name;
	}
	public function setMedia_type_name($media_type_name)
	{
		$this->media_type_name = $media_type_name;
	}
}