<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="role_master")
*/
class Rolemaster
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $role_master_id;

	/**
	* @ORM\Column(type="string")
	*/
	protected $is_deleted="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $role_description="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $role_status="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $roleid=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $rolename="";

	public function getRole_master_id()
	{
		return $this->role_master_id;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}

	public function getRole_description()
	{
		return $this->role_description;
	}
	public function setRole_description($role_description)
	{
		$this->role_description = $role_description;
	}

	public function getRole_status()
	{
		return $this->role_status;
	}
	public function setRole_status($role_status)
	{
		$this->role_status = $role_status;
	}

	public function getRoleid()
	{
		return $this->roleid;
	}
	public function setRoleid($roleid)
	{
		$this->roleid = $roleid;
	}

	public function getRolename()
	{
		return $this->rolename;
	}
	public function setRolename($rolename)
	{
		$this->rolename = $rolename;
	}
}