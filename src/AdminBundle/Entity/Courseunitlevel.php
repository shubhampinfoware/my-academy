<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="course_unit_level")
*/
class Courseunitlevel
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $course_unit_level_id;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $class_level_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $course_unit_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $is_deleted="";

	public function getCourse_unit_level_id()
	{
		return $this->course_unit_level_id;
	}

	public function getClass_level_id()
	{
		return $this->class_level_id;
	}
	public function setClass_level_id($class_level_id)
	{
		$this->class_level_id = $class_level_id;
	}

	public function getCourse_unit_id()
	{
		return $this->course_unit_id;
	}
	public function setCourse_unit_id($course_unit_id)
	{
		$this->course_unit_id = $course_unit_id;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}
}