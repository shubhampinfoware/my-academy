<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="course_unit_relation")
*/
class Courseunitrelation
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $course_unit_relation_id;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $course_unit_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $field_1="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $field_2="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $is_deleted="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $relation_object_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $relation_with="";

	public function getCourse_unit_relation_id()
	{
		return $this->course_unit_relation_id;
	}

	public function getCourse_unit_id()
	{
		return $this->course_unit_id;
	}
	public function setCourse_unit_id($course_unit_id)
	{
		$this->course_unit_id = $course_unit_id;
	}

	public function getField_1()
	{
		return $this->field_1;
	}
	public function setField_1($field_1)
	{
		$this->field_1 = $field_1;
	}

	public function getField_2()
	{
		return $this->field_2;
	}
	public function setField_2($field_2)
	{
		$this->field_2 = $field_2;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}

	public function getRelation_object_id()
	{
		return $this->relation_object_id;
	}
	public function setRelation_object_id($relation_object_id)
	{
		$this->relation_object_id = $relation_object_id;
	}

	public function getRelation_with()
	{
		return $this->relation_with;
	}
	public function setRelation_with($relation_with)
	{
		$this->relation_with = $relation_with;
	}
}