<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="wallet_master")
*/
class Walletmaster
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $wallet_master_id;

	/**
	* @ORM\Column(type="string")
	*/
	protected $actual_money_balance="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $cashback_balance="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $is_deleted="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $last_updated_wallet_datetime="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $user_master_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $virtual_money_balance="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $wallet_code="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $wallet_member_tag_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $wallet_points="";

	public function getWallet_master_id()
	{
		return $this->wallet_master_id;
	}

	public function getActual_money_balance()
	{
		return $this->actual_money_balance;
	}
	public function setActual_money_balance($actual_money_balance)
	{
		$this->actual_money_balance = $actual_money_balance;
	}

	public function getCashback_balance()
	{
		return $this->cashback_balance;
	}
	public function setCashback_balance($cashback_balance)
	{
		$this->cashback_balance = $cashback_balance;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}

	public function getLast_updated_wallet_datetime()
	{
		return $this->last_updated_wallet_datetime;
	}
	public function setLast_updated_wallet_datetime($last_updated_wallet_datetime)
	{
		$this->last_updated_wallet_datetime = $last_updated_wallet_datetime;
	}

	public function getUser_master_id()
	{
		return $this->user_master_id;
	}
	public function setUser_master_id($user_master_id)
	{
		$this->user_master_id = $user_master_id;
	}

	public function getVirtual_money_balance()
	{
		return $this->virtual_money_balance;
	}
	public function setVirtual_money_balance($virtual_money_balance)
	{
		$this->virtual_money_balance = $virtual_money_balance;
	}

	public function getWallet_code()
	{
		return $this->wallet_code;
	}
	public function setWallet_code($wallet_code)
	{
		$this->wallet_code = $wallet_code;
	}

	public function getWallet_member_tag_id()
	{
		return $this->wallet_member_tag_id;
	}
	public function setWallet_member_tag_id($wallet_member_tag_id)
	{
		$this->wallet_member_tag_id = $wallet_member_tag_id;
	}

	public function getWallet_points()
	{
		return $this->wallet_points;
	}
	public function setWallet_points($wallet_points)
	{
		$this->wallet_points = $wallet_points;
	}
}