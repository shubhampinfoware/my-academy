<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="coupon_applied_to_object_list")
*/
class Couponappliedtoobjectlist
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $coupon_applied_to_object_list_id;

	/**
	* @ORM\Column(type="string")
	*/
	protected $applied_on="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $coupon_applied_to_object_list_field_1="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $coupon_applied_to_object_list_field_2="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $coupon_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $is_deleted="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $relation_id.0=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $relation_id.1=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $relation_id.2=0;

	public function getCoupon_applied_to_object_list_id()
	{
		return $this->coupon_applied_to_object_list_id;
	}

	public function getApplied_on()
	{
		return $this->applied_on;
	}
	public function setApplied_on($applied_on)
	{
		$this->applied_on = $applied_on;
	}

	public function getCoupon_applied_to_object_list_field_1()
	{
		return $this->coupon_applied_to_object_list_field_1;
	}
	public function setCoupon_applied_to_object_list_field_1($coupon_applied_to_object_list_field_1)
	{
		$this->coupon_applied_to_object_list_field_1 = $coupon_applied_to_object_list_field_1;
	}

	public function getCoupon_applied_to_object_list_field_2()
	{
		return $this->coupon_applied_to_object_list_field_2;
	}
	public function setCoupon_applied_to_object_list_field_2($coupon_applied_to_object_list_field_2)
	{
		$this->coupon_applied_to_object_list_field_2 = $coupon_applied_to_object_list_field_2;
	}

	public function getCoupon_id()
	{
		return $this->coupon_id;
	}
	public function setCoupon_id($coupon_id)
	{
		$this->coupon_id = $coupon_id;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}

	public function getRelation_id.0()
	{
		return $this->relation_id.0;
	}
	public function setRelation_id.0($relation_id.0)
	{
		$this->relation_id.0 = $relation_id.0;
	}

	public function getRelation_id.1()
	{
		return $this->relation_id.1;
	}
	public function setRelation_id.1($relation_id.1)
	{
		$this->relation_id.1 = $relation_id.1;
	}

	public function getRelation_id.2()
	{
		return $this->relation_id.2;
	}
	public function setRelation_id.2($relation_id.2)
	{
		$this->relation_id.2 = $relation_id.2;
	}
}