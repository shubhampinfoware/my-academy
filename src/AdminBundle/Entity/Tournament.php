<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="tournament")
*/
class Tournament
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $tournament_id;

	/**
	* @ORM\Column(type="string")
	*/
	protected $city="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $code="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $country="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $desc="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $end="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $endstr="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $is_deleted="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $monthyear="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $organizeremail="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $organizername="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $organizerphone="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $pdfname="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $prizecurrency="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $prizefirst="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $prizesecond="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $prizethird="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $start="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $state="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $title="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $totalprizefund="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $type="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $venue="";

	public function getTournament_id()
	{
		return $this->tournament_id;
	}

	public function getCity()
	{
		return $this->city;
	}
	public function setCity($city)
	{
		$this->city = $city;
	}

	public function getCode()
	{
		return $this->code;
	}
	public function setCode($code)
	{
		$this->code = $code;
	}

	public function getCountry()
	{
		return $this->country;
	}
	public function setCountry($country)
	{
		$this->country = $country;
	}

	public function getDesc()
	{
		return $this->desc;
	}
	public function setDesc($desc)
	{
		$this->desc = $desc;
	}

	public function getEnd()
	{
		return $this->end;
	}
	public function setEnd($end)
	{
		$this->end = $end;
	}

	public function getEndstr()
	{
		return $this->endstr;
	}
	public function setEndstr($endstr)
	{
		$this->endstr = $endstr;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}

	public function getMonthyear()
	{
		return $this->monthyear;
	}
	public function setMonthyear($monthyear)
	{
		$this->monthyear = $monthyear;
	}

	public function getOrganizeremail()
	{
		return $this->organizeremail;
	}
	public function setOrganizeremail($organizeremail)
	{
		$this->organizeremail = $organizeremail;
	}

	public function getOrganizername()
	{
		return $this->organizername;
	}
	public function setOrganizername($organizername)
	{
		$this->organizername = $organizername;
	}

	public function getOrganizerphone()
	{
		return $this->organizerphone;
	}
	public function setOrganizerphone($organizerphone)
	{
		$this->organizerphone = $organizerphone;
	}

	public function getPdfname()
	{
		return $this->pdfname;
	}
	public function setPdfname($pdfname)
	{
		$this->pdfname = $pdfname;
	}

	public function getPrizecurrency()
	{
		return $this->prizecurrency;
	}
	public function setPrizecurrency($prizecurrency)
	{
		$this->prizecurrency = $prizecurrency;
	}

	public function getPrizefirst()
	{
		return $this->prizefirst;
	}
	public function setPrizefirst($prizefirst)
	{
		$this->prizefirst = $prizefirst;
	}

	public function getPrizesecond()
	{
		return $this->prizesecond;
	}
	public function setPrizesecond($prizesecond)
	{
		$this->prizesecond = $prizesecond;
	}

	public function getPrizethird()
	{
		return $this->prizethird;
	}
	public function setPrizethird($prizethird)
	{
		$this->prizethird = $prizethird;
	}

	public function getStart()
	{
		return $this->start;
	}
	public function setStart($start)
	{
		$this->start = $start;
	}

	public function getState()
	{
		return $this->state;
	}
	public function setState($state)
	{
		$this->state = $state;
	}

	public function getTitle()
	{
		return $this->title;
	}
	public function setTitle($title)
	{
		$this->title = $title;
	}

	public function getTotalprizefund()
	{
		return $this->totalprizefund;
	}
	public function setTotalprizefund($totalprizefund)
	{
		$this->totalprizefund = $totalprizefund;
	}

	public function getType()
	{
		return $this->type;
	}
	public function setType($type)
	{
		$this->type = $type;
	}

	public function getVenue()
	{
		return $this->venue;
	}
	public function setVenue($venue)
	{
		$this->venue = $venue;
	}
}