<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="reviews_master")
*/
class Reviewsmaster
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $reviews_master_id;

	/**
	* @ORM\Column(type="string")
	*/
	protected $review_text="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $review_type="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $relation_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $ratings=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $is_testimonial="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $created_by=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $domain_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $created_datetime="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $is_deleted=0;

	public function getReviews_master_id()
	{
		return $this->reviews_master_id;
	}

	public function getReview_text()
	{
		return $this->review_text;
	}
	public function setReview_text($review_text)
	{
		$this->review_text = $review_text;
	}

	public function getReview_type()
	{
		return $this->review_type;
	}
	public function setReview_type($review_type)
	{
		$this->review_type = $review_type;
	}

	public function getRelation_id()
	{
		return $this->relation_id;
	}
	public function setRelation_id($relation_id)
	{
		$this->relation_id = $relation_id;
	}

	public function getRatings()
	{
		return $this->ratings;
	}
	public function setRatings($ratings)
	{
		$this->ratings = $ratings;
	}

	public function getIs_testimonial()
	{
		return $this->is_testimonial;
	}
	public function setIs_testimonial($is_testimonial)
	{
		$this->is_testimonial = $is_testimonial;
	}

	public function getCreated_by()
	{
		return $this->created_by;
	}
	public function setCreated_by($created_by)
	{
		$this->created_by = $created_by;
	}

	public function getDomain_id()
	{
		return $this->domain_id;
	}
	public function setDomain_id($domain_id)
	{
		$this->domain_id = $domain_id;
	}

	public function getCreated_datetime()
	{
		return $this->created_datetime;
	}
	public function setCreated_datetime($created_datetime)
	{
		$this->created_datetime = $created_datetime;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}
}