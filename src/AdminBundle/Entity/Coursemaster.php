<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="coursemaster")
*/
class Coursemaster
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $coursemaster_id;

	/**
	* @ORM\Column(type="string")
	*/
	protected $auto_enroll="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $catalog_display="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $course_catid="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $course_code="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $course_description="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $course_image="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $course_name="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $course_price="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $course_status="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $create_date="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $domain_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $is_deleted="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $language_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $main_coursemaster_id="";

	public function getCoursemaster_id()
	{
		return $this->coursemaster_id;
	}

	public function getAuto_enroll()
	{
		return $this->auto_enroll;
	}
	public function setAuto_enroll($auto_enroll)
	{
		$this->auto_enroll = $auto_enroll;
	}

	public function getCatalog_display()
	{
		return $this->catalog_display;
	}
	public function setCatalog_display($catalog_display)
	{
		$this->catalog_display = $catalog_display;
	}

	public function getCourse_catid()
	{
		return $this->course_catid;
	}
	public function setCourse_catid($course_catid)
	{
		$this->course_catid = $course_catid;
	}

	public function getCourse_code()
	{
		return $this->course_code;
	}
	public function setCourse_code($course_code)
	{
		$this->course_code = $course_code;
	}

	public function getCourse_description()
	{
		return $this->course_description;
	}
	public function setCourse_description($course_description)
	{
		$this->course_description = $course_description;
	}

	public function getCourse_image()
	{
		return $this->course_image;
	}
	public function setCourse_image($course_image)
	{
		$this->course_image = $course_image;
	}

	public function getCourse_name()
	{
		return $this->course_name;
	}
	public function setCourse_name($course_name)
	{
		$this->course_name = $course_name;
	}

	public function getCourse_price()
	{
		return $this->course_price;
	}
	public function setCourse_price($course_price)
	{
		$this->course_price = $course_price;
	}

	public function getCourse_status()
	{
		return $this->course_status;
	}
	public function setCourse_status($course_status)
	{
		$this->course_status = $course_status;
	}

	public function getCreate_date()
	{
		return $this->create_date;
	}
	public function setCreate_date($create_date)
	{
		$this->create_date = $create_date;
	}

	public function getDomain_id()
	{
		return $this->domain_id;
	}
	public function setDomain_id($domain_id)
	{
		$this->domain_id = $domain_id;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}

	public function getLanguage_id()
	{
		return $this->language_id;
	}
	public function setLanguage_id($language_id)
	{
		$this->language_id = $language_id;
	}

	public function getMain_coursemaster_id()
	{
		return $this->main_coursemaster_id;
	}
	public function setMain_coursemaster_id($main_coursemaster_id)
	{
		$this->main_coursemaster_id = $main_coursemaster_id;
	}
}