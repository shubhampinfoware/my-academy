<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="subscription_course_relation")
*/
class Subscriptioncourserelation
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $subscription_course_relation_id;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $subscription_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $course_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $created_date="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $is_deleted=0;

	public function getSubscription_course_relation_id()
	{
		return $this->subscription_course_relation_id;
	}

	public function getSubscription_id()
	{
		return $this->subscription_id;
	}
	public function setSubscription_id($subscription_id)
	{
		$this->subscription_id = $subscription_id;
	}

	public function getCourse_id()
	{
		return $this->course_id;
	}
	public function setCourse_id($course_id)
	{
		$this->course_id = $course_id;
	}

	public function getCreated_date()
	{
		return $this->created_date;
	}
	public function setCreated_date($created_date)
	{
		$this->created_date = $created_date;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}
}