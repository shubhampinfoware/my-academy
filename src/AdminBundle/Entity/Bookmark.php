<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="bookmark")
*/
class Bookmark
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $bookmark_id;

	/**
	* @ORM\Column(type="string")
	*/
	protected $isdeleted="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $type="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $userid="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $videoid="";

	public function getBookmark_id()
	{
		return $this->bookmark_id;
	}

	public function getIsdeleted()
	{
		return $this->isdeleted;
	}
	public function setIsdeleted($isdeleted)
	{
		$this->isdeleted = $isdeleted;
	}

	public function getType()
	{
		return $this->type;
	}
	public function setType($type)
	{
		$this->type = $type;
	}

	public function getUserid()
	{
		return $this->userid;
	}
	public function setUserid($userid)
	{
		$this->userid = $userid;
	}

	public function getVideoid()
	{
		return $this->videoid;
	}
	public function setVideoid($videoid)
	{
		$this->videoid = $videoid;
	}
}