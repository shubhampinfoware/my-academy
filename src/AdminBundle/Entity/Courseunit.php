<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="course_unit")
*/
class Courseunit
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $course_unit_id;

	/**
	* @ORM\Column(type="string")
	*/
	protected $allow_shuffle="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $class_cover_image="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $class_level_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $course_media_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $course_type="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $course_unit_media_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $coursemaster_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $create_date="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $created_by="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $domain_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $instruction="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $is_deleted="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $language_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $lock_feature="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $main_course_unit_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $prerequisites="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $privateflag="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $sort_order="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $status="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $tag_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $unit_code="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $unit_name="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $updated_date="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $youtubelink="";

	public function getCourse_unit_id()
	{
		return $this->course_unit_id;
	}

	public function getAllow_shuffle()
	{
		return $this->allow_shuffle;
	}
	public function setAllow_shuffle($allow_shuffle)
	{
		$this->allow_shuffle = $allow_shuffle;
	}

	public function getClass_cover_image()
	{
		return $this->class_cover_image;
	}
	public function setClass_cover_image($class_cover_image)
	{
		$this->class_cover_image = $class_cover_image;
	}

	public function getClass_level_id()
	{
		return $this->class_level_id;
	}
	public function setClass_level_id($class_level_id)
	{
		$this->class_level_id = $class_level_id;
	}

	public function getCourse_media_id()
	{
		return $this->course_media_id;
	}
	public function setCourse_media_id($course_media_id)
	{
		$this->course_media_id = $course_media_id;
	}

	public function getCourse_type()
	{
		return $this->course_type;
	}
	public function setCourse_type($course_type)
	{
		$this->course_type = $course_type;
	}

	public function getCourse_unit_media_id()
	{
		return $this->course_unit_media_id;
	}
	public function setCourse_unit_media_id($course_unit_media_id)
	{
		$this->course_unit_media_id = $course_unit_media_id;
	}

	public function getCoursemaster_id()
	{
		return $this->coursemaster_id;
	}
	public function setCoursemaster_id($coursemaster_id)
	{
		$this->coursemaster_id = $coursemaster_id;
	}

	public function getCreate_date()
	{
		return $this->create_date;
	}
	public function setCreate_date($create_date)
	{
		$this->create_date = $create_date;
	}

	public function getCreated_by()
	{
		return $this->created_by;
	}
	public function setCreated_by($created_by)
	{
		$this->created_by = $created_by;
	}

	public function getDomain_id()
	{
		return $this->domain_id;
	}
	public function setDomain_id($domain_id)
	{
		$this->domain_id = $domain_id;
	}

	public function getInstruction()
	{
		return $this->instruction;
	}
	public function setInstruction($instruction)
	{
		$this->instruction = $instruction;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}

	public function getLanguage_id()
	{
		return $this->language_id;
	}
	public function setLanguage_id($language_id)
	{
		$this->language_id = $language_id;
	}

	public function getLock_feature()
	{
		return $this->lock_feature;
	}
	public function setLock_feature($lock_feature)
	{
		$this->lock_feature = $lock_feature;
	}

	public function getMain_course_unit_id()
	{
		return $this->main_course_unit_id;
	}
	public function setMain_course_unit_id($main_course_unit_id)
	{
		$this->main_course_unit_id = $main_course_unit_id;
	}

	public function getPrerequisites()
	{
		return $this->prerequisites;
	}
	public function setPrerequisites($prerequisites)
	{
		$this->prerequisites = $prerequisites;
	}

	public function getPrivateflag()
	{
		return $this->privateflag;
	}
	public function setPrivateflag($privateflag)
	{
		$this->privateflag = $privateflag;
	}

	public function getSort_order()
	{
		return $this->sort_order;
	}
	public function setSort_order($sort_order)
	{
		$this->sort_order = $sort_order;
	}

	public function getStatus()
	{
		return $this->status;
	}
	public function setStatus($status)
	{
		$this->status = $status;
	}

	public function getTag_id()
	{
		return $this->tag_id;
	}
	public function setTag_id($tag_id)
	{
		$this->tag_id = $tag_id;
	}

	public function getUnit_code()
	{
		return $this->unit_code;
	}
	public function setUnit_code($unit_code)
	{
		$this->unit_code = $unit_code;
	}

	public function getUnit_name()
	{
		return $this->unit_name;
	}
	public function setUnit_name($unit_name)
	{
		$this->unit_name = $unit_name;
	}

	public function getUpdated_date()
	{
		return $this->updated_date;
	}
	public function setUpdated_date($updated_date)
	{
		$this->updated_date = $updated_date;
	}

	public function getYoutubelink()
	{
		return $this->youtubelink;
	}
	public function setYoutubelink($youtubelink)
	{
		$this->youtubelink = $youtubelink;
	}
}