<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="fideuser")
*/
class Fideuser
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $fideuser_id;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $fide_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $fide_elo="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $fide_title="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $is_deleted="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $user_id=0;

	public function getFideuser_id()
	{
		return $this->fideuser_id;
	}

	public function getFide_id()
	{
		return $this->fide_id;
	}
	public function setFide_id($fide_id)
	{
		$this->fide_id = $fide_id;
	}

	public function getFide_elo()
	{
		return $this->fide_elo;
	}
	public function setFide_elo($fide_elo)
	{
		$this->fide_elo = $fide_elo;
	}

	public function getFide_title()
	{
		return $this->fide_title;
	}
	public function setFide_title($fide_title)
	{
		$this->fide_title = $fide_title;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}

	public function getUser_id()
	{
		return $this->user_id;
	}
	public function setUser_id($user_id)
	{
		$this->user_id = $user_id;
	}
}