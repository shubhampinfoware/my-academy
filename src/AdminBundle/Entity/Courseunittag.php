<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="course_unit_tag")
*/
class Courseunittag
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $course_unit_tag_id;

	/**
	* @ORM\Column(type="string")
	*/
	protected $field_1="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $field_2="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $tag_name="";

	public function getCourse_unit_tag_id()
	{
		return $this->course_unit_tag_id;
	}

	public function getField_1()
	{
		return $this->field_1;
	}
	public function setField_1($field_1)
	{
		$this->field_1 = $field_1;
	}

	public function getField_2()
	{
		return $this->field_2;
	}
	public function setField_2($field_2)
	{
		$this->field_2 = $field_2;
	}

	public function getTag_name()
	{
		return $this->tag_name;
	}
	public function setTag_name($tag_name)
	{
		$this->tag_name = $tag_name;
	}
}