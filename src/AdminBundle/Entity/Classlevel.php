<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="class_level")
*/
class Classlevel
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $class_level_id;

	/**
	* @ORM\Column(type="string")
	*/
	protected $field_1="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $field_2="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $is_deleted="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $level_desc="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $level_end_range="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $level_name="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $level_start_range="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $level_status="";

	public function getClass_level_id()
	{
		return $this->class_level_id;
	}

	public function getField_1()
	{
		return $this->field_1;
	}
	public function setField_1($field_1)
	{
		$this->field_1 = $field_1;
	}

	public function getField_2()
	{
		return $this->field_2;
	}
	public function setField_2($field_2)
	{
		$this->field_2 = $field_2;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}

	public function getLevel_desc()
	{
		return $this->level_desc;
	}
	public function setLevel_desc($level_desc)
	{
		$this->level_desc = $level_desc;
	}

	public function getLevel_end_range()
	{
		return $this->level_end_range;
	}
	public function setLevel_end_range($level_end_range)
	{
		$this->level_end_range = $level_end_range;
	}

	public function getLevel_name()
	{
		return $this->level_name;
	}
	public function setLevel_name($level_name)
	{
		$this->level_name = $level_name;
	}

	public function getLevel_start_range()
	{
		return $this->level_start_range;
	}
	public function setLevel_start_range($level_start_range)
	{
		$this->level_start_range = $level_start_range;
	}

	public function getLevel_status()
	{
		return $this->level_status;
	}
	public function setLevel_status($level_status)
	{
		$this->level_status = $level_status;
	}
}