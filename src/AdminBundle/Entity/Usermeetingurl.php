<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="user_meeting_url")
*/
class Usermeetingurl
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $user_meeting_url_id;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $course_unit_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $is_deleted="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $meeting_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $meeting_url="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $registrant_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $user_id=0;

	public function getUser_meeting_url_id()
	{
		return $this->user_meeting_url_id;
	}

	public function getCourse_unit_id()
	{
		return $this->course_unit_id;
	}
	public function setCourse_unit_id($course_unit_id)
	{
		$this->course_unit_id = $course_unit_id;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}

	public function getMeeting_id()
	{
		return $this->meeting_id;
	}
	public function setMeeting_id($meeting_id)
	{
		$this->meeting_id = $meeting_id;
	}

	public function getMeeting_url()
	{
		return $this->meeting_url;
	}
	public function setMeeting_url($meeting_url)
	{
		$this->meeting_url = $meeting_url;
	}

	public function getRegistrant_id()
	{
		return $this->registrant_id;
	}
	public function setRegistrant_id($registrant_id)
	{
		$this->registrant_id = $registrant_id;
	}

	public function getUser_id()
	{
		return $this->user_id;
	}
	public function setUser_id($user_id)
	{
		$this->user_id = $user_id;
	}
}