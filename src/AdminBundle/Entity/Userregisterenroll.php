<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="user_register_enroll")
*/
class Userregisterenroll
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $user_register_enroll_id;

	/**
	* @ORM\Column(type="string")
	*/
	protected $assign_by="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $assign_user_datetime="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $assign_user_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $created_by="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $created_datetime="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $enrollment_Code="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $enrollment_status="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $extra1="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $extra2="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $is_deleted="";

	public function getUser_register_enroll_id()
	{
		return $this->user_register_enroll_id;
	}

	public function getAssign_by()
	{
		return $this->assign_by;
	}
	public function setAssign_by($assign_by)
	{
		$this->assign_by = $assign_by;
	}

	public function getAssign_user_datetime()
	{
		return $this->assign_user_datetime;
	}
	public function setAssign_user_datetime($assign_user_datetime)
	{
		$this->assign_user_datetime = $assign_user_datetime;
	}

	public function getAssign_user_id()
	{
		return $this->assign_user_id;
	}
	public function setAssign_user_id($assign_user_id)
	{
		$this->assign_user_id = $assign_user_id;
	}

	public function getCreated_by()
	{
		return $this->created_by;
	}
	public function setCreated_by($created_by)
	{
		$this->created_by = $created_by;
	}

	public function getCreated_datetime()
	{
		return $this->created_datetime;
	}
	public function setCreated_datetime($created_datetime)
	{
		$this->created_datetime = $created_datetime;
	}

	public function getEnrollment_Code()
	{
		return $this->enrollment_Code;
	}
	public function setEnrollment_Code($enrollment_Code)
	{
		$this->enrollment_Code = $enrollment_Code;
	}

	public function getEnrollment_status()
	{
		return $this->enrollment_status;
	}
	public function setEnrollment_status($enrollment_status)
	{
		$this->enrollment_status = $enrollment_status;
	}

	public function getExtra1()
	{
		return $this->extra1;
	}
	public function setExtra1($extra1)
	{
		$this->extra1 = $extra1;
	}

	public function getExtra2()
	{
		return $this->extra2;
	}
	public function setExtra2($extra2)
	{
		$this->extra2 = $extra2;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}
}