<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="wallet_transaction")
*/
class Wallettransaction
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $wallet_transaction_id;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $actual_payment_transaction_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $balance_type="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $expiry_date_of_balance="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $is_archive="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $is_deleted="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $transaction_for_table="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $transaction_for_table_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $transaction_note="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $updated_wallet_balance="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $user_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $wallet_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $wallet_transaction="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $wallet_transaction_amount="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $wallet_transaction_by="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $wallet_transaction_datetime="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $wallet_transaction_field_1="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $wallet_transaction_field_2="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $wallet_transaction_field_3="";

	public function getWallet_transaction_id()
	{
		return $this->wallet_transaction_id;
	}

	public function getActual_payment_transaction_id()
	{
		return $this->actual_payment_transaction_id;
	}
	public function setActual_payment_transaction_id($actual_payment_transaction_id)
	{
		$this->actual_payment_transaction_id = $actual_payment_transaction_id;
	}

	public function getBalance_type()
	{
		return $this->balance_type;
	}
	public function setBalance_type($balance_type)
	{
		$this->balance_type = $balance_type;
	}

	public function getExpiry_date_of_balance()
	{
		return $this->expiry_date_of_balance;
	}
	public function setExpiry_date_of_balance($expiry_date_of_balance)
	{
		$this->expiry_date_of_balance = $expiry_date_of_balance;
	}

	public function getIs_archive()
	{
		return $this->is_archive;
	}
	public function setIs_archive($is_archive)
	{
		$this->is_archive = $is_archive;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}

	public function getTransaction_for_table()
	{
		return $this->transaction_for_table;
	}
	public function setTransaction_for_table($transaction_for_table)
	{
		$this->transaction_for_table = $transaction_for_table;
	}

	public function getTransaction_for_table_id()
	{
		return $this->transaction_for_table_id;
	}
	public function setTransaction_for_table_id($transaction_for_table_id)
	{
		$this->transaction_for_table_id = $transaction_for_table_id;
	}

	public function getTransaction_note()
	{
		return $this->transaction_note;
	}
	public function setTransaction_note($transaction_note)
	{
		$this->transaction_note = $transaction_note;
	}

	public function getUpdated_wallet_balance()
	{
		return $this->updated_wallet_balance;
	}
	public function setUpdated_wallet_balance($updated_wallet_balance)
	{
		$this->updated_wallet_balance = $updated_wallet_balance;
	}

	public function getUser_id()
	{
		return $this->user_id;
	}
	public function setUser_id($user_id)
	{
		$this->user_id = $user_id;
	}

	public function getWallet_id()
	{
		return $this->wallet_id;
	}
	public function setWallet_id($wallet_id)
	{
		$this->wallet_id = $wallet_id;
	}

	public function getWallet_transaction()
	{
		return $this->wallet_transaction;
	}
	public function setWallet_transaction($wallet_transaction)
	{
		$this->wallet_transaction = $wallet_transaction;
	}

	public function getWallet_transaction_amount()
	{
		return $this->wallet_transaction_amount;
	}
	public function setWallet_transaction_amount($wallet_transaction_amount)
	{
		$this->wallet_transaction_amount = $wallet_transaction_amount;
	}

	public function getWallet_transaction_by()
	{
		return $this->wallet_transaction_by;
	}
	public function setWallet_transaction_by($wallet_transaction_by)
	{
		$this->wallet_transaction_by = $wallet_transaction_by;
	}

	public function getWallet_transaction_datetime()
	{
		return $this->wallet_transaction_datetime;
	}
	public function setWallet_transaction_datetime($wallet_transaction_datetime)
	{
		$this->wallet_transaction_datetime = $wallet_transaction_datetime;
	}

	public function getWallet_transaction_field_1()
	{
		return $this->wallet_transaction_field_1;
	}
	public function setWallet_transaction_field_1($wallet_transaction_field_1)
	{
		$this->wallet_transaction_field_1 = $wallet_transaction_field_1;
	}

	public function getWallet_transaction_field_2()
	{
		return $this->wallet_transaction_field_2;
	}
	public function setWallet_transaction_field_2($wallet_transaction_field_2)
	{
		$this->wallet_transaction_field_2 = $wallet_transaction_field_2;
	}

	public function getWallet_transaction_field_3()
	{
		return $this->wallet_transaction_field_3;
	}
	public function setWallet_transaction_field_3($wallet_transaction_field_3)
	{
		$this->wallet_transaction_field_3 = $wallet_transaction_field_3;
	}
}