<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="channelmaster")
*/
class Channelmaster
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $channelmaster_id;

	/**
	* @ORM\Column(type="string")
	*/
	protected $channelid="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $is_deleted="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $name="";

	public function getChannelmaster_id()
	{
		return $this->channelmaster_id;
	}

	public function getChannelid()
	{
		return $this->channelid;
	}
	public function setChannelid($channelid)
	{
		$this->channelid = $channelid;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}

	public function getName()
	{
		return $this->name;
	}
	public function setName($name)
	{
		$this->name = $name;
	}
}