<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="bannercountryshop")
*/
class Bannercountryshop
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $bannercountryshop_id;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $banner_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $country_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $country_name="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $extra="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $is_deleted="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $shop_url="";

	public function getBannercountryshop_id()
	{
		return $this->bannercountryshop_id;
	}

	public function getBanner_id()
	{
		return $this->banner_id;
	}
	public function setBanner_id($banner_id)
	{
		$this->banner_id = $banner_id;
	}

	public function getCountry_id()
	{
		return $this->country_id;
	}
	public function setCountry_id($country_id)
	{
		$this->country_id = $country_id;
	}

	public function getCountry_name()
	{
		return $this->country_name;
	}
	public function setCountry_name($country_name)
	{
		$this->country_name = $country_name;
	}

	public function getExtra()
	{
		return $this->extra;
	}
	public function setExtra($extra)
	{
		$this->extra = $extra;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}

	public function getShop_url()
	{
		return $this->shop_url;
	}
	public function setShop_url($shop_url)
	{
		$this->shop_url = $shop_url;
	}
}