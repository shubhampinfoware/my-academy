<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="channel_section")
*/
class Channelsection
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $channel_section_id;

	/**
	* @ORM\Column(type="string")
	*/
	protected $channelid="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $ext1="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $is_deleted="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $name="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $order="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $playlist_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $type="";

	public function getChannel_section_id()
	{
		return $this->channel_section_id;
	}

	public function getChannelid()
	{
		return $this->channelid;
	}
	public function setChannelid($channelid)
	{
		$this->channelid = $channelid;
	}

	public function getExt1()
	{
		return $this->ext1;
	}
	public function setExt1($ext1)
	{
		$this->ext1 = $ext1;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}

	public function getName()
	{
		return $this->name;
	}
	public function setName($name)
	{
		$this->name = $name;
	}

	public function getOrder()
	{
		return $this->order;
	}
	public function setOrder($order)
	{
		$this->order = $order;
	}

	public function getPlaylist_id()
	{
		return $this->playlist_id;
	}
	public function setPlaylist_id($playlist_id)
	{
		$this->playlist_id = $playlist_id;
	}

	public function getType()
	{
		return $this->type;
	}
	public function setType($type)
	{
		$this->type = $type;
	}
}