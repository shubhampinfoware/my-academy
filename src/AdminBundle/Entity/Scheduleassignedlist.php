<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="schedule_assigned_list")
*/
class Scheduleassignedlist
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $schedule_assigned_list_id;

	/**
	* @ORM\Column(type="string")
	*/
	protected $schedule_title="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $schedule_day_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $schedule_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $schedule_date="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $course_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $unit_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $trainer_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $created_datetime="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $is_deleted=0;

	public function getSchedule_assigned_list_id()
	{
		return $this->schedule_assigned_list_id;
	}

	public function getSchedule_title()
	{
		return $this->schedule_title;
	}
	public function setSchedule_title($schedule_title)
	{
		$this->schedule_title = $schedule_title;
	}

	public function getSchedule_day_id()
	{
		return $this->schedule_day_id;
	}
	public function setSchedule_day_id($schedule_day_id)
	{
		$this->schedule_day_id = $schedule_day_id;
	}

	public function getSchedule_id()
	{
		return $this->schedule_id;
	}
	public function setSchedule_id($schedule_id)
	{
		$this->schedule_id = $schedule_id;
	}

	public function getSchedule_date()
	{
		return $this->schedule_date;
	}
	public function setSchedule_date($schedule_date)
	{
		$this->schedule_date = $schedule_date;
	}

	public function getCourse_id()
	{
		return $this->course_id;
	}
	public function setCourse_id($course_id)
	{
		$this->course_id = $course_id;
	}

	public function getUnit_id()
	{
		return $this->unit_id;
	}
	public function setUnit_id($unit_id)
	{
		$this->unit_id = $unit_id;
	}

	public function getTrainer_id()
	{
		return $this->trainer_id;
	}
	public function setTrainer_id($trainer_id)
	{
		$this->trainer_id = $trainer_id;
	}

	public function getCreated_datetime()
	{
		return $this->created_datetime;
	}
	public function setCreated_datetime($created_datetime)
	{
		$this->created_datetime = $created_datetime;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}
}