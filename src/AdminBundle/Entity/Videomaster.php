<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="video_master")
*/
class Videomaster
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $video_master_id;

	/**
	* @ORM\Column(type="string")
	*/
	protected $description="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $is_deleted="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $name="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $playlist_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $publishdate="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $video_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $viewcount="";

	public function getVideo_master_id()
	{
		return $this->video_master_id;
	}

	public function getDescription()
	{
		return $this->description;
	}
	public function setDescription($description)
	{
		$this->description = $description;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}

	public function getName()
	{
		return $this->name;
	}
	public function setName($name)
	{
		$this->name = $name;
	}

	public function getPlaylist_id()
	{
		return $this->playlist_id;
	}
	public function setPlaylist_id($playlist_id)
	{
		$this->playlist_id = $playlist_id;
	}

	public function getPublishdate()
	{
		return $this->publishdate;
	}
	public function setPublishdate($publishdate)
	{
		$this->publishdate = $publishdate;
	}

	public function getVideo_id()
	{
		return $this->video_id;
	}
	public function setVideo_id($video_id)
	{
		$this->video_id = $video_id;
	}

	public function getViewcount()
	{
		return $this->viewcount;
	}
	public function setViewcount($viewcount)
	{
		$this->viewcount = $viewcount;
	}
}