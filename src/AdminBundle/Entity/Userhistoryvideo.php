<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="user_history_video")
*/
class Userhistoryvideo
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $user_history_video_id;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $course_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $extra1_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $extra_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $is_deleted="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $main_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $user_id=0;

	public function getUser_history_video_id()
	{
		return $this->user_history_video_id;
	}

	public function getCourse_id()
	{
		return $this->course_id;
	}
	public function setCourse_id($course_id)
	{
		$this->course_id = $course_id;
	}

	public function getExtra1_id()
	{
		return $this->extra1_id;
	}
	public function setExtra1_id($extra1_id)
	{
		$this->extra1_id = $extra1_id;
	}

	public function getExtra_id()
	{
		return $this->extra_id;
	}
	public function setExtra_id($extra_id)
	{
		$this->extra_id = $extra_id;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}

	public function getMain_id()
	{
		return $this->main_id;
	}
	public function setMain_id($main_id)
	{
		$this->main_id = $main_id;
	}

	public function getUser_id()
	{
		return $this->user_id;
	}
	public function setUser_id($user_id)
	{
		$this->user_id = $user_id;
	}
}