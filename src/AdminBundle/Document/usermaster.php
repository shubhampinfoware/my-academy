<?php

namespace AdminBundle\Document;



/**
 * AdminBundle\Document\usermaster
 */
class usermaster
{
    /**
     * @var MongoId $id
     */
    protected $id;

    /**
     * @var string $user_first_name
     */
    protected $user_first_name;

    /**
     * @var string $user_last_name
     */
    protected $user_last_name;

    /**
     * @var string $user_email_password
     */
    protected $user_email_password;

    /**
     * @var string $user_password
     */
    protected $user_password;


     /**
     * @var string $user_mobile
     */
    protected $user_mobile;

    /**
     * @var string $user_status
     */
    protected $user_status;

    /**
     * @var string $user_role_id
     */
    protected $user_role_id;

    /**
     * @var string $domain_id
     */
    protected $domain_id;

    /**
     * @var string $create_date
     */
    protected $create_date;

       /**
     * @var string $logindate
     */
    protected $logindate;

     /**
     * @var string $birthdate
     */
    protected $birthdate;

     /**
     * @var string $ext_field
     */
    protected $ext_field;

    /**
     * @var string $media_id
     */
    protected $media_id;

    /**
     * @var string $ext_field1
     */
    protected $ext_field1;
    /**
     * @var string $register_from
     */
    protected $register_from;
    /**
     * @var string $external_id
     */
    protected $external_id;

    /**
     * @var string $is_deleted
     */
    protected $is_deleted;


    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userFirstName
     *
     * @param string $userFirstName
     * @return self
     */
    public function setUserFirstName($userFirstName)
    {
        $this->user_first_name = $userFirstName;
        return $this;
    }

    /**
     * Get userFirstName
     *
     * @return string $userFirstName
     */
    public function getUserFirstName()
    {
        return $this->user_first_name;
    }

    /**
     * Set userLastName
     *
     * @param string $userLastName
     * @return self
     */
    public function setUserLastName($userLastName)
    {
        $this->user_last_name = $userLastName;
        return $this;
    }

    /**
     * Get userLastName
     *
     * @return string $userLastName
     */
    public function getUserLastName()
    {
        return $this->user_last_name;
    }

    /**
     * Set userEmailPassword
     *
     * @param string $userEmailPassword
     * @return self
     */
    public function setUserEmailPassword($userEmailPassword) //emailadress only
    {
        $this->user_email_password = $userEmailPassword;
        return $this;
    }

    /**
     * Get userEmailPassword
     *
     * @return string $userEmailPassword
     */
    public function getUserEmailPassword()
    {
        return $this->user_email_password;
    }

    /**
     * Set userPassword
     *
     * @param string $userPassword
     * @return self
     */
    public function setUserPassword($userPassword)
    {
        $this->user_password = $userPassword;
        return $this;
    }

    /**
     * Get userPassword
     *
     * @return string $userPassword
     */
    public function getUserPassword()
    {
        return $this->user_password;
    }

    /**
     * Set userMobile
     *
     * @param string $userMobile
     * @return self
     */
    public function setUserMobile($userMobile)
    {
        $this->user_mobile = $userMobile;
        return $this;
    }

    /**
     * Get userMobile
     *
     * @return string $userMobile
     */
    public function getUser_mobile()
    {
        return $this->user_mobile;
    }

    /**
     * Set userStatus
     *
     * @param string $userStatus
     * @return self
     */
    public function setUserStatus($userStatus)
    {
        $this->user_status = $userStatus;
        return $this;
    }

    /**
     * Get userStatus
     *
     * @return string $userStatus
     */
    public function getUserStatus()
    {
        return $this->user_status;
    }

    /**
     * Set userRoleId
     *
     * @param string $userRoleId
     * @return self
     */
    public function setUserRoleId($userRoleId)
    {
        $this->user_role_id = $userRoleId;
        return $this;
    }

    /**
     * Get userRoleId
     *
     * @return string $userRoleId
     */
    public function getUserRoleId()
    {
        return $this->user_role_id;
    }

    /**
     * Set domainId
     *
     * @param string $domainId
     * @return self
     */
    public function setDomainId($domainId)
    {
        $this->domain_id = $domainId;
        return $this;
    }

    /**
     * Get domainId
     *
     * @return string $domainId
     */
    public function getDomainId()
    {
        return $this->domain_id;
    }

    /**
     * Set createDate
     *
     * @param string $createDate
     * @return self
     */
    public function setCreateDate($createDate)
    {
        $this->create_date = $createDate;
        return $this;
    }

    /**
     * Get createDate
     *
     * @return string $createDate
     */
    public function getCreate_date()
    {
        return $this->create_date;
    }

    /**
     * Set logindate
     *
     * @param string $logindate
     * @return self
     */
    public function setLogindate($logindate)
    {
        $this->logindate = $logindate;
        return $this;
    }

    /**
     * Get logindate
     *
     * @return string $logindate
     */
    public function getLogindate()
    {
        return $this->logindate;
    }

    /**
     * Set languageId
     *
     * @param string $languageId
     * @return self
     */
    public function setLanguage_id($languageId)
    {
        $this->language_id = $languageId;
        return $this;
    }

    /**
     * Get languageId
     *
     * @return string $languageId
     */
    public function getLanguageId()
    {
        return $this->language_id;
    }

    /**
     * Set extField
     *
     * @param string $extField
     * @return self
     */
    public function setExtField($extField)  //verify mobile
    {
        $this->ext_field = $extField;
        return $this;
    }

    /**
     * Get extField
     *
     * @return string $extField
     */
    public function getExtField()
    {
        return $this->ext_field;
    }

    /**
     * Set extField1
     *
     * @param string $extField1
     * @return self
     */
    public function setExtField1($extField1)
    {
        $this->ext_field1 = $extField1;
        return $this;
    }

    /**
     * Get extField1
     *
     * @return string $extField1
     */
    public function getExtField1()
    {
        return $this->ext_field1;
    }

    /**
     * Set isDeleted
     *
     * @param string $isDeleted
     * @return self
     */
    public function setIsDeleted($isDeleted)
    {
        $this->is_deleted = $isDeleted;
        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return string $isDeleted
     */
    public function getIsDeleted()
    {
        return $this->is_deleted;
    }

    /**
     * Set birthdate
     *
     * @param string $birthdate
     * @return self
     */
    public function setBirthdate($birthdate)
    {
        $this->birthdate = $birthdate;
        return $this;
    }

    /**
     * Get birthdate
     *
     * @return string $birthdate
     */
    public function getBirthdate()
    {
        return $this->birthdate;
    }

    /**
     * Set mediaId
     *
     * @param string $mediaId
     * @return self
     */
    public function setMediaId($mediaId)
    {
        $this->media_id = $mediaId;
        return $this;
    }

    /**
     * Get mediaId
     *
     * @return string $mediaId
     */
    public function getMediaId()
    {
        return $this->media_id;
    }
    /**
     * Set register_from
     *
     * @param string $register_from
     * @return self
     */
    public function setRegisterFrom($register_from)
    {
        $this->register_from = $register_from;
        return $this;
    }

    /**
     * Get register_from
     *
     * @return string $register_from
     */
    public function getRegister_from()
    {
        return $this->register_from;
    }
    /**
     * Set external_id
     *
     * @param string $external_id
     * @return self
     */
    public function setExternalId($external_id)
    {
        $this->external_id = $external_id;
        return $this;
    }

    /**
     * Get external_id
     *
     * @return string $external_id
     */
    public function getExternalId()
    {
        return $this->external_id;
    }
}
