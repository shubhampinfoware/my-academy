<?php

namespace AdminBundle\Document;


/**
 * AdminBundle\Document\transaction_master
 */
class transaction_master
{
    /**
     * @var MongoId $id
     */
    protected $id;

    /**
     * @var string $ref_number
     */
    protected $ref_number;

    /**
     * @var string $payment_id
     */
    protected $payment_id;

    /**
     * @var string $product_id
     */
    protected $product_id;

    /**
     * @var string $user_id
     */
    protected $user_id;

    /**
     * @var string $email
     */
    protected $email;

    /**
     * @var string $status
     */
    protected $status;
    

    /**
     * @var string $unmappedstatus
    */
    protected $unmappedstatus;

    /**
     * @var string $txnid
     */
    protected $txnid;

    /**
     * @var string $amount
     */
    protected $amount;
	  /**
     * @var string $nameoncard
     */
    protected $nameoncard;

	/**
     * @var string $error_message
     */
    protected $error_message; 
	/**
     * @var string $txn_field_1
     */
    protected $txn_field_1; 
	/**
     * @var string $txn_field_2
     */
    protected $txn_field_2;
	
	/**
     * @var string $txn_field_3
     */
    protected $txn_field_3;
	/**
     * @var string $created_datetime
     */
    protected $created_datetime;
	/**
     * @var string $is_deleted
     */
    protected $is_deleted;
	
    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ref_number
     *
     * @param string $ref_number
     * @return self
     */
    public function setRef_number($ref_number)
    {
        $this->ref_number = $ref_number;
        return $this;
    }

    /**
     * Get ref_number
     *
     * @return string $ref_number
     */
    public function getRef_number()
    {
        return $this->ref_number;
    }

    /**
     * Set payment_id
     *
     * @param string $payment_id
     * @return self
     */
    public function setPayment_id($payment_id)
    {
        $this->payment_id = $payment_id;
        return $this;
    }

    /**
     * Get payment_id
     *
     * @return string $payment_id
     */
    public function getPayment_id()
    {
        return $this->payment_id;
    }

    /**
     * Set product_id
     *
     * @param string $product_id
     * @return self
     */
    public function setProduct_id($product_id)
    {
        $this->product_id = $product_id;
        return $this;
    }

    /**
     * Get product_id
     *
     * @return string $product_id
     */
    public function getProduct_id()
    {
        return $this->product_id;
    }

    /**
     * Set user_id
     *
     * @param string $user_id
     * @return self
     */
    public function setUser_id($user_id)
    {
        $this->user_id = $user_id;
        return $this;
    }

    /**
     * Get user_id
     *
     * @return string $user_id
     */
    public function getUser_id()
    {
        return $this->user_id;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return self
     */
    public function setEmail($email)
    {
        $this->app_id = $email;
        return $this;
    }

    /**
     * Get email
     *
     * @return string $email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return self
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * Get status
     *
     * @return string $status
     */
    public function geStatus()
    {
        return $this->status;
    }

    /**
     * Set unmappedstatus
     *
     * @param string $unmappedstatus
     * @return self
     */
    public function setUnmappedstatus($unmappedstatus)
    {
        $this->unmappedstatus = $unmappedstatus;
        return $this;
    }

    /**
     * Get unmappedstatus
     *
     * @return string $unmappedstatus
     */
    public function getUnmappedstatus()
    {
        return $this->unmappedstatus;
    }

    /**
     * Set txnid
     *
     * @param string $txnid
     * @return self
     */
    public function setTxnid($txnid)
    {
        $this->txnid = $txnid;
        return $this;
    }

    /**
     * Get txnid
     *
     * @return string $txnid
     */
    public function getTxnid()
    {
        return $this->txnid;
    }

    /**
     * Set amount
     *
     * @param string $amount
     * @return self
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * Get amount
     *
     * @return string $amount
     */
    public function getAmount()
    {
        return $this->amount;
    }
	
	 /**
     * Set nameoncard
     *
     * @param string $nameoncard
     * @return self
     */
    public function setNameoncard($nameoncard)
    {
        $this->nameoncard = $nameoncard;
        return $this;
    }

    /**
     * Get nameoncard
     *
     * @return string $nameoncard
     */
    public function getNameoncard()
    {
        return $this->nameoncard;
    }

	/**
     * Set error_message
     *
     * @param string $error_message
     * @return self
     */
    public function setError_message($error_message)
    {
        $this->error_message = $error_message;
        return $this;
    }

    /**
     * Get error_message
     *
     * @return string $error_message
     */
    public function getError_message()
    {
        return $this->error_message;
    } 
	
	/**
     * Set txn_field_1
     *
     * @param string $txn_field_1
     * @return self
     */
    public function setTxn_field_1($txn_field_1)
    {
        $this->txn_field_1 = $txn_field_1;
        return $this;
    }

    /**
     * Get txn_field_1
     *
     * @return string $txn_field_1
     */
    public function getTxn_field_1()
    {
        return $this->txn_field_1;
    } 
	
	/**
     * Set txn_field_2
     *
     * @param string $txn_field_2
     * @return self
     */
    public function setTxn_field_2($txn_field_2)
    {
        $this->txn_field_2 = $txn_field_2;
        return $this;
    }

    /**
     * Get txn_field_2
     *
     * @return string $txn_field_2
     */
    public function getTxn_field_2()
    {
        return $this->txn_field_2;
    } 
	/**
     * Set txn_field_3
     *
     * @param string $txn_field_3
     * @return self
     */
    public function setTxn_field_3($txn_field_3)
    {
        $this->txn_field_3 = $txn_field_3;
        return $this;
    }

    /**
     * Get txn_field_3
     *
     * @return string $txn_field_3
     */
    public function getTxn_field_3()
    {
        return $this->txn_field_3;
    } /**
     * Set created_datetime
     *
     * @param string $created_datetime
     * @return self
     */
    public function setCreated_datetime($created_datetime)
    {
        $this->created_datetime = $created_datetime;
        return $this;
    }

    /**
     * Get created_datetime
     *
     * @return string $created_datetime
     */
    public function getCreated_datetime()
    {
        return $this->created_datetime;
    } /**
     * Set is_deleted
     *
     * @param string $is_deleted
     * @return self
     */
    public function setIs_deleted($is_deleted)
    {
        $this->is_deleted = $is_deleted;
        return $this;
    }

    /**
     * Get is_deleted
     *
     * @return string $is_deleted
     */
    public function getIs_deleted()
    {
        return $this->is_deleted;
    }
}
