<?php
    namespace AdminBundle\Document;

    use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

    /**
     * @MongoDB\Document
     */
    class bookmark
    {
        /**
         * @MongoDB\Id
         */
        protected $id;

        /**
         * @MongoDB\Field(type="string")
         */
        protected $userid;

          /**
         * @MongoDB\Field(type="string")
         */
        protected $type;

        /**
         * @MongoDB\Field(type="string")
         */
        protected $videoid;

        /**
         * @MongoDB\Field(type="string")
         */
        protected $isdeleted;
    
    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userid
     *
     * @param string $userid
     * @return self
     */
    public function setUserid($userid)
    {
        $this->userid = $userid;
        return $this;
    }

    /**
     * Get userid
     *
     * @return string $userid
     */
    public function getUserid()
    {
        return $this->userid;
    }

    /**
     * Set videoid
     *
     * @param string $videoid
     * @return self
     */
    public function setVideoid($videoid)
    {
        $this->videoid = $videoid;
        return $this;
    }

    /**
     * Get videoid
     *
     * @return string $videoid
     */
    public function getVideoid()
    {
        return $this->videoid;
    }

    /**
     * Set isdeleted
     *
     * @param string $isdeleted
     * @return self
     */
    public function setIsdeleted($isdeleted)
    {
        $this->isdeleted = $isdeleted;
        return $this;
    }

    /**
     * Get isdeleted
     *
     * @return string $isdeleted
     */
    public function getIsdeleted()
    {
        return $this->isdeleted;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return self
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * Get type
     *
     * @return string $type
     */
    public function getType()
    {
        return $this->type;
    }
}
