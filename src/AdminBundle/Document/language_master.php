<?php
    namespace AdminBundle\Document;

    use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

    /**
     * @MongoDB\Document
     */
    class language_master
    {
        /**
         * @MongoDB\Id
         */
        protected $id;
        /**
         * @MongoDB\Field(type="string")
         */
        protected $language_name;
        /**
         * @MongoDB\Field(type="string")
         */
        protected $display_title;
        /**
         * @MongoDB\Field(type="string")
         */
        protected $language_code;
        /**
         * @MongoDB\Field(type="string")
         */
        protected $is_deleted;
    

    
    
    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set languageName
     *
     * @param string $languageName
     * @return self
     */
    public function setLanguageName($languageName)
    {
        $this->language_name = $languageName;
        return $this;
    }

    /**
     * Get languageName
     *
     * @return string $languageName
     */
    public function getLanguageName()
    {
        return $this->language_name;
    }

    /**
     * Set displayTitle
     *
     * @param string $displayTitle
     * @return self
     */
    public function setDisplayTitle($displayTitle)
    {
        $this->display_title = $displayTitle;
        return $this;
    }

    /**
     * Get displayTitle
     *
     * @return string $displayTitle
     */
    public function getDisplayTitle()
    {
        return $this->display_title;
    }

    /**
     * Set languageCode
     *
     * @param string $languageCode
     * @return self
     */
    public function setLanguageCode($languageCode)
    {
        $this->language_code = $languageCode;
        return $this;
    }

    /**
     * Get languageCode
     *
     * @return string $languageCode
     */
    public function getLanguageCode()
    {
        return $this->language_code;
    }

    /**
     * Set isDeleted
     *
     * @param string $isDeleted
     * @return self
     */
    public function setIsDeleted($isDeleted)
    {
        $this->is_deleted = $isDeleted;
        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return string $isDeleted
     */
    public function getIsDeleted()
    {
        return $this->is_deleted;
    }
}
