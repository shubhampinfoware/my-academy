<?php
    namespace AdminBundle\Document;

    use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

    /**
     * @MongoDB\Document
     */
    class media_type

    {
        /**
         * @MongoDB\Id
         */
        protected $id;
        /**
         * @MongoDB\Field(type="string")
         */
        protected $media_type_name;
        /**
         * @MongoDB\Field(type="string")
         */
        protected $media_type_description;
        /**
         * @MongoDB\Field(type="string")
         */
        protected $media_type_allowed;
        /**
         * @MongoDB\Field(type="string")
         */
        protected $language_id;
        /**
         * @MongoDB\Field(type="string")
         */
        protected $main_media_type_id;
        /**
         * @MongoDB\Field(type="string")
         */
        protected $media_status;
        /**
         * @MongoDB\Field(type="string")
         */
        protected $is_deleted;
    
    
    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set mediaTypeName
     *
     * @param string $mediaTypeName
     * @return self
     */
    public function setMediaTypeName($mediaTypeName)
    {
        $this->media_type_name = $mediaTypeName;
        return $this;
    }

    /**
     * Get mediaTypeName
     *
     * @return string $mediaTypeName
     */
    public function getMediaTypeName()
    {
        return $this->media_type_name;
    }

    /**
     * Set mediaTypeDescription
     *
     * @param string $mediaTypeDescription
     * @return self
     */
    public function setMediaTypeDescription($mediaTypeDescription)
    {
        $this->media_type_description = $mediaTypeDescription;
        return $this;
    }

    /**
     * Get mediaTypeDescription
     *
     * @return string $mediaTypeDescription
     */
    public function getMediaTypeDescription()
    {
        return $this->media_type_description;
    }

    /**
     * Set mediaTypeAllowed
     *
     * @param string $mediaTypeAllowed
     * @return self
     */
    public function setMediaTypeAllowed($mediaTypeAllowed)
    {
        $this->media_type_allowed = $mediaTypeAllowed;
        return $this;
    }

    /**
     * Get mediaTypeAllowed
     *
     * @return string $mediaTypeAllowed
     */
    public function getMediaTypeAllowed()
    {
        return $this->media_type_allowed;
    }

    /**
     * Set languageId
     *
     * @param string $languageId
     * @return self
     */
    public function setLanguage_id($languageId)
    {
        $this->language_id = $languageId;
        return $this;
    }

    /**
     * Get languageId
     *
     * @return string $languageId
     */
    public function getLanguageId()
    {
        return $this->language_id;
    }

    /**
     * Set mainMediaTypeId
     *
     * @param string $mainMediaTypeId
     * @return self
     */
    public function setMainMediaTypeId($mainMediaTypeId)
    {
        $this->main_media_type_id = $mainMediaTypeId;
        return $this;
    }

    /**
     * Get mainMediaTypeId
     *
     * @return string $mainMediaTypeId
     */
    public function getMainMediaTypeId()
    {
        return $this->main_media_type_id;
    }

    /**
     * Set mediaStatus
     *
     * @param string $mediaStatus
     * @return self
     */
    public function setMediaStatus($mediaStatus)
    {
        $this->media_status = $mediaStatus;
        return $this;
    }

    /**
     * Get mediaStatus
     *
     * @return string $mediaStatus
     */
    public function getMediaStatus()
    {
        return $this->media_status;
    }

    /**
     * Set isDeleted
     *
     * @param string $isDeleted
     * @return self
     */
    public function setIsDeleted($isDeleted)
    {
        $this->is_deleted = $isDeleted;
        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return string $isDeleted
     */
    public function getIsDeleted()
    {
        return $this->is_deleted;
    }
}
