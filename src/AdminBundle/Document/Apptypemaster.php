<?php

namespace AdminBundle\Document;



/**
 * AdminBundle\Document\Apptypemaster
 */
class Apptypemaster
{
    /**
     * @var MongoId $id
     */
    protected $id;

    /**
     * @var string $app_type_name
     */
    protected $app_type_name;

    /**
     * @var string $app_type_code
     */
    protected $app_type_code;

    /**
     * @var string $app_type_status
     */
    protected $app_type_status;

    /**
     * @var string $is_deleted
     */
    protected $is_deleted;

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set appTypeName
     *
     * @param string $appTypeName
     * @return self
     */
    public function setAppTypeName($appTypeName)
    {
        $this->app_type_name = $appTypeName;
        return $this;
    }

    /**
     * Get appTypeName
     *
     * @return string $appTypeName
     */
    public function getAppTypeName()
    {
        return $this->app_type_name;
    }

    /**
     * Set appTypeCode
     *
     * @param string $appTypeCode
     * @return self
     */
    public function setAppTypeCode($appTypeCode)
    {
        $this->app_type_code = $appTypeCode;
        return $this;
    }

    /**
     * Get appTypeCode
     *
     * @return string $appTypeCode
     */
    public function getAppTypeCode()
    {
        return $this->app_type_code;
    }

    /**
     * Set appTypeStatus
     *
     * @param string $appTypeStatus
     * @return self
     */
    public function setAppTypeStatus($appTypeStatus)
    {
        $this->app_type_status = $appTypeStatus;
        return $this;
    }

    /**
     * Get appTypeStatus
     *
     * @return string $appTypeStatus
     */
    public function getAppTypeStatus()
    {
        return $this->app_type_status;
    }

    /**
     * Set isDeleted
     *
     * @param string $isDeleted
     * @return self
     */
    public function setIsDeleted($isDeleted)
    {
        $this->is_deleted = $isDeleted;
        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return string $isDeleted
     */
    public function getIsDeleted()
    {
        return $this->is_deleted;
    }
}
