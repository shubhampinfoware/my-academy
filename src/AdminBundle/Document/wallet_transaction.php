<?php

namespace AdminBundle\Document;

/**
 * AdminBundle\Document\wallet_transaction
 */
class wallet_transaction
{
    /**
     * @var MongoId $id
     */
    protected $id;

    /**
     * @var string $user_id
     */
    protected $user_id;

    /**
     * @var string $wallet_id
     */
    protected $wallet_id;

    /**
     * @var string $wallet_transaction
     */
    protected $wallet_transaction;

    /**
     * @var string $wallet_transaction_amount
     */
    protected $wallet_transaction_amount;

    /**
     * @var string $updated_wallet_balance
     */
    protected $updated_wallet_balance;

    /**
     * @var string $transaction_for_table
     */
    protected $transaction_for_table;
    

    /**
     * @var string $transaction_for_table_id
    */
    protected $transaction_for_table_id;

    /**
     * @var string $wallet_transaction_datetime
     */
    protected $wallet_transaction_datetime;

    /**
     * @var string $wallet_transaction_by
     */
    protected $wallet_transaction_by;
 /**
     * @var string $is_deleted
    */
    protected $is_deleted;
 /**
     * @var string $actual_payment_transaction_id
    */
    protected $actual_payment_transaction_id;
 /**
     * @var string $balance_type
    */
    protected $balance_type;
 /**
     * @var string $expiry_date_of_balance
    */
    protected $expiry_date_of_balance;
 /**
     * @var string $is_archive
    */
    protected $is_archive;
 /**
     * @var string $transaction_note
    */
    protected $transaction_note;
 /**
     * @var string $wallet_transaction_field_1
    */
    protected $wallet_transaction_field_1;
 /**
     * @var string $wallet_transaction_field_2
    */
    protected $wallet_transaction_field_2;
 /**
     * @var string $wallet_transaction_field_3
    */
    protected $wallet_transaction_field_3;

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user_id
     *
     * @param string $user_id
     * @return self
     */
    public function setUser_id($user_id)
    {
        $this->user_id = $user_id;
        return $this;
    }
    /**
     * Get user_id
     *
     * @return string $user_id
     */
    public function getUser_id()
    {
        return $this->user_id;
    }


 /**
     * Set wallet_id
     *
     * @param string $wallet_id
     * @return self
     */
    public function setWallet_id($wallet_id)
    {
        $this->wallet_id = $wallet_id;
        return $this;
    }
    /**
     * Get wallet_id
     *
     * @return string $wallet_id
     */
    public function getWallet_id()
    {
        return $this->wallet_id;
    }
    
    
    
    
     /**
    * Set wallet_transaction
    *
    * @param string $wallet_transaction
    * @return self
    */
   public function setWallet_transaction($wallet_transaction)
   {
       $this->wallet_transaction = $wallet_transaction;
       return $this;
   }
   /**
    * Get wallet_transaction
    
    * @return string $wallet_transaction
    */
   public function getWallet_transaction()
   {
       return $this->wallet_transaction;
   } 
   
   /**
   * Set wallet_transaction_amount
   *
   * @param string $wallet_transaction_amount
   * @return self
   */
  public function setWallet_transaction_amount($wallet_transaction_amount)
  {
      $this->wallet_transaction_amount = $wallet_transaction_amount;
      return $this;
  }
  /**
   * Get wallet_transaction_amount
   *
   * @return string $wallet_transaction_amount
   */
  public function getWallet_transaction_amount()
  {
      return $this->wallet_transaction_amount;
  }
  
  
  
  
   /**
  * Set updated_wallet_balance
  *
  * @param string $updated_wallet_balance
  * @return self
  */
 public function setUpdated_wallet_balance($updated_wallet_balance)
 {
     $this->updated_wallet_balance = $updated_wallet_balance;
     return $this;
 }
 /**
  * Get updated_wallet_balance
  *
  
  * @return string $updated_wallet_balance
  */
 public function getUpdated_wallet_balance()
 {
     return $this->updated_wallet_balance;
 } 
 
 /**
 * Set transaction_for_table
 *
 * @param string $transaction_for_table
 * @return self
 */
public function setTransaction_for_table($transaction_for_table)
{
    $this->transaction_for_table = $transaction_for_table;
    return $this;
}
/**
 * Get transaction_for_table
 *
 
 * @return string $transaction_for_table
 */
public function getTransaction_for_table()
{
    return $this->transaction_for_table;
} 

/**
* Set transaction_for_table_id
*
* @param string $transaction_for_table_id
* @return self
*/
public function setTransaction_for_table_id($transaction_for_table_id)
{
   $this->transaction_for_table_id = $transaction_for_table_id;
   return $this;
}
/**
* Get transaction_for_table_id
*
* @return string $transaction_for_table_id
*/
public function getTransaction_for_table_id()
{
   return $this->transaction_for_table_id;
} 

/**
* Set wallet_transaction_datetime
*
* @param string $wallet_transaction_datetime
* @return self
*/
public function setWallet_transaction_datetime($wallet_transaction_datetime)
{
   $this->wallet_transaction_datetime = $wallet_transaction_datetime;
   return $this;
}
/**
* Get wallet_transaction_datetime
*
* @return string $wallet_transaction_datetime
*/
public function getWallet_transaction_datetime()
{
   return $this->wallet_transaction_datetime;
} 

/**
* Set wallet_transaction_by
*
* @param string $wallet_transaction_by
* @return self
*/
public function setWallet_transaction_by($wallet_transaction_by)
{
   $this->wallet_transaction_by = $wallet_transaction_by;
   return $this;
}
/**
* Get wallet_transaction_by
*
* @return string $wallet_transaction_by
*/
public function getWallet_transaction_by()
{
   return $this->wallet_transaction_by;
} 


/**
* Set is_deleted
*
* @param string $is_deleted
* @return self
*/
public function setIs_deleted($is_deleted)
{
   $this->is_deleted = $is_deleted;
   return $this;
}

/**
* Get actual_payment_transaction_id
*
* @return string $is_deleted
*/
public function getIs_deleted()
{
   return $this->is_deleted;
} 

/**
* Set actual_payment_transaction_id
*
* @param string $actual_payment_transaction_id
* @return self
*/
public function setActual_payment_transaction_id($actual_payment_transaction_id)
{
   $this->actual_payment_transaction_id = $actual_payment_transaction_id;
   return $this;
}
/**
* Get actual_payment_transaction_id
*
* @return string $actual_payment_transaction_id
*/
public function getActual_payment_transaction_id()
{
   return $this->actual_payment_transaction_id;
} 

/**
* Set balance_type
*
* @param string $balance_type
* @return self
*/
public function setBalance_type($balance_type)
{
   $this->balance_type = $balance_type;
   return $this;
}
/**
* Get balance_type
*
* @return string $balance_type
*/
public function getBalance_type()
{
   return $this->balance_type;
} 

/**
* Set expiry_date_of_balance
*
* @param string $expiry_date_of_balance
* @return self
*/
public function setExpiry_date_of_balance($expiry_date_of_balance)
{
   $this->expiry_date_of_balance = $expiry_date_of_balance;
   return $this;
}
/**
* Get expiry_date_of_balance
*
* @return string $expiry_date_of_balance
*/
public function getExpiry_date_of_balance()
{
   return $this->expiry_date_of_balance;
} 

/**
* Set is_archive
*
* @param string $is_archive
* @return self
*/
public function setIs_archive($is_archive)
{
   $this->is_archive = $is_archive;
   return $this;
}
/**
* Get is_archive
*
* @return string $is_archive
*/
public function getIs_archive()
{
   return $this->is_archive;
} 


/**
* Set transaction_note
*
* @param string $transaction_note
* @return self
*/
public function setTransaction_note($transaction_note)
{
   $this->transaction_note = $transaction_note;
   return $this;
}
/**
* Get transaction_note

* @return string $hello
*/
public function getTransaction_note()
{
   return $this->transaction_note;
}

 /**
* Set wallet_transaction_field_1
*
* @param string $wallet_transaction_field_1
* @return self
*/
public function setWallet_transaction_field_1($wallet_transaction_field_1)
{
   $this->wallet_transaction_field_1 = $wallet_transaction_field_1;
   return $this;
}
/**
* Get wallet_transaction_field_1
*
* @return string $wallet_transaction_field_1
*/
public function getWallet_transaction_field_1()
{
   return $this->wallet_transaction_field_1;
} 
 /**
* Set wallet_transaction_field_2
*
* @param string $wallet_transaction_field_2
* @return self
*/
public function setWallet_transaction_field_2($wallet_transaction_field_2)
{
   $this->wallet_transaction_field_2 = $wallet_transaction_field_2;
   return $this;
}
/**
* Get wallet_transaction_field_2

* @return string $wallet_transaction_field_2
*/
public function getWallet_transaction_field_2()
{
   return $this->wallet_transaction_field21;
}  /**
* Set wallet_transaction_field_3
*
* @param string $wallet_transaction_field_3
* @return self
*/
public function setWallet_transaction_field_3($wallet_transaction_field_3)
{
   $this->wallet_transaction_field_3 = $wallet_transaction_field_3;
   return $this;
}
/**
* Get wallet_transaction_field_3
*
* @return string $wallet_transaction_field_3
*/
public function getWallet_transaction_field_3()
{
   return $this->wallet_transaction_field_3;
} 


    /**
     * Set userId
     *
     * @param string $userId
     * @return self
     */
    public function setUserId($userId)
    {
        $this->user_id = $userId;
        return $this;
    }

    /**
     * Get userId
     *
     * @return string $userId
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Set walletId
     *
     * @param string $walletId
     * @return self
     */
    public function setWalletId($walletId)
    {
        $this->wallet_id = $walletId;
        return $this;
    }

    /**
     * Get walletId
     *
     * @return string $walletId
     */
    public function getWalletId()
    {
        return $this->wallet_id;
    }

    /**
     * Set walletTransaction
     *
     * @param string $walletTransaction
     * @return self
     */
    public function setWalletTransaction($walletTransaction)
    {
        $this->wallet_transaction = $walletTransaction;
        return $this;
    }

    /**
     * Get walletTransaction
     *
     * @return string $walletTransaction
     */
    public function getWalletTransaction()
    {
        return $this->wallet_transaction;
    }

    /**
     * Set walletTransactionAmount
     *
     * @param string $walletTransactionAmount
     * @return self
     */
    public function setWalletTransactionAmount($walletTransactionAmount)
    {
        $this->wallet_transaction_amount = $walletTransactionAmount;
        return $this;
    }

    /**
     * Get walletTransactionAmount
     *
     * @return string $walletTransactionAmount
     */
    public function getWalletTransactionAmount()
    {
        return $this->wallet_transaction_amount;
    }

    /**
     * Set updatedWalletBalance
     *
     * @param string $updatedWalletBalance
     * @return self
     */
    public function setUpdatedWalletBalance($updatedWalletBalance)
    {
        $this->updated_wallet_balance = $updatedWalletBalance;
        return $this;
    }

    /**
     * Get updatedWalletBalance
     *
     * @return string $updatedWalletBalance
     */
    public function getUpdatedWalletBalance()
    {
        return $this->updated_wallet_balance;
    }

    /**
     * Set transactionForTable
     *
     * @param string $transactionForTable
     * @return self
     */
    public function setTransactionForTable($transactionForTable)
    {
        $this->transaction_for_table = $transactionForTable;
        return $this;
    }

    /**
     * Get transactionForTable
     *
     * @return string $transactionForTable
     */
    public function getTransactionForTable()
    {
        return $this->transaction_for_table;
    }

    /**
     * Set transactionForTableId
     *
     * @param string $transactionForTableId
     * @return self
     */
    public function setTransactionForTableId($transactionForTableId)
    {
        $this->transaction_for_table_id = $transactionForTableId;
        return $this;
    }

    /**
     * Get transactionForTableId
     *
     * @return string $transactionForTableId
     */
    public function getTransactionForTableId()
    {
        return $this->transaction_for_table_id;
    }

    /**
     * Set walletTransactionDatetime
     *
     * @param string $walletTransactionDatetime
     * @return self
     */
    public function setWalletTransactionDatetime($walletTransactionDatetime)
    {
        $this->wallet_transaction_datetime = $walletTransactionDatetime;
        return $this;
    }

    /**
     * Get walletTransactionDatetime
     *
     * @return string $walletTransactionDatetime
     */
    public function getWalletTransactionDatetime()
    {
        return $this->wallet_transaction_datetime;
    }

    /**
     * Set walletTransactionBy
     *
     * @param string $walletTransactionBy
     * @return self
     */
    public function setWalletTransactionBy($walletTransactionBy)
    {
        $this->wallet_transaction_by = $walletTransactionBy;
        return $this;
    }

    /**
     * Get walletTransactionBy
     *
     * @return string $walletTransactionBy
     */
    public function getWalletTransactionBy()
    {
        return $this->wallet_transaction_by;
    }

    /**
     * Set isDeleted
     *
     * @param string $isDeleted
     * @return self
     */
    public function setIsDeleted($isDeleted)
    {
        $this->is_deleted = $isDeleted;
        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return string $isDeleted
     */
    public function getIsDeleted()
    {
        return $this->is_deleted;
    }

    /**
     * Set actualPaymentTransactionId
     *
     * @param string $actualPaymentTransactionId
     * @return self
     */
    public function setActualPaymentTransactionId($actualPaymentTransactionId)
    {
        $this->actual_payment_transaction_id = $actualPaymentTransactionId;
        return $this;
    }

    /**
     * Get actualPaymentTransactionId
     *
     * @return string $actualPaymentTransactionId
     */
    public function getActualPaymentTransactionId()
    {
        return $this->actual_payment_transaction_id;
    }

    /**
     * Set balanceType
     *
     * @param string $balanceType
     * @return self
     */
    public function setBalanceType($balanceType)
    {
        $this->balance_type = $balanceType;
        return $this;
    }

    /**
     * Get balanceType
     *
     * @return string $balanceType
     */
    public function getBalanceType()
    {
        return $this->balance_type;
    }

    /**
     * Set expiryDateOfBalance
     *
     * @param string $expiryDateOfBalance
     * @return self
     */
    public function setExpiryDateOfBalance($expiryDateOfBalance)
    {
        $this->expiry_date_of_balance = $expiryDateOfBalance;
        return $this;
    }

    /**
     * Get expiryDateOfBalance
     *
     * @return string $expiryDateOfBalance
     */
    public function getExpiryDateOfBalance()
    {
        return $this->expiry_date_of_balance;
    }

    /**
     * Set isArchive
     *
     * @param string $isArchive
     * @return self
     */
    public function setIsArchive($isArchive)
    {
        $this->is_archive = $isArchive;
        return $this;
    }

    /**
     * Get isArchive
     *
     * @return string $isArchive
     */
    public function getIsArchive()
    {
        return $this->is_archive;
    }

    /**
     * Set transactionNote
     *
     * @param string $transactionNote
     * @return self
     */
    public function setTransactionNote($transactionNote)
    {
        $this->transaction_note = $transactionNote;
        return $this;
    }

    /**
     * Get transactionNote
     *
     * @return string $transactionNote
     */
    public function getTransactionNote()
    {
        return $this->transaction_note;
    }

    /**
     * Set walletTransactionField1
     *
     * @param string $walletTransactionField1
     * @return self
     */
    public function setWalletTransactionField1($walletTransactionField1)
    {
        $this->wallet_transaction_field_1 = $walletTransactionField1;
        return $this;
    }

    /**
     * Get walletTransactionField1
     *
     * @return string $walletTransactionField1
     */
    public function getWalletTransactionField1()
    {
        return $this->wallet_transaction_field_1;
    }

    /**
     * Set walletTransactionField2
     *
     * @param string $walletTransactionField2
     * @return self
     */
    public function setWalletTransactionField2($walletTransactionField2)
    {
        $this->wallet_transaction_field_2 = $walletTransactionField2;
        return $this;
    }

    /**
     * Get walletTransactionField2
     *
     * @return string $walletTransactionField2
     */
    public function getWalletTransactionField2()
    {
        return $this->wallet_transaction_field_2;
    }

    /**
     * Set walletTransactionField3
     *
     * @param string $walletTransactionField3
     * @return self
     */
    public function setWalletTransactionField3($walletTransactionField3)
    {
        $this->wallet_transaction_field_3 = $walletTransactionField3;
        return $this;
    }

    /**
     * Get walletTransactionField3
     *
     * @return string $walletTransactionField3
     */
    public function getWalletTransactionField3()
    {
        return $this->wallet_transaction_field_3;
    }
}
