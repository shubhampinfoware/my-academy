<?php
    namespace AdminBundle\Document;

    use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

    /**
     * @MongoDB\Document
     */
    class playlistmaster
    {
        /**
         * @MongoDB\Id
         */
        protected $id;

        /**
         * @MongoDB\Field(type="string")
         */
        protected $name;

        /**
         * @MongoDB\Field(type="string")
         */
        protected $descriptions;

        /**
         * @MongoDB\Field(type="string")
         */
        protected $channelid;
        
        /**
         * @MongoDB\Field(type="string")
         */
        protected $playlist_id;


        /**
         * @MongoDB\Field(type="string")
         */
        protected $is_deleted;

    
    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set channelid
     *
     * @param string $channelid
     * @return self
     */
    public function setChannelid($channelid)
    {
        $this->channelid = $channelid;
        return $this;
    }

    /**
     * Get channelid
     *
     * @return string $channelid
     */
    public function getChannelid()
    {
        return $this->channelid;
    }

    /**
     * Set playlistId
     *
     * @param string $playlistId
     * @return self
     */
    public function setPlaylistId($playlistId)
    {
        $this->playlist_id = $playlistId;
        return $this;
    }

    /**
     * Get playlistId
     *
     * @return string $playlistId
     */
    public function getPlaylistId()
    {
        return $this->playlist_id;
    }

    /**
     * Set isDeleted
     *
     * @param string $isDeleted
     * @return self
     */
    public function setIsDeleted($isDeleted)
    {
        $this->is_deleted = $isDeleted;
        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return string $isDeleted
     */
    public function getIsDeleted()
    {
        return $this->is_deleted;
    }
   


    /**
     * Set description
     *
     * @param string $description
     * @return self
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * Get description
     *
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }
    /**
     * @var string $description
     */
    protected $description;


}
