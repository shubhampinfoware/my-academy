<?php

namespace AdminBundle\Document;



/**
 * AdminBundle\Document\Gcmuser
 */
class Gcmuser
{
    /**
     * @var MongoId $id
     */
    protected $id;

    /**
     * @var string $gcm_regid
     */
    protected $gcm_regid;

    /**
     * @var string $user_id
     */
    protected $user_id;

    /**
     * @var string $device_id
     */
    protected $device_id;

    /**
     * @var string $app_id
     */
    protected $app_id;

    /**
     * @var string $name
     */
    protected $name;

    /**
     * @var string $created_date
     */
    protected $created_date;

    /**
     * @var string $fide
     */
    protected $is_deleted;



    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set gcmRegid
     *
     * @param string $gcmRegid
     * @return self
     */
    public function setGcmRegid($gcmRegid)
    {
        $this->gcm_regid = $gcmRegid;
        return $this;
    }

    /**
     * Get gcmRegid
     *
     * @return string $gcmRegid
     */
    public function getGcmRegid()
    {
        return $this->gcm_regid;
    }

    /**
     * Set userId
     *
     * @param string $userId
     * @return self
     */
    public function setUserId($userId)
    {
        $this->user_id = $userId;
        return $this;
    }

    /**
     * Get userId
     *
     * @return string $userId
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Set deviceId
     *
     * @param string $deviceId
     * @return self
     */
    public function setDeviceId($deviceId)
    {
        $this->device_id = $deviceId;
        return $this;
    }

    /**
     * Get deviceId
     *
     * @return string $deviceId
     */
    public function getDeviceId()
    {
        return $this->device_id;
    }

    /**
     * Set appId
     *
     * @param string $appId
     * @return self
     */
    public function setAppId($appId)
    {
        $this->app_id = $appId;
        return $this;
    }

    /**
     * Get appId
     *
     * @return string $appId
     */
    public function getAppId()
    {
        return $this->app_id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set createdDate
     *
     * @param string $createdDate
     * @return self
     */
    public function setCreatedDate($createdDate)
    {
        $this->created_date = $createdDate;
        return $this;
    }

    /**
     * Get createdDate
     *
     * @return string $createdDate
     */
    public function getCreatedDate()
    {
        return $this->created_date;
    }

    /**
     * Set isDeleted
     *
     * @param string $isDeleted
     * @return self
     */
    public function setIsDeleted($isDeleted)
    {
        $this->is_deleted = $isDeleted;
        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return string $isDeleted
     */
    public function getIsDeleted()
    {
        return $this->is_deleted;
    }
}
