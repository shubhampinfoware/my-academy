<?php

namespace AdminBundle\Document;



/**
 * AdminBundle\Document\Appdetails
 */
class Appdetails
{
    /**
     * @var MongoId $id
     */
    protected $id;

    /**
     * @var string $app_code
     */
    protected $app_code;

    /**
     * @var string $app_name
     */
    protected $app_name;

    /**
     * @var string $app_type_id
     */
    protected $app_type_id;

    /**
     * @var string $domain_id
     */
    protected $domain_id;
	
	/**
     * @var string $app_gcm_key
     */
    protected $app_gcm_key;
	
	/**
     * @var string $app_apns_certificate_development
     */
    protected $app_apns_certificate_development;
	
	/**
     * @var string $app_apns_certificate_development_password
     */
    protected $app_apns_certificate_development_password;
	
	/**
     * @var string $app_apns_certificate_production
     */
    protected $app_apns_certificate_production;
	
	/**
     * @var string $app_apns_certificate_production_password
     */
    protected $app_apns_certificate_production_password;
	
	/**
     * @var string $status
     */
    protected $status;
	
	/**
     * @var string $is_deleted
     */
    protected $is_deleted;

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set appCode
     *
     * @param string $appCode
     * @return self
     */
    public function setAppCode($appCode)
    {
        $this->app_code = $appCode;
        return $this;
    }

    /**
     * Get appCode
     *
     * @return string $appCode
     */
    public function getAppCode()
    {
        return $this->app_code;
    }

    /**
     * Set appName
     *
     * @param string $appName
     * @return self
     */
    public function setAppName($appName)
    {
        $this->app_name = $appName;
        return $this;
    }

    /**
     * Get appName
     *
     * @return string $appName
     */
    public function getAppName()
    {
        return $this->app_name;
    }

    /**
     * Set appTypeId
     *
     * @param string $appTypeId
     * @return self
     */
    public function setAppTypeId($appTypeId)
    {
        $this->app_type_id = $appTypeId;
        return $this;
    }

    /**
     * Get appTypeId
     *
     * @return string $appTypeId
     */
    public function getAppTypeId()
    {
        return $this->app_type_id;
    }

    /**
     * Set domainId
     *
     * @param string $domainId
     * @return self
     */
    public function setDomainId($domainId)
    {
        $this->domain_id = $domainId;
        return $this;
    }

    /**
     * Get domainId
     *
     * @return string $domainId
     */
    public function getDomainId()
    {
        return $this->domain_id;
    }

    /**
     * Set appGcmKey
     *
     * @param string $appGcmKey
     * @return self
     */
    public function setAppGcmKey($appGcmKey)
    {
        $this->app_gcm_key = $appGcmKey;
        return $this;
    }

    /**
     * Get appGcmKey
     *
     * @return string $appGcmKey
     */
    public function getAppGcmKey()
    {
        return $this->app_gcm_key;
    }

    /**
     * Set appApnsCertificateDevelopment
     *
     * @param string $appApnsCertificateDevelopment
     * @return self
     */
    public function setAppApnsCertificateDevelopment($appApnsCertificateDevelopment)
    {
        $this->app_apns_certificate_development = $appApnsCertificateDevelopment;
        return $this;
    }

    /**
     * Get appApnsCertificateDevelopment
     *
     * @return string $appApnsCertificateDevelopment
     */
    public function getAppApnsCertificateDevelopment()
    {
        return $this->app_apns_certificate_development;
    }

    /**
     * Set appApnsCertificateDevelopmentPassword
     *
     * @param string $appApnsCertificateDevelopmentPassword
     * @return self
     */
    public function setAppApnsCertificateDevelopmentPassword($appApnsCertificateDevelopmentPassword)
    {
        $this->app_apns_certificate_development_password = $appApnsCertificateDevelopmentPassword;
        return $this;
    }

    /**
     * Get appApnsCertificateDevelopmentPassword
     *
     * @return string $appApnsCertificateDevelopmentPassword
     */
    public function getAppApnsCertificateDevelopmentPassword()
    {
        return $this->app_apns_certificate_development_password;
    }

    /**
     * Set appApnsCertificateProduction
     *
     * @param string $appApnsCertificateProduction
     * @return self
     */
    public function setAppApnsCertificateProduction($appApnsCertificateProduction)
    {
        $this->app_apns_certificate_production = $appApnsCertificateProduction;
        return $this;
    }

    /**
     * Get appApnsCertificateProduction
     *
     * @return string $appApnsCertificateProduction
     */
    public function getAppApnsCertificateProduction()
    {
        return $this->app_apns_certificate_production;
    }

    /**
     * Set appApnsCertificateProductionPassword
     *
     * @param string $appApnsCertificateProductionPassword
     * @return self
     */
    public function setAppApnsCertificateProductionPassword($appApnsCertificateProductionPassword)
    {
        $this->app_apns_certificate_production_password = $appApnsCertificateProductionPassword;
        return $this;
    }

    /**
     * Get appApnsCertificateProductionPassword
     *
     * @return string $appApnsCertificateProductionPassword
     */
    public function getAppApnsCertificateProductionPassword()
    {
        return $this->app_apns_certificate_production_password;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return self
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * Get status
     *
     * @return string $status
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set isDeleted
     *
     * @param string $isDeleted
     * @return self
     */
    public function setIsDeleted($isDeleted)
    {
        $this->is_deleted = $isDeleted;
        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return string $isDeleted
     */
    public function getIsDeleted()
    {
        return $this->is_deleted;
    }
}
