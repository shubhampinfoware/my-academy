<?php

namespace AdminBundle\Document;



/**
 * AdminBundle\Document\Userinfo
 */
class Userinfo
{
    /**
     * @var MongoId $id
     */
    protected $id;

    /**
     * @var string $userid
     */
    protected $userid;

    /**
     * @var string $firstname
     */
    protected $firstname;

    /**
     * @var string $lastname
     */
    protected $lastname;

    /**
     * @var string $birthdate
     */
    protected $birthdate;

    /**
     * @var integer $mobile
     */
    protected $mobile;

    /**
     * @var string $email
     */
    protected $email;

    /**
     * @var string $fide
     */
    protected $fide;

    /**
     * @var string $createddate
     */
    protected $createddate;

     /**
     * @var string $logindate
     */
    protected $logindate;

    /**
     * @var string $roleid
     */
    protected $roleid;


    /**
     * @var string $mediaid
     */
    protected $mediaid;

    /**
     * @var integer $isdeleted
     */
    protected $isdeleted;



    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userid
     *
     * @param string $userid
     * @return self
     */
    public function setUserid($userid)
    {
        $this->userid = $userid;
        return $this;
    }

    /**
     * Get userid
     *
     * @return string $userid
     */
    public function getUserid()
    {
        return $this->userid;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     * @return self
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
        return $this;
    }

    /**
     * Get firstname
     *
     * @return string $firstname
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     * @return self
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
        return $this;
    }

    /**
     * Get lastname
     *
     * @return string $lastname
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set birthdate
     *
     * @param string $birthdate
     * @return self
     */
    public function setBirthdate($birthdate)
    {
        $this->birthdate = $birthdate;
        return $this;
    }

    /**
     * Get birthdate
     *
     * @return string $birthdate
     */
    public function getBirthdate()
    {
        return $this->birthdate;
    }

    /**
     * Set mobile
     *
     * @param integer $mobile
     * @return self
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;
        return $this;
    }

    /**
     * Get mobile
     *
     * @return integer $mobile
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return self
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * Get email
     *
     * @return string $email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set fide
     *
     * @param string $fide
     * @return self
     */
    public function setFide($fide)
    {
        $this->fide = $fide;
        return $this;
    }

    /**
     * Get fide
     *
     * @return string $fide
     */
    public function getFide()
    {
        return $this->fide;
    }

    /**
     * Set roleid
     *
     * @param string $roleid
     * @return self
     */
    public function setRoleid($roleid)
    {
        $this->roleid = $roleid;
        return $this;
    }

    /**
     * Get roleid
     *
     * @return string $roleid
     */
    public function getRoleid()
    {
        return $this->roleid;
    }

    /**
     * Set mediaid
     *
     * @param string $mediaid
     * @return self
     */
    public function setMediaid($mediaid)
    {
        $this->mediaid = $mediaid;
        return $this;
    }

    /**
     * Get mediaid
     *
     * @return string $mediaid
     */
    public function getMediaid()
    {
        return $this->mediaid;
    }

    /**
     * Set isdeleted
     *
     * @param integer $isdeleted
     * @return self
     */
    public function setIsdeleted($isdeleted)
    {
        $this->isdeleted = $isdeleted;
        return $this;
    }

    /**
     * Get isdeleted
     *
     * @return integer $isdeleted
     */
    public function getIsdeleted()
    {
        return $this->isdeleted;
    }

    /**
     * Set createddate
     *
     * @param string $createddate
     * @return self
     */
    public function setCreateddate($createddate)
    {
        $this->createddate = $createddate;
        return $this;
    }

    /**
     * Get createddate
     *
     * @return string $createddate
     */
    public function getCreateddate()
    {
        return $this->createddate;
    }

    /**
     * Set logindate
     *
     * @param string $logindate
     * @return self
     */
    public function setLogindate($logindate)
    {
        $this->logindate = $logindate;
        return $this;
    }

    /**
     * Get logindate
     *
     * @return string $logindate
     */
    public function getLogindate()
    {
        return $this->logindate;
    }
}
