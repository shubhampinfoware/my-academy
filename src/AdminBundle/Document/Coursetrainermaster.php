<?php
    namespace AdminBundle\Document;

    use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

    /**
     * @MongoDB\Document
     */
    class Coursetrainermaster
    {
        /**
         * @MongoDB\Id
         */
        protected $id;

        /**
         * @MongoDB\Field(type="string")
         */
        protected $usermaster_id;

        /**
         * @MongoDB\Field(type="string")
         */
        protected $coursemaster_id;
          /**
         * @MongoDB\Field(type="string")
         */
        protected $is_accepted;

        /**
         * @MongoDB\Field(type="string")
         */
        protected $domain_id;

        /**
         * @MongoDB\Field(type="string")
         */
        protected $is_deleted;
  
  	
    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set usermasterId
     *
     * @param string $usermasterId
     * @return self
     */
    public function setUsermasterId($usermasterId)
    {
        $this->usermaster_id = $usermasterId;
        return $this;
    }

    /**
     * Get usermasterId
     *
     * @return string $usermasterId
     */
    public function getUsermasterId()
    {
        return $this->usermaster_id;
    }

    /**
     * Set coursemasterId
     *
     * @param string $coursemasterId
     * @return self
     */
    public function setCoursemaster_id($coursemasterId)
    {
        $this->coursemaster_id = $coursemasterId;
        return $this;
    }

    /**
     * Get coursemasterId
     *
     * @return string $coursemasterId
     */
    public function getCoursemasterId()
    {
        return $this->coursemaster_id;
    }

    /**
     * Set isAccepted
     *
     * @param string $isAccepted
     * @return self
     */
    public function setIs_accepted($isAccepted)
    {
        $this->is_accepted = $isAccepted;
        return $this;
    }

    /**
     * Get isAccepted
     *
     * @return string $isAccepted
     */
    public function getIsAccepted()
    {
        return $this->is_accepted;
    }

    /**
     * Set domainId
     *
     * @param string $domainId
     * @return self
     */
    public function setDomainId($domainId)
    {
        $this->domain_id = $domainId;
        return $this;
    }

    /**
     * Get domainId
     *
     * @return string $domainId
     */
    public function getDomainId()
    {
        return $this->domain_id;
    }

    /**
     * Set isDeleted
     *
     * @param string $isDeleted
     * @return self
     */
    public function setIsDeleted($isDeleted)
    {
        $this->is_deleted = $isDeleted;
        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return string $isDeleted
     */
    public function getIsDeleted()
    {
        return $this->is_deleted;
    }
}
