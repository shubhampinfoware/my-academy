<?php

namespace AdminBundle\Document;

/**
 * AdminBundle\Document\Couponmaster
 */
class Couponmaster
{
    /**
     * @var MongoId $id
     */
    protected $id;

    /**
     * @var string $coupon_name
     */
    protected $coupon_name;

    /**
     * @var string $coupon_code
     */
    protected $coupon_code;

    /**
     * @var string $start_date
     */
    protected $start_date;

    /**
     * @var string $end_date
     */
    protected $end_date;

    /**
     * @var string $discount_value
     */
    protected $discount_value;
    

    /**
     * @var string $discount_type
    */
    protected $discount_type;
    
    /**
     * @var string $return_type
    */
    protected $return_type;

    /**
     * @var string $no_of_user_use
     */
    protected $no_of_user_use;

    /**
     * @var string $no_of_times_use
     */
    protected $no_of_times_use;

    /**
     * @var string $coupon_usage_interval
     */
    protected $coupon_usage_interval;
    /**
     *  /**
     * @var string $min_order_amount
     */
    protected $min_order_amount;

    /**
     * @var string $coupon_status
     */
    protected $coupon_status;
    /**
     * **
     * @var string $visible_all
     */
    protected $visible_all;
    /**
     * **
     * @var string $created_datetime
     */
    protected $created_datetime;
    /****
     * @var string $domain_id
     */
    protected $domain_id;
    /****
     * @var string $is_deleted
     */
    protected $is_deleted;
    /****
     * @var string $coupon_master_field_1
     */
    protected $coupon_master_field_1;
    /****
     * @var string $coupon_master_field_2
     */
    protected $coupon_master_field_2;
    /****
     * @var string $coupon_master_field_2
     */
    /****
     * @var string $coupon_master_field_3
     */
    protected $coupon_master_field_3;    
    
    /**
     * 
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set coupon_name
     *
     * @param string $coupon_name
     * @return self
     */
    public function setCoupon_name($coupon_name)
    {
        $this->coupon_name = $coupon_name;
        return $this;
    }

    /**
     * Get coupon_name
     *
     * @return string $coupon_name
     */
    public function getCoupon_name()
    {
        return $this->coupon_name;
    }


    /**
     * Set coupon_code
     *
     * @param string $coupon_code
     * @return self
     */
    public function setCoupon_code($coupon_code)
    {
        $this->coupon_code = $coupon_code;
        return $this;
    }

    /**
     * Get coupon_code
     *
     * @return string $coupon_code
     */
    public function getCoupon_code()
    {
        return $this->coupon_code;
    }

    /**
     * Set start_date
     *
     * @param string $start_date
     * @return self
     */
    public function setStart_date($start_date)
    {
        $this->start_date = $start_date;
        return $this;
    }

    /**
     * Get start_date
     *
     * @return string $start_date
     */
    public function getStart_date()
    {
        return $this->start_date;
    }

    /**
     * Set end_date
     *
     * @param string $end_date
     * @return self
     */
    public function setEnd_date($end_date)
    {
        $this->end_date = $end_date;
        return $this;
    }

    /**
     * Get end_date
     *
     * @return string $end_date
     */
    public function getEnd_date()
    {
        return $this->end_date;
    }

    /**
     * Set discount_value
     *
     * @param string $discount_value
     * @return self
     */
    public function setDiscount_value($discount_value)
    {
        $this->discount_value = $discount_value;
        return $this;
    }

    /**
     * Get discount_value
     *
     * @return string $discount_value
     */
    public function getDiscount_value()
    {
        return $this->discount_value;
    }

    /**
     * Set discount_type
     *
     * @param string $discount_type
     * @return self
     */
    public function setDiscount_type($discount_type)
    {
        $this->discount_type = $discount_type;
        return $this;
    }

    /**
     * Get discount_type
     *
     * @return string $discount_type
     */
    public function getDiscount_type()
    {
        return $this->discount_type;
    }

    /**
     * Set return_type
     *
     * @param string $return_type
     * @return self
     */
    public function setReturn_type($return_type)
    {
        $this->return_type = $return_type;
        return $this;
    }

    /**
     * Get return_type
     *
     * @return string $return_type
     */
    public function getReturn_type()
    {
        return $this->return_type;
    }

    /**
     * Set no_of_user_use
     *
     * @param string $no_of_user_use
     * @return self
     */
    public function setNo_of_user_use($no_of_user_use)
    {
        $this->no_of_user_use = $no_of_user_use;
        return $this;
    }

    /**
     * Get no_of_user_use
     *
     * @return string $no_of_user_use
     */
    public function getNo_of_user_use()
    {
        return $this->no_of_user_use;
    }

    /**
     * Set no_of_times_use
     *
     * @param string $no_of_times_use
     * @return self
     */
    public function setNo_of_times_use($no_of_times_use)
    {
        $this->no_of_times_use = $no_of_times_use;
        return $this;
    }

    /**
     * Get no_of_times_use
     *
     * @return string $no_of_times_use
     */
    public function getNo_of_times_use()
    {
        return $this->no_of_times_use;
    }

    /**
     * Set coupon_usage_interval
     *
     * @param string $coupon_usage_interval
     * @return self
     */
    public function setCoupon_usage_interval($coupon_usage_interval)
    {
        $this->coupon_usage_interval = $coupon_usage_interval;
        return $this;
    }

    /**
     * Get coupon_usage_interval
     *
     * @return string $coupon_usage_interval
     */
    public function getCoupon_usage_interval()
    {
        return $this->coupon_usage_interval;
    }


     /**
     * Set min_order_amount
     *
     * @param string $min_order_amount
     * @return self
     */
    public function setMin_order_amount($min_order_amount)
    {
        $this->min_order_amount = $min_order_amount;
        return $this;
    }
    /**
     * Get min_order_amount
     *
     * @return string $min_order_amount
     */
    public function getMin_order_amount()
    {
        return $this->min_order_amount;
    }


     /**
     * Set coupon_status
     *
     * @param string $coupon_status
     * @return self
     */
    public function setCoupon_status($coupon_status)
    {
        $this->coupon_status = $coupon_status;
        return $this;
    }

    /**
     * Get coupon_status
     *
     * @return string $coupon_status
     */
    public function getCoupon_status()
    {
        return $this->coupon_status;
    }


     /**
     * Set visible_all
     *
     * @param string $visible_all
     * @return self
     */
    public function setVisible_all($visible_all)
    {
        $this->visible_all = $visible_all;
        return $this;
    }
    /**
     * Get visible_all
     *
     * @return string $visible_all
     */
    public function getVisible_all()
    {
        return $this->visible_all;
    }
    /**
     * Set created_datetime
     *
     * @param string $created_datetime
     * @return self
     */
    public function setCreated_datetime($created_datetime)
    {
        $this->created_datetime = $created_datetime;
        return $this;
    }
    /**
     * Get created_datetime
     *
     * @return string $created_datetime
     */
    public function getCreated_datetimes()
    {
        return $this->created_datetime;
    }
    /**
     * Set domain_id
     *
     * @param string $domain_id
     * @return self
     */
    public function setDomain_id($domain_id)
    {
        $this->domain_id = $domain_id;
        return $this;
    }
    /**
     * Get domain_id
     *
     * @return string $domain_id
     */
    public function getDomain_id()
    {
        return $this->domain_id;
    }
    /**
     * Set is_deleted
     *
     * @param string $is_deleted
     * @return self
     */
    public function setIs_deleted($is_deleted)
    {
        $this->is_deleted = $is_deleted;
        return $this;
    }
    /**
     * Get is_deleted
     *
     * @return string $is_deleted
     */
    public function getIs_deleted()
    {
        return $this->is_deleted;
    }
     /**
     * Set coupon_master_field_1
     *
     * @param string $coupon_master_field_1
     * @return self
     */
    public function setCoupon_master_field_1($coupon_master_field_1)
    {
        $this->coupon_master_field_1 = $coupon_master_field_1;
        return $this;
    }
    /**
     * Get coupon_master_field_1
     *
     * @return string $coupon_master_field_1
     */
    public function getCoupon_master_field_1()
    {
        return $this->coupon_master_field_1;
    }
     /**
     * Set coupon_master_field_2
     *
     * @param string $coupon_master_field_2
     * @return self
     */
    public function setCoupon_master_field_2($coupon_master_field_2)
    {
        $this->coupon_master_field_2 = $coupon_master_field_2;
        return $this;
    }
    /**
     * Get coupon_master_field_2
     *
     * @return string $coupon_master_field_2
     */
    public function getCoupon_master_field_2()
    {
        return $this->coupon_master_field_2;
    }/**
     * Set coupon_master_field_3
     *
     * @param string $coupon_master_field_3
     * @return self
     */
    public function setCoupon_master_field_3($coupon_master_field_3)
    {
        $this->coupon_master_field_3 = $coupon_master_field_3;
        return $this;
    }
    /**
     * Get coupon_master_field_3
     *
     * @return string $coupon_master_field_3
     */
    public function getCoupon_master_field_3()
    {
        return $this->coupon_master_field_3;
    }
    /**
     * Set couponName
     *
     * @param string $couponName
     * @return self
     */
    public function setCouponName($couponName)
    {
        $this->coupon_name = $couponName;
        return $this;
    }

    /**
     * Get couponName
     *
     * @return string $couponName
     */
    public function getCouponName()
    {
        return $this->coupon_name;
    }

    /**
     * Set couponCode
     *
     * @param string $couponCode
     * @return self
     */
    public function setCouponCode($couponCode)
    {
        $this->coupon_code = $couponCode;
        return $this;
    }

    /**
     * Get couponCode
     *
     * @return string $couponCode
     */
    public function getCouponCode()
    {
        return $this->coupon_code;
    }

    /**
     * Set startDate
     *
     * @param string $startDate
     * @return self
     */
    public function setStartDate($startDate)
    {
        $this->start_date = $startDate;
        return $this;
    }

    /**
     * Get startDate
     *
     * @return string $startDate
     */
    public function getStartDate()
    {
        return $this->start_date;
    }

    /**
     * Set endDate
     *
     * @param string $endDate
     * @return self
     */
    public function setEndDate($endDate)
    {
        $this->end_date = $endDate;
        return $this;
    }

    /**
     * Get endDate
     *
     * @return string $endDate
     */
    public function getEndDate()
    {
        return $this->end_date;
    }

    /**
     * Set discountValue
     *
     * @param string $discountValue
     * @return self
     */
    public function setDiscountValue($discountValue)
    {
        $this->discount_value = $discountValue;
        return $this;
    }

    /**
     * Get discountValue
     *
     * @return string $discountValue
     */
    public function getDiscountValue()
    {
        return $this->discount_value;
    }

    /**
     * Set discountType
     *
     * @param string $discountType
     * @return self
     */
    public function setDiscountType($discountType)
    {
        $this->discount_type = $discountType;
        return $this;
    }

    /**
     * Get discountType
     *
     * @return string $discountType
     */
    public function getDiscountType()
    {
        return $this->discount_type;
    }

    /**
     * Set noOfUserUse
     *
     * @param string $noOfUserUse
     * @return self
     */
    public function setNoOfUserUse($noOfUserUse)
    {
        $this->no_of_user_use = $noOfUserUse;
        return $this;
    }

    /**
     * Get noOfUserUse
     *
     * @return string $noOfUserUse
     */
    public function getNoOfUserUse()
    {
        return $this->no_of_user_use;
    }

    /**
     * Set noOfTimesUse
     *
     * @param string $noOfTimesUse
     * @return self
     */
    public function setNoOfTimesUse($noOfTimesUse)
    {
        $this->no_of_times_use = $noOfTimesUse;
        return $this;
    }

    /**
     * Get noOfTimesUse
     *
     * @return string $noOfTimesUse
     */
    public function getNoOfTimesUse()
    {
        return $this->no_of_times_use;
    }

    /**
     * Set couponUsageInterval
     *
     * @param string $couponUsageInterval
     * @return self
     */
    public function setCouponUsageInterval($couponUsageInterval)
    {
        $this->coupon_usage_interval = $couponUsageInterval;
        return $this;
    }

    /**
     * Get couponUsageInterval
     *
     * @return string $couponUsageInterval
     */
    public function getCouponUsageInterval()
    {
        return $this->coupon_usage_interval;
    }

    /**
     * Set minOrderAmount
     *
     * @param string $minOrderAmount
     * @return self
     */
    public function setMinOrderAmount($minOrderAmount)
    {
        $this->min_order_amount = $minOrderAmount;
        return $this;
    }

    /**
     * Get minOrderAmount
     *
     * @return string $minOrderAmount
     */
    public function getMinOrderAmount()
    {
        return $this->min_order_amount;
    }

    /**
     * Set couponStatus
     *
     * @param string $couponStatus
     * @return self
     */
    public function setCouponStatus($couponStatus)
    {
        $this->coupon_status = $couponStatus;
        return $this;
    }

    /**
     * Get couponStatus
     *
     * @return string $couponStatus
     */
    public function getCouponStatus()
    {
        return $this->coupon_status;
    }

    /**
     * Set visibleAll
     *
     * @param string $visibleAll
     * @return self
     */
    public function setVisibleAll($visibleAll)
    {
        $this->visible_all = $visibleAll;
        return $this;
    }

    /**
     * Get visibleAll
     *
     * @return string $visibleAll
     */
    public function getVisibleAll()
    {
        return $this->visible_all;
    }

    /**
     * Set createdDatetime
     *
     * @param string $createdDatetime
     * @return self
     */
    public function setCreated_datetime($createdDatetime)
    {
        $this->created_datetime = $createdDatetime;
        return $this;
    }

    /**
     * Get createdDatetime
     *
     * @return string $createdDatetime
     */
    public function getCreatedDatetime()
    {
        return $this->created_datetime;
    }

    /**
     * Set domainId
     *
     * @param string $domainId
     * @return self
     */
    public function setDomainId($domainId)
    {
        $this->domain_id = $domainId;
        return $this;
    }

    /**
     * Get domainId
     *
     * @return string $domainId
     */
    public function getDomainId()
    {
        return $this->domain_id;
    }

    /**
     * Set isDeleted
     *
     * @param string $isDeleted
     * @return self
     */
    public function setIsDeleted($isDeleted)
    {
        $this->is_deleted = $isDeleted;
        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return string $isDeleted
     */
    public function getIsDeleted()
    {
        return $this->is_deleted;
    }

    /**
     * Set couponMasterField1
     *
     * @param string $couponMasterField1
     * @return self
     */
    public function setCouponMasterField1($couponMasterField1)
    {
        $this->coupon_master_field_1 = $couponMasterField1;
        return $this;
    }

    /**
     * Get couponMasterField1
     *
     * @return string $couponMasterField1
     */
    public function getCouponMasterField1()
    {
        return $this->coupon_master_field_1;
    }

    /**
     * Set couponMasterField2
     *
     * @param string $couponMasterField2
     * @return self
     */
    public function setCouponMasterField2($couponMasterField2)
    {
        $this->coupon_master_field_2 = $couponMasterField2;
        return $this;
    }

    /**
     * Get couponMasterField2
     *
     * @return string $couponMasterField2
     */
    public function getCouponMasterField2()
    {
        return $this->coupon_master_field_2;
    }

    /**
     * Set couponMasterField3
     *
     * @param string $couponMasterField3
     * @return self
     */
    public function setCouponMasterField3($couponMasterField3)
    {
        $this->coupon_master_field_3 = $couponMasterField3;
        return $this;
    }

    /**
     * Get couponMasterField3
     *
     * @return string $couponMasterField3
     */
    public function getCouponMasterField3()
    {
        return $this->coupon_master_field_3;
    }

    /**
     * Set returnType
     *
     * @param string $returnType
     * @return self
     */
    public function setReturnType($returnType)
    {
        $this->return_type = $returnType;
        return $this;
    }

    /**
     * Get returnType
     *
     * @return string $returnType
     */
    public function getReturnType()
    {
        return $this->return_type;
    }
}
