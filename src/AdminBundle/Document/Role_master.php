<?php
    namespace AdminBundle\Document;

    use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

    /**
     * @MongoDB\Document
     */
    class Role_master
    {
        /**
         * @MongoDB\Id
         */
        protected $id;

        /**
         * @MongoDB\Field(type="integer")
         */
        protected $roleid;

        /**
         * @MongoDB\Field(type="string")
         */
        protected $rolename;

        /**
         * @MongoDB\Field(type="string")
         */
        protected $role_description;

         /**
         * @MongoDB\Field(type="string")
         */
        protected $role_status;

         /**
         * @MongoDB\Field(type="string")
         */
        protected $is_deleted;

       
        
    
    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set rolename
     *
     * @param string $rolename
     * @return self
     */
    public function setRolename($rolename)
    {
        $this->rolename = $rolename;
        return $this;
    }

    /**
     * Get rolename
     *
     * @return string $rolename
     */
    public function getRolename()
    {
        return $this->rolename;
    }

    /**
     * Set roleDescription
     *
     * @param string $roleDescription
     * @return self
     */
    public function setRoleDescription($roleDescription)
    {
        $this->role_description = $roleDescription;
        return $this;
    }

    /**
     * Get roleDescription
     *
     * @return string $roleDescription
     */
    public function getRoleDescription()
    {
        return $this->role_description;
    }

    /**
     * Set roleStatus
     *
     * @param string $roleStatus
     * @return self
     */
    public function setRoleStatus($roleStatus)
    {
        $this->role_status = $roleStatus;
        return $this;
    }

    /**
     * Get roleStatus
     *
     * @return string $roleStatus
     */
    public function getRoleStatus()
    {
        return $this->role_status;
    }

    /**
     * Set isDeleted
     *
     * @param string $isDeleted
     * @return self
     */
    public function setIsDeleted($isDeleted)
    {
        $this->is_deleted = $isDeleted;
        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return string $isDeleted
     */
    public function getIsDeleted()
    {
        return $this->is_deleted;
    }

    /**
     * Set roleid
     *
     * @param integer $roleid
     * @return self
     */
    public function setRoleid($roleid)
    {
        $this->roleid = $roleid;
        return $this;
    }

    /**
     * Get roleid
     *
     * @return integer $roleid
     */
    public function getRoleid()
    {
        return $this->roleid;
    }
}
