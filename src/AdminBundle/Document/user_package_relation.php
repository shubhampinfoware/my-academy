<?php
    namespace AdminBundle\Document;

    use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

    /**
     * @MongoDB\Document
     */
    class user_package_relation

    {
        /**
         * @MongoDB\Id
         */
        protected $id;
        /**
         * @MongoDB\Field(type="string")
         */
        protected $user_id;
        /**
         * @MongoDB\Field(type="string")
         */
        protected $package_id;
        /**
         * @MongoDB\Field(type="string")
         */
        protected $created_datetime;
        /**
         * @MongoDB\Field(type="string")
         */
        protected $is_archieved;
        /**
         * @MongoDB\Field(type="string")
         */
        protected $payment_status;
        
        /**
         * @MongoDB\Field(type="string")
         */
        protected $is_deleted;
        
    
    
    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userId
     *
     * @param string $userId
     * @return self
     */
    public function setUserId($userId)
    {
        $this->user_id = $userId;
        return $this;
    }

    /**
     * Get userId
     *
     * @return string $userId
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Set packageId
     *
     * @param string $packageId
     * @return self
     */
    public function setPackageId($packageId)
    {
        $this->package_id = $packageId;
        return $this;
    }

    /**
     * Get packageId
     *
     * @return string $packageId
     */
    public function getPackageId()
    {
        return $this->package_id;
    }

    /**
     * Set createdDatetime
     *
     * @param string $createdDatetime
     * @return self
     */
    public function setCreated_datetime($createdDatetime)
    {
        $this->created_datetime = $createdDatetime;
        return $this;
    }

    /**
     * Get createdDatetime
     *
     * @return string $createdDatetime
     */
    public function getCreatedDatetime()
    {
        return $this->created_datetime;
    }

    /**
     * Set isArchieved
     *
     * @param string $isArchieved
     * @return self
     */
    public function setIsArchieved($isArchieved)
    {
        $this->is_archieved = $isArchieved;
        return $this;
    }

    /**
     * Get isArchieved
     *
     * @return string $isArchieved
     */
    public function getIsArchieved()
    {
        return $this->is_archieved;
    }
    /**
     * Set payment_status
     *
     * @param string $payment_status
     * @return self
     */
    public function setPayment_status($payment_status)
    {
        $this->payment_status = $payment_status;
        return $this;
    }

    /**
     * Get payment_status
     *
     * @return string $payment_status
     */
    public function getPayment_status()
    {
        return $this->payment_status;
    }

    /**
     * Set isDeleted
     *
     * @param string $isDeleted
     * @return self
     */
    public function setIsDeleted($isDeleted)
    {
        $this->is_deleted = $isDeleted;
        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return string $isDeleted
     */
    public function getIsDeleted()
    {
        return $this->is_deleted;
    }
}
