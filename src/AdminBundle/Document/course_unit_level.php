<?php
    namespace AdminBundle\Document;

    use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

    /**
     * @MongoDB\Document
     */
    class course_unit_level
    {
        /**
         * @MongoDB\Id
         */
        protected $id;
        /**
         * @MongoDB\Field(type="string")
         */
        protected $course_unit_id;
        /**
         * @MongoDB\Field(type="string")
         */
        protected $class_level_id; 
        
        /**
         * @MongoDB\Field(type="string")
         */
        protected $is_deleted;
        
        
    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set Course_unit_id
     *
     * @param string $course_unit_id
     * @return self
     */
    public function setCourse_unit_id($course_unit_id)
    {
        $this->course_unit_id = $course_unit_id;
        return $this;
    }

    /**
     * Get Course_unit_id
     *
     * @return string $course_unit_id
     */
    public function getCourse_unit_id()
    {
        return $this->course_unit_id;
    }

    /**
     * Set class_level_id
     *
     * @param string $class_level_id
     * @return self
     */
    public function setClass_level_id($class_level_id)
    {
        $this->class_level_id = $class_level_id;
        return $this;
    }

    /**
     * Get class_level_id
     *
     * @return string $class_level_id
     */
    public function getClass_level_id()
    {
        return $this->class_level_id;
    }

    /**
   
    /**
     * Set isDeleted
     *
     * @param string $isDeleted
     * @return self
     */
    public function setIsDeleted($isDeleted)
    {
        $this->is_deleted = $isDeleted;
        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return string $isDeleted
     */
    public function getIsDeleted()
    {
        return $this->is_deleted;
    }
    
  
}
