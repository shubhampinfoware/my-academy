<?php
    namespace AdminBundle\Document;

    use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

    /**
     * @MongoDB\Document
     */
    class Class_level
    {
        /**
         * @MongoDB\Id
         */
        protected $id;

        /**
         * @MongoDB\Field(type="string")
         */
        protected $level_name;

         /**
         * @MongoDB\Field(type="string")
         */
        protected $level_desc;

          /**
         * @MongoDB\Field(type="string")
         */
        protected $level_start_range;

           /**
         * @MongoDB\Field(type="string")
         */
        protected $level_end_range;

         /**
         * @MongoDB\Field(type="string")
         */
        protected $level_status;

         /**
         * @MongoDB\Field(type="string")
         */
        protected $is_deleted;
         /**
         * @MongoDB\Field(type="string")
         */
        protected $field_1;

        /**
         * @MongoDB\Field(type="string")
         */
        protected $field_2;



    
    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return self
     */
    public function setLevel_name($level_name)
    {
        $this->level_name = $level_name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string $name
     */
    public function getLevel_name()
    {
        return $this->level_name;
    }

    /**
     * Set description
     *
     * @param string $level_desc
     * @return self
     */
    public function setLevel_desc($level_desc)
    {
        $this->level_desc = $level_desc;
        return $this;
    }

    /**
     * Get description
     *
     * @return string $level_desc
     */
    public function getLevel_desc()
    {
        return $this->level_desc;
    }

    /**
     * Set mediaId
     *
     * @param string $level_start_range
     * @return self
     */
    public function setLevel_start_range($level_start_range)
    {
        $this->level_start_range = $level_start_range;
        return $this;
    }

    /**
     * Get mediaId
     *
     * @return string $level_start_range
     */
    public function getLevel_start_range()
    {
        return $this->level_start_range;
    }

    /**
     * Set type
     *
     * @param string $level_end_range
     * @return self
     */
    public function setLevel_end_range($level_end_range)
    {
        $this->level_end_range = $level_end_range;
        return $this;
    }

    /**
     * Get type
     *
     * @return string $level_end_range
     */
    public function getLevel_end_range()
    {
        return $this->level_end_range;
    }

    /**
     * Set url
     *
     * @param string $level_status
     * @return self
     */
    public function setLevel_status($level_status)
    {
        $this->level_status = $level_status;
        return $this;
    }

    /**
     * Get url
     *
     * @return string $level_status
     */
    public function getLevel_status()
    {
        return $this->level_status;
    }
  
    /**
     * Set isDeleted
     *
     * @param string $isDeleted
     * @return self
     */
    public function setIsDeleted($isDeleted)
    {
        $this->is_deleted = $isDeleted;
        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return string $isDeleted
     */
    public function getIsDeleted()
    {
        return $this->is_deleted;
    }
    
    
    /**
     * Set field_1
     * @param string $field_1
     * @return self
     */
    public function setfield_1($field_1)
    {
        $this->field_1 = $field_1;
        return $this;
    }

    /**
     * Get field_1
     * @return string $field_1
     */
    public function getField_1()
    {
        return $this->field_1;
    }
    /**
     * Set field_1
     *
     * @param string $field_2
     * @return self
     */
    public function setfield_2($field_2)
    {
        $this->field_2 = $field_2;
        return $this;
    }

    /**
     * Get field_2    
     * @return string field_2
     */
    public function getField_2()
    {
        return $this->field_2;
    }
}
