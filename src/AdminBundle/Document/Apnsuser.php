<?php

namespace AdminBundle\Document;



/**
 * AdminBundle\Document\Apnsuser
 */
class Apnsuser
{
    /**
     * @var MongoId $id
     */
    protected $id;

    /**
     * @var string $gcm_regid
     */
    protected $apns_regid;

    /**
     * @var string $user_id
     */
    protected $user_id;

    /**
     * @var string $user_type
     */
    protected $user_type;

    /**
     * @var string $device_id
     */
    protected $device_id;

    /**
     * @var string $app_id
     */
    protected $app_id;

    /**
     * @var string $name
     */
    protected $name;
    

    /**
     * @var string $badge
    */
    protected $badge;

    /**
     * @var string $created_date
     */
    protected $created_date;

    /**
     * @var string $is_deleted
     */
    protected $is_deleted;

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set apnsRegid
     *
     * @param string $apnsRegid
     * @return self
     */
    public function setApnsRegid($apnsRegid)
    {
        $this->apns_regid = $apnsRegid;
        return $this;
    }

    /**
     * Get apnsRegid
     *
     * @return string $apnsRegid
     */
    public function getApnsRegid()
    {
        return $this->apns_regid;
    }

    /**
     * Set userId
     *
     * @param string $userId
     * @return self
     */
    public function setUserId($userId)
    {
        $this->user_id = $userId;
        return $this;
    }

    /**
     * Get userId
     *
     * @return string $userId
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Set userType
     *
     * @param string $userType
     * @return self
     */
    public function setUserType($userType)
    {
        $this->user_type = $userType;
        return $this;
    }

    /**
     * Get userType
     *
     * @return string $userType
     */
    public function getUserType()
    {
        return $this->user_type;
    }

    /**
     * Set deviceId
     *
     * @param string $deviceId
     * @return self
     */
    public function setDeviceId($deviceId)
    {
        $this->device_id = $deviceId;
        return $this;
    }

    /**
     * Get deviceId
     *
     * @return string $deviceId
     */
    public function getDeviceId()
    {
        return $this->device_id;
    }

    /**
     * Set appId
     *
     * @param string $appId
     * @return self
     */
    public function setAppId($appId)
    {
        $this->app_id = $appId;
        return $this;
    }

    /**
     * Get appId
     *
     * @return string $appId
     */
    public function getAppId()
    {
        return $this->app_id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set badge
     *
     * @param string $badge
     * @return self
     */
    public function setBadge($badge)
    {
        $this->badge = $badge;
        return $this;
    }

    /**
     * Get badge
     *
     * @return string $badge
     */
    public function getBadge()
    {
        return $this->badge;
    }

    /**
     * Set createdDate
     *
     * @param string $createdDate
     * @return self
     */
    public function setCreatedDate($createdDate)
    {
        $this->created_date = $createdDate;
        return $this;
    }

    /**
     * Get createdDate
     *
     * @return string $createdDate
     */
    public function getCreatedDate()
    {
        return $this->created_date;
    }

    /**
     * Set isDeleted
     *
     * @param string $isDeleted
     * @return self
     */
    public function setIsDeleted($isDeleted)
    {
        $this->is_deleted = $isDeleted;
        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return string $isDeleted
     */
    public function getIsDeleted()
    {
        return $this->is_deleted;
    }
}
