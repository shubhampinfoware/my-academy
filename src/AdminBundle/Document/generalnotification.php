<?php

namespace AdminBundle\Document;



/**
 * AdminBundle\Document\generalnotification
 */
class generalnotification
{
    /**
     * @var MongoId $id
     */
    protected $id;

    /**
     * @var string $notification_type
     */
    protected $notification_type;

    /**
     * @var string $title
     */
    protected $title;

    /**
     * @var string $message
     */
    protected $message;

    /**
     * @var string $image_id
     */
    protected $image_id;

    /**
     * @var string $user_master_id
     */
    protected $user_master_id;

    /**
     * @var string $send_to
     */
    protected $send_to;

    /**
     * @var string $create_date
     */
    protected $create_date;

    /**
     * @var string $is_deleted
     */
    protected $is_deleted;

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set notificationType
     *
     * @param string $notificationType
     * @return self
     */
    public function setNotificationType($notificationType)
    {
        $this->notification_type = $notificationType;
        return $this;
    }

    /**
     * Get notificationType
     *
     * @return string $notificationType
     */
    public function getNotificationType()
    {
        return $this->notification_type;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return self
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * Get title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set message
     *
     * @param string $message
     * @return self
     */
    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }

    /**
     * Get message
     *
     * @return string $message
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set imageId
     *
     * @param string $imageId
     * @return self
     */
    public function setImageId($imageId)
    {
        $this->image_id = $imageId;
        return $this;
    }

    /**
     * Get imageId
     *
     * @return string $imageId
     */
    public function getImageId()
    {
        return $this->image_id;
    }

    /**
     * Set userMasterId
     *
     * @param string $userMasterId
     * @return self
     */
    public function setUserMasterId($userMasterId)
    {
        $this->user_master_id = $userMasterId;
        return $this;
    }

    /**
     * Get userMasterId
     *
     * @return string $userMasterId
     */
    public function getUserMasterId()
    {
        return $this->user_master_id;
    }

    /**
     * Set sendTo
     *
     * @param string $sendTo
     * @return self
     */
    public function setSendTo($sendTo)
    {
        $this->send_to = $sendTo;
        return $this;
    }

    /**
     * Get sendTo
     *
     * @return string $sendTo
     */
    public function getSendTo()
    {
        return $this->send_to;
    }

    /**
     * Set createDate
     *
     * @param string $createDate
     * @return self
     */
    public function setCreateDate($createDate)
    {
        $this->create_date = $createDate;
        return $this;
    }

    /**
     * Get createDate
     *
     * @return string $createDate
     */
    public function getCreate_date()
    {
        return $this->create_date;
    }

    /**
     * Set isDeleted
     *
     * @param string $isDeleted
     * @return self
     */
    public function setIsDeleted($isDeleted)
    {
        $this->is_deleted = $isDeleted;
        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return string $isDeleted
     */
    public function getIsDeleted()
    {
        return $this->is_deleted;
    }
}
