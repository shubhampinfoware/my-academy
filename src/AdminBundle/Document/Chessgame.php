<?php

namespace AdminBundle\Document;



/**
 * AdminBundle\Document\Chessgame
 */
class Chessgame
{
    /**
     * @var MongoId $id
     */
    protected $id;

    /**
     * @var string $player_one
     */
    protected $player_one;

    /**
     * @var string $player_two
     */
    protected $player_two;

    /**
     * @var string $start_on
     */
    protected $start_on;

    /**
     * @var string $end_on
     */
    protected $end_on;

    /**
     * @var string $image_id
     */
    protected $image_id;

    /**
     * @var string $pgn_id
     */
    protected $pgn_id;

    /**
     * @var string $game_session_id
     */
    protected $game_session_id;

    /**
     * @var string $game_type
     */
    protected $game_type;

    /**
     * @var string $is_archieved
     */
    protected $is_archieved;

    /**
     * @var string $is_deleted
     */
    protected $is_deleted;

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set playerOne
     *
     * @param string $playerOne
     * @return self
     */
    public function setPlayerOne($playerOne)
    {
        $this->player_one = $playerOne;
        return $this;
    }

    /**
     * Get playerOne
     *
     * @return string $playerOne
     */
    public function getPlayerOne()
    {
        return $this->player_one;
    }

    /**
     * Set playerTwo
     *
     * @param string $playerTwo
     * @return self
     */
    public function setPlayerTwo($playerTwo)
    {
        $this->player_two = $playerTwo;
        return $this;
    }

    /**
     * Get playerTwo
     *
     * @return string $playerTwo
     */
    public function getPlayerTwo()
    {
        return $this->player_two;
    }

    /**
     * Set startOn
     *
     * @param string $startOn
     * @return self
     */
    public function setStartOn($startOn)
    {
        $this->start_on = $startOn;
        return $this;
    }

    /**
     * Get startOn
     *
     * @return string $startOn
     */
    public function getStartOn()
    {
        return $this->start_on;
    }

    /**
     * Set endOn
     *
     * @param string $endOn
     * @return self
     */
    public function setEndOn($endOn)
    {
        $this->end_on = $endOn;
        return $this;
    }

    /**
     * Get endOn
     *
     * @return string $endOn
     */
    public function getEndOn()
    {
        return $this->end_on;
    }

    /**
     * Set pgnId
     *
     * @param string $pgnId
     * @return self
     */
    public function setPgnId($pgnId)
    {
        $this->pgn_id = $pgnId;
        return $this;
    }

    /**
     * Get pgnId
     *
     * @return string $pgnId
     */
    public function getPgnId()
    {
        return $this->pgn_id;
    }

    /**
     * Set gameSessionId
     *
     * @param string $gameSessionId
     * @return self
     */
    public function setGameSessionId($gameSessionId)
    {
        $this->game_session_id = $gameSessionId;
        return $this;
    }

    /**
     * Get gameSessionId
     *
     * @return string $gameSessionId
     */
    public function getGameSessionId()
    {
        return $this->game_session_id;
    }

    /**
     * Set gameType
     *
     * @param string $gameType
     * @return self
     */
    public function setGameType($gameType)
    {
        $this->game_type = $gameType;
        return $this;
    }

    /**
     * Get gameType
     *
     * @return string $gameType
     */
    public function getGameType()
    {
        return $this->game_type;
    }

    /**
     * Set isArchieved
     *
     * @param string $isArchieved
     * @return self
     */
    public function setIsArchieved($isArchieved)
    {
        $this->is_archieved = $isArchieved;
        return $this;
    }

    /**
     * Get isArchieved
     *
     * @return string $isArchieved
     */
    public function getIsArchieved()
    {
        return $this->is_archieved;
    }

    /**
     * Set isDeleted
     *
     * @param string $isDeleted
     * @return self
     */
    public function setIsDeleted($isDeleted)
    {
        $this->is_deleted = $isDeleted;
        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return string $isDeleted
     */
    public function getIsDeleted()
    {
        return $this->is_deleted;
    }

    /**
     * Set imageId
     *
     * @param string $imageId
     * @return self
     */
    public function setImageId($imageId)
    {
        $this->image_id = $imageId;
        return $this;
    }

    /**
     * Get imageId
     *
     * @return string $imageId
     */
    public function getImageId()
    {
        return $this->image_id;
    }
}
