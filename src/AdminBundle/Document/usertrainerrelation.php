<?php
    namespace AdminBundle\Document;

    use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

    /**
     * @MongoDB\Document
     */
    class usertrainerrelation
    {
        /**
         * @MongoDB\Id
         */
        protected $id;

        /**
         * @MongoDB\Field(type="string")
         */
        protected $user_id;

        /**
         * @MongoDB\Field(type="string")
         */
        protected $is_chessbase;
          /**
         * @MongoDB\Field(type="string")
         */
        protected $is_approved;
        /**
         * @MongoDB\Field(type="string")
         */
        protected $is_deleted;
  
  	
    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userId
     *
     * @param string $userId
     * @return self
     */
    public function setUserId($userId)
    {
        $this->user_id = $userId;
        return $this;
    }

    /**
     * Get userId
     *
     * @return string $userId
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Set isChessbase
     *
     * @param string $isChessbase
     * @return self
     */
    public function setIsChessbase($isChessbase)
    {
        $this->is_chessbase = $isChessbase;
        return $this;
    }

    /**
     * Get isChessbase
     *
     * @return string $isChessbase
     */
    public function getIsChessbase()
    {
        return $this->is_chessbase;
    }

    /**
     * Set isApproved
     *
     * @param string $isApproved
     * @return self
     */
    public function setIsApproved($isApproved)
    {
        $this->is_approved = $isApproved;
        return $this;
    }

    /**
     * Get isApproved
     *
     * @return string $isApproved
     */
    public function getIsApproved()
    {
        return $this->is_approved;
    }

    /**
     * Set isDeleted
     *
     * @param string $isDeleted
     * @return self
     */
    public function setIsDeleted($isDeleted)
    {
        $this->is_deleted = $isDeleted;
        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return string $isDeleted
     */
    public function getIsDeleted()
    {
        return $this->is_deleted;
    }
}
