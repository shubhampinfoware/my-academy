<?php
    namespace AdminBundle\Document;

    use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

    /**
     * @MongoDB\Document
     */
    class mediamaster
    {
        /**
         * @MongoDB\Id
         */
        protected $id;

        /**
         * @MongoDB\Field(type="string")
         */
        protected $media_title;

        /**
         * @MongoDB\Field(type="string")
         */
        protected $media_location;

         /**
         * @MongoDB\Field(type="string")
         */
        protected $media_name;

         /**
         * @MongoDB\Field(type="string")
         */
        protected $created_datetime;

         /**
         * @MongoDB\Field(type="string")
         */
        protected $is_deleted;

       
        
    
    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set mediaTitle
     *
     * @param string $mediaTitle
     * @return self
     */
    public function setMedia_title($mediaTitle)
    {
        $this->media_title = $mediaTitle;
        return $this;
    }

    /**
     * Get mediaTitle
     *
     * @return string $mediaTitle
     */
    public function getMediaTitle()
    {
        return $this->media_title;
    }

    /**
     * Set mediaLocation
     *
     * @param string $mediaLocation
     * @return self
     */
    public function setMedia_location($mediaLocation)
    {
        $this->media_location = $mediaLocation;
        return $this;
    }

    /**
     * Get mediaLocation
     *
     * @return string $mediaLocation
     */
    public function getMedia_location()
    {
        return $this->media_location;
    }

    /**
     * Set mediaName
     *
     * @param string $mediaName
     * @return self
     */
    public function setMedia_name($mediaName)
    {
        $this->media_name = $mediaName;
        return $this;
    }

    /**
     * Get mediaName
     *
     * @return string $mediaName
     */
    public function getMedia_name()
    {
        return $this->media_name;
    }

    /**
     * Set createdDatetime
     *
     * @param string $createdDatetime
     * @return self
     */
    public function setCreated_datetime($createdDatetime)
    {
        $this->created_datetime = $createdDatetime;
        return $this;
    }

    /**
     * Get createdDatetime
     *
     * @return string $createdDatetime
     */
    public function getCreatedDatetime()
    {
        return $this->created_datetime;
    }

    /**
     * Set isDeleted
     *
     * @param string $isDeleted
     * @return self
     */
    public function setIsDeleted($isDeleted)
    {
        $this->is_deleted = $isDeleted;
        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return string $isDeleted
     */
    public function getIsDeleted()
    {
        return $this->is_deleted;
    }
}
