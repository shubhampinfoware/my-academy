<?php
    namespace AdminBundle\Document;

    use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

    /**
     * @MongoDB\Document
     */
    class course_unit_relation
    {
        /**
         * @MongoDB\Id
         */
        protected $id;
        /**
         * @MongoDB\Field(type="string")
         */
        protected $course_unit_id;
        /**
         * @MongoDB\Field(type="string")
         */
        protected $relation_with; //category or course
        /**
         * @MongoDB\Field(type="string")
         */
        protected $relation_object_id;
        /**
         * @MongoDB\Field(type="string")
         */
        protected $is_deleted;
        /**
         * @MongoDB\Field(type="string")
         */
        protected $field_1;
        /**
         * @MongoDB\Field(type="string")
         */
        protected $field_2;
    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set Course_unit_id
     *
     * @param string $course_unit_id
     * @return self
     */
    public function setCourse_unit_id($course_unit_id)
    {
        $this->course_unit_id = $course_unit_id;
        return $this;
    }

    /**
     * Get Course_unit_id
     *
     * @return string $course_unit_id
     */
    public function getCourse_unit_id()
    {
        return $this->course_unit_id;
    }

    /**
     * Set relation_with
     *
     * @param string $relation_with
     * @return self
     */
    public function setRelation_with($relation_with)
    {
        $this->relation_with = $relation_with;
        return $this;
    }

    /**
     * Get relation_with
     *
     * @return string $relation_with
     */
    public function getRelation_with()
    {
        return $this->$relation_with;
    }

    /**
     * Set unitName
     *
     * @param string $relation_object_id
     * @return self
     */
    public function setRelation_object_id($relation_object_id)
    {
        $this->relation_object_id = $relation_object_id;
        return $this;
    }

    /**
     * Get relation_object_id
     *
     * @return string $relation_object_id
     */
    public function getRelation_object_id()
    {
        return $this->relation_object_id;
    }
    /**
     * Set isDeleted
     *
     * @param string $isDeleted
     * @return self
     */
    public function setIsDeleted($isDeleted)
    {
        $this->is_deleted = $isDeleted;
        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return string $isDeleted
     */
    public function getIsDeleted()
    {
        return $this->is_deleted;
    }
    
    /**
     * Set field_1
     *
     * @param string field_1
     * @return self
     */
    public function setField_1($field_1)
    {
        $this->field_1 = $field_1;
        return $this;
    }

    /**
     * Get field_1
     *
     * @return string $field_1
     */
    public function getField_1()
    {
        return $this->field_1;
    }
    
    /**
     * Set field_2
     *
     * @param string field_2
     * @return self
     */
    public function setField_2($field_2)
    {
        $this->field_2 = $field_2;
        return $this;
    }

    /**
     * Get field_2
     *
     * @return string $field_2
     */
    public function getField_2()
    {
        return $this->field_2;
    }
}
