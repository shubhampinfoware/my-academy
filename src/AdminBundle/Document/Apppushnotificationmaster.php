<?php

namespace AdminBundle\Document;



/**
 * AdminBundle\Document\Apppushnotificationmaster
 */
class Apppushnotificationmaster
{
    /**
     * @var MongoId $id
     */
    protected $id;

    /**
     * @var string $device_name
     */
    protected $device_name;

    /**
     * @var string $app_id
     */
    protected $app_id;

    /**
     * @var string $domain_id
     */
    protected $domain_id;
	
	/**
     * @var string $user_id
     */
    protected $user_id;
	
	/**
     * @var string $language_id
     */
    protected $language_id;
	
	/**
     * @var string $device_token
     */
    protected $device_token;
	
	/**
     * @var string $device_id
     */
    protected $device_id;
	
	/**
     * @var string $data
     */
    protected $data;
	
	/**
     * @var string $code
     */
    protected $code;
	
	/**
     * @var string $table_name
     */
    protected $table_name;
	
	/**
     * @var string $table_id
     */
    protected $table_id;
	
	/**
     * @var string $response
     */
    protected $response;
	
	/**
     * @var string $datetime
     */
    protected $datetime;
	
	/**
     * @var string $is_deleted
     */
    protected $is_deleted;

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set deviceName
     *
     * @param string $deviceName
     * @return self
     */
    public function setDeviceName($deviceName)
    {
        $this->device_name = $deviceName;
        return $this;
    }

    /**
     * Get deviceName
     *
     * @return string $deviceName
     */
    public function getDeviceName()
    {
        return $this->device_name;
    }

    /**
     * Set appId
     *
     * @param string $appId
     * @return self
     */
    public function setAppId($appId)
    {
        $this->app_id = $appId;
        return $this;
    }

    /**
     * Get appId
     *
     * @return string $appId
     */
    public function getAppId()
    {
        return $this->app_id;
    }

    /**
     * Set domainId
     *
     * @param string $domainId
     * @return self
     */
    public function setDomainId($domainId)
    {
        $this->domain_id = $domainId;
        return $this;
    }

    /**
     * Get domainId
     *
     * @return string $domainId
     */
    public function getDomainId()
    {
        return $this->domain_id;
    }

    /**
     * Set userId
     *
     * @param string $userId
     * @return self
     */
    public function setUserId($userId)
    {
        $this->user_id = $userId;
        return $this;
    }

    /**
     * Get userId
     *
     * @return string $userId
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Set languageId
     *
     * @param string $languageId
     * @return self
     */
    public function setLanguage_id($languageId)
    {
        $this->language_id = $languageId;
        return $this;
    }

    /**
     * Get languageId
     *
     * @return string $languageId
     */
    public function getLanguageId()
    {
        return $this->language_id;
    }

    /**
     * Set deviceToken
     *
     * @param string $deviceToken
     * @return self
     */
    public function setDeviceToken($deviceToken)
    {
        $this->device_token = $deviceToken;
        return $this;
    }

    /**
     * Get deviceToken
     *
     * @return string $deviceToken
     */
    public function getDeviceToken()
    {
        return $this->device_token;
    }

    /**
     * Set deviceId
     *
     * @param string $deviceId
     * @return self
     */
    public function setDeviceId($deviceId)
    {
        $this->device_id = $deviceId;
        return $this;
    }

    /**
     * Get deviceId
     *
     * @return string $deviceId
     */
    public function getDeviceId()
    {
        return $this->device_id;
    }

    /**
     * Set data
     *
     * @param string $data
     * @return self
     */
    public function setData($data)
    {
        $this->data = $data;
        return $this;
    }

    /**
     * Get data
     *
     * @return string $data
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return self
     */
    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }

    /**
     * Get code
     *
     * @return string $code
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set tableName
     *
     * @param string $tableName
     * @return self
     */
    public function setTableName($tableName)
    {
        $this->table_name = $tableName;
        return $this;
    }

    /**
     * Get tableName
     *
     * @return string $tableName
     */
    public function getTableName()
    {
        return $this->table_name;
    }

    /**
     * Set tableId
     *
     * @param string $tableId
     * @return self
     */
    public function setTableId($tableId)
    {
        $this->table_id = $tableId;
        return $this;
    }

    /**
     * Get tableId
     *
     * @return string $tableId
     */
    public function getTableId()
    {
        return $this->table_id;
    }

    /**
     * Set response
     *
     * @param string $response
     * @return self
     */
    public function setResponse($response)
    {
        $this->response = $response;
        return $this;
    }

    /**
     * Get response
     *
     * @return string $response
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * Set datetime
     *
     * @param string $datetime
     * @return self
     */
    public function setDatetime($datetime)
    {
        $this->datetime = $datetime;
        return $this;
    }

    /**
     * Get datetime
     *
     * @return string $datetime
     */
    public function getDatetime()
    {
        return $this->datetime;
    }

    /**
     * Set isDeleted
     *
     * @param string $isDeleted
     * @return self
     */
    public function setIsDeleted($isDeleted)
    {
        $this->is_deleted = $isDeleted;
        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return string $isDeleted
     */
    public function getIsDeleted()
    {
        return $this->is_deleted;
    }
}
