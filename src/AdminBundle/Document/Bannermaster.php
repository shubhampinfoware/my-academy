<?php
    namespace AdminBundle\Document;

    use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

    /**
     * @MongoDB\Document
     */
    class Bannermaster
    {
        /**
         * @MongoDB\Id
         */
        protected $id;

        /**
         * @MongoDB\Field(type="string")
         */
        protected $name;

         /**
         * @MongoDB\Field(type="string")
         */
        protected $description;

          /**
         * @MongoDB\Field(type="string")
         */
        protected $media_id;

           /**
         * @MongoDB\Field(type="string")
         */
        protected $type;

         /**
         * @MongoDB\Field(type="string")
         */
        protected $link1;

         /**
         * @MongoDB\Field(type="string")
         */
        protected $link2;
         /**
         * @MongoDB\Field(type="string")
         */
        protected $status;

        /**
         * @MongoDB\Field(type="string")
         */
        protected $is_deleted;



    
    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return self
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * Get description
     *
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set mediaId
     *
     * @param string $mediaId
     * @return self
     */
    public function setMediaId($mediaId)
    {
        $this->media_id = $mediaId;
        return $this;
    }

    /**
     * Get mediaId
     *
     * @return string $mediaId
     */
    public function getMediaId()
    {
        return $this->media_id;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return self
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * Get type
     *
     * @return string $type
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set url
     *
     * @param string $link1
     * @return self
     */
    public function setLink1($link1)
    {
        $this->link1 = $link1;
        return $this;
    }

    /**
     * Get url
     *
     * @return string $link1
     */
    public function getLink1()
    {
        return $this->link1;
    }
    /**
     * Set url
     *
     * @param string $url
     * @return self
     */
    public function setLink2($link2)
    {
        $this->link2 = $link2;
        return $this;
    }

    /**
     * Get link2
     *
     * @return string $link2
     */
    public function getLink2()
    {
        return $this->link2;
    }
    /**
     * Set status
     *
     * @param string $status
     * @return self
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * Get link2
     *
     * @return string $status
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set isDeleted
     *
     * @param string $isDeleted
     * @return self
     */
    public function setIsDeleted($isDeleted)
    {
        $this->is_deleted = $isDeleted;
        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return string $isDeleted
     */
    public function getIsDeleted()
    {
        return $this->is_deleted;
    }
}
