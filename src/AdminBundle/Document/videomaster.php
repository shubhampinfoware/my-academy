<?php
    namespace AdminBundle\Document;

    use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

    /**
     * @MongoDB\Document
     */
    class videomaster
    {
        /**
         * @MongoDB\Id
         */
        protected $id;

        /**
         * @MongoDB\Field(type="string")
         */
        protected $name;


         /**
         * @MongoDB\Field(type="string")
         */
        protected $description;
        
        /**
         * @MongoDB\Field(type="string")
         */
        protected $viewcount;
        
        /**
         * @MongoDB\Field(type="string")
         */
        protected $video_id;
        
        /**
         * @MongoDB\Field(type="string")
         */
        protected $playlist_id;


        /**
         * @MongoDB\Field(type="string")
         */
        protected $is_deleted;

    
    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set viewcount
     *
     * @param string $viewcount
     * @return self
     */
    public function setViewcount($viewcount)
    {
        $this->viewcount = $viewcount;
        return $this;
    }

    /**
     * Get viewcount
     *
     * @return string $viewcount
     */
    public function getViewcount()
    {
        return $this->viewcount;
    }

    /**
     * Set videoId
     *
     * @param string $videoId
     * @return self
     */
    public function setVideoId($videoId)
    {
        $this->video_id = $videoId;
        return $this;
    }

    /**
     * Get videoId
     *
     * @return string $videoId
     */
    public function getVideoId()
    {
        return $this->video_id;
    }

    /**
     * Set playlistId
     *
     * @param string $playlistId
     * @return self
     */
    public function setPlaylistId($playlistId)
    {
        $this->playlist_id = $playlistId;
        return $this;
    }

    /**
     * Get playlistId
     *
     * @return string $playlistId
     */
    public function getPlaylistId()
    {
        return $this->playlist_id;
    }

    /**
     * Set isDeleted
     *
     * @param string $isDeleted
     * @return self
     */
    public function setIsDeleted($isDeleted)
    {
        $this->is_deleted = $isDeleted;
        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return string $isDeleted
     */
    public function getIsDeleted()
    {
        return $this->is_deleted;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return self
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * Get description
     *
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }
}
