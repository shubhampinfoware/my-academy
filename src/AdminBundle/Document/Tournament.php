<?php

namespace AdminBundle\Document;



/**
 * AdminBundle\Document\Tournament
 */
class Tournament
{
    /**
     * @var MongoId $id
     */
    protected $id;

    /**
     * @var string $title
     */
    protected $title;

    /**
     * @var string $start
     */
    protected $start;

    /**
     * @var string $end
     */
    protected $end;

    /**
     * @var string $endstr
     */
    protected $endstr;

    /**
     * @var string $city
     */
    protected $city;

    /**
     * @var string $state
     */
    protected $state;

    /**
     * @var string $country
     */
    protected $country;

    /**
     * @var string $pdfname
     */
    protected $pdfname;

    /**
     * @var string $type
     */
    protected $type;

        /**
     * @var string $desc
     */
    protected $desc;

    /**
     * @var string $venue
     */
    protected $venue;

    /**
     * @var string $prizecurrency
     */
    protected $prizecurrency;

    /**
     * @var string $totalprizefund
     */
    protected $totalprizefund;

    /**
     * @var string $prizefirst
     */
    protected $prizefirst;

    /**
     * @var string $prizesecond
     */
    protected $prizesecond;

    /**
     * @var string $prizethird
     */
    protected $prizethird;

    /**
     * @var string $organizername
     */
    protected $organizername;

    /**
     * @var string $organizerphone
     */
    protected $organizerphone;


    /**
     * @var string $organizeremail
     */
    protected $organizeremail;

    /**
     * @var string $code
     */
    protected $code;

    /**
     * @var string $monthyear
     */
    protected $monthyear;

    /**
     * @var string $is_deleted
     */
    protected $is_deleted;

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return self
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * Get title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set start
     *
     * @param string $start
     * @return self
     */
    public function setStart($start)
    {
        $this->start = $start;
        return $this;
    }

    /**
     * Get start
     *
     * @return string $start
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Set end
     *
     * @param string $end
     * @return self
     */
    public function setEnd($end)
    {
        $this->end = $end;
        return $this;
    }

    /**
     * Get end
     *
     * @return string $end
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * Set endstr
     *
     * @param string $endstr
     * @return self
     */
    public function setEndstr($endstr)
    {
        $this->endstr = $endstr;
        return $this;
    }

    /**
     * Get endstr
     *
     * @return string $endstr
     */
    public function getEndstr()
    {
        return $this->endstr;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return self
     */
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * Get city
     *
     * @return string $city
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set state
     *
     * @param string $state
     * @return self
     */
    public function setState($state)
    {
        $this->state = $state;
        return $this;
    }

    /**
     * Get state
     *
     * @return string $state
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set country
     *
     * @param string $country
     * @return self
     */
    public function setCountry($country)
    {
        $this->country = $country;
        return $this;
    }

    /**
     * Get country
     *
     * @return string $country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set pdfname
     *
     * @param string $pdfname
     * @return self
     */
    public function setPdfname($pdfname)
    {
        $this->pdfname = $pdfname;
        return $this;
    }

    /**
     * Get pdfname
     *
     * @return string $pdfname
     */
    public function getPdfname()
    {
        return $this->pdfname;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return self
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * Get type
     *
     * @return string $type
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set desc
     *
     * @param string $desc
     * @return self
     */
    public function setDesc($desc)
    {
        $this->desc = $desc;
        return $this;
    }

    /**
     * Get desc
     *
     * @return string $desc
     */
    public function getDesc()
    {
        return $this->desc;
    }

    /**
     * Set venue
     *
     * @param string $venue
     * @return self
     */
    public function setVenue($venue)
    {
        $this->venue = $venue;
        return $this;
    }

    /**
     * Get venue
     *
     * @return string $venue
     */
    public function getVenue()
    {
        return $this->venue;
    }

    /**
     * Set prizecurrency
     *
     * @param string $prizecurrency
     * @return self
     */
    public function setPrizecurrency($prizecurrency)
    {
        $this->prizecurrency = $prizecurrency;
        return $this;
    }

    /**
     * Get prizecurrency
     *
     * @return string $prizecurrency
     */
    public function getPrizecurrency()
    {
        return $this->prizecurrency;
    }

    /**
     * Set totalprizefund
     *
     * @param string $totalprizefund
     * @return self
     */
    public function setTotalprizefund($totalprizefund)
    {
        $this->totalprizefund = $totalprizefund;
        return $this;
    }

    /**
     * Get totalprizefund
     *
     * @return string $totalprizefund
     */
    public function getTotalprizefund()
    {
        return $this->totalprizefund;
    }

    /**
     * Set prizefirst
     *
     * @param string $prizefirst
     * @return self
     */
    public function setPrizefirst($prizefirst)
    {
        $this->prizefirst = $prizefirst;
        return $this;
    }

    /**
     * Get prizefirst
     *
     * @return string $prizefirst
     */
    public function getPrizefirst()
    {
        return $this->prizefirst;
    }

    /**
     * Set prizesecond
     *
     * @param string $prizesecond
     * @return self
     */
    public function setPrizesecond($prizesecond)
    {
        $this->prizesecond = $prizesecond;
        return $this;
    }

    /**
     * Get prizesecond
     *
     * @return string $prizesecond
     */
    public function getPrizesecond()
    {
        return $this->prizesecond;
    }

    /**
     * Set prizethird
     *
     * @param string $prizethird
     * @return self
     */
    public function setPrizethird($prizethird)
    {
        $this->prizethird = $prizethird;
        return $this;
    }

    /**
     * Get prizethird
     *
     * @return string $prizethird
     */
    public function getPrizethird()
    {
        return $this->prizethird;
    }

    /**
     * Set organizername
     *
     * @param string $organizername
     * @return self
     */
    public function setOrganizername($organizername)
    {
        $this->organizername = $organizername;
        return $this;
    }

    /**
     * Get organizername
     *
     * @return string $organizername
     */
    public function getOrganizername()
    {
        return $this->organizername;
    }

    /**
     * Set organizerphone
     *
     * @param string $organizerphone
     * @return self
     */
    public function setOrganizerphone($organizerphone)
    {
        $this->organizerphone = $organizerphone;
        return $this;
    }

    /**
     * Get organizerphone
     *
     * @return string $organizerphone
     */
    public function getOrganizerphone()
    {
        return $this->organizerphone;
    }

    /**
     * Set organizeremail
     *
     * @param string $organizeremail
     * @return self
     */
    public function setOrganizeremail($organizeremail)
    {
        $this->organizeremail = $organizeremail;
        return $this;
    }

    /**
     * Get organizeremail
     *
     * @return string $organizeremail
     */
    public function getOrganizeremail()
    {
        return $this->organizeremail;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return self
     */
    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }

    /**
     * Get code
     *
     * @return string $code
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set monthyear
     *
     * @param string $monthyear
     * @return self
     */
    public function setMonthyear($monthyear)
    {
        $this->monthyear = $monthyear;
        return $this;
    }

    /**
     * Get monthyear
     *
     * @return string $monthyear
     */
    public function getMonthyear()
    {
        return $this->monthyear;
    }

    /**
     * Set isDeleted
     *
     * @param string $isDeleted
     * @return self
     */
    public function setIsDeleted($isDeleted)
    {
        $this->is_deleted = $isDeleted;
        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return string $isDeleted
     */
    public function getIsDeleted()
    {
        return $this->is_deleted;
    }
}
