<?php

namespace AdminBundle\Document;

/**
 * AdminBundle\Document\coupon_assigned_to_user
 */
class coupon_assigned_to_user
{
    /**
     * @var MongoId $id
     */
    protected $id;

    /**
     * @var string $user_id
     */
    protected $user_id;

    /**
     * @var string $coupon_id
     */
    protected $coupon_id;

    /**
     * @var string $assign_datetime
     */
    protected $assign_datetime;

    /**
     * @var string $assign_by
     */
    protected $assign_by;

    /**
     * @var string $use_before_days
     */
    protected $use_before_days;

    /**
     * @var string $is_deleted
     */
    protected $is_deleted;
    

    /**
     * @var string $usage_status
    */
    protected $usage_status;

    /**
     * @var string $coupon_assigned_to_user_field_1
     */
    protected $coupon_assigned_to_user_field_1;

    /**
     * @var string $coupon_assigned_to_user_field_2
     */
    protected $coupon_assigned_to_user_field_2;

 /**
     * @var string $coupon_assigned_to_user_field_3
     */
    protected $coupon_assigned_to_user_field_3;

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user_id
     *
     * @param string $user_id
     * @return self
     */
    public function setUser_id($user_id)
    {
        $this->user_id = $user_id;
        return $this;
    }

    /**
     * Get user_id
     *
     * @return string $user_id
     */
    public function getUser_id()
    {
        return $this->user_id;
    }

    /**
     * Set coupon_id
     *
     * @param string $coupon_id
     * @return self
     */
    public function setCoupon_id($coupon_id)
    {
        $this->coupon_id = $coupon_id;
        return $this;
    }

    /**
     * Get coupon_id
     *
     * @return string $coupon_id
     */
    public function getCoupon_id()
    {
        return $this->coupon_id;
    }

    /**
     * Set assign_datetime
     *
     * @param string $assign_datetime
     * @return self
     */
    public function setAssign_datetime($assign_datetime)
    {
        $this->assign_datetime = $assign_datetime;
        return $this;
    }
    /**
     * Get assign_datetime
     *
     * @return string $assign_datetime
     */
    public function getAssign_datetime()
    {
        return $this->assign_datetime;
    }

    /**
     * Set assign_by
     *
     * @param string $assign_by
     * @return self
     */
    public function setAssign_by($assign_by)
    {
        $this->assign_by = $assign_by;
        return $this;
    }

    /**
     * Get assign_by
     *
     * @return string $assign_by
     */
    public function getAssign_by()
    {
        return $this->assign_by;
    }

    /**
     * Set use_before_days
     *
     * @param string $use_before_days
     * @return self
     */
    public function setUse_before_days($use_before_days)
    {
        $this->use_before_days = $use_before_days;
        return $this;
    }

    /**
     * Get use_before_days
     *
     * @return string $use_before_days
     */
    public function getUse_before_days()
    {
        return $this->use_before_days;
    }

    /**
     * Set is_deleted
     *
     * @param string $is_deleted
     * @return self
     */
    public function setIs_deleted($is_deleted)
    {
        $this->is_deleted = $is_deleted;
        return $this;
    }

    /**
     * Get is_deleted
     *
     * @return string $is_deleted
     */
    public function getIs_deleted()
    {
        return $this->is_deleted;
    }

    /**
     * Set usage_status
     *
     * @param string $usage_status
     * @return self
     */
    public function setUsage_status($usage_status)
    {
        $this->usage_status = $usage_status;
        return $this;
    }

    /**
     * Get usage_status
     *
     * @return string $usage_status
     */
    public function getUsage_status()
    {
        return $this->usage_status;
    }

    /**
     * Set coupon_assigned_to_user_field_1
     *
     * @param string $coupon_assigned_to_user_field_1
     * @return self
     */
    public function setCoupon_assigned_to_user_field_1($coupon_assigned_to_user_field_1)
    {
        $this->coupon_assigned_to_user_field_1 = $coupon_assigned_to_user_field_1;
        return $this;
    }

    /**
     * Get coupon_assigned_to_user_field_1
     *
     * @return string $coupon_assigned_to_user_field_1
     */
    public function getCoupon_assigned_to_user_field_1()
    {
        return $this->coupon_assigned_to_user_field_1;
    }

    /**
     * Set coupon_assigned_to_user_field_2
     *
     * @param string $coupon_assigned_to_user_field_2
     * @return self
     */
    public function setCoupon_assigned_to_user_field_2($coupon_assigned_to_user_field_2)
    {
        $this->coupon_assigned_to_user_field_2 = $coupon_assigned_to_user_field_2;
        return $this;
    }

    /**
     * Get coupon_assigned_to_user_field_2
     *
     * @return string $coupon_assigned_to_user_field_2
     */
    public function getCoupon_assigned_to_user_field_2()
    {
        return $this->coupon_assigned_to_user_field_2;
    }
      /**
     * Set coupon_assigned_to_user_field_3
     *
     * @param string $coupon_assigned_to_user_field_3
     * @return self
     */
    public function setCoupon_assigned_to_user_field_3($coupon_assigned_to_user_field_3)
    {
        $this->coupon_assigned_to_user_field_3 = $coupon_assigned_to_user_field_3;
        return $this;
    }

    /**
     * Get coupon_assigned_to_user_field_3
     *
     * @return string $coupon_assigned_to_user_field_3
     */
    public function getCoupon_assigned_to_user_field_3()
    {
        return $this->coupon_assigned_to_user_field_3;
    }

    /**
     * Set userId
     *
     * @param string $userId
     * @return self
     */
    public function setUserId($userId)
    {
        $this->user_id = $userId;
        return $this;
    }

    /**
     * Get userId
     *
     * @return string $userId
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Set couponId
     *
     * @param string $couponId
     * @return self
     */
    public function setCouponId($couponId)
    {
        $this->coupon_id = $couponId;
        return $this;
    }

    /**
     * Get couponId
     *
     * @return string $couponId
     */
    public function getCouponId()
    {
        return $this->coupon_id;
    }

    /**
     * Set assignDatetime
     *
     * @param string $assignDatetime
     * @return self
     */
    public function setAssignDatetime($assignDatetime)
    {
        $this->assign_datetime = $assignDatetime;
        return $this;
    }

    /**
     * Get assignDatetime
     *
     * @return string $assignDatetime
     */
    public function getAssignDatetime()
    {
        return $this->assign_datetime;
    }

    /**
     * Set assignBy
     *
     * @param string $assignBy
     * @return self
     */
    public function setAssign_by($assignBy)
    {
        $this->assign_by = $assignBy;
        return $this;
    }

    /**
     * Get assignBy
     *
     * @return string $assignBy
     */
    public function getAssign_by()
    {
        return $this->assign_by;
    }

    /**
     * Set useBeforeDays
     *
     * @param string $useBeforeDays
     * @return self
     */
    public function setUseBeforeDays($useBeforeDays)
    {
        $this->use_before_days = $useBeforeDays;
        return $this;
    }

    /**
     * Get useBeforeDays
     *
     * @return string $useBeforeDays
     */
    public function getUseBeforeDays()
    {
        return $this->use_before_days;
    }

    /**
     * Set isDeleted
     *
     * @param string $isDeleted
     * @return self
     */
    public function setIsDeleted($isDeleted)
    {
        $this->is_deleted = $isDeleted;
        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return string $isDeleted
     */
    public function getIsDeleted()
    {
        return $this->is_deleted;
    }

    /**
     * Set usageStatus
     *
     * @param string $usageStatus
     * @return self
     */
    public function setUsageStatus($usageStatus)
    {
        $this->usage_status = $usageStatus;
        return $this;
    }

    /**
     * Get usageStatus
     *
     * @return string $usageStatus
     */
    public function getUsageStatus()
    {
        return $this->usage_status;
    }

    /**
     * Set couponAssignedToUserField1
     *
     * @param string $couponAssignedToUserField1
     * @return self
     */
    public function setCouponAssignedToUserField1($couponAssignedToUserField1)
    {
        $this->coupon_assigned_to_user_field_1 = $couponAssignedToUserField1;
        return $this;
    }

    /**
     * Get couponAssignedToUserField1
     *
     * @return string $couponAssignedToUserField1
     */
    public function getCouponAssignedToUserField1()
    {
        return $this->coupon_assigned_to_user_field_1;
    }

    /**
     * Set couponAssignedToUserField2
     *
     * @param string $couponAssignedToUserField2
     * @return self
     */
    public function setCouponAssignedToUserField2($couponAssignedToUserField2)
    {
        $this->coupon_assigned_to_user_field_2 = $couponAssignedToUserField2;
        return $this;
    }

    /**
     * Get couponAssignedToUserField2
     *
     * @return string $couponAssignedToUserField2
     */
    public function getCouponAssignedToUserField2()
    {
        return $this->coupon_assigned_to_user_field_2;
    }

    /**
     * Set couponAssignedToUserField3
     *
     * @param string $couponAssignedToUserField3
     * @return self
     */
    public function setCouponAssignedToUserField3($couponAssignedToUserField3)
    {
        $this->coupon_assigned_to_user_field_3 = $couponAssignedToUserField3;
        return $this;
    }

    /**
     * Get couponAssignedToUserField3
     *
     * @return string $couponAssignedToUserField3
     */
    public function getCouponAssignedToUserField3()
    {
        return $this->coupon_assigned_to_user_field_3;
    }
}
