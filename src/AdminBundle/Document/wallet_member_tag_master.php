<?php

namespace AdminBundle\Document;

/**
 * AdminBundle\Document\wallet_member_tag_master
 */
class wallet_member_tag_master
{
    /**
     * @var MongoId $id
     */
    protected $id;

    /**
     * @var string $virtual_balance_start_range
     */
    protected $virtual_balance_start_range;

    /**
     * @var string $virtual_balance_end_range
     */
    protected $virtual_balance_end_range;

    /**
     * @var string $cashback_start_range
     */
    protected $cashback_start_range;

    /**
     * @var string $cashback_end_range
     */
    protected $cashback_end_range;

    /**
     * @var string $wallet_points_start_range
     */
    protected $wallet_points_start_range;

    /**
     * @var string $wallet_points_end_range
     */
    protected $wallet_points_end_range;
    

    /**
     * @var string $wallet_member_tag_name
    */
    protected $wallet_member_tag_name;

    /**
     * @var string $wallet_member_tag_desc
     */
    protected $wallet_member_tag_desc;

    /**
     * @var string $wallet_member_tag_status
     */
    protected $wallet_member_tag_status;
    

    /**
     * @var string $is_deleted
    */
    protected $is_deleted;

    /**
     * @var string $wallet_member_tag_master_field_1
     */
    protected $wallet_member_tag_master_field_1;

    /**
     * @var string $wallet_member_tag_master_field_2
     */
    protected $wallet_member_tag_master_field_2;
        /**
     * @var string $wallet_member_tag_master_field_3
     */
    protected $wallet_member_tag_master_field_3;


    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set virtual_balance_start_range
     *
     * @param string $virtual_balance_start_range
     * @return self
     */
    public function setVirtual_balance_start_range($virtual_balance_start_range)
    {
        $this->virtual_balance_start_range = $virtual_balance_start_range;
        return $this;
    }

    /**
     * Get virtual_balance_start_range
     *
     * @return string $virtual_balance_start_range
     */
    public function getVirtual_balance_start_range()
    {
        return $this->virtual_balance_start_range;
    }

    /**
     * Set virtual_balance_end_range
     *
     * @param string $virtual_balance_end_range
     * @return self
     */
    public function setVirtual_balance_end_range($virtual_balance_end_range)
    {
        $this->virtual_balance_end_range = $virtual_balance_end_range;
        return $this;
    }

    /**
     * Get virtual_balance_end_range
     *
     * @return string $virtual_balance_end_range
     */
    public function getVirtual_balance_end_range()
    {
        return $this->virtual_balance_end_range;
    }

    /**
     * Set cashback_start_range
     *
     * @param string $cashback_start_range
     * @return self
     */
    public function setCashback_start_range($cashback_start_range)
    {
        $this->cashback_start_range = $cashback_start_range;
        return $this;
    }

    /**
     * Get cashback_start_range
     *
     * @return string $cashback_start_range
     */
    public function getCashback_start_range()
    {
        return $this->cashback_start_range;
    }

    /**
     * Set cashback_end_range
     *
     * @param string $cashback_end_range
     * @return self
     */
    public function setCashback_end_range($cashback_end_range)
    {
        $this->cashback_end_range = $cashback_end_range;
        return $this;
    }

    /**
     * Get cashback_end_range
     *
     * @return string $cashback_end_range
     */
    public function getCashback_end_range()
    {
        return $this->cashback_end_range;
    }

    /**
     * Set wallet_points_start_range
     *
     * @param string $wallet_points_start_range
     * @return self
     */
    public function setWallet_points_start_range($wallet_points_start_range)
    {
        $this->wallet_points_start_range = $wallet_points_start_range;
        return $this;
    }
/**
    * Get wallet_points_start_range
    *
    * @return string $wallet_points_start_range
    */
    public function getWallet_points_start_range()
    {
        return $this->wallet_points_start_range;
    }
       /*
     Set wallet_points_end_range
    *
    * @param string $wallet_points_end_range
    * @return self
    */
   public function setWallet_points_end_range($wallet_points_end_range)
   {
       $this->wallet_points_end_range = $wallet_points_end_range;
       return $this;
   }

   
 /**
     * Get wallet_points_end_range
     *
     * @return string $wallet_points_end_range
     */
    public function getWallet_points_end_range()
    {
        return $this->wallet_points_end_range;
    }

    /**
     * Set wallet_member_tag_name
     *
     * @param string $wallet_member_tag_name
     * @return self
     */
    public function setWallet_member_tag_name($wallet_member_tag_name)
    {
        $this->wallet_member_tag_name = $wallet_member_tag_name;
        return $this;
    }

    /**
     * Get wallet_member_tag_name
     *
     * @return string $wallet_member_tag_name
     */
    public function getWallet_member_tag_name()
    {
        return $this->wallet_member_tag_name;
    }

    /**
     * Set wallet_member_tag_desc
     *
     * @param string $wallet_member_tag_desc
     * @return self
     */
    public function setWallet_member_tag_desc($wallet_member_tag_desc)
    {
        $this->wallet_member_tag_desc = $wallet_member_tag_desc;
        return $this;
    }

    /**
     * Get wallet_member_tag_desc
     *
     * @return string $wallet_member_tag_desc
     */
    public function getWallet_member_tag_desc()
    {
        return $this->wallet_member_tag_desc;
    }

    /**
     * Set wallet_member_tag_status
     *
     * @param string $wallet_member_tag_status
     * @return self
     */
    public function setWallet_member_tag_status($wallet_member_tag_status)
    {
        $this->wallet_member_tag_status = $wallet_member_tag_status;
        return $this;
    }

    /**
     * Get wallet_member_tag_status
     *
     * @return string $wallet_member_tag_status
     */
    public function getWallet_member_tag_status()
    {
        return $this->wallet_member_tag_status;
    }

    /**
     * Set is_deleted
     *
     * @param string $is_deleted
     * @return self
     */
    public function setIs_deleted($is_deleted)
    {
        $this->is_deleted = $is_deleted;
        return $this;
    }

    /**
     * Get is_deleted
     *
     * @return string $is_deleted
     */
    public function getIs_deleted()
    {
        return $this->is_deleted;
    }

     /**
     * Set wallet_member_tag_master_field_1
     *
     * @param string $wallet_member_tag_master_field_1
     * @return self
     */
    public function setWallet_member_tag_master_field_1($wallet_member_tag_master_field_1)
    {
        $this->wallet_member_tag_master_field_1 = $wallet_member_tag_master_field_1;
        return $this;
    }

    /**
     * Get wallet_member_tag_master_field_1
     *
     * @return string $wallet_member_tag_master_field_1
     */
    public function getWallet_member_tag_master_field_1()
    {
        return $this->is_deleted;
    }
    
    
     /**
    * Set wallet_member_tag_master_field_2
    *
    * @param string $wallet_member_tag_master_field_2
    * @return self
    */
   public function setWallet_member_tag_master_field_2($wallet_member_tag_master_field_2)
   {
       $this->wallet_member_tag_master_field_2 = $wallet_member_tag_master_field_2;
       return $this;
   }

   /**
    * Get wallet_member_tag_master_field_2
    *
    * @return string $wallet_member_tag_master_field_2
    */
   public function getWallet_member_tag_master_field_2()
   {
       return $this->wallet_member_tag_master_field_2;
   } 
   
   
   /**
   * Set wallet_member_tag_master_field_3
   *
   * @param string $wallet_member_tag_master_field_3
   * @return self
   */
  public function setWallet_member_tag_master_field_3($wallet_member_tag_master_field_3)
  {
      $this->wallet_member_tag_master_field_3 = $wallet_member_tag_master_field_3;
      return $this;
  }

  /**
   * Get wallet_member_tag_master_field_3
   *
   * @return string $wallet_member_tag_master_field_3
   */
  public function getWallet_member_tag_master_field_3()
  {
      return $this->wallet_member_tag_master_field_3;
  }

    /**
     * Set virtualBalanceStartRange
     *
     * @param string $virtualBalanceStartRange
     * @return self
     */
    public function setVirtualBalanceStartRange($virtualBalanceStartRange)
    {
        $this->virtual_balance_start_range = $virtualBalanceStartRange;
        return $this;
    }

    /**
     * Get virtualBalanceStartRange
     *
     * @return string $virtualBalanceStartRange
     */
    public function getVirtualBalanceStartRange()
    {
        return $this->virtual_balance_start_range;
    }

    /**
     * Set virtualBalanceEndRange
     *
     * @param string $virtualBalanceEndRange
     * @return self
     */
    public function setVirtualBalanceEndRange($virtualBalanceEndRange)
    {
        $this->virtual_balance_end_range = $virtualBalanceEndRange;
        return $this;
    }

    /**
     * Get virtualBalanceEndRange
     *
     * @return string $virtualBalanceEndRange
     */
    public function getVirtualBalanceEndRange()
    {
        return $this->virtual_balance_end_range;
    }

    /**
     * Set cashbackStartRange
     *
     * @param string $cashbackStartRange
     * @return self
     */
    public function setCashbackStartRange($cashbackStartRange)
    {
        $this->cashback_start_range = $cashbackStartRange;
        return $this;
    }

    /**
     * Get cashbackStartRange
     *
     * @return string $cashbackStartRange
     */
    public function getCashbackStartRange()
    {
        return $this->cashback_start_range;
    }

    /**
     * Set cashbackEndRange
     *
     * @param string $cashbackEndRange
     * @return self
     */
    public function setCashbackEndRange($cashbackEndRange)
    {
        $this->cashback_end_range = $cashbackEndRange;
        return $this;
    }

    /**
     * Get cashbackEndRange
     *
     * @return string $cashbackEndRange
     */
    public function getCashbackEndRange()
    {
        return $this->cashback_end_range;
    }

    /**
     * Set walletPointsStartRange
     *
     * @param string $walletPointsStartRange
     * @return self
     */
    public function setWalletPointsStartRange($walletPointsStartRange)
    {
        $this->wallet_points_start_range = $walletPointsStartRange;
        return $this;
    }

    /**
     * Get walletPointsStartRange
     *
     * @return string $walletPointsStartRange
     */
    public function getWalletPointsStartRange()
    {
        return $this->wallet_points_start_range;
    }

    /**
     * Set walletPointsEndRange
     *
     * @param string $walletPointsEndRange
     * @return self
     */
    public function setWalletPointsEndRange($walletPointsEndRange)
    {
        $this->wallet_points_end_range = $walletPointsEndRange;
        return $this;
    }

    /**
     * Get walletPointsEndRange
     *
     * @return string $walletPointsEndRange
     */
    public function getWalletPointsEndRange()
    {
        return $this->wallet_points_end_range;
    }

    /**
     * Set walletMemberTagName
     *
     * @param string $walletMemberTagName
     * @return self
     */
    public function setWalletMemberTagName($walletMemberTagName)
    {
        $this->wallet_member_tag_name = $walletMemberTagName;
        return $this;
    }

    /**
     * Get walletMemberTagName
     *
     * @return string $walletMemberTagName
     */
    public function getWalletMemberTagName()
    {
        return $this->wallet_member_tag_name;
    }

    /**
     * Set walletMemberTagDesc
     *
     * @param string $walletMemberTagDesc
     * @return self
     */
    public function setWalletMemberTagDesc($walletMemberTagDesc)
    {
        $this->wallet_member_tag_desc = $walletMemberTagDesc;
        return $this;
    }

    /**
     * Get walletMemberTagDesc
     *
     * @return string $walletMemberTagDesc
     */
    public function getWalletMemberTagDesc()
    {
        return $this->wallet_member_tag_desc;
    }

    /**
     * Set walletMemberTagStatus
     *
     * @param string $walletMemberTagStatus
     * @return self
     */
    public function setWalletMemberTagStatus($walletMemberTagStatus)
    {
        $this->wallet_member_tag_status = $walletMemberTagStatus;
        return $this;
    }

    /**
     * Get walletMemberTagStatus
     *
     * @return string $walletMemberTagStatus
     */
    public function getWalletMemberTagStatus()
    {
        return $this->wallet_member_tag_status;
    }

    /**
     * Set isDeleted
     *
     * @param string $isDeleted
     * @return self
     */
    public function setIsDeleted($isDeleted)
    {
        $this->is_deleted = $isDeleted;
        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return string $isDeleted
     */
    public function getIsDeleted()
    {
        return $this->is_deleted;
    }

    /**
     * Set walletMemberTagMasterField1
     *
     * @param string $walletMemberTagMasterField1
     * @return self
     */
    public function setWalletMemberTagMasterField1($walletMemberTagMasterField1)
    {
        $this->wallet_member_tag_master_field_1 = $walletMemberTagMasterField1;
        return $this;
    }

    /**
     * Get walletMemberTagMasterField1
     *
     * @return string $walletMemberTagMasterField1
     */
    public function getWalletMemberTagMasterField1()
    {
        return $this->wallet_member_tag_master_field_1;
    }

    /**
     * Set walletMemberTagMasterField2
     *
     * @param string $walletMemberTagMasterField2
     * @return self
     */
    public function setWalletMemberTagMasterField2($walletMemberTagMasterField2)
    {
        $this->wallet_member_tag_master_field_2 = $walletMemberTagMasterField2;
        return $this;
    }

    /**
     * Get walletMemberTagMasterField2
     *
     * @return string $walletMemberTagMasterField2
     */
    public function getWalletMemberTagMasterField2()
    {
        return $this->wallet_member_tag_master_field_2;
    }

    /**
     * Set walletMemberTagMasterField3
     *
     * @param string $walletMemberTagMasterField3
     * @return self
     */
    public function setWalletMemberTagMasterField3($walletMemberTagMasterField3)
    {
        $this->wallet_member_tag_master_field_3 = $walletMemberTagMasterField3;
        return $this;
    }

    /**
     * Get walletMemberTagMasterField3
     *
     * @return string $walletMemberTagMasterField3
     */
    public function getWalletMemberTagMasterField3()
    {
        return $this->wallet_member_tag_master_field_3;
    }
}
