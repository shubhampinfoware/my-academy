<?php
    namespace AdminBundle\Document;

    use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

    /**
     * @MongoDB\Document
     */
    class course_unit
    {
        /**
         * @MongoDB\Id         * 
         */
        protected $id;
        /**
         * @MongoDB\Field(type="string")
         */
        protected $course_code;
        /**
         * @MongoDB\Field(type="string")
         */
        protected $unit_code; //metting_id
        /** @MongoDB\Field(type="string") @Index   */
        protected $unit_name;
        /**
         * @MongoDB\Field(type="string")
         */
        protected $course_type;
        /**
         * @MongoDB\Field(type="string")
         */
        protected $youtubelink;
         /**
         * @MongoDB\Field(type="string")
         */
        protected $privateflag;
        /**
         * @MongoDB\Field(type="string")
         */
        protected $coursemaster_id;
        /**
         * @MongoDB\Field(type="string")
         */
        protected $course_media_id; // as category
           /**
         * @MongoDB\Field(type="string")
         */
        protected $course_unit_media_id;

        /**
         * @MongoDB\Field(type="string")
         */
        protected $status;
        /**
         * @MongoDB\Field(type="string")
         */
        protected $prerequisites; // as vimeo/youtube/zoom
        /**
         * @MongoDB\Field(type="string")
         */
        protected $instruction;
        /**
         * @MongoDB\Field(type="string")
         */
        protected $created_by;
        /**
         * @MongoDB\Field(type="string")
         */
        protected $class_cover_image;
        /**
         * @MongoDB\Field(type="string")
         */
        protected $domain_id;
        /**
         * @MongoDB\Field(type="string")
         */
        protected $language_id;
           /**
         * @MongoDB\Field(type="string")
         */
        protected $main_course_unit_id;

        /**
         * @MongoDB\Field(type="string")
         */
        protected $allow_shuffle; //as display at
        /**
         * @MongoDB\Field(type="string")
         */
        protected $lock_feature; //as asigneduser
        /**
         * @MongoDB\Field(type="string")
         */
        protected $create_date; 
        /**
         * @MongoDB\Field(type="string")
         */
        protected $updated_date;  
        /**
         * @MongoDB\Field(type="string")
         */
        protected $is_deleted;
        /**
         * @MongoDB\Field(type="string")
         */
        protected $tag_id;
        /**
         * @MongoDB\Field(type="string")
         */
        protected $class_level_id;

    
        /**
        * @var string $sort_order
        */
       protected $sort_order;


    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set courseCode
     *
     * @param string $courseCode
     * @return self
     */
    public function setCourseCode($courseCode)
    {
        $this->course_code = $courseCode;
        return $this;
    }

    /**
     * Get courseCode
     *
     * @return string $courseCode
     */
    public function getCourseCode()
    {
        return $this->course_code;
    }

    /**
     * Set unitCode
     *
     * @param string $unitCode
     * @return self
     */
    public function setUnitCode($unitCode)
    {
        $this->unit_code = $unitCode;
        return $this;
    }

    /**
     * Get unitCode
     *
     * @return string $unitCode
     */
    public function getUnitCode()
    {
        return $this->unit_code;
    }

    /**
     * Set unitName
     *
     * @param string $unitName
     * @return self
     */
    public function setUnit_name($unitName)
    {
        $this->unit_name = $unitName;
        return $this;
    }

    /**
     * Get unitName
     *
     * @return string $unitName
     */
    public function getUnitName()
    {
        return $this->unit_name;
    }

    /**
     * Set courseType
     *
     * @param string $courseType
     * @return self
     */
    public function setCourse_type($courseType)
    {
        $this->course_type = $courseType;
        return $this;
    }

    /**
     * Get courseType
     *
     * @return string $courseType
     */
    public function getCourseType()
    {
        return $this->course_type;
    }

    /**
     * Set youtubelink
     *
     * @param string $youtubelink
     * @return self
     */
    public function setYoutubelink($youtubelink)
    {
        $this->youtubelink = $youtubelink;
        return $this;
    }

    /**
     * Get youtubelink
     *
     * @return string $youtubelink
     */
    public function getYoutubelink()
    {
        return $this->youtubelink;
    }
    /**
     * Set privateflag
     *
     * @param string $privateflag
     * @return self
     */
    public function setPrivateflag($privateflag)
    {
        $this->privateflag = $privateflag;
        return $this;
    }

    /**
     * Get privateflag
     *
     * @return string $privateflag
     */
    public function getPrivateflag()
    {
        return $this->privateflag;
    }

    /**
     * Set coursemasterId
     *
     * @param string $coursemasterId
     * @return self
     */
    public function setCoursemaster_id($coursemasterId)
    {
        $this->coursemaster_id = $coursemasterId;
        return $this;
    }

    /**
     * Get coursemasterId
     *
     * @return string $coursemasterId
     */
    public function getCoursemasterId()
    {
        return $this->coursemaster_id;
    }

    /**
     * Set courseMediaId
     *
     * @param string $courseMediaId
     * @return self
     */
    public function setCourse_media_id($courseMediaId)
    {
        $this->course_media_id = $courseMediaId;
        return $this;
    }

    /**
     * Get courseMediaId
     *
     * @return string $courseMediaId
     */
    public function getCourseMediaId() //as category_id
    {
        return $this->course_media_id;
    }

    /**
     * Set courseUnitMediaId
     *
     * @param string $courseUnitMediaId
     * @return self
     */
    public function setCourse_unit_media_id($courseUnitMediaId)
    {
        $this->course_unit_media_id = $courseUnitMediaId;
        return $this;
    }

    /**
     * Get courseUnitMediaId
     *
     * @return string $courseUnitMediaId
     */
    public function getCourseUnitMediaId()
    {
        return $this->course_unit_media_id;
    }

    /**
     * Set sortOrder
     *
     * @param string $sortOrder
     * @return self
     */
    public function setSort_order($sortOrder)
    {
        $this->sort_order = $sortOrder;
        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return string $sortOrder
     */
    public function getSortOrder()
    {
        return $this->sort_order;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return self
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * Get status
     *
     * @return string $status
     */
    public function getStatus()
    {
        return $this->status;
    }
    /**
     * Set class_cover_image
     *
     * @param string $class_cover_image
     * @return self
     */
    public function setClass_cover_image($class_cover_image)
    {
        $this->class_cover_image = $class_cover_image;
        return $this;
    }

    /**
     * Get class_cover_image
     *
     * @return string $class_cover_image
     */
    public function getClass_cover_image()
    {
        return $this->class_cover_image;
    }

    /**
     * Set prerequisites
     *
     * @param string $prerequisites
     * @return self
     */
    public function setPrerequisites($prerequisites)
    {
        $this->prerequisites = $prerequisites;
        return $this;
    }

    /**
     * Get prerequisites
     *
     * @return string $prerequisites
     */
    public function getPrerequisites()
    {
        return $this->prerequisites;
    }

    /**
     * Set instruction
     *
     * @param string $instruction
     * @return self
     */
    public function setInstruction($instruction)
    {
        $this->instruction = $instruction;
        return $this;
    }

    /**
     * Get instruction
     *
     * @return string $instruction
     */
    public function getInstruction()
    {
        return $this->instruction;
    }

    /**
     * Set createdBy
     *
     * @param string $createdBy
     * @return self
     */
    public function setCreated_by($createdBy)
    {
        $this->created_by = $createdBy;
        return $this;
    }

    /**
     * Get createdBy
     *
     * @return string $createdBy
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * Set domainId
     *
     * @param string $domainId
     * @return self
     */
    public function setDomainId($domainId)
    {
        $this->domain_id = $domainId;
        return $this;
    }

    /**
     * Get domainId
     *
     * @return string $domainId
     */
    public function getDomainId()
    {
        return $this->domain_id;
    }

    /**
     * Set languageId
     *
     * @param string $languageId
     * @return self
     */
    public function setLanguage_id($languageId)
    {
        $this->language_id = $languageId;
        return $this;
    }

    /**
     * Get languageId
     *
     * @return string $languageId
     */
    public function getLanguageId()
    {
        return $this->language_id;
    }

    /**
     * Set mainCourseUnitId
     *
     * @param string $mainCourseUnitId
     * @return self
     */
    public function setMain_course_unit_id($mainCourseUnitId)
    {
        $this->main_course_unit_id = $mainCourseUnitId;
        return $this;
    }

    /**
     * Get mainCourseUnitId
     *
     * @return string $mainCourseUnitId
     */
    public function getMainCourseUnitId()
    {
        return $this->main_course_unit_id;
    }

    /**
     * Set allowShuffle
     *
     * @param string $allowShuffle
     * @return self
     */
    public function setAllow_shuffle($allowShuffle)
    {
        $this->allow_shuffle = $allowShuffle;
        return $this;
    }

    /**
     * Get allowShuffle
     *
     * @return string $allowShuffle
     */
    public function getAllowShuffle()
    {
        return $this->allow_shuffle;
    }

    /**
     * Set lockFeature
     *
     * @param string $lockFeature
     * @return self
     */
    public function setLock_feature($lockFeature)
    {
        $this->lock_feature = $lockFeature;
        return $this;
    }

    /**
     * Get lockFeature
     *
     * @return string $lockFeature
     */
    public function getLock_feature()
    {
        return $this->lock_feature;
    }

    /**
     * Set createDate
     *
     * @param string $createDate
     * @return self
     */
    public function setCreateDate($createDate)
    {
        $this->create_date = $createDate;
        return $this;
    }

    /**
     * Get createDate
     *
     * @return string $createDate
     */
    public function getCreate_date()
    {
        return $this->create_date;
    }

    /**
     * Set updatedDate
     *
     * @param string $updatedDate
     * @return self
     */
    public function setUpdated_date($updatedDate)
    {
        $this->updated_date = $updatedDate;
        return $this;
    }

    /**
     * Get updatedDate
     *
     * @return string $updatedDate
     */
    public function getUpdatedDate()
    {
        return $this->updated_date;
    }

    /**
     * Set isDeleted
     *
     * @param string $isDeleted
     * @return self
     */
    public function setIsDeleted($isDeleted)
    {
        $this->is_deleted = $isDeleted;
        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return string $isDeleted
     */
    public function getIsDeleted()
    {
        return $this->is_deleted;
    }

    /**
     * Set tag_id
     * @param string $tag_id
     * @return self
     */
    public function setTag_id($tag_id)
    {
        $this->tag_id = $tag_id;
        return $this;
    }

    /**
     * Get tag_id
     * @return string $class_level_id
     */
    public function getTag_id()
    {
        return $this->tag_id;
    }
    /**
     * Set class_level_id
     * @param string $class_level_id
     * @return self
     */
    public function setClass_level_id($class_level_id)
    {
        $this->class_level_id = $class_level_id;
        return $this;
    }

    /**
     * Get tag_id
     * @return string $class_level_id
     */
    public function getClass_level_id()
    {
        return $this->class_level_id;
    }
}
