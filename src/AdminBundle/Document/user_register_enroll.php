<?php
    namespace AdminBundle\Document;

    use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

    /**
     * @MongoDB\Document
     */
    class user_register_enroll
    {
        /**
         * @MongoDB\Id
         */
        protected $id;

        /**
         * @MongoDB\Field(type="string")
         */
        protected $enrollment_Code;

        /**
         * @MongoDB\Field(type="string")
         */
        protected $created_by;
          /**
         * @MongoDB\Field(type="string")
         */
        protected $created_datetime;

        /**
         * @MongoDB\Field(type="string")
         */
        protected $assign_user_id;
          /**
         * @MongoDB\Field(type="string")
         */
        protected $assign_user_datetime;

        /**
         * @MongoDB\Field(type="string")
         */
        protected $assign_by;
          /**
         * @MongoDB\Field(type="string")
         */
        protected $enrollment_status;

        /**
         * @MongoDB\Field(type="string")
         */
        protected $extra1;
          /**
         * @MongoDB\Field(type="string")
         */
        protected $extra2;

        /**
         * @MongoDB\Field(type="string")
         */
        protected $is_deleted;
  
  	
    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set enrollmentCode
     *
     * @param string $enrollmentCode
     * @return self
     */
    public function setEnrollmentCode($enrollmentCode)
    {
        $this->enrollment_Code = $enrollmentCode;
        return $this;
    }

    /**
     * Get enrollmentCode
     *
     * @return string $enrollmentCode
     */
    public function getEnrollmentCode()
    {
        return $this->enrollment_Code;
    }

    /**
     * Set createdBy
     *
     * @param string $createdBy
     * @return self
     */
    public function setCreated_by($createdBy)
    {
        $this->created_by = $createdBy;
        return $this;
    }

    /**
     * Get createdBy
     *
     * @return string $createdBy
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * Set createdDatetime
     *
     * @param string $createdDatetime
     * @return self
     */
    public function setCreated_datetime($createdDatetime)
    {
        $this->created_datetime = $createdDatetime;
        return $this;
    }

    /**
     * Get createdDatetime
     *
     * @return string $createdDatetime
     */
    public function getCreatedDatetime()
    {
        return $this->created_datetime;
    }

    /**
     * Set assignUserId
     *
     * @param string $assignUserId
     * @return self
     */
    public function setAssignUserId($assignUserId)
    {
        $this->assign_user_id = $assignUserId;
        return $this;
    }

    /**
     * Get assignUserId
     *
     * @return string $assignUserId
     */
    public function getAssignUserId()
    {
        return $this->assign_user_id;
    }

    /**
     * Set assignUserDatetime
     *
     * @param string $assignUserDatetime
     * @return self
     */
    public function setAssign_user_datetime($assignUserDatetime)
    {
        $this->assign_user_datetime = $assignUserDatetime;
        return $this;
    }

    /**
     * Get assignUserDatetime
     *
     * @return string $assignUserDatetime
     */
    public function getAssign_user_datetime()
    {
        return $this->assign_user_datetime;
    }

    /**
     * Set assignBy
     *
     * @param string $assignBy
     * @return self
     */
    public function setAssign_by($assignBy)
    {
        $this->assign_by = $assignBy;
        return $this;
    }

    /**
     * Get assignBy
     *
     * @return string $assignBy
     */
    public function getAssign_by()
    {
        return $this->assign_by;
    }

    /**
     * Set enrollmentStatus
     *
     * @param string $enrollmentStatus
     * @return self
     */
    public function ssetEnrollment_status($enrollmentStatus)
    {
        $this->enrollment_status = $enrollmentStatus;
        return $this;
    }

    /**
     * Get enrollmentStatus
     *
     * @return string $enrollmentStatus
     */
    public function gsetEnrollment_status()
    {
        return $this->enrollment_status;
    }

    /**
     * Set extra1
     *
     * @param string $extra1
     * @return self
     */
    public function setExtra1($extra1)
    {
        $this->extra1 = $extra1;
        return $this;
    }

    /**
     * Get extra1
     *
     * @return string $extra1
     */
    public function getExtra1()
    {
        return $this->extra1;
    }

    /**
     * Set extra2
     *
     * @param string $extra2
     * @return self
     */
    public function setExtra2($extra2)
    {
        $this->extra2 = $extra2;
        return $this;
    }

    /**
     * Get extra2
     *
     * @return string $extra2
     */
    public function getExtra2()
    {
        return $this->extra2;
    }

    /**
     * Set isDeleted
     *
     * @param string $isDeleted
     * @return self
     */
    public function setIsDeleted($isDeleted)
    {
        $this->is_deleted = $isDeleted;
        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return string $isDeleted
     */
    public function getIsDeleted()
    {
        return $this->is_deleted;
    }
}
