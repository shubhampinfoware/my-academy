<?php
    namespace AdminBundle\Document;

    use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

    /**
     * @MongoDB\Document
     */
    class coursetype
    {
        /**
         * @MongoDB\Id
         */
        protected $id;

        /**
         * @MongoDB\Field(type="string")
         */
        protected $type_name;

        /**
         * @MongoDB\Field(type="string")
         */
        protected $name_label;
        /**
         * @MongoDB\Field(type="string")
         */
        protected $description;
        /**
         * @MongoDB\Field(type="string")
         */
        protected $icon;
        /**
         * @MongoDB\Field(type="string")
         */
        protected $created_by;
        /**
         * @MongoDB\Field(type="string")
         */
        protected $create_date;
        /**
         * @MongoDB\Field(type="string")
         */
        protected $is_deleted;
    
	    /**
     * @var string $price
     */
    protected $price;


    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set typeName
     *
     * @param string $typeName
     * @return self
     */
    public function setTypeName($typeName)
    {
        $this->type_name = $typeName;
        return $this;
    }

    /**
     * Get typeName
     *
     * @return string $typeName
     */
    public function getTypeName()
    {
        return $this->type_name;
    }

    /**
     * Set nameLabel
     *
     * @param string $nameLabel
     * @return self
     */
    public function setNameLabel($nameLabel)
    {
        $this->name_label = $nameLabel;
        return $this;
    }

    /**
     * Get nameLabel
     *
     * @return string $nameLabel
     */
    public function getNameLabel()
    {
        return $this->name_label;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return self
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * Get description
     *
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set price
     *
     * @param string $price
     * @return self
     */
    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    /**
     * Get price
     *
     * @return string $price
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set icon
     *
     * @param string $icon
     * @return self
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;
        return $this;
    }

    /**
     * Get icon
     *
     * @return string $icon
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * Set createdBy
     *
     * @param string $createdBy
     * @return self
     */
    public function setCreated_by($createdBy)
    {
        $this->created_by = $createdBy;
        return $this;
    }

    /**
     * Get createdBy
     *
     * @return string $createdBy
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * Set createDate
     *
     * @param string $createDate
     * @return self
     */
    public function setCreateDate($createDate)
    {
        $this->create_date = $createDate;
        return $this;
    }

    /**
     * Get createDate
     *
     * @return string $createDate
     */
    public function getCreate_date()
    {
        return $this->create_date;
    }

    /**
     * Set isDeleted
     *
     * @param string $isDeleted
     * @return self
     */
    public function setIsDeleted($isDeleted)
    {
        $this->is_deleted = $isDeleted;
        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return string $isDeleted
     */
    public function getIsDeleted()
    {
        return $this->is_deleted;
    }
}
