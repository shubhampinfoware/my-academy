<?php

namespace AdminBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document
 */
class course_unit_media {

    /**
     * @MongoDB\Id         * 
     */
    protected $id;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $course_id;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $course_unit_id;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $media_id;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $media_type;
    /**
     * @MongoDB\Field(type="string")
     */
    protected $UploadFileTitle;

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set courseCode
     *
     * @param string $course_id
     * @return self
     */
    public function setCourseId($course_id) {
        $this->course_id = $course_id;
        return $this;
    }

    /**
     * Get courseCode
     *
     * @return string $course_id
     */
    public function getCourseId() {
        return $this->course_id;
    }

    /**
     * Set course_unit_id
     *
     * @param string $course_unit_id
     * @return self
     */
    public function setCourseUnitId($course_unit_id) {
        $this->course_unit_id = $course_unit_id;
        return $this;
    }

    /**
     * Get course_unit_id
     *
     * @return string $course_unit_id
     */
    public function getCourseUnitId() {
        return $this->course_unit_id;
    }

    /**
     * Set media_id
     *
     * @param string $media_id
     * @return self
     */
    public function setMediaId($media_id) {
        $this->media_id = $media_id;
        return $this;
    }

    /**
     * Get media_id
     *
     * @return string $media_id
     */
    public function getMediaId() {
        return $this->media_id;
    }

    /**
     * Set media_type
     *
     * @param string $media_type
     * @return self
     */
    public function setMediaType($media_type) {
        $this->media_type = $media_type;
        return $this;
    }

    /**
     * Get media_type
     *
     * @return string $media_type
     */
    public function getMediaType() {
        return $this->media_type;
    }
    /**
     * Set UploadFileTitle
     *
     * @param string $UploadFileTitle
     * @return self
     */
    public function setUploadFileTitle($UploadFileTitle) {
        $this->UploadFileTitle = $UploadFileTitle;
        return $this;
    }

    /**
     * Get UploadFileTitle
     *
     * @return string $UploadFileTitle
     */
    public function getUploadFileTitle() {
        return $this->UploadFileTitle;
    }

}
