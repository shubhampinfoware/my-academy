<?php

namespace AdminBundle\Document;

/**
 * AdminBundle\Document\Virtualmoney
 */

class Virtualmoney
{
  /**
     * @var MongoId $id
     */
    protected $id;

    /**
     * @MongoDB\Field(type="string")
     * @var string $wallet_code
     */
    protected $wallet_id;

    /**
     * @MongoDB\Field(type="string")
     * @var string $user_master_id
     */
    protected $user_master_id;

    /**
     * @MongoDB\Field(type="string")
     * @var string $virtual_money_balance
     */
    protected $virtual_money_balance;



    /**
     * @MongoDB\Field(type="string")
     * @var string $start_date
     */
    protected $start_date;
    

    /**
     * @MongoDB\Field(type="string")
     * @var string $end_date
    */
    protected $end_date;

    /**
     * @MongoDB\Field(type="string")
     * @var string $is_deleted
     */
    protected $is_deleted;

   
     

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set walletId
     *
     * @param string $walletId
     * @return self
     */
    public function setWalletId($walletId)
    {
        $this->wallet_id = $walletId;
        return $this;
    }

    /**
     * Get walletId
     *
     * @return string $walletId
     */
    public function getWalletId()
    {
        return $this->wallet_id;
    }

    /**
     * Set userMasterId
     *
     * @param string $userMasterId
     * @return self
     */
    public function setUserMasterId($userMasterId)
    {
        $this->user_master_id = $userMasterId;
        return $this;
    }

    /**
     * Get userMasterId
     *
     * @return string $userMasterId
     */
    public function getUserMasterId()
    {
        return $this->user_master_id;
    }

    /**
     * Set virtualMoneyBalance
     *
     * @param string $virtualMoneyBalance
     * @return self
     */
    public function setVirtualMoneyBalance($virtualMoneyBalance)
    {
        $this->virtual_money_balance = $virtualMoneyBalance;
        return $this;
    }

    /**
     * Get virtualMoneyBalance
     *
     * @return string $virtualMoneyBalance
     */
    public function getVirtualMoneyBalance()
    {
        return $this->virtual_money_balance;
    }

    /**
     * Set startDate
     *
     * @param string $startDate
     * @return self
     */
    public function setStartDate($startDate)
    {
        $this->start_date = $startDate;
        return $this;
    }

    /**
     * Get startDate
     *
     * @return string $startDate
     */
    public function getStartDate()
    {
        return $this->start_date;
    }

    /**
     * Set endDate
     *
     * @param string $endDate
     * @return self
     */
    public function setEndDate($endDate)
    {
        $this->end_date = $endDate;
        return $this;
    }

    /**
     * Get endDate
     *
     * @return string $endDate
     */
    public function getEndDate()
    {
        return $this->end_date;
    }

    /**
     * Set isDeleted
     *
     * @param string $isDeleted
     * @return self
     */
    public function setIsDeleted($isDeleted)
    {
        $this->is_deleted = $isDeleted;
        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return string $isDeleted
     */
    public function getIsDeleted()
    {
        return $this->is_deleted;
    }
}
