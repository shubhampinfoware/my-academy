<?php
    namespace AdminBundle\Document;

    use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

    /**
     * @MongoDB\Document
     */
    class subscription_master
    {
        /**
         * @MongoDB\Id
         */
        protected $id;

        /**
         * @MongoDB\Field(type="hash")
         */
        protected $course_id;

        /**
         * @MongoDB\Field(type="string")
         */
        protected $created_by;

         /**
         * @MongoDB\Field(type="string")
         */
        protected $price;

        /**
         * @MongoDB\Field(type="string")
         */
        protected $start_date;

           /**
         * @MongoDB\Field(type="string")
         */
        protected $end_date;

          /**
         * @MongoDB\Field(type="string")
         */
        protected $created_date;

         /**
         * @MongoDB\Field(type="string")
         */
        protected $status;

         /**
         * @MongoDB\Field(type="string")
         */
        protected $extra;  //subscription_name

        /**
         * @MongoDB\Field(type="string")
         */
        protected $extra1; //img_id
        /**
         * @MongoDB\Field(type="string")
         */
        protected $subscription_desc; //


           /**
         * @MongoDB\Field(type="string")
         */
        protected $is_deleted;



    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set courseId
     *
     * @param hash $courseId
     * @return self
     */
    public function setCourseId($courseId)
    {
        $this->course_id = $courseId;
        return $this;
    }

    /**
     * Get courseId
     *
     * @return hash $courseId
     */
    public function getCourseId()
    {
        return $this->course_id;
    }

    /**
     * Set createdBy
     *
     * @param string $createdBy
     * @return self
     */
    public function setCreated_by($createdBy)
    {
        $this->created_by = $createdBy;
        return $this;
    }

    /**
     * Get createdBy
     *
     * @return string $createdBy
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * Set price
     *
     * @param string $price
     * @return self
     */
    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    /**
     * Get price
     *
     * @return string $price
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set startDate
     *
     * @param string $startDate
     * @return self
     */
    public function setStartDate($startDate)
    {
        $this->start_date = $startDate;
        return $this;
    }

    /**
     * Get startDate
     *
     * @return string $startDate
     */
    public function getStartDate()
    {
        return $this->start_date;
    }

    /**
     * Set endDate
     *
     * @param string $endDate
     * @return self
     */
    public function setEndDate($endDate)
    {
        $this->end_date = $endDate;
        return $this;
    }

    /**
     * Get endDate
     *
     * @return string $endDate
     */
    public function getEndDate()
    {
        return $this->end_date;
    }

    /**
     * Set createdDate
     *
     * @param string $createdDate
     * @return self
     */
    public function setCreatedDate($createdDate)
    {
        $this->created_date = $createdDate;
        return $this;
    }

    /**
     * Get createdDate
     *
     * @return string $createdDate
     */
    public function getCreatedDate()
    {
        return $this->created_date;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return self
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * Get status
     *
     * @return string $status
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set extra
     *
     * @param string $extra
     * @return self
     */
    public function setExtra($extra)
    {
        $this->extra = $extra;
        return $this;
    }

    /**
     * Get extra
     *
     * @return string $extra
     */
    public function getExtra()
    {
        return $this->extra;
    }

    /**
     * Set extra1
     *
     * @param string $extra1
     * @return self
     */
    public function setExtra1($extra1)
    {
        $this->extra1 = $extra1;
        return $this;
    }

    /**
     * Get extra1
     *
     * @return string $extra1
     */
    public function getExtra1()
    {
        return $this->extra1;
    }
    /**
     * Set subscription_desc
     *
     * @param string $subscription_desc
     * @return self
     */
    public function setSubscription_desc($subscription_desc)
    {
        $this->subscription_desc = $subscription_desc;
        return $this;
    }

    /**
     * Get subscription_desc
     *
     * @return string $subscription_desc
     */
    public function getSubscription_desc()
    {
        return $this->subscription_desc;
    }

    /**
     * Set isDeleted
     *
     * @param string $isDeleted
     * @return self
     */
    public function setIsDeleted($isDeleted)
    {
        $this->is_deleted = $isDeleted;
        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return string $isDeleted
     */
    public function getIsDeleted()
    {
        return $this->is_deleted;
    }
}
