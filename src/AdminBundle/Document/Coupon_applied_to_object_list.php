<?php

namespace AdminBundle\Document;


/**
 * AdminBundle\Document\Coupon_applied_to_object_list
 */
class Coupon_applied_to_object_list
{
    /**
     * @var MongoId $id
     */
    protected $id;
    
    /**
     * @var string $applied_on
     */
    protected $applied_on; //(package/course)

    /**
     * @var string $coupon_id
     */
    protected $coupon_id;

    /**
     * @var hash $relation_id
     */
    protected $relation_id; //(course_id,package_ID)

    /**
     * @var string $create_date
     */
    protected $create_date;

    /**
     * @var string $is_deleted
     */
    protected $is_deleted;

    /**
     * @var string $coupon_applied_to_object_list_field_1
     */
    protected $coupon_applied_to_object_list_field_1;
    

    /**
     * @var string $coupon_applied_to_object_list_field_2
    */
    protected $coupon_applied_to_object_list_field_2;

    /**
     * @var string $coupon_applied_to_object_list_field_3
     */
    protected $coupon_applied_to_object_list_field_3;

  

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set applied_on
     *
     * @param string $applied_on
     * @return self
     */
    public function setApplied_on($applied_on)
    {
        $this->applied_on = $applied_on;
        return $this;
    }

    /**
     * Get applied_on
     *
     * @return string $applied_on
     */
    public function getApplied_on()
    {
        return $this->applied_on;
    }

    /**
     * Set coupon_id
     *
     * @param string $coupon_id
     * @return self
     */
    public function setCoupon_id($coupon_id)
    {
        $this->coupon_id = $coupon_id;
        return $this;
    }

    /**
     * Get coupon_id
     *
     * @return string $coupon_id
     */
    public function getCoupon_id()
    {
        return $this->coupon_id;
    }

    /**
     * Set relation_id
     *
     * @param string $relation_id
     * @return self
     */
    public function setRelation_id($relation_id)
    {
        $this->relation_id = $relation_id;
        return $this;
    }

    /**
     * Get relation_id
     *
     * @return string $relation_id
     */
    public function getRelation_id()
    {
        return $this->relation_id;
    }

    /**
     * Set created_date
     *
     * @param string $created_date
     * @return self
     */
    public function setCreated_date($created_date)
    {
        $this->created_date = $created_date;
        return $this;
    }

    /**
     * Get created_date
     *
     * @return string $created_date
     */
    public function getCreated_date()
    {
        return $this->created_date;
    }

    /**
     * Set is_deleted
     *
     * @param string $is_deleted
     * @return self
     */
    public function setIs_deleted($is_deleted)
    {
        $this->is_deleted = $is_deleted;
        return $this;
    }

    /**
     * Get is_deleted
     *
     * @return string $is_deleted
     */
    public function getIs_deleted()
    {
        return $this->is_deleted;
    }

    /**
     * Set coupon_applied_to_object_list_field_1
     *
     * @param string $coupon_applied_to_object_list_field_1
     * @return self
     */
    public function setCoupon_applied_to_object_list_field_1($coupon_applied_to_object_list_field_1)
    {
        $this->coupon_applied_to_object_list_field_1 = $coupon_applied_to_object_list_field_1;
        return $this;
    }

    /**
     * Get coupon_applied_to_object_list_field_1
     *
     * @return string $coupon_applied_to_object_list_field_1
     */
    public function getCoupon_applied_to_object_list_field_1()
    {
        return $this->coupon_applied_to_object_list_field_1;
    }

    /**
     * Set coupon_applied_to_object_list_field_2
     *
     * @param string $coupon_applied_to_object_list_field_2
     * @return self
     */
    public function setCoupon_applied_to_object_list_field_2($coupon_applied_to_object_list_field_2)
    {
        $this->coupon_applied_to_object_list_field_2 = $coupon_applied_to_object_list_field_2;
        return $this;
    }

    /**
     * Get coupon_applied_to_object_list_field_2
     *
     * @return string $coupon_applied_to_object_list_field_2
     */
    public function getCoupon_applied_to_object_list_field_2()
    {
        return $this->coupon_applied_to_object_list_field_2;
    }

    /**
     * Set coupon_applied_to_object_list_field_3
     *
     * @param string $coupon_applied_to_object_list_field_3
     * @return self
     */
    public function setCoupon_applied_to_object_list_field_3($coupon_applied_to_object_list_field_3)
    {
        $this->coupon_applied_to_object_list_field_3 = $coupon_applied_to_object_list_field_3;
        return $this;
    }

    /**
     * Get coupon_applied_to_object_list_field_3
     *
     * @return string $coupon_applied_to_object_list_field_3
     */
    public function getCoupon_applied_to_object_list_field_3()
    {
        return $this->coupon_applied_to_object_list_field_3;
    }

   
    /**
     * Set appliedOn
     *
     * @param string $appliedOn
     * @return self
     */
    public function setAppliedOn($appliedOn)
    {
        $this->applied_on = $appliedOn;
        return $this;
    }

    /**
     * Get appliedOn
     *
     * @return string $appliedOn
     */
    public function getAppliedOn()
    {
        return $this->applied_on;
    }

    /**
     * Set couponId
     *
     * @param string $couponId
     * @return self
     */
    public function setCouponId($couponId)
    {
        $this->coupon_id = $couponId;
        return $this;
    }

    /**
     * Get couponId
     *
     * @return string $couponId
     */
    public function getCouponId()
    {
        return $this->coupon_id;
    }

    /**
     * Set relationId
     *
     * @param hash $relationId
     * @return self
     */
    public function setRelationId($relationId)
    {
        $this->relation_id = $relationId;
        return $this;
    }

    /**
     * Get relationId
     *
     * @return hash $relationId
     */
    public function getRelationId()
    {
        return $this->relation_id;
    }

    /**
     * Set createDate
     *
     * @param string $createDate
     * @return self
     */
    public function setCreateDate($createDate)
    {
        $this->create_date = $createDate;
        return $this;
    }

    /**
     * Get createDate
     *
     * @return string $createDate
     */
    public function getCreate_date()
    {
        return $this->create_date;
    }

    /**
     * Set isDeleted
     *
     * @param string $isDeleted
     * @return self
     */
    public function setIsDeleted($isDeleted)
    {
        $this->is_deleted = $isDeleted;
        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return string $isDeleted
     */
    public function getIsDeleted()
    {
        return $this->is_deleted;
    }

    /**
     * Set couponAppliedToObjectListField1
     *
     * @param string $couponAppliedToObjectListField1
     * @return self
     */
    public function setCouponAppliedToObjectListField1($couponAppliedToObjectListField1)
    {
        $this->coupon_applied_to_object_list_field_1 = $couponAppliedToObjectListField1;
        return $this;
    }

    /**
     * Get couponAppliedToObjectListField1
     *
     * @return string $couponAppliedToObjectListField1
     */
    public function getCouponAppliedToObjectListField1()
    {
        return $this->coupon_applied_to_object_list_field_1;
    }

    /**
     * Set couponAppliedToObjectListField2
     *
     * @param string $couponAppliedToObjectListField2
     * @return self
     */
    public function setCouponAppliedToObjectListField2($couponAppliedToObjectListField2)
    {
        $this->coupon_applied_to_object_list_field_2 = $couponAppliedToObjectListField2;
        return $this;
    }

    /**
     * Get couponAppliedToObjectListField2
     *
     * @return string $couponAppliedToObjectListField2
     */
    public function getCouponAppliedToObjectListField2()
    {
        return $this->coupon_applied_to_object_list_field_2;
    }

    /**
     * Set couponAppliedToObjectListField3
     *
     * @param string $couponAppliedToObjectListField3
     * @return self
     */
    public function setCouponAppliedToObjectListField3($couponAppliedToObjectListField3)
    {
        $this->coupon_applied_to_object_list_field_3 = $couponAppliedToObjectListField3;
        return $this;
    }

    /**
     * Get couponAppliedToObjectListField3
     *
     * @return string $couponAppliedToObjectListField3
     */
    public function getCouponAppliedToObjectListField3()
    {
        return $this->coupon_applied_to_object_list_field_3;
    }
}
