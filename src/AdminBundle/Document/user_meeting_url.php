<?php
    namespace AdminBundle\Document;

    use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

    /**
     * @MongoDB\Document
     */
    class user_meeting_url
    {
        /**
         * @MongoDB\Id         * 
         */
        protected $id;
        /**
         * @MongoDB\Field(type="string")
         */
        protected $user_id;
        /**
         * @MongoDB\Field(type="string")
         */
        protected $course_unit_id; //metting_id
        /** 
         * @MongoDB\Field(type="string") @Index  
         *  */
        protected $meeting_id;
        /**
         * @MongoDB\Field(type="string")
         */
        protected $meeting_url;
        /**
         * @MongoDB\Field(type="string")
         */
        protected $registrant_id;    
        /**
         * @MongoDB\Field(type="string")
         */
        protected $is_deleted;
        

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user_id
     *
     * @param string $user_id
     * @return self
     */
    public function setUser_id($user_id)
    {
        $this->user_id = $user_id;
        return $this;
    }

    /**
     * Get courseCode
     *
     * @return string $user_id
     */
    public function getUser_id()
    {
        return $this->user_id;
    }

    /**
     * Set unitCode
     *
     * @param string $course_unit_id
     * @return self
     */
    public function setCourse_unit_id($course_unit_id)
    {
        $this->course_unit_id = $course_unit_id;
        return $this;
    }

    /**
     * Get unitCode
     *
     * @return string $course_unit_id
     */
    public function getCourse_unit_id()
    {
        return $this->course_unit_id;
    }

    /**
     * Set unitName
     *
     * @param string $meeting_id
     * @return self
     */
    public function setMeeting_id($meeting_id)
    {
        $this->meeting_id = $meeting_id;
        return $this;
    }

    /**
     * Get unitName
     *
     * @return string $meeting_id
     */
    public function getMeeting_id()
    {
        return $this->meeting_id;
    }

    /**
     * Set courseType
     *
     * @param string $meeting_url
     * @return self
     */
    public function setMeeting_url($meeting_url)
    {
        $this->meeting_url = $meeting_url;
        return $this;
    }

    /**
     * Get courseType
     *
     * @return string $meeting_url
     */
    public function getMeeting_url()
    {
        return $this->meeting_url;
    }

    /**
     * Set registrant_id
     *
     * @param string $registrant_id
     * @return self
     */
    public function setRegistrant_id($registrant_id)
    {
        $this->registrant_id = $registrant_id;
        return $this;
    }

    /**
     * Get registrant_id
     *
     * @return string $registrant_id
     */
    public function getRegistrant_id()
    {
        return $this->registrant_id;
    }

 
    /**
     * Set isDeleted
     *
     * @param string $isDeleted
     * @return self
     */
    public function setIsDeleted($isDeleted)
    {
        $this->is_deleted = $isDeleted;
        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return string $isDeleted
     */
    public function getIsDeleted()
    {
        return $this->is_deleted;
    }
}
