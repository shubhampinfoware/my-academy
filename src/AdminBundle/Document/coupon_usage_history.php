<?php

namespace AdminBundle\Document;

/**
 * AdminBundle\Document\coupon_usage_history
 */
class coupon_usage_history
{
    /**
     * @var MongoId $id
     */
    protected $id;

    /**
     * @var string $coupon_id
     */
    protected $coupon_id;

    /**
     * @var string $user_master_id
     */
    protected $user_master_id;

    /**
     * @var string $usage_count
     */
    protected $usage_count;

    /**
     * @var string $create_date
     */
    protected $create_date;

    /**
     * @var string $domain_id
     */
    protected $domain_id;

    /**
     * @var string $is_deleted
     */
    protected $is_deleted;
    

    /**
     * @var string $coupon_usage_history_field_1
    */
    protected $coupon_usage_history_field_1;

    /**
     * @var string $coupon_usage_history_field_2
     */
    protected $coupon_usage_history_field_2;

    /**
     * @var string $coupon_usage_history_field_3
     */
    protected $coupon_usage_history_field_3;

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set coupon_id
     *
     * @param string $coupon_id
     * @return self
     */
    public function setCoupon_id($coupon_id)
    {
        $this->coupon_id = $coupon_id;
        return $this;
    }

    /**
     * Get coupon_id
     *
     * @return string $coupon_id
     */
    public function getCoupon_id()
    {
        return $this->coupon_id;
    }

    /**
     * Set user_master_id
     *
     * @param string $user_master_id
     * @return self
     */
    public function setUser_master_id($user_master_id)
    {
        $this->user_master_id = $user_master_id;
        return $this;
    }

    /**
     * Get user_master_id
     *
     * @return string $user_master_id
     */
    public function getUser_master_id()
    {
        return $this->user_master_id;
    }

    /**
     * Set usage_count
     *
     * @param string $usage_count
     * @return self
     */
    public function setUsage_count($usage_count)
    {
        $this->usage_count = $usage_count;
        return $this;
    }

    /**
     * Get usage_count
     *
     * @return string $usage_count
     */
    public function getUsage_count()
    {
        return $this->usage_count;
    }

    /**
     * Set create_date
     *
     * @param string $create_date
     * @return self
     */
    public function setCreate_date($create_date)
    {
        $this->create_date = $create_date;
        return $this;
    }

    /**
     * Get create_date
     *
     * @return string $create_date
     */
    public function getCreate_date()
    {
        return $this->create_date;
    }

    /**
     * Set domain_id
     *
     * @param string $domain_id
     * @return self
     */
    public function setDomain_id($domain_id)
    {
        $this->domain_id = $domain_id;
        return $this;
    }

    /**
     * Get domain_id
     *
     * @return string $domain_id
     */
    public function getDomain_id()
    {
        return $this->domain_id;
    }

    /**
     * Set is_deleted
     *
     * @param string $is_deleted
     * @return self
     */
    public function setIs_deleted($is_deleted)
    {
        $this->is_deleted = $is_deleted;
        return $this;
    }

    /**
     * Get is_deleted
     *
     * @return string $is_deleted
     */
    public function getIs_deleted()
    {
        return $this->is_deleted;
    }

    /**
     * Set coupon_usage_history_field_1
     *
     * @param string $coupon_usage_history_field_1
     * @return self
     */
    public function setCoupon_usage_history_field_1($coupon_usage_history_field_1)
    {
        $this->coupon_usage_history_field_1 = $coupon_usage_history_field_1;
        return $this;
    }

    /**
     * Get coupon_usage_history_field_1
     *
     * @return string $coupon_usage_history_field_1
     */
    public function getCoupon_usage_history_field_1()
    {
        return $this->coupon_usage_history_field_1;
    }

    /**
     * Set coupon_usage_history_field_2
     *
     * @param string $coupon_usage_history_field_2
     * @return self
     */
    public function setCoupon_usage_history_field_2($coupon_usage_history_field_2)
    {
        $this->coupon_usage_history_field_2 = $coupon_usage_history_field_2;
        return $this;
    }

    /**
     * Get coupon_usage_history_field_2
     *
     * @return string $coupon_usage_history_field_2
     */
    public function getCoupon_usage_history_field_2()
    {
        return $this->coupon_usage_history_field_2;
    }

    /**
     * Set coupon_usage_history_field_3
     *
     * @param string $coupon_usage_history_field_3
     * @return self
     */
    public function setCoupon_usage_history_field_3($coupon_usage_history_field_3)
    {
        $this->coupon_usage_history_field_3 = $coupon_usage_history_field_3;
        return $this;
    }

    /**
     * Get coupon_usage_history_field_3
     *
     * @return string $coupon_usage_history_field_3
     */
    public function getCoupon_usage_history_field_3()
    {
        return $this->coupon_usage_history_field_3;
    }

    /**
     * Set couponId
     *
     * @param string $couponId
     * @return self
     */
    public function setCouponId($couponId)
    {
        $this->coupon_id = $couponId;
        return $this;
    }

    /**
     * Get couponId
     *
     * @return string $couponId
     */
    public function getCouponId()
    {
        return $this->coupon_id;
    }

    /**
     * Set userMasterId
     *
     * @param string $userMasterId
     * @return self
     */
    public function setUserMasterId($userMasterId)
    {
        $this->user_master_id = $userMasterId;
        return $this;
    }

    /**
     * Get userMasterId
     *
     * @return string $userMasterId
     */
    public function getUserMasterId()
    {
        return $this->user_master_id;
    }

    /**
     * Set usageCount
     *
     * @param string $usageCount
     * @return self
     */
    public function setUsageCount($usageCount)
    {
        $this->usage_count = $usageCount;
        return $this;
    }

    /**
     * Get usageCount
     *
     * @return string $usageCount
     */
    public function getUsageCount()
    {
        return $this->usage_count;
    }

    /**
     * Set createDate
     *
     * @param string $createDate
     * @return self
     */
    public function setCreateDate($createDate)
    {
        $this->create_date = $createDate;
        return $this;
    }

    /**
     * Get createDate
     *
     * @return string $createDate
     */
    public function getCreate_date()
    {
        return $this->create_date;
    }

    /**
     * Set domainId
     *
     * @param string $domainId
     * @return self
     */
    public function setDomainId($domainId)
    {
        $this->domain_id = $domainId;
        return $this;
    }

    /**
     * Get domainId
     *
     * @return string $domainId
     */
    public function getDomainId()
    {
        return $this->domain_id;
    }

    /**
     * Set isDeleted
     *
     * @param string $isDeleted
     * @return self
     */
    public function setIsDeleted($isDeleted)
    {
        $this->is_deleted = $isDeleted;
        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return string $isDeleted
     */
    public function getIsDeleted()
    {
        return $this->is_deleted;
    }

    /**
     * Set couponUsageHistoryField1
     *
     * @param string $couponUsageHistoryField1
     * @return self
     */
    public function setCouponUsageHistoryField1($couponUsageHistoryField1)
    {
        $this->coupon_usage_history_field_1 = $couponUsageHistoryField1;
        return $this;
    }

    /**
     * Get couponUsageHistoryField1
     *
     * @return string $couponUsageHistoryField1
     */
    public function getCouponUsageHistoryField1()
    {
        return $this->coupon_usage_history_field_1;
    }

    /**
     * Set couponUsageHistoryField2
     *
     * @param string $couponUsageHistoryField2
     * @return self
     */
    public function setCouponUsageHistoryField2($couponUsageHistoryField2)
    {
        $this->coupon_usage_history_field_2 = $couponUsageHistoryField2;
        return $this;
    }

    /**
     * Get couponUsageHistoryField2
     *
     * @return string $couponUsageHistoryField2
     */
    public function getCouponUsageHistoryField2()
    {
        return $this->coupon_usage_history_field_2;
    }

    /**
     * Set couponUsageHistoryField3
     *
     * @param string $couponUsageHistoryField3
     * @return self
     */
    public function setCouponUsageHistoryField3($couponUsageHistoryField3)
    {
        $this->coupon_usage_history_field_3 = $couponUsageHistoryField3;
        return $this;
    }

    /**
     * Get couponUsageHistoryField3
     *
     * @return string $couponUsageHistoryField3
     */
    public function getCouponUsageHistoryField3()
    {
        return $this->coupon_usage_history_field_3;
    }
}
