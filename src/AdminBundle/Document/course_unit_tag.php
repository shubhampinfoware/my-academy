<?php
    namespace AdminBundle\Document;

    use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

    /**
     * @MongoDB\Document
     */
    class course_unit_tag
    {
        /**
         * @MongoDB\Id
         */
        protected $id;
        /**
         * @MongoDB\Field(type="string")
         */
        protected $tag_name;
        /**
         * @MongoDB\Field(type="string")
         */
        protected $is_deleted; 
        /**
         * @MongoDB\Field(type="string")
         */
        protected $field_1;
        /**
         * @MongoDB\Field(type="string")
         */
        protected $field_2;
    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set Course_unit_id
     *
     * @param string $course_unit_id
     * @return self
     */
    public function setTag_name($tag_name)
    {
        $this->tag_name = $tag_name;
        return $this;
    }

    /**
     * Get tag_name
     *
     * @return string $tag_name
     */
    public function getTag_name()
    {
        return $this->tag_name;
    }

    /**
     * Set isDeleted
     *
     * @param string $isDeleted
     * @return self
     */
    public function setIsDeleted($isDeleted)
    {
        $this->is_deleted = $isDeleted;
        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return string $isDeleted
     */
    public function getIsDeleted()
    {
        return $this->is_deleted;
    }
    
    /**
     * Set field_1
     *
     * @param string field_1
     * @return self
     */
    public function setField_1($field_1)
    {
        $this->field_1 = $field_1;
        return $this;
    }

    /**
     * Get field_1
     *
     * @return string $field_1
     */
    public function getField_1()
    {
        return $this->field_1;
    }
    
    /**
     * Set field_2
     *
     * @param string field_2
     * @return self
     */
    public function setField_2($field_2)
    {
        $this->field_2 = $field_2;
        return $this;
    }

    /**
     * Get field_2
     *
     * @return string $field_2
     */
    public function getField_2()
    {
        return $this->field_2;
    }
}
