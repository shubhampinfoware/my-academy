<?php
    namespace AdminBundle\Document;

    use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

    /**
     * @MongoDB\Document
     */
    class news
    {
        /**
         * @MongoDB\Id
         */
        protected $id;

        /**
         * @MongoDB\Field(type="string")
         */
        protected $language;

        /**
         * @MongoDB\Field(type="hash")
         */
        protected $editing;

         /**
         * @MongoDB\Field(type="hash")
         */
        protected $live;

            /**
         * @MongoDB\Field(type="date")
         */
        protected $date;

            /**
         * @MongoDB\Field(type="date")
         */
        protected $publishdate;

          /**
         * @MongoDB\Field(type="date")
         */
        protected $sortdate;

        /**
         * @MongoDB\Field(type="string")
         */
        protected $user;

         /**
         * @MongoDB\Field(type="string")
         */
        protected $urlname;

        /**
         * @MongoDB\Field(type="string")
         */
        protected $userurlname;

         /**
         * @MongoDB\Field(type="string")
         */
        protected $disqusid;

         /**
         * @MongoDB\Field(type="string")
         */
        protected $umbracoid;
    
    

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set language
     *
     * @param string $language
     * @return self
     */
    public function setLanguage($language)
    {
        $this->language = $language;
        return $this;
    }

    /**
     * Get language
     *
     * @return string $language
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Set editing
     *
     * @param hash $editing
     * @return self
     */
    public function setEditing($editing)
    {
        $this->editing = $editing;
        return $this;
    }

    /**
     * Get editing
     *
     * @return hash $editing
     */
    public function getEditing()
    {
        return $this->editing;
    }

    /**
     * Set live
     *
     * @param hash $live
     * @return self
     */
    public function setLive($live)
    {
        $this->live = $live;
        return $this;
    }

    /**
     * Get live
     *
     * @return hash $live
     */
    public function getLive()
    {
        return $this->live;
    }

    /**
     * Set date
     *
     * @param date $date
     * @return self
     */
    public function setDate($date)
    {
        $this->date = $date;
        return $this;
    }

    /**
     * Get date
     *
     * @return date $date
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set publishdate
     *
     * @param date $publishdate
     * @return self
     */
    public function setPublishdate($publishdate)
    {
        $this->publishdate = $publishdate;
        return $this;
    }

    /**
     * Get publishdate
     *
     * @return date $publishdate
     */
    public function getPublishdate()
    {
        return $this->publishdate;
    }

    /**
     * Set sortdate
     *
     * @param date $sortdate
     * @return self
     */
    public function setSortdate($sortdate)
    {
        $this->sortdate = $sortdate;
        return $this;
    }

    /**
     * Get sortdate
     *
     * @return date $sortdate
     */
    public function getSortdate()
    {
        return $this->sortdate;
    }

    /**
     * Set user
     *
     * @param string $user
     * @return self
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * Get user
     *
     * @return string $user
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set urlname
     *
     * @param string $urlname
     * @return self
     */
    public function setUrlname($urlname)
    {
        $this->urlname = $urlname;
        return $this;
    }

    /**
     * Get urlname
     *
     * @return string $urlname
     */
    public function getUrlname()
    {
        return $this->urlname;
    }

    /**
     * Set userurlname
     *
     * @param string $userurlname
     * @return self
     */
    public function setUserurlname($userurlname)
    {
        $this->userurlname = $userurlname;
        return $this;
    }

    /**
     * Get userurlname
     *
     * @return string $userurlname
     */
    public function getUserurlname()
    {
        return $this->userurlname;
    }

    /**
     * Set disqusid
     *
     * @param string $disqusid
     * @return self
     */
    public function setDisqusid($disqusid)
    {
        $this->disqusid = $disqusid;
        return $this;
    }

    /**
     * Get disqusid
     *
     * @return string $disqusid
     */
    public function getDisqusid()
    {
        return $this->disqusid;
    }

    /**
     * Set umbracoid
     *
     * @param string $umbracoid
     * @return self
     */
    public function setUmbracoid($umbracoid)
    {
        $this->umbracoid = $umbracoid;
        return $this;
    }

    /**
     * Get umbracoid
     *
     * @return string $umbracoid
     */
    public function getUmbracoid()
    {
        return $this->umbracoid;
    }
}
