<?php
    namespace AdminBundle\Document;

    use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

    /**
     * @MongoDB\Document
     */
    class coursemaster
    {
        /**
         * @MongoDB\Id
         */
        protected $id;

        /**
         * @MongoDB\Field(type="string")
         */
        protected $course_catid;

        /**
         * @MongoDB\Field(type="string")
         */
        protected $course_name;
        /**
         * @MongoDB\Field(type="string")
         */
        protected $course_status;
        /**
         * @MongoDB\Field(type="string")
         */
        protected $catalog_display;
        /**
         * @MongoDB\Field(type="string")
         */
        protected $auto_enroll;
        /**
         * @MongoDB\Field(type="string")
         */
        protected $course_description;
        /**
         * @MongoDB\Field(type="string")
         */
        protected $course_image;
           /**
         * @MongoDB\Field(type="string")
         */
        protected $image_path;

        /**
         * @MongoDB\Field(type="string")
         */
        protected $cover_image_id;
        /**
         * @MongoDB\Field(type="string")
         */
        protected $course_code;
        /**
         * @MongoDB\Field(type="string")
         */
        protected $course_price;
        /**
         * @MongoDB\Field(type="string")
         */
        protected $course_duration;
        /**
         * @MongoDB\Field(type="string")
         */
        protected $time_per_day;
        /**
         * @MongoDB\Field(type="string")
         */
        protected $total_points;
           /**
         * @MongoDB\Field(type="string")
         */
        protected $course_certifcation;

        /**
         * @MongoDB\Field(type="string")
         */
        protected $certification_duration;
        /**
         * @MongoDB\Field(type="string")
         */
        protected $course_availability;
        /**
         * @MongoDB\Field(type="string")
         */
        protected $course_sharing;
        /**
         * @MongoDB\Field(type="string")
         */
        protected $sharing_accessible;
        /**
         * @MongoDB\Field(type="string")
         */
        protected $single_unit_sharing;
        /**
         * @MongoDB\Field(type="string")
         */
        protected $main_coursemaster_id;
        
        /**
         * @MongoDB\Field(type="string")
         */
        protected $language_id;
        /**
         * @MongoDB\Field(type="string")
         */
        protected $sort_order;
        /**
         * @MongoDB\Field(type="string")
         */
        protected $domain_id;
        /**
         * @MongoDB\Field(type="string")
         */
        protected $created_by;
        /**
         * @MongoDB\Field(type="string")
         */
        protected $create_date;
        /**
         * @MongoDB\Field(type="string")
         */
        protected $is_deleted;

    
    
    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set courseCatid
     *
     * @param string $courseCatid
     * @return self
     */
    public function setCourseCatid($courseCatid)
    {
        $this->course_catid = $courseCatid;
        return $this;
    }

    /**
     * Get courseCatid
     *
     * @return string $courseCatid
     */
    public function getCourseCatid()
    {
        return $this->course_catid;
    }

    /**
     * Set courseName
     *
     * @param string $courseName
     * @return self
     */
    public function setCourse_name($courseName)
    {
        $this->course_name = $courseName;
        return $this;
    }

    /**
     * Get courseName
     *
     * @return string $courseName
     */
    public function getCourseName()
    {
        return $this->course_name;
    }

    /**
     * Set courseStatus
     *
     * @param string $courseStatus
     * @return self
     */
    public function setCourseStatus($courseStatus)
    {
        $this->course_status = $courseStatus;
        return $this;
    }

    /**
     * Get courseStatus
     *
     * @return string $courseStatus
     */
    public function getCourseStatus()
    {
        return $this->course_status;
    }

    /**
     * Set catalogDisplay
     *
     * @param string $catalogDisplay
     * @return self
     */
    public function setCatalogDisplay($catalogDisplay)
    {
        $this->catalog_display = $catalogDisplay;
        return $this;
    }

    /**
     * Get catalogDisplay
     *
     * @return string $catalogDisplay
     */
    public function getCatalogDisplay()
    {
        return $this->catalog_display;
    }

    /**
     * Set autoEnroll
     *
     * @param string $autoEnroll
     * @return self
     */
    public function setAutoEnroll($autoEnroll)
    {
        $this->auto_enroll = $autoEnroll;
        return $this;
    }

    /**
     * Get autoEnroll
     *
     * @return string $autoEnroll
     */
    public function getAutoEnroll()
    {
        return $this->auto_enroll;
    }

    /**
     * Set courseDescription
     *
     * @param string $courseDescription
     * @return self
     */
    public function setCourse_description($courseDescription)
    {
        $this->course_description = $courseDescription;
        return $this;
    }

    /**
     * Get courseDescription
     *
     * @return string $courseDescription
     */
    public function getCourseDescription()
    {
        return $this->course_description;
    }

    /**
     * Set courseImage
     *
     * @param string $courseImage
     * @return self
     */
    public function setCourse_image($courseImage)
    {
        $this->course_image = $courseImage;
        return $this;
    }

    /**
     * Get courseImage
     *
     * @return string $courseImage
     */
    public function getCourseImage()
    {
        return $this->course_image;
    }

    /**
     * Set imagePath
     *
     * @param string $imagePath
     * @return self
     */
    public function setImagePath($imagePath)
    {
        $this->image_path = $imagePath;
        return $this;
    }

    /**
     * Get imagePath
     *
     * @return string $imagePath
     */
    public function getImagePath()
    {
        return $this->image_path;
    }

    /**
     * Set coverImageId
     *
     * @param string $coverImageId
     * @return self
     */
    public function setCoverImageId($coverImageId)
    {
        $this->cover_image_id = $coverImageId;
        return $this;
    }

    /**
     * Get coverImageId
     *
     * @return string $coverImageId
     */
    public function getCoverImageId()
    {
        return $this->cover_image_id;
    }

    /**
     * Set courseCode
     *
     * @param string $courseCode
     * @return self
     */
    public function setCourseCode($courseCode)
    {
        $this->course_code = $courseCode;
        return $this;
    }

    /**
     * Get courseCode
     *
     * @return string $courseCode
     */
    public function getCourseCode()
    {
        return $this->course_code;
    }

    /**
     * Set coursePrice
     *
     * @param string $coursePrice
     * @return self
     */
    public function setCoursePrice($coursePrice)
    {
        $this->course_price = $coursePrice;
        return $this;
    }

    /**
     * Get coursePrice
     *
     * @return string $coursePrice
     */
    public function getCoursePrice()
    {
        return $this->course_price;
    }

    /**
     * Set courseDuration
     *
     * @param string $courseDuration
     * @return self
     */
    public function setCourseDuration($courseDuration)
    {
        $this->course_duration = $courseDuration;
        return $this;
    }

    /**
     * Get courseDuration
     *
     * @return string $courseDuration
     */
    public function getCourseDuration()
    {
        return $this->course_duration;
    }

    /**
     * Set timePerDay
     *
     * @param string $timePerDay
     * @return self
     */
    public function setTimePerDay($timePerDay)
    {
        $this->time_per_day = $timePerDay;
        return $this;
    }

    /**
     * Get timePerDay
     *
     * @return string $timePerDay
     */
    public function getTimePerDay()
    {
        return $this->time_per_day;
    }

    /**
     * Set totalPoints
     *
     * @param string $totalPoints
     * @return self
     */
    public function setTotalPoints($totalPoints)
    {
        $this->total_points = $totalPoints;
        return $this;
    }

    /**
     * Get totalPoints
     *
     * @return string $totalPoints
     */
    public function getTotalPoints()
    {
        return $this->total_points;
    }

    /**
     * Set courseCertifcation
     *
     * @param string $courseCertifcation
     * @return self
     */
    public function setCourseCertifcation($courseCertifcation)
    {
        $this->course_certifcation = $courseCertifcation;
        return $this;
    }

    /**
     * Get courseCertifcation
     *
     * @return string $courseCertifcation
     */
    public function getCourseCertifcation()
    {
        return $this->course_certifcation;
    }

    /**
     * Set certificationDuration
     *
     * @param string $certificationDuration
     * @return self
     */
    public function setCertificationDuration($certificationDuration)
    {
        $this->certification_duration = $certificationDuration;
        return $this;
    }

    /**
     * Get certificationDuration
     *
     * @return string $certificationDuration
     */
    public function getCertificationDuration()
    {
        return $this->certification_duration;
    }

    /**
     * Set courseAvailability
     *
     * @param string $courseAvailability
     * @return self
     */
    public function setCourseAvailability($courseAvailability)
    {
        $this->course_availability = $courseAvailability;
        return $this;
    }

    /**
     * Get courseAvailability
     *
     * @return string $courseAvailability
     */
    public function getCourseAvailability()
    {
        return $this->course_availability;
    }

    /**
     * Set courseSharing
     *
     * @param string $courseSharing
     * @return self
     */
    public function setCourseSharing($courseSharing)
    {
        $this->course_sharing = $courseSharing;
        return $this;
    }

    /**
     * Get courseSharing
     *
     * @return string $courseSharing
     */
    public function getCourseSharing()
    {
        return $this->course_sharing;
    }

    /**
     * Set sharingAccessible
     *
     * @param string $sharingAccessible
     * @return self
     */
    public function setSharingAccessible($sharingAccessible)
    {
        $this->sharing_accessible = $sharingAccessible;
        return $this;
    }

    /**
     * Get sharingAccessible
     *
     * @return string $sharingAccessible
     */
    public function getSharingAccessible()
    {
        return $this->sharing_accessible;
    }

    /**
     * Set singleUnitSharing
     *
     * @param string $singleUnitSharing
     * @return self
     */
    public function setSingleUnitSharing($singleUnitSharing)
    {
        $this->single_unit_sharing = $singleUnitSharing;
        return $this;
    }

    /**
     * Get singleUnitSharing
     *
     * @return string $singleUnitSharing
     */
    public function getSingleUnitSharing()
    {
        return $this->single_unit_sharing;
    }

    /**
     * Set mainCoursemasterId
     *
     * @param string $mainCoursemasterId
     * @return self
     */
    public function setMainCoursemasterId($mainCoursemasterId)
    {
        $this->main_coursemaster_id = $mainCoursemasterId;
        return $this;
    }

    /**
     * Get mainCoursemasterId
     *
     * @return string $mainCoursemasterId
     */
    public function getMainCoursemasterId()
    {
        return $this->main_coursemaster_id;
    }

    /**
     * Set languageId
     *
     * @param string $languageId
     * @return self
     */
    public function setLanguage_id($languageId)
    {
        $this->language_id = $languageId;
        return $this;
    }

    /**
     * Get languageId
     *
     * @return string $languageId
     */
    public function getLanguageId()
    {
        return $this->language_id;
    }

    /**
     * Set sortOrder
     *
     * @param string $sortOrder
     * @return self
     */
    public function setSort_order($sortOrder)
    {
        $this->sort_order = $sortOrder;
        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return string $sortOrder
     */
    public function getSortOrder()
    {
        return $this->sort_order;
    }

    /**
     * Set domainId
     *
     * @param string $domainId
     * @return self
     */
    public function setDomainId($domainId)
    {
        $this->domain_id = $domainId;
        return $this;
    }

    /**
     * Get domainId
     *
     * @return string $domainId
     */
    public function getDomainId()
    {
        return $this->domain_id;
    }

    /**
     * Set createdBy
     *
     * @param string $createdBy
     * @return self
     */
    public function setCreated_by($createdBy)
    {
        $this->created_by = $createdBy;
        return $this;
    }

    /**
     * Get createdBy
     *
     * @return string $createdBy
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * Set createDate
     *
     * @param string $createDate
     * @return self
     */
    public function setCreateDate($createDate)
    {
        $this->create_date = $createDate;
        return $this;
    }

    /**
     * Get createDate
     *
     * @return string $createDate
     */
    public function getCreate_date()
    {
        return $this->create_date;
    }

    /**
     * Set isDeleted
     *
     * @param string $isDeleted
     * @return self
     */
    public function setIsDeleted($isDeleted)
    {
        $this->is_deleted = $isDeleted;
        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return string $isDeleted
     */
    public function getIsDeleted()
    {
        return $this->is_deleted;
    }
}
