<?php
    namespace AdminBundle\Document;

    use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

    /**
     * @MongoDB\Document
     */
    class Categorymaster
    {
        /**
         * @MongoDB\Id
         */
        protected $id;

        /**
         * @MongoDB\Field(type="string")
         */
        protected $category_name;

         /**
         * @MongoDB\Field(type="string")
         */
        protected $category_description;

          /**
         * @MongoDB\Field(type="string")
         */
        protected $main_category_id;

           /**
         * @MongoDB\Field(type="string")
         */
        protected $language_id;

         /**
         * @MongoDB\Field(type="string")
         */
        protected $category_status;

        /**
         * @MongoDB\Field(type="string")
         */
        protected $category_image_id;

         /**
         * @MongoDB\Field(type="string")
         */
        protected $parent_category_id;

         /**
         * @MongoDB\Field(type="string")
         */
        protected $domain_id;

         /**
         * @MongoDB\Field(type="string")
         */
        protected $created_datetime;

        /**
         * @MongoDB\Field(type="string")
         */
        protected $created_by;

        /**
         * @MongoDB\Field(type="string")
         */
        protected $sort_order;

        /**
         * @MongoDB\Field(type="string")
         */
        protected $is_deleted;
        /**
         * @MongoDB\Field(type="string")
         */
        protected $category_type;
        /**
         * @MongoDB\Field(type="string")
         */
        protected $display_on_home;

    
    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set categoryName
     *
     * @param string $categoryName
     * @return self
     */
    public function setCategoryName($categoryName)
    {
        $this->category_name = $categoryName;
        return $this;
    }

    /**
     * Get categoryName
     *
     * @return string $categoryName
     */
    public function getCategoryName()
    {
        return $this->category_name;
    }

    /**
     * Set categoryDescription
     *
     * @param string $categoryDescription
     * @return self
     */
    public function setCategoryDescription($categoryDescription)
    {
        $this->category_description = $categoryDescription;
        return $this;
    }

    /**
     * Get categoryDescription
     *
     * @return string $categoryDescription
     */
    public function getCategoryDescription()
    {
        return $this->category_description;
    }

    /**
     * Set mainCategoryId
     *
     * @param string $mainCategoryId
     * @return self
     */
    public function setMainCategoryId($mainCategoryId)
    {
        $this->main_category_id = $mainCategoryId;
        return $this;
    }

    /**
     * Get mainCategoryId
     *
     * @return string $mainCategoryId
     */
    public function getMainCategoryId()
    {
        return $this->main_category_id;
    }

    /**
     * Set languageId
     *
     * @param string $languageId
     * @return self
     */
    public function setLanguage_id($languageId)
    {
        $this->language_id = $languageId;
        return $this;
    }

    /**
     * Get languageId
     *
     * @return string $languageId
     */
    public function getLanguageId()
    {
        return $this->language_id;
    }

    /**
     * Set categoryStatus
     *
     * @param string $categoryStatus
     * @return self
     */
    public function setCategoryStatus($categoryStatus)
    {
        $this->category_status = $categoryStatus;
        return $this;
    }

    /**
     * Get categoryStatus
     *
     * @return string $categoryStatus
     */
    public function getCategoryStatus()
    {
        return $this->category_status;
    }

    /**
     * Set categoryImageId
     *
     * @param string $categoryImageId
     * @return self
     */
    public function setCategoryImageId($categoryImageId)
    {
        $this->category_image_id = $categoryImageId;
        return $this;
    }

    /**
     * Get categoryImageId
     *
     * @return string $categoryImageId
     */
    public function getCategoryImageId()
    {
        return $this->category_image_id;
    }

    /**
     * Set parentCategoryId
     *
     * @param string $parentCategoryId
     * @return self
     */
    public function setParentCategoryId($parentCategoryId)
    {
        $this->parent_category_id = $parentCategoryId;
        return $this;
    }

    /**
     * Get parentCategoryId
     *
     * @return string $parentCategoryId
     */
    public function getParentCategoryId()
    {
        return $this->parent_category_id;
    }

    /**
     * Set domainId
     *
     * @param string $domainId
     * @return self
     */
    public function setDomainId($domainId)
    {
        $this->domain_id = $domainId;
        return $this;
    }

    /**
     * Get domainId
     *
     * @return string $domainId
     */
    public function getDomainId()
    {
        return $this->domain_id;
    }

    /**
     * Set createdDatetime
     *
     * @param string $createdDatetime
     * @return self
     */
    public function setCreated_datetime($createdDatetime)
    {
        $this->created_datetime = $createdDatetime;
        return $this;
    }

    /**
     * Get createdDatetime
     *
     * @return string $createdDatetime
     */
    public function getCreatedDatetime()
    {
        return $this->created_datetime;
    }

    /**
     * Set createdBy
     *
     * @param string $createdBy
     * @return self
     */
    public function setCreated_by($createdBy)
    {
        $this->created_by = $createdBy;
        return $this;
    }

    /**
     * Get createdBy
     *
     * @return string $createdBy
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * Set sort_order
     *
     * @param string $sort_order
     * @return self
     */
    public function setSort_order($sort_order)
    {
        $this->sort_order = $sort_order;
        return $this;
    }

    /**
     * Get sort_order
     *
     * @return string $sort_order
     */
    public function getSort_order()
    {
        return $this->sort_order;
    }

    /**
     * Set isDeleted
     *
     * @param string $isDeleted
     * @return self
     */
    public function setIsDeleted($isDeleted)
    {
        $this->is_deleted = $isDeleted;
        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return string $isDeleted
     */
    public function getIsDeleted()
    {
        return $this->is_deleted;
    }
     /*
     * @param string $category_type
     * @return self
     */
    public function setCategory_type($category_type)
    {
        $this->category_type = $category_type;
        return $this;
    }

    /**
     * Get category_type
     *
     * @return string $category_type
     */
    public function getCategory_type()
    {
        return $this->category_type;
    }
     /*
     * @param string $display_on_home
     * @return self
     */
    public function setDisplay_on_home($display_on_home)
    {
        $this->display_on_home = $display_on_home;
        return $this;
    }

    /**
     * Get display_on_home
     *
     * @return string $display_on_home
     */
    public function getDisplay_on_home()
    {
        return $this->display_on_home;
    }
}
