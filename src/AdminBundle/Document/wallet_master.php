<?php

namespace AdminBundle\Document;

/**
 * AdminBundle\Document\wallet_master
 */

class wallet_master
{
  /**
     * @var MongoId $id
     */
    protected $id;

    /**
     * @MongoDB\Field(type="string")
     * @var string $wallet_code
     */
    protected $wallet_code;

    /**
     * @MongoDB\Field(type="string")
     * @var string $user_master_id
     */
    protected $user_master_id;

    /**
     * @MongoDB\Field(type="string")
     * @var string $virtual_money_balance
     */
    protected $virtual_money_balance;

    /**
     * @MongoDB\Field(type="string")
     * @var string $wallet_points
     */
    protected $wallet_points;

    /**
     * @MongoDB\Field(type="string")
     * @var string $actual_money_balance
     */
    protected $actual_money_balance;

    /**
     * @MongoDB\Field(type="string")
     * @var string $cashback_balance
     */
    protected $cashback_balance;
    

    /**
     * @MongoDB\Field(type="string")
     * @var string $last_updated_wallet_datetime
    */
    protected $last_updated_wallet_datetime;

    /**
     * @MongoDB\Field(type="string")
     * @var string $is_deleted
     */
    protected $is_deleted;

    /**
     * @MongoDB\Field(type="string")
     * @var string $wallet_member_tag_id
     */
    protected $wallet_member_tag_id;

    /**
     *  @MongoDB\Field(type="string")
     * @var string $wallet_master_field_1
     */
    protected $wallet_master_field_1;

      /**
       *  @MongoDB\Field(type="string")
     * @var string $wallet_master_field_2
     */
    protected $wallet_master_field_2;
    
     /**
      *  @MongoDB\Field(type="string")
     * @var string $wallet_master_field_3
     */
    protected $wallet_master_field_3;
     
    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set wallet_code
     *
     * @param string $wallet_code
     * @return self
     */
    public function setWallet_code($wallet_code)
    {
        $this->wallet_code = $wallet_code;
        return $this;
    }

    /**
     * Get wallet_code
     *
     * @return string $wallet_code
     */
    public function getWallet_code()
    {
        return $this->wallet_code;
    }

    /**
     * Set user_master_id
     *
     * @param string $user_master_id
     * @return self
     */
    public function setUser_master_id($user_master_id)
    {
        $this->user_master_id = $user_master_id;
        return $this;
    }

    /**
     * Get user_master_id
     *
     * @return string $user_master_id
     */
    public function getUser_master_id()
    {
        return $this->user_master_id;
    }

    /**
     * Set virtual_money_balance
     *
     * @param string $virtual_money_balance
     * @return self
     */
    public function setVirtual_money_balance($virtual_money_balance)
    {
        $this->virtual_money_balance = $virtual_money_balance;
        return $this;
    }

    /**
     * Get virtual_money_balance
     *
     * @return string $virtual_money_balance
     */
    public function getVirtual_money_balance()
    {
        return $this->virtual_money_balance;
    }

    /**
     * Set wallet_points
     *
     * @param string $wallet_points
     * @return self
     */
    public function setWallet_points($wallet_points)
    {
        $this->wallet_points = $wallet_points;
        return $this;
    }

    /**
     * Get wallet_points
     *
     * @return string $wallet_points
     */
    public function getWallet_points()
    {
        return $this->wallet_points;
    }

    /**
     * Set actual_money_balance
     *
     * @param string $actual_money_balance
     * @return self
     */
    public function setActual_money_balance($actual_money_balance)
    {
        $this->actual_money_balance = $actual_money_balance;
        return $this;
    }

    /**
     * Get actual_money_balance
     *
     * @return string $actual_money_balance
     */
    public function getActual_money_balance()
    {
        return $this->actual_money_balance;
    }

    /**
     * Set cashback_balance
     *
     * @param string $cashback_balance
     * @return self
     */
    public function setCashback_balance($cashback_balance)
    {
        $this->cashback_balance = $cashback_balance;
        return $this;
    }

    /**
     * Get cashback_balance
     *
     * @return string $cashback_balance
     */
    public function getCashback_balance()
    {
        return $this->cashback_balance;
    }

    /**
     * Set last_updated_wallet_datetime
     *
     * @param string $last_updated_wallet_datetime
     * @return self
     */
    public function setLast_updated_wallet_datetime($last_updated_wallet_datetime)
    {
        $this->last_updated_wallet_datetime = $last_updated_wallet_datetime;
        return $this;
    }

    /**
     * Get last_updated_wallet_datetime
     *
     * @return string $last_updated_wallet_datetime
     */
    public function getLast_updated_wallet_datetime()
    {
        return $this->last_updated_wallet_datetime;
    }

       /**
     * Set is_deleted
     *
     * @param string $is_deleted
     * @return self
     */
    public function setIs_deleted($is_deleted)
    {
        $this->is_deleted = $is_deleted;
        return $this;
    }

    /**
     * Get is_deleted
     *
     * @return string $is_deleted
     */
    public function getIs_deleted()
    {
        return $this->is_deleted;
    }
       /**
     * Set wallet_member_tag_id
     *
     * @param string $wallet_member_tag_id
     * @return self
     */
    public function setWallet_member_tag_id($wallet_member_tag_id)
    {
        $this->wallet_member_tag_id = $wallet_member_tag_id;
        return $this;
    }

    /**
     * Get wallet_member_tag_id
     *
     * @return string $wallet_member_tag_id
     */
    public function getWallet_member_tag_id()
    {
        return $this->wallet_member_tag_id;
    }   /**
    * Set wallet_master_field_1
    *
    * @param string $wallet_master_field_1
    * @return self
    */
   public function setWallet_master_field_1($wallet_master_field_1)
   {
       $this->wallet_master_field_1 = $wallet_master_field_1;
       return $this;
   }

   /**
    * Get wallet_master_field_1
    *
    * @return string $wallet_master_field_1
    */
   public function getWallet_master_field_1()
   {
       return $this->wallet_master_field_1;
   } 
   
   
     /**
   * Set wallet_master_field_2
   *
   * @param string $wallet_master_field_2
   * @return self
   */
  public function setWallet_master_field_2($wallet_master_field_2)
  {
      $this->wallet_master_field_2 = $wallet_master_field_2;
      return $this;
  }

  /**
   * Get wallet_master_field_2
   *
   * @return string $wallet_master_field_2
   */
  public function getWallet_master_field_2()
  {
      return $this->wallet_master_field_2;
  }  
  
   /**
  * Set wallet_master_field_3
  *
  * @param string $wallet_master_field_3
  * @return self
  */
 public function setWallet_master_field_3($wallet_master_field_3)
 {
     $this->wallet_master_field_3 = $wallet_master_field_3;
     return $this;
 }

 /**
  * Get wallet_master_field_3
  *
  * @return string $wallet_master_field_3
  */
 public function getWallet_master_field_3()
 {
     return $this->wallet_master_field_3;
 }   

    /**
     * Set walletCode
     *
     * @param string $walletCode
     * @return self
     */
    public function setWalletCode($walletCode)
    {
        $this->wallet_code = $walletCode;
        return $this;
    }

    /**
     * Get walletCode
     *
     * @return string $walletCode
     */
    public function getWalletCode()
    {
        return $this->wallet_code;
    }

    /**
     * Set userMasterId
     *
     * @param string $userMasterId
     * @return self
     */
    public function setUserMasterId($userMasterId)
    {
        $this->user_master_id = $userMasterId;
        return $this;
    }

    /**
     * Get userMasterId
     *
     * @return string $userMasterId
     */
    public function getUserMasterId()
    {
        return $this->user_master_id;
    }

    /**
     * Set virtualMoneyBalance
     *
     * @param string $virtualMoneyBalance
     * @return self
     */
    public function setVirtualMoneyBalance($virtualMoneyBalance)
    {
        $this->virtual_money_balance = $virtualMoneyBalance;
        return $this;
    }

    /**
     * Get virtualMoneyBalance
     *
     * @return string $virtualMoneyBalance
     */
    public function getVirtualMoneyBalance()
    {
        return $this->virtual_money_balance;
    }

    /**
     * Set walletPoints
     *
     * @param string $walletPoints
     * @return self
     */
    public function setWalletPoints($walletPoints)
    {
        $this->wallet_points = $walletPoints;
        return $this;
    }

    /**
     * Get walletPoints
     *
     * @return string $walletPoints
     */
    public function getWalletPoints()
    {
        return $this->wallet_points;
    }

    /**
     * Set actualMoneyBalance
     *
     * @param string $actualMoneyBalance
     * @return self
     */
    public function setActualMoneyBalance($actualMoneyBalance)
    {
        $this->actual_money_balance = $actualMoneyBalance;
        return $this;
    }

    /**
     * Get actualMoneyBalance
     *
     * @return string $actualMoneyBalance
     */
    public function getActualMoneyBalance()
    {
        return $this->actual_money_balance;
    }

    /**
     * Set cashbackBalance
     *
     * @param string $cashbackBalance
     * @return self
     */
    public function setCashbackBalance($cashbackBalance)
    {
        $this->cashback_balance = $cashbackBalance;
        return $this;
    }

    /**
     * Get cashbackBalance
     *
     * @return string $cashbackBalance
     */
    public function getCashbackBalance()
    {
        return $this->cashback_balance;
    }

    /**
     * Set lastUpdatedWalletDatetime
     *
     * @param string $lastUpdatedWalletDatetime
     * @return self
     */
    public function setLastUpdatedWalletDatetime($lastUpdatedWalletDatetime)
    {
        $this->last_updated_wallet_datetime = $lastUpdatedWalletDatetime;
        return $this;
    }

    /**
     * Get lastUpdatedWalletDatetime
     *
     * @return string $lastUpdatedWalletDatetime
     */
    public function getLastUpdatedWalletDatetime()
    {
        return $this->last_updated_wallet_datetime;
    }

    /**
     * Set isDeleted
     *
     * @param string $isDeleted
     * @return self
     */
    public function setIsDeleted($isDeleted)
    {
        $this->is_deleted = $isDeleted;
        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return string $isDeleted
     */
    public function getIsDeleted()
    {
        return $this->is_deleted;
    }

    /**
     * Set walletMemberTagId
     *
     * @param string $walletMemberTagId
     * @return self
     */
    public function setWalletMemberTagId($walletMemberTagId)
    {
        $this->wallet_member_tag_id = $walletMemberTagId;
        return $this;
    }

    /**
     * Get walletMemberTagId
     *
     * @return string $walletMemberTagId
     */
    public function getWalletMemberTagId()
    {
        return $this->wallet_member_tag_id;
    }

    /**
     * Set walletMasterField1
     *
     * @param string $walletMasterField1
     * @return self
     */
    public function setWalletMasterField1($walletMasterField1)
    {
        $this->wallet_master_field_1 = $walletMasterField1;
        return $this;
    }

    /**
     * Get walletMasterField1
     *
     * @return string $walletMasterField1
     */
    public function getWalletMasterField1()
    {
        return $this->wallet_master_field_1;
    }

    /**
     * Set walletMasterField2
     *
     * @param string $walletMasterField2
     * @return self
     */
    public function setWalletMasterField2($walletMasterField2)
    {
        $this->wallet_master_field_2 = $walletMasterField2;
        return $this;
    }

    /**
     * Get walletMasterField2
     *
     * @return string $walletMasterField2
     */
    public function getWalletMasterField2()
    {
        return $this->wallet_master_field_2;
    }

    /**
     * Set walletMasterField3
     *
     * @param string $walletMasterField3
     * @return self
     */
    public function setWalletMasterField3($walletMasterField3)
    {
        $this->wallet_master_field_3 = $walletMasterField3;
        return $this;
    }

    /**
     * Get walletMasterField3
     *
     * @return string $walletMasterField3
     */
    public function getWalletMasterField3()
    {
        return $this->wallet_master_field_3;
    }
}
