<?php
    namespace AdminBundle\Document;

    use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

    /**
     * @MongoDB\Document
     */
    class user_history_video
    {
        /**
         * @MongoDB\Id
         */
        protected $id;

        /**
         * @MongoDB\Field(type="string")
         */
        protected $user_id;

        /**
         * @MongoDB\Field(type="string")
         */
        protected $main_id;

         /**
         * @MongoDB\Field(type="string")
         */
        protected $course_id;

        /**
         * @MongoDB\Field(type="string")
         */
        protected $extra_id;

           /**
         * @MongoDB\Field(type="string")
         */
        protected $extra1_id;

           /**
         * @MongoDB\Field(type="string")
         */
        protected $is_deleted;





    
    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set mainId
     *
     * @param string $mainId
     * @return self
     */
    public function setMainId($mainId)
    {
        $this->main_id = $mainId;
        return $this;
    }

    /**
     * Get mainId
     *
     * @return string $mainId
     */
    public function getMainId()
    {
        return $this->main_id;
    }

    /**
     * Set courseId
     *
     * @param string $courseId
     * @return self
     */
    public function setCourseId($courseId)
    {
        $this->course_id = $courseId;
        return $this;
    }

    /**
     * Get courseId
     *
     * @return string $courseId
     */
    public function getCourseId()
    {
        return $this->course_id;
    }

    /**
     * Set extraId
     *
     * @param string $extraId
     * @return self
     */
    public function setExtraId($extraId) //user_id
    {
        $this->extra_id = $extraId;
        return $this;
    }

    /**
     * Get extraId
     *
     * @return string $extraId
     */
    public function getExtraId()
    {
        return $this->extra_id;
    }

    /**
     * Set extra1Id
     *
     * @param string $extra1Id
     * @return self
     */
    public function setExtra1Id($extra1Id)
    {
        $this->extra1_id = $extra1Id;
        return $this;
    }

    /**
     * Get extra1Id
     *
     * @return string $extra1Id
     */
    public function getExtra1Id()
    {
        return $this->extra1_id;
    }

    /**
     * Set isDeleted
     *
     * @param string $isDeleted
     * @return self
     */
    public function setIsDeleted($isDeleted)
    {
        $this->is_deleted = $isDeleted;
        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return string $isDeleted
     */
    public function getIsDeleted()
    {
        return $this->is_deleted;
    }

    /**
     * Set userId
     *
     * @param string $userId
     * @return self
     */
    public function setUserId($userId)
    {
        $this->user_id = $userId;
        return $this;
    }

    /**
     * Get userId
     *
     * @return string $userId
     */
    public function getUserId()
    {
        return $this->user_id;
    }
}
