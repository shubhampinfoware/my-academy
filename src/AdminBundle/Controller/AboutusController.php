<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AdminBundle\Entity\Usermaster;
use AdminBundle\Entity\Aboutus;
use AdminBundle\Entity\Governoratemaster;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\PropertyAccess\PropertyAccess;

/**
* @Route("/{domain}/admin")
*/
class AboutusController extends BaseController {
	
    public function __construct() {
        parent::__construct();
        $obj = new BaseController();
        $obj->checkSessionAction();
    }
    
    /**
     * @Route("/aboutus")
     * @Template()
     */
    public function aboutusAction() {

        $language_list = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Languagemaster')->findBy(array("is_deleted"=>0));
        $about_us_list = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Aboutus')->findAll();
        
        return array("languages" => $language_list ,"aboutuslist"=>$about_us_list);
    }
    
     /**
     * @Route("/saveaboutus",name="save_about_us")
     * @Template()
     */
    public function saveaboutusAction() {

       $session = new Session();
       $em = $this->getDoctrine()->getManager();
       $domain_id = $this->get('session')->get('domain_id');
       $about_us_info = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Aboutus')->findOneBy(array("is_deleted"=>0,"language_id"=>$_REQUEST['language_id'],"domain_id"=>$domain_id));
       if(!empty($about_us_info)){
           $about_us_info->setDescription($_REQUEST['who_we_are']) ;
           $em->persist($about_us_info);
           $em->flush();
       }
       else{
           $about_us_info = new Aboutus();
           $about_us_info->setDescription($_REQUEST['who_we_are']);
           $about_us_info->setLanguage_id($_REQUEST['language_id']);
           $about_us_info->setDomain_id($domain_id);
           $about_us_info->setMain_about_us_id(0);
           $about_us_info->setIs_deleted(0);
          
           $em->persist($about_us_info);
           $em->flush();
           $main_abt_id = $about_us_info->getAbout_us_id();
           
           $about_us_info->setMain_about_us_id($main_abt_id);
           $em->persist($about_us_info);
           $em->flush();
           
       }
        $this->get('session')->getFlashBag()->set('success_msg', 'Inserted successfully');
		return $this->redirect($this->generateUrl('admin_aboutus_aboutus',array("domain"=>$this->get('session')->get('domain'))));
    }

}
