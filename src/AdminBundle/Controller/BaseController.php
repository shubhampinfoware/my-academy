<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\PropertyAccess\PropertyAccess;
use AdminBundle\Entity\media_type;
use AdminBundle\Entity\Mediamaster;
use AdminBundle\Entity\Apppushnotificationmaster;
use AdminBundle\Entity\Usermaster;
use AdminBundle\Entity\Usersetting;
use AdminBundle\Entity\Categorymaster;
use AdminBundle\Entity\Apptypemaster;
use AdminBundle\Entity\Appdetails;

class BaseController extends Controller {

    //public $api_key = 'cfz5c7FMTKGQzgWtrGROfA'; //Please Input Your Own API Key Here';
    public $api_key = 'waRgqjneRTyIhCdnRhVl8A';
    public $api_secret = 'D1ArW1vL1yyFG5bwd87pgGMGwrOcK2n1'; //Please Input Your Own API Secret Here';
    public $api_url = 'https://api.zoom.us/v2/';
    //public $access_token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJpVFdrWGcxRFRjV2hVMG05cGU4V0R3IiwiZXhwIjoxNDk2MDkxOTY0MDAwfQ.Fijyp8y6Usz5mQ19PVh_SAcjtPANmjIXn3McMyklS4k';
    public $access_token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJ3YVJncWpuZVJUeUloQ2RuUmhWbDhBIiwiZXhwIjo0MDc5MDQ4NzgxMDAwfQ.fnQie6TWV4W1VOfDIpCrqVFNrcLvsrz11GV5x3sDP38'; //(Chess base account access token)
     
    public $MERCHANT_KEY = "gtKFFx";
    public $salt = "eCwWELxi";
    public $SUCCESS = 1;
    public $FAILURE = 0;
    public $payu_server_url = "https://sandboxsecure.payu.in/_payment";//https://secure.payu.in/";
   // public $payu_server_url = "https://secure.payu.in/";
    public $base_url = "https://skillgamesacademy.com/chesscoaching";
    public $site_base_url = "https://skillgamesacademy.com/chess_coaching_site/#/course_desc/";

    public $ADMIN_ROLE_ID = 1;
    public $LEARNER_ROLE_ID = 2;
    public $GUEST_ROLE_ID = 3;
    public $TRAINER_ROLE_ID = 4;
    public $ORGANIZATION_ROLE_ID = 5;

    public function __construct() {
        date_default_timezone_set("Asia/Calcutta");
    }

    /**
     * @Route("/checkSession")
     */
    function checkSessionAction() {
        $session = new Session();
        if ($session->get('user_id') == '' && $session->get('username') == '') {
            $hostname = $_SERVER["SERVER_NAME"];
            header("location:http://" . $hostname . "/superadmin/");
            exit;
        }
    }

    public function keyEncryptionAction($string) {
        if ($string != "" && $string != NULL && !empty($string) && ctype_space($string) == false) {
            $key = $this->container->getParameter('key');
            $res = '';
            for ($i = 0; $i < strlen($string); $i++) {
                $c = ord(substr($string, $i));

                $c += ord(substr($key, (($i + 1) % strlen($key))));
                $res .= chr($c & 0xFF);
            }
            return base64_encode($res);
        }
        return "";
    }

    public function keyDecryptionAction($string) {
        if ($string != "" && $string != NULL && !empty($string) && ctype_space($string) == false) {
            $key = $this->container->getParameter('key');

            $res = '';
            $string = base64_decode($string);
            for ($i = 0; $i < strlen($string); $i++) {
                $c = ord(substr($string, $i));

                $c -= ord(substr($key, (($i + 1) % strlen($key))));
                $res .= chr(abs($c) & 0xFF);
            }
            return $res;
        }
        return "";
    }

    /**
     *
     * use for security
     *
     * @param mix $value
     * @param string $type ('display')
     *
     * @return string $value
     */
    public function bwiz_security($value, $type = "") {
        //check type
        if (isset($type) && !empty($type) && $type == "display") {
            $str = stripslashes($value);
        } else {
            $str = addslashes($value);
        }

        return $str;
    }

    //use to calculate memory usage of php
    // for call - echo $this->convert(memory_get_usage()) . "<br>"; // at start and end both
    function convert($size) {
        $unit = array('b', 'kb', 'mb', 'gb', 'tb', 'pb');
        return @round($size / pow(1024, ($i = floor(log($size, 1024)))), 2) . ' ' . $unit[$i];
    }

    public function mediatype($file_type) {
        $em = $this->getDoctrine()->getManager();

        $media_type_list = $em->getRepository('AdminBundle:Mediatype')->findBy(array("is_deleted" => "0"));

        foreach (array_slice($media_type_list, 0) as $pkey => $pval) {
            $type = explode(",", $pval->getMedia_type_allowed());
            foreach ($type as $val) {
                if ($val == $file_type) {
                    $media_type_id = $pval->getMedia_type_id();
                }
            }
        }
        if (!empty($media_type_id)) {
            return $media_type_id;
        } else {
            return FALSE;
        }
    }

    function mediauploadAction($file, $tmpname, $path, $upload_dir, $mediatype_id) {

        $clean_image = preg_replace('/\s+/', '', $file);
        $logo_name = date('Y_m_d_H_i_s') . '_' . $clean_image;

        if (!file_exists($upload_dir)) {
            mkdir($upload_dir, 0777);
        }
        //logo upload check
        if (move_uploaded_file($tmpname, $upload_dir . $logo_name)) {
            $Mediamaster = new Mediamaster();


            $Mediamaster->setMedia_title($logo_name);
            $Mediamaster->setMedia_location($path);
            $Mediamaster->setMedia_name($logo_name);
            $Mediamaster->setCreated_datetime(date('Y-m-d H:i:s'));
            $Mediamaster->setIs_deleted(0);

            $em = $this->getDoctrine()->getManager();
            $em->persist($Mediamaster);
            $em->flush();
            $media_library_master_id = $Mediamaster->getMediamaster_id();
            return $media_library_master_id;
        } else {
            return FALSE;
        }
    }

    /**
     * @Route("/getmedia/{media_library_master_id}/{live_path}")
     */
    public function getmediaAction($media_library_master_id, $live_path) {
        $em = $this->getDoctrine()->getManager();
        $media_library = $em
                ->getRepository('AdminBundle:Mediamaster')
                ->findOneBy(array('mediamaster_id' => $media_library_master_id, 'is_deleted' => "0"));
        if ($media_library) {
            return new Response ($live_path . $media_library->getMedia_location() . "/" . $media_library->getMedia_name());
        } else {
            return new Response();
        }
    }
    
     /**
     * @Route("/getmedia1/{media_library_master_id}/{live_path}")
     */
    public function getmedia1Action($media_library_master_id, $live_path) {
        $em = $this->getDoctrine()->getManager();
        $media_library = $em
                ->getRepository('AdminBundle:Mediamaster')
                ->findOneBy(array('mediamaster_id' => $media_library_master_id, 'is_deleted' => "0"));
        if ($media_library) {
            return $live_path . $media_library->getMedia_location() . "/" . $media_library->getMedia_name();
        } else {
            return 0;
        }
    }

    public function getdata($table, $condition) {
        $doc = $this->getDoctrine()->getManager()
                ->getRepository("AdminBundle:" . $table)
                ->findBy($condition);
        return $doc;
    }

    public function getonedata($table, $condition) {
        $doc = $this->getDoctrine()->getManager()
                ->getRepository("AdminBundle:" . $table)
                ->findOneBy($condition);
        return $doc;
    }

    public function send_notification($registration_ids, $title, $message, $provider, $app_id, $domain_id, $tablename, $tabledataid) {
        //$date_t = strtotime(date("y-m-d H:i:s"));
        ob_start();
        /*
          $app_id= CUST // Customer App
          $app_id = DEL // Delivery App
          $domain_id = domain_code
         */

        switch ($provider) {
            case 1:
                $result = "FALSE";
                $development = false;
                $apns_url = NULL; // Set Later
                $pathCk = NULL; // Set Later
                $apns_port = 2195;

                /* if($app_id == 'CUST')
                  {
                  // Customer App
                  if ($development) {
                  $apns_url = 'gateway.sandbox.push.apple.com';

                  $pathCk=$this->container->get('kernel')->locateResource('@WSBundle/Controller/');
                  } else {
                  $apns_url = 'gateway.push.apple.com';

                  $pathCk=$this->container->get('kernel')->locateResource('@WSBundle/Controller/');
                  }

                  $passphrase = '123';
                  }
                  elseif($app_id == 'DEL')
                  {
                  // Delivery App

                  if ($development) {
                  $apns_url = 'gateway.sandbox.push.apple.com';

                  $pathCk=$this->container->get('kernel')->locateResource('@WSBundle/Controller/');
                  } else {
                  $apns_url = 'gateway.push.apple.com';

                  $pathCk=$this->container->get('kernel')->locateResource('@WSBundle/Controller/');
                  }

                  $passphrase = '123';

                  } */

                $app_type_master = $this->getDoctrine()
                        ->getManager()
                        ->getRepository('AdminBundle:Apptypemaster')
                        ->findOneBy(array('is_deleted' => 0, 'app_type_code' => $app_id));

                if (!empty($app_type_master)) {
                    $app_details = $this->getDoctrine()
                            ->getManager()
                            ->getRepository('AdminBundle:Appdetails')
                            ->findOneBy(array('is_deleted' => 0, 'app_type_id' => $app_type_master->getApp_type_id(), "domain_id" => $domain_id, "status" => 'active'));

                    if (!empty($app_details)) {
                        if ($development) {

                            if (!empty($app_details->getApp_apns_certificate_development()) && $app_details->getApp_apns_certificate_development() != "" && !empty($app_details->getApp_apns_certificate_development_password()) && $app_details->getApp_apns_certificate_development_password() != "") {

                                $apns_url = 'gateway.sandbox.push.apple.com';

                                $pathCk = $this->container->get('kernel')->locateResource('@WSBundle/Controller/' . $app_details->getApp_apns_certificate_development());
                                $passphrase = $app_details->getApp_apns_certificate_development_password();
                            }
                        } else {
                            if (!empty($app_details->getApp_apns_certificate_production()) && $app_details->getApp_apns_certificate_production() != "" && !empty($app_details->getApp_apns_certificate_production_password()) && $app_details->getApp_apns_certificate_production_password() != "") {
                                $apns_url = 'gateway.push.apple.com';

                                $pathCk = $this->container->get('kernel')->locateResource('@WSBundle/Controller/' . $app_details->getApp_apns_certificate_production());
                                $passphrase = $app_details->getApp_apns_certificate_production_password();
                            }
                        }
                    }
                }

                if (!empty($apns_url) && $apns_url != "") {
                    /*
                      $ctx = stream_context_create();
                      stream_context_set_option($ctx, 'ssl', 'local_cert', $pathCk);
                      stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

                      $apns = stream_socket_client(
                      'ssl://' . $apns_url . ':' . $apns_port, $err,
                      $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
                     */
                    $data = json_decode($message);
                    $response = $response1 = "";
                    /*
                     * $payload['aps'] = array(
                      'alert' => $data->detail,
                      'badge' => 1,
                      'sound' => 'default',
                      'response'=>$data->response
                      );
                     */
                    $payload['job_id'] = $data->code;
                    $code = $data->code;
                    $detail = $data->detail;
                    $response = print_r($data->response, 1);

                    // START LOOP
                    if ($registration_ids != "ALL") {
                        unset($where);
                        if (is_array($registration_ids)) {
                            $registration_ids = implode("','", $registration_ids);
                        }

                        $em = $this->getDoctrine()->getManager();

                        $connection = $em->getConnection();
                        $apns_user = $connection->prepare("SELECT * FROM apns_user WHERE apns_regid in ('" . $registration_ids . "') and is_deleted=0");

                        $apns_user->execute();
                        $apns_user_list = $apns_user->fetchAll();

                        // specific user

                        $em = $this->getDoctrine()->getManager();
                        $connection = $em->getConnection();
                        $statement = $connection->prepare("UPDATE apns_user SET badge=(badge+1) WHERE apns_regid in ('" . $registration_ids . "') and is_deleted=0");
                        $statement->execute();
                    } else {
                        // All user
                        $em = $this->getDoctrine()->getManager();
                        $connection = $em->getConnection();
                        $statement = $connection->prepare("UPDATE apns_user SET badge = (badge+1) WHERE  is_deleted=0");
                        $statement->execute();
                    }
                    $result = "";
                    //$res_arr = array();
                    /*
                      $ctx = stream_context_create();
                      stream_context_set_option($ctx, 'ssl', 'local_cert', $pathCk);
                      stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

                      $apns = stream_socket_client(
                      'ssl://' . $apns_url . ':' . $apns_port, $err,
                      $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
                     */
                    $ctx = stream_context_create();
                    stream_context_set_option($ctx, 'ssl', 'local_cert', $pathCk);
                    stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);


                    $apns = stream_socket_client(
                            'ssl://' . $apns_url . ':' . $apns_port, $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);

                    stream_set_blocking($apns, 0);

                    foreach (array_slice($apns_user_list, 0) as $key => $val) {
                        if ($key == "50" || $key == "100" || $key == "250" || $key == "500" || $key == "750" || $key == "1000" || $key == "1250" || $key == "1500" || $key == "1750" ||
                                $key == "2000" || $key == "2250" || $key == "2500" || $key == "2750" || $key == "3000" || $key == "3250" ||
                                $key == "3500" || $key == "3750" || $key == "4000") {
                            ob_flush();
                            flush();
                        }
                        $device = $val['apns_regid'];

                        //$final_payload = json_encode($payload);
                        if (strlen($device) == 64) {
                            if (!$apns) {
                                echo "Failed to connect (stream_socket_client): $err $errstr";
                                $result = "Failed to connect : $error $errorString " . PHP_EOL;
                                $ctx = stream_context_create();
                                stream_context_set_option($ctx, 'ssl', 'local_cert', $pathCk);
                                stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

                                $apns = stream_socket_client(
                                        'ssl://' . $apns_url . ':' . $apns_port, $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
                                stream_set_blocking($apns, 0);
                            } else {
                                $payload['aps'] = array(
                                    'alert' => $data->detail,
                                    'badge' => 1,
                                    'sound' => 'default',
                                    'response' => $data->response
                                );

                                $final_payload = json_encode($payload);

                                $apnsMessage = chr(0) . pack('n', 32) . pack('H*', str_replace(' ', '', $device)) . pack('n', strlen($final_payload)) . $final_payload;
                                $result = '';

                                try {
                                    $result = fwrite($apns, $apnsMessage);
                                } catch (\Exception $e) {
                                    fclose($apns);
                                    $result = $e->getMessage();
                                    echo('<br>Error sending Device: ' . $device );
                                    echo('<br>Error sending payload: ' . $e->getMessage());


                                    $ctx = stream_context_create();
                                    stream_context_set_option($ctx, 'ssl', 'local_cert', $pathCk);
                                    stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

                                    $apns = stream_socket_client(
                                            'ssl://' . $apns_url . ':' . $apns_port, $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
                                    stream_set_blocking($apns, 0);
                                }
                                //write log start
                                $txtf = "iphone_push_file.txt";
                                if (!file_exists($txtf)) {
                                    fopen($txtf, 'w');
                                }
                                //-----write file to track error---------
                                // Open the file to get existing content
                                $current = file_get_contents($txtf);
                                // Append a new person to the file
                                $current .= "\nAPNS :- " . $val['apns_regid'] . "====>" . $result;
                                // Write the contents back to the file
                                file_put_contents($txtf, $current);

                                unset($payload);
                                unset($statement);
                                unset($final_payload);
                                unset($apnsMessage);
                                $result = $device = '';
                            }
                            // END LOOP
                        }
                    }

                    foreach (array_slice($apns_user_list, 0) as $key => $val) {
                        $device = $val['apns_regid'];
                        $apppushnotificationmaster = new Apppushnotificationmaster();
                        $apppushnotificationmaster->setDevice_name('ios');
                        $apppushnotificationmaster->setApp_id($app_id);
                        $apppushnotificationmaster->setDomain_id($domain_id);
                        $apppushnotificationmaster->setDevice_token($device);

                        $connection = $em->getConnection();
                        $apns_query = $connection->prepare("SELECT * FROM apns_user WHERE apns_regid = '" . $device . "' and is_deleted=0 ORDER BY apns_user_id DESC");
                        $apns_query->execute();
                        $apns_user = $apns_query->fetchAll();
                        $user_id = '';
                        if (!empty($apns_user)) {
                            $user_id = $apns_user[0]['user_id'];
                            $device_id = $apns_user[0]['device_id'];

                            $em = $this->getDoctrine()->getManager();
                            $user_setting = $em->getRepository('AdminBundle:Usersetting')->findOneBy(array("user_id" => $user_id, "is_deleted" => 0));

                            $value = json_decode($user_setting->getSetting_value(), true);

                            $lang_id = $value['language'];

                            $apppushnotificationmaster->setUser_id($user_id);
                            $apppushnotificationmaster->setLanguage_id($lang_id);
                            $apppushnotificationmaster->setDevice_id($device_id);
                        }

                        $apppushnotificationmaster->setData($detail);
                        $apppushnotificationmaster->setCode($code);
                        $apppushnotificationmaster->setTable_name($tablename);
                        $apppushnotificationmaster->setTable_id($tabledataid);
                        $apppushnotificationmaster->setResponse($response);
                        $apppushnotificationmaster->setDatetime(date("Y-m-d H:i:s"));
                        $apppushnotificationmaster->setIs_deleted(0);
                        $em = $this->getDoctrine()->getManager();
                        $em->persist($apppushnotificationmaster);
                        $em->flush();
                    }

                    // END LOOP


                    fclose($apns);
                    unset($apns_user_list);
                }

                break;

            case 2:
                $result = "FALSE";
                $title_name = $title;
                $data = array("title" => $title_name, "message" => $message);
                $URL = 'https://android.googleapis.com/gcm/send';

                $fields = array(
                    'registration_ids' => $registration_ids,
                    'data' => $data,
                );

                $app_type_master = $this->getDoctrine()
                        ->getManager()
                        ->getRepository('AdminBundle:Apptypemaster')
                        ->findOneBy(array('is_deleted' => 0, 'app_type_code' => $app_id));

                if (!empty($app_type_master)) {
                    $app_details = $this->getDoctrine()
                            ->getManager()
                            ->getRepository('AdminBundle:Appdetails')
                            ->findOneBy(array('is_deleted' => 0, 'app_type_id' => $app_type_master->getApp_type_id(), "domain_id" => $domain_id, "status" => 'active'));

                    if (!empty($app_details)) {
                        /* if($app_id == '1')
                          {
                          // Customer App
                          $headers = array(
                          'Authorization: key=#',
                          'Content-Type: application/json'
                          );
                          }
                          elseif($app_id == '2')
                          {

                          // Delivery App
                          $headers = array(
                          'Authorization: key=#',
                          'Content-Type: application/json'
                          );
                          } */
                        if (!empty($app_details->getApp_gcm_key()) && $app_details->getApp_gcm_key() != "") {
                            $headers = array(
                                'Authorization: key=' . $app_details->getApp_gcm_key(),
                                'Content-Type: application/json'
                            );

                            $data1 = json_decode($message);
                            $response = "";
                            if (isset($data1->response) && !empty($data1->response)) {
                                $response = print_r($data1->response, 1);
                            } else {
                                $response = "";
                            }

                            $code = "";
                            if (isset($data1->code) && !empty($data1->code)) {
                                $code = $data1->code;
                            } else {
                                $code = "";
                            }

                            $registration_ids_array = $registration_ids;
                            if (is_array($registration_ids)) {
                                $registration_ids = implode("','", $registration_ids);
                            } else {
                                $registration_ids = "";
                            }

                            $detail = "";
                            if (isset($data1->detail) && !empty($data1->detail)) {
                                $detail = $data1->detail;
                            } else {
                                $detail = "";
                            }
                            $em = $this->getDoctrine()->getManager();
                            //$registration_ids_array = '';

                            if (!empty($registration_ids_array)) {
                                foreach ($registration_ids_array as $val) {

                                    $apppushnotificationmaster = new Apppushnotificationmaster();
                                    $apppushnotificationmaster->setDevice_name('android');
                                    $apppushnotificationmaster->setApp_id($app_id);
                                    $apppushnotificationmaster->setDomain_id($domain_id);
                                    $apppushnotificationmaster->setDevice_token($val);

                                    $connection = $em->getConnection();
                                    $gcm = $connection->prepare("SELECT * FROM gcm_user WHERE gcm_regid = '" . $val . "' and is_deleted=0 ORDER BY gcm_user_id DESC");
                                    $gcm->execute();
                                    $gcm_user = $gcm->fetchAll();
                                    if (!empty($gcm_user)) {
                                        $user_id = $gcm_user[0]['user_id'];
                                        $device_id = $gcm_user[0]['device_id'];

                                        $em = $this->getDoctrine()->getManager();
                                        $user_setting = $em->getRepository('AdminBundle:Usersetting')->findOneBy(array("user_id" => $user_id, "is_deleted" => 0));

                                        $value = json_decode($user_setting->getSetting_value(), true);

                                        $lang_id = $value['language'];

                                        $apppushnotificationmaster->setUser_id($user_id);
                                        $apppushnotificationmaster->setLanguage_id($lang_id);
                                        $apppushnotificationmaster->setDevice_id($device_id);
                                    }

                                    $apppushnotificationmaster->setData($detail);
                                    $apppushnotificationmaster->setCode($code);
                                    $apppushnotificationmaster->setTable_name($tablename);
                                    $apppushnotificationmaster->setTable_id($tabledataid);
                                    $apppushnotificationmaster->setResponse($response);
                                    $apppushnotificationmaster->setDatetime(date("Y-m-d H:i:s"));
                                    $apppushnotificationmaster->setIs_deleted(0);
                                    $em = $this->getDoctrine()->getManager();
                                    $em->persist($apppushnotificationmaster);
                                    $em->flush();
                                }
                            }
                            // Open connection
                            $ch = curl_init();

                            // Set the url, number of POST vars, POST data
                            curl_setopt($ch, CURLOPT_URL, $URL);
                            curl_setopt($ch, CURLOPT_POST, true);
                            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                            // Disabling SSL Certificate support temporarly
                            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
                            $txtf = "android_push_file.txt";
                            if (!file_exists($txtf)) {
                                fopen($txtf, 'w');
                            }
                            //-----write file to track error---------
                            // Open the file to get existing content
                            $current = file_get_contents($txtf);
                            // Append a new person to the file
                            $current .= http_build_query($fields);
                            // Write the contents back to the file
                            file_put_contents($txtf, $current);

                            // Execute post


                            $result = curl_exec($ch);

                            if ($result === FALSE) {
                                die('Curl failed: ' . curl_error($ch));
                            }
                            // Close connection
                            curl_close($ch);
                        }
                    }
                }

                break;
        }

        return $result;
    }

    /*
      get GCM user device / @param int $user_id / @return mixed
     */

    public function find_gcm_regid($user_id) {

        /*

          if (is_array($user_id)) {

          $user_id = implode("','", $user_id);

          }

         */

        $repository = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Gcmuser');



        $gcm_users = $repository->createQueryBuilder()
                        ->field('is_deleted')->equals("0")
                        ->field('userid')->in($user_id)
                        ->getQuery()->execute()->toArray();



        if (!empty($gcm_users)) {

            foreach ($gcm_users as $g_user) {

                if ($g_user->getGcmRegid() != "") {

                    $reg_ids [] = $g_user->getGcmRegid();
                }
            }

            return $reg_ids;
        } else {

            return false;
        }
    }

    function get_hirerachy($lang_id, $parent_hieraerchy_id, $current_category_id) {
        $child_data = "";
        $domain_id = $this->get('session')->get('domain_id');
        $em = $this->getDoctrine()->getManager();
        
        $single_category = $em
                ->getRepository('AdminBundle:Categorymaster')
                ->findOneBy(array('is_deleted' => "0", 'main_category_id' => $current_category_id, "language_id" => $lang_id));

        $all_sub_category = $em
                ->getRepository('AdminBundle:Categorymaster')
                ->findBy(array('is_deleted' => "0", 'parent_category_id' => $single_category->getMainCategoryId(), "language_id" => $lang_id));

        if (count($all_sub_category) == 0) {
            if (!empty($single_category)) {
                $parent_category_name = $single_category->getCategoryName();
            }
            $data = array(
                "category_master_id" => $single_category->getId(),
                "category_name" => $single_category->getCategoryName(),
                "parent_category_id" => $single_category->getParentCategoryid(),
                "parent_category_name" => $parent_category_name,
                "category_description" => strip_tags($single_category->getCategorydescription()),
                "main_category_id" => $single_category->getMaincategoryid(),
                "category_image_id" => $single_category->getCategoryImageId(),
                "language_id" => $single_category->getLanguage_id(),
                "child_data" => null
            );
        } else {
            if (!empty($single_category)) {
                $parent_category_name = $single_category->getCategoryName();
            }
            $data_temp[] = array(
                "category_master_id" => $single_category->getId(),
                "category_name" => $single_category->getCategoryName(),
                "parent_category_id" => $single_category->getParentCategoryid(),
                "parent_category_name" => $parent_category_name,
                "category_description" => strip_tags($single_category->getCategorydescription()),
                "main_category_id" => $single_category->getMaincategoryid(),
                "category_image_id" => $single_category->getCategoryImageId(),
                "language_id" => $single_category->getLanguage_id(),
                "classname" => "cat " . $single_category->getMainCategoryId()
            );

            $data_child = '';
            if (count($all_sub_category) > 0) {
                foreach (array_slice($all_sub_category, 0) as $lkey => $lval) {
                    $parent_category_name = 'No Parent ';

                    if ($lval->getParentCategoryId() == 0) {
                        
                    } else {

                        $data_child[] = $this->get_hirerachy($lang_id, $lval->getParentCategoryId(), $lval->getMainCategoryId());
                    }
                }
            }

            $data = array(
                "category_master_id" => $data_temp[0]['category_master_id'],
                "category_name" => $data_temp[0]['category_name'],
                "parent_category_id" => $data_temp[0]['parent_category_id'],
                "parent_category_name" => $data_temp[0]['parent_category_name'],
                "category_description" => $data_temp[0]['category_description'],
                "main_category_id" => $data_temp[0]['main_category_id'],
                "category_image_id" => $data_temp[0]['category_image_id'],
                "language_id" => $data_temp[0]['language_id'],
                "child_data" => $data_child,
                "classname" => $data_temp[0]['classname']
            );
        }
        return $data;
    }

    public function getprofilename($session_code) {
        $query = "SELECT COUNT(quiz_answer_user_id) as cnt ,option_profile_id FROM `quiz_answer_user` WHERE session = " . $session_code . " GROUP by option_profile_id ORDER by cnt DESC";
        $em = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();
        $statement = $connection->prepare($query);
        $statement->execute();
        $quizanswer = $statement->fetchAll();

        $query = "SELECT * FROM `profilemaster` where is_deleted = 0 and profilemaster_id=" . $quizanswer[0]["option_profile_id"];
        $statement = $connection->prepare($query);
        $statement->execute();
        $profile = $statement->fetchAll();
        if (!empty($profile)) {

            return $profile[0]["profile_name"];
        } else {
            return '';
        }
    }

    protected $payu_params = [];

    private function check_params() {
        if (empty($this->payu_params['key']))
            return $this->error('key');
        if (empty($this->payu_params['txnid']))
            return $this->error('txnid');
        if (empty($this->payu_params['amount']))
            return $this->error('amount');
        if (empty($this->payu_params['firstname']))
            return $this->error('firstname');
        if (empty($this->payu_params['email']))
            return $this->error('email');
        if (empty($this->payu_params['phone']))
            return $this->error('phone');
        if (empty($this->payu_params['productinfo']))
            return $this->error('productinfo');
        if (empty($this->payu_params['surl']))
            return $this->error('surl');
        if (empty($this->payu_params['furl']))
            return $this->error('furl');

        return true;
    }

    public static function get_hash($params, $salt) {
        $posted = array();

        if (!empty($params))
            foreach ($params as $key => $value)
                $posted[$key] = htmlentities($value, ENT_QUOTES);

        $hash_sequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";

        $hash_vars_seq = explode('|', $hash_sequence);
        $hash_string = null;

        foreach ($hash_vars_seq as $hash_var) {
            $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
            $hash_string .= '|';
        }


        $hash_string .= $salt;
        return strtolower(hash('sha512', $hash_string));
    }

    public static function curl_call($url, $data) {

        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_POST => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_USERAGENT => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36',
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0));

        $o = curl_exec($ch);

        if (curl_errno($ch)) {
            $c_error = curl_error($ch);

            if (empty($c_error))
                $c_error = 'Server Error';

            return array('curl_status' => 0, 'error' => $c_error);
        }

        $o = trim($o);
        return array('curl_status' => 1, 'result' => $o);
    }

    public function pay_page($params, $salt) {

        if (is_array($params)) {
            foreach ($params as $key => $value) {
                $this->payu_params[$key] = $value;
            }
        }

        $result = $this->pay($params, $salt);

        return $result;
    }

    public function pay($params = null) {

        $this->payu_params['hash'] = $this->get_hash($this->payu_params, $this->salt);
        $result = $this->curl_call($this->payu_server_url . '_payment?type=merchant_txn', http_build_query($this->payu_params));


        $transaction_id = ($result['curl_status'] === $this->SUCCESS) ? $result['result'] : null;
        if (empty($transaction_id)) {
            return array(
                'status' => $this->FAILURE,
                'data' => $result['error']
            );
        }

        return array(
            'status' => $this->SUCCESS,
            'data' => $this->payu_server_url . '_payment_options?mihpayid=' . $transaction_id
        );
    }

   public function object_to_array($data)
    {
        if (is_array($data) || is_object($data))
        {
            $result = array();
            foreach ($data as $key => $value)
            {
                $result[$key] = $this->object_to_array($value);
            }
            return $result;
        }
        return $data;
    }

    public function firequery($query) {
        $em = $this->getDoctrine()->getManager()->getConnection()->prepare($query);
        $em->execute();
        $data = $em->fetchAll();
        return $data;
    }
}