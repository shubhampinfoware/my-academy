<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\PropertyAccess\PropertyAccess;
use AdminBundle\Document\Coursetrainermaster;
use AdminBundle\Document\usermaster;
use AdminBundle\Document\user_register_enroll;
use AdminBundle\Document\fideuser;
use AdminBundle\Document\usertrainerrelation;
use AdminBundle\Document\user_package_relation;
use AdminBundle\Document\coupon_assigned_to_user;
use AdminBundle\Document\wallet_master;

/**
 * @Route("/")
 */
class EnrollController extends BaseController {

    public function __construct() {
        
    }

    /**
     * @Route("/userenrolllist")
     * @Template()
     */
    public function indexAction() {

        $data = null;
        return array("usermaster" => $data);
    }

    /**
     * @Route("/adduserenroll/{userid}")
     * @Template()
     */
    public function adduserenrollAction($userid) {
        $response = null;
        $em = $this->getDoctrine()->getManager();
        $enrolled = $em->getRepository('AdminBundle:Userregisterenroll')
                ->findBy(array('is_deleted' => "0", "extra2" => $userid));
        
        $userInfo = $em->getRepository('AdminBundle:Usermaster')
                ->find($userid);
        $userName = "";
        if($userInfo){
            $userName = " ( " . $userInfo->getUser_first_name() . " " . $userInfo->getUser_last_name() . " ) ";
        }
        foreach ($enrolled as $key => $value) {
            $user_id = $value->getAssignUserId();
            $enroll_id = $value->getId();
            $coupan_code = $value->getExtra1();
            $status = "";
            if (!empty($coupan_code)) {
                $coupon_master = $em->getRepository('AdminBundle:Couponmaster')
                        ->findOneBy(array('is_deleted' => "0", 'coupon_code' => $coupan_code));
                if (!empty($coupon_master)) {
                    if ($coupon_master->getStart_date() <= date("Y-m-d H:i:s") && $coupon_master->getEnd_date() >= date("Y-m-d H:i:s")) {
                        $coupan_id = $coupon_master->getId();
                    } else {
                        $status = "Coupon Expired";
                    }
                } else {
                    $status = "Coupon Expired";
                }
            }
            if ($value->gsetEnrollment_status() == "Approved") {

                $enrolled = $em->getRepository('AdminBundle:Userregisterenroll')
                        ->findOneBy(array("id" => $enroll_id));
                $usermaster = $em->getRepository('AdminBundle:usermaster')
                        ->findOneBy(array('is_deleted' => "0", "id" => $user_id));
                $response[] = array("enroll" => $enrolled, "usermaster" => $usermaster, "status" => "");
            } else {
                $enrolled = $em->getRepository('AdminBundle:Userregisterenroll')
                        ->findOneBy(array("id" => $enroll_id));
                $usermaster = $em->getRepository('AdminBundle:usermaster')
                        ->findOneBy(array('is_deleted' => "0", "id" => $user_id));
                $response[] = array("enroll" => $enrolled, "usermaster" => $usermaster, "status" => $status);
            }
        }

        return array(
            "enrolled" => $response,
            "userid" => $userid,
            "userName"=>$userName
        );
    }

    /**
     * @Route("/saveuserenroll")
     * @Template()
     */
    public function saveuserenrollAction() {
        $em = $this->getDoctrine()->getManager();
        $enroll = $_REQUEST["enroll"];
        $userid = $_REQUEST["userid"];
        $coupan_code = $_REQUEST["coupan_code"];
        $coupan_id = '';
        if (!empty($coupan_code)) {
            $coupon_master = $em->getRepository('AdminBundle:Couponmaster')
                    ->findOneBy(array('is_deleted' => "0", 'coupon_code' => $coupan_code));

            if (empty($coupon_master)) {
                $this->get("session")->getFlashBag()->set("error_msg", "Invalid coupon code");
                return $this->redirect($this->generateUrl("admin_enroll_adduserenroll", array("userid" => $userid)));
            } else {
                if ($coupon_master->getStart_date() > date("Y-m-d H:i:s")) {
                    $this->get("session")->getFlashBag()->set("error_msg", "coupon is not start yet, coupon start date is " . $coupon_master->getStart_date());
                    return $this->redirect($this->generateUrl("admin_enroll_adduserenroll", array("userid" => $userid)));
                }
                if ($coupon_master->getStart_date() <= date("Y-m-d H:i:s") && $coupon_master->getEnd_date() >= date("Y-m-d H:i:s")) {
                    $coupan_id = $coupon_master->getId();
                } else {
                    $this->get("session")->getFlashBag()->set("error_msg", " coupon code Expired");
                    return $this->redirect($this->generateUrl("admin_enroll_adduserenroll", array("userid" => $userid)));
                }
                if ($coupon_master->getNo_of_user_use() >= $enroll) {
                    $total = $coupon_master->getNo_of_user_use();
                    $totalse = $total - $enroll;
                    $coupon_master->setNo_of_user_use($totalse);
                    $em->flush();
                } else {
                    $this->get("session")->getFlashBag()->set("error_msg", " coupon usage limit exceeded");
                    return $this->redirect($this->generateUrl("admin_enroll_adduserenroll", array("userid" => $userid)));
                }
            }
        }
        $var = 1;
        $user_id = $this->get('session')->get('user_id');
        for ($i = 0; $i < $enroll; $i++) {
            $var = $var + 1;
            $data1 = substr(md5((time() - 1) . $var), 0, 8);
            $user_register_enroll = new user_register_enroll();
            $user_register_enroll->setEnrollmentCode($data1);
            $user_register_enroll->setCreated_by($user_id);
            $user_register_enroll->setCreated_datetime(date("Y-m-d H:i:s"));
            $user_register_enroll->setAssignUserId("0");
            $user_register_enroll->setAssign_user_datetime("");
            $user_register_enroll->setAssign_by("0");
            $user_register_enroll->ssetEnrollment_status("pending");
            $user_register_enroll->setExtra1($coupan_code);
            $user_register_enroll->setExtra2($userid);
            $user_register_enroll->setIs_deleted("0");
            $em->persist($user_register_enroll);
            $em->flush();
        }

        $this->get("session")->getFlashBag()->set("success_msg", "Enrollerd Generated successfully");
        return $this->redirect($this->generateUrl("admin_enroll_adduserenroll", array("userid" => $userid)));
    }

    /**
     * @Route("/assignuser/{userid}/{enroll_id}")
     * @Template()
     */
    public function assignuserAction($userid, $enroll_id) {
        $em = $this->getDoctrine()->getManager();
        $enrolled = $em->getRepository('AdminBundle:Userregisterenroll')
                ->findOneBy(array('is_deleted' => "0", "extra2" => $userid, "id" => $enroll_id));
        $user_id = $enrolled->getAssignUserId();

        $usermaster = $em->getRepository('AdminBundle:usermaster')
                ->findOneBy(array('is_deleted' => "0", "id" => $user_id));
        return array("enrolled" => $enrolled, "usermaster" => $usermaster, "userid" => $userid);
    }

    /**
     * @Route("/saveassignuser")
     * @Template()
     */
    public function saveassignuserAction() {
        $loginuser_id = $this->get('session')->get('user_id');
        $enroll_id = $_REQUEST["enroll_id"];
        $mobile_no = $_REQUEST["mobile"];
        $userid = $_REQUEST["userid"];
        $em = $this->getDoctrine()->getManager();
        $userDetail = $em->getRepository("AdminBundle:usermaster")
                ->findOneBy(array(
            "user_mobile" => $mobile_no,
                )
                );
        if (!empty($userDetail)) {
            $this->get("session")->getFlashBag()->set("error_msg", "User Already Registered");
            return $this->redirect($this->generateUrl("admin_enroll_adduserenroll", array("userid" => $userid)));
        }
        $enrolled = $em->getRepository('AdminBundle:Userregisterenroll')
                ->findOneBy(array('is_deleted' => "0", "id" => $enroll_id));
        if ($enrolled->gsetEnrollment_status() == "pending") {




            $usermaster = new usermaster();
            $usermaster->setUserFirstName("");
            $usermaster->setUserLastName("");
            $usermaster->setUserEmailPassword("");
            $usermaster->setUserPassword(md5($mobile_no));
            $usermaster->setUserMobile($mobile_no);
            $usermaster->setUserStatus("active");

            $usermaster->setUser_role_id($this->LEARNER_ROLE_ID);
            $usermaster->setDomain_id("1");
            $usermaster->setCreate_date(date('Y-m-d H:i:s'));
            $usermaster->setLogindate(date('Y-m-d H:i:s'));
            $usermaster->setBirthdate(date('Y-m-d H:i:s'));
            $usermaster->setExtField("true");
            $usermaster->setExtField1("0");
            $usermaster->setIs_deleted("0");

            $em->persist($usermaster);

            $fideuser = new fideuser();
            $fideuser->setUser_id($usermaster->getUsermaster_id());
            $fideuser->setFide_id("0");
            $fideuser->setIs_deleted("0");
            $em->persist($fideuser);

            $usertrainerrelation = new usertrainerrelation();
            $usertrainerrelation->setUser_id($usermaster->getUsermaster_id());
            $usertrainerrelation->setIs_chessbase("0");
            $usertrainerrelation->setIs_approved("0");
            $usertrainerrelation->setIs_deleted("0");

            $em->persist($usertrainerrelation);
            $em->flush();
            //-----------------------
            $wallet_code = $usermaster->getUsermaster_id() ."_".substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 5)), 0, 5);
            $wallet_master = new wallet_master();
            $wallet_master->setWallet_code($wallet_code);
            $wallet_master->setUser_master_id($usermaster->getUsermaster_id());
            $wallet_master->setVirtual_money_balance(0);
            $wallet_master->setActual_money_balance(0);
            $wallet_master->setCashback_balance(0);
            $wallet_master->setWallet_points(0);
            $wallet_master->setLast_updated_wallet_datetime(date("Y-m-d H:i:s"));
            $wallet_master->setWallet_member_tag_id(1);
            $wallet_master->setIs_deleted(0);
            $em->persist($wallet_master);
            $em->flush();
            //--------------------------------------------------
            $coupan_code = $enrolled->getExtra1(); //coupan code
            if (!empty($coupan_code)) {
                $coupon_master = $em->getRepository('AdminBundle:Couponmaster')
                        ->findOneBy(array('is_deleted' => "0", 'coupon_code' => $coupan_code));
                if (!empty($coupon_master)) {
                    $coupan_id = $coupon_master->getId();
                    $coupon_applied_list = $em->getRepository('AdminBundle:Coupon_applied_to_object_list')
                            ->findOneBy(array('is_deleted' => "0", 'coupon_id' => $coupan_id));
                    if (!empty($coupon_applied_list)) {
                        $subscription_ids = $coupon_applied_list->getRelation_id();

                        foreach ($subscription_ids as $key => $value) {
                            $subscription_master_rel = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Userpackagerelation')->findBy(array(
                                "user_id" => $usermaster->getUsermaster_id(),
                                "subscription_id" => $value,
                                "is_deleted" => "0"
                            ));
                            if (empty($subscription_master_rel)) {
                                $user_package_relation = new user_package_relation();
                                $user_package_relation->setUser_id($usermaster->getUsermaster_id());
                                $user_package_relation->setPackageId($value);
                                $user_package_relation->setCreated_datetime(date("Y-m-d H:i:s"));
                                $user_package_relation->setIsArchieved("");
                                $user_package_relation->setIs_deleted("0");
                                $em->persist($user_package_relation);
                                $em->flush();
                            }
                        }
                       
                    }
                     $coupon_assigned_to_user = new coupon_assigned_to_user();
                        $coupon_assigned_to_user->setUser_id($usermaster->getUsermaster_id());
                        $coupon_assigned_to_user->setCoupon_id($coupan_id);
                        $coupon_assigned_to_user->setAssign_datetime(date("Y-m-d H:i:s"));
                        $coupon_assigned_to_user->setAssign_by($enrolled->getAssignUserId());
                        $coupon_assigned_to_user->setUse_before_days("");
                        $coupon_assigned_to_user->setIs_deleted("0");
                        $em->persist($coupon_assigned_to_user);
                        $em->flush();
                }
            }
            $enrolled->setAssignUserId($usermaster->getUsermaster_id());
            $enrolled->setAssign_user_datetime(date("Y-m-d H:i:s"));
            $enrolled->setAssign_by($loginuser_id);
            $enrolled->ssetEnrollment_status("Approved");
            $em->flush();

          
            $this->get("session")->getFlashBag()->set("success_msg", "Enrollerd Assigned successfully");
            return $this->redirect($this->generateUrl("admin_enroll_adduserenroll", array("userid" => $userid)));
        } else {
            $user_id = $enrolled->getAssignUserId();
            $usermaster = $em->getRepository('AdminBundle:usermaster')
                    ->findOneBy(array('is_deleted' => "0", "id" => $user_id));
            if (!empty($usermaster)) {
                $usermaster->setUserMobile($mobile_no);
                $em->flush();
                // check Wallet Created or not 
                $repository = $this->getDoctrine()->getManager()->getRepository('AdminBundle:wallet_master');
                $user_wallet = $repository->findOneBy(array('is_deleted' => "0",'user_master_id' => $user_id));

                if(empty($user_wallet)){
            
                    $wallet_code = $usermaster->getUsermaster_id() ."_".substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 5)), 0, 5);
                    $wallet_master = new wallet_master();
                    $wallet_master->setWallet_code($wallet_code);
                    $wallet_master->setUser_master_id($usermaster->getUsermaster_id());
                    $wallet_master->setVirtual_money_balance(0);
                    $wallet_master->setActual_money_balance(0);
                    $wallet_master->setCashback_balance(0);
                    $wallet_master->setWallet_points(0);
                    $wallet_master->setLast_updated_wallet_datetime(date("Y-m-d H:i:s"));
                    $wallet_master->setWallet_member_tag_id(1);
                    $wallet_master->setIs_deleted(0);
                    $em->persist($wallet_master);
                    $em->flush();
                }
            //-------------------------------------
                $this->get("session")->getFlashBag()->set("success_msg", "Enrollerd Updated successfully");
            } else {
                $this->get("session")->getFlashBag()->set("error_msg", "User is deactivted from Database");
            }


            return $this->redirect($this->generateUrl("admin_enroll_adduserenroll", array("userid" => $userid)));
        }
    }

}
