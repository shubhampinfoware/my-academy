<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use AdminBundle\Entity\Usermaster;

use Google_Client;
use Google_Service_Oauth2;

/**
 * @Route("/")
 */
class DefaultController extends BaseController
{
	/**
	 * @Route("/")
	 * @Template("@Admin/Default/index.html.twig")
	 */
	public function indexAction()
	{
		// to get login url

		//return $this->render('@Admin/default/index.html.twig');
		return array("url" => 0);
	}

	/**
	 * @Route("/superadmin")
	 * @Template("@Admin/Default/superadmin.html.twig")
	 */
	public function superadminAction()
	{
		return array();
	}


	/**
	 * @Route("/logincheck")
	 * @Template()
	 */
	public function logincheckAction(Request $request)
	{
		if ($request->get('type') == null) {
			$crrf_token = $request->get('_csrf_token');
			if (isset($crrf_token) && $crrf_token != "") {

				if ($request->get('username') != "" && $request->get('password') != "") {
					$username = $this->bwiz_security($request->get('username'));
					$password = $this->bwiz_security($request->get('password'));

					$repository = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Usermaster');
					$user = $repository->findOneBy(
						array(
							'user_email_password' => $username,
							'user_password' => md5($password),
							'is_deleted' => "0"
						)
					);
					//var_dump($user);exit;
					// check domain

					$domain_check = '';


					if (!empty($user) && count($user) == 1 && $user != "" && $user != NULL) {
						$role = null;
						$em = $this->getDoctrine()->getManager();
						$role = $em->getRepository('AdminBundle:Rolemaster')
							->find(
								$user->getUser_role_id()
							);

						$this->get('session')->set('user_id', $user->getUsermaster_id());
						$this->get('session')->set('username', $user->getUser_email_password());
						$this->get('session')->set('domain_id', $user->getDomain_id());
						$this->get('session')->set('email', $user->getUser_email_password());
						$this->get('session')->set('role_id', $user->getUser_role_id());
						$this->get('session')->set('role', $role->getRolename());
						$this->get('session')->set("domain", "myacademy");
						$this->get('session')->getFlashBag()->set('success_msg', 'Login successfully');

						return $this->redirect($this->generateUrl('admin_dashboard_index', array('domain' => $this->get('session')->get('domain'))));
					} else {
						$this->get('session')->getFlashBag()->set('error_msg', 'Username or password or Domain is wrong');
					}
				} else {
					$this->get('session')->getFlashBag()->set('error_msg', 'Username and password is required');
				}
			} else {
				$this->get('session')->getFlashBag()->set('error_msg', 'Oops! Something goes wrong! Try again later');
			}
			return $this->redirect($this->generateUrl('admin_default_index'));
		} else {
			$OAUTH2_CLIENT_ID = '255955648270-82cmt4s1njh8kfpg9bjj9r6sajs4nfbu.apps.googleusercontent.com';
			$OAUTH2_CLIENT_SECRET = 'PUz4eV_Imh1ucAAvykANgc31';
			$client = new \Google_Client();
			$client->setApplicationName("New Project"); // to set app name
			$client->setClientId($OAUTH2_CLIENT_ID); // to set app id or client id
			$client->setClientSecret($OAUTH2_CLIENT_SECRET); // to set app secret or client secret
			$tokenSessionKey = 'token-' . $client->prepareScopes();
			$redirect = filter_var(
				'http://' . $_SERVER['HTTP_HOST'] . '/chesscoaching/logincheck?type=' . $request->get('type'),
				FILTER_SANITIZE_URL
			);
			$client->setRedirectUri($redirect); // to set redirect uri

			$service = new Google_Service_Oauth2($client);

			$value = $client->authenticate($_REQUEST["code"]);

			$client->setAccessToken($value);


			$this->get('session')->set('token', $client->getAccessToken());

			$date_time = date("Y-m-d H:i:s");
			$gpUserProfile = $service->userinfo->get();
			$repository = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Usermaster');
			$usermaster = $repository->findOneBy(
				array(
					'user_email_password' => $gpUserProfile["email"],
					'user_status' => 'Active',
					'is_deleted' => 0
				)
			);

			if (!isset($usermaster)) {
				$usermaster = new usermaster();
				$password = rand();
				$md5password = md5($password);
				$usermaster->setUserFirstName($gpUserProfile["familyName"]);
				$usermaster->setUserLastName($gpUserProfile["givenName"]);

				$usermaster->setUserEmailPassword($gpUserProfile["email"]);
				$usermaster->setUserMobile("");
				$usermaster->setUserPassword($md5password);
				$usermaster->setUserStatus("Active");
				$usermaster->setUser_role_id($request->get('type'));
				$usermaster->setDomain_id(1);
				$usermaster->setCreate_date($date_time);
				$usermaster->setLogindate($date_time);

				$usermaster->setLanguage_id(1);
				$usermaster->setExtField(1);
				$usermaster->setExtField1(1);
				$usermaster->setIs_deleted(0);
				$em = $this->getDoctrine()->getManager();
				$em->persist($usermaster);
				$em->flush();
			} else {
				$role_id = $usermaster->getUser_role_id();
				if ($role_id != $request->get('type')) {
					$usermaster->setUser_role_id($request->get('type'));
					$em = $this->getDoctrine()->getManager();
					$em->persist($usermaster);
					$em->flush();
				}
			}
			$em = $this->getDoctrine()->getManager();
			$role = $em->getRepository('AdminBundle:Rolemaster')
				->find(
					$usermaster->getUser_role_id()
				);

			$this->get('session')->set('user_id', $usermaster->getUsermaster_id());
			$this->get('session')->set('username', $usermaster->getUser_email_password());
			$this->get('session')->set('domain_id', $usermaster->getDomainId());
			$this->get('session')->set('email', $usermaster->getUser_email_password());
			$this->get('session')->set('role_id', $usermaster->getUser_role_id());
			$this->get('session')->set('role', $role->getRolename());
			$this->get('session')->set("domain", "myacademy");

			$this->get('session')->getFlashBag()->set('success_msg', 'Login successfully');

			return $this->redirect($this->generateUrl('admin_dashboard_index', array('domain' => $this->get('session')->get('domain'))));

			var_dump($gpUserProfile);
			die;
		}
	}

	/**
	 * @Route("/logout")
	 */
	public function logoutAction()
	{
		$this->get('session')->remove('role_id');
		$this->get('session')->remove('role');
		$this->get('session')->remove('user_id');
		$this->get('session')->remove('domain_id');
		$this->get('session')->remove('username');
		$this->get('session')->remove('domain');
		$this->get('session')->remove('token');
		$this->get('session')->getFlashBag()->set('success_msg', 'Logout successfully');

		return $this->redirect($this->generateUrl('admin_default_index'));
	}



	/**
	 * @Route("/url")
	 */
	public function urlAction()
	{
		$client = new Google_Client();
		$OAUTH2_CLIENT_ID = '255955648270-82cmt4s1njh8kfpg9bjj9r6sajs4nfbu.apps.googleusercontent.com';
		$OAUTH2_CLIENT_SECRET = 'PUz4eV_Imh1ucAAvykANgc31';

		$client->setApplicationName("New Project"); // to set app name
		$client->setClientId($OAUTH2_CLIENT_ID); // to set app id or client id
		$client->setClientSecret($OAUTH2_CLIENT_SECRET); // to set app secret or client secret

		$redirect = filter_var(
			'http://' . $_SERVER['HTTP_HOST'] . '/chesscoaching/logincheck?type=' . $_REQUEST["id"],
			FILTER_SANITIZE_URL
		);

		$client->setScopes('https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/youtube');
		$client->setRedirectUri($redirect); // to set redirect uri
		//$client->setHostedDomain(‘your hosted domain’);// to set hosted domain (optional)
		$url = $client->createAuthUrl();
		return new Response($url);
	}
}
