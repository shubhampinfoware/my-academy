<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\PropertyAccess\PropertyAccess;

use AdminBundle\Entity\Coursetrainermaster;
use AdminBundle\Entity\usermaster;
use AdminBundle\Entity\Userenrollments;
use AdminBundle\Entity\Userpackagerelation;
use AdminBundle\Entity\Transaction_master;
use AdminBundle\Entity\Subscriptionmaster;
use AdminBundle\Entity\Subscriptioncourserelation;

/**
 * @Route("/")
 */
class SubscriptionController extends BaseController {

    public function __construct() {
        
    }

    /**
     * @Route("/subscription")
     * @Template()
     */
    public function indexAction() {
        $subscription_master = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Subscriptionmaster')->findBy(array(
            "is_deleted" => "0"
        ));

        $subscriptionList = [];
        if(!empty($subscription_master)){
            foreach($subscription_master as $_subscription){
                $subscription_id = $_subscription->getSubscription_master_id();
                $_sql = "SELECT * from subscription_course_relation where subscription_id = {$subscription_id} and is_deleted = 0";
                $course_list = $this->firequery($_sql);

                $_subscription->no_of_courses = sizeof($course_list);

                $subscriptionList[] = $_subscription;
            }
        }

        return array("subscription_master" => $subscriptionList);
    }

    /**
     * @Route("/subscriptionuserinfo/{id}",defaults={"id":""})
     * @Template()
     */
    public function subscriptionuserinfoAction($id) {
        $em = $this->getDoctrine()->getManager();
        $user_info = $em->getRepository('AdminBundle:usermaster')->find($id);
        $user_data = [] ; 
        if($user_info){
            $user_data =array(
                "id"=>$id,
                "user_first_name"=>$user_info->getUser_first_name(),
                "user_last_name"=>$user_info->getUser_last_name(),
                "user_email_password"=>$user_info->getUser_email_password(),
                "user_mobile"=>$user_info->getUser_mobile(),
            );
        }
        $user_package_relation = $em->getRepository('AdminBundle:Userpackagerelation')
                ->findBy(array('is_deleted' => "0", 'user_id' => $id));
        $subscription_data = [];
        if (!empty($user_package_relation)) {
            
            foreach ($user_package_relation as $ukey => $uval) {
                $subscription_master = $this->getDoctrine()->getManager()->getRepository('AdminBundle:subscription_master')->findOneBy(array(
                    "is_deleted" => "0", "id" => $uval->getPackage_id()));

                if ($subscription_master) {
                    $transaction_info = $this->getDoctrine()->getManager()->getRepository('AdminBundle:transaction_master')->findOneBy(array(
                        "is_deleted" => "0", "product_id" => $uval->getPackage_id(), "user_id" => $id));
                    $transaction_id =  $payment_id = '' ;
                    $payment_type = "Online";
                    $payment_status = 'Pending';
            
                    if ($transaction_info) {
                        $transaction_id = $transaction_info->getId();
                        if (strtolower($transaction_info->getRef_number()) == "cash") {
                            $payment_type = "Cash";
                        } else {
                            $payment_type = "Online";
                        }
                        $payment_id = $transaction_info->getPayment_id();
                        $payment_status = $transaction_info->geStatus();
                    }

                    $dataSingle['payment_status'] = $uval->getPayment_status();
                    $dataSingle['package_id'] = $uval->getPackage_id();
                    $dataSingle['package_name'] = $subscription_master->getExtra();
                    $dataSingle['transaction_id'] = $transaction_id;
                    $dataSingle['payment_type'] = $payment_type;
                    $dataSingle['payment_id'] = $payment_id;
                    $dataSingle['payment_status'] = $payment_status;
                    $subscription_data[] = $dataSingle;
                }
            }
        }
        return array("subscription_master" => $subscription_data, "user_info" =>$user_data);
    }
    /**
     * @Route("/viewsubscribeduser/{id}",defaults={"id":""})
     * @Template()
     */
    public function viewsubscribeduserAction($id) {
        $em = $this->getDoctrine()->getManager();
        $subscription_info = $em->getRepository('AdminBundle:Subscriptionmaster')->find($id);
        $subscription_data = [] ; 
        if($subscription_info){
            $subscription_data =array(
                "id"=>$id,
                "name"=>$subscription_info->getExtra(),
                "price"=>$subscription_info->getPrice()
            );
        }
        $user_package_relation = $em->getRepository('AdminBundle:Userpackagerelation')
                ->findBy(array('is_deleted' => "0", 'package_id' => $id));
       
        $subscribed_user_data = [] ;
        if (!empty($user_package_relation)) {
            
            foreach ($user_package_relation as $ukey => $uval) {
                $user_info = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Usermaster')->find($uval->getUser_id());

                if ($user_info) {
                    $transaction_info = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Transactionmaster')->findOneBy(array(
                        "is_deleted" => "0", "product_id" => $uval->getPackage_id(), "user_id" => $uval->getUser_id()));
                    $transaction_id =  $payment_id = '' ;
                    $payment_type = "Online";
                    $payment_status = 'Pending';
            
                    if ($transaction_info) {
                        $transaction_id = $transaction_info->getTransaction_master_id();
                        if (strtolower($transaction_info->getRef_number()) == "cash") {
                            $payment_type = "Cash";
                        } else {
                            $payment_type = "Online";
                        }
                        $payment_id = $transaction_info->getPayment_id();
                        $payment_status = $transaction_info->getStatus();
                    }

                    $dataSingle['payment_status'] = $transaction_info->getStatus();
                    $dataSingle['package_id'] = $uval->getPackage_id();
                    $dataSingle['user_name'] = $user_info->getUser_first_name() . " " . $user_info->getUser_last_name();
                    $dataSingle['transaction_id'] = $transaction_id;
                    $dataSingle['payment_type'] = $payment_type;
                    $dataSingle['payment_id'] = $payment_id;
                    $dataSingle['created_datetime'] = $transaction_info->getCreated_datetime();
                    $dataSingle['payment_status'] = $payment_status;
                    $subscribed_user_data[] = $dataSingle;
                }
            }
        }

        return array("subscription_master" => $subscription_data, "subscribed_user_data" =>$subscribed_user_data);
    }

    /**
     * @Route("/addsubscription/{id}",defaults={"id":""})
     * @Template()
     */
    public function addsubscriptionAction($id) {
        $coursemaster = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Coursemaster')->findBy(array(
            "is_deleted" => "0"
        ));

        if (!empty($id)) {
            $subscription_master = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Subscriptionmaster')->find($id);

            return array("coursemaster" => $coursemaster, "subscription_master" => $subscription_master);
        }
        return array("coursemaster" => $coursemaster);
    }

    /**
     * @Route("/save/{id}",defaults={"id":""})
     * @Template()
     */
    public function saveAction($id) {
        $em = $this->getDoctrine()->getManager();
        $course = $_REQUEST["course"];
        $sub_desc = $_REQUEST["sub_desc"];
        $name = $_REQUEST["name"];
        $price = $_REQUEST["price"];
        $start = $_REQUEST["start"];
        $end = $_REQUEST["end"];
        $user_id = $this->get('session')->get('user_id');
        $media_id = "";
        if ($_REQUEST['save'] == 'save_category') {
            $media_id = "";
            //add image
            $category_logo = $_FILES['s_logo'];
            if (isset($_REQUEST['image_hidden'])) {
                $media_id = $_REQUEST['image_hidden'];

                if ($category_logo['name'] != "" && !empty($category_logo['name'])) {
                    $extension = pathinfo($category_logo['name'], PATHINFO_EXTENSION);

                    $media_type_id = $this->mediatype($extension);

                    if (!empty($media_type_id)) {
                        $logo = $category_logo['name'];
                        $tmpname = $category_logo['tmp_name'];
                        $file_path = $this->container->getParameter('file_path');
                        $logo_path = $file_path . '/category';
                        $logo_upload_dir = $this->container->getParameter('upload_dir') . '/category/';

                        $media_id = $this->mediauploadAction($logo, $tmpname, $logo_path, $logo_upload_dir, $media_type_id);
                    } else {
                        $this->get("session")->getFlashBag()->set("error_msg", "Category Logo is Required");
                        return $this->redirect($this->generateUrl("admin_course_addcourse", array("domain" => $this->get('session')->get('domain'), "course_id" => $course_id)));
                    }
                }
            } else {


                if ($category_logo['name'] != "" && !empty($category_logo['name'])) {
                    $extension = pathinfo($category_logo['name'], PATHINFO_EXTENSION);

                    $media_type_id = $this->mediatype($extension);

                    if (!empty($media_type_id)) {
                        $logo = $category_logo['name'];

                        $tmpname = $category_logo['tmp_name'];

                        $file_path = $this->container->getParameter('file_path');

                        $logo_path = $file_path . '/category';

                        $logo_upload_dir = $this->container->getParameter('upload_dir') . '/category/';

                        $media_id = $this->mediauploadAction($logo, $tmpname, $logo_path, $logo_upload_dir, $media_type_id);
                    } else {
                        $this->get("session")->getFlashBag()->set("error_msg", "Course Logo is Required or extension problem");
                        return $this->redirect($this->generateUrl("admin_course_addcourse", array("domain" => $this->get('session')->get('domain'))));
                    }
                }
            }
        }
        if (!empty($id)) {
            $subscription = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Subscriptionmaster')->find($id);
            $subscription->setCourse_id(0);
            $subscription->setCreated_by($user_id);
            $subscription->setPrice($price);
            $subscription->setStart_date($start);
            $subscription->setEnd_date($end);
            $subscription->setCreated_date(date("Y-m-d"));
            $subscription->setStatus("active");
            $subscription->setExtra($name);
            $subscription->setSubscription_desc($sub_desc);
            if ($media_id != "") {
                $subscription->setExtra1($media_id);
            }
            $subscription->setIs_deleted("0");

            $em->flush();

            // remove all courses
            $subscription_courses = $em->getRepository('AdminBundle:Subscriptioncourserelation')->findBy([
                'subscription_id' => $id,
                'is_deleted' => 0
            ]);

            if(!empty($subscription_courses)){
                foreach($subscription_courses as $_course){
                    $_course->setIs_deleted(1);
                    $em->flush();
                }
            }

            $subscription_id = $id;

        } else {
            $em = $this->getDoctrine()->getManager();
            $subscription = new Subscriptionmaster();
            $subscription->setCourse_id(0);
            $subscription->setCreated_by($user_id);
            $subscription->setPrice($price);
            $subscription->setStart_date($start);
            $subscription->setEnd_date($end);
            $subscription->setCreated_date(date("Y-m-d"));
            $subscription->setStatus("active");
            $subscription->setSubscription_desc($sub_desc);
            $subscription->setExtra($name);
            $subscription->setExtra1($media_id);
            $subscription->setIs_deleted(0);
            $em->persist($subscription);
            $em->flush();

            $subscription_id = $subscription->getSubscription_master_id();
        }

        foreach($course as $_courseId){
            $subscription_course_relation = new Subscriptioncourserelation();
            $subscription_course_relation->setSubscription_id($subscription_id);
            $subscription_course_relation->setCourse_id($_courseId);
            $subscription_course_relation->setCreated_date(date('Y-m-d H:i:s'));
            $subscription_course_relation->setIs_deleted(0);

            $em->persist($subscription_course_relation);
            $em->flush();
        }

        $this->get("session")->getFlashBag()->set("success_msg", "Subscription Added successfully");
        return $this->redirect($this->generateUrl("admin_subscription_index"));
    }

    /**
     * @Route("/deletesub/{id}",defaults={"id":""})
     * @Template()
     */
    public function deletesubAction($id) {
        $em = $this->getDoctrine()->getManager();
        if ($id != '0') {

            $selected_category = $em
                    ->getRepository('AdminBundle:subscription_master')
                    ->findOneBy(array('is_deleted' => "0", 'id' => $id));
            // var_dump($selected_category);exit;
            if (!empty($selected_category)) {

                $selected_category->setIs_deleted("1");

                $em->persist($selected_category);
                $em->flush();
            }
        }
        $this->get("session")->getFlashBag()->set("success_msg", " Deleted successfully");
        return $this->redirect($this->generateUrl("admin_subscription_index"));
    }

    /**
     * @Route("/assignuser/{userid}",defaults={"userid":""})
     * @Template()
     */
    public function assignuserAction() {
        $em = $this->getDoctrine()->getManager();
        $subscription_master = $this->getDoctrine()->getManager()->getRepository('AdminBundle:subscription_master')->findBy(array(
            "is_deleted" => "0"
        ));
        $user_id = $this->get('session')->get('user_id');
        $enrolled = $em->getRepository('AdminBundle:Userregisterenroll')
                ->findBy(array('is_deleted' => "0", "extra2" => $user_id, 'enrollment_status' => 'Approved'));
        $data = [];
        if (!empty($enrolled)) {
            foreach ($enrolled as $key => $value) {
                $usermaster = $this->getDoctrine()->getManager()->getRepository('AdminBundle:usermaster')->find($value->getAssignUserId());
                if (!empty($usermaster)) {
                    $data[] = array(
                        "firstname" => $usermaster->getUser_first_name() . " " . $usermaster->getUser_last_name(),
                        "mobile_no" => $usermaster->getUser_mobile(),
                        "email" => $usermaster->getUser_email_password(),
                        "role_name" => "",
                        "user_id" => $usermaster->getUsermaster_id(),
                        "create_date" => $usermaster->getCreate_date()
                    );
                }
            }
        }

        return array("subscription_master" => $subscription_master, "learners" => $data);
    }

    /**
     * @Route("/saveassigsubscriptionnuser")
     * @Template()
     */
    public function saveassigsubscriptionnuserAction() {
        $em = $this->getDoctrine()->getManager();
        $loguser_id = $this->get('session')->get('user_id');
        if (count($_REQUEST['user']) > 0) {
            $users = $_REQUEST['user'];
            $subscribed = $unsubscribed = 0;
            foreach ($users as $key => $value) {
                // check user assign or not 
                $user_package_relation = $em->getRepository('AdminBundle:Userpackagerelation')
                        ->findOneBy(array('is_deleted' => "0", "user_id" => $value, 'package_id' => $_REQUEST['subscription']));
                if (empty($user_package_relation)) {
                    $user_package_relation = new user_package_relation();
                    $user_package_relation->setUser_id($value);
                    $user_package_relation->setPackageId($_REQUEST['subscription']);
                    $user_package_relation->setCreated_datetime(date("Y-m-d H:i:s"));
                    $user_package_relation->setIsArchieved(0);
                    if ($_REQUEST['payment'] == 'online') {
                        $user_package_relation->setPayment_status('pending');
                    } else {
                        $user_package_relation->setPayment_status('sucess');
                    }
                    $user_package_relation->setIs_deleted(0);
                    $em->persist($user_package_relation);
                    $em->flush();
                    if ($_REQUEST['payment'] == 'cash') {
                        $transacation_master = new Transaction_master();
                        $transacation_master->setRef_number('Cash');
                        $transacation_master->setPayment_id($user_package_relation->getId());
                        $transacation_master->setProduct_id($_REQUEST['subscription']);
                        $transacation_master->setUser_id($value);
                        $transacation_master->setStatus('success');
                        $transacation_master->setUnmappedstatus('cash');
                        $transacation_master->setTxnid($user_package_relation->getId());
                        $transacation_master->setAmount($_REQUEST['amount']);
                        $transacation_master->setNameoncard('');
                        $transacation_master->setTxn_field_1('');
                        $transacation_master->setTxn_field_2('');
                        $transacation_master->setTxn_field_3('');
                        $transacation_master->setCreated_datetime(date("Y-m-d H:i:s"));
                        $transacation_master->setIs_deleted(0);
                        $em->persist($transacation_master);
                        $em->flush();
                    }
                    $subscribed++;
                } else {
                    $unsubscribed++;
                }
            }
            $this->get("session")->getFlashBag()->set("success_msg", $subscribed . " User(s) Assigned and " . $unsubscribed . " User(s) already Assgined ");
            return $this->redirect($this->generateUrl("admin_subscription_assignuser", array("user_id" => $loguser_id, "domain" => $this->get('session')->get('domain'))));
        }
        $this->get("session")->getFlashBag()->set("error_msg", "Please Select Learners");
        return $this->redirect($this->generateUrl("admin_subscription_assignuser", array("user_id" => $loguser_id, "domain" => $this->get('session')->get('domain'))));
    }

}
