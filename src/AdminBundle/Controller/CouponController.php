<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\PropertyAccess\PropertyAccess;
use AdminBundle\Document\Coursetrainermaster;
use AdminBundle\Document\usermaster;
use AdminBundle\Document\user_register_enroll;
use AdminBundle\Document\fideuser;
use AdminBundle\Document\usertrainerrelation;
use AdminBundle\Document\Couponmaster;
use AdminBundle\Document\Coupon_applied_to_object_list;
use AdminBundle\Document\coupon_assigned_to_user;

/**
 * @Route("/")
 */
class CouponController extends BaseController {

    public function __construct() {
        parent::__construct();
        //$obj = new BaseController();
        //$obj->checkSessionAction();
    }

    /**
     * @Route("/coupon")
     * @Template()
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();
        if ($this->get('session')->get('role_id') == '1') {
            $coupon_master = $em->getRepository('AdminBundle:Couponmaster')
                    ->findBy(array('is_deleted' => "0"));
        } else {
            $coupon_master = $em->getRepository('AdminBundle:Couponmaster')
                    ->findBy(array('is_deleted' => "0"));
        }
        return array("coupon_master" => $coupon_master);
    }

    /**
     * @Route("/addcoupon/{coupon_master_id}",defaults = {"coupon_master_id" = ""})
     * @Template()
     */
    public function addcouponAction($coupon_master_id) {
        $em = $this->getDoctrine()->getManager();
        if (isset($coupon_master_id) && $coupon_master_id != "") {
            $coupon_master = $em->getRepository('AdminBundle:Couponmaster')
                    ->findOneBy(array('id' => $coupon_master_id, 'is_deleted' => "0"));
            //var_dump($coupon_master);exit;			   
            return array("coupon_master" => $coupon_master, "coupon_master_id" => $coupon_master_id);
        }
        return array();
    }

    /**
     * @Route("/applycoupon/{coupon_master_id}",defaults = {"coupon_master_id" = ""})
     * @Template()
     */
    public function applycouponAction($coupon_master_id) {
        $em = $this->getDoctrine()->getManager();
        $coupon_master = $em->getRepository('AdminBundle:Couponmaster')
                ->findBy(array('is_deleted' => "0"));

        $coupon_applied_list = $em->getRepository('AdminBundle:Coupon_applied_to_object_list')
                ->findOneBy(array('is_deleted' => "0", 'coupon_id' => $coupon_master_id));

        //var_dump($coupon_applied_list);exit;
        $subscription_master = $em->getRepository('AdminBundle:subscription_master')->findBy(array("is_deleted" => "0"));
        if (!empty($subscription_master)) {
            foreach ($subscription_master as $pkey => $pval) {
                $image = '';
                $subscription_array[] = array(
                    "subscription_master_id" => $pval->getId(),
                    "product_name" => $pval->getExtra(),
                    "original_price" => $pval->getPrice(),
                    "status" => $pval->getStatus()
                );
            }            
        }
        $coupon_assigned_user =  $this->getDoctrine()->getManager()
                                ->getRepository('AdminBundle:coupon_assigned_to_user')
                                ->createQueryBuilder()
                                ->field('coupon_id')->equals($coupon_master_id)
                                ->field('is_deleted')->equals("0")->getQuery()->execute()->toArray();
        
        $assigned_users = [] ;
        if(!empty($coupon_assigned_user)){
            foreach($coupon_assigned_user as $ckey=>$cval){
                $assigned_users[] = $cval->getUser_id();
            }
        }
       // var_dump($assigned_users);exit;
        $repository = $this->getDoctrine()->getManager()->getRepository('AdminBundle:usermaster');
        $usermaster = $repository->createQueryBuilder()
                        ->field('user_role_id')
                        ->equals($this->LEARNER_ROLE_ID)
                        ->field('is_deleted')
                        ->equals("0")->sort('id', 'desc')->getQuery()->execute()->toArray();
        $user_master_data = [] ;
        if($usermaster){
            foreach($usermaster as $key=>$val){
                $user_master_data[] = array(
                    "id"=>$val->getId(),
                    "name"=>$val->getUser_first_name() . " " .$val->getUser_last_name(),
                    "email"=>$val->getUser_email_password(),
                    "mobile"=>$val->getUser_mobile()
                );
            }
        }
//var_dump($user_master_data);exit;
        return array("assigned_users"=>$assigned_users,"user_master_data"=>$user_master_data,"coupon_master" => $coupon_master, "product_list" => $subscription_array, "coupon_master_id" => $coupon_master_id, "coupon_applied_list" => $coupon_applied_list);
    }

    /**
     * @Route("/savecoupon/{coupon_master_id}",defaults={"coupon_master_id"=""})
     * @Template()
     */
    public function savecouponAction($coupon_master_id) {


        $em = $this->getDoctrine()->getManager();
        if (isset($_POST['save_coupon']) && $_POST['save_coupon'] == 'save_coupon') {
            if ($_POST['coupon_name'] != "" && $_POST['coupon_code'] != "" && $_POST['discount_type'] != "" && $_POST['discount_value'] != "" && $_POST['starting_date'] != "" && $_POST['ending_date'] != "" && $_POST['status'] != "" && $_POST['no_of_user_use'] != "" && $_POST['no_of_times_use'] != "" && $_POST['coupon_usage_interval'] != "") {
                $coupon_name = $_POST['coupon_name'];
                $coupon_code = $_POST['coupon_code'];
                $discount_type = $_POST['discount_type'];
                $return_type = $_POST['return_type'];
                $discount_value = $_POST['discount_value'];
                $start_date = date("Y-m-d H:i:s", strtotime($_POST['starting_date']));
                $end_date = date("Y-m-d H:i:s", strtotime($_POST['ending_date']));
                $status = $_POST['status'];
                $no_of_user_use = $_POST['no_of_user_use'];
                $no_of_times_use = $_POST['no_of_times_use'];
                $coupon_usage_interval = $_POST['coupon_usage_interval'];
                $min_amount = $_POST['min_amount'];


                if (isset($_POST['visible_all']) && !empty($_POST['visible_all'])) {
                    $visible_all = $_POST['visible_all'];
                } else {
                    $visible_all = 'no';
                }
                if (isset($coupon_master_id) && $coupon_master_id != "") {
                    $coupon_master = $em
                            ->getRepository("AdminBundle:Couponmaster")
                            ->findOneBy(array("id" => $coupon_master_id, "is_deleted" => "0"));
                    if (!empty($coupon_master)) {

                        $update = $em->getRepository('AdminBundle:Couponmaster')->find($coupon_master->getId());
                        $update->setCoupon_name($coupon_name);
                        $update->setCoupon_code($coupon_code);

                        $update->setStart_date($start_date);
                        $update->setEnd_date($end_date);

                        $update->setDiscount_value($discount_value);
                        $update->setDiscount_type($discount_type);
                        $update->setReturn_type($return_type);
                        $update->setNo_of_user_use($no_of_user_use);
                        $update->setNo_of_times_use($no_of_times_use);
                        $update->setCoupon_usage_interval($coupon_usage_interval);
                        $update->setMin_order_amount($min_amount);
                        $update->setCoupon_status($status);
                        $update->setVisible_all($visible_all);
                        $em->flush();
                        $this->get('session')->getFlashBag()->set('success_msg', 'Coupon Updated successfully.');
                        return $this->redirect($this->generateUrl('admin_coupon_addcoupon', array('domain' => $this->get('session')->get('domain'), 'coupon_master_id' => $coupon_master_id)));
                        // till here done
                    } else {

                        $couponmaster = new Couponmaster();
                        $couponmaster->setCoupon_name($coupon_name);
                        $couponmaster->setCoupon_code($coupon_code);
                        $couponmaster->setStart_date($start_date);
                        $couponmaster->setEnd_date($end_date);
                        $couponmaster->setDiscount_value($discount_value);
                        $couponmaster->setDiscount_type($discount_type);
                        $couponmaster->setReturn_type($return_type);
                        $couponmaster->setNo_of_user_use($no_of_user_use);
                        $couponmaster->setNo_of_times_use($no_of_times_use);
                        $couponmaster->setCoupon_usage_interval($coupon_usage_interval);
                        $couponmaster->setMin_order_amount($min_amount);
                        $couponmaster->setCoupon_status($status);
                        $couponmaster->setVisible_all($visible_all);
                        $couponmaster->setCreated_datetime(date('Y-m-d H:i:s'));
                        $couponmaster->setDomain_id(1);
                        $couponmaster->setIs_deleted(0);
                        //$em = $this->getDoctrine()->getManager();
                        $em->persist($couponmaster);
                        $em->flush();



                        $this->get('session')->getFlashBag()->set('success_msg', 'Coupon inserted successfully.');
                        return $this->redirect($this->generateUrl('admin_coupon_addcoupon', array('domain' => $this->get('session')->get('domain'), 'coupon_master_id' => $coupon_master_id)));
                    }
                } else {

                    $couponmaster = new Couponmaster();
                    $couponmaster->setCoupon_name($coupon_name);
                    $couponmaster->setCoupon_code($coupon_code);
                    $couponmaster->setStart_date($start_date);
                    $couponmaster->setEnd_date($end_date);
                    $couponmaster->setDiscount_value($discount_value);
                    $couponmaster->setDiscount_type($discount_type);
                    $couponmaster->setReturn_type($return_type);
                    $couponmaster->setCoupon_status($status);
                    $couponmaster->setVisible_all($visible_all);
                    $couponmaster->setNo_of_user_use($no_of_user_use);
                    $couponmaster->setNo_of_times_use($no_of_times_use);
                    $couponmaster->setCoupon_usage_interval($coupon_usage_interval);
                    $couponmaster->setMin_order_amount($min_amount);
                    $couponmaster->setCreated_datetime(date('Y-m-d H:i:s'));
                    $couponmaster->setDomain_id(1);
                    $couponmaster->setIs_deleted(0);
                    //$em = $this->getDoctrine()->getManager();
                    $em->persist($couponmaster);
                    $em->flush();

                    $this->get('session')->getFlashBag()->set('success_msg', 'Coupon inserted successfully.');


                    return $this->redirect($this->generateUrl('admin_coupon_index', array("domain" => $this->get('session')->get('domain'))));
                }
            } else {
                $this->get('session')->getFlashBag()->set('error_msg', 'Please fill all required fields.');
                return $this->redirect($this->generateUrl('admin_coupon_addcoupon', array("domain" => $this->get('session')->get('domain'))));
            }
        }
        return array();
    }

    /**
     * @Route("/getstateajax")
     * @Template()
     * */
    public function getstateajaxAction() {
        $html = "<option>Select State</option>";
        $method = $this->get('request')->getMethod();
        if ($method = "POST") {
            $country_id = $_POST['country_id'];
            $lang_id = $_POST['lang_id'];

            $state_master = $this->getDoctrine()
                    ->getManager()
                    ->getRepository('AdminBundle:Statemaster')
                    ->findBy(array('main_country_id' => $country_id, "language_id" => $lang_id, 'is_deleted' => 0));
            if (!empty($state_master)) {
                foreach ($state_master as $key => $val) {
                    $html .= "<option value='" . $val->getMain_state_id() . "'>" . $val->getState_name() . "</option>";
                }
            }
        }

        $response = new Response();
        $response->setContent($html);
        return $response;
    }

    /**
     * @Route("/couponstatus")
     * @Template()
     */
    public function couponstatusAction() {
        if (isset($_REQUEST['id']) && !empty($_REQUEST['id']) && isset($_REQUEST['status']) && !empty($_REQUEST['status'])) {

            $em = $this->getDoctrine()->getManager();



            $status = $_POST['status'];
            $id = $_POST['id'];


            if ($_POST['status'] == "true") {
                $status = "active";
            } else {
                $status = "inactive";
            }

            $coupon_list = $em->getRepository('AdminBundle:Couponmaster')
                    ->findBy(
                    array(
                        'id' => $id,
                        'is_deleted' => "0"
                    )
                    );

            if (!empty($coupon_list)) {
                foreach ($coupon_list as $key => $val) {
                    $coupon = $em->getRepository('AdminBundle:Couponmaster')
                            ->findOneBy(
                            array(
                                'id' => $val->getId(),
                                'is_deleted' => "0"
                            )
                            );
                    $coupon->setCoupon_status($status);
                    $em->persist($coupon);
                    $em->flush();
                }
            }
        }
        return new Response();
    }

    /**
     * @Route("/couponvisible")
     * @Template()
     */
    public function couponvisibleAction() {
        if (isset($_REQUEST['id']) && !empty($_REQUEST['id']) && isset($_REQUEST['visible']) && !empty($_REQUEST['visible'])) {
            //$request = $this->getRequest() ;
            //$session = $request->getSession() ;
            $em = $this->getDoctrine()->getManager();


            $id = $_POST['id'];

            if ($_POST['visible'] == "true") {
                $visible = "yes";
            } else {
                $visible = "no";
            }

            $coupon_list = $em->getRepository('AdminBundle:Couponmaster')
                    ->findBy(
                    array(
                        'id' => $id,
                        'is_deleted' => "0"
                    )
                    );


            if (!empty($coupon_list)) {
                foreach ($coupon_list as $key => $val) {
                    $coupon = $em->getRepository('AdminBundle:Couponmaster')
                            ->findOneBy(
                            array(
                                'id' => $val->getId(),
                                'is_deleted' => "0"
                            )
                            );

                    $coupon->setVisible_all($visible);
                    $em->persist($coupon);
                    $em->flush();
                }
            }
        }
        return new Response();
    }

    /**
     * @Route("/saveapplycoupon/{coupon_master_id}")
     * @Template()
     */
    public function saveapplycouponAction($coupon_master_id) {
        $em = $this->getDoctrine()->getManager();
        if (isset($_POST['save_coupon_apply']) && $_POST['save_coupon_apply'] == "save_coupon_apply") {
            if (isset($_POST['assign_on']) && $_POST['assign_on'] != "" && isset($_POST['relation_id']) && $_POST['relation_id'] != "" && isset($_POST['status'])) {


                $coupon_list = $em->getRepository('AdminBundle:Coupon_applied_to_object_list')->findBy( array('coupon_id' => $coupon_master_id, 'is_deleted' => "0"  ) );
                if($coupon_list){
                    foreach($coupon_list as $key=>$val){
                        $val->setIs_deleted(1);
                        $em->flush();
                    }
                }
                $coupon_applied_list = new Coupon_applied_to_object_list();
                $coupon_applied_list->setApplied_on($_POST['assign_on']);
                $coupon_applied_list->setCoupon_id($coupon_master_id);
                $coupon_applied_list->setRelation_id($_POST['relation_id']);
                $coupon_applied_list->setCreated_date(date("Y-m-d H:i:s"));
                $coupon_applied_list->setCoupon_applied_to_object_list_field_1("");
                $coupon_applied_list->setCoupon_applied_to_object_list_field_2("");
                $coupon_applied_list->setIs_deleted(0);

                $em->persist($coupon_applied_list);
                $em->flush();
                
                // Save Coupon To users 
                $coupon_list = $em->getRepository('AdminBundle:coupon_assigned_to_user')->findBy( array('coupon_id' => $coupon_master_id, 'is_deleted' => "0"  ) );
                if($coupon_list){
                    foreach($coupon_list as $key=>$val){
                        $val->setIs_deleted(1);
                        $em->flush();
                    }
                }
                $coupon_users = $_POST['users'];
                foreach($coupon_users as $ckey=>$cval){
                    $coupon_assigned_to_user = new coupon_assigned_to_user();
                    $coupon_assigned_to_user->setUser_id($cval);
                    $coupon_assigned_to_user->setCoupon_id($coupon_master_id);
                    $coupon_assigned_to_user->setAssign_datetime(date("Y-m-d H:i:s"));
                    $coupon_assigned_to_user->setAssign_by($this->get('session')->get('user_id'));
                    $coupon_assigned_to_user->setUse_before_days("");
                    $coupon_assigned_to_user->setIs_deleted("0");
                    $em->persist($coupon_assigned_to_user);
                    $em->flush();
                }
                $this->get('session')->getFlashBag()->set('success_msg', 'Coupon applied successfully');
            } else {
                $this->get('session')->getFlashBag()->set('error_msg', 'Please fill all required fields');
            }
        } else {
            $this->get('session')->getFlashBag()->set('error_msg', 'Oops! Somethign goes wrong! try again later!');
        }
        return $this->redirect($this->generateUrl('admin_coupon_applycoupon', array('domain' => $this->get('session')->get('domain'), 'coupon_master_id' => $coupon_master_id)));
    }

    /**
     * @Route("/deletecoupon/{coupon_master_id}",defaults={"coupon_master_id":""})
     * @Template()
     */
    public function deletecouponAction($coupon_master_id) {
        $em = $this->getDoctrine()->getManager();
        if ($coupon_master_id != '0') {
            //var_dump($coupon_master_id);exit;
            $coupon_master = $em
                    ->getRepository('AdminBundle:Couponmaster')
                    ->findOneBy(array('is_deleted' => "0", 'id' => $coupon_master_id));
            if (!empty($coupon_master)) {
                $coupon_master->setIs_deleted("1");

                $em->remove($coupon_master);
                $em->flush();
            }
            $applycoupon_list = $em
                    ->getRepository('AdminBundle:Coupon_applied_to_object_list')
                    ->findOneBy(array('is_deleted' => 0, 'coupon_id' => $coupon_master_id));
            if (!empty($applycoupon_list)) {
                $applycoupon_list->setIs_deleted("1");

                $em->remove($applycoupon_list);
                $em->flush();
            }
            $this->get("session")->getFlashBag()->set("success_msg", "Coupon Deleted successfully");
            return $this->redirect($this->generateUrl("admin_coupon_index", array("domain" => $this->get('session')->get('domain'))));
        }
    }

    private function _savecustomerassigncoupons($assing_copon_arr) {
        $em = $this->getDoctrine()->getManager();
        //prepare for database
        $connection = $em->getConnection();
        //prepare query
        $customer_user = $connection->prepare("SELECT `user_master_id` as user_master_id FROM user_master WHERE  user_role_id=7 And user_type='user' And user_status='active' AND is_deleted=0 AND domain_id Like '" . $assing_copon_arr['custcounpon']['domain_id'] . "'");
        //query exexute
        $customer_user->execute();
        //fetch record into table
        $customer_user_id_list = $customer_user->fetchAll();

        if (isset($customer_user_id_list) && !empty($customer_user_id_list)) {
            foreach ($customer_user_id_list as $customer_user_id_list_key => $customer_user_id_list_value) {
                $customer_list[] = $customer_user_id_list_value['user_master_id'];
            }
            $customer_list_slipt = implode(",", $customer_list);
        } else {
            $customer_list_slipt = "";
        }
        if (isset($customer_list_slipt) && !empty($customer_list_slipt) && !empty($assing_copon_arr)) {
            $customer_user_ids = $customer_list_slipt;
            $custassigncopon = new Custassigncopon();
            $custassigncopon->setCustomer_user_id($customer_user_ids);
            $custassigncopon->setDomain_id($assing_copon_arr['custcounpon']['domain_id']);
            $custassigncopon->setApp_id($assing_copon_arr['custcounpon']['app_id']);
            $custassigncopon->setTable_name($assing_copon_arr['custcounpon']['table_name']);
            $custassigncopon->setTable_id($assing_copon_arr['custcounpon']['table_id']);
            $custassigncopon->setIs_deleted(0);
            $em->persist($custassigncopon);
            $em->flush();
            return true;
        }
    }

}
