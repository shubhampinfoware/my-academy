<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\PropertyAccess\PropertyAccess;

use AdminBundle\Entity\Categorymaster;

/**
 * @Route("/")
 */
class CategoryController extends BaseController
{

    public function __construct()
    { }

    /**
     * @Route("/category")
     * @Template()
     */
    public function indexAction()
    {
        $language = null;
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('AdminBundle:Languagemaster');
        $language_list = $repository->findAll();

        foreach ($language_list as $key => $value) {
            $language[] = array(
                "language_master_id" => $value->getLanguage_master_id(),
                "language_name" => $value->getLanguage_name(),
                "display_title" => $value->getDisplay_title(),
                "language_code" => $value->getLanguage_code(),
            );
        }

        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('AdminBundle:Categorymaster');
        $all_category = $repository->findBy(array('is_deleted' => "0"));

        if (count($all_category) > 0) {
            foreach (array_slice($all_category, 0) as $lkey => $lval) {
                $parent_category_name = '';
                if ($lval->getParentCategoryId() != 0) {
                    $parent_category_name = '';
                    $parent_category = $em
                        ->getRepository('AdminBundle:Categorymaster')
                        ->findOneBy(array('is_deleted' => "0", 'categorymaster_id' => $lval->getParentCategoryId()));
                    if (!empty($parent_category)) {
                        $parent_category_name = $parent_category->getCategoryName();
                    }
                }
                $lang_wise_category = "";

                foreach ($language as $lngkey => $lngval) {
                    $lang_category_name = $em->getRepository('AdminBundle:Categorymaster')->findOneBy(array('language_id' => $lngval['language_master_id'], "main_category_id" => $lval->getMainCategoryId()));
                    $category_name = '';
                    if (!empty($lang_category_name)) {
                        $category_name = $lang_category_name->getCategoryName();
                    }
                    $lang_wise_category[] = array(
                        "language_id" => $lngval['language_master_id'],
                        "category_name" => $category_name
                    );
                }

                $all_category_details[] = array(
                    "category_master_id" => $lval->getCategorymasterId(),
                    "category_name" => $lval->getCategoryName(),
                    "parent_category_id" => $lval->getParentCategoryId(),
                    "sort_order" => $lval->getSort_order(),
                    "parent_category_name" => $parent_category_name,
                    "category_description" => $lval->getCategoryDescription(),
                    "main_category_id" => $lval->getMainCategoryId(),
                    "category_image_id" => $lval->getCategoryImageId(),
                    "category_type" => $lval->getCategory_type(),
                    "language_id" => $lval->getLanguage_id(),
                    "lang_wise_category" => $lang_wise_category
                );
            }

            return array("language" => $language, "all_category" => $all_category_details);
        }

        return array("language" => $language);
    }

    /**
     * @Route("/addcategory/{category_id}",defaults={"category_id":""})
     * @Template()
     */
    public function addcategoryAction($category_id)
    {
        $language = null;
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('AdminBundle:Languagemaster');
        $language_list = $repository->findAll();

        foreach ($language_list as $key => $value) {
            $language[] = array(
                "language_master_id" => $value->getLanguage_master_id(),
                "language_name" => $value->getLanguage_name(),
                "display_title" => $value->getDisplay_title(),
                "language_code" => $value->getLanguage_code(),
            );
        }

        $parent_category = $em
            ->getRepository('AdminBundle:Categorymaster')
            ->findBy(array('is_deleted' => "0", "category_type" => 'section'));

        if (!empty($category_id)) {

            /* $query = "SELECT category_master.*,
              media_master.media_location,media_master.media_name
              FROM category_master
              JOIN media_master
              ON category_master.category_image_id = media_master.media_master_id
              WHERE category_master.is_deleted = 0 AND category_master.main_category_id = '" . $category_id . "'"; */

            $selected_category = $em
                ->getRepository('AdminBundle:Categorymaster')
                ->findBy(array('is_deleted' => "0", "main_category_id" => $category_id));

            return array("language" => $language, "parent_category" => $parent_category, "selected_category" => $selected_category,);
        } else {
            return array("language" => $language, "parent_category" => $parent_category);
        }
    }

    /**
     * @Route("/savecategory")
     * @Template()
     */
    public function savecategoryAction()
    {
        $language = null;
        $em = $this->getDoctrine()->getManager();
        $catrepository = $em->getRepository('AdminBundle:Categorymaster');
        $cate_list_cnt = count($catrepository->findAll()) + 1;
        $repository = $em->getRepository('AdminBundle:language_master');
        $language_list = $repository->findAll();
        
        foreach ($language_list as $key => $value) {
            $language[] = array(
                "language_master_id" => $value->getLanguage_master_id(),
                "language_name" => $value->getLanguage_name(),
                "display_title" => $value->getDisplay_title(),
                "language_code" => $value->getLanguage_code(),
            );
        }

        $media_id = 0;
        if ($_REQUEST['save'] == 'save_category' && !empty($_REQUEST['language_id']) && !empty($_REQUEST['category_name'])) {

            //add image
            $category_logo = $_FILES['category_logo'];
            if (isset($_REQUEST['image_hidden'])) {
                $media_id = $_REQUEST['image_hidden'];

                if ($category_logo['name'] != "" && !empty($category_logo['name'])) {
                    $extension = pathinfo($category_logo['name'], PATHINFO_EXTENSION);

                    $media_type_id = $this->mediatype($extension);

                    if (!empty($media_type_id)) {
                        $logo = $category_logo['name'];
                        $tmpname = $category_logo['tmp_name'];
                        $file_path = $this->container->getParameter('file_path');
                        $logo_path = $file_path . '/category';
                        $logo_upload_dir = $this->container->getParameter('upload_dir') . '/category/';

                        $media_id = $this->mediaremoveAction($logo, $tmpname, $logo_path, $logo_upload_dir, $media_id, $media_type_id);
                    } else {
                        $this->get("session")->getFlashBag()->set("error_msg", "Category Logo is Required");
                        //return $this->redirect($this->generateUrl("admin_category_addcategory", array("category_id" => $category_id)));
                        return $this->redirect($this->generateUrl("admin_category_addcategory", array("domain" => $this->get('session')->get('domain'))));
                    }
                }
            } else {
                if ($category_logo['name'] != "" && !empty($category_logo['name'])) {
                    $extension = pathinfo($category_logo['name'], PATHINFO_EXTENSION);

                    $media_type_id = $this->mediatype($extension);

                    if (!empty($media_type_id)) {
                        $logo = $category_logo['name'];
                        $tmpname = $category_logo['tmp_name'];
                        $file_path = $this->container->getParameter('file_path');
                        $logo_path = $file_path . '/category';
                        $logo_upload_dir = $this->container->getParameter('upload_dir') . '/category/';
                        $media_id = $this->mediauploadAction($logo, $tmpname, $logo_path, $logo_upload_dir, $media_type_id);
                    } else {
                        $this->get("session")->getFlashBag()->set("error_msg", "Category Logo is Required");
                        return $this->redirect($this->generateUrl("admin_category_addcategory", array("domain" => $this->get('session')->get('domain'))));
                    }
                }
            }

            $session = new Session();
            $date_time = date("Y-m-d H:i:s");
            $category = new Categorymaster();
            $category->setCategoryName($_REQUEST['category_name']);
            $category->setCategory_type($_REQUEST['type']);
            $category->setParentCategoryId($_REQUEST['parent_category']);
            $category->setCategoryDescription($_REQUEST['category_description']);
            $category->setCategoryImageId($media_id);
            $category->setLanguage_id($_REQUEST['language_id']);
            $category->setSort_order($_REQUEST['sort_order']);
            $category->setDisplay_on_home($_REQUEST['display_on_home']);
            $category->setIs_deleted(0);
            $category->setDomain_id(1);
            $category->setCreated_datetime($date_time);

            if (!empty($_REQUEST['main_category_id']) && $_REQUEST['main_category_id'] != '0') {
                /* if(isset($media_id) && !empty($media_id))
                  {
                  $category->set_category_image_id($media_id);
                  } */
                $category->setMainCategoryIid($_REQUEST['main_category_id']);
                $category->setCreated_datetime($date_time);

                $em->persist($category);
                $em->flush();
            } else {
                /* $_category->set_category_image_id($media_id); */
                $category->setMainCategoryId(0);

                $em->persist($category);
                $em->flush();
                $main_cat = $category->getCategorymaster_id();
                $category->setMainCategoryId($main_cat);
                $category->setCreated_datetime($date_time);
                $category->setCategory_type($_REQUEST['type']);
                $category->setDisplay_on_home($_REQUEST['display_on_home']);
                $category->setDomain_id(1);

                $em->persist($category);
                $em->flush();
            }
        }
        $this->get("session")->getFlashBag()->set("success_msg", "Category Added successfully");
        return $this->redirect($this->generateUrl("admin_category_index"));
    }

    /**
     * @Route("/deletecategory/{category_id}",defaults={"category_id":""})
     * @Template()
     */
    public function deletecategoryAction($category_id)
    {
        $em = $this->getDoctrine()->getManager();
        if ($category_id != '0') {

            $selected_category = $em
                ->getRepository('AdminBundle:Categorymaster')
                ->findBy(array('is_deleted' => "0", "main_category_id" => $category_id));

            if (!empty($selected_category)) {
                foreach ($selected_category as $selkey => $selval) {
                    $selected_category_del = $em
                        ->getRepository('AdminBundle:Categorymaster')
                        ->findOneBy(array('is_deleted' => '0', 'categorymaster_id' => $selval->getCategorymasterId()));
                    $selected_category_del->setIs_deleted('1');

                    $em->persist($selected_category_del);
                    $em->flush();
                }
            }

            $selected_category = $this->getDoctrine()
                ->getRepository('AdminBundle:Categorymaster')
                ->findBy(array('is_deleted' => '0', 'parent_category_id' => $category_id));
            if (!empty($selected_category)) {
                foreach ($selected_category as $selkey => $selval) {
                    $selected_category_del = $em
                        ->getRepository('AdminBundle:Categorymaster')
                        ->findOneBy(array('is_deleted' => 0, 'categorymaster_id' => $selval->getCategorymasterId()));

                    $selected_category_del->setIs_deleted('1');

                    $em->persist($selected_category_del);
                    $em->flush();
                }
            }
        }

        $this->get("session")->getFlashBag()->set("success_msg", "Category Deleted successfully");
        return $this->redirect($this->generateUrl("admin_category_index", array("domain" => $this->get('session')->get('domain'))));
    }

    /**
     * @Route("/updatecategory/{category_id}",defaults={"category_id":""})
     * @Template()
     */
    public function updatecategoryAction($category_id)
    {
        if (!empty($category_id)) {

            //update image
            $category_logo = $_FILES['category_logo'];

            $media_id = "";

            if ($category_logo['name'] != "" && !empty($category_logo['name'])) {
                $extension = pathinfo($category_logo['name'], PATHINFO_EXTENSION);

                $media_type_id = $this->mediatype($extension);

                if (!empty($media_type_id)) {
                    $logo = $category_logo['name'];
                    $tmpname = $category_logo['tmp_name'];
                    $file_path = $this->container->getParameter('file_path');
                    $logo_path = $file_path . '/category';
                    $logo_upload_dir = $this->container->getParameter('upload_dir') . '/category/';

                    $media_id = $this->mediauploadAction($logo, $tmpname, $logo_path, $logo_upload_dir, $media_id, $media_type_id);
                } else {
                    $this->get("session")->getFlashBag()->set("error_msg", "Category Logo is Required");
                    return $this->redirect($this->generateUrl("admin_category_addcategory", array("domain" => $this->get('session')->get('domain'), "category_id" => $category_id)));
                }
            }

            $category = $this->getDoctrine()->getManager()
                ->getRepository('AdminBundle:Categorymaster')
                ->findOneBy(array('is_deleted' => "0", 'id' => $category_id));
            if (!empty($category)) {
                $disp_home = 'no';
                if (isset($_REQUEST['display_on_home'])) {
                    $disp_home = $_REQUEST['display_on_home'];
                }
                $category->setCategoryName($_REQUEST['category_name']);
                $category->setParentCategoryId($_REQUEST['parent_category']);
                $category->setCategory_type($_REQUEST['type']);
                $category->setCategoryDescription($_REQUEST['category_description']);
                $category->setSort_order($_REQUEST['sort_order']);
                $category->setDisplay_on_home($disp_home);

                if ($media_id != "" and !empty($media_id)) {
                    $category->setCategoryImageId($media_id);
                }
                $em = $this->getDoctrine()->getManager();
                $em->persist($category);
                $em->flush();
            }
        }
        $this->get("session")->getFlashBag()->set("success_msg", "Category Updated successfully");
        return $this->redirect($this->generateUrl("admin_category_index"));
    }

    /**
     * @Route("/tags/{category_id}/{language_id}/{tag_module_relation_id}",defaults={"category_id":"","language_id":"","tag_module_relation_id":""})
     * @Template()
     */
    public function tagsAction($category_id, $language_id, $tag_module_relation_id)
    {
        if (!empty($category_id) && !empty($language_id) && !empty($tag_module_relation_id)) { } else {
            $language = $this->getDoctrine()
                ->getManager()
                ->getRepository('AdminBundle:Languagemaster')
                ->findBy(array('is_deleted' => 0));

            $tag_master = $this->getDoctrine()
                ->getManager()
                ->getRepository('AdminBundle:Tagmaster')
                ->findBy(array('is_deleted' => 0, 'language_id' => $language_id));

            $query = "SELECT tag_module_relation.* , tag_master.* FROM tag_module_relation Join tag_master on tag_module_relation.main_tag_id = tag_master.main_tag_id WHERE tag_module_relation.main_category_id ='" . $category_id . "' and tag_module_relation.is_deleted=0 and tag_master.language_id = '" . $language_id . "'";

            $em = $this->getDoctrine()->getManager();
            $connection = $em->getConnection();
            $statement = $connection->prepare($query);
            $statement->execute();
            $tag_display = $statement->fetchAll();

            return array("language" => $language, "tag_master" => $tag_master, "cat_language_id" => $language_id, "cat_main_id" => $category_id, "tag_display" => $tag_display);
        }
    }

    /**
     * @Route("/savetag")
     * @Template()
     */
    public function savetagAction()
    {
        $language_id = $_POST['cat_language_id'];
        $category_id = $_POST['cat_main_id'];
        if (isset($_POST['save_category_tag']) && $_POST['save_category_tag'] == 'save_category_tag') {

            if ($_POST['main_tag_id'] != "" && !empty($_POST['main_tag_id'])) {

                $tag_module_relation = $this->getDoctrine()
                    ->getManager()
                    ->getRepository('AdminBundle:Tagmodulerelation')
                    ->findOneBy(array('is_deleted' => 0, 'main_tag_id' => $_POST['main_tag_id'], "main_category_id" => $category_id));
                if (empty($tag_module_relation)) {
                    $tag_module_relation = new Tagmodulerelation();

                    $tag_module_relation->setModule_name('category');

                    $tag_module_relation->setMain_tag_id($_POST['main_tag_id']);
                    $tag_module_relation->setMain_category_id($category_id);
                    $tag_module_relation->setIs_deleted(0);
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($tag_module_relation);
                    $em->flush();
                    $this->get('session')->getFlashBag()->set('success_msg', 'Tag Inserted For Category Successfully');
                    return $this->redirect($this->generateUrl("admin_category_tags", array("domain" => $this->get('session')->get('domain'), "category_id" => $category_id, "language_id" => $language_id)));
                } else {
                    $this->get('session')->getFlashBag()->set('error_msg', 'This Tag already added for this category.');
                    return $this->redirect($this->generateUrl("admin_category_tags", array("domain" => $this->get('session')->get('domain'), "category_id" => $category_id, "language_id" => $language_id)));
                }
            } else {
                $this->get('session')->getFlashBag()->set('error_msg', 'Please! Fill all required fields.');
                return $this->redirect($this->generateUrl("admin_category_tags", array("domain" => $this->get('session')->get('domain'), "category_id" => $category_id, "language_id" => $language_id)));
            }
        } else {
            $this->get('session')->getFlashBag()->set('error_msg', 'Oops! something goes wrong! try again later');
            return $this->redirect($this->generateUrl("admin_category_tags", array("domain" => $this->get('session')->get('domain'), "category_id" => $category_id, "language_id" => $language_id)));
        }
    }

    /**
     *  @Route ("/deletecategorytag/{tag_module_relation_id}/{category_id}/{language_id}",defaults={"tag_module_relation_id":"","category_id":"","language_id":""})
     *  @Template()
     */
    public function deletecategorytag($tag_module_relation_id, $category_id, $language_id)
    {
        if (!empty($tag_module_relation_id)) {
            $tag_module_relation = $this->getDoctrine()
                ->getManager()
                ->getRepository('AdminBundle:Tagmodulerelation')
                ->findOneBy(array('is_deleted' => 0, 'tag_module_relation_id' => $tag_module_relation_id));
            if (!empty($tag_module_relation)) {
                $tag_module_relation->setIs_deleted(1);
                $em = $this->getDoctrine()->getManager();
                $em->persist($tag_module_relation);
                $em->flush();
                $this->get('session')->getFlashBag()->set('success_msg', 'Tag Deleted For Category Successfully');
                return $this->redirect($this->generateUrl("admin_category_tags", array("domain" => $this->get('session')->get('domain'), "category_id" => $category_id, "language_id" => $language_id)));
            }
        } else {
            $this->get('session')->getFlashBag()->set('error_msg', 'Oops! something goes wrong! try again later');
            return $this->redirect($this->generateUrl("admin_category_tags", array("domain" => $this->get('session')->get('domain'), "category_id" => $category_id, "language_id" => $language_id)));
        }
    }
}
