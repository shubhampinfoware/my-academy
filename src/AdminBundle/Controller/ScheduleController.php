<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\PropertyAccess\PropertyAccess;

use AdminBundle\Entity\Schedulemaster;
use AdminBundle\Entity\Scheduledaylist;
use AdminBundle\Entity\Scheduleassignedlist;

/**
* @Route("/{domain}/admin")
*/
class ScheduleController extends BaseController {
	
    public function __construct() {
        parent::__construct();
        $obj = new BaseController();
        $obj->checkSessionAction();
    }
    
    /**
     * @Route("/schedule")
     * @Template()
     */
    public function indexAction() {

        $_sql = "SELECT * from schedule_master where is_deleted = 0";
        $schedule_list = $this->firequery($_sql);

        return array(
            'schedule_list' => $schedule_list
        );
    }

    /**
     * @Route("/addschedule/{id}",defaults={"id"=""})
     * @Template()
     */
    public function addscheduleAction($id)
    {
        $session = $this->get('session');
        if (!empty($id)) {
            $domain_id = $session->get('domain_id');

            $schedule = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Schedulemaster')->findOneBy(array("is_deleted" => "0", "schedule_master_id" => $id));

            $day_array = [];
            $course_list = [];
            $start_date = '';
            $end_date = '';
            if(!empty($schedule)){
                $start_date = $schedule->getStart_date();
                $end_date = $schedule->getEnd_date();

                $datediff = strtotime($end_date) - strtotime($start_date);
                $days = round($datediff / (60 * 60 * 24));

                if($days > 0){
                    for($i=0; $i <= $days; $i++){

                        $schedule_date = date('Y-m-d', strtotime("+{$i} days ". $start_date));

                        $_sql = "SELECT * from schedule_assigned_list where schedule_date = '{$schedule_date}' and schedule_id = {$id} and is_deleted = 0";
                        $assigned_list = $this->firequery($_sql);

                        $day_array[] = array(
                            'schedule_date' => $schedule_date,
                            'assigned_list' => $assigned_list
                        );
                    }
                }

                $_sql = "SELECT * from coursemaster where course_status = 'Active' and domain_id = {$domain_id} and is_deleted = 0 group by main_coursemaster_id";
                $course_list = $this->firequery($_sql);
            }

            return array(
                "schedule_id" => $id,
                "schedule" => $schedule,
                "start_date" => $start_date,
                "end_date" => $end_date,
                "day_array" => $day_array,
                "course_list" => $course_list
            );
        }

        return array();
    }

    /**
     * @Route("/saveschedule")
     */
    public function savescheduleAction()
    {
        $start_date = date('Y-m-d', strtotime($_POST["start_date"]));
        $end_date = date('Y-m-d', strtotime($_POST["end_date"]));

        $em = $this->getDoctrine()->getManager();
        $session = $this->get('session');

        if (isset($_POST["id"]) && $_POST["id"] != "") {
            $id = $_POST["id"];
            $schedule_master = $em->getRepository('AdminBundle:Schedulemaster')->findOneBy(
                array(
                    "is_deleted" => "0",
                    "schedule_master_id" => $id
                )
            );
            $schedule_master->setStart_date($start_date);
            $schedule_master->setEnd_date($end_date);
            $schedule_master->setIs_deleted(0);

            $em->flush();

            $datediff = strtotime($end_date) - strtotime($start_date);

            $days = round($datediff / (60 * 60 * 24));

            $day_array = [];
            if($days > 0){
                for($i=0; $i <= $days; $i++){
                    $day_array[] = date('Y-m-d', strtotime("+{$i} days ". $start_date));
                }
            }

            // remove old day if not in new date range
            $schedule_day_list = $em->getRepository("AdminBundle:Scheduledaylist")->findBy([
                'schedule_id' => $id,
                'is_deleted' => 0,
            ]);

            if(!empty($schedule_day_list)){
                foreach($schedule_day_list as $_scheduleDay){
                    if(!in_array($_scheduleDay->getSchedule_date(), $day_array)){
                        $_scheduleDay->setIs_deleted(1);
                        $em->flush();
                    }
                }
            }

            $session->getFlashBag()->set('success_msg', " Schedule Updated Successfully");
        } else {
            $schedule_master = new Schedulemaster();
            $schedule_master->setStart_date($start_date);
            $schedule_master->setEnd_date($end_date);
            $schedule_master->setCreated_by($session->get('user_id'));
            $schedule_master->setCreated_datetime(date('Y-m-d H:i:s'));
            $schedule_master->setIs_deleted(0);
            
            $em->persist($schedule_master);
            $em->flush();
            $id = $schedule_master->getSchedule_master_id();

            $session->getFlashBag()->set('success_msg', " Schedule Created Successfully");
        }

        return $this->redirectToRoute('admin_schedule_addschedule', array('domain' => $session->get('domain'), 'id' => $id));
    }

    /**
     * @Route("/schedule-day-list/{id}",defaults={"id"=""})
     * @Template()
     */
    public function scheduledaylistAction($id)
    {
        $session = $this->get('session');
        if (!empty($id)) {
            $domain_id = $session->get('domain_id');

            $schedule = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Schedulemaster')->findOneBy(array("is_deleted" => "0", "schedule_master_id" => $id));

            $day_array = [];
            $course_list = [];
            if(!empty($schedule)){
                $start_date = $schedule->getStart_date();
                $end_date = $schedule->getEnd_date();

                $datediff = strtotime($end_date) - strtotime($start_date);

                $days = round($datediff / (60 * 60 * 24));

                if($days > 0){
                    for($i=0; $i <= $days; $i++){
                        $day_array[] = date('Y-m-d', strtotime("+{$i} days ". $start_date));
                    }
                }

                $_sql = "SELECT * from coursemaster where course_status = 'Active' and domain_id = {$domain_id} and is_deleted = 0 group by main_coursemaster_id";
                $course_list = $this->firequery($_sql);
            }

            return array(
                "day_array" => $day_array,
                "course_list" => $course_list,
                "schedule_id" => $id
            );
        }

        $session->getFlashBag()->set('error_msg', "No Schedule Found");
        return $this->redirectToRoute('admin_schedule_index', array('domain' => $session->get('domain')));
    }

    /**
     * @Route("/assignschedule")
     * @Template()
     */
    public function assignScheduleAction(Request $request) {

        if(!empty($_REQUEST) && isset($_REQUEST['schedule_id']) && isset($_REQUEST['schedule_date']) && isset($_REQUEST['course_id']) && isset($_REQUEST['unit_id']) && isset($_REQUEST['trainer_id'])){
            $schedule_title = isset($_REQUEST['schedule_title']) ? $_REQUEST['schedule_title'] : '';

            $schedule_id = $_REQUEST['schedule_id'];
            $schedule_date = date('Y-m-d', strtotime($_REQUEST['schedule_date']));

            $course_id = $_REQUEST['course_id'];
            $unit_id = $_REQUEST['unit_id'];
            $trainer_id = $_REQUEST['trainer_id'];

            if($schedule_title == ''){
                $sql = "SELECT * from course_unit where main_course_unit_id = {$unit_id} and is_deleted = 0";
                $course_unit = $this->firequery($sql);

                $unit_name = '';
                if(!empty($course_unit)){
                    $unit_name = $course_unit[0]['unit_name'];
                }

                $sql = "SELECT * from usermaster where usermaster_id = {$trainer_id} and is_deleted = 0";
                $trainer_detail = $this->firequery($sql);

                $trainer_name = '';
                if(!empty($trainer_detail)){
                    $trainer_name = trim($trainer_detail[0]['user_first_name'].' '.$trainer_detail[0]['user_last_name']);
                }

                $schedule_title = "{$unit_name} with {$trainer_name}";
            }

            $em = $this->getDoctrine()->getManager();
            $em->beginTransaction();

            $schedule_day = $em->getRepository("AdminBundle:Scheduledaylist")->findOneBy([
                'schedule_id' => $schedule_id,
                'schedule_date' => $schedule_date,
                'is_deleted' => 0,
            ]);

            if(!empty($schedule_day)){
                // edit day

                $schedule_day_id = $schedule_day->getSchedule_day_list_id();
            } else {
                // add day
                $schedule_day = new Scheduledaylist();
                $schedule_day->setSchedule_id($schedule_id);
                $schedule_day->setSchedule_date($schedule_date);
                $schedule_day->setCreated_datetime(date('Y-m-d H:i:s'));
                $schedule_day->setIs_deleted(0);

                $em->persist($schedule_day);
                $em->flush();

                $schedule_day_id = $schedule_day->getSchedule_day_list_id();
            }

            // remove all previous schedule_assigned_list
            $prev_schedule_assigned_list = $em->getRepository("AdminBundle:Scheduleassignedlist")->findBy([
                'schedule_day_id' => $schedule_day_id,
                'schedule_id' => $schedule_id,
            ]);

            if(!empty($prev_schedule_assigned_list)){
                foreach($prev_schedule_assigned_list as $_prevList){
                    $_prevList->setIs_deleted(1);
                    $em->flush();
                }
            }

            // add new schedule_assigned_list
            $schedule_assigned_list = new Scheduleassignedlist();
            $schedule_assigned_list->setSchedule_title($schedule_title);
            $schedule_assigned_list->setSchedule_day_id($schedule_day_id);
            $schedule_assigned_list->setSchedule_id($schedule_id);
            $schedule_assigned_list->setSchedule_date($schedule_date);
            $schedule_assigned_list->setCourse_id($course_id);
            $schedule_assigned_list->setUnit_id($unit_id);
            $schedule_assigned_list->setTrainer_id($trainer_id);
            $schedule_assigned_list->setCreated_datetime(date('Y-m-d H:i:s'));
            $schedule_assigned_list->setIs_deleted(0);

            $em->persist($schedule_assigned_list);
            $em->flush();

            $em->commit();
            $em->clear();

            echo $schedule_id;exit;
        }

        echo '';exit;
    }

    /**
     * @Route("/get-course-units/{course_id}", defaults={"course_id"=""})
     * @Template()
     */
    public function getCourseUnitsAction($course_id, Request $request) {

        $postData = $request->request->all();

        $session = $this->get('session');
        $domain_id = $session->get('domain_id');

        $_html = "<option value=''>Select Course Unit</option>";
        // if(isset($postData['course_id']) && !empty($postData)){
        if($course_id != ''){
            // $course_id = $postData['course_id'];

            $_query = "SELECT unit.* from course_unit_relation cur, course_unit unit where relation_object_id = {$course_id} and relation_with = 'course' and unit.domain_id = {$domain_id} and cur.is_deleted = 0 and unit.is_deleted = 0 group by main_course_unit_id";
            $selected_class = $this->firequery($_query);

            if(!empty($selected_class)){
                foreach($selected_class as $_class){

                    $_html .= "<option value='{$_class['main_course_unit_id']}'>{$_class['unit_name']}</option>";
                }
            }
        }

        echo $_html;exit;
    }

    /**
     * @Route("/get-course-trainers/{course_id}", defaults={"course_id"=""})
     * @Template()
     */
    public function getCourseTrainersAction($course_id, Request $request) {

        $postData = $request->request->all();

        $session = $this->get('session');
        $domain_id = $session->get('domain_id');

        $_html = "<option value=''>Select Trainer</option>";
        // if(isset($postData['course_id']) && !empty($postData)){
        if($course_id != ''){
            // $course_id = $postData['course_id'];

            $_query = "SELECT * from usermaster u, coursetrainermaster ctm where u.usermaster_id = ctm.usermaster_id and ctm.coursemaster_id = {$course_id} and u.is_deleted = 0 and ctm.is_deleted = 0";
            $associated_trainer = $this->firequery($_query);

            if(!empty($associated_trainer)){
                foreach($associated_trainer as $_trainer){
                    $username = trim($_trainer['user_first_name'].' '.$_trainer['user_last_name']);
                    $_html .= "<option value='{$_trainer['usermaster_id']}'>{$username}</option>";
                }
            }
        }

        echo $_html;exit;
    }
}