<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\PropertyAccess\PropertyAccess;

/**
* @Route("/admin")
*/
class SettingsController extends Controller
{
    public function __construct()
    {
        
    }
	
    /**
	* @Route("/settings")
	* @Template()
	*/
    public function indexAction()
    {
	    return array();
    }
	
	/**
	* @Route("/settings/language")
	* @Template()
	*/
    public function languageAction()
    {	
    	  $em = $this->getDoctrine()->getManager();
		$repository =  $em->getRepository('AdminBundle:Languagemaster');
		$language_list = $repository->findAll();
		//var_dump(	$language_list );exit;
		foreach ($language_list as $key => $value) {
			$language[] = array(
				"id"=>$value->getLanguage_master_id(),
				"language_name"=>$value->getLanguage_name(),
				"display_title"=>$value->getDisplay_title(),
				"language_code"=>$value->getLanguage_code(),
			); 
		}
		return array(
			'language_list' => $language
		);
    }
}
