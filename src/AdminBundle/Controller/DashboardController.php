<?php
namespace AdminBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Riazxrazor\Payumoney;
/**
 * @Route("/{domain}")
 */
class DashboardController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $obj = new BaseController();
        $obj->checkSessionAction();
    }

    /**
     * @Route("/index2")
     * @Template()
     */
    public function index2Action()
    {
        require 'vendor/autoload.php';
        $payumoney = new Payumoney\Payumoney([
            'KEY' => 'n0ZYBhaF',
            'SALT'  => 'eqknWQym1s',
            'TEST_MODE'   => true, // optional default to true
            'DEBUG' => FALSE // optional default to false
        ]);
        // All of these parameters are required!
        $params = [
            'txnid'       => '12345678676',
            'amount'      => 10.50,
            'productinfo' => 'A book',
            'firstname'   => 'Peter',
            'email'       => 'aaska@infoware.ws',
            'phone'       => '7405093206',
            'surl'        => 'http://admin-pc/chesscoaching/chesscoach/dashboard',
            'furl'        => 'http://admin-pc/chesscoaching/chesscoach/dashboard',
        ];
        var_dump($payumoney);
        var_dump($params);

        // Redirects to PayUMoney
        $payumoney->pay($params)->send();
        //----------------------------
    }

    /**
     * @Route("/dashboard")
     * @Template()
     */
    public function indexAction()
    {
        $_sql = "SELECT * from coursemaster where is_deleted != 1";
        $coursemaster = $this->firequery($_sql);
        $coursecount = sizeof($coursemaster);

        $_sql = "SELECT * from course_unit where is_deleted != 1";
        $course_unit = $this->firequery($_sql);
        $course_unitcount = sizeof($course_unit);
        
        $_sql = "SELECT * from usermaster where is_deleted != 1";
        $usermaster = $this->firequery($_sql);
        $usercount = sizeof($usermaster);
        
        $_sql = "SELECT * from user_package_relation where is_deleted != 1";
        $user_package_relation = $this->firequery($_sql);
        $user_packagecount = sizeof($user_package_relation);

        $data = null;
        $repository = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Usermaster');
        $user =  $repository->findBy(array('is_deleted' => "0"));

        $newdata = null;
        $repository = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Courseunit');

        if ($user) {

            foreach ($user as  $value) {
                $repository = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Usermaster');
                $usermaster = $repository->findOneBy(
                    array(
                        'usermaster_id' => $value->getUsermaster_id(),
                        'is_deleted' => "0"
                    )
                );

                $firstname = $value->getUser_first_name();
                $lastname = '';
                $email = '';
                $roleid = '';
                $birthdate = "12/07/2018";
                $rolename = 'Guest';

                $guest = '$this->GUEST_ROLE_ID';

                if (isset($usermaster)) {
                    if ($usermaster->getUsermaster_id() != $guest) {
                        $firstname = $usermaster->getUser_first_name();
                        $lastname = $usermaster->getUser_last_name();
                        $email = $usermaster->getUser_email_password();
                        $media_id = $usermaster->getMedia_id();
                        $roleid = $usermaster->getUser_role_id();
                        $birthdate = $usermaster->getBirthdate();

                        $em = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Rolemaster');
                        $userrole =  $em->find($roleid);
                        if ($userrole) {
                            $rolename = $userrole->getRolename();
                        }

                        $live_path = $this->container->getParameter('live_path');
                        $em = $this->getDoctrine()->getManager();
                        $media_info = $em->getRepository("AdminBundle:Mediamaster")
                            ->findOneBy(array("mediamaster_id" => $media_id));

                        if (!empty($media_info)) {
                            $image_url = $live_path . $media_info->getMedia_location() . "/" . $media_info->getMedia_name();
                        } else {
                            $image_url = 'http://pngimages.net/sites/default/files/user-png-image-15189.png';
                        }

                        $userdata[] = array(
                            "firstname" => $firstname,
                            "lastname" => $lastname,
                            "email" => $email,
                            "roleid" => $roleid,
                            "image_url" => $image_url,
                            "role_name" =>  $rolename,
                            "user_id" => $value->getUsermaster_id(),
                            "create_date" => $birthdate

                        );
                    }
                }
                # code...
            }
        }
        $userdata1 = null;
        if (isset($userdata)) {
            $userdata1 = array_reverse($userdata);
        }
        return array("coursecount" => $coursecount, "user_packagecount" => $user_packagecount, "usercount" => $usercount, "course_unitcount" => $course_unitcount, "userdata" => $userdata1);
    }
    /**
     * @Route("/dashboardV1")
     * @Template()
     */
    public function indexV1Action()
    {
        return array();
    }
    /**
     * @Route("/dashboardV2")
     * @Template()
     */
    public function indexV2Action()
    {
        return array();
    }
    /**
     * @Route("/exhibitor")
     * @Template()
     */
    public function exhibitorAction()
    {
        return array();
    }
    /**
     * @Route("/keyEncryption/{string}",defaults = {"string"=""},requirements={"string"=".+"})
     */
    public function keyEncryptionAction($string)
    {
        if ($string != "" && $string != NULL && !empty($string) && ctype_space($string) == false) {
            $key = $this->container->getParameter('key');
            $res = '';
            for ($i = 0; $i < strlen($string); $i++) {
                $c = ord(substr($string, $i));
                $c += ord(substr($key, (($i + 1) % strlen($key))));
                $res .= chr($c & 0xFF);
            }
            return new Response(base64_encode($res));
        }
        return new Response("");
    }
    /**
     * @Route("/keyDecryption/{string}",defaults = {"string"=""},requirements={"string"=".+"})
     */
    public function keyDecryptionAction($string)
    {
        if ($string != "" && $string != NULL && !empty($string) && ctype_space($string) == false) {
            $key = $this->container->getParameter('key');
            $res = '';
            $string = base64_decode($string);
            for ($i = 0; $i < strlen($string); $i++) {
                $c = ord(substr($string, $i));
                $c -= ord(substr($key, (($i + 1) % strlen($key))));
                $res .= chr(abs($c) & 0xFF);
            }
            return new Response($res);
        }
        return new Response("");
    }
}
