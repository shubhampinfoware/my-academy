<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Google_Client;
use Google_Service_YouTube;
use Google_Service_YouTube_LiveBroadcastSnippet;
use DateTime;
use Google_Service_Exception;
use Google_Exception;
use DateTimeZone;
use AdminBundle\Entity\Usermaster;
use AdminBundle\Entity\Coursemaster;
use AdminBundle\Entity\Coursetrainermaster;
use AdminBundle\Entity\Courseunit;
use AdminBundle\Entity\Courseunittag;
use AdminBundle\Entity\Courseunitrelation;
use AdminBundle\Entity\Courseunitlevel;
use AdminBundle\Entity\Courseunitmedia;
use AdminBundle\Entity\Usermeetingurl;

/**
 * @Route("/admin")
 */
class CourseController extends BaseController {

    public function __construct() {
        
    }

    /**
     * @Route("/course")
     * @Template()
     */
    public function indexAction() {

        $language = $all_category_details = null;
        $em = $this->getDoctrine()->getManager();


        $repository = $em->getRepository('AdminBundle:Languagemaster');
        $language_list = $repository->findAll();
        foreach ($language_list as $key => $value) {
            $language[] = array(
                "language_master_id" => $value->getLanguage_master_id(),
                "language_name" => $value->getLanguage_name(),
                "display_title" => $value->getDisplay_title(),
                "language_code" => $value->getLanguage_code(),
            );
        }
        $role_id = $this->get('session')->get('role_id');
        $all_category_details = [];
        $coupon_codes = [];
        if ($role_id == "5c261503d618fc0d0c174c1f") {
            $user_id = $this->get('session')->get('user_id');
            $enrolled = $em->getRepository('AdminBundle:Userregisterenroll')
                    ->findBy(array('is_deleted' => "0", "extra2" => $user_id, 'enrollment_status' => 'Approved'));
            if (!empty($enrolled)) {
                foreach ($enrolled as $key => $value) {
                    $coupon_code = $value->getExtra1();


                    if (!in_array($coupon_code, $coupon_codes)) {
                        $coupon_codes[] = $coupon_code;

                        $coupon_master = $em->getRepository('AdminBundle:Couponmaster')->findOneBy(array('coupon_code' => $coupon_code));
                        if (!empty($coupon_master)) {
                            $coupan_id = $coupon_master->getId();
                            $coupon_applied_list = $em->getRepository('AdminBundle:Coupon_applied_to_object_list')->findOneBy(array('is_deleted' => "0", 'coupon_id' => $coupan_id));
                            if (!empty($coupon_applied_list)) {
                                $subscription_ids = $coupon_applied_list->getRelation_id();

                                foreach ($subscription_ids as $subscription) {
                                    $subscription_master = $em->getRepository('AdminBundle:subscription_master')->find($subscription);
                                    $course_ids = $subscription_master->getCourseId();
                                    foreach ($course_ids as $key => $course) {
                                        $selected_course = $em->getRepository('AdminBundle:Coursemaster')->findOneBy(array('is_deleted' => "0", "id" => $course));
                                        if ($selected_course) {
                                            foreach ($language as $lngkey => $lngval) {
                                                $lang_course_name = $em->getRepository('AdminBundle:Coursemaster')->findOneBy(array('language_id' => $lngval['language_master_id'], "main_coursemaster_id" => $selected_course->getMain_coursemaster_id()));
                                                $course_name = '';
                                                if (!empty($lang_category_name)) {
                                                    $course_name = $lang_course_name->getCourse_name();
                                                }
                                                $lang_wise_category[] = array(
                                                    "language_id" => $lngval['language_master_id'],
                                                    "course_name" => $course_name
                                                );
                                            }

                                            $status = "";
                                            /*  $coursetrainer = $em
                                              ->getRepository('AdminBundle:Coursetrainermaster')
                                              ->findOneBy(array('is_deleted'=>0,'usermaster_id'=>$this->get('session')->get('user_id'),'coursemaster_id'=>$lval['main_coursemaster_id']));
                                              if(isset($coursetrainer)){
                                              $status = $coursetrainer->getIs_accepted();
                                              } */
                                            $all_category_details[] = array(
                                                "coursemaster_id" => $selected_course->getId(),
                                                "course_name" => $selected_course->getCourse_name(),
                                                "course_description" => $selected_course->getCourse_description(),
                                                "main_coursemaster_id" => $selected_course->getMain_coursemaster_id(),
                                                "course_image" => $selected_course->getCourse_image(),
                                                "language_id" => $selected_course->getLanguage_id(),
                                                "lang_wise_course" => $lang_wise_category,
                                                "category_name" => "Chess",
                                                "approvalstatus" => $status
                                            );
                                            $lang_wise_category = null;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            $all_category_details = array_map("unserialize", array_unique(array_map("serialize", $all_category_details)));
            return array("language" => $language, "all_course" => $all_category_details);
        } else {


            $selected_course = $em->getRepository('AdminBundle:Coursemaster')
                    ->findBy(array('is_deleted' => "0"));
            if (count($selected_course) > 0) {
                foreach ($selected_course as $lkey => $lval) {

                    foreach ($language as $lngkey => $lngval) {
                        $lang_course_name = $em->getRepository('AdminBundle:Coursemaster')->findOneBy(array('language_id' => $lngval['language_master_id'], "main_coursemaster_id" => $lval->getMain_coursemaster_id()));
                        $course_name = '';
                        if (!empty($lang_category_name)) {
                            $course_name = $lang_course_name->getCourse_name();
                        }
                        $lang_wise_category[] = array(
                            "language_id" => $lngval['language_master_id'],
                            "course_name" => $course_name
                        );
                    }

                    $status = "";
                    /*  $coursetrainer = $em
                      ->getRepository('AdminBundle:Coursetrainermaster')
                      ->findOneBy(array('is_deleted'=>0,'usermaster_id'=>$this->get('session')->get('user_id'),'coursemaster_id'=>$lval['main_coursemaster_id']));
                      if(isset($coursetrainer)){
                      $status = $coursetrainer->getIs_accepted();
                      } */
                    $all_category_details[] = array(
                        "coursemaster_id" => $lval->getCoursemaster_id(),
                        "course_name" => $lval->getCourse_name(),
                        "course_description" => $lval->getCourse_description(),
                        "main_coursemaster_id" => $lval->getMain_coursemaster_id(),
                        "course_image" => $lval->getCourse_image(),
                        "language_id" => $lval->getLanguage_id(),
                        "lang_wise_course" => $lang_wise_category,
                        "category_name" => "Chess",
                        "approvalstatus" => $status
                    );
                }



                return array("language" => $language, "all_course" => $all_category_details);
            }
        }
        return array("language" => $language);
    }

    /**
     * @Route("/addcourse/{course_id}",defaults={"course_id":""})
     * @Template()
     */
    public function addcourseAction($course_id) {

        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('AdminBundle:Languagemaster');
        $language_list = $repository->findAll();
        //var_dump( $language_list );exit;
        foreach ($language_list as $key => $value) {
            $language[] = array(
                "language_master_id" => $value->getLanguage_master_id(),
                "language_name" => $value->getLanguage_name(),
                "display_title" => $value->getDisplay_title(),
                "language_code" => $value->getLanguage_code(),
            );
        }
        /* $category = $this->getDoctrine()
          ->getManager()
          ->getRepository('AdminBundle:Categorymaster')
          ->findBy(array('is_deleted'=>0,'domain_id'=>$this->get('session')->get('domain_id'))); */
        $category = null;

        // var_dump($course_id);exit;

        if (!empty($course_id) /* || $category_id != '0' */) {

            $selected_course = $em->getRepository('AdminBundle:Coursemaster')
                    ->findBy(array('is_deleted' => "0", "main_coursemaster_id" => $course_id));
            
            /*  $query = "SELECT coursemaster.*,
              media_master.media_location,media_master.media_name
              FROM coursemaster
              JOIN media_master
              ON coursemaster.course_image = media_master.media_master_id
              WHERE coursemaster.is_deleted = 0 AND coursemaster.main_coursemaster_id = '" . $course_id . "'";
              $selected_category = $em->getRepository('AdminBundle:Categorymaster')
              ->findBy(array('is_deleted'=>"0")); */
            return array("language" => $language, "selected_course" => $selected_course, "domain_list" => null, "category" => $category);
        } else {

            return array("language" => $language, "domain_list" => null, "category" => $category);
        }
    }

    /**
     * @Route("/savecourse")
     * @Template()
     */
    public function savecourseAction() {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('AdminBundle:Languagemaster');
        $language_list = $repository->findAll();
        //var_dump( $language_list );exit;
        foreach ($language_list as $key => $value) {
            $language[] = array(
                "language_master_id" => $value->getLanguage_master_id(),
                "language_name" => $value->getLanguage_name(),
                "display_title" => $value->getDisplay_title(),
                "language_code" => $value->getLanguage_code(),
            );
        }

        if ($_REQUEST['save'] == 'save_category' && !empty($_REQUEST['language_id']) && !empty($_REQUEST['course_name'])) {
            $media_id = "";
            //add image
            $category_logo = $_FILES['course_logo'];
            if (isset($_REQUEST['image_hidden'])) {
                $media_id = $_REQUEST['image_hidden'];

                if ($category_logo['name'] != "" && !empty($category_logo['name'])) {
                    $extension = pathinfo($category_logo['name'], PATHINFO_EXTENSION);

                    $media_type_id = $this->mediatype($extension);

                    if (!empty($media_type_id)) {
                        $logo = $category_logo['name'];
                        $tmpname = $category_logo['tmp_name'];
                        $file_path = $this->container->getParameter('file_path');
                        $logo_path = $file_path . '/category';
                        $logo_upload_dir = $this->container->getParameter('upload_dir') . '/category/';

                        $media_id = $this->mediauploadAction($logo, $tmpname, $logo_path, $logo_upload_dir, $media_type_id);
                    } else {
                        $this->get("session")->getFlashBag()->set("error_msg", "Category Logo is Required");
                        return $this->redirect($this->generateUrl("admin_course_addcourse", array("domain" => $this->get('session')->get('domain'), "course_id" => $course_id)));
                    }
                }
            } else {


                if ($category_logo['name'] != "" && !empty($category_logo['name'])) {
                    $extension = pathinfo($category_logo['name'], PATHINFO_EXTENSION);

                    $media_type_id = $this->mediatype($extension);

                    if (!empty($media_type_id)) {
                        $logo = $category_logo['name'];

                        $tmpname = $category_logo['tmp_name'];

                        $file_path = $this->container->getParameter('file_path');

                        $logo_path = $file_path . '/category';

                        $logo_upload_dir = $this->container->getParameter('upload_dir') . '/category/';

                        $media_id = $this->mediauploadAction($logo, $tmpname, $logo_path, $logo_upload_dir, $media_type_id);
                    } else {
                        $this->get("session")->getFlashBag()->set("error_msg", "Course Logo is Required or extension problem");
                        return $this->redirect($this->generateUrl("admin_course_addcourse", array("domain" => $this->get('session')->get('domain'))));
                    }
                }
            }
            $session = new Session();
            $date_time = date("Y-m-d H:i:s");
            $course = new Coursemaster();
            $course->setCourse_name($_REQUEST['course_name']);
            $course->setCourse_catid($_REQUEST['category']);
            $course->setCourse_status('Active');
            $course->setCatalog_display('show');
            $course->setAuto_enroll('yes');
            $course->setCourse_description($_REQUEST['course_description']);
            $course->setCourse_image($media_id);
            $course->setCourse_code("");
            $course->setCourse_price(0);

            $course->setLanguage_id($_REQUEST['language_id']);
            $course->setDomain_id(1);
            $course->setDomain_id(0);

            $course->setCreate_date($date_time);
            $course->setIs_deleted("0");
            if (!empty($_REQUEST['main_coursemaster_id']) && $_REQUEST['main_coursemaster_id'] != '0') {
                /* if(isset($media_id) && !empty($media_id))
                  {
                  $category->set_category_image_id($media_id);
                  } */
                $course->setMain_coursemaster_id($_REQUEST['main_coursemaster_id']);
                $course->setCreate_date($date_time);
                $course->setIs_deleted("0");
                $em->persist($course);
                $em->flush();
            } else {
                /* $_category->set_category_image_id($media_id); */
                $course->setMain_coursemaster_id(0);

                $em->persist($course);
                $em->flush();
                $main_cat = $course->getCoursemaster_id();
                $course->setMain_coursemaster_id($main_cat);
                $course->setCreate_date($date_time);
                $course->setIs_deleted("0");

                $em->persist($course);
                $em->flush();
            }
        }
        $this->get("session")->getFlashBag()->set("success_msg", "Course Added successfully");
        return $this->redirect($this->generateUrl("admin_course_index"));
    }
    /**
     * @Route("/uploadvideo")
     * @Template()
     */
    public function uploadvideoAction() {
        //"authorization: Apisecret 43edcebba4784f639d5bfe544f2677f9ea49abe8ae6348d29c5281c1473bed04",
       $video_cipher_authArray = array(
                
                "authorization: Apisecret CcQEnBzTEbGH3RwnxFKgdhb5a0XZ6ScL3GGQb2HexKHu8cYDw9fVF1jS3axuWDiE",
                "cache-control: no-cache",
               // "postman-token: 534d8f24-a0da-e8c1-0061-4198291674d2"
              );
        $em = $this->getDoctrine()->getManager();
        $videoId_On_Video_cipher = NULL ;
        $videoCipherPassword = "Encrypt@CS@123";
        $InputPassword = $_POST['password'] ;
         $main_course_unit_id = $_POST['main_course_unit_id'];
             $course_id = $_POST['course_id'];
        if($InputPassword == $videoCipherPassword ){
            
            if(!empty($_FILES['video_upload']['name']) && ($_POST['main_course_unit_id'] != 0 ) && ($_FILES['video_upload']['name'] != '') ){
            // var_dump($_FILES);
             $file_name = $_FILES['video_upload']['name'];
            
           //  var_dump($_POST);
          //   exit;
             
             
            $curl = curl_init();
            $step1_url = "https://dev.vdocipher.com/api/videos?title=".$file_name ;
            curl_setopt_array($curl, array(
              CURLOPT_URL => $step1_url,
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "PUT",
              CURLOPT_HTTPHEADER => $video_cipher_authArray,
            ));

            $step1_response = curl_exec($curl);
            $step1_response = json_decode($step1_response);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
              echo "Step 1 Error : cURL Error #:" . $err;
            } else  {
                // Now Step 2  Start    
              //  var_dump($step1_response);
               if($step1_response){
               $step2responseArray  = $step1_response->clientPayload ;
               $videoId_On_Video_cipher  = $step1_response->videoId ;
               $step2responseArray = (array) $step2responseArray ; 
               $tmpfile = $_FILES['video_upload']['tmp_name'];
               $file_name_with_full_path = realpath($tmpfile);
             
                $step2_curlPostArray = array(
                    "policy"=>$step1_response->clientPayload->policy,
                    "key"=>$step1_response->clientPayload->key,
                    "x-amz-signature"=>$step2responseArray['x-amz-signature'],
                    "x-amz-algorithm"=>$step2responseArray['x-amz-algorithm'],
                    "x-amz-date"=>$step2responseArray['x-amz-date'],
                    "x-amz-credential"=>$step2responseArray['x-amz-credential'],
                    "success_action_status"=>201,
                    "success_action_redirect"=>"",
                    "file"=>'@'.$file_name_with_full_path 
                   
                );
              // var_dump(curl_file_create($tmpfile, $_FILES['video_upload']['type'], $filename));exit;
                $step2_curl = curl_init();
                $step2URL = $step1_response->clientPayload->uploadLink;
                $video_cipher_authArray2 =  array(
                        "cache-control: no-cache",
                        "content-type: multipart/form-data"                       
                      );
                curl_setopt_array($step2_curl, array(
                  CURLOPT_URL => $step2URL,
                  CURLOPT_RETURNTRANSFER => true,
                  CURLOPT_ENCODING => "",
                  CURLOPT_MAXREDIRS => 10,
                  CURLOPT_SAFE_UPLOAD => false,
                  CURLOPT_TIMEOUT => 500,
                  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                  CURLOPT_CUSTOMREQUEST => "POST",
                  CURLOPT_POSTFIELDS =>$step2_curlPostArray,
                  CURLOPT_HTTPHEADER => $video_cipher_authArray2,
                ));

                $step2_response = curl_exec($step2_curl);
                $step_2err = curl_error($step2_curl);

                curl_close($step2_curl);

                if ($step_2err) {
                 // echo "cURL Error #:" . $step_2err;exit;
                    $this->get("session")->getFlashBag()->set("error_msg", "Error : " .$step_2err);
                    return $this->redirect($this->generateUrl("admin_course_index"));
                } else {
                    $class = $em
                            ->getRepository('AdminBundle:Courseunit')
                            ->findOneBy(array('is_deleted' => "0", 'id' => $main_course_unit_id));
                    if($class){
                        $class->setYoutubelink($videoId_On_Video_cipher);
                        $class->setPrivateflag(true);                        
                        $em->flush();
                        
                    }
                    $this->get("session")->getFlashBag()->set("success_msg", "Video Added successfully");
                    return $this->redirect($this->generateUrl("admin_course_index"));
                }
                
               }
            }
             
        }
            else if(isset($_POST['uploaded_video_cipher_id']) && $_POST['uploaded_video_cipher_id'] != 0  ){
                $class = $em
                        ->getRepository('AdminBundle:Courseunit')
                        ->findOneBy(array('is_deleted' => "0", 'id' => $main_course_unit_id));
                if($class){
                    $class->setYoutubelink($_POST['uploaded_video_cipher_id']);
                    $class->setPrivateflag(true);                        
                    $em->flush();

                }
                $this->get("session")->getFlashBag()->set("success_msg", "Video Added successfully");
                return $this->redirect($this->generateUrl("admin_course_index"));
            }
            else{
               $this->get("session")->getFlashBag()->set("error_msg", "Video Empty");
            return $this->redirect($this->generateUrl("admin_course_addadminclass",array('course_id'=>$_POST['course_id'],'class_id'=>$_POST['main_course_unit_id']))); 
            }
        }
        else{
            $this->get("session")->getFlashBag()->set("error_msg", "Please Enter correct Password for Encrypt Video");
            return $this->redirect($this->generateUrl("admin_course_addadminclass",array('course_id'=>$_POST['course_id'],'class_id'=>$_POST['main_course_unit_id'])));
        }
        $this->get("session")->getFlashBag()->set("success_msg", "Course Added successfully");
        return $this->redirect($this->generateUrl("admin_course_index"));
    }

    /**
     * @Route("/deletecourse/{course_id}",defaults={"course_id":""})
     * @Template()
     */
    public function deletecourseAction($course_id) {
        $em = $this->getDoctrine()->getManager();
        if ($course_id != '0') {

            $selected_category = $em
                    ->getRepository('AdminBundle:Coursemaster')
                    ->findBy(array('is_deleted' => "0", 'main_coursemaster_id' => $course_id));
            // var_dump($selected_category);exit;
            if (!empty($selected_category)) {
                foreach ($selected_category as $selkey => $selval) {
                    $selected_category_del = $em
                            ->getRepository('AdminBundle:Coursemaster')
                            ->findOneBy(array('is_deleted' => "0", 'id' => $selval->getId()));
                    $selected_category_del->setIs_deleted("1");

                    $em->persist($selected_category_del);
                    $em->flush();
                }
            }
        }
        $this->get("session")->getFlashBag()->set("success_msg", "Course Deleted successfully");
        return $this->redirect($this->generateUrl("admin_course_index"));
    }

    /**
     * @Route("/updatecourse/{course_id}",defaults={"course_id":""})
     * @Template()
     */
    public function updatecourseAction($course_id) {
        $em = $this->getDoctrine()->getManager();
        if (!empty($course_id)) {

            //update image
            $category_logo = $_FILES['course_logo'];

            $media_id = $_REQUEST['image_hidden'];

            if ($category_logo['name'] != "" && !empty($category_logo['name'])) {
                $extension = pathinfo($category_logo['name'], PATHINFO_EXTENSION);

                $media_type_id = $this->mediatype($extension);

                if (!empty($media_type_id)) {
                    $logo = $category_logo['name'];
                    $tmpname = $category_logo['tmp_name'];
                    $file_path = $this->container->getParameter('file_path');
                    $logo_path = $file_path . '/category';
                    $logo_upload_dir = $this->container->getParameter('upload_dir') . '/category/';

                    $media_id = $this->mediauploadAction($logo, $tmpname, $logo_path, $logo_upload_dir, $media_id, $media_type_id);
                } else {
                    $this->get("session")->getFlashBag()->set("error_msg", "Course Logo is Required");
                    return $this->redirect($this->generateUrl("admin_course_addcourse", array("domain" => $this->get('session')->get('domain'), "course_id" => $course_id)));
                }
            }

            $course = $em
                    ->getRepository('AdminBundle:Coursemaster')
                    ->findOneBy(array('is_deleted' => "0", 'main_coursemaster_id' => $course_id));
            if (!empty($course)) {
                $course->setCourse_name($_REQUEST['course_name']);
                //   $course->setCourse_catid(0);
                $course->setCourse_description($_REQUEST['course_description']);
                $course->setCourse_image($media_id);
                $course->setDomain_id(1);


                $em->persist($course);
                $em->flush();
            }
        }
        $this->get("session")->getFlashBag()->set("success_msg", "Course Updated successfully");
        return $this->redirect($this->generateUrl("admin_course_index"));
    }

    /**
     * @Route("/view/{course_id}",defaults={"course_id":""})
     * @Template()
     */
    public function viewAction($course_id) {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('AdminBundle:Languagemaster');
        $language_list = $repository->findAll();
        //var_dump( $language_list );exit;
        foreach ($language_list as $key => $value) {
            $language[] = array(
                "language_master_id" => $value->getId(),
                "language_name" => $value->getLanguage_name(),
                "display_title" => $value->getDisplay_title(),
                "language_code" => $value->getLanguage_code(),
            );
        }
        $selected_course = $em
                ->getRepository('AdminBundle:Coursemaster')
                ->findOneBy(array('is_deleted' => "0", 'main_coursemaster_id' => $course_id));
        $repo = $em->getRepository('AdminBundle:Courseunit');
        $selected_class = $repo->createQueryBuilder()
                        ->field('is_deleted')
                        ->equals('0')
                        ->sort('id', 'desc')
                        ->getQuery()->execute()->toArray();

        $class = null;
        foreach ($selected_class as $value) {
            $img_url = '';
            $query_v = '';
            $views = 0;
            $time = "";
            if ($value->getPrerequisites() == 'youtube') {
                $parts = parse_url($url);
                parse_str($parts['query'], $query);
                $json = file_get_contents("https://www.googleapis.com/youtube/v3/videos?part=snippet%2CcontentDetails%2Cstatistics&id=" . $query['v'] . "&key=AIzaSyAS2T7d5SqW81JNqAmICfTtEVRNiOxtAYY");
                $obj = json_decode($json);
                $id = sizeof($obj->items);
                $category_id = 17;

                $publishedAt = date("d-M-Y");

                if ($id > 0) {
                    $views = $obj->items[0]->statistics->viewCount;
                    $duration = $obj->items[0]->contentDetails->duration;
                    $data2 = substr($duration, 2);
                    $data3 = substr($data2, 0, strlen($data2) - 1);
                    $time = "";
                    if (strpos($data3, 'M') !== false) {
                        $array = explode("M", $data3);
                        if (strpos($array[0], 'H') !== false) {
                            $hours = explode("H", $array[0]);
                            $time = $hours[0] . ":" . $hours[1] . ":" . $array[1];
                        } else {
                            $time = $array[0] . ":" . $array[1];
                        }
                    } else {
                        $time = $data3;
                    }
                    $category_id = $obj->items[0]->snippet->categoryId;
                    $publishedAt = date("d-M-Y", strtotime($obj->items[0]->snippet->publishedAt));
                    $flag = false;
                } else {
                    $flag = true;
                }
                $course_type = $value->getCourse_type();
                if (($time != "0" or $time != 0) and $course_type == "1") {
                    $class_master = $em->getRepository('AdminBundle:Courseunit')
                            ->findOneBy(array('is_deleted' => "0", 'id' => $value->getId()));
                    $class_master->setCourse_type("0");
                    $em = $this->getDoctrine()->getManager();
                    $em->flush();
                    $course_type = "0";
                }

                $json = file_get_contents("https://www.googleapis.com/youtube/v3/videoCategories?part=snippet&id=" . $category_id . "&key=AIzaSyAS2T7d5SqW81JNqAmICfTtEVRNiOxtAYY");

                $obj = json_decode($json);
                $id = sizeof($obj->items);
                if ($id > 0) {
                    $category_name = $obj->items[0]->snippet->title;
                }
                $img_url = 'https://img.youtube.com/vi/' . $query['v'] . '/0.jpg';
                $query_v = $query['v'];
            } elseif ($value->getPrerequisites() == 'zoom') {
                $flag = true;
                $img_url = '';
                $publishedAt = '';
                $meeting_id = $value->getUnitCode();
                $calledFunction = 'meetings/' . $meeting_id;
                $request_url = $this->api_url . $calledFunction . '?access_token=' . $this->access_token;
                $handle = curl_init($request_url);
                $headers = array();
                $headers[] = 'Content-Type: application/json';
                curl_setopt($handle, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
                $response_curl = curl_exec($handle);
                $httpcode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
                $resp = json_decode($response_curl);
                curl_close($handle);
                if ($httpcode == 200) {
                    $publishedAt = $resp->created_at;
                }
            }

            $course_type = $value->getCourse_type();
            if ($time != 0 and $course_type == 1) {
                $class_master = $em->getRepository('AdminBundle:Courseunit')
                        ->findOneBy(array('is_deleted' => "0", 'id' => $value->getId()));
                $class_master->setCourse_type(0);
                $em = $this->getDoctrine()->getManager();
                $em->flush();
                $course_type = 0;
            }
            $class[] = array(
                'unit_id' => $value->getId(),
                'unit_name' => $value->getUnit_name(),
                'course_type' => $value->getCourse_type(),
                'youtubelink' => $value->getYoutubelink(),
                'videoid' => $query_v,
                'language_id' => $value->getLanguage_id(),
                'main_unit_id' => $value->getMainCourseUnitId(),
                'videolength' => $time
            );
        }

        // $course_type = $this->getDoctrine()
        //  ->getManager()
        //  ->getRepository('AdminBundle:Coursetype')
//->findBy(array('is_deleted'=>0));                                  
        $course_type = null;
        // var_dump($selected_course);exit;
        return array('selected_course' => $selected_course, "language" => $language, "selected_class" => $class, "course_type" => $course_type);
    }

    /**
     * @Route("/addclass/{course_id}/{class_id}",defaults={"course_id":"","class_id":""})
     * @Template()
     */
    public function addclassAction($course_id, $class_id) {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('AdminBundle:Languagemaster');
        $language_list = $repository->findAll();
        //var_dump( $language_list );exit;
        foreach ($language_list as $key => $value) {
            $language[] = array(
                "language_master_id" => $value->getId(),
                "language_name" => $value->getLanguage_name(),
                "display_title" => $value->getDisplay_title(),
                "language_code" => $value->getLanguage_code(),
            );
        }
        /* $query= "select * from coursetype where  is_deleted = 0 and coursetype_id != 3";

          $em = $this->getDoctrine()->getManager()->getConnection()->prepare($query);
          $em->execute();
          $course_type = $em->fetchAll(); */

        if (!empty($class_id)) {

            //$query = "SELECT * FROM course_unit WHERE course_unit.is_deleted = 0 AND course_unit.main_course_unit_id = '" . $class_id . "'";

            $em = $this->getDoctrine()->getManager();
            $repository = $em->getRepository('AdminBundle:Courseunit');
            $selected_class = $repository->findBy(array("main_course_unit_id" => $class_id, "is_deleted" => "0"));

            //var_dump($selected_class);exit;            
            return array("language" => $language, "selected_class" => $selected_class, "course_id" => $course_id);
        } else {


            return array("language" => $language, "course_id" => $course_id);
        }
    }

    /**
     * @Route("/addadminclass/{course_id}/{class_id}",defaults={"course_id":"","class_id":""})
     * @Template()
     */
    public function addadminclassAction($course_id, $class_id) {
        $private_flag = false;
        $em = $this->getDoctrine()->getManager();

        $trainermaster = $em->createQueryBuilder()->select('p')
            ->from('AdminBundle:Usermaster', 'p')
            ->where('p.user_role_id = :user_role_id and p.is_deleted = :is_deleted')
            ->setParameter('user_role_id', $this->TRAINER_ROLE_ID)
            ->setParameter('is_deleted', 0)
            ->getQuery()
            ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);

        $repository = $em->getRepository('AdminBundle:Languagemaster');
        $language_list = $repository->findAll();
        //var_dump( $language_list );exit;
        foreach ($language_list as $key => $value) {
            $language[] = array(
                "language_master_id" => $value->getLanguage_master_id(),
                "language_name" => $value->getLanguage_name(),
                "display_title" => $value->getDisplay_title(),
                "language_code" => $value->getLanguage_code(),
            );
        }

        $category_master = $em->getRepository('AdminBundle:Categorymaster')
                ->findBy(array('is_deleted' => "0", 'category_type' => 'category'));
        $course_master_info = $em->getRepository('AdminBundle:Coursemaster')
                ->find($course_id);
        $course_name = '';
        if ($course_master_info) {
            $course_name = $course_master_info->getCourse_name();
        }
        $all_tag = null;
        foreach ($language as $lgkey => $lgval) {
            $data = $all_category = null;
            $all_tag = $all_category_details = null;
        }
        $all_tag = $em->getRepository('AdminBundle:Courseunittag')->findAll();


        $AllCourse = $em->getRepository('AdminBundle:Coursemaster')
                ->findBy(array('is_deleted' => "0"));    
        $all_levels = $em->getRepository('AdminBundle:Classlevel')->findBy(array('is_deleted' => '0', 'level_status' => 'active'));
        //var_dump($em->getRepository('AdminBundle:Classlevel')->findall());exit;
        // echo "<pre>";print_r($data);exit;
        $private_flag = false ;
        $unit_files = [];
        if (!empty($class_id)) {

            //$query = "SELECT * FROM course_unit WHERE course_unit.is_deleted = 0 AND course_unit.main_course_unit_id = '" . $class_id . "'";

            $em = $this->getDoctrine()->getManager();
            $repository = $em->getRepository('AdminBundle:Courseunit');
            $selected_class = $repository->findBy(array("main_course_unit_id" => $class_id, "is_deleted" => "0"));
            //var_dump($selected_class);exit;
            $private_flag = $selected_class[0]->getPrivateflag();
            //----selected class category ----
            $selected_category_list = $selected_course_list = $selected_level_list = [];
            $course_unit_relation_list = $em
                    ->getRepository('AdminBundle:Courseunitrelation')
                    ->findBy(array('is_deleted' => "0", 'course_unit_id' => $class_id, "relation_with" => "category"));
            if ($course_unit_relation_list) {
                foreach ($course_unit_relation_list as $cakey => $caval) {
                    $selected_category_list[] = $caval->getRelation_object_id();
                }
            }
            //--selcted Class Course---------
            $course_unit_relation_list = $em
                    ->getRepository('AdminBundle:Courseunitrelation')
                    ->findBy(array('is_deleted' => "0", 'course_unit_id' => $class_id, "relation_with" => "course"));
            if ($course_unit_relation_list) {
                foreach ($course_unit_relation_list as $cakey => $caval) {
                    $selected_course_list[] = $caval->getRelation_object_id();
                }
            }
            //--selcted Class Levels---------
            $course_level_relation_list = $em
                    ->getRepository('AdminBundle:Courseunitlevel')
                    ->findBy(array('is_deleted' => "0", 'course_unit_id' => $class_id));
            if ($course_level_relation_list) {
                foreach ($course_level_relation_list as $cakey => $caval) {
                    $selected_level_list[] = $caval->getClass_level_id();
                }
            }
            
            // get Course unit media Files 
            $course_unit_media =  $em
                    ->getRepository('AdminBundle:Courseunitmedia')
                    ->findBy(array("course_unit_id"=>$class_id,"course_id"=>$course_id));
            $unit_files = [];
            if($course_unit_media){
                foreach($course_unit_media as $uval){
                    $live_path = $this->container->getParameter('live_path');
                    $imgClass = $this->getmedia1Action($uval->getMedia_id(), $live_path);
                    $unit_files[] = array(
                        "media_id"=>$uval->getMedia_id(),
                        "media_type"=>$uval->getMediaType(),
                        "media_file_name"=>$uval->getUploadFileTitle(),
                        "media_url"=>$imgClass
                    );
                }
            }

            return array(
                "all_levels" => $all_levels, 
                'selected_category' => $selected_category_list, 
                'selected_course_list' => $selected_course_list,
                'selected_level_list' => $selected_level_list,
                "all_course" => $AllCourse,
                "course_name" => $course_name,
                "language" => $language,
                "selected_class" => $selected_class, 
                "private_flag"=>$private_flag,
                "course_id" => $course_id,
                "category" => $category_master, 
                "catdata" => $data,
                "unit_files"=>$unit_files,
                'trainermaster' => $trainermaster);
        } else {
            return array( "unit_files"=>$unit_files,"private_flag"=>$private_flag,"all_course" => $AllCourse, "all_levels" => $all_levels, "course_name" => $course_name, "language" => $language, "course_id" => $course_id, "category" => $category_master, "catdata" => $all_tag, 'trainermaster' => $trainermaster);
        }
    }

    /**
     * @Route("/saveclass")
     * @Template()
     */
    public function saveclassAction() {
        $date_time = date("Y-m-d H:i:s");
        $type = "normal";
        $user_id = $this->get("session")->get("user_id");
        if ($type == "normal") {
            $em = $this->getDoctrine()->getManager();
            $language = $em
                    ->getRepository('AdminBundle:Languagemaster')
                    ->findBy(array('is_deleted' => "0"));

            if ($_REQUEST['save'] == 'save_category' && !empty($_REQUEST['language_id']) && !empty($_REQUEST['class_name'])) {

                $session = new Session();

                $query_str = parse_url($_REQUEST['youtube_link'], PHP_URL_QUERY);
                parse_str($query_str, $query_params);
                $json = file_get_contents('https://www.googleapis.com/youtube/v3/videos?id=' . $query_params['v'] . '&part=contentDetails&key=AIzaSyAS2T7d5SqW81JNqAmICfTtEVRNiOxtAYY');
                $obj = json_decode($json);
                if ($obj->items) {
                    $date_time = date("Y-m-d H:i:s");
                    $class = new Courseunit();
                    $class->setUnit_name($_REQUEST["class_name"]);
                    $class->setCourse_type(0);
                    $class->setYoutubelink($_REQUEST["youtube_link"]);
                    $class->setCoursemaster_id($_REQUEST['course_id']);
                    $class->setCourse_media_id(0);
                    $class->setCourse_unit_media_id(0);
                    $class->setSort_order(0);

                    $class->setStatus("Active");
                    $class->setPrerequisites("Active");
                    $class->setInstruction("");
                    $class->setCreated_by($user_id);
                    $class->setLock_feature("Yes");

                    $class->setLanguage_id($_REQUEST["language_id"]);
                    $class->setDomain_id("1");
                    $class->setIs_deleted("0");

                    $class->setCreate_date($date_time);
                    $class->setUpdated_date($date_time);
                    $class->setAllow_shuffle("Yes");

                    if (!empty($_REQUEST['main_class_id']) && $_REQUEST['main_class_id'] != '0') {
                        /* if(isset($media_id) && !empty($media_id))
                          {
                          $category->set_category_image_id($media_id);
                          } */
                        $class->setMain_course_unit_id($_REQUEST['main_class_id']);
                        $class->setCreate_date($date_time);
                        $em = $this->getDoctrine()->getManager();
                        $em->persist($class);
                        $em->flush();
                    } else {
                        /* $_category->set_category_image_id($media_id); */
                        $class->setMain_course_unit_id(0);
                        $em = $this->getDoctrine()->getManager();
                        $em->persist($class);
                        $em->flush();
                        $main_cat = $class->getCourse_unit_id();
                        $class->setMain_course_unit_id($main_cat);
                        $class->setCreate_date($date_time);
                        //$class->setDomain_id($this->get('session')->get('domain_id'));
                        $em = $this->getDoctrine()->getManager();
                        $em->persist($class);
                        $em->flush();
                    }
                } else {
                    $this->get("session")->getFlashBag()->set("success_msg", "Incorrect URL or video is deleted");
                    return $this->redirect($this->generateUrl("admin_course_view", array("domain" => $this->get('session')->get('domain'), "course_id" => $_REQUEST['course_id'])));
                }
            }
            $this->get("session")->getFlashBag()->set("success_msg", "Class Added successfully");
            return $this->redirect($this->generateUrl("admin_course_view", array("domain" => $this->get('session')->get('domain'), "course_id" => $_REQUEST['course_id'])));
        } else {
            if ($_REQUEST['save'] == 'save_category' && !empty($_REQUEST['language_id']) && !empty($_REQUEST['class_name'])) {

                //youtube interigation

                $startdate = $_REQUEST["startdate"];
                $starttime = $_REQUEST["starttime"];
                //var_dump($starttime);exit;
                $value1 = strtolower($starttime);
                $arr = explode(":", $value1);
                $arr0 = (int) $arr[0];
                $arr1 = $arr[1];
                //   var_dump($arr0);
                $str = substr($arr1, 1);

                // $part0 =(int)$part[0];
                //$part1 = $part[1];
                $starttimemod = (string) $arr0 . ':' . $str;


                $a = new DateTime($starttimemod);
                $b = new DateTime('5:30');
                $interval = $a->diff($b);
                $starttime = $interval->format('%H:%i');


                $dt = new DateTime($startdate . ' ' . $starttime);
                $dt->setTimezone(new \DateTimeZone('UTC'));
                $startdattime = $dt->format('Y-m-d\TH:i:s.u\Z');

                $enddate = $_REQUEST["enddate"];
                $endtime = $_REQUEST["endtime"];

                $value1 = strtolower($endtime);
                $arr = explode(":", $value1);
                $arr0 = (int) $arr[0];
                $arr1 = $arr[1];
                //  var_dump($arr0);
                $str = substr($arr1, 1);

                //  $part0 =(int)$part[0];
//$part1 = $part[1];
                $endtimemod = (string) $arr0 . ':' . $str;

                $a = new DateTime($endtimemod);
                $b = new DateTime('5:30');
                $interval = $a->diff($b);
                $endtime = $interval->format('%H:%i');
                $dt = new DateTime($enddate . ' ' . $endtime);
                $dt->setTimezone(new \DateTimeZone('UTC'));
                $enddattime = $dt->format('Y-m-d\TH:i:s.u\Z');
                $OAUTH2_CLIENT_ID = '255955648270-vrq2e3lu7shh8jfe7rnqlgjomeuh8da3.apps.googleusercontent.com';
                $OAUTH2_CLIENT_SECRET = '8mJFfsB2rX-RtVpXsoM9i2HC';
                $client = new Google_Client();
                $client->setClientId($OAUTH2_CLIENT_ID);
                $client->setClientSecret($OAUTH2_CLIENT_SECRET);
                $client->setScopes('https://www.googleapis.com/auth/youtube');
                $redirect = filter_var('http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'], FILTER_SANITIZE_URL);
                $client->setRedirectUri($redirect);
// Define an object that will be used to make all API requests.
                $youtube = new Google_Service_YouTube($client);
// Check if an auth token exists for the required scopes
                $tokenSessionKey = 'token';
                if (isset($_GET['code'])) {
                    if (strval($_SESSION['state']) !== strval($_GET['state'])) {
                        die('The session state did not match.');
                    }
                    $client->authenticate($_GET['code']);
                    $_SESSION[$tokenSessionKey] = $client->getAccessToken();
                    header('Location: ' . $redirect);
                }
                if ($this->get('session')->get('token') !== null) {
                    $client->setAccessToken($this->get('session')->get('token'));
                }
                //var_dump($this->get('session')->get('token'));exit;
// Check to ensure that the access token was successfully acquired.
                if ($client->getAccessToken()) {
                    try {
                        // Create an object for the liveBroadcast resource's snippet. Specify values
                        // for the snippet's title, scheduled start time, and scheduled end time.
                        $broadcastSnippet = new \Google_Service_YouTube_LiveBroadcastSnippet();
                        $broadcastSnippet->setTitle($_REQUEST['class_name']);
                        $broadcastSnippet->setScheduledStartTime($startdattime);
                        $broadcastSnippet->setScheduledEndTime($enddattime);
                        // Create an object for the liveBroadcast resource's status, and set the
                        // broadcast's status to "private".
                        $status = new \Google_Service_YouTube_LiveBroadcastStatus();
                        $status->setPrivacyStatus($_REQUEST['optradio']);
                        // Create the API request that inserts the liveBroadcast resource.
                        $broadcastInsert = new \Google_Service_YouTube_LiveBroadcast();
                        $broadcastInsert->setSnippet($broadcastSnippet);
                        $broadcastInsert->setStatus($status);
                        $broadcastInsert->setKind('youtube#liveBroadcast');
                        // Execute the request and return an object that contains information
                        // about the new broadcast.

                        $broadcastsResponse = $youtube->liveBroadcasts->insert('snippet,status', $broadcastInsert, array());



                        // Create an object for the liveStream resource's snippet. Specify a value
                        // for the snippet's title.
                        $streamSnippet = new \Google_Service_YouTube_LiveStreamSnippet();
                        $streamSnippet->setTitle('New Stream');
                        // Create an object for content distribution network details for the live
                        // stream and specify the stream's format and ingestion type.
                        $cdn = new \Google_Service_YouTube_CdnSettings();
                        $cdn->setFormat("1080p");
                        $cdn->setIngestionType('rtmp');
                        // Create the API request that inserts the liveStream resource.
                        $streamInsert = new \Google_Service_YouTube_LiveStream();
                        $streamInsert->setSnippet($streamSnippet);
                        $streamInsert->setCdn($cdn);
                        $streamInsert->setKind('youtube#liveStream');
                        // Execute the request and return an object that contains information
                        // about the new stream.

                        $streamsResponse = $youtube->liveStreams->insert('snippet,cdn', $streamInsert, array());
                        // Bind the broadcast to the live stream.
                        $htmlBody = '';
                        $bindBroadcastResponse = $youtube->liveBroadcasts->bind(
                                $broadcastsResponse['id'], 'id,contentDetails', array(
                            'streamId' => $streamsResponse['id'],
                        ));
                        $htmlBody .= "<h3>Added Broadcast</h3><ul>";
                        $htmlBody .= sprintf('<li>%s published at %s (%s)</li>', $broadcastsResponse['snippet']['title'], $broadcastsResponse['snippet']['publishedAt'], $broadcastsResponse['id']);
                        $htmlBody .= '</ul>';
                        $htmlBody .= "<h3>Added Stream</h3><ul>";
                        $htmlBody .= sprintf('<li>%s (%s)</li>', $streamsResponse['snippet']['title'], $streamsResponse['id']);
                        $htmlBody .= '</ul>';
                        $htmlBody .= "<h3>Bound Broadcast</h3><ul>";
                        $htmlBody .= sprintf('<li>Broadcast (%s) was bound to stream (%s).</li>', $bindBroadcastResponse['id'], $bindBroadcastResponse['contentDetails']['boundStreamId']);
                        $htmlBody .= '</ul>';
                        //var_dump($htmlBody);exit;
                    } catch (Google_Service_Exception $e) {
                        $htmlBody = sprintf('<p>A service error occurred: <code>%s</code></p>', htmlspecialchars($e->getMessage()));
                        $data = json_decode($e->getMessage());
                        if ($data->error->code == '400') {
                            $this->get('session')->getFlashBag()->set('error_msg', $data->error->message);
                        } else {
                            $this->get('session')->getFlashBag()->set('error_msg', 'The user is not enabled for live streaming. first enable liveStream from youtube');
                        }
                        return $this->redirect($this->generateUrl("admin_course_addlive", array("domain" => $this->get('session')->get('domain'), "course_id" => $_REQUEST['course_id'])));
                    } catch (Google_Exception $e) {
                        $htmlBody = sprintf('<p>An client error occurred: <code>%s</code></p>', htmlspecialchars($e->getMessage()));
                        var_dump($htmlBody);
                        exit;
                    }

                    $_SESSION[$tokenSessionKey] = $client->getAccessToken();
                } elseif ($OAUTH2_CLIENT_ID == 'REPLACE_ME') {
                    $htmlBody = <<<END
  <h3>Client Credentials Required</h3>
  <p>
    You need to set <code>\$OAUTH2_CLIENT_ID</code> and
    <code>\$OAUTH2_CLIENT_ID</code> before proceeding.
  <p>
END;
                } else {
                    // If the user hasn't authorized the app, initiate the OAuth flow
                    $state = mt_rand();
                    $client->setState($state);
                    $_SESSION['state'] = $state;
                    $authUrl = $client->createAuthUrl();
                    $htmlBody = <<<END
  <h3>Authorization Required</h3>
  <p>You need to <a href="$authUrl">authorize access</a> before proceeding.<p>
END;
                    var_dump($htmlBody);
                    exit;
                }


                $class = new Courseunit();
                $class->setUnit_name($_REQUEST['class_name']);
                $class->setCourse_type(3);
                $class->setYoutubelink('https://www.youtube.com/watch?v=' . $broadcastsResponse['id']);
                $class->setCoursemaster_id($_REQUEST['course_id']);
                $class->setCourse_media_id(0);
                $class->setCourse_unit_media_id(0);
                $class->setSort_order(0);

                $class->setStatus("Active");
                $class->setPrerequisites("Active");
                $class->setInstruction("Active");
                $class->setCreated_by($this->get('session')->get('user_id'));


                $class->setLanguage_id($_REQUEST['language_id']);
                $class->setDomain_id($this->get('session')->get('domain_id'));
                $class->setIs_deleted(0);
                //$class->setDomain_id($session->get('domain_id'));
                $class->setCreate_date($date_time);
                $class->setUpdated_date($date_time);
                $class->setAllow_shuffle("Yes");
                $class->setLock_feature("Yes");
                $em = $this->getDoctrine()->getManager();
                $em->persist($class);
                $em->flush();
                $main_cat = $class->getCourse_unit_id();
                $class->setMain_course_unit_id($main_cat);
                $em = $this->getDoctrine()->getManager();
                $em->persist($class);
                $em->flush();
                $this->get("session")->getFlashBag()->set("success_msg", "Class Added successfully");
                return $this->redirect($this->generateUrl("admin_course_view", array("domain" => $this->get('session')->get('domain'), "course_id" => $_REQUEST['course_id'])));
            }
        }
    }

    /**
     * @Route("/updateclass/{class_id}",defaults={"class_id":""})
     * @Template()
     */
    public function updateclassAction($class_id) {
        if (!empty($class_id)) {

            //update image
            $em = $this->getDoctrine()->getManager();
            $date_time = date("Y-m-d H:i:s");
            $class = $em
                    ->getRepository('AdminBundle:Courseunit')
                    ->findOneBy(array('is_deleted' => "0", 'id' => $class_id));
            if (!empty($class)) {
                $class->setUnit_name($_REQUEST['class_name']);
                $class->setCourse_type("0");
                $class->setYoutubelink($_REQUEST['youtube_link']);
                $class->setUpdated_date($date_time);
                $em = $this->getDoctrine()->getManager();
                $em->persist($class);
                $em->flush();
                $course_id = $class->getCoursemaster_id();
                $this->get("session")->getFlashBag()->set("success_msg", "Class Updated successfully");
                return $this->redirect($this->generateUrl("admin_course_view", array("domain" => $this->get('session')->get('domain'), "course_id" => $course_id)));
            }
        }
    }

    /**
     * @Route("/deleteclass/{course_id}/{class_id}",defaults={"course_id":"","class_id":""})
     * @Template()
     */
    public function deleteclassAction($course_id, $class_id) {
        if ($class_id != '0') {
            $em = $this->getDoctrine()->getManager();
            $selected_category = $em
                    ->getRepository('AdminBundle:Courseunit')
                    ->findBy(array('is_deleted' => "0", 'main_course_unit_id' => $class_id));

            if (!empty($selected_category)) {
                foreach ($selected_category as $selkey => $selval) {
                    $selected_category_del = $em
                            ->getRepository('AdminBundle:Courseunit')
                            ->findOneBy(array('is_deleted' => "0", 'id' => $selval->getId()));
                    $selected_category_del->setIs_deleted("1");
                    $em = $this->getDoctrine()->getManager();
                    $em->remove($selected_category_del);
                    $em->flush();
                }
                $this->get("session")->getFlashBag()->set("success_msg", "Class Deleted successfully");
                return $this->redirect($this->generateUrl("admin_course_view", array("domain" => $this->get('session')->get('domain'), "course_id" => $course_id)));
            }
        }
    }

    /**
     * @Route("/preview/{class_id}",defaults={"class_id":""})
     * @Template()
     */
    public function previewAction($class_id) {
        $em = $this->getDoctrine()->getManager();
        $selected_class = $em
                ->getRepository('AdminBundle:Courseunit')
                ->findOneBy(array('is_deleted' => "0", 'id' => $class_id));
        $query_str = parse_url($selected_class->getYoutubelink(), PHP_URL_QUERY);
        parse_str($query_str, $query_params);
        $videoid = $query_params['v'];

        return array("selected_class" => $selected_class, "videoid" => $videoid);
    }

    /**
     * @Route("/approve")
     */
    public function approveAction() {
        $em = $this->getDoctrine()->getManager();
        $course_id = $_REQUEST["course_id"];
        $flag = $_REQUEST["flag"];
        $user_id = $this->get('session')->get('user_id');
        $coursetrainer = $this->getDoctrine()
                ->getManager()
                ->getRepository('AdminBundle:Coursetrainermaster')
                ->findOneBy(array('is_deleted' => 0, 'usermaster_id' => $user_id, 'coursemaster_id' => $course_id));
        if (isset($coursetrainer)) {
            $status = 1;
            $coursetrainer->setIs_accepted($flag);
            if ($flag == 'rejected') {
                $coursetrainer->setIs_deleted(1);
                $status = 0;
            }
            $em->flush();
        }
        return new Response($status);
    }

    /**
     * @Route("/addlive/{course_id}/{class_id}",defaults={"course_id":"","class_id":""})
     * @Template()
     */
    public function addliveAction($course_id, $class_id) {
        $query = "select * from language_master where is_deleted = 0";

        $em = $this->getDoctrine()->getManager()->getConnection()->prepare($query);
        $em->execute();
        $language = $em->fetchAll();
        $course_type = $this->getDoctrine()
                ->getManager()
                ->getRepository('AdminBundle:Coursetype')
                ->findBy(array('is_deleted' => 0));
        if (!empty($class_id) /* || $category_id != '0' */) {

            $query = "SELECT * FROM course_unit WHERE course_unit.is_deleted = 0 AND course_unit.main_course_unit_id = '" . $class_id . "'";

            $em = $this->getDoctrine()->getManager();
            $connection = $em->getConnection();
            $statement = $connection->prepare($query);
            $statement->execute();
            $selected_class = $statement->fetchAll();

            return array("language" => $language, "selected_class" => $selected_class, "course_type" => $course_type, "course_id" => $course_id);
        } else {


            return array("language" => $language, "course_type" => $course_type, "course_id" => $course_id);
        }
    }

    /**
     * @Route("/saveadminclass")
     * @Template()
     */
    public function saveadminclassAction() {

        $date_time = date("Y-m-d H:i:s");
        $zoom_msg = '';
        $type = "normal";
        $user_id = $this->get("session")->get("user_id");
        $category_arr = $_REQUEST["category"];
        $course_id_arr = $_REQUEST["course_id"];

        $em = $this->getDoctrine()->getManager();

        $language = $em->getRepository('AdminBundle:Languagemaster')
                ->findBy(array('is_deleted' => "0"));
        $category_logo = $_FILES['cover_img'];
        $trainer = $_REQUEST['trainer_id'];

        $media_id = 0;
        $tag = '';
        if (isset($_FILES['cover_img'])) {
            $media_type_id = 1;
            $logo = $category_logo['name'];
            $tmpname = $category_logo['tmp_name'];
            $file_path = $this->container->getParameter('file_path');
            $logo_path = $file_path . '/course';
            $logo_upload_dir = $this->container->getParameter('upload_dir') . '/course/';
            $media_id = $this->mediauploadAction($logo, $tmpname, $logo_path, $logo_upload_dir, $media_type_id);
        }
        if ($_REQUEST['save'] == 'save_category' && !empty($_REQUEST['language_id']) && !empty($_REQUEST['class_name'])) {

            $session = new Session();

            $date_time = date("Y-m-d H:i:s");
            $class = new Courseunit();
            $class->setUnit_name($_REQUEST["class_name"]);
            $class->setCourse_type($_REQUEST['class_type']);
            if (isset($_REQUEST['sessionvideolink']) && $_REQUEST['sessionvideolink'] != "") {
                $class->setYoutubelink($_REQUEST['sessionvideolink']);
            } else {
                $class->setYoutubelink("");
            }
            $class->setCoursemaster_id(0);
            $class->setCourse_media_id(0);
            $class->setClass_cover_image($media_id);
            $class->setCourse_unit_media_id(0);
            $class->setSort_order(0);

            $class->setStatus("Active");
              $class->setSort_order(0);
              $class->setPrivateflag(0);
            if(isset($_REQUEST['class_type']) && ($_REQUEST['class_type'] == 0)){
                  $class->setPrivateflag(1);
            }
            $class->setLock_feature($trainer); // Assigne trainer

            $live_type = '';
            if (isset($_REQUEST['prerequisites']) && ($_REQUEST['prerequisites'] != '' )) {
                $class->setPrerequisites($_REQUEST['prerequisites']);
                $live_type = $_REQUEST['prerequisites'];
            }

            $class->setInstruction($_REQUEST['description']);
            $class->setCreated_by($user_id);


            $class->setLanguage_id($_REQUEST["language_id"]);
            $class->setDomain_id("1");
            $class->setIs_deleted("0");

            $class->setCreate_date($date_time);
            $class->setUpdated_date($date_time);
            $class->setAllow_shuffle("Yes");
            $class->setClass_level_id(0);
            $main_cat = 0;
            if (!empty($_REQUEST['main_class_id']) && $_REQUEST['main_class_id'] != '0') {
                /* if(isset($media_id) && !empty($media_id))
                  {
                  $category->set_category_image_id($media_id);
                  } */
                $class->setMain_course_unit_id($_REQUEST['main_class_id']);
                $class->setCreate_date($date_time);
                $class->setLock_feature($trainer);
                $class->setTag_id($_REQUEST['unit_tag']);
                $em->persist($class);
                $em->flush();
                $main_cat = $_REQUEST['main_class_id'];
            } 
            else {
                /* $_category->set_category_image_id($media_id); */
                $class->setMain_course_unit_id(0);

                $em->persist($class);
                $em->flush();
                $main_cat = $class->getCourse_unit_id();
                $class->setMain_course_unit_id($main_cat);
                $class->setCreate_date($date_time);
                $class->setTag_id($_REQUEST['unit_tag']);
                if (!empty($_REQUEST['unit_tag']) && $_REQUEST['unit_tag'] != '') {
                    $tag = $_REQUEST['unit_tag'];
                }
                //$class->setDomain_id($this->get('session')->get('domain_id'));
                $em = $this->getDoctrine()->getManager();
                $em->persist($class);
                $em->flush();
            }

            if (isset($_REQUEST['new_tag_name']) && $_REQUEST['new_tag_name'] != '') {
                $course_unit_tag = new Courseunittag();
                $course_unit_tag->setTag_name($_REQUEST['new_tag_name']);
                $course_unit_tag->setField_1("");
                $course_unit_tag->setField_2("");
                $em->persist($course_unit_tag);
                $em->flush();

                if ($tag == '') {
                    $tag = $course_unit_tag->getCourse_unit_tag_id();
                } else {
                    $tag = " , " . $course_unit_tag->getCourse_unit_tag_id();
                }
                $class->setTag_id($tag);
                $em->flush();
            }
            //-------------------Category Assign to class -------------------            
            if ($category_arr != NULL && !empty($category_arr)) {
                foreach ($category_arr as $cakey => $caval) {
                    if ($caval != "" && $caval != "0") {
                        $class_catgeory_realtion = new Courseunitrelation();
                        $class_catgeory_realtion->setCourse_unit_id($main_cat);
                        $class_catgeory_realtion->setRelation_with('category');
                        $class_catgeory_realtion->setRelation_object_id($caval);
                        $class_catgeory_realtion->setIs_deleted(0);
                        $class_catgeory_realtion->setField_1('');
                        $class_catgeory_realtion->setField_2('');
                        $em->persist($class_catgeory_realtion);
                        $em->flush();
                    }
                }
            }
            //-----------------Course Assign to class-------------------            
            if ($course_id_arr != NULL && !empty($course_id_arr)) {
                foreach ($course_id_arr as $cokey => $coval) {
                    if ($coval != "" && $coval != "0") {
                        $class_catgeory_realtion = new Courseunitrelation();
                        $class_catgeory_realtion->setCourse_unit_id($main_cat);
                        $class_catgeory_realtion->setRelation_with('course');
                        $class_catgeory_realtion->setRelation_object_id($coval);
                        $class_catgeory_realtion->setIs_deleted(0);
                        $class_catgeory_realtion->setField_1('');
                        $class_catgeory_realtion->setField_2('');
                        $em->persist($class_catgeory_realtion);
                        $em->flush();
                    }
                }
            }
            //----------------- Level to class-------------------            
            if (isset($_REQUEST['level_id']) && $_REQUEST['level_id'] != NULL && !empty($_REQUEST['level_id'])) {
                //Delete OLD Entries
                $Class_level_list = $em
                        ->getRepository('AdminBundle:Courseunitlevel')
                        ->findBy(array('is_deleted' => "0", 'course_unit_id' => $main_cat));
                if ($Class_level_list) {
                    foreach ($Class_level_list as $clkey => $clval) {
                        $clval->setIs_deleted("1");
                        $em->flush();
                    }
                }
                $levelArr = $_REQUEST['level_id'];
                for ($i = 0; $i < count($levelArr); $i++) {
                    $Class_level_list = new Courseunitlevel();
                    $Class_level_list->setCourse_unit_id($main_cat);
                    $Class_level_list->setClass_level_id($levelArr[$i]);
                    $Class_level_list->setIs_deleted("0");
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($Class_level_list);
                    $em->flush();
                                    

                }
            }
          
            //-------Trainer Assign --------
            if ($trainer != NULL) {
                $coursetrainer = $em
                        ->getRepository('AdminBundle:Coursetrainermaster')
                        ->findOneBy(array('is_deleted' => "0", 'usermaster_id' => $trainer, 'coursemaster_id' => $_REQUEST['course_id']));
                if (empty($coursetrainer)) {
                    $Coursetrainermaster = new Coursetrainermaster();
                    $Coursetrainermaster->setUsermaster_id($trainer);
                    $Coursetrainermaster->setCoursemaster_id($main_cat);
                    $Coursetrainermaster->setDomain_id(1);

                    $Coursetrainermaster->setIs_accepted("accepted");
                    $Coursetrainermaster->setIs_deleted("0");
                    $em->persist($Coursetrainermaster);
                    $em->flush();
                }
            }

            //-------------------------Live Type ---------------------            
            if ($live_type == 'zoom') {
                $zoom_usermaster = $em->getRepository("AdminBundle:Userzoomrelation")
                        ->findOneBy(array(
                    "user_id" => $trainer,
                    "is_deleted" => "0",
                        )
                );

                if (!empty($zoom_usermaster)) {

                    $zoom_user_id = $zoom_usermaster->getZoomUserId();
                    $startdate = $_REQUEST['start_date'];
                    $zoom_meeting_created = false;
                    $zoom_error_msg = NULL;
                    $zoom_meeting_id = $media_id = 0;
                    $zoom_trainerID = "CjbvEPzXQAORznGLeziolA";

                    // sendRequest($calledFunction, $data){
                    $calledFunction = 'users/' . $zoom_trainerID . '/meetings';
                    $request_url = $this->api_url . $calledFunction . '?access_token=' . $this->access_token;
                    $data = array(
                        'topic' => $_REQUEST["class_name"],
                        'type' => 2, // scheduled meetings
                        'start_time' => $startdate,
                        'duration' => 30,
                        "timezone" => "Asia/Kolkata",
                        "password" => "123456",
                        "agenda" => $_REQUEST["class_name"],
                        "settings" => array(
                            "host_video" => true,
                            "participant_video" => false,
                            "registration_type" => 3,
                            "approval_type" => 1,
                            //"cn_meeting"=>true,
                            //"in_meeting"=> false,
                            "join_before_host" => false,
                            "mute_upon_entry" => true,
                            // "watermark"=> true,
                            "use_pmi" => false,
                            "audio" => "both",
                            "auto_recording" => "none",
                            "enforce_login" => true,
                            "enforce_login_domains" => ""
                        )
                    );


                    $headers = array();
                    // $headers[] = 'Accept: application/xml';
                    $headers[] = 'Content-Type: application/json';

                    $handle = curl_init($request_url);
                    curl_setopt($handle, CURLOPT_HTTPHEADER, $headers);
                    curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($handle, CURLOPT_POST, true);
                    curl_setopt($handle, CURLOPT_POSTFIELDS, json_encode($data));

                    //curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
                    // curl_setopt($handle, CURLOPT_ENCODING, "");
                    // curl_setopt($handle, CURLOPT_MAXREDIRS, 10);
                    // curl_setopt($handle, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
                    // curl_setopt($handle, CURLOPT_TIMEOUT, 30);

                    $responseCURL = curl_exec($handle);
                    $httpcode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
                    $ZoomResponseData = json_decode($responseCURL);

                    curl_close($handle);

                    if ($httpcode == '201') {
                        // user created ..

                        if ($ZoomResponseData) {

                            $zoom_meeting_id = $ZoomResponseData->id;

                            $zoom_meeting_url = $ZoomResponseData->join_url;
                            $current_user_id = $trainer;
                        }
                    } else {
                        //  var_dump(json_encode($ZoomResponseData));exit;
                        $zoom_error_msg = $ZoomResponseData;
                    }
                    if ($zoom_meeting_id != 0) {

                        $class->setYoutubelink($zoom_meeting_url); // Zoom Link
                        $class->setUnitCode($zoom_meeting_id);
                        $em->flush();
                        $zoom_msg = " Meeting Created ( " . $zoom_meeting_id . " ) ";
                        // after Meeting Created , add meetin registrant
                        $ConsiderSubscriptionIDs = [];
                        $subscription_master = $this->getDoctrine()->getManager()->getRepository('AdminBundle:subscription_master')->findBy(array("is_deleted" => "0"));
                        if ($subscription_master) {
                            foreach ($subscription_master as $subkey => $subval) {

                                $courseidsofSub = $subval->getCourseId();
                                $courseidarray = [];
                                foreach ($courseidsofSub as $key => $ids) {
                                    $courseidarray[] = $ids;
                                }

                                if (!empty($courseidarray) && !empty($course_id_arr)) {
                                    $resultArr = array_intersect($courseidarray, $course_id_arr);
                                    if ($resultArr != NULL) {
                                        $ConsiderSubscriptionIDs[] = $subval->getId();
                                    }
                                }
                            }
                        }
                       
                        $registrantUsers[] = $trainer;
                        if (!empty($ConsiderSubscriptionIDs)) {
                            foreach ($ConsiderSubscriptionIDs as $conkey => $conval) {

                                $user_package_realtion_list = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Userpackagerelation')->findBy(array("is_deleted" => "0"));
                                if ($user_package_realtion_list) {
                                    foreach ($user_package_realtion_list as $uskey => $usval) {
                                        if (!in_array($usval->getUserId(), $registrantUsers))
                                            $registrantUsers[] = $usval->getUserId();
                                    }
                                }
                            }
                        }
                        
                        $need_approveUsers = [];
                        if ($registrantUsers != NULL) {
                            foreach ($registrantUsers as $rkey => $rval) {
                                $RegisterUserInfo = $this->getDoctrine()->getManager()->getRepository('AdminBundle:usermaster')->find($rval);
                                if ($RegisterUserInfo) {

                                    $Meeting_reg_Function = 'meetings/' . $zoom_meeting_id . '/registrants';
                                    $meeting_reg_request_url = $this->api_url . $Meeting_reg_Function;
                                    $curl = curl_init();
                                    $headers = array();
                                    $headers[] = 'content-type: application/json';
                                    $headers[] = 'authorization: Bearer ' . $this->access_token;
                                    $meeting_reg_data = array(
                                        "email" => $RegisterUserInfo->getUser_email_password(),
                                        "first_name" => $RegisterUserInfo->getUser_first_name(),
                                        "last_name" => $RegisterUserInfo->getUser_last_name(),
                                        "phone" => $RegisterUserInfo->getUser_mobile()
                                    );
                                    curl_setopt_array($curl, array(
                                        CURLOPT_URL => $meeting_reg_request_url,
                                        CURLOPT_RETURNTRANSFER => true,
                                        CURLOPT_ENCODING => "",
                                        CURLOPT_MAXREDIRS => 10,
                                        CURLOPT_TIMEOUT => 30,
                                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                        CURLOPT_CUSTOMREQUEST => "POST",
                                        CURLOPT_POSTFIELDS => json_encode($meeting_reg_data),
                                        CURLOPT_HTTPHEADER => $headers
                                    ));

                                    $Meeting_registrant_response = curl_exec($curl);
                                    $Meeting_registrant_err = curl_error($curl);

                                    curl_close($curl);

                                    if ($Meeting_registrant_err) {
                                        //echo "cURL Error #:" . $Meeting_registrant_err;
                                    } else {

                                        $Meeting_registrant_response = json_decode($Meeting_registrant_response);

                                        if (isset($Meeting_registrant_response->registrant_id)) {
                                            $need_approveUsers[] = array(
                                                "id" => $Meeting_registrant_response->registrant_id,
                                                "email" => $RegisterUserInfo->getUser_email_password()
                                            );
                                         //   var_dump($Meeting_registrant_response);
                                           
                                            $joinURL = '';
                                            if(isset($Meeting_registrant_response->join_url)){
                                                $joinURL = $Meeting_registrant_response->join_url;
                                            }
                                            $user_meeting_url = new user_meeting_url();
                                            $user_meeting_url->setUser_id($rval);
                                            $user_meeting_url->setCourse_unit_id($main_cat);
                                            $user_meeting_url->setMeeting_id($zoom_meeting_id);
                                            $user_meeting_url->setMeeting_url($joinURL);
                                            $user_meeting_url->setRegistrant_id($Meeting_registrant_response->registrant_id);
                                            $user_meeting_url->setIs_deleted("0");
                                            $em->persist($user_meeting_url);
                                            $em->flush();
                                        }
                                    }
                                }
                            }
                        }
                        if ($need_approveUsers != NULL) {
                            $curl = curl_init();
                            $Meeting_reg_StatusFunction = 'meetings/' . $zoom_meeting_id . '/registrants/status';
                            $meeting_reg_Statusrequest_url = $this->api_url . $Meeting_reg_StatusFunction;
                            $meeting_registrant_updateData = array(
                                "action" => "approve",
                                "registrants" => $need_approveUsers
                            );

                            curl_setopt_array($curl, array(
                                CURLOPT_URL => $meeting_reg_Statusrequest_url,
                                CURLOPT_RETURNTRANSFER => true,
                                CURLOPT_ENCODING => "",
                                CURLOPT_MAXREDIRS => 10,
                                CURLOPT_TIMEOUT => 30,
                                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                CURLOPT_CUSTOMREQUEST => "PUT",
                                CURLOPT_POSTFIELDS => json_encode($meeting_registrant_updateData),
                                CURLOPT_HTTPHEADER => $headers
                            ));

                            $Meeting_registrant_status_response = curl_exec($curl);
                            $Meeting_registrant_status_err = curl_error($curl);

                            curl_close($curl);

                            if ($Meeting_registrant_status_err) {
                                echo "cURL Error #:" . $Meeting_registrant_status_err;
                                exit;
                            } else {
                               // echo $Meeting_registrant_status_response;
                            }
                           
                            //-------------------------------------------------------------------------------------

                            $curl = curl_init();
                            $Meeting_getRegistrant_Function = 'meetings/' . $zoom_meeting_id . '/registrants?status=approved';
                            $meeting_getRegistrant_url = $this->api_url . $Meeting_getRegistrant_Function;
                            curl_setopt_array($curl, array(
                              CURLOPT_URL => $meeting_getRegistrant_url,
                              CURLOPT_RETURNTRANSFER => true,
                              CURLOPT_ENCODING => "",
                              CURLOPT_MAXREDIRS => 10,
                              CURLOPT_TIMEOUT => 30,
                              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                              CURLOPT_CUSTOMREQUEST => "GET",
                              CURLOPT_HTTPHEADER => $headers
                            ));

                            $Meeting_getRegistrant_response = curl_exec($curl);
                            $err = curl_error($curl);

                            curl_close($curl);

                            if ($err) {
                              echo "cURL Error #:" . $err;
                            } else {
                             // echo $Meeting_getRegistrant_response;
                            }
                            $Meeting_getRegistrant_response = (json_decode($Meeting_getRegistrant_response));
                            $Meeting_getRegistrant_response = $Meeting_getRegistrant_response->registrants;
                           
                            foreach($Meeting_getRegistrant_response as $nval){
                               
                                $need_reg_id = $nval->id;
                                $need_join_url = $nval->join_url;
                                //-------------------------------------------------------------------------------------
                                $user_meeting_url = $this->getDoctrine()->getManager()->getRepository('AdminBundle:user_meeting_url')->findOneBy(array("registrant_id" => $need_reg_id));
                                if($user_meeting_url){
                                    $user_meeting_url->setMeeting_url($need_join_url);
                                    $em->flush();
                                }
                            }
                        }
                    }
                }
            }
        }
        $res_msg = "Class Added successfully";
        if ($live_type == 'zoom') {
            $res_msg = $res_msg . " " . $zoom_msg;
        }
        $course_id_pass = 0;
        $this->get("session")->getFlashBag()->set("success_msg", $res_msg);
        if (count($_REQUEST['course_id']) > 1) {
            $course_id_pass = 0;
        } elseif (count($_REQUEST['course_id']) == 1) {
            $course_id_pass = $_REQUEST['course_id'][0];
        } else {
            $course_id_pass = 0;
        }

        return $this->redirect($this->generateUrl("admin_course_addadminclass", array("domain" => $this->get('session')->get('domain'), "course_id" => 0, "class_id" => $main_cat)));
    }

    /**
     * @Route("/updateadminclass/{class_id}",defaults={"class_id":""})
     * @Template()
     */
    public function updateadminclassAction($class_id) {
        if (!empty($class_id)) {
            $category_arr = $_REQUEST["category"];
            $course_id_arr = $_REQUEST["course_id"];
            //update image
            $em = $this->getDoctrine()->getManager();
            $date_time = date("Y-m-d H:i:s");
            $class = $em
                    ->getRepository('AdminBundle:Courseunit')
                    ->findOneBy(array('is_deleted' => "0", 'main_course_unit_id' => $class_id));
            $trainer = $_REQUEST['trainer_id'];
            if (!empty($class)) {
                $category_logo = $_FILES['cover_img'];
                $media_id = 0;
                
                if (isset($_FILES['cover_img']) && (count($_FILES['cover_img']) > 1 ) && !empty($_FILES['cover_img']['name'])) {
                    $media_type_id = 1;
                   
                    $logo = $category_logo['name'];
                    $tmpname = $category_logo['tmp_name'];
                    $file_path = $this->container->getParameter('file_path');
                    $logo_path = $file_path . '/course';
                    $logo_upload_dir = $this->container->getParameter('upload_dir') . '/course/';
                    $media_id = $this->mediauploadAction($logo, $tmpname, $logo_path, $logo_upload_dir, $media_type_id);
                    $class->setClass_cover_image($media_id);
                }
                if (isset($_REQUEST['sessionvideolink']) && $_REQUEST['sessionvideolink'] != "") {
                    $class->setYoutubelink($_REQUEST['sessionvideolink']);
                } else {
                    $class->setYoutubelink("");
                }
                $class->setUnit_name($_REQUEST['class_name']);
                $class->setCourse_type($_REQUEST['class_type']);
                $class->setInstruction($_REQUEST['description']);
                
                $class->setClass_level_id(0);
                $class->setCourse_media_id(0); //as category_id
                $class->setUpdated_date($date_time);
                $em = $this->getDoctrine()->getManager();
                $em->persist($class);
                $em->flush();
               
                $course_id = $class->getCoursemaster_id();
                $main_cat = $class->getCourse_unit_id();
                //------------- delete OLD course relation to class ------------------
                $course_unit_relation_list = $em
                        ->getRepository('AdminBundle:Courseunitrelation')
                        ->findBy(array('is_deleted' => "0", 'course_unit_id' => $class_id));
                if ($course_unit_relation_list) {
                    foreach ($course_unit_relation_list as $delkey => $delval) {
                        $delval->setIs_deleted("1");
                        $em->flush();
                    }
                }
                //-------------------Category Assign to class -------------------            
                if ($category_arr != NULL && !empty($category_arr)) {
                    foreach ($category_arr as $cakey => $caval) {
                        $class_catgeory_realtion = new Courseunitrelation();
                        $class_catgeory_realtion->setCourse_unit_id($main_cat);
                        $class_catgeory_realtion->setRelation_with('category');
                        $class_catgeory_realtion->setRelation_object_id($caval);
                        $class_catgeory_realtion->setIs_deleted("0");
                        $class_catgeory_realtion->setField_1('');
                        $class_catgeory_realtion->setField_2('');
                        $em = $this->getDoctrine()->getManager();
                        $em->persist($class_catgeory_realtion);
                        $em->flush();
                    }
                }

                //-----------------Course Assign to class-------------------            
                if ($course_id_arr != NULL && !empty($course_id_arr)) {
                    foreach ($course_id_arr as $cokey => $coval) {
                        $class_catgeory_realtion = new Courseunitrelation();
                        $class_catgeory_realtion->setCourse_unit_id($main_cat);
                        $class_catgeory_realtion->setRelation_with('course');
                        $class_catgeory_realtion->setRelation_object_id($coval);
                        $class_catgeory_realtion->setIs_deleted("0");
                        $class_catgeory_realtion->setField_1('');
                        $class_catgeory_realtion->setField_2('');
                        $em = $this->getDoctrine()->getManager();
                        $em->persist($class_catgeory_realtion);
                        $em->flush();
                    }
                }

                //----------------- Level to class-------------------       

                if (isset($_REQUEST['level_id']) && $_REQUEST['level_id'] != NULL && !empty($_REQUEST['level_id'])) {
                    //Delete OLD Entries
                    $Class_level_list = $em
                            ->getRepository('AdminBundle:Courseunitlevel')
                            ->findBy(array('is_deleted' => "0", 'course_unit_id' => $main_cat));
                    if ($Class_level_list) {
                        foreach ($Class_level_list as $clkey => $clval) {
                            $clval->setIs_deleted("1");
                            $em->flush();
                        }
                    }
                    $levelArr = $_REQUEST['level_id'];
                  
                    for ($i = 0; $i < count($levelArr); $i++) {

                        $Class_level_list = new Courseunitlevel();
                        $Class_level_list->setCourse_unit_id($main_cat);
                        $Class_level_list->setClass_level_id($levelArr[$i]);
                        $Class_level_list->setIs_deleted("0");
                        $em = $this->getDoctrine()->getManager();
                        $em->persist($Class_level_list);
                        $em->flush();
                    }
                }
                // //-------Trainer Assign --------

                if ($trainer != NULL && $trainer != 0 && $trainer != "0") {
                    $coursetrainer = $em
                            ->getRepository('AdminBundle:Coursetrainermaster')
                            ->findOneBy(array('is_deleted' => "0", 'usermaster_id' => $trainer, 'coursemaster_id' => $_REQUEST['course_id']));
                    if (empty($coursetrainer)) {
                        $Coursetrainermaster = new Coursetrainermaster();
                        $Coursetrainermaster->setUsermaster_id($trainer);
                        $Coursetrainermaster->setCoursemaster_id($main_cat);
                        $Coursetrainermaster->setDomain_id(1);

                        $Coursetrainermaster->setIs_accepted("accepted");
                        $Coursetrainermaster->setIs_deleted("0");
                        $em->persist($Coursetrainermaster);
                        $em->flush();
                    }
                } else {
                    $coursetrainer = $em
                            ->getRepository('AdminBundle:Coursetrainermaster')
                            ->findBy(array('is_deleted' => "0", 'coursemaster_id' => $main_cat));
                    if (!empty($coursetrainer)) {
                        foreach ($coursetrainer as $cval) {
                            $cval->setIs_deleted("1");
                            $em->persist($cval);
                            $em->flush();
                        }
                    }
                }

                $this->get("session")->getFlashBag()->set("success_msg", "Class Updated successfully");
                return $this->redirect($this->generateUrl("admin_course_addadminclass", array("domain" => $this->get('session')->get('domain'), "course_id" => 0, "class_id" => $main_cat)));
                //return $this->redirect($this->generateUrl("admin_course_viewadmin", array("domain" => $this->get('session')->get('domain'), "course_id" => $course_id)));
            }
        }
    }

    /**
     * @Route("/viewadmin/{course_id}",defaults={"course_id":""})
     * @Template()
     */
    public function viewadminAction($course_id) {
        $selected_courseArr = NULL;
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('AdminBundle:Languagemaster');
        $language_list = $repository->findAll();
        //var_dump( $language_list );exit;
        foreach ($language_list as $key => $value) {
            $language[] = array(
                "language_master_id" => $value->getLanguage_master_id(),
                "language_name" => $value->getLanguage_name(),
                "display_title" => $value->getDisplay_title(),
                "language_code" => $value->getLanguage_code(),
            );
        }

        $trainermaster = $em->createQueryBuilder()->select('p')
                        ->from('AdminBundle:Usermaster', 'p')
                        ->where('p.user_role_id = :user_role_id and p.is_deleted = :is_deleted')
                        ->setParameter('is_deleted', 0)
                        ->setParameter('user_role_id', $this->TRAINER_ROLE_ID)
                        ->getQuery()
                        ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        
        $selected_course = $em
                ->getRepository('AdminBundle:Coursemaster')
                ->findOneBy(array('is_deleted' => "0", 'main_coursemaster_id' => $course_id));
        
        if ($course_id != 0) {

            $selected_class = $em->createQueryBuilder()->select('p')
                            ->from('AdminBundle:Courseunitrelation', 'p')
                            ->where('p.relation_object_id = :relation_object_id and p.relation_with = :relation_with and p.is_deleted = :is_deleted')
                            ->setParameter('is_deleted', 0)
                            ->setParameter('relation_object_id', $course_id)
                            ->setParameter('relation_with', 'course')
                            ->getQuery()
                            ->getResult();
            
        } else {
            $selected_class = $em->createQueryBuilder()->select('p')
                            ->from('AdminBundle:Courseunitrelation', 'p')
                            ->where('p.relation_with = :relation_with and p.is_deleted = :is_deleted')
                            ->setParameter('is_deleted', 0)
                            ->setParameter('relation_with', 'course')
                            ->getQuery()
                            ->getResult();
        }

        $class = null;
        $classIDS = [];
        foreach ($selected_class as $value1) {
            $value = $em->getRepository('AdminBundle:Courseunit')->findOneBy(
                    array(
                        'is_deleted' => "0",
                        'main_course_unit_id' => $value1->getCourse_unit_id()
                    )
            );


            if (!empty($value)) {
                $videoid = "";
                $query_v = "";
                if (!empty($value->getYoutubelink())) {
                    $img_url = '';
                    $url = $value->getYoutubelink();
                    $query_v = '';
                    $views = 0;
                    $time = "";
                    if ($value->getPrerequisites() == 'youtube') {
                        $parts = parse_url($url);

                        if (isset($parts['query'])) {
                            parse_str($parts['query'], $query);
                            $category_id = 17;

                            $publishedAt = date("d-M-Y");
                            $flag = true;

                            $course_type = $value->getCourse_type();
                            if (($time != "0" or $time != 0) and $course_type == "1") {
                                $class_master = $em->getRepository('AdminBundle:Courseunit')
                                        ->findOneBy(array('is_deleted' => "0", 'id' => $value->getMain_course_unit_id()));
                                $em = $this->getDoctrine()->getManager();
                                $em->flush();
                                $course_type = "0";
                            }
                            $img_url = 'https://img.youtube.com/vi/' . $query['v'] . '/0.jpg';
                            $query_v = $query['v'];
                        }
                    } elseif ($value->getPrerequisites() == 'zoom') {
                        $flag = true;
                    }
                    $course_type = $value->getCourse_type();
                    if ($time != "0" and $course_type == 1) {
                        $class_master = $em->getRepository('AdminBundle:Courseunit')
                                ->findOneBy(array('is_deleted' => "0", 'main_course_unit_id' => $value->getMain_course_unit_id()));
                        //$class_master->setCourse_type(0);
                        $em = $this->getDoctrine()->getManager();
                        $em->flush();
                        $course_type = 0;
                    }
                }
                $usermaster = $em->getRepository('AdminBundle:usermaster')
                        ->find($value->getCreated_by());
                $createdby = null;
                if (!empty($usermaster)) {
                    $user_id = $usermaster->getUsermaster_id();
                    $username = $usermaster->getUser_first_name() . " " . $usermaster->getUser_last_name();
                    $role_id = $usermaster->getUser_role_id();
                    $role_master = $em->getRepository('AdminBundle:Rolemaster')
                            ->find($role_id);
                    $role_name = "";
                    if (!empty($role_master)) {
                        $role_name = $role_master->getRolename();
                    }
                    $createdby = array(
                        "user_id" => $user_id,
                        "name" => $username,
                        "role_name" => $role_name
                    );
                }
                //            $category_id = $value->getCourseMediaId(); //as category_id 
                //            $category = $em->getRepository('AdminBundle:Categorymaster')
                //                    ->find($category_id);
                $category_name = $course_name = "";
                //            if (!empty($category)) {
                //                $category_name = $category->getCategory_name();
                //            }
                //-------fetch categories of class --------------
                $category_id_arr = [];
                $course_unit_relation_list = $em
                        ->getRepository('AdminBundle:Courseunitrelation')
                        ->findBy(array('is_deleted' => "0", 'course_unit_id' => $value->getMain_course_unit_id(), 'relation_with' => 'category'));
                if ($course_unit_relation_list) {
                    foreach ($course_unit_relation_list as $delkey => $delval) {
                        $category_id_arr[] = $delval->getRelation_object_id();
                    }
                }

                if ($category_id_arr) {
                    foreach ($category_id_arr as $ckey => $cval) {
                        $category = $em->getRepository('AdminBundle:Categorymaster')->find($cval);
                        if (!empty($category)) {
                            if ($category_name == "") {
                                $category_name = $category->getCategory_name();
                            } else {
                                $category_name = $category_name . " , " . $category->getCategory_name();
                            }
                        }
                    }
                }
                //-------fetch Courses of class --------------
                $course_id_arr = [];
                $course_unit_relation_list = $em
                        ->getRepository('AdminBundle:Courseunitrelation')
                        ->findBy(array('is_deleted' => "0", 'course_unit_id' => $value->getMain_course_unit_id(), 'relation_with' => 'course'));
                if ($course_unit_relation_list) {
                    foreach ($course_unit_relation_list as $delkey => $delval) {
                        $course_id_arr[] = $delval->getRelation_object_id();
                    }
                }

                if ($course_id_arr) {
                    foreach ($course_id_arr as $ckey => $cval) {
                        $courseC = $em->getRepository('AdminBundle:Coursemaster')->find($cval);
                        if (!empty($courseC)) {
                            if ($course_name == "") {
                                $course_name = $courseC->getCourse_name();
                            } else {
                                $course_name = $course_name . " , " . $courseC->getCourse_name();
                            }
                        }
                    }
                }
                if ($value->getCourse_type() == "1") {
                    $course_type = "Live";
                } else {
                    $course_type = "Recorded";
                }
                $imgClass = '';
                if ($value->getClass_cover_image() != "") {
                    $live_path = $this->container->getParameter('live_path');
                    $imgClass = $this->getmedia1Action($value->getClass_cover_image(), $live_path);
                }
                if (!in_array($value->getMain_course_unit_id(), $classIDS)) {
                    $classIDS[] = $value->getMain_course_unit_id();
                    $class[] = array(
                        'unit_id' => $value->getMain_course_unit_id(),
                        'unit_name' => $value->getUnit_name(),
                        'course_type' => $course_type,
                        'course_type_id' => $value->getCourse_type(),
                        "course_id" => $course_id,
                        'youtubelink' => $value->getYoutubelink(),
                        "assignuser_id" => $value->getLock_feature(),
                        'videoid' => $query_v,
                        'language_id' => $value->getLanguage_id(),
                        'main_unit_id' => $value->getMain_course_unit_id(),
                        "create_date" => $value->getCreate_date(),
                        "createdby" => $createdby,
                        "status" => $value->getStatus(),
                        'category_name' => $category_name,
                        'course_name' => $course_name,
                        'assignuser_id' => $value->getLock_feature(),
                        'class_cover_image' => $imgClass
                    );
                }
            }
        }
        if ($selected_course) {
            $selected_courseArr = array(
                "course_name" => $selected_course->getCourse_name()
            );
        }
        // $course_type = $this->getDoctrine()
        //  ->getManager()
        //  ->getRepository('AdminBundle:Coursetype')
        //->findBy(array('is_deleted'=>0));                                  
        $course_type = null;
        //echo"<pre>";print_r($class);exit;

        return array('selected_course' => $selected_courseArr, "language" => $language, "selected_class" => $class, "course_type" => $course_type, "trainermaster" => $trainermaster, "course_id" => $course_id);
    }

    /**
     * @Route("/assigntrainer")
     */
    public function assigntrainerAction() {
        $em = $this->getDoctrine()->getManager();
        $unit_id = $_REQUEST["unit_id"];
        $assign_user_id = $_REQUEST["value"];

        $class_master = $em->getRepository('AdminBundle:Courseunit')
                ->findOneBy(array('is_deleted' => "0", 'id' => $unit_id));
        if (!empty($class_master)) {
            $class_master->setLock_feature($assign_user_id);
            $em->persist($class_master);
            $em->flush();
            return new Response(1);
        }
        return new Response(0);
    }

    /**
     * @Route("/deleteadminclass/{course_id}/{class_id}",defaults={"course_id":"","class_id":""})
     * @Template()
     */
    public function deleteadminclassAction($course_id, $class_id) {
        if ($class_id != '0') {
            $em = $this->getDoctrine()->getManager();
            $selected_category = $em
                    ->getRepository('AdminBundle:Courseunit')
                    ->findBy(array('is_deleted' => "0", 'main_course_unit_id' => $class_id));

            if (!empty($selected_category)) {
                foreach ($selected_category as $selkey => $selval) {
                    $selected_category_del = $em
                            ->getRepository('AdminBundle:Courseunit')
                            ->findOneBy(array('is_deleted' => "0", 'course_unit_id' => $selval->getCourse_unit_id()));
                    $selected_category_del->setIs_deleted("1");
                    $em = $this->getDoctrine()->getManager();
                    $em->remove($selected_category_del);
                    $em->flush();
                }
                //------------- delete OLD course relation to class ------------------
                $course_unit_relation_list = $em
                        ->getRepository('AdminBundle:Courseunitrelation')
                        ->findBy(array('is_deleted' => "0", 'course_unit_id' => $class_id));
                if ($course_unit_relation_list) {
                    foreach ($course_unit_relation_list as $delkey => $delval) {
                        $delval->setIs_deleted(1);
                        $em->flush();
                    }
                }
                $this->get("session")->getFlashBag()->set("success_msg", "Class Deleted successfully");
                return $this->redirect($this->generateUrl("admin_course_viewadmin", array("domain" => $this->get('session')->get('domain'), "course_id" => $course_id)));
            }
        }
    }

    /**
     * @Route("/statusajax")
     */
    public function statusajaxAction() {
        $dm = $this->getDoctrine()->getManager();
        if (isset($_REQUEST['flag']) && isset($_REQUEST['facility_id'])) {
            if (!empty($_REQUEST['facility_id'])) {
                $facility_id = $_REQUEST['facility_id'];
                $facility = $dm
                        ->getRepository("AdminBundle:Courseunit")
                        ->findBy(array("is_deleted" => "0", "id" => $facility_id));
                if ($facility != null) {
                    $curr_status = $facility[0]->getStatus();
                    $status = $curr_status == 'Active' ? 'Inactive' : 'Active';
                    foreach ($facility as $key => $val) {
                        $em = $this->getDoctrine()->getManager();
                        $update = $em->getRepository("AdminBundle:Courseunit")->find($val->getId());
                        $update->setStatus($status);
                        $em->flush();
                    }
                }
            }
        }
        return new Response(1);
    }

    /**
     * @Route("/sendnotificationadminclass/{course_id}/{class_id}",defaults={"course_id":"","class_id":""})
     * @Template()
     */
    public function sendnotificationadminclassAction($course_id, $class_id) {
        if ($class_id != '0') {
            $em = $this->getDoctrine()->getManager();
            $subscription_list = $this->getDoctrine()->getManager()->getRepository('AdminBundle:subscription_master')->findBy(array(
                "is_deleted" => "0"
            ));
            if ($subscription_list) {
                $class_info = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Courseunit')->find($class_id);

                $user_IDS = [];
                $img_url = '';
                $query_v = '';
                $views = 0;
                $time = "";
                $successMail = $failureMail = 0;
                $successEmailAdddress = '';
                $faildEmailAddress = ' ';
                $publishedAt = '';
                if ($class_info->getPrerequisites() == 'youtube') {
                    $url = $class_info->getYoutubelink();
                    $parts = parse_url($url);
                    parse_str($parts['query'], $query);
                    $json = file_get_contents("https://www.googleapis.com/youtube/v3/videos?part=snippet%2CcontentDetails%2Cstatistics&id=" . $query['v'] . "&key=AIzaSyAS2T7d5SqW81JNqAmICfTtEVRNiOxtAYY");
                    $obj = json_decode($json);
                    $id = sizeof($obj->items);
                    $category_id = 17;

                    $publishedAt = date("d-M-Y");

                    if ($id > 0) {
                        $views = $obj->items[0]->statistics->viewCount;
                        $duration = $obj->items[0]->contentDetails->duration;
                        $data2 = substr($duration, 2);
                        $data3 = substr($data2, 0, strlen($data2) - 1);
                        $time = "";
                        if (strpos($data3, 'M') !== false) {
                            $array = explode("M", $data3);
                            if (strpos($array[0], 'H') !== false) {
                                $hours = explode("H", $array[0]);
                                $time = $hours[0] . ":" . $hours[1] . ":" . $array[1];
                            } else {
                                $time = $array[0] . ":" . $array[1];
                            }
                        } else {
                            $time = $data3;
                        }
                        $category_id = $obj->items[0]->snippet->categoryId;
                        $publishedAt = date("d-M-Y", strtotime($obj->items[0]->snippet->publishedAt));
                        $flag = false;
                    } else {
                        $flag = true;
                    }
                    $course_type = $class_info->getCourse_type();
                    if (($time != "0" or $time != 0) and $course_type == "1") {
                        $class_master = $em->getRepository('AdminBundle:Courseunit')
                                ->findOneBy(array('is_deleted' => "0", 'id' => $value->getId()));
                        $class_master->setCourse_type("0");
                        $em = $this->getDoctrine()->getManager();
                        $em->flush();
                        $course_type = "0";
                    }

                    $json = file_get_contents("https://www.googleapis.com/youtube/v3/videoCategories?part=snippet&id=" . $category_id . "&key=AIzaSyAS2T7d5SqW81JNqAmICfTtEVRNiOxtAYY");

                    $obj = json_decode($json);
                    $id = sizeof($obj->items);
                    if ($id > 0) {
                        $category_name = $obj->items[0]->snippet->title;
                    }
                    $img_url = 'https://img.youtube.com/vi/' . $query['v'] . '/0.jpg';
                    $query_v = $query['v'];
                } elseif ($class_info->getPrerequisites() == 'zoom') {
                    $flag = true;
                    $img_url = '';
                    $publishedAt = '';
                    $meeting_id = $value->getUnitCode();
                    $calledFunction = 'meetings/' . $meeting_id;
                    $request_url = $this->api_url . $calledFunction . '?access_token=' . $this->access_token;
                    $handle = curl_init($request_url);
                    $headers = array();
                    $headers[] = 'Content-Type: application/json';
                    curl_setopt($handle, CURLOPT_HTTPHEADER, $headers);
                    curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
                    $response_curl = curl_exec($handle);
                    $httpcode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
                    $resp = json_decode($response_curl);
                    curl_close($handle);
                    if ($httpcode == 200) {
                        $publishedAt = $resp->created_at;
                    }
                }

                foreach ($subscription_list as $skey => $sval) {
                    $course_IDS = $sval->getCourseId();
                    if (in_array($course_id, $course_IDS)) {
                        $user_package_relation = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Userpackagerelation')
                                ->findBy(array("is_deleted" => "0", "package_id" => $sval->getId()));
                        //var_dump($user_package_relation);exit;
                        if ($user_package_relation) {
                            foreach ($user_package_relation as $selkey => $selval) {
                                $user_IDS[] = $selval->getUserid();
                                $user_info = $this->getDoctrine()->getManager()->getRepository('AdminBundle:usermaster')->find($selval->getUserid());
                                $email_address = $user_info->getUser_email_password();
                                if ($email_address != '') {
                                    if ($publishedAt != '')
                                        $email_message = "Your training session is scheduled at " . $publishedAt . " . Please click on this link to join . You will need to download Zoom to view the training session first time";
                                    else
                                        $email_message = "Your training session is scheduled, Please click on this link to join . You will need to download Zoom to view the training session first time";

                                    $headers = 'From: <chesscoaching>' . "\r\n";


                                    $headers .= "X-Sender: testsite < mail@testsite.com >\n";
                                    $headers .= 'X-Mailer: PHP/' . phpversion();
                                    $headers .= "X-Priority: 1\n"; // Urgent message!
                                    $headers .= "Return-Path: mail@testsite.com\n"; // Return path for errors
                                    $headers .= "MIME-Version: 1.0\r\n";
                                    $headers .= "Content-Type: text/html; charset=iso-8859-1\n";
                                    $result = @mail($email_address, "Chesscoaching Session ", $email_message, $headers);
                                    if (!$result) {
                                        $failureMail++;
                                        $faildEmailAddress .= " " . $email_address . " , ";
                                    } else {
                                        $successMail++;
                                        $successEmailAdddress .= " " . $email_address . " , ";
                                    }
                                } else {
                                    
                                }
                            }
                        }
                    }
                }

                $this->get("session")->getFlashBag()->set("success_msg", "Mail sent Successfuly to " . $successMail . " , Success Email Address : " . $successEmailAdddress . " User(s) and " . $failureMail . " User(s) Failed , Fail Email address : " . $faildEmailAddress);
                return $this->redirect($this->generateUrl("admin_course_viewadmin", array("domain" => $this->get('session')->get('domain'), "course_id" => $course_id)));
            } else {
                $this->get("session")->getFlashBag()->set("error_msg", "No Subscription , please Check");
                return $this->redirect($this->generateUrl("admin_course_viewadmin", array("domain" => $this->get('session')->get('domain'), "course_id" => $course_id)));
            }
        }
    }

    /**
     * @Route("/sendmessageadminclass/{course_id}/{class_id}",defaults={"course_id":"","class_id":""})
     * @Template()
     */
    public function sendmessageadminclassAction($course_id, $class_id) {
        if ($class_id != '0') {
            $em = $this->getDoctrine()->getManager();
            $subscription_list = $this->getDoctrine()->getManager()->getRepository('AdminBundle:subscription_master')->findBy(array(
                "is_deleted" => "0"
            ));
            $auth_key = '249885A6wgqU73LYvq5c826926';
            $message = urlencode("Welcome to Chesscoaching. Your OTP verification code is ##OTP##.");
            $sender = 'Chessc';
            if ($subscription_list) {
                $class_info = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Courseunit')->find($class_id);

                $user_IDS = [];
                $img_url = '';
                $query_v = '';
                $views = 0;
                $time = "";
                $successMail = $failureMail = 0;
                $publishedAt = '';
                $successEmailAdddress = '';
                $faildEmailAddress = ' ';
                if ($class_info->getPrerequisites() == 'youtube') {
                    $url = $class_info->getYoutubelink();
                    $parts = parse_url($url);
                    parse_str($parts['query'], $query);
                    $json = file_get_contents("https://www.googleapis.com/youtube/v3/videos?part=snippet%2CcontentDetails%2Cstatistics&id=" . $query['v'] . "&key=AIzaSyAS2T7d5SqW81JNqAmICfTtEVRNiOxtAYY");
                    $obj = json_decode($json);
                    $id = sizeof($obj->items);
                    $category_id = 17;

                    $publishedAt = date("d-M-Y");

                    if ($id > 0) {
                        $views = $obj->items[0]->statistics->viewCount;
                        $duration = $obj->items[0]->contentDetails->duration;
                        $data2 = substr($duration, 2);
                        $data3 = substr($data2, 0, strlen($data2) - 1);
                        $time = "";
                        if (strpos($data3, 'M') !== false) {
                            $array = explode("M", $data3);
                            if (strpos($array[0], 'H') !== false) {
                                $hours = explode("H", $array[0]);
                                $time = $hours[0] . ":" . $hours[1] . ":" . $array[1];
                            } else {
                                $time = $array[0] . ":" . $array[1];
                            }
                        } else {
                            $time = $data3;
                        }
                        $category_id = $obj->items[0]->snippet->categoryId;
                        $publishedAt = date("d-M-Y", strtotime($obj->items[0]->snippet->publishedAt));
                        $flag = false;
                    } else {
                        $flag = true;
                    }
                    $course_type = $class_info->getCourse_type();
                    if (($time != "0" or $time != 0) and $course_type == "1") {
                        $class_master = $em->getRepository('AdminBundle:Courseunit')
                                ->findOneBy(array('is_deleted' => "0", 'id' => $value->getId()));
                        $class_master->setCourse_type("0");
                        $em = $this->getDoctrine()->getManager();
                        $em->flush();
                        $course_type = "0";
                    }

                    $json = file_get_contents("https://www.googleapis.com/youtube/v3/videoCategories?part=snippet&id=" . $category_id . "&key=AIzaSyAS2T7d5SqW81JNqAmICfTtEVRNiOxtAYY");

                    $obj = json_decode($json);
                    $id = sizeof($obj->items);
                    if ($id > 0) {
                        $category_name = $obj->items[0]->snippet->title;
                    }
                    $img_url = 'https://img.youtube.com/vi/' . $query['v'] . '/0.jpg';
                    $query_v = $query['v'];
                } elseif ($class_info->getPrerequisites() == 'zoom') {
                    $flag = true;
                    $img_url = '';
                    $publishedAt = '';
                    $meeting_id = $value->getUnitCode();
                    $calledFunction = 'meetings/' . $meeting_id;
                    $request_url = $this->api_url . $calledFunction . '?access_token=' . $this->access_token;
                    $handle = curl_init($request_url);
                    $headers = array();
                    $headers[] = 'Content-Type: application/json';
                    curl_setopt($handle, CURLOPT_HTTPHEADER, $headers);
                    curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
                    $response_curl = curl_exec($handle);
                    $httpcode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
                    $resp = json_decode($response_curl);
                    curl_close($handle);
                    if ($httpcode == 200) {
                        $publishedAt = $resp->created_at;
                    }
                }

                foreach ($subscription_list as $skey => $sval) {
                    $course_IDS = $sval->getCourseId();
                    if (in_array($course_id, $course_IDS)) {
                        $user_package_relation = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Userpackagerelation')
                                ->findBy(array("is_deleted" => "0", "package_id" => $sval->getId()));
                        //var_dump($user_package_relation);exit;
                        if ($user_package_relation) {
                            foreach ($user_package_relation as $selkey => $selval) {
                                $user_IDS[] = $selval->getUserid();
                                $user_info = $this->getDoctrine()->getManager()->getRepository('AdminBundle:usermaster')->find($selval->getUserid());
                                $email_address = $user_info->getUser_mobile();
                                if ($email_address != '') {
                                    if ($publishedAt != '')
                                        $email_message = "Your session scheduled at " . $publishedAt . " .You will need to download Zoom";
                                    else
                                        $email_message = "Your session scheduled .You will need to download Zoom";



                                    $curl = curl_init();
                                    curl_setopt_array($curl, array(
                                        CURLOPT_URL => "http://api.msg91.com/api/sendhttp.php?country=91&sender=$sender&route=4&mobiles=$email_address&authkey=$auth_key&message=$email_message",
                                        CURLOPT_RETURNTRANSFER => true,
                                        CURLOPT_ENCODING => "",
                                        CURLOPT_MAXREDIRS => 10,
                                        CURLOPT_TIMEOUT => 30,
                                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                        CURLOPT_CUSTOMREQUEST => "POST",
                                        CURLOPT_POSTFIELDS => "",
                                        CURLOPT_SSL_VERIFYHOST => 0,
                                        CURLOPT_SSL_VERIFYPEER => 0,
                                        CURLOPT_HTTPHEADER => array(
                                            "content-type: application/x-www-form-urlencoded"
                                        ),
                                    ));
                                    $response = curl_exec($curl);
                                    $err = curl_error($curl);
                                    curl_close($curl);

                                    $response_data = json_decode($response);
                                    // var_dump($response_data);exit;
                                    if ($err) {
                                        $this->error = "SFND";
                                        $failureMail++;
                                    } else {
                                        if (isset($response_data->type) == 'success') {
                                            $response = true;
                                            $this->error = "SFD";
                                            $successMail++;
                                        } else {
                                            $msg = '';
                                            if (isset($response_data->message)) {
                                                $msg = $response_data->message;
                                            }
                                            $response = $msg;
                                            $this->error = "SFND";
                                            $failureMail++;
                                        }
                                    }
                                } else {
                                    
                                }
                            }
                        }
                    }
                }

                $this->get("session")->getFlashBag()->set("success_msg", "Message sent Successfuly to " . $successMail . " ,  User(s) and " . $failureMail . " User(s) Failed ");
                return $this->redirect($this->generateUrl("admin_course_viewadmin", array("domain" => $this->get('session')->get('domain'), "course_id" => $course_id)));
            } else {
                $this->get("session")->getFlashBag()->set("error_msg", "No Subscription , please Check");
                return $this->redirect($this->generateUrl("admin_course_viewadmin", array("domain" => $this->get('session')->get('domain'), "course_id" => $course_id)));
            }
        }
    }

     /**
     * @Route("/uploadfiletype")
     * @Template()
     */
    public function uploadfiletypeAction() {
        if(!empty($_FILES['filetype_upload']) && ($_POST['main_course_unit_id'] != 0 ) ){            
            $file_name = $_FILES['filetype_upload'];
            //$file_uploadname = $_FILES['filetype_upload'];['name'];
            $file_type = $_POST['pgn_file_type'];
            $main_course_unit_id = $_POST['main_course_unit_id'];
            $course_id = $_POST['course_id'];
            $upload_file_title = $_POST['file_name'];
            $extension = pathinfo($_FILES['filetype_upload']['name'], PATHINFO_EXTENSION);

            $media_type_id = $this->mediatype($extension);

            if (!empty($media_type_id)) {
                $logo = $file_name['name'];
                $tmpname = $file_name['tmp_name'];
                $file_path = $this->container->getParameter('file_path');
                $logo_path = $file_path . '/category';
                $logo_upload_dir = $this->container->getParameter('upload_dir') . '/category/';

                $media_id = $this->mediauploadAction($logo, $tmpname, $logo_path, $logo_upload_dir, $media_type_id);
                
                $courseUnitMedia = new Courseunitmedia();
                $courseUnitMedia->setCourse_id($course_id);
                $courseUnitMedia->setCourse_unit_id($main_course_unit_id);
                $courseUnitMedia->setMedia_id($media_id);
                $courseUnitMedia->setUploadFileTitle($upload_file_title);
                $courseUnitMedia->setMedia_type($file_type);                
               
                $em = $this->getDoctrine()->getManager();
                $em->persist($courseUnitMedia);
                $em->flush();
                
                
            }
        }
       
        return $this->redirect($this->generateUrl("admin_course_index"));
    }
}
