<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\PropertyAccess\PropertyAccess;

use AdminBundle\Entity\Coursetrainermaster;
use AdminBundle\Entity\Usermaster;
use AdminBundle\Entity\Usertrainerrelation;
use AdminBundle\Entity\Walletmaster;
use AdminBundle\Entity\Userzoomrelation;
use AdminBundle\Entity\Userenrollments;
use AdminBundle\Entity\Fideuser;

/**
 * @Route("/")
 */
class UserController extends BaseController
{

    public function __construct()
    { }

    /**
     * @Route("/user")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $role_id = $this->get('session')->get('role_id');
        if ($role_id == $this->GUEST_ROLE_ID) {

            $user_id = $this->get('session')->get('user_id');
            $enrolled = $em->getRepository('AdminBundle:Userregisterenroll')
                ->findBy(array('is_deleted' => "0", "extra2" => $user_id, 'enrollment_status' => 'Approved'));

            if (!empty($enrolled)) {
                foreach ($enrolled as $key => $value) {
                    $usermaster = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Usermaster')->find($value->getAssign_user_id());
                    if (!empty($usermaster)) {
                        $data[] = array(
                            "firstname" => $usermaster->getUser_first_name() . " " . $usermaster->getUser_last_name(),
                            "mobile_no" => $usermaster->getUser_mobile(),
                            "email" => $usermaster->getUser_email_password(),
                            "role_name" => "",
                            "user_id" => $usermaster->getUsermaster_id(),
                            "create_date" => $usermaster->getCreate_date()
                        );
                    }
                }
            }
            return array("usermaster" => $data);
        } else {

            $usermaster = $em->createQueryBuilder()->select('p')
                ->from('AdminBundle:Usermaster', 'p')
                ->where('p.user_role_id = :user_role_id and p.is_deleted = :is_deleted')
                ->setParameter('user_role_id', $this->LEARNER_ROLE_ID)
                ->setParameter('is_deleted', 0)
                ->getQuery()
                ->getResult();
            // \Doctrine\ORM\Query::HYDRATE_ARRAY

            $data = null;
            foreach ($usermaster as $value) {

                $role_id = $value->getUser_role_id();
                if ($role_id != $this->GUEST_ROLE_ID) {
                    $role_id = $this->LEARNER_ROLE_ID;
                    $em = $this->getDoctrine()->getManager();
                    $repository = $em->getRepository('AdminBundle:Rolemaster');
                    $userrole = $repository->find($role_id);
                    $userrolename = $userrole->getRolename();
                    $register_from = '';
                    if ($value->getRegister_from() == "actualuserchesscoachingsite") {
                        $register_from = "Chesscoaching User";
                    } else if ($value->getRegister_from() == "chessbaseuserchesscoachingsite") {
                        $register_from = "Chessbase User";
                    }
                    $data[] = array(
                        "firstname" => $value->getUser_first_name() . " " . $value->getUser_last_name(),
                        "mobile_no" => $value->getUser_mobile(),
                        "email" => $value->getUser_email_password(),
                        "role_name" => $userrolename,
                        "user_id" => $value->getUsermaster_id(),
                        "create_date" => $value->getCreate_date(),
                        "register_from" => $register_from
                    );
                    # code...
                }
            }
            // var_dump($data);exit;
            return array("usermaster" => $data);
        }
    }

    /**
     * @Route("/userrole")
     * @Template()
     */
    public function userroleAction()
    {
        $repository = $this->getDoctrine()->getRepository('AdminBundle:Rolemaster');
        $userroles = $repository->findBy(
            array(
                'role_status' => 'active',
                'is_deleted' => 0
            )
        );

        return array(
            'userroles' => $userroles
        );
    }

    /**
     * @Route("/user/addresslist")
     * @Template()
     */
    public function addresslistAction()
    {
        echo '1';
        exit;
    }

    /**
     * @Route("/user/adduser/{usermaster_id}",defaults={"usermaster_id":""})
     * @Template()
     */
    public function adduserAction($usermaster_id)
    {
        echo $usermaster_id;
        exit;
    }

    /**
     * @Route("/user/deleteuser/{usermaster_id}",defaults={"usermaster_id":""})
     * @Template()
     */
    public function deleteuserAction($usermaster_id)
    {
        echo $usermaster_id;
        exit;
    }

    /**
     * @Route("/user/userquiz/{usermaster_id}",defaults={"usermaster_id":""})
     * @Template()
     */
    public function userquizAction($usermaster_id)
    {
        $usermaster = null;
        $data = null;
        $session = null;
        $option = null;
        $repository = $this->getDoctrine()->getRepository('AdminBundle:Usermaster');
        $usermaster = $repository->find($usermaster_id);
        $em = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();
        $question = NULL;
        $query = "SELECT session FROM `quiz_answer_user` WHERE usermaster_id =" . $usermaster_id . "  and is_deleted = 0 GROUP by session ORDER by quiz_answer_user_id DESC ";
        $statement = $connection->prepare($query);
        $statement->execute();
        $quiz_answer = $statement->fetchAll();
        if (!empty($quiz_answer)) {
            $session = $quiz_answer[0]['session'];

            foreach ($quiz_answer as $value) {
                $quiz_progress = "SELECT * FROM `quiz_progress` WHERE session =" . $value['session'] . " and usermaster_id  =" . $usermaster_id . " and is_deleted = 0";
                $statement = $connection->prepare($quiz_progress);
                $statement->execute();
                $quiz_progress = $statement->fetchAll();

                $language_id = $quiz_progress[0]["language_id"];
                $query1 = "SELECT * FROM `quiz_answer_user` WHERE usermaster_id =" . $usermaster_id . "  and session = " . $value['session'] . " and is_deleted = 0 group by question_id";
                $statement = $connection->prepare($query1);
                $statement->execute();
                $quiz_answer1 = $statement->fetchAll();
                foreach ($quiz_answer1 as $val) {
                    $query2 = "SELECT * FROM `question_master` WHERE main_question_id = " . $val['question_id'] . " and language_id =" . $language_id . " and is_deleted = 0";
                    $statement = $connection->prepare($query2);
                    $statement->execute();
                    $questionview = $statement->fetchAll();
                    //echo ""; print_r($questionview);exit;

                    if (!empty($questionview)) {
                        $question_id = $questionview[0]["question_master_id"];
                        $question_name = $questionview[0]["question_title"];
                        $imageid = $questionview[0]["media_id"];

                        $live_path = $this->container->getParameter('live_path');

                        $file_path = $this->container->getParameter('file_path');
                        if (isset($imageid) && $imageid != 0) {
                            $media1 = $this->getDoctrine()->getRepository('AdminBundle:Mediamaster')->find($imageid);
                            if (!empty($media1)) {
                                $question_image = $live_path . $file_path . $media1->getMedia_location() . $media1->getMedia_name();
                            }
                        }
                        $query1 = "SELECT * FROM `quiz_answer_user` WHERE usermaster_id =" . $usermaster_id . "  and session = " . $value['session'] . " and is_deleted = 0 and question_id=" . $question_id;
                        $statement = $connection->prepare($query1);
                        $statement->execute();
                        $option_quiz_answer1 = $statement->fetchAll();
                        foreach ($option_quiz_answer1 as $val1) {
                            # code...

                            $query2 = "SELECT * FROM `question_option` WHERE main_option_id = " . $val1['option_answer_id'] . " and language_id =" . $language_id . " and is_deleted = 0";
                            $statement = $connection->prepare($query2);
                            $statement->execute();
                            $optionview = $statement->fetchAll();

                            if (!empty($optionview)) {
                                $option_id = $optionview[0]["question_option_id"];
                                $option_name = $optionview[0]['option_title'];
                                $option_img = $optionview[0]['media_id'];
                                $live_path = $this->container->getParameter('live_path');
                                $file_path = $this->container->getParameter('file_path');

                                if (isset($option_img) && $option_img != 0) {
                                    $media1 = $this->getDoctrine()->getRepository('AdminBundle:Mediamaster')->find($option_img);
                                    if (!empty($media1)) {
                                        $option_image = $live_path . $file_path . $media1->getMedia_location() . $media1->getMedia_name();
                                    }
                                }
                                $query = "SELECT * FROM `profilemaster` where is_deleted = 0 and profilemaster_id=" . $optionview[0]["option_tag"];
                                $statement = $connection->prepare($query);
                                $statement->execute();
                                $profile = $statement->fetchAll();
                                $option_profile = $profile[0]["profile_name"];
                                $option[] = array(
                                    "option_id" => $option_id,
                                    "option_name" => $option_name,
                                    "option_image" => $option_image,
                                    "option_profile" => $option_profile
                                );
                                $option_profile = null;
                                $option_image = null;
                                $option_name = null;
                            }
                        }
                        $quiz_id = $val["quiz_id"];

                        $question[] = array(
                            "question_id" => $question_id,
                            "question_name" => $question_name,
                            "question_image" => $question_image,
                            "option" => $option
                        );
                        $option = null;
                    }
                }

                $data[] = array(
                    "session" => $value['session'],
                    "quiz" => $question,
                    "profile_name" => $this->getprofilename($value['session'])
                );
                $question = null;
            }
        }
        //echo"<pre>";print_r($data);exit;
        return array("data" => $data, "user" => $usermaster, "session" => $session);
    }

    /**
     * @Route("/traniner")
     * @Template()
     */
    public function traninerAction()
    {

        $em = $this->getDoctrine()->getManager();

        $repository = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Usermaster');

        $usermaster = $em->createQueryBuilder()->select('p')
            ->from('AdminBundle:Usermaster', 'p')
            ->where('p.user_role_id = :user_role_id and p.is_deleted = :is_deleted')
            ->setParameter('user_role_id', $this->TRAINER_ROLE_ID)
            ->setParameter('is_deleted', 0)
            ->getQuery()
            ->getResult();

        $data = null;
        foreach ($usermaster as $value) {
            $usertrainerrelation = $em->getRepository("AdminBundle:Usertrainerrelation")
                ->findOneBy(
                    array(
                        "user_id" => $value->getUsermaster_id(),
                    )
                );
            if (!empty($usertrainerrelation)) {
                $approvalstatus = $usertrainerrelation->getIs_approved();
            } else {
                $usertrainerrelation = new Usertrainerrelation();
                $usertrainerrelation->setUser_id($value->getUsermaster_id());
                $usertrainerrelation->setIs_chessbase("0");
                $usertrainerrelation->setIs_approved("0");
                $usertrainerrelation->setIs_deleted("0");

                $em->persist($usertrainerrelation);
                $em->flush();
                $approvalstatus = $usertrainerrelation->getIs_approved();
            }

            $role_id = $value->getUser_role_id();
            $em = $this->getDoctrine()->getManager();
            $repository = $em->getRepository('AdminBundle:Rolemaster');
            $userrole = $repository->find($role_id);
            $userrolename = $userrole->getRolename();
            $data[] = array(
                "firstname" => $value->getUser_first_name(),
                "lastname" => $value->getUser_last_name(),
                "email" => $value->getUser_email_password(),
                "role_name" => $userrolename,
                "user_id" => $value->getUsermaster_id(),
                "create_date" => $value->getCreate_date(),
                "approvalstatus" => $approvalstatus,
            );
            # code...
        }
        // var_dump($data);exit;
        return array("usermaster" => $data);
    }

    /**
     * @Route("/organization")
     * @Template()
     */
    public function organizationAction()
    {

        $em = $this->getDoctrine()->getManager();

        $repository = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Usermaster');

        $usermaster = $em->createQueryBuilder()->select('p')
            ->from('AdminBundle:Usermaster', 'p')
            ->where('p.is_deleted = :is_deleted and p.user_role_id = :user_role_id')
            ->setParameter('is_deleted', 0)
            ->setParameter('user_role_id', $this->ORGANIZATION_ROLE_ID)
            ->getQuery()
            ->getResult();

        $data = null;
        foreach ($usermaster as $value) {
            $usertrainerrelation = $em->getRepository("AdminBundle:Usertrainerrelation")
                ->findOneBy(
                    array(
                        "user_id" => $value->getUsermaster_id(),
                    )
                );
            if (!empty($usertrainerrelation)) {
                $approvalstatus = $usertrainerrelation->getIs_approved();
            } else {
                $usertrainerrelation = new Usertrainerrelation();
                $usertrainerrelation->setUser_id($value->getId());
                $usertrainerrelation->setIs_chessbase("0");
                $usertrainerrelation->setIs_approved("0");
                $usertrainerrelation->setIs_deleted("0");

                $em->persist($usertrainerrelation);
                $em->flush();
                $approvalstatus = $usertrainerrelation->getIs_approved();
            }

            $role_id = $value->getUser_role_id();
            $em = $this->getDoctrine()->getManager();
            $repository = $em->getRepository('AdminBundle:Rolemaster');
            $userrole = $repository->find($role_id);
            $userrolename = $userrole->getRolename();
            $data[] = array(
                "firstname" => $value->getUser_first_name(),
                "lastname" => $value->getUser_last_name(),
                "email" => $value->getUser_email_password(),
                "role_name" => $userrolename,
                "user_id" => $value->getId(),
                "create_date" => $value->getCreate_date(),
                "approvalstatus" => $approvalstatus,
            );
            # code...
        }

        return array("usermaster" => $data);
    }

    /**
     * @Route("/traninerinfo/{user_id}",defaults={"user_id":""})
     * @Template()
     */
    public function traninerinfoAction($user_id)
    {
        $role_id = $this->get('session')->get('role_id');
        //var_dump($role_id);exit;   
        $role_master = null;
        $coursetrainer = null;
        if ($role_id == $this->ADMIN_ROLE_ID) {
            $em = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Rolemaster');
            $role = $em->findAll();

            $userroles_new = null;

            if ($role) {
                foreach ($role as $role_) {

                    $role_master[] = array(
                        'role_id' => $role_->getRole_master_id(),
                        'role_name' => $role_->getRolename()
                    );
                }
            }
        }
        if ($role_id == "5b9cb19cff27210948d995a2" or $role_id == $this->LEARNER_ROLE_ID) {
            $em = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Rolemaster');
            $role = $em->findAll();

            $userroles_new = null;

            $guest = $this->GUEST_ROLE_ID;
            $admin = $this->ADMIN_ROLE_ID;

            if ($role) {
                foreach ($role as $role_) {
                    if (strtolower($role_->getRole_master_id()) != $guest && strtolower($role_->getRole_master_id()) != $admin) {
                        $role_master[] = array(
                            'role_id' => $role_->getRole_master_id(),
                            'role_name' => $role_->getRolename()
                        );
                    }
                }
            }
        }

        $wallet = NULL;
        if ($user_id) {
            $usermaster = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Usermaster')->find($user_id);
            // var_dump( $usermaster );exit;

            /* $coursetrainer = $this->getDoctrine()
              ->getManager()
              ->getRepository('AdminBundle:Coursetrainermaster')
              ->findBy(array('is_deleted'=>0,'usermaster_id'=>$user_id)); */
            $data = null;

            $role_id = $usermaster->getUser_role_id();
            $repository = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Rolemaster');
            $userrole = $repository->findOneBy(array('is_deleted' => "0", 'role_master_id' => $role_id));
            $userrolename = $userrole->getRolename();

            $repository = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Walletmaster');
            $user_wallet = $repository->findOneBy(array('is_deleted' => "0", 'user_master_id' => $user_id));

            if ($user_wallet) {
                $wallet = array(
                    "virtual_money" => $user_wallet->getVirtual_money_balance(),
                    "wallet_points" => $user_wallet->getWallet_points(),
                    "cashback" => $user_wallet->getCashback_balance()
                );
            }

            $data[] = array(
                "role_id" => $role_id,
                "firstname" => $usermaster->getUser_first_name(),
                "lastname" => $usermaster->getUser_last_name(),
                "email" => $usermaster->getUser_email_password(),
                "role_name" => $userrolename,
                "user_master_id" => $usermaster->getUsermaster_id(),
                "create_date" => $usermaster->getCreate_date(),
                "status" => $usermaster->getUser_status(),
                "wallet_details" => $wallet,
                "user_bio" => $usermaster->getUser_bio(),
                "achievements" => $usermaster->getAchievements()
            );

            $em = $this->getDoctrine()->getManager();
            $repository = $em->getRepository('AdminBundle:Languagemaster');
            $language_list = $repository->findAll();
            //var_dump( $language_list );exit;
            foreach ($language_list as $key => $value) {
                $language[] = array(
                    "language_master_id" => $value->getLanguage_master_id(),
                    "language_name" => $value->getLanguage_name(),
                    "display_title" => $value->getDisplay_title(),
                    "language_code" => $value->getLanguage_code(),
                );
            }
            $all_course = '';
            $all_category = $em->getRepository('AdminBundle:Coursemaster')
                ->findBy(array('is_deleted' => "0"));
            $all_category_details = null;
            if (count($all_category) > 0) {

                foreach ($all_category as $lkey => $lval) {
                    $parent_category_name = 'No Parent ';

                    $lang_wise_category = "";
                    // fetch product name in all languages
                    //  var_dump($language);exit;
                    foreach ($language as $lngkey => $lngval) {
                        $lang_course_name = $em->getRepository('AdminBundle:Coursemaster')->findOneBy(array('language_id' => $lngval['language_master_id'], "main_coursemaster_id" => $lval->getMain_coursemaster_id()));
                        $course_name = '';
                        if (!empty($lang_category_name)) {
                            $course_name = $lang_course_name->getCourse_name();
                        }
                        $lang_wise_category[] = array(
                            "language_id" => $lngval['language_master_id'],
                            "course_name" => $course_name
                        );
                    }

                    $coursetrainerone = $em
                        ->getRepository('AdminBundle:Coursetrainermaster')
                        ->findOneBy(array('is_deleted' => "0", 'coursemaster_id' => $lval->getCoursemaster_id(), 'usermaster_id' => $user_id));
                    if (isset($coursetrainerone)) {
                        $enroll = "Unenroll";
                    } else {
                        $enroll = "Enroll";
                    }
                    $all_category_details[] = array(
                        "coursemaster_id" => $lval->getCoursemaster_id(),
                        "course_name" => $lval->getCourse_name(),
                        "course_description" => $lval->getCourse_description(),
                        "main_coursemaster_id" => $lval->getMain_coursemaster_id(),
                        "course_image" => $lval->getCourse_image(),
                        "language_id" => $lval->getLanguage_id(),
                        "lang_wise_course" => $lang_wise_category,
                        "category_name" => "Chess",
                        "enroll" => $enroll
                    );
                }
            }
            
            return array("usermaster" => $data, "role_master" => $role_master, "language" => $language, "all_course" => $all_category_details, "user_id" => $user_id, "coursetrainer" => $coursetrainer);
        }
        return array("role_master" => $role_master);
    }

    /**
     * @Route("/organizationinfo/{user_id}",defaults={"user_id":""})
     * @Template()
     */
    public function organizationinfoAction($user_id)
    {
        $role_id = $this->get('session')->get('role_id');
        //var_dump($role_id);exit;   
        $role_master = null;
        $coursetrainer = null;
        if ($role_id == $this->ADMIN_ROLE_ID) {
            $em = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Rolemaster');
            $role = $em->findAll();

            $userroles_new = null;
            if ($role) {
                foreach ($role as $role_) {

                    $role_master[] = array(
                        'role_id' => $role_->getRole_master_id(),
                        'role_name' => $role_->getRolename()
                    );
                }
            }
        }

        if ($role_id == "5b9cb19cff27210948d995a2" or $role_id == $this->LEARNER_ROLE_ID) {
            $em = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Rolemaster');
            $role = $em->findAll();

            $userroles_new = null;

            $guest = $this->GUEST_ROLE_ID;

            $admin = $this->ADMIN_ROLE_ID;

            if ($role) {
                foreach ($role as $role_) {
                    if (strtolower($role_->getRole_master_id()) != $guest && strtolower($role_->getRole_master_id()) != $admin) {
                        $role_master[] = array(
                            'role_id' => $role_->getRole_master_id(),
                            'role_name' => $role_->getRolename()
                        );
                    }
                }
            }
        }

        $wallet = NULL;
        if ($user_id) {
            $usermaster = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Usermaster')->find($user_id);

            /* $coursetrainer = $this->getDoctrine()
              ->getManager()
              ->getRepository('AdminBundle:Coursetrainermaster')
              ->findBy(array('is_deleted'=>0,'usermaster_id'=>$user_id)); */

            $data = null;

            $role_id = $usermaster->getUser_role_id();
            $repository = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Rolemaster');
            $userrole = $repository->findOneBy(array('is_deleted' => "0", 'role_master_id' => $role_id));
            $userrolename = $userrole->getRolename();

            $repository = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Walletmaster');
            $user_wallet = $repository->findOneBy(array('is_deleted' => "0", 'user_master_id' => $user_id));

            if ($user_wallet) {
                $wallet = array(
                    "virtual_money" => $user_wallet->getVirtual_money_balance(),
                    "wallet_points" => $user_wallet->getWallet_points(),
                    "cashback" => $user_wallet-BetCashback_balance()
                );
            }
            $data[] = array(
                "role_id" => $role_id,
                "firstname" => $usermaster->getUser_first_name(),
                "lastname" => $usermaster->getUser_last_name(),
                "email" => $usermaster->getUser_email_password(),
                "role_name" => $userrolename,
                "user_master_id" => $usermaster->getUsermaster_id(),
                "create_date" => $usermaster->getCreate_date(),
                "status" => $usermaster->getUser_status(),
                "wallet_details" => $wallet
            );



            $em = $this->getDoctrine()->getManager();
            $repository = $em->getRepository('AdminBundle:Languagemaster');
            $language_list = $repository->findAll();
            //var_dump( $language_list );exit;
            foreach ($language_list as $key => $value) {
                $language[] = array(
                    "language_master_id" => $value->getId(),
                    "language_name" => $value->getLanguage_name(),
                    "display_title" => $value->getDisplay_title(),
                    "language_code" => $value->getLanguage_code(),
                );
            }
            $all_course = '';
            $all_category = $em->getRepository('AdminBundle:Coursemaster')
                ->findBy(array('is_deleted' => "0"));
            $all_category_details = null;
            if (count($all_category) > 0) {

                foreach ($all_category as $lkey => $lval) {
                    $parent_category_name = 'No Parent ';

                    $lang_wise_category = "";
                    // fetch product name in all languages
                    //  var_dump($language);exit;
                    foreach ($language as $lngkey => $lngval) {
                        $lang_course_name = $em->getRepository('AdminBundle:Coursemaster')->findOneBy(array('language_id' => $lngval['language_master_id'], "main_coursemaster_id" => $lval->getMain_coursemaster_id()));
                        $course_name = '';
                        if (!empty($lang_category_name)) {
                            $course_name = $lang_course_name->getCourse_name();
                        }
                        $lang_wise_category[] = array(
                            "language_id" => $lngval['language_master_id'],
                            "course_name" => $course_name
                        );
                    }


                    $coursetrainerone = $em
                        ->getRepository('AdminBundle:Coursetrainermaster')
                        ->findOneBy(array('is_deleted' => "0", 'coursemaster_id' => $lval->getId(), 'usermaster_id' => $user_id));
                    if (isset($coursetrainerone)) {
                        $enroll = "Unenroll";
                    } else {
                        $enroll = "Enroll";
                    }
                    $all_category_details[] = array(
                        "coursemaster_id" => $lval->getId(),
                        "course_name" => $lval->getCourse_name(),
                        "course_description" => $lval->getCourse_description(),
                        "main_coursemaster_id" => $lval->getMain_coursemaster_id(),
                        "course_image" => $lval->getCourse_image(),
                        "language_id" => $lval->getLanguage_id(),
                        "lang_wise_course" => $lang_wise_category,
                        "category_name" => "Chess",
                        "enroll" => $enroll
                    );
                }
            }
            //var_dump($coursetrainer);exit;
            return array("usermaster" => $data, "role_master" => $role_master, "language" => $language, "all_course" => $all_category_details, "user_id" => $user_id, "coursetrainer" => $coursetrainer);
        }
        // var_dump($data);exit;


        return array("role_master" => $role_master);
    }

    /**
     * @Route("/saveuser")
     * @Template()
     */
    public function saveuserAction()
    {
        // =================this is for trainer only =============

        $user_id = "";
        if (!empty($_REQUEST['first_name'])) {
            $firstname = $_REQUEST["first_name"];
            $last_name = $_REQUEST["last_name"];
            $password = $_REQUEST["password"];
            $email = $_REQUEST["email"];
            $user_bio = isset($_REQUEST["user_bio"]) ? $_REQUEST["user_bio"] : '';
            $achievements = isset($_REQUEST["achievements"]) ? $_REQUEST["achievements"] : '';
            $active = "Active";
            if (isset($_REQUEST["optradio"])) {
                $active = $_REQUEST["optradio"];
            }

            $user_type = $_REQUEST["user_type"];

            $media_id = 0;
            if (isset($_FILES['image']) && !empty($_FILES['image'])) {
                $category_logo = $_FILES['image'];
                $logo = $category_logo['name'];
                $media_type_id = 1;
                $tmpname = $category_logo['tmp_name'];
                $file_path = $this->container->getParameter('file_path');
                $logo_path = $file_path . '/user';
                $logo_upload_dir = $this->container->getParameter('upload_dir') . '/user/';
                $media_id = $this->mediauploadAction($logo, $tmpname, $logo_path, $logo_upload_dir, $media_type_id);
            }

            $em = $this->getDoctrine()->getManager();

            $register_from = 'actualuserchesscoachingsite';
            $session = new Session();

            $domain_id = $session->get('domain_id');

            $date_time = date("Y-m-d H:i:s");
            $usermaster_info_id = new Usermaster();
            $usermaster_info_id->setUser_first_name($firstname);
            $usermaster_info_id->setUser_last_name($last_name);
            $usermaster_info_id->setUser_password(md5($password));
            $usermaster_info_id->setUser_email_password($email);
            $usermaster_info_id->getUser_status($active);
            //$usermaster_info_id->setUser_type('user');
            $usermaster_info_id->setCreate_date($date_time);
            $usermaster_info_id->setUser_role_id($user_type);
            $usermaster_info_id->setDomain_id($domain_id);
            $usermaster_info_id->setUser_bio($user_bio);
            $usermaster_info_id->setAchievements($achievements);
            // $usermaster_info_id->setLanguage_id(1);
            $usermaster_info_id->setIs_deleted(0);
            $usermaster_info_id->setRegister_from($register_from);
            $usermaster_info_id->setExternal_id("0");
            $usermaster_info_id->setMedia_id($media_id);
            $em->persist($usermaster_info_id);
            $em->flush();

            $user_id = $usermaster_info_id->getUsermaster_id();
            $fideuser = new fideuser();
            $fideuser->setUser_id($usermaster_info_id->getUsermaster_id());
            $fideuser->setFide_id(0);
            $fideuser->setIs_deleted(0);
            $em->persist($fideuser);
            //=--------- user from chess base or chess coaching  aor apporved or not ------------
            $usertrainerrelation = new Usertrainerrelation();
            $usertrainerrelation->setUser_id($usermaster_info_id->getUsermaster_id());
            $usertrainerrelation->setIs_chessbase("0");
            $usertrainerrelation->setIs_approved("0");
            $usertrainerrelation->setIs_deleted("0");
            //-------------------------------
            $em->persist($usertrainerrelation);
            $em->flush();
            //-----------------Zoom intsert user trainer ----------------------------
            // sendRequest($calledFunction, $data){
            $calledFunction = 'users';
            $request_url = $this->api_url . $calledFunction . '?access_token=' . $this->access_token;
            $data = array('action' => 'create', 'user_info' => array(
                "email" => $email,
                "type" => "1",
                "first_name" => $firstname,
                "last_name" => $last_name,
                "password" => $password
            ));

            $headers = array();
            // $headers[] = 'Accept: application/xml';
            $headers[] = 'Content-Type: application/json';

            $handle = curl_init($request_url);
            curl_setopt($handle, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($handle, CURLOPT_POST, true);
            curl_setopt($handle, CURLOPT_POSTFIELDS, json_encode($data));

            //curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
            // curl_setopt($handle, CURLOPT_ENCODING, "");
            // curl_setopt($handle, CURLOPT_MAXREDIRS, 10);
            // curl_setopt($handle, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
            // curl_setopt($handle, CURLOPT_TIMEOUT, 30);

            $responseCURL = curl_exec($handle);
            $httpcode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
            $ZoomResponseData = json_decode($responseCURL);
            curl_close($handle);
            if ($httpcode == '201') {
                // user created ..

                if ($ZoomResponseData) {

                    $zoom_user_id = $ZoomResponseData->id;
                    $current_user_id = $usermaster_info_id->getId();
                    $userzoomrelation = new Userzoomrelation();
                    $userzoomrelation->setUser_id($current_user_id);
                    $userzoomrelation->setZoomUserId($zoom_user_id);
                    $userzoomrelation->setZoomInsertFlag("created");
                    $userzoomrelation->setZoomInsertStatus("");
                    $userzoomrelation->setIs_deleted("0");
                    $em->persist($userzoomrelation);
                    $em->flush();
                }
            }
            //---------User Wallet Creation--------------
            $wallet_code = $usermaster_info_id->getUsermaster_id() . "_" . substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 5)), 0, 5);
            $wallet_master = new Walletmaster();
            $wallet_master->setWallet_code($wallet_code);
            $wallet_master->setUser_master_id($usermaster_info_id->getUsermaster_id());
            $wallet_master->setVirtual_money_balance(0);
            $wallet_master->setActual_money_balance(0);
            $wallet_master->setCashback_balance(0);
            $wallet_master->setWallet_points(0);
            $wallet_master->setLast_updated_wallet_datetime(date("Y-m-d H:i:s"));
            $wallet_master->setWallet_member_tag_id(1);
            $wallet_master->setIs_deleted(0);
            $em->persist($wallet_master);
            $em->flush();
        }
        $this->get("session")->getFlashBag()->set("success_msg", "User Register successfully");
        return $this->redirect($this->generateUrl("admin_user_traninerinfo", array("domain" => $this->get('session')->get('domain'), "user_id" => $user_id)));
    }

    /**
     * @Route("/updateuser/{user_id}",defaults={"user_id":""})
     * @Template()
     */
    public function updateuserAction($user_id)
    {

        if (!empty($user_id)) {
            $session = new Session();
            $firstname = $_REQUEST["first_name"];
            $last_name = $_REQUEST["last_name"];
            $password = $_REQUEST["password"];

            $user_bio = isset($_REQUEST["user_bio"]) ? $_REQUEST["user_bio"] : '';
            $achievements = isset($_REQUEST["achievements"]) ? $_REQUEST["achievements"] : '';

            $email = $_REQUEST["email"];
            $active = "Active";
            if (isset($_REQUEST["optradio"])) {
                $active = $_REQUEST["optradio"];
            }

            $media_id = 0;
            if (isset($_FILES['image']) && !empty($_FILES['image'])) {
                $category_logo = $_FILES['image'];
                $logo = $category_logo['name'];
                $media_type_id = 1;
                $tmpname = $category_logo['tmp_name'];
                $file_path = $this->container->getParameter('file_path');
                $logo_path = $file_path . '/user';
                $logo_upload_dir = $this->container->getParameter('upload_dir') . '/user/';
                $media_id = $this->mediauploadAction($logo, $tmpname, $logo_path, $logo_upload_dir, $media_type_id);
            }

            $user_type = $_REQUEST["user_type"];
            $em = $this->getDoctrine()->getManager();
            $usermaster_info_id = $em
                ->getRepository('AdminBundle:Usermaster')
                ->findOneBy(array('is_deleted' => "0", 'usermaster_id' => $user_id));
            if (!empty($usermaster_info_id)) {
                $usermaster_info_id->setUser_first_name($firstname);
                $usermaster_info_id->setUser_last_name($last_name);
                if ($password != "") {
                    $usermaster_info_id->setUser_password(md5($password));
                }
                $usermaster_info_id->setUser_email_password($email);
                $usermaster_info_id->setUser_status($active);
                $usermaster_info_id->setUser_bio($user_bio);
                $usermaster_info_id->setAchievements($achievements);

                if($media_id != 0){
                    $usermaster_info_id->setMedia_id($media_id);
                }

                $usermaster_info_id->setUser_role_id($user_type);
                if ($user_id == $this->get('session')->get('user_id')) {
                    $this->get('session')->set('role_id', $user_type);
                    $role = $em
                        ->getRepository('AdminBundle:Rolemaster')
                        ->find(
                            $user_type
                        );
                    $this->get('session')->set('role', $role->getRolename());
                }

                $em->persist($usermaster_info_id);
                $em->flush();
                $this->get("session")->getFlashBag()->set("success_msg", "User Update successfully");
            }
        }

        return $this->redirect($this->generateUrl("admin_user_traninerinfo", array("domain" => $this->get('session')->get('domain'), "user_id" => $user_id)));
    }

    /**
     * @Route("/enroll")
     */
    public function enrollAction()
    {
        $em = $this->getDoctrine()->getManager();
        $course_id = $_REQUEST["course_id"];
        $user_id = $_REQUEST["user_id"];
        $coursetrainer = $em
            ->getRepository('AdminBundle:Coursetrainermaster')
            ->findOneBy(array('is_deleted' => "0", 'usermaster_id' => $user_id, 'coursemaster_id' => $course_id));
        if (isset($coursetrainer)) {
            $coursetrainer->setIs_deleted("1");
            $em->flush();
            $status = 0;
        } else {
            $Coursetrainermaster = new Coursetrainermaster();
            $Coursetrainermaster->setUsermaster_id($user_id);
            $Coursetrainermaster->setCoursemaster_id($course_id);
            $Coursetrainermaster->setDomain_id(1);

            $Coursetrainermaster->setIs_accepted("accepted");
            $Coursetrainermaster->setIs_deleted("0");

            $em->persist($Coursetrainermaster);
            $em->flush();
            $status = 1;
        }
        return new Response($status);
    }

    /**
     * @Route("/userenroll")
     */
    public function userenrollAction()
    {
        $em = $this->getDoctrine()->getManager();
        $course_id = $_REQUEST["course_id"];
        $user_id = $_REQUEST["user_id"];
        $userenrollments = $this->getDoctrine()
            ->getManager()
            ->getRepository('AdminBundle:Userenrollments')
            ->findOneBy(array('is_deleted' => 0, 'usermaster_id' => $user_id, 'coursemaster_id' => $course_id));
        if (isset($userenrollments)) {
            $userenrollments->setIs_deleted(1);
            $em->flush();
            $status = 0;
        } else {
            $userenrollments = new Userenrollments();
            $userenrollments->setusermaster_id($user_id);
            $userenrollments->setCoursemaster_id($course_id);
            $userenrollments->setDomain_id($this->get('session')->get('domain_id'));
            $userenrollments->setCourse_unit_domain_relation_id(0);
            $userenrollments->setUser_enrollments_code(0);
            $userenrollments->setSubscription_start_date(date("Y-m-d H:i:s"));
            $userenrollments->setEnrolled_by($this->get('session')->get('user_id'));
            $userenrollments->setCourse_progress(0);
            $userenrollments->setUser_enrollments_status("Active");
            $userenrollments->setTransacation_id(0);
            $userenrollments->setCreate_date(date("Y-m-d H:i:s"));
            $userenrollments->setModified_date(date("Y-m-d H:i:s"));
            $userenrollments->setCompletion_date(date("Y-m-d H:i:s"));

            $userenrollments->setIs_deleted(0);
            $em = $this->getDoctrine()->getManager();
            $em->persist($userenrollments);
            $em->flush();
            $status = 1;
        }
        return new Response($status);
    }

    /**
     * @Route("/userinfo/{user_id}",defaults={"user_id":""})
     * @Template()
     */
    public function userinfoAction($user_id)
    {
        $role_id = $this->get('session')->get('role_id');
        $data = null;
        //var_dump($role_id);exit;   
        $role_master = null;
        $coursetrainer = null;
        if ($role_id == $this->ADMIN_ROLE_ID) {
            $em = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Rolemaster');
            $role = $em->findAll();
            $userroles_new = null;
            if ($role) {
                foreach ($role as $role_) {

                    $role_master[] = array(
                        'role_id' => $role_->getRole_master_id(),
                        'role_name' => $role_->getRolename()
                    );
                }
            }
        }
        if ($role_id == "5b9cb19cff27210948d995a2" or $role_id == $this->LEARNER_ROLE_ID) {
            $em = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Rolemaster');
            $role = $em->findAll();
            $userroles_new = null;
            $guest = $this->GUEST_ROLE_ID;
            $admin = $this->ADMIN_ROLE_ID;
            if ($role) {
                foreach ($role as $role_) {
                    if (strtolower($role_->getRole_master_id()) != $guest && strtolower($role_->getRole_master_id()) != $admin) {
                        $role_master[] = array(
                            'role_id' => $role_->getRole_master_id(),
                            'role_name' => $role_->getRolename()
                        );
                    }
                }
            }
        }
        $wallet = NULL;
        if (!empty($user_id)) {

            $usermaster = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Usermaster')->find($user_id);

            /* $coursetrainer = $this->getDoctrine()
              ->getManager()
              ->getRepository('AdminBundle:Coursetrainermaster')
              ->findBy(array('is_deleted'=>0,'usermaster_id'=>$user_id)); */


            $role_id = $usermaster->getUser_role_id();
            $repository = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Rolemaster');
            $userrole = $repository->findOneBy(array('is_deleted' => "0", 'role_master_id' => $role_id));
            $userrolename = $userrole->getRolename();
            $user_wallet = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Walletmaster')->findOneBy(array('is_deleted' => "0", 'user_master_id' => $usermaster->getUsermaster_id()));
            if ($user_wallet) {
                $wallet = array(
                    "virtual_money" => $user_wallet->getVirtual_money_balance(),
                    "wallet_points" => $user_wallet->getWallet_points(),
                    "cashback" => $user_wallet-BetCashback_balance()
                );
            }
            $data = array(
                "role_id" => $role_id,
                "firstname" => $usermaster->getUser_first_name(),
                "lastname" => $usermaster->getUser_last_name(),
                "email" => $usermaster->getUser_email_password(),
                "role_name" => $userrolename,
                "user_master_id" => $usermaster->getUsermaster_id(),
                "create_date" => $usermaster->getCreate_date(),
                "status" => $usermaster->getUser_status(),
                "wallet_details" => $wallet

            );
            # code...







            return array("usermaster" => $data, "role_master" => $role_master, "user_id" => $user_id);
        }
        // var_dump($data);exit;


        return array("role_master" => $role_master);
    }

    /**
     * @Route("/saveuser1")
     * @Template()
     */
    public function saveuse1rAction()
    {

        $user_id = "";
        if (!empty($_REQUEST['first_name'])) {
            $firstname = $_REQUEST["first_name"];
            $last_name = $_REQUEST["last_name"];
            $password = $_REQUEST["password"];
            $email = $_REQUEST["email"];
            $active = "Active";
            if (isset($_REQUEST["optradio"])) {
                $active = $_REQUEST["optradio"];
            }

            $user_type = $_REQUEST["user_type"];

            $em = $this->getDoctrine()->getManager();
            $session = new Session();
            $date_time = date("Y-m-d H:i:s");
            $usermaster_info_id = new usermaster();
            $usermaster_info_id->setUser_first_name($firstname);
            $usermaster_info_id->setUser_last_name($last_name);
            $usermaster_info_id->setUser_password(md5($password));
            $usermaster_info_id->setUser_email_address($email);
            $usermaster_info_id->setUser_status($active);
            //$usermaster_info_id->setUser_type('user');
            $usermaster_info_id->setCreate_date($date_time);
            $usermaster_info_id->setUser_role_id($user_type);
            $usermaster_info_id->setDomain_id($session->get('domain_id'));
            $usermaster_info_id->setLanguage_id(1);
            $usermaster_info_id->setIs_deleted(0);
            $em->persist($usermaster_info_id);

            $em->flush();
            $user_id = $usermaster_info_id->getId();
        }
        $this->get("session")->getFlashBag()->set("success_msg", "User Register successfully");
        return $this->redirect($this->generateUrl("admin_user_userinfo", array("domain" => $this->get('session')->get('domain'), "user_id" => $user_id)));
    }

    /**
     * @Route("/updateuser1/{user_id}",defaults={"user_id":""})
     * @Template()
     */
    public function updateuser1Action($user_id)
    {

        if (!empty($user_id)) {
            $session = new Session();
            $firstname = $_REQUEST["first_name"];
            $last_name = $_REQUEST["last_name"];
            $password = $_REQUEST["password"];

            $email = $_REQUEST["email"];
            $active = "Active";
            if (isset($_REQUEST["optradio"])) {
                $active = $_REQUEST["optradio"];
            }

            $user_type = $_REQUEST["user_type"];


            $em = $this->getDoctrine()->getManager();
            $usermaster_info_id = $em
                ->getRepository('AdminBundle:Usermaster')
                ->findOneBy(array('is_deleted' => "0", 'id' => $user_id));
            if (!empty($usermaster_info_id)) {
                $usermaster_info_id->setUser_first_name($firstname);
                $usermaster_info_id->setUser_last_name($last_name);
                if ($password != "") {
                    $usermaster_info_id->setUser_password(md5($password));
                }
                $usermaster_info_id->setUser_email_password($email);
                $usermaster_info_id->setUser_role_id($user_type);
                if ($user_id == $this->get('session')->get('user_id')) {
                    $this->get('session')->set('role_id', $user_type);
                    $role = $em
                        ->getRepository('AdminBundle:Rolemaster')
                        ->find(
                            $user_type
                        );
                    $this->get('session')->set('role', $role->getRolename());
                }

                $em->persist($usermaster_info_id);
                $em->flush();
                $user_wallet = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Walletmaster')->findOneBy(array('is_deleted' => "0", 'user_master_id' => $user_id));
                if (empty($user_wallet)) {
                    $wallet_code = $user_id . "_" . substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 5)), 0, 5);
                    $wallet_master = new Walletmaster();
                    $wallet_master->setWallet_code($wallet_code);
                    $wallet_master->setUser_master_id($user_id);
                    $wallet_master->setVirtual_money_balance(0);
                    $wallet_master->setActual_money_balance(0);
                    $wallet_master->setCashback_balance(0);
                    $wallet_master->setWallet_points(0);
                    $wallet_master->setLast_updated_wallet_datetime(date("Y-m-d H:i:s"));
                    $wallet_master->setWallet_member_tag_id(1);
                    $wallet_master->setIs_deleted(0);
                    $em->persist($wallet_master);
                    $em->flush();
                }
            }
        }
        $this->get("session")->getFlashBag()->set("success_msg", "User Update successfully");
        return $this->redirect($this->generateUrl("admin_user_userinfo", array("user_id" => $user_id)));
    }

    /**
     * @Route("/approve")
     */
    public function approveAction()
    {
        $user_id = $_REQUEST["course_id"];
        $flag = $_REQUEST["flag"];
        $dm = $this->getDoctrine()->getManager();
        $usertrainerrelation = $dm
            ->getRepository('AdminBundle:Usertrainerrelation')
            ->findOneBy(array('is_deleted' => "0", 'user_id' => $user_id));
        if (isset($usertrainerrelation)) {
            $status = 1;
            $usertrainerrelation->setIs_approved($flag);
            if ($flag == '-1') {
                $usertrainerrelation->setIs_approved($flag);
                $usertrainerrelation->setIs_deleted("1");
                $status = 0;
            }
            $dm->flush();
        }
        return new Response($status);
    }
}
