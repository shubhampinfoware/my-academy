<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\PropertyAccess\PropertyAccess;
use AdminBundle\Document\Coursetrainermaster;
use AdminBundle\Document\usermaster;
use AdminBundle\Document\usertrainerrelation;
use AdminBundle\Document\Userzoomrelation;
use AdminBundle\Entity\Userenrollments;
use AdminBundle\Document\fideuser;
use AdminBundle\Document\wallet_master;
use AdminBundle\Document\wallet_transaction;
use AdminBundle\Document\wallet_transaction_history;

/**
 * @Route("/")
 */
class WalletController extends BaseController {

    public function __construct() {
        
    }

    /**
     * @Route("/addwallet")
     * @Template()
     */
    public function addwalletAction() {
        $user_list = null;
        $user = $this->getDoctrine()->getManager()->getRepository('AdminBundle:usermaster')->findBy(array("is_deleted" => "0"));
        if ($user) {
            foreach ($user as $value) {
                $repository = $this->getDoctrine()->getManager()->getRepository('AdminBundle:usermaster');
                $usermaster = $repository->find($value->getId());
                $firstname = $value->getUser_first_name();
                $lastname = '';
                $email = '';
                $roleid = '';
                $birthdate = "12/07/2018";
                $rolename = 'Guest';

                $guest = '$this->GUEST_ROLE_ID';
                $organization = '5c261503d618fc0d0c174c1f';
                if (isset($usermaster)) {
                    if ($usermaster->getUser_role_id() != $guest and $usermaster->getUser_role_id() != $organization) {
                        $firstname = $usermaster->getUser_first_name();
                        $lastname = $usermaster->getUser_last_name();
                        $email = $usermaster->getUser_email_password();
                        $roleid = $usermaster->getUser_role_id();
                        $birthdate = $usermaster->getBirthdate();
                        $mobile_no = $usermaster->getUser_mobile();
                        $em = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Role_master');
                        $userrole = $em->find($roleid);
                        if ($userrole) {
                            $rolename = $userrole->getRolename();
                        }

                        $full_name = $firstname . " " . $lastname;
                        if (empty($full_name) or $full_name == " ") {
                            $full_name = $mobile_no;
                        }
                        $user_list[] = array(
                            "firstname" => $firstname,
                            "lastname" => $lastname,
                            "full_name" => $full_name,
                            "mobile_no" => $mobile_no,
                            "email" => $email,
                            "roleid" => $roleid,
                            "role_name" => $rolename,
                            "user_id" => $value->getId(),
                            "create_date" => $birthdate
                        );
                    }
                }
            }
        }
        return array("user_list" => $user_list);
    }

    /**
     * @Route("/savewallet")
     * @Template()
     */
    public function savewalletAction() {
        $em = $this->getDoctrine()->getManager();
        $user_id = $this->get('session')->get('user_id');
        $virtual_money = $_REQUEST["virtual_money"];
        $expiry_date = $_REQUEST["expiry_date"];
        $expiry_date = date("Y-m-d", strtotime($expiry_date));
        $user = $_REQUEST['user'];
        $number = sizeof($user);
        foreach ($user as $value) {
            $wallet_code = $value . "_" . substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 5)), 0, 5);
            $repository = $this->getDoctrine()->getManager()->getRepository('AdminBundle:wallet_master');
            $wallet_master = $repository->findOneBy(array("user_master_id" => $value));
            if (!empty($wallet_master)) {
                $currentvirtual = $wallet_master->getVirtual_money_balance();
                $totalvirtual = $virtual_money + $currentvirtual;
                $wallet_master->setVirtual_money_balance($totalvirtual);
                $em->flush();

                $wallet_tarns = new wallet_transaction();
                $wallet_tarns->setUser_id($value);
                $wallet_tarns->setWallet_id($wallet_master->getId());
                $wallet_tarns->setWallet_transaction("credit");
                $wallet_tarns->setWallet_transaction_amount($virtual_money);
                $wallet_tarns->setUpdated_wallet_balance($totalvirtual);
                $wallet_tarns->setTransaction_for_table("wallet_master");
                $wallet_tarns->setTransaction_for_table_id($wallet_master->getId());
                $wallet_tarns->setWallet_transaction_datetime(date("Y-m-d H:i:s"));
                $wallet_tarns->setWallet_transaction_by($user_id);
                $wallet_tarns->setIs_deleted(0);
                $wallet_tarns->setActual_payment_transaction_id(0);
                $wallet_tarns->setBalance_type("virtual");
                $wallet_tarns->setExpiry_date_of_balance($expiry_date);
                $wallet_tarns->setIs_archive(0);
                $wallet_tarns->setTransaction_note("");
                $wallet_tarns->setWallet_transaction_field_1("");
                $wallet_tarns->setWallet_transaction_field_2("");
                $wallet_tarns->setWallet_transaction_field_3("");
                $em->persist($wallet_tarns);
                $em->flush();

                $wallet_tarns_hist = new wallet_transaction_history();
                $wallet_tarns_hist->setUser_id($value);
                $wallet_tarns_hist->setWallet_id($wallet_master->getId());
                $wallet_tarns_hist->setWallet_trasaction_id($wallet_tarns->getId());
                $wallet_tarns_hist->setWallet_trasaction_amount($virtual_money);
                $wallet_tarns_hist->setTransacation_operation("credit");
                $wallet_tarns_hist->setUpdated_balance($totalvirtual);
                $wallet_tarns_hist->setBalance_type("virtual");
                $wallet_tarns_hist->setTransaction_created_by($user_id);
                $wallet_tarns_hist->setTransaction_created_datetime(date("Y-m-d H:i:s"));
                $wallet_tarns_hist->setIs_deleted(0);
                $wallet_tarns_hist->setWallet_transaction_history_field_1("");
                $wallet_tarns_hist->setWallet_transaction_history_field_2("");
                $wallet_tarns_hist->setWallet_transaction_history_field_3("");
                $em->persist($wallet_tarns_hist);
                $em->flush();
            } else {
                $wallet_master = new wallet_master();
                $wallet_master->setWallet_code($wallet_code);
                $wallet_master->setUser_master_id($value);
                $wallet_master->setVirtual_money_balance($virtual_money);
                $wallet_master->setActual_money_balance(0);
                $wallet_master->setCashback_balance(0);
                $wallet_master->setWallet_points(0);
                $wallet_master->setLast_updated_wallet_datetime(date("Y-m-d H:i:s"));
                $wallet_master->setWallet_member_tag_id(1);
                $wallet_master->setIs_deleted(0);
                $em->persist($wallet_master);
                $em->flush();

                $wallet_tarns = new wallet_transaction();
                $wallet_tarns->setUser_id($value);
                $wallet_tarns->setWallet_id($wallet_master->getId());
                $wallet_tarns->setWallet_transaction("credit");
                $wallet_tarns->setWallet_transaction_amount($virtual_money);
                $wallet_tarns->setUpdated_wallet_balance($virtual_money);
                $wallet_tarns->setTransaction_for_table("wallet_master");
                $wallet_tarns->setTransaction_for_table_id($wallet_master->getId());
                $wallet_tarns->setWallet_transaction_datetime(date("Y-m-d H:i:s"));
                $wallet_tarns->setWallet_transaction_by($user_id);
                $wallet_tarns->setIs_deleted(0);
                $wallet_tarns->setActual_payment_transaction_id(0);
                $wallet_tarns->setBalance_type("virtual");
                $wallet_tarns->setExpiry_date_of_balance($expiry_date);
                $wallet_tarns->setIs_archive(0);
                $wallet_tarns->setTransaction_note("");
                $wallet_tarns->setWallet_transaction_field_1("");
                $wallet_tarns->setWallet_transaction_field_2("");
                $wallet_tarns->setWallet_transaction_field_3("");
                $em->persist($wallet_tarns);
                $em->flush();

                $wallet_tarns_hist = new wallet_transaction_history();
                $wallet_tarns_hist->setUser_id($value);
                $wallet_tarns_hist->setWallet_id($wallet_master->getId());
                $wallet_tarns_hist->setWallet_trasaction_id($wallet_tarns->getId());
                $wallet_tarns_hist->setWallet_trasaction_amount($virtual_money);
                $wallet_tarns_hist->setTransacation_operation("credit");
                $wallet_tarns_hist->setUpdated_balance($virtual_money);
                $wallet_tarns_hist->setBalance_type("virtual");
                $wallet_tarns_hist->setTransaction_created_by($user_id);
                $wallet_tarns_hist->setTransaction_created_datetime(date("Y-m-d H:i:s"));
                $wallet_tarns_hist->setIs_deleted(0);
                $wallet_tarns_hist->setWallet_transaction_history_field_1("");
                $wallet_tarns_hist->setWallet_transaction_history_field_2("");
                $wallet_tarns_hist->setWallet_transaction_history_field_3("");
                $em->persist($wallet_tarns_hist);
                $em->flush();
            }
        }
        $this->get("session")->getFlashBag()->set("success_msg", $virtual_money . " Money Assigned to " . $number . " users");
        return $this->redirect($this->generateUrl("admin_wallet_addwallet"));
    }

}
