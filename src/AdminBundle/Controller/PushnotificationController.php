<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\PropertyAccess\PropertyAccess;

use AdminBundle\Entity\Generalnotification;

/**
 * @Route("/")
 */
class PushnotificationController extends BaseController
{

    public function __construct()
    {
        parent::__construct();
        $obj = new BaseController();
        $obj->checkSessionAction();
    }

    /**
     * @Route("/pushnotification")
     * @Template()
     */
    public function indexAction()
    {
        $domain_id = 1;

        $note_list = $this->getDoctrine()->getManager()
            ->getRepository('AdminBundle:Generalnotification')
            ->findBy(array('is_deleted' => '0', 'notification_type' => 'general'), array('create_date' => 'desc'));

        $health_list = $this->getDoctrine()->getManager()
            ->getRepository('AdminBundle:Generalnotification')
            ->findBy(array('is_deleted' => '0', 'notification_type' => 'healthtip'), array('create_date' => 'desc'));

        return array("note_list" => $note_list, "health_list" => $health_list);
    }

    /**
     * @Route("/notification/delete/{id}",defaults={"id"=""})
     * @Template()
     */
    public function deleteNotificationAction($id, Request $request)
    {
        if ($id != '') {
            $entity = $this->getDoctrine()->getManager();
            $note_list = $entity->getRepository('AdminBundle:Generalnotification')->findOneBy([
                'generalnotification_id' => $id,
                'is_deleted' => "0"
            ]);

            if (!empty($note_list)) {
                $note_list->setIs_deleted("1");
                $entity->flush();
                $this->get('session')->getFlashBag()->set('success_msg', 'Notification Removed Successfully');
            } else {
                $this->get('session')->getFlashBag()->set('error_msg', 'Notification not found');
            }
        } else {
            $this->get('session')->getFlashBag()->set('error_msg', 'Notification not found');
        }

        $referer = $request->headers->get('referer');
        return $this->redirect($referer);
    }

    /**
     * @Route("/addpushnotification")
     * @Template()
     */
    public function addpushnotificationAction()
    {

        $user_list = null;
        $user = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Usermaster')->findBy(array("is_deleted" => "0"));

        $em = $this->getDoctrine()->getManager();
        
        $role = $em->createQueryBuilder()->select('p')
            ->from('AdminBundle:Rolemaster', 'p')
            ->getQuery()
            ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);

        $subscription = $em->createQueryBuilder()->select('p')
            ->from('AdminBundle:Subscriptionmaster', 'p')
            ->where('p.is_deleted = :is_deleted')
            ->setParameter('is_deleted', 0)
            ->getQuery()
            ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        
        $coursemaster = $em->createQueryBuilder()->select('p')
            ->from('AdminBundle:Coursemaster', 'p')
            ->where('p.is_deleted = :is_deleted')
            ->setParameter('is_deleted', 0)
            ->getQuery()
            ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);

        if ($user) {

            foreach ($user as  $value) {

                if($value->getUser_role_id() == 1){
                    continue;
                }

                $repository = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Usermaster');
                $usermaster = $repository->find($value->getUsermaster_id());
                $firstname = $value->getUser_first_name();
                $lastname = '';
                $email = '';
                $roleid = '';
                $birthdate = "12/07/2018";
                $rolename = 'Guest';

                $guest = '$this->GUEST_ROLE_ID';

                if (isset($usermaster)) {
                    if ($usermaster->getUser_role_id() != $guest) {
                        $firstname = $usermaster->getUser_first_name();
                        $lastname = $usermaster->getUser_last_name();
                        $email = $usermaster->getUser_email_password();
                        $roleid = $usermaster->getUser_role_id();
                        $birthdate = $usermaster->getBirthdate();

                        $em = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Rolemaster');
                        $userrole =  $em->find($roleid);
                        if ($userrole) {
                            $rolename = $userrole->getRolename();
                        }

                        $full_name = $firstname . " " . $lastname;
                        $user_list[] = array(
                            "firstname" => $firstname,
                            "lastname" => $lastname,
                            "full_name" => $full_name,
                            "email" => $email,
                            "roleid" => $roleid,
                            "role_name" =>  $rolename,
                            "user_id" => $value->getUsermaster_id(),
                            "create_date" => $birthdate

                        );
                    }
                }
            }
        }

        return array("user_list" => $user_list, "role" => $role, "subscription" => $subscription, "coursemaster" => $coursemaster);
    }

    /**
     * @Route("/sendnotification")
     */
    public function sendnotificationAction()
    {
        if (isset($_POST['send_notification']) && $_POST['send_notification'] == "send_notification") {
            if ($_POST['note_title'] != "" && $_POST['note_message'] != "") {
                if (!empty($_POST['notification_type']) && $_POST['notification_type'] == 'healthtip') {
                    $code = '7';
                } elseif (!empty($_POST['notification_type']) && $_POST['notification_type'] == 'app_alert') {
                    $code = '11';
                } else {
                    $code = '10';
                }
                $send_to = 'customer'; //$_POST['send_to'];
                $media_id = 'FALSE';
                if (!empty($_FILES['image'])) {
                    $Config_live_site = $this->container->getParameter('live_path');
                    $file_path = $this->container->getParameter('file_path');
                    $file = $_FILES['image']['name'];     // only profile image is allowed
                    $extension = strtolower(pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION));
                    if ($extension == 'jpg' || $extension == 'png' || $extension == 'jpeg') {
                        $tmpname = $_FILES['image']['tmp_name'];
                        $path = $file_path . '/uploads/notification';
                        $upload_dir = $this->container->getParameter('upload_dir') . '/uploads/notification/';
                        $media_id = $this->mediauploadAction($_FILES['image']['name'], $tmpname, $path, $upload_dir, 1);
                        $Config_live_site = $this->container->getParameter('live_path');
                        //media upload check

                        if ($media_id != FALSE) {

                            $media_library = $this->getDoctrine()->getManager()
                                ->getRepository('AdminBundle:Mediamaster')
                                ->findOneBy(array('mediamaster_id' => $media_id));

                            $response = array(
                                "notification_title" => $_POST['note_title'],
                                "notification_image" => $Config_live_site . $media_library->getMedia_location() . "/" . $media_library->getMedia_name()
                            );



                            $message = json_encode(array("detail" => $_POST['note_message'], "code" => $code, "response" => $response));
                        } else {

                            $message = json_encode(array("detail" => $_POST['note_message'], "code" => $code, "response" => $_POST['note_title']));
                        }
                    } else {

                        $message = json_encode(array("detail" => $_POST['note_message'], "code" => $code, "response" => $_POST['note_title']));
                    }
                } else {
                    $message = json_encode(array("detail" => $_POST['note_message'], "code" => $code, "response" => $_POST['note_title']));
                }
                $general_notification = new Generalnotification();
                $general_notification->setNotificationType($_POST['notification_type']);
                $general_notification->setTitle($_POST['note_title']);
                $general_notification->setMessage($_POST['note_message']);
                if (!empty($_FILES['image'])) {
                    if ($media_id != "FALSE") {
                        $general_notification->setImageId($media_id);
                    }
                }
                if (isset($_POST['user']) && !empty($_POST['user'])) {
                    $user_id = $_POST['user'];
                    $user_id =  implode(',', $user_id);
                } else {
                    $user_id = 0;
                }

                $general_notification->setUser_master_id($user_id);
                $general_notification->setSendTo($send_to);

                $general_notification->setCreate_date(date("Y-m-d H:i:s"));
                $general_notification->setIs_deleted(0);
                $em = $this->getDoctrine()->getManager();

                $em->persist($general_notification);
                $em->flush();

                $notification_id_send = $general_notification->getGeneralnotificationId();

                $domain_id = 1;
                $app_id = 'CUST';
                $user_array = array();
                $user_apns_id = array();

                if (isset($_POST['user']) && !empty($_POST['user'])) {
                    foreach ($_POST['user'] as $check) {
                        $user_array[] = $check;
                    }
                }
                $user_gcm_id = array();

                $gcm_regids = $this->find_gcm_regid($user_array);


                if (!empty($gcm_regids)) {
                    if (count($gcm_regids) > 0) {
                        $this->send_notification($gcm_regids, $_POST['note_title'], $message, 2, $app_id, $domain_id, "general_notification", $notification_id_send);
                    }
                }

                $this->get('session')->getFlashBag()->set('success_msg', "Notification sent successfully");

                return $this->redirect($this->generateUrl('admin_pushnotification_index', array("domain" => $this->get('session')->get('domain'))));
            } else {

                $this->get('session')->getFlashBag()->set('error_msg', "Notification title and message is required");
            }
        } else {

            $this->get('session')->getFlashBag()->set('error_msg', 'Oops! Something goes wrong! Try again later');
        }

        return $this->redirect($this->generateUrl('admin_pushnotification_addpushnotification', array("domain" => $this->get('session')->get('domain'))));
    }

    /**

     * @Route("/getuserlist")

     */
    public function getuserlistAction()
    {
        $html = "";
        if (isset($_POST['flag']) && $_POST['flag'] == 'getuser' && $_POST['user_type'] != "") {

            $user_type = $_POST['user_type'];
            $city_id = !empty($_POST['city_id']) ? $_POST['city_id'] : 0;
            $user_list = array();
            if ($user_type == 'CUST') {

                $user_list = $this->getDoctrine()
                    ->getManager()
                    ->getRepository('AdminBundle:Usermaster')
                    ->findBy(array("user_role_id" => 7, "user_status" => "active", "is_deleted" => 0, "domain_id" => $this->get('session')->get('domain_id'), "user_type" => "user"));
            }

            if ($user_type == 'DEL') {

                $user_list = $this->getDoctrine()
                    ->getManager()
                    ->getRepository('AdminBundle:Usermaster')
                    ->findBy(array("user_role_id" => 6, "user_status" => "active", "is_deleted" => 0, "domain_id" => $this->get('session')->get('domain_id'), "user_type" => "user"));
            }



            if (!empty($user_list)) {



                $html .= '<label class="col-sm-2 control-label">&nbsp;</label>

								<div class="col-md-10">

									<div class="box box-success box-solid">

										<div class="box-header with-border">

											<h3 class="box-title">Users list</h3>

											<input type="checkbox" id="checkAll" class="checkbox pull-right"/>

										</div>

										<div class="box-body" id="userlistbox">';

                foreach ($user_list as $key => $val) {

                    if (isset($city_id) && !empty($city_id)) {

                        $address_id = 0;

                        $order_master_id = "";

                        $em = $this->getDoctrine()->getManager();

                        $connection = $em->getConnection();

                        $statement = $connection->prepare("select delivery_address_id from order_master where order_createdby='" . $val->getUser_master_id() . "' order by `order_master_id` desc LIMIT 0,1");

                        $statement->execute();

                        $order_master_id = $statement->fetchAll();

                        if (isset($order_master_id) && !empty($order_master_id)) {

                            $address_id = $order_master_id[0]['delivery_address_id'];
                        } else {

                            if ($val->getAddress_master_id() == "0") {

                                //echo "test";

                                continue;
                            } else {

                                $address_id = $val->getAddress_master_id();
                            }
                        }

                        $address_master_id = "";

                        $connection = $em->getConnection();

                        $statement = $connection->prepare("select `city_id` from address_master where `address_master_id`='" . $address_id . "'");

                        $statement->execute();

                        $address_master_id = $statement->fetchAll();

                        if (isset($address_master_id) && !empty($address_master_id)) {

                            $get_city_id = $address_master_id[0]['city_id'];

                            //if($address_master_id[0]['city_id'])
                        }

                        if ($get_city_id == $city_id) {

                            $mono = $this->keyDecryptionAction($val->getUser_mobile());
                            if ($mono != '' && $mono != 0 && $mono != NULL) {
                                $html .= '<div class="col-md-3"><input type="checkbox" name="user[]" class="checkBoxClass" id="mychk" value="' . $val->getUser_master_id() . '"> ' . $mono . '&emsp;</div>';
                            }
                        }
                    } else {

                        $mono = $this->keyDecryptionAction($val->getUser_mobile());

                        $html .= '<input type="checkbox" name="user[]" class="checkBoxClass" id="mychk" value="' . $val->getUser_master_id() . '"> ' . $mono . '&emsp;';
                    }
                }

                $html .= '</div>

									</div>

								</div>';

                $html .= "<script>$('#checkAll').change(function () {

							$('input:checkbox').prop('checked', $(this).prop('checked'));

						});</script>";
            }
        }

        echo $html;
        return new Response;
    }

    /**

     * @Route("/getuserlist1")

     */
    public function getuserlist1Action()
    {
        $html = "";
        $em = $this->getDoctrine()->getManager();

        $role = $_POST['role'];
        $subscription = $_POST['subscription'];
        $course = $_POST['course'];
        $user_list = array();
        if ($role == "0") {

            $user_list =  $em->getRepository('AdminBundle:usermaster')
                ->findBy(array("is_deleted" => "0"));
        }

        if ($role != "0") {
            $user_list =  $em->getRepository('AdminBundle:usermaster')
                ->findBy(array("user_role_id" => $role, "is_deleted" => "0"));
        }





        if (!empty($user_list)) {

            if ($subscription != "0") {


                $html .= '<label class="col-sm-2 control-label">&nbsp;</label>

                                <div class="col-md-10">

                                    <div class="box box-success box-solid">

                                        <div class="box-header with-border">

                                            <h3 class="box-title">Users list</h3>

                                            <input type="checkbox" id="checkAll" class="checkbox pull-right"/>

                                        </div>

                                        <div class="box-body" id="userlistbox">';
                $guest = "$this->GUEST_ROLE_ID";
                foreach ($user_list as $key => $val) {
                    $user_package_relation = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Userpackagerelation')->findBy(array(
                        "user_id" => $val->getId(),
                        "package_id" => $subscription,
                        "is_deleted" => "0"
                    ));
                    if (!empty($user_package_relation)) {
                        if ($val->getUser_role_id() != $guest) {

                            $firstname = $val->getUser_first_name();
                            $lastname = $val->getUser_last_name();
                            $full_name = $firstname . " " . $lastname;
                            $html .= '<div class="col-md-3"><input type="checkbox" name="user[]" class="checkBoxClass" id="mychk" value="' . $val->getId() . '"> ' . $full_name . '&emsp;</div>';
                        }
                    }
                }

                $html .= '</div>

                                    </div>

                                </div>';

                $html .= "<script>$('#checkAll').change(function () {

                            $('input:checkbox').prop('checked', $(this).prop('checked'));

                        });</script>";
            } else if ($course != "0") {

                $html .= '<label class="col-sm-2 control-label">&nbsp;</label>

                                <div class="col-md-10">

                                    <div class="box box-success box-solid">

                                        <div class="box-header with-border">

                                            <h3 class="box-title">Users list</h3>

                                            <input type="checkbox" id="checkAll" class="checkbox pull-right"/>

                                        </div>

                                        <div class="box-body" id="userlistbox">';
                $guest = "$this->GUEST_ROLE_ID";
                foreach ($user_list as $key => $val) {
                    $coursetrainerone = $em
                        ->getRepository('AdminBundle:Coursetrainermaster')
                        ->findOneBy(array('is_deleted' => "0", 'coursemaster_id' => $course, 'usermaster_id' => $val->getId()));
                    if (!empty($coursetrainerone)) {
                        if ($val->getUser_role_id() != $guest) {

                            $firstname = $val->getUser_first_name();
                            $lastname = $val->getUser_last_name();
                            $full_name = $firstname . " " . $lastname;
                            $html .= '<div class="col-md-3"><input type="checkbox" name="user[]" class="checkBoxClass" id="mychk" value="' . $val->getId() . '"> ' . $full_name . '&emsp;</div>';
                        }
                    }
                }

                $html .= '</div>

                                    </div>

                                </div>';

                $html .= "<script>$('#checkAll').change(function () {

                            $('input:checkbox').prop('checked', $(this).prop('checked'));

                        });</script>";
            } else {

                $html .= '<label class="col-sm-2 control-label">&nbsp;</label>

                                <div class="col-md-10">

                                    <div class="box box-success box-solid">

                                        <div class="box-header with-border">

                                            <h3 class="box-title">Users list</h3>

                                            <input type="checkbox" id="checkAll" class="checkbox pull-right"/>

                                        </div>

                                        <div class="box-body" id="userlistbox">';
                $guest = "$this->GUEST_ROLE_ID";
                foreach ($user_list as $key => $val) {
                    if ($val->getUser_role_id() != $guest) {

                        $firstname = $val->getUser_first_name();
                        $lastname = $val->getUser_last_name();
                        $full_name = $firstname . " " . $lastname;
                        $html .= '<div class="col-md-3"><input type="checkbox" name="user[]" class="checkBoxClass" id="mychk" value="' . $val->getId() . '"> ' . $full_name . '&emsp;</div>';
                    }
                }

                $html .= '</div>

                                    </div>

                                </div>';

                $html .= "<script>$('#checkAll').change(function () {

                            $('input:checkbox').prop('checked', $(this).prop('checked'));

                        });</script>";
            }
        }


        echo $html;
        return new Response;
    }
}
