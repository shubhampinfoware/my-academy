<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\PropertyAccess\PropertyAccess;
use AdminBundle\Document\Coursetrainermaster;
use AdminBundle\Document\usermaster;
use AdminBundle\Document\user_register_enroll;
use AdminBundle\Document\fideuser;
use AdminBundle\Document\usertrainerrelation;
use AdminBundle\Document\user_package_relation;
use AdminBundle\Document\coupon_assigned_to_user;
use AdminBundle\Document\wallet_master;
use AdminBundle\Document\Categorymaster;

/**
 * @Route("/")
 */
class VideoviewController extends BaseController {

    public function __construct() {
        
    }

    /**
     * @Route("/videoview")
     * @Template()
     */
    public function videoviewAction() {
        $language = null;
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('AdminBundle:language_master');
        $language_list = $repository->findAll();
        $user_id = 21 ;
        $course_id = 45 ;
        $class_id = 56 ;
        
        $Enc_user_id = $this->keyEncryptionAction($user_id) ;
       // var_dump($Enc_user_id);exit;
        $Enc_course_id =  $this->keyEncryptionAction($course_id);
        $Enc_class_id =  $this->keyEncryptionAction($class_id) ;
        
        //var_dump( $language_list );exit;
       //$video_url = "small.mp4";
       $video_url = "chesscoaching/bundles/design/course/small.mp4";
       $Enc_video_url = $video_url;// $this->keyEncryptionAction( $video_url);
//       $url =  ;
       
       if(isset($_POST['save'])){
           
           return $this->redirect($this->generateUrl("admin_videoview_videoplay", array("user_id" =>$Enc_user_id ,"class_id"=>$Enc_class_id,"course_id"=>$course_id,"video_url"=>$Enc_video_url )));
       }
       else{
           return array("language" => $language,"course_id"=>$course_id,"class_id"=>$class_id,"user_id"=>$user_id,"video_url"=>$video_url);
       }
       ////return array("language" => $language);
        //var_dump($all_category_details);exit;
    }

     /**
     * @Route("/videoplay/{user_id}/{class_id}/{course_id}/{video_url}",defaults={"user_id":"","class_id":"","course_id":"","video_url":""},requirements={"video_url"=".+"})
     * @Template()
     */
    public function videoplayAction($user_id,$class_id,$course_id,$video_url) {
//         $user_id = 21 ;
//        $course_id = 45 ;
//        $class_id = 56 ;
     //   $video_url = "small.mp4";
           
         $Dec_user_id = $this->keyDecryptionAction($user_id) ;
        $Dec_course_id =  $this->keyDecryptionAction($course_id);
        $Dec_class_id =  $this->keyDecryptionAction($class_id);
        $Dec_video =  $video_url ; //$this->keyDecryptionAction($video_url);
        //var_dump($Dec_video);exit;
        return array("dec_video"=>$Dec_video);
    }

}
