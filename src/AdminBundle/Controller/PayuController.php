<?php

namespace AdminBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

use AdminBundle\Document\Userinfo;
use AdminBundle\Document\Rolemaster;
use AdminBundle\Document\Gcmuser;
use AdminBundle\Document\Apnsuser;
use AdminBundle\Document\users;
use AdminBundle\Document\coursemaster;
use AdminBundle\Document\Tournament;
use AdminBundle\Document\transaction_master;
use AdminBundle\Document\user_package_relation;
use AdminBundle\Document\user_meeting_url;


/**
* @Route("/")
*/
class PayuController extends BaseController
{

    /**
     * @Route("/payment-successful")
     * @Template()
     */
    public function paymentSuccessfulAction(Request $request)
    {
        $subject_id = 0;
        try{
            $entity = $em  = $this->getDoctrine()->getManager();
            $postData = $_POST;

            // status, email, mihpayid

            if(trim(strtolower($postData['status'])) == 'success'){
                // subscribe user package
                
                $user_id = $postData['udf1'];
                $coupon_id = $postData['udf2'];
                $user = $this->getDoctrine()->getManager()->getRepository('AdminBundle:usermaster')->find($user_id);
                $subscription_amt = $subscription_id = 0 ;
                $user_package_relation_id = $postData['productinfo'];
                $course_ids = NULL ;
                $user_package_relation_info = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Userpackagerelation')->find($user_package_relation_id);
                if($user_package_relation_info){
                    $user_package_relation_info->setIsArchieved("0");
                    $user_package_relation_info->setIs_deleted("0");
                    $em->flush();
                    
                    $subscription_info = $this->getDoctrine()->getManager()->getRepository("AdminBundle:subscription_master")->find($user_package_relation_info->getPackageId());
                    $subscription_amt = $subscription_info->getPrice();
                    $subscription_id = $subscription_info->getId();
                    $course_ids = $subscription_info->getCourseId();
                }
                
                $user_info = $this->getDoctrine()->getManager()->getRepository('AdminBundle:usermaster')->find($user_id);
                
                // store transaction
                $transaction = new transaction_master();
                $transaction->setRef_number($_POST['bank_ref_num']);
                $transaction->setPayment_id($_POST['mihpayid']);
                $transaction->setUser_id($user_id);
                $transaction->setEmail($user_info->getUser_email_password());
                $transaction->setStatus($_POST['status']);
                $transaction->setUnmappedstatus($_POST['unmappedstatus']);
                $transaction->setTxnid($_POST['txnid']);
                $transaction->setAmount($_POST['amount']);
                $transaction->setNameoncard($_POST['name_on_card']);
                $transaction->setTxn_field_1('');
                $transaction->setTxn_field_2('');
                $transaction->setTxn_field_3('');
                $transaction->setCreated_datetime(date("Y-m-d H:i:s"));
                $transaction->setIs_deleted('0');
                $transaction->setProduct_id($subscription_info->getId());

                $entity->persist($transaction);
                $entity->flush();
                $ProjectTransactionID = $transaction->getId();
                $cashback_amt = 0 ; 
                $final_payment_amt = $postData['amount'];
                
               
                if($coupon_id != NULL && $coupon_id != ""){
                   
                    $coupon_info = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Couponmaster")->findOneBy(array("is_deleted"=>0,"_id"=>$coupon_id));
                    if($coupon_info){
                        $discount_type = $coupon_info->getDiscount_type();
                        $return_type = $coupon_info->getReturn_type();
                        $disc_cal_number = $coupon_info->getDiscount_value();

                        if($discount_type == 'percentage'){
                            $discount_value = ($subscription_amt * $disc_cal_number ) / 100 ;
                        }
                        else{
                            $discount_value = $disc_cal_number ; 
                        }
                        if($return_type = 'less_amount'){
                            $final_payment_amt = $final_payment_amt - $discount_value ; 
                        }
                        elseif($return_type == 'cashback'){
                            $cashback_amt = $discount_value ;
                        }
                        
                        //-----------------------
                         if($cashback_amt > 0 ){
                            $wallet_info = $this->getDoctrine()->getManager()->getRepository('AdminBundle:wallet_master')->findOneBy(array("user_id"=>$user_id));
                            if($wallet_info){
                                $wallet_id = $wallet_info->get_id();
                            }
            
                            $wallet_transaction = new wallet_transaction();
                            $wallet_transaction->setUser_id($user_id);
                            $wallet_transaction->setWallet_id($wallet_id);
                            $wallet_transaction->setWallet_transaction('credit');
                            $wallet_transaction->setWallet_transaction_amount($cashback_amt);
                            $wallet_transaction->setUpdated_wallet_balance($cashback_amt);
                            $wallet_transaction->setTransaction_for_table('user_package_relation');
                            $wallet_transaction->setTransaction_for_table_id($user_package_relation_id);
                            $wallet_transaction->setWallet_transaction_datetime(date("Y-m-d H:i:s"));
                            $wallet_transaction->setWallet_transaction_by($user_id);
                            $wallet_transaction->setIs_deleted(0);
                            $wallet_transaction->setActual_payment_transaction_id($ProjectTransactionID); // PAY u money transaction
                            $wallet_transaction->setBalance_type('cashback'); // PAY u monet transaction
                            $wallet_transaction->setExpiry_date_of_balance(); // PAY u monet transaction
                            $wallet_transaction->setIs_archive(0); // PAY u monet transaction
                            $wallet_transaction->setTransaction_note('Cashback from coupon : '.$coupon_info->getCoupon_code() . " @ buying Sunscription : " . $subscription_info->getExtra()); 
                            $wallet_transaction->setwallet_transaction_field_1(''); 
                            $wallet_transaction->setwallet_transaction_field_2(''); 
                            $wallet_transaction->setwallet_transaction_field_3(''); 
                            $em->persist($wallet_transaction);
                            $em->flush();
                            $wallet_transaction_id = $wallet_transaction->get_id();

                            $wallet_transaction_history = new wallet_transaction_history();
                            $wallet_transaction_history->setUser_id($user_id);
                            $wallet_transaction_history->setWallet_id($wallet_id);
                            $wallet_transaction_history->setWallet_transaction_id($wallet_transaction_id);
                            $wallet_transaction_history->setWallet_transaction_amount($cashback_amt);
                            $wallet_transaction_history->setTransacation_operation('credit');
                            $wallet_transaction_history->setUpdated_balance($cashback_amt);
                            $wallet_transaction_history->setBalance_type('cashback');
                            $wallet_transaction_history->setTransaction_created_by($user_id);
                            $wallet_transaction_history->setTransaction_created_datetime(date("Y-m-d H:i:s"));
                            $wallet_transaction_history->setIs_deleted(0);
                            $wallet_transaction_history->setWallet_transaction_history_field_1("");
                            $wallet_transaction_history->setWallet_transaction_history_field_2("");
                            $wallet_transaction_history->setWallet_transaction_history_field_3("");
                            $em->persist($wallet_transaction_history);
                            $em->flush();


                        }

                    }
                }
                // Subscribe into Zoom meetings
                $course_unit_arr = [] ; 
                if($course_ids != NULL && $course_ids != ""){
                    foreach($course_ids as $ckey=>$cval){
                        $course_unit_relation_list = $em
                                ->getRepository('AdminBundle:course_unit_relation')
                                ->findBy(array('is_deleted' => "0", 'relation_object_id' => $cval,'relation_with'=>'course'));
                       
                        if($course_unit_relation_list){
                            foreach($course_unit_relation_list as $delkey=>$delval){
                                $course_unit_arr[] = $delval->getCourse_unit_id();
                            }
                        }
                    }
                }
                
                if($course_unit_arr != NULL && !empty($course_unit_arr)){
                    foreach($course_unit_arr as $cukey=>$cuval){
                        $course_unit_info = $this->getDoctrine()->getManager()->getRepository('AdminBundle:course_unit')->find(array("id"=>$cuval,"is_deleted"=>"0","course_type"=>"1"));
                        if($course_unit_info != NULL){
                            $regIDD = '';
                            $need_approveUsers = [];
                            $zoom_meeting_id = $course_unit_info->getUnitCode();
                           // echo "<br>Meeting is : " . $zoom_meeting_id ;
                            $Meeting_reg_Function = 'meetings/' . $zoom_meeting_id . '/registrants';
                            $meeting_reg_request_url = $this->api_url . $Meeting_reg_Function;
                            $curl = curl_init();
                            $headers = array();
                            $headers[] = 'content-type: application/json';
                            $headers[] = 'authorization: Bearer ' . $this->access_token;
                            $meeting_reg_data = array(
                                "email" => $user_info->getUser_email_password(),
                                "first_name"=> $user_info->getUser_first_name(),
                                "last_name"=> $user_info->getUser_last_name(),
                                "phone"=>$user_info->getUser_mobile()                              

                            );
                            curl_setopt_array($curl, array(
                                CURLOPT_URL => $meeting_reg_request_url,
                                CURLOPT_RETURNTRANSFER => true,
                                CURLOPT_ENCODING => "",
                                CURLOPT_MAXREDIRS => 10,
                                CURLOPT_TIMEOUT => 30,
                                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                CURLOPT_CUSTOMREQUEST => "POST",
                                CURLOPT_POSTFIELDS =>json_encode($meeting_reg_data),
                                CURLOPT_HTTPHEADER => $headers
                            ));

                            $Meeting_registrant_response = curl_exec($curl);
                            $Meeting_registrant_err = curl_error($curl);

                            curl_close($curl);

                            if ($Meeting_registrant_err) {
                               // echo "cURL Error #:" . $Meeting_registrant_err; 
                            } else {   
                                
                                 $Meeting_registrant_response = json_decode($Meeting_registrant_response);
                                
                                 if(isset($Meeting_registrant_response->registrant_id)){
                                     
                                     $need_approveUsers[] = array(                                              
                                                "id"=> $Meeting_registrant_response->registrant_id,
                                                "email"=> $user_info->getUser_email_password()
                                               );
                                     
                                 }
                            }
                            //var_dump($need_approveUsers);
                            if($need_approveUsers != NULL){
                                $curl = curl_init();
                                $Meeting_reg_StatusFunction = 'meetings/' . $zoom_meeting_id . '/registrants/status';
                                $meeting_reg_Statusrequest_url = $this->api_url . $Meeting_reg_StatusFunction;
                                $meeting_registrant_updateData = array(                                        
                                        "action"=> "approve",
                                        "registrants"=>$need_approveUsers
                                );

                                curl_setopt_array($curl, array(
                                  CURLOPT_URL => $meeting_reg_Statusrequest_url,
                                  CURLOPT_RETURNTRANSFER => true,
                                  CURLOPT_ENCODING => "",
                                  CURLOPT_MAXREDIRS => 10,
                                  CURLOPT_TIMEOUT => 30,
                                  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                  CURLOPT_CUSTOMREQUEST => "PUT",
                                  CURLOPT_POSTFIELDS =>json_encode($meeting_registrant_updateData),
                                  CURLOPT_HTTPHEADER => $headers
                                ));

                                $Meeting_registrant_status_response = curl_exec($curl);
                                $Meeting_registrant_status_err = curl_error($curl);

                                curl_close($curl);

                                if ($Meeting_registrant_status_err) {
                                  //echo " --> cURL Error #:" . $Meeting_registrant_status_err;exit;
                                } else {
                                  echo $Meeting_registrant_status_response;
                                  //var_dump($Meeting_registrant_status_response);exit;
                                  // add in meeting user table
                                  $joinURL = '';
                                     $regIDD = $Meeting_registrant_response->registrant_id ;
                                    if(isset($Meeting_registrant_response->join_url)){
                                        $joinURL = $Meeting_registrant_response->join_url;
                                    }
                                    //----------------------------------
                                    $curl = curl_init();
                                    $Meeting_getRegistrant_Function = 'meetings/' . $zoom_meeting_id . '/registrants?status=approved';
                                    $meeting_getRegistrant_url = $this->api_url . $Meeting_getRegistrant_Function;
                                    curl_setopt_array($curl, array(
                                      CURLOPT_URL => $meeting_getRegistrant_url,
                                      CURLOPT_RETURNTRANSFER => true,
                                      CURLOPT_ENCODING => "",
                                      CURLOPT_MAXREDIRS => 10,
                                      CURLOPT_TIMEOUT => 30,
                                      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                      CURLOPT_CUSTOMREQUEST => "GET",
                                      CURLOPT_HTTPHEADER => $headers
                                    ));

                                    $Meeting_getRegistrant_response = curl_exec($curl);
                                    $err = curl_error($curl);

                                    curl_close($curl);

                                    if ($err) {
                                      echo "cURL Error #:" . $err;
                                    } else {
                                     // echo $Meeting_getRegistrant_response;
                                    }
                                    $Meeting_getRegistrant_response = (json_decode($Meeting_getRegistrant_response));
                                    $Meeting_getRegistrant_response = $Meeting_getRegistrant_response->registrants;
                                    $need_join_url = ''; 
                                    foreach($Meeting_getRegistrant_response as $nval){

                                        $need_reg_id = $nval->id;
                                       
                                        if($regIDD == $need_reg_id){
                                             $need_join_url = $nval->join_url;
                                            //-------------------------------------------------------------------------------------
//                                            $user_meeting_url = $this->getDoctrine()->getManager()->getRepository('AdminBundle:user_meeting_url')->findOneBy(array("registrant_id" => $need_reg_id));
//                                            if($user_meeting_url){
//                                                $user_meeting_url->setMeeting_url($need_join_url);
//                                                $em->flush();
//                                            }
                                        }
                                    }
                                    $user_meeting_url = new user_meeting_url();
                                    $user_meeting_url->setUser_id($user_id);
                                    $user_meeting_url->setCourse_unit_id($course_unit_info->getId());
                                    $user_meeting_url->setMeeting_id($zoom_meeting_id);
                                    $user_meeting_url->setMeeting_url($need_join_url);
                                    $user_meeting_url->setRegistrant_id($Meeting_registrant_response->registrant_id);
                                    $user_meeting_url->setIs_deleted("0");
                                    $em->persist($user_meeting_url);
                                    $em->flush();
                                    
                                    

                                }
                           }
                        }
                    }
                }
                
            }
        } catch(\Exception $e) {
            echo $e->getMessage();exit;
        }

        $site_url = $this->site_base_url.$subscription_id;
        /* if($subject_id != 0){
            $site_url .= "/package/{$subject_id}";
        } */

        header("location:{$site_url}");
        exit;
    }

    /**
     * @Route("/payment-failure")
     * @Template()
     */
    public function paymentFailureAction(Request $request)
    {
        $entity = $em  = $this->getDoctrine()->getManager();
       $subscription_amt = $subscription_id = 0 ;
       $postData = $_POST;
        try{
            
            if($_POST['status'] == 'failure'){
                $user_id = $postData['udf1'];
                $coupon_id = $postData['udf2'];
                $user = $this->getDoctrine()->getManager()->getRepository('AdminBundle:usermaster')->find($user_id);
                
                $user_package_relation_id = $postData['productinfo'];
                $user_package_relation_info = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Userpackagerelation')->find($user_package_relation_id);
                if($user_package_relation_info){                   
                    
                    $subscription_info = $this->getDoctrine()->getManager()->getRepository("AdminBundle:subscription_master")->find($user_package_relation_info->getPackageId());
                    $subscription_amt = $subscription_info->getPrice();
                    $subscription_id = $subscription_info->getId();
                }

                // store transaction
                $transaction = new transaction_master();
                $transaction->setRef_number($_POST['bank_ref_num']);
                $transaction->setPayment_id($_POST['mihpayid']);
                $transaction->setProduct_id($_POST['productinfo']);
                $transaction->setUser_id($user_id);
                $transaction->setEmail($user->getUser_email_password());
                $transaction->setStatus($_POST['status']);
                $transaction->setUnmappedstatus($_POST['unmappedstatus']);
                $transaction->setTxnid($_POST['txnid']);
                $transaction->setAmount($_POST['amount']);
                $transaction->setNameoncard($_POST['name_on_card']);
                $transaction->setTxn_field_1('');
                $transaction->setTxn_field_2('');
                $transaction->setTxn_field_3('');
                $transaction->setCreated_datetime(date("Y-m-d H:i:s"));
                $transaction->setIs_deleted('0');

                $entity->persist($transaction);
                $entity->flush();
                $ProjectTransactionID = $transaction->getId();
            }
        } catch(\Exception $e) {
            echo $e->getMessage();exit;
        } 

        $site_url = $this->site_base_url.$subscription_id;
       
        header("location:{$site_url}");
        exit();
    }
    
    
}
