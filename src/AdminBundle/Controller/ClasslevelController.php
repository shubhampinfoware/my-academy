<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\PropertyAccess\PropertyAccess;
use AdminBundle\Entity\users;
use AdminBundle\Entity\Userinfo;
use AdminBundle\Entity\Rolemaster;
use AdminBundle\Entity\Classlevel;
use AdminBundle\Entity\mediamaster;

/**
 * @Route("/")
 */
class ClasslevelController extends BaseController {

    public function __construct() {
        $obj = new BaseController();
        $obj->checkSessionAction();
    }

    /**
     * @Route("/classlevellist")
     * @Template()
     */
    public function indexAction() {
        $data = null;
        $em = $this->getDoctrine()->getManager();
        $class_level =  $em->getRepository('AdminBundle:Classlevel')->findBy(array('is_deleted'=>'0'));
      
        return array("class_level" => $class_level);
    }

    /**
     * @Route("/addclasslevel/{id}",defaults={"id"=""})
     * @Template()
     */
    public function addclasslevelAction($id) {

        // $country = $this->getCountry();
        if (!empty($id)) {
            $class_level = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Classlevel')->findOneBy(array("is_deleted" => "0", "class_level_id" => $id));
            return array("class_level" => $class_level, "country" => '');
        }

        return array("country" => '', "counter" => 1);
    }

    /**
     * @Route("/saveclasslevel")
     */
    public function saveclasslevelAction() {
        $request = new Request(
                $_GET, $_POST, [], $_COOKIE, $_FILES, $_SERVER
        );
        $name = $_POST["name"];      
        $description = $_POST["description"];
        $status = $_POST["status"];      
        
        $dm = $this->getDoctrine()->getManager();

        if (isset($_POST["id"]) && $_POST["id"] != "") {
            $id = $_POST["id"];
            $class_level = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Classlevel')->findOneBy(array("is_deleted" => "0", "class_level_id" => $id));
            
            // $class_level = new Classlevel();
            $class_level->setLevel_name($name);
            $class_level->setLevel_desc($description);
            $class_level->setLevel_status($status);
            $class_level->setIs_deleted(0);
            // $dm = $this->getDoctrine()->getManager();

            $dm->flush();


            $this->get('session')->getFlashBag()->set('success_msg', " updated Successfully");

            return $this->redirectToRoute('admin_classlevel_index');
        } else {
            $class_level = new Classlevel();
            $class_level->setLevel_name($name);
            $class_level->setLevel_desc($description);
            $class_level->setLevel_status($status);
            $class_level->setIs_deleted(0);
            $dm = $this->getDoctrine()->getManager();
            $dm->persist($class_level);
            $dm->flush();
            $id = $class_level->getClass_level_id();
        }

        $this->get('session')->getFlashBag()->set('success_msg', " Added Successfully");

        return $this->redirectToRoute('admin_classlevel_index');
    }

     /**
     * @Route("/deleteclasslevel/{id}",defaults={"id":""})
     * @Template()
     */
    public function deleteclasslevelAction($id) {
        $dm = $this->getDoctrine()->getManager();
        if ($id != '0') {
            $repository = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Classlevel');
            $game = $repository->find($id);
            if (!empty($game)) {
                $game->setIs_deleted(1);
                $dm->flush();
            }
        }
        $this->get("session")->getFlashBag()->set("success_msg", " Deleted successfully");
        return $this->redirect($this->generateUrl("admin_classlevel_index"));
    }
    
      /**
     * @Route("/classlevelstatusajax")
     */
    public function classlevelstatusajaxAction() {
        $dm = $this->getDoctrine()->getManager();
        if (isset($_REQUEST['flag']) && isset($_REQUEST['facility_id'])) {
            if (!empty($_REQUEST['facility_id'])) {
                $facility_id = $_REQUEST['facility_id'];
                $facility = $dm
                        ->getRepository("AdminBundle:Classlevel")
                        ->findOneBy(array("is_deleted" => "0", "id" => $facility_id));
                if ($facility != null) {
                    $curr_status = $facility->getLevel_status();
                    $status = $curr_status == 'active' ? 'inactive' : 'active';

                    $facility->setLevel_status($status);
                    $dm->flush();
                }
            }
        }
        return new Response(1);
    }

}
