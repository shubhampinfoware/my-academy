<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use AdminBundle\Entity\Webservicelistmaster;

/**
 * @Route("/{domain}")
 */
class WebserviceController extends BaseController {

    public function __construct() {
        parent::__construct();
    }

    /**
     * @Route("/webservices/{ws_id}",defaults={"ws_id"="0"})
     * @Template
     */
    public function indexAction(Request $req, $ws_id) {
        $baseurl = $req->getScheme() . '://' . $req->getHttpHost() . $req->getBasePath();
        $web_service_selected = array();
        if ($ws_id != Null && $ws_id != 0) {
            $web_service_selected = $this->getDoctrine()->getManager()->getRepository(Webservicelistmaster :: class)->findOneBy(array('web_service_list_master_id' => $ws_id));
        }
        $web_service = $this->getDoctrine()->getManager()->getRepository(Webservicelistmaster :: class)->findAll();
        return array('web_service' => $web_service, 'web_service_selected' => $web_service_selected, 'base_url' => $baseurl);
    }

    /**
     * @Route("/addwebservices/")
     * @Template
     */
    public function addwebserviceAction(Request $req) {
        $ws_id = $req->request->get('ws_id');
        $em = $this->getDoctrine()->getManager();
        $ws_url = $req->request->get('ws_url');
        $check = $em->getRepository(Webservicelistmaster :: class)->findOneBy(array('web_service_list_master_id' => $ws_id));

		$response_param = '';		
		if($req->request->get('ws_response_param') != NULL){
			$response_param = $req->request->get('ws_response_param');		
		}

        if ($check) {
            $check->setWs_name($req->request->get('ws_name'));
            $check->setWs_url($ws_url);
            $check->setRequest_param($req->request->get('ws_req_param'));
            $check->setDetails($req->request->get('ws_details'));
            $check->setResponse_param($response_param);
            $em->flush();
            $this->get('session')->getFlashBag()->set('success', ' Ws updated successfully');
            return $this->redirect($this->generateUrl("admin_webservice_index", array('domain' => $this->get('session')->get('domain'))));
        } else {
            $new_Webservicelistmaster = new Webservicelistmaster();
            $new_Webservicelistmaster->setWs_name($req->request->get('ws_name'));
            $new_Webservicelistmaster->setWs_url($ws_url);
            $new_Webservicelistmaster->setRequest_param($req->request->get('ws_req_param'));
            $new_Webservicelistmaster->setDetails($req->request->get('ws_details'));
            $new_Webservicelistmaster->setResponse_param($response_param);
            $em->persist($new_Webservicelistmaster);
            $em->flush();
            $this->get('session')->getFlashBag()->set('success', ' Ws added successfully');
            return $this->redirect($this->generateUrl("admin_webservice_index", array('domain' => $this->get('session')->get('domain'))));
        }
    }

}
