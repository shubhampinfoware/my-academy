<?php
namespace AdminBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\PropertyAccess\PropertyAccess;

use AdminBundle\Entity\Bannermaster;

/**
 * @Route("/")
 */
class BannerController extends BaseController
{
    public function __construct()
    {
        $obj = new BaseController();
        $obj->checkSessionAction();
    }

    /**
     * @Route("/banner")
     * @Template()
     */
    public function indexAction()
    {
        $data = null;
        $repository = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Bannermaster');
        $games = $repository->createQueryBuilder()
            ->field('is_deleted')
            ->notEqual("1")
            ->getQuery()->execute()->toArray();
        return array("banner" => $games);
    }

    /**
     * @Route("/addbanner/{id}",defaults={"id"=""})
     * @Template()
     */
    public function addbannerAction($id)
    {
        // $country = $this->getCountry();
        if (!empty($id)) {
            $Bannermaster = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Bannermaster')->findOneBy(array("is_deleted" => "0", "bannermaster_id" => $id));

            return array("banner" => $Bannermaster, "country" => '');
        }
        return array("country" => '', "counter" => 1);
    }

    /**
     * @Route("/savebanner")
     */
    public function savebannerAction()
    {
        $request = new Request(
            $_GET,
            $_POST,
            [],
            $_COOKIE,
            $_FILES,
            $_SERVER
        );
        $name = $_POST["name"];
        $link1 = $_POST["link1"];
        $link2 = $_POST["link2"];
        $description = $_POST["description"];
        $status = $_POST["status"];
        $image_id = 0;
        if (isset($_FILES['image']) && !empty($_FILES['image'])) {
            $category_logo = $_FILES['image'];
            $logo = $category_logo['name'];
            $media_type_id = 1;
            $tmpname = $category_logo['tmp_name'];
            $file_path = $this->container->getParameter('file_path');
            $logo_path = $file_path . '/banner';
            $logo_upload_dir = $this->container->getParameter('upload_dir') . '/banner/';
            $image_id = $this->mediauploadAction($logo, $tmpname, $logo_path, $logo_upload_dir, $media_type_id);
        }
        if (isset($_POST["id"]) && $_POST["id"] != "") {
            $id = $_POST["id"];
            $bannermaster = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Bannermaster')->findOneBy(array("is_deleted" => "0", "id" => $id));
            if ($image_id != 0 and !empty($image_id)) {
                $bannermaster->setMediaId($image_id);
            }
            $bannermaster->setName($name);
            $bannermaster->setDescription($description);
            $bannermaster->setType("home");
            $bannermaster->setLink1($link1);
            $bannermaster->setLink2($link2);
            $bannermaster->setStatus($status);
            $bannermaster->setIs_deleted(0);
            $dm = $this->getDoctrine()->getManager();
            $dm->flush();

            $this->get('session')->getFlashBag()->set('success_msg', " updated Successfully");
            return $this->redirectToRoute('admin_banner_index');
        } else {
            $bannermaster = new Bannermaster();
            $bannermaster->setName($name);
            $bannermaster->setDescription($description);
            $bannermaster->setMediaId($image_id);
            $bannermaster->setType("home");
            $bannermaster->setLink1($link1);
            $bannermaster->setLink2($link2);
            $bannermaster->setStatus($status);
            $bannermaster->setIs_deleted(0);
            $dm = $this->getDoctrine()->getManager();
            $dm->persist($bannermaster);
            $dm->flush();
            $id = $bannermaster->getBannermasterId();
        }
        $this->get('session')->getFlashBag()->set('success_msg', " Added Successfully");
        return $this->redirectToRoute('admin_banner_index');
    }

    /**
     * @Route("/deletebanner/{id}",defaults={"id":""})
     * @Template()
     */
    public function deletebannerAction($id)
    {
        $dm = $this->getDoctrine()->getManager();
        if ($id != '0') {
            $repository = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Bannermaster');
            $game = $repository->find($id);
            if (!empty($game)) {
                $game->setIs_deleted(1);
                $dm->flush();
            }
        }
        $this->get("session")->getFlashBag()->set("success_msg", " Deleted successfully");
        return $this->redirect($this->generateUrl("admin_banner_index"));
    }

    /**
     * @Route("/statusajax")
     */
    public function statusajaxAction()
    {
        $dm = $this->getDoctrine()->getManager();
        if (isset($_REQUEST['flag']) && isset($_REQUEST['facility_id'])) {
            if (!empty($_REQUEST['facility_id'])) {
                $bannermaster_id = $_REQUEST['facility_id'];
                $facility = $dm
                    ->getRepository("AdminBundle:Bannermaster")
                    ->findOneBy(array("is_deleted" => "0", "bannermaster_id" => $bannermaster_id));
                if ($facility != null) {
                    $curr_status = $facility->getStatus();
                    $status = $curr_status == 'Active' ? 'Inactive' : 'Active';
                    $facility->setStatus($status);
                    $dm->flush();
                }
            }
        }
        return new Response(1);
    }
}
